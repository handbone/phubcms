/**
 * CheckNumberPortabilityByPhoneNumberRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.olleh.pHubCMS.api.model.oif552;

@SuppressWarnings({ "serial", "unused" })
public class CheckNumberPortabilityByPhoneNumberRequest implements java.io.Serializable {

	private java.lang.String TRANSACTIONID;
	private java.lang.String SEQUENCENO;
	private java.lang.String USERID;
	private java.lang.String SCREENID;
	private java.lang.String CALL_CTN;

	public CheckNumberPortabilityByPhoneNumberRequest() {
	}

	public CheckNumberPortabilityByPhoneNumberRequest(
			java.lang.String TRANSACTIONID
			, java.lang.String SEQUENCENO
			, java.lang.String USERID
			, java.lang.String SCREENID
			, java.lang.String CALL_CTN) {
		this.TRANSACTIONID = TRANSACTIONID;
		this.SEQUENCENO = SEQUENCENO;
		this.USERID = USERID;
		this.SCREENID = SCREENID;
		this.CALL_CTN = CALL_CTN;
	}

	/**
	 * Gets the TRANSACTIONID value for this
	 * CheckNumberPortabilityByPhoneNumberRequest.
	 * 
	 * @return TRANSACTIONID
	 */
	public java.lang.String getTRANSACTIONID() {
		return TRANSACTIONID;
	}

	/**
	 * Sets the TRANSACTIONID value for this
	 * CheckNumberPortabilityByPhoneNumberRequest.
	 * 
	 * @param TRANSACTIONID
	 */
	public void setTRANSACTIONID(java.lang.String TRANSACTIONID) {
		this.TRANSACTIONID = TRANSACTIONID;
	}

	/**
	 * Gets the SEQUENCENO value for this
	 * CheckNumberPortabilityByPhoneNumberRequest.
	 * 
	 * @return SEQUENCENO
	 */
	public java.lang.String getSEQUENCENO() {
		return SEQUENCENO;
	}

	/**
	 * Sets the SEQUENCENO value for this
	 * CheckNumberPortabilityByPhoneNumberRequest.
	 * 
	 * @param SEQUENCENO
	 */
	public void setSEQUENCENO(java.lang.String SEQUENCENO) {
		this.SEQUENCENO = SEQUENCENO;
	}

	/**
	 * Gets the USERID value for this
	 * CheckNumberPortabilityByPhoneNumberRequest.
	 * 
	 * @return USERID
	 */
	public java.lang.String getUSERID() {
		return USERID;
	}

	/**
	 * Sets the USERID value for this
	 * CheckNumberPortabilityByPhoneNumberRequest.
	 * 
	 * @param USERID
	 */
	public void setUSERID(java.lang.String USERID) {
		this.USERID = USERID;
	}

	/**
	 * Gets the SCREENID value for this
	 * CheckNumberPortabilityByPhoneNumberRequest.
	 * 
	 * @return SCREENID
	 */
	public java.lang.String getSCREENID() {
		return SCREENID;
	}

	/**
	 * Sets the SCREENID value for this
	 * CheckNumberPortabilityByPhoneNumberRequest.
	 * 
	 * @param SCREENID
	 */
	public void setSCREENID(java.lang.String SCREENID) {
		this.SCREENID = SCREENID;
	}

	/**
	 * Gets the CALL_CTN value for this
	 * CheckNumberPortabilityByPhoneNumberRequest.
	 * 
	 * @return CALL_CTN
	 */
	public java.lang.String getCALL_CTN() {
		return CALL_CTN;
	}

	/**
	 * Sets the CALL_CTN value for this
	 * CheckNumberPortabilityByPhoneNumberRequest.
	 * 
	 * @param CALL_CTN
	 */
	public void setCALL_CTN(java.lang.String CALL_CTN) {
		this.CALL_CTN = CALL_CTN;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof CheckNumberPortabilityByPhoneNumberRequest))
			return false;
		CheckNumberPortabilityByPhoneNumberRequest other = (CheckNumberPortabilityByPhoneNumberRequest) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.TRANSACTIONID == null && other.getTRANSACTIONID() == null)
						|| (this.TRANSACTIONID != null && this.TRANSACTIONID.equals(other.getTRANSACTIONID())))
				&& ((this.SEQUENCENO == null && other.getSEQUENCENO() == null)
						|| (this.SEQUENCENO != null && this.SEQUENCENO.equals(other.getSEQUENCENO())))
				&& ((this.USERID == null && other.getUSERID() == null)
						|| (this.USERID != null && this.USERID.equals(other.getUSERID())))
				&& ((this.SCREENID == null && other.getSCREENID() == null)
						|| (this.SCREENID != null && this.SCREENID.equals(other.getSCREENID())))
				&& ((this.CALL_CTN == null && other.getCALL_CTN() == null)
						|| (this.CALL_CTN != null && this.CALL_CTN.equals(other.getCALL_CTN())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getTRANSACTIONID() != null) {
			_hashCode += getTRANSACTIONID().hashCode();
		}
		if (getSEQUENCENO() != null) {
			_hashCode += getSEQUENCENO().hashCode();
		}
		if (getUSERID() != null) {
			_hashCode += getUSERID().hashCode();
		}
		if (getSCREENID() != null) {
			_hashCode += getSCREENID().hashCode();
		}
		if (getCALL_CTN() != null) {
			_hashCode += getCALL_CTN().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

}
