/**
 * WSDL_MessageSendSMSReportNoNetCharge_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.olleh.pHubCMS.api.model.oif2118;

public class WSDL_MessageSendSMSReportNoNetCharge_ServiceLocator extends org.apache.axis.client.Service
		implements com.olleh.pHubCMS.api.model.oif2118.WSDL_MessageSendSMSReportNoNetCharge_Service {

	public WSDL_MessageSendSMSReportNoNetCharge_ServiceLocator() {
	}

	public WSDL_MessageSendSMSReportNoNetCharge_ServiceLocator(org.apache.axis.EngineConfiguration config) {
		super(config);
	}

	public WSDL_MessageSendSMSReportNoNetCharge_ServiceLocator(java.lang.String wsdlLoc,
			javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
		super(wsdlLoc, sName);
	}

	// Use to get a proxy class for WSDL_MessageSendSMSReportNoNetChargeSOAP
	private java.lang.String WSDL_MessageSendSMSReportNoNetChargeSOAP_address = "http://msg.api.kt.com/MESG";

	public java.lang.String getWSDL_MessageSendSMSReportNoNetChargeSOAPAddress() {
		return WSDL_MessageSendSMSReportNoNetChargeSOAP_address;
	}

	// The WSDD service name defaults to the port name.
	private java.lang.String WSDL_MessageSendSMSReportNoNetChargeSOAPWSDDServiceName = "WSDL_MessageSendSMSReportNoNetChargeSOAP";

	public java.lang.String getWSDL_MessageSendSMSReportNoNetChargeSOAPWSDDServiceName() {
		return WSDL_MessageSendSMSReportNoNetChargeSOAPWSDDServiceName;
	}

	public void setWSDL_MessageSendSMSReportNoNetChargeSOAPWSDDServiceName(java.lang.String name) {
		WSDL_MessageSendSMSReportNoNetChargeSOAPWSDDServiceName = name;
	}

	public com.olleh.pHubCMS.api.model.oif2118.WSDL_MessageSendSMSReportNoNetCharge_PortType getWSDL_MessageSendSMSReportNoNetChargeSOAP()
			throws javax.xml.rpc.ServiceException {
		java.net.URL endpoint;
		try {
			endpoint = new java.net.URL(WSDL_MessageSendSMSReportNoNetChargeSOAP_address);
		} catch (java.net.MalformedURLException e) {
			throw new javax.xml.rpc.ServiceException(e);
		}
		return getWSDL_MessageSendSMSReportNoNetChargeSOAP(endpoint);
	}

	public com.olleh.pHubCMS.api.model.oif2118.WSDL_MessageSendSMSReportNoNetCharge_PortType getWSDL_MessageSendSMSReportNoNetChargeSOAP(
			java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
		try {
			com.olleh.pHubCMS.api.model.oif2118.WSDL_MessageSendSMSReportNoNetChargeSOAPStub _stub = new com.olleh.pHubCMS.api.model.oif2118.WSDL_MessageSendSMSReportNoNetChargeSOAPStub(
					portAddress, this);
			_stub.setPortName(getWSDL_MessageSendSMSReportNoNetChargeSOAPWSDDServiceName());
			return _stub;
		} catch (org.apache.axis.AxisFault e) {
			return null;
		}
	}

	public void setWSDL_MessageSendSMSReportNoNetChargeSOAPEndpointAddress(java.lang.String address) {
		WSDL_MessageSendSMSReportNoNetChargeSOAP_address = address;
	}

	/**
	 * For the given interface, get the stub implementation. If this service has
	 * no port for the given interface, then ServiceException is thrown.
	 */
	public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
		try {
			if (com.olleh.pHubCMS.api.model.oif2118.WSDL_MessageSendSMSReportNoNetCharge_PortType.class
					.isAssignableFrom(serviceEndpointInterface)) {
				com.olleh.pHubCMS.api.model.oif2118.WSDL_MessageSendSMSReportNoNetChargeSOAPStub _stub = new com.olleh.pHubCMS.api.model.oif2118.WSDL_MessageSendSMSReportNoNetChargeSOAPStub(
						new java.net.URL(WSDL_MessageSendSMSReportNoNetChargeSOAP_address), this);
				_stub.setPortName(getWSDL_MessageSendSMSReportNoNetChargeSOAPWSDDServiceName());
				return _stub;
			}
		} catch (java.lang.Throwable t) {
			throw new javax.xml.rpc.ServiceException(t);
		}
		throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  "
				+ (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
	}

	/**
	 * For the given interface, get the stub implementation. If this service has
	 * no port for the given interface, then ServiceException is thrown.
	 */
	public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface)
			throws javax.xml.rpc.ServiceException {
		if (portName == null) {
			return getPort(serviceEndpointInterface);
		}
		java.lang.String inputPortName = portName.getLocalPart();
		if ("WSDL_MessageSendSMSReportNoNetChargeSOAP".equals(inputPortName)) {
			return getWSDL_MessageSendSMSReportNoNetChargeSOAP();
		} else {
			java.rmi.Remote _stub = getPort(serviceEndpointInterface);
			((org.apache.axis.client.Stub) _stub).setPortName(portName);
			return _stub;
		}
	}

	public javax.xml.namespace.QName getServiceName() {
		return new javax.xml.namespace.QName("http://xml.kt.com/sdp/so/BS/MessageSendSMSReportNoNetCharge",
				"WSDL_MessageSendSMSReportNoNetCharge");
	}

	private java.util.HashSet ports = null;

	public java.util.Iterator getPorts() {
		if (ports == null) {
			ports = new java.util.HashSet();
			ports.add(new javax.xml.namespace.QName("http://xml.kt.com/sdp/so/BS/MessageSendSMSReportNoNetCharge",
					"WSDL_MessageSendSMSReportNoNetChargeSOAP"));
		}
		return ports.iterator();
	}

	/**
	 * Set the endpoint address for the specified port name.
	 */
	public void setEndpointAddress(java.lang.String portName, java.lang.String address)
			throws javax.xml.rpc.ServiceException {

		if ("WSDL_MessageSendSMSReportNoNetChargeSOAP".equals(portName)) {
			setWSDL_MessageSendSMSReportNoNetChargeSOAPEndpointAddress(address);
		} else { // Unknown Port Name
			throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
		}
	}

	/**
	 * Set the endpoint address for the specified port name.
	 */
	public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address)
			throws javax.xml.rpc.ServiceException {
		setEndpointAddress(portName.getLocalPart(), address);
	}

}
