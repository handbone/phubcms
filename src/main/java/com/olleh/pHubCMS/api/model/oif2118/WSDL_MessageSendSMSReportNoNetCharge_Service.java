/**
 * WSDL_MessageSendSMSReportNoNetCharge_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.olleh.pHubCMS.api.model.oif2118;

public interface WSDL_MessageSendSMSReportNoNetCharge_Service extends javax.xml.rpc.Service {
    public java.lang.String getWSDL_MessageSendSMSReportNoNetChargeSOAPAddress();

    public com.olleh.pHubCMS.api.model.oif2118.WSDL_MessageSendSMSReportNoNetCharge_PortType getWSDL_MessageSendSMSReportNoNetChargeSOAP() throws javax.xml.rpc.ServiceException;

    public com.olleh.pHubCMS.api.model.oif2118.WSDL_MessageSendSMSReportNoNetCharge_PortType getWSDL_MessageSendSMSReportNoNetChargeSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
