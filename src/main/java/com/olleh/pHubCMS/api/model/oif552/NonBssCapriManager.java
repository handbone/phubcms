/**
 * NonBssCapriManager.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.olleh.pHubCMS.api.model.oif552;

public interface NonBssCapriManager extends java.rmi.Remote {
	
    public CheckNumberPortabilityByPhoneNumberResponse checkNumberPortabilityByPhoneNumber(CheckNumberPortabilityByPhoneNumberRequest parameters) throws java.rmi.RemoteException, WamException;
}
