/**
 * MessageSendSMSReportNoNetChargeResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.olleh.pHubCMS.api.model.oif2118;

@SuppressWarnings({ "serial", "unused" })
public class MessageSendSMSReportNoNetChargeResponse implements java.io.Serializable {
	private java.lang.String TRANSACTIONID;
	private java.lang.String SEQUENCENO;
	private java.lang.String RT;
	private java.lang.String RT_MSG;
	private java.lang.String ERROR_CODE;
	private java.lang.String ERROR_DESCRIPTION;
	private java.lang.String QUERYSESSIONKEY;

	public MessageSendSMSReportNoNetChargeResponse() {
	}

	public MessageSendSMSReportNoNetChargeResponse(
			java.lang.String TRANSACTIONID
			, java.lang.String SEQUENCENO
			, java.lang.String RT
			, java.lang.String RT_MSG
			, java.lang.String ERROR_CODE
			, java.lang.String ERROR_DESCRIPTION
			, java.lang.String QUERYSESSIONKEY) {
		this.TRANSACTIONID = TRANSACTIONID;
		this.SEQUENCENO = SEQUENCENO;
		this.RT = RT;
		this.RT_MSG = RT_MSG;
		this.ERROR_CODE = ERROR_CODE;
		this.ERROR_DESCRIPTION = ERROR_DESCRIPTION;
		this.QUERYSESSIONKEY = QUERYSESSIONKEY;
	}

	/**
	 * Gets the TRANSACTIONID value for this
	 * MessageSendSMSReportNoNetChargeResponse.
	 * 
	 * @return TRANSACTIONID
	 */
	public java.lang.String getTRANSACTIONID() {
		return TRANSACTIONID;
	}

	/**
	 * Sets the TRANSACTIONID value for this
	 * MessageSendSMSReportNoNetChargeResponse.
	 * 
	 * @param TRANSACTIONID
	 */
	public void setTRANSACTIONID(java.lang.String TRANSACTIONID) {
		this.TRANSACTIONID = TRANSACTIONID;
	}

	/**
	 * Gets the SEQUENCENO value for this
	 * MessageSendSMSReportNoNetChargeResponse.
	 * 
	 * @return SEQUENCENO
	 */
	public java.lang.String getSEQUENCENO() {
		return SEQUENCENO;
	}

	/**
	 * Sets the SEQUENCENO value for this
	 * MessageSendSMSReportNoNetChargeResponse.
	 * 
	 * @param SEQUENCENO
	 */
	public void setSEQUENCENO(java.lang.String SEQUENCENO) {
		this.SEQUENCENO = SEQUENCENO;
	}

	/**
	 * Gets the RT value for this MessageSendSMSReportNoNetChargeResponse.
	 * 
	 * @return RT
	 */
	public java.lang.String getRT() {
		return RT;
	}

	/**
	 * Sets the RT value for this MessageSendSMSReportNoNetChargeResponse.
	 * 
	 * @param RT
	 */
	public void setRT(java.lang.String RT) {
		this.RT = RT;
	}

	/**
	 * Gets the RT_MSG value for this MessageSendSMSReportNoNetChargeResponse.
	 * 
	 * @return RT_MSG
	 */
	public java.lang.String getRT_MSG() {
		return RT_MSG;
	}

	/**
	 * Sets the RT_MSG value for this MessageSendSMSReportNoNetChargeResponse.
	 * 
	 * @param RT_MSG
	 */
	public void setRT_MSG(java.lang.String RT_MSG) {
		this.RT_MSG = RT_MSG;
	}

	/**
	 * Gets the ERROR_CODE value for this
	 * MessageSendSMSReportNoNetChargeResponse.
	 * 
	 * @return ERROR_CODE
	 */
	public java.lang.String getERROR_CODE() {
		return ERROR_CODE;
	}

	/**
	 * Sets the ERROR_CODE value for this
	 * MessageSendSMSReportNoNetChargeResponse.
	 * 
	 * @param ERROR_CODE
	 */
	public void setERROR_CODE(java.lang.String ERROR_CODE) {
		this.ERROR_CODE = ERROR_CODE;
	}

	/**
	 * Gets the ERROR_DESCRIPTION value for this
	 * MessageSendSMSReportNoNetChargeResponse.
	 * 
	 * @return ERROR_DESCRIPTION
	 */
	public java.lang.String getERROR_DESCRIPTION() {
		return ERROR_DESCRIPTION;
	}

	/**
	 * Sets the ERROR_DESCRIPTION value for this
	 * MessageSendSMSReportNoNetChargeResponse.
	 * 
	 * @param ERROR_DESCRIPTION
	 */
	public void setERROR_DESCRIPTION(java.lang.String ERROR_DESCRIPTION) {
		this.ERROR_DESCRIPTION = ERROR_DESCRIPTION;
	}

	/**
	 * Gets the QUERYSESSIONKEY value for this
	 * MessageSendSMSReportNoNetChargeResponse.
	 * 
	 * @return QUERYSESSIONKEY
	 */
	public java.lang.String getQUERYSESSIONKEY() {
		return QUERYSESSIONKEY;
	}

	/**
	 * Sets the QUERYSESSIONKEY value for this
	 * MessageSendSMSReportNoNetChargeResponse.
	 * 
	 * @param QUERYSESSIONKEY
	 */
	public void setQUERYSESSIONKEY(java.lang.String QUERYSESSIONKEY) {
		this.QUERYSESSIONKEY = QUERYSESSIONKEY;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof MessageSendSMSReportNoNetChargeResponse))
			return false;
		MessageSendSMSReportNoNetChargeResponse other = (MessageSendSMSReportNoNetChargeResponse) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.TRANSACTIONID == null && other.getTRANSACTIONID() == null)
						|| (this.TRANSACTIONID != null && this.TRANSACTIONID.equals(other.getTRANSACTIONID())))
				&& ((this.SEQUENCENO == null && other.getSEQUENCENO() == null)
						|| (this.SEQUENCENO != null && this.SEQUENCENO.equals(other.getSEQUENCENO())))
				&& ((this.RT == null && other.getRT() == null) || (this.RT != null && this.RT.equals(other.getRT())))
				&& ((this.RT_MSG == null && other.getRT_MSG() == null)
						|| (this.RT_MSG != null && this.RT_MSG.equals(other.getRT_MSG())))
				&& ((this.ERROR_CODE == null && other.getERROR_CODE() == null)
						|| (this.ERROR_CODE != null && this.ERROR_CODE.equals(other.getERROR_CODE())))
				&& ((this.ERROR_DESCRIPTION == null && other.getERROR_DESCRIPTION() == null)
						|| (this.ERROR_DESCRIPTION != null
								&& this.ERROR_DESCRIPTION.equals(other.getERROR_DESCRIPTION())))
				&& ((this.QUERYSESSIONKEY == null && other.getQUERYSESSIONKEY() == null)
						|| (this.QUERYSESSIONKEY != null && this.QUERYSESSIONKEY.equals(other.getQUERYSESSIONKEY())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getTRANSACTIONID() != null) {
			_hashCode += getTRANSACTIONID().hashCode();
		}
		if (getSEQUENCENO() != null) {
			_hashCode += getSEQUENCENO().hashCode();
		}
		if (getRT() != null) {
			_hashCode += getRT().hashCode();
		}
		if (getRT_MSG() != null) {
			_hashCode += getRT_MSG().hashCode();
		}
		if (getERROR_CODE() != null) {
			_hashCode += getERROR_CODE().hashCode();
		}
		if (getERROR_DESCRIPTION() != null) {
			_hashCode += getERROR_DESCRIPTION().hashCode();
		}
		if (getQUERYSESSIONKEY() != null) {
			_hashCode += getQUERYSESSIONKEY().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

}
