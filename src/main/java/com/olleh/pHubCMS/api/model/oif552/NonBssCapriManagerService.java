/**
 * NonBssCapriManagerService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.olleh.pHubCMS.api.model.oif552;

public interface NonBssCapriManagerService extends javax.xml.rpc.Service {
	
    public java.lang.String getNonBssCapriManagerAddress();

    public NonBssCapriManager getNonBssCapriManager() throws javax.xml.rpc.ServiceException;

    public NonBssCapriManager getNonBssCapriManager(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
