/**
 * MessageSendSMSReportNoNetCharge.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.olleh.pHubCMS.api.model.oif2118;

@SuppressWarnings({"serial", "unused"})
public class MessageSendSMSReportNoNetCharge implements java.io.Serializable {

	private java.lang.String TRANSACTIONID;
	private java.lang.String SEQUENCENO;
	private java.lang.String USERID;
	private java.lang.String SCREENID;
	private java.lang.String MSG_TYPE;
	private java.lang.String CALLBACK_CTN;
	private java.lang.String CALLBACK_URL;
	private java.lang.String MSG_CONTENT;
	private java.lang.String PFORM_TYPE;
	private java.lang.String SERVICE_TYPE;
	private java.lang.String CALL_CTN;
	private java.lang.String RCV_CTN;
	private java.lang.String BILL_URL;
	private java.lang.String REPORT_URL;
	private java.lang.String APP_ID;
	private java.lang.String BILLING_TYPE;
	private java.lang.String CALL_ORG_IND;
	private java.lang.String RCV_PHONE_TYPE;

	public MessageSendSMSReportNoNetCharge() {
	}

	public MessageSendSMSReportNoNetCharge(
			java.lang.String TRANSACTIONID
			, java.lang.String SEQUENCENO
			, java.lang.String USERID
			, java.lang.String SCREENID
			, java.lang.String MSG_TYPE
			, java.lang.String CALLBACK_CTN
			, java.lang.String CALLBACK_URL
			, java.lang.String MSG_CONTENT
			, java.lang.String PFORM_TYPE
			, java.lang.String SERVICE_TYPE
			, java.lang.String CALL_CTN
			, java.lang.String RCV_CTN
			, java.lang.String BILL_URL
			, java.lang.String REPORT_URL
			, java.lang.String APP_ID
			, java.lang.String BILLING_TYPE
			, java.lang.String CALL_ORG_IND
			, java.lang.String RCV_PHONE_TYPE) {
		this.TRANSACTIONID = TRANSACTIONID;
		this.SEQUENCENO = SEQUENCENO;
		this.USERID = USERID;
		this.SCREENID = SCREENID;
		this.MSG_TYPE = MSG_TYPE;
		this.CALLBACK_CTN = CALLBACK_CTN;
		this.CALLBACK_URL = CALLBACK_URL;
		this.MSG_CONTENT = MSG_CONTENT;
		this.PFORM_TYPE = PFORM_TYPE;
		this.SERVICE_TYPE = SERVICE_TYPE;
		this.CALL_CTN = CALL_CTN;
		this.RCV_CTN = RCV_CTN;
		this.BILL_URL = BILL_URL;
		this.REPORT_URL = REPORT_URL;
		this.APP_ID = APP_ID;
		this.BILLING_TYPE = BILLING_TYPE;
		this.CALL_ORG_IND = CALL_ORG_IND;
		this.RCV_PHONE_TYPE = RCV_PHONE_TYPE;
	}

	/**
	 * Gets the TRANSACTIONID value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @return TRANSACTIONID
	 */
	public java.lang.String getTRANSACTIONID() {
		return TRANSACTIONID;
	}

	/**
	 * Sets the TRANSACTIONID value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @param TRANSACTIONID
	 */
	public void setTRANSACTIONID(java.lang.String TRANSACTIONID) {
		this.TRANSACTIONID = TRANSACTIONID;
	}

	/**
	 * Gets the SEQUENCENO value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @return SEQUENCENO
	 */
	public java.lang.String getSEQUENCENO() {
		return SEQUENCENO;
	}

	/**
	 * Sets the SEQUENCENO value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @param SEQUENCENO
	 */
	public void setSEQUENCENO(java.lang.String SEQUENCENO) {
		this.SEQUENCENO = SEQUENCENO;
	}

	/**
	 * Gets the USERID value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @return USERID
	 */
	public java.lang.String getUSERID() {
		return USERID;
	}

	/**
	 * Sets the USERID value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @param USERID
	 */
	public void setUSERID(java.lang.String USERID) {
		this.USERID = USERID;
	}

	/**
	 * Gets the SCREENID value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @return SCREENID
	 */
	public java.lang.String getSCREENID() {
		return SCREENID;
	}

	/**
	 * Sets the SCREENID value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @param SCREENID
	 */
	public void setSCREENID(java.lang.String SCREENID) {
		this.SCREENID = SCREENID;
	}

	/**
	 * Gets the MSG_TYPE value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @return MSG_TYPE
	 */
	public java.lang.String getMSG_TYPE() {
		return MSG_TYPE;
	}

	/**
	 * Sets the MSG_TYPE value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @param MSG_TYPE
	 */
	public void setMSG_TYPE(java.lang.String MSG_TYPE) {
		this.MSG_TYPE = MSG_TYPE;
	}

	/**
	 * Gets the CALLBACK_CTN value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @return CALLBACK_CTN
	 */
	public java.lang.String getCALLBACK_CTN() {
		return CALLBACK_CTN;
	}

	/**
	 * Sets the CALLBACK_CTN value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @param CALLBACK_CTN
	 */
	public void setCALLBACK_CTN(java.lang.String CALLBACK_CTN) {
		this.CALLBACK_CTN = CALLBACK_CTN;
	}

	/**
	 * Gets the CALLBACK_URL value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @return CALLBACK_URL
	 */
	public java.lang.String getCALLBACK_URL() {
		return CALLBACK_URL;
	}

	/**
	 * Sets the CALLBACK_URL value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @param CALLBACK_URL
	 */
	public void setCALLBACK_URL(java.lang.String CALLBACK_URL) {
		this.CALLBACK_URL = CALLBACK_URL;
	}

	/**
	 * Gets the MSG_CONTENT value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @return MSG_CONTENT
	 */
	public java.lang.String getMSG_CONTENT() {
		return MSG_CONTENT;
	}

	/**
	 * Sets the MSG_CONTENT value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @param MSG_CONTENT
	 */
	public void setMSG_CONTENT(java.lang.String MSG_CONTENT) {
		this.MSG_CONTENT = MSG_CONTENT;
	}

	/**
	 * Gets the PFORM_TYPE value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @return PFORM_TYPE
	 */
	public java.lang.String getPFORM_TYPE() {
		return PFORM_TYPE;
	}

	/**
	 * Sets the PFORM_TYPE value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @param PFORM_TYPE
	 */
	public void setPFORM_TYPE(java.lang.String PFORM_TYPE) {
		this.PFORM_TYPE = PFORM_TYPE;
	}

	/**
	 * Gets the SERVICE_TYPE value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @return SERVICE_TYPE
	 */
	public java.lang.String getSERVICE_TYPE() {
		return SERVICE_TYPE;
	}

	/**
	 * Sets the SERVICE_TYPE value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @param SERVICE_TYPE
	 */
	public void setSERVICE_TYPE(java.lang.String SERVICE_TYPE) {
		this.SERVICE_TYPE = SERVICE_TYPE;
	}

	/**
	 * Gets the CALL_CTN value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @return CALL_CTN
	 */
	public java.lang.String getCALL_CTN() {
		return CALL_CTN;
	}

	/**
	 * Sets the CALL_CTN value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @param CALL_CTN
	 */
	public void setCALL_CTN(java.lang.String CALL_CTN) {
		this.CALL_CTN = CALL_CTN;
	}

	/**
	 * Gets the RCV_CTN value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @return RCV_CTN
	 */
	public java.lang.String getRCV_CTN() {
		return RCV_CTN;
	}

	/**
	 * Sets the RCV_CTN value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @param RCV_CTN
	 */
	public void setRCV_CTN(java.lang.String RCV_CTN) {
		this.RCV_CTN = RCV_CTN;
	}

	/**
	 * Gets the BILL_URL value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @return BILL_URL
	 */
	public java.lang.String getBILL_URL() {
		return BILL_URL;
	}

	/**
	 * Sets the BILL_URL value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @param BILL_URL
	 */
	public void setBILL_URL(java.lang.String BILL_URL) {
		this.BILL_URL = BILL_URL;
	}

	/**
	 * Gets the REPORT_URL value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @return REPORT_URL
	 */
	public java.lang.String getREPORT_URL() {
		return REPORT_URL;
	}

	/**
	 * Sets the REPORT_URL value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @param REPORT_URL
	 */
	public void setREPORT_URL(java.lang.String REPORT_URL) {
		this.REPORT_URL = REPORT_URL;
	}

	/**
	 * Gets the APP_ID value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @return APP_ID
	 */
	public java.lang.String getAPP_ID() {
		return APP_ID;
	}

	/**
	 * Sets the APP_ID value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @param APP_ID
	 */
	public void setAPP_ID(java.lang.String APP_ID) {
		this.APP_ID = APP_ID;
	}

	/**
	 * Gets the BILLING_TYPE value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @return BILLING_TYPE
	 */
	public java.lang.String getBILLING_TYPE() {
		return BILLING_TYPE;
	}

	/**
	 * Sets the BILLING_TYPE value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @param BILLING_TYPE
	 */
	public void setBILLING_TYPE(java.lang.String BILLING_TYPE) {
		this.BILLING_TYPE = BILLING_TYPE;
	}

	/**
	 * Gets the CALL_ORG_IND value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @return CALL_ORG_IND
	 */
	public java.lang.String getCALL_ORG_IND() {
		return CALL_ORG_IND;
	}

	/**
	 * Sets the CALL_ORG_IND value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @param CALL_ORG_IND
	 */
	public void setCALL_ORG_IND(java.lang.String CALL_ORG_IND) {
		this.CALL_ORG_IND = CALL_ORG_IND;
	}

	/**
	 * Gets the RCV_PHONE_TYPE value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @return RCV_PHONE_TYPE
	 */
	public java.lang.String getRCV_PHONE_TYPE() {
		return RCV_PHONE_TYPE;
	}

	/**
	 * Sets the RCV_PHONE_TYPE value for this MessageSendSMSReportNoNetCharge.
	 * 
	 * @param RCV_PHONE_TYPE
	 */
	public void setRCV_PHONE_TYPE(java.lang.String RCV_PHONE_TYPE) {
		this.RCV_PHONE_TYPE = RCV_PHONE_TYPE;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof MessageSendSMSReportNoNetCharge))
			return false;
		MessageSendSMSReportNoNetCharge other = (MessageSendSMSReportNoNetCharge) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.TRANSACTIONID == null && other.getTRANSACTIONID() == null)
						|| (this.TRANSACTIONID != null && this.TRANSACTIONID.equals(other.getTRANSACTIONID())))
				&& ((this.SEQUENCENO == null && other.getSEQUENCENO() == null)
						|| (this.SEQUENCENO != null && this.SEQUENCENO.equals(other.getSEQUENCENO())))
				&& ((this.USERID == null && other.getUSERID() == null)
						|| (this.USERID != null && this.USERID.equals(other.getUSERID())))
				&& ((this.SCREENID == null && other.getSCREENID() == null)
						|| (this.SCREENID != null && this.SCREENID.equals(other.getSCREENID())))
				&& ((this.MSG_TYPE == null && other.getMSG_TYPE() == null)
						|| (this.MSG_TYPE != null && this.MSG_TYPE.equals(other.getMSG_TYPE())))
				&& ((this.CALLBACK_CTN == null && other.getCALLBACK_CTN() == null)
						|| (this.CALLBACK_CTN != null && this.CALLBACK_CTN.equals(other.getCALLBACK_CTN())))
				&& ((this.CALLBACK_URL == null && other.getCALLBACK_URL() == null)
						|| (this.CALLBACK_URL != null && this.CALLBACK_URL.equals(other.getCALLBACK_URL())))
				&& ((this.MSG_CONTENT == null && other.getMSG_CONTENT() == null)
						|| (this.MSG_CONTENT != null && this.MSG_CONTENT.equals(other.getMSG_CONTENT())))
				&& ((this.PFORM_TYPE == null && other.getPFORM_TYPE() == null)
						|| (this.PFORM_TYPE != null && this.PFORM_TYPE.equals(other.getPFORM_TYPE())))
				&& ((this.SERVICE_TYPE == null && other.getSERVICE_TYPE() == null)
						|| (this.SERVICE_TYPE != null && this.SERVICE_TYPE.equals(other.getSERVICE_TYPE())))
				&& ((this.CALL_CTN == null && other.getCALL_CTN() == null)
						|| (this.CALL_CTN != null && this.CALL_CTN.equals(other.getCALL_CTN())))
				&& ((this.RCV_CTN == null && other.getRCV_CTN() == null)
						|| (this.RCV_CTN != null && this.RCV_CTN.equals(other.getRCV_CTN())))
				&& ((this.BILL_URL == null && other.getBILL_URL() == null)
						|| (this.BILL_URL != null && this.BILL_URL.equals(other.getBILL_URL())))
				&& ((this.REPORT_URL == null && other.getREPORT_URL() == null)
						|| (this.REPORT_URL != null && this.REPORT_URL.equals(other.getREPORT_URL())))
				&& ((this.APP_ID == null && other.getAPP_ID() == null)
						|| (this.APP_ID != null && this.APP_ID.equals(other.getAPP_ID())))
				&& ((this.BILLING_TYPE == null && other.getBILLING_TYPE() == null)
						|| (this.BILLING_TYPE != null && this.BILLING_TYPE.equals(other.getBILLING_TYPE())))
				&& ((this.CALL_ORG_IND == null && other.getCALL_ORG_IND() == null)
						|| (this.CALL_ORG_IND != null && this.CALL_ORG_IND.equals(other.getCALL_ORG_IND())))
				&& ((this.RCV_PHONE_TYPE == null && other.getRCV_PHONE_TYPE() == null)
						|| (this.RCV_PHONE_TYPE != null && this.RCV_PHONE_TYPE.equals(other.getRCV_PHONE_TYPE())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getTRANSACTIONID() != null) {
			_hashCode += getTRANSACTIONID().hashCode();
		}
		if (getSEQUENCENO() != null) {
			_hashCode += getSEQUENCENO().hashCode();
		}
		if (getUSERID() != null) {
			_hashCode += getUSERID().hashCode();
		}
		if (getSCREENID() != null) {
			_hashCode += getSCREENID().hashCode();
		}
		if (getMSG_TYPE() != null) {
			_hashCode += getMSG_TYPE().hashCode();
		}
		if (getCALLBACK_CTN() != null) {
			_hashCode += getCALLBACK_CTN().hashCode();
		}
		if (getCALLBACK_URL() != null) {
			_hashCode += getCALLBACK_URL().hashCode();
		}
		if (getMSG_CONTENT() != null) {
			_hashCode += getMSG_CONTENT().hashCode();
		}
		if (getPFORM_TYPE() != null) {
			_hashCode += getPFORM_TYPE().hashCode();
		}
		if (getSERVICE_TYPE() != null) {
			_hashCode += getSERVICE_TYPE().hashCode();
		}
		if (getCALL_CTN() != null) {
			_hashCode += getCALL_CTN().hashCode();
		}
		if (getRCV_CTN() != null) {
			_hashCode += getRCV_CTN().hashCode();
		}
		if (getBILL_URL() != null) {
			_hashCode += getBILL_URL().hashCode();
		}
		if (getREPORT_URL() != null) {
			_hashCode += getREPORT_URL().hashCode();
		}
		if (getAPP_ID() != null) {
			_hashCode += getAPP_ID().hashCode();
		}
		if (getBILLING_TYPE() != null) {
			_hashCode += getBILLING_TYPE().hashCode();
		}
		if (getCALL_ORG_IND() != null) {
			_hashCode += getCALL_ORG_IND().hashCode();
		}
		if (getRCV_PHONE_TYPE() != null) {
			_hashCode += getRCV_PHONE_TYPE().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

}
