/**
 * WSDL_MessageSendSMSReportNoNetCharge_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.olleh.pHubCMS.api.model.oif2118;

public interface WSDL_MessageSendSMSReportNoNetCharge_PortType extends java.rmi.Remote {
    public com.olleh.pHubCMS.api.model.oif2118.MessageSendSMSReportNoNetChargeResponse BS_MessageSendSMSReportNoNetCharge(com.olleh.pHubCMS.api.model.oif2118.MessageSendSMSReportNoNetCharge parameters) throws java.rmi.RemoteException, com.olleh.pHubCMS.api.model.oif2118.Sofaultmessage;
}
