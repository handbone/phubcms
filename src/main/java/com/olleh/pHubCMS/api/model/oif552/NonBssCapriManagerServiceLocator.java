/**
 * NonBssCapriManagerServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.olleh.pHubCMS.api.model.oif552;

@SuppressWarnings({ "serial", "rawtypes", "unchecked" })
public class NonBssCapriManagerServiceLocator extends org.apache.axis.client.Service implements NonBssCapriManagerService {
	
	// Use to get a proxy class for NonBssCapriManager
	private java.lang.String NonBssCapriManager_address = "https://cus.tbapi.kt.com:443/nonBssCapri/NonBssCapriManager";
	
	// The WSDD service name defaults to the port name.
	private java.lang.String NonBssCapriManagerWSDDServiceName = "NonBssCapriManager";
	
	private java.util.HashSet ports = null;

	public NonBssCapriManagerServiceLocator() {}

	public NonBssCapriManagerServiceLocator(org.apache.axis.EngineConfiguration config) {
		super(config);
	}

	public NonBssCapriManagerServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
		super(wsdlLoc, sName);
	}

	public java.lang.String getNonBssCapriManagerAddress() {
		return NonBssCapriManager_address;
	}
	
	public void setNonBssCapriManagerEndpointAddress(java.lang.String address) {
		NonBssCapriManager_address = address;
	}

	public java.lang.String getNonBssCapriManagerWSDDServiceName() {
		return NonBssCapriManagerWSDDServiceName;
	}

	public void setNonBssCapriManagerWSDDServiceName(java.lang.String name) {
		NonBssCapriManagerWSDDServiceName = name;
	}

	public NonBssCapriManager getNonBssCapriManager() throws javax.xml.rpc.ServiceException {
		java.net.URL endpoint;
		try {
			endpoint = new java.net.URL(NonBssCapriManager_address);
		} catch (java.net.MalformedURLException e) {
			throw new javax.xml.rpc.ServiceException(e);
		}
		return getNonBssCapriManager(endpoint);
	}

	public NonBssCapriManager getNonBssCapriManager(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
		try {
			NonBssCapriManagerSoapBindingStub _stub = new NonBssCapriManagerSoapBindingStub(portAddress, this);
			_stub.setPortName(getNonBssCapriManagerWSDDServiceName());
			return _stub;
		} catch (org.apache.axis.AxisFault e) {
			return null;
		}
	}

	/**
	 * For the given interface, get the stub implementation. If this service has
	 * no port for the given interface, then ServiceException is thrown.
	 */
	public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
		try {
			if (NonBssCapriManager.class.isAssignableFrom(serviceEndpointInterface)) {
				NonBssCapriManagerSoapBindingStub _stub = new NonBssCapriManagerSoapBindingStub(new java.net.URL(NonBssCapriManager_address), this);
				_stub.setPortName(getNonBssCapriManagerWSDDServiceName());
				return _stub;
			}
		} catch (java.lang.Throwable t) {
			throw new javax.xml.rpc.ServiceException(t);
		}
		throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  "+ (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
	}

	/**
	 * For the given interface, get the stub implementation. If this service has
	 * no port for the given interface, then ServiceException is thrown.
	 */
	public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
		if (portName == null) {
			return getPort(serviceEndpointInterface);
		}
		java.lang.String inputPortName = portName.getLocalPart();
		if ("NonBssCapriManager".equals(inputPortName)) {
			return getNonBssCapriManager();
		} else {
			java.rmi.Remote _stub = getPort(serviceEndpointInterface);
			((org.apache.axis.client.Stub) _stub).setPortName(portName);
			return _stub;
		}
	}

	public javax.xml.namespace.QName getServiceName() {
		return new javax.xml.namespace.QName("http://kt.com/sdp", "NonBssCapriManagerService");
	}

	public java.util.Iterator getPorts() {
		if (ports == null) {
			ports = new java.util.HashSet();
			ports.add(new javax.xml.namespace.QName("http://kt.com/sdp", "NonBssCapriManager"));
		}
		return ports.iterator();
	}

	/**
	 * Set the endpoint address for the specified port name.
	 */
	public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
		if ("NonBssCapriManager".equals(portName)) {
			setNonBssCapriManagerEndpointAddress(address);
		} else { // Unknown Port Name
			throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
		}
	}

	/**
	 * Set the endpoint address for the specified port name.
	 */
	public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
		setEndpointAddress(portName.getLocalPart(), address);
	}

}
