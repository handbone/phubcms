/**
 * Errordetail.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.olleh.pHubCMS.api.model.oif552;

@SuppressWarnings("serial")
public class Errordetail  implements java.io.Serializable {
	
    private java.lang.String errorcode;

    private java.lang.String errordescription;

    public Errordetail() {
    }

    public Errordetail(
           java.lang.String errorcode,
           java.lang.String errordescription) {
           this.errorcode = errorcode;
           this.errordescription = errordescription;
    }


    /**
     * Gets the errorcode value for this Errordetail.
     * 
     * @return errorcode
     */
    public java.lang.String getErrorcode() {
        return errorcode;
    }


    /**
     * Sets the errorcode value for this Errordetail.
     * 
     * @param errorcode
     */
    public void setErrorcode(java.lang.String errorcode) {
        this.errorcode = errorcode;
    }


    /**
     * Gets the errordescription value for this Errordetail.
     * 
     * @return errordescription
     */
    public java.lang.String getErrordescription() {
        return errordescription;
    }


    /**
     * Sets the errordescription value for this Errordetail.
     * 
     * @param errordescription
     */
    public void setErrordescription(java.lang.String errordescription) {
        this.errordescription = errordescription;
    }

    private java.lang.Object __equalsCalc = null;
    @SuppressWarnings("unused")
	public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Errordetail)) return false;
        Errordetail other = (Errordetail) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.errorcode==null && other.getErrorcode()==null) || 
             (this.errorcode!=null &&
              this.errorcode.equals(other.getErrorcode()))) &&
            ((this.errordescription==null && other.getErrordescription()==null) || 
             (this.errordescription!=null &&
              this.errordescription.equals(other.getErrordescription())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getErrorcode() != null) {
            _hashCode += getErrorcode().hashCode();
        }
        if (getErrordescription() != null) {
            _hashCode += getErrordescription().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

}
