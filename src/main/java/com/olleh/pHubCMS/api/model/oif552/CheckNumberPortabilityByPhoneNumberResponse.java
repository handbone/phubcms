/**
 * CheckNumberPortabilityByPhoneNumberResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.olleh.pHubCMS.api.model.oif552;

@SuppressWarnings({ "serial", "unused" })
public class CheckNumberPortabilityByPhoneNumberResponse implements java.io.Serializable {

	private java.lang.String TRANSACTIONID;
	private java.lang.String SEQUENCENO;
	private Errordetail ERRORDETAIL;
	private java.lang.String returnCode;
	private java.lang.String returnDesc;
	private java.lang.String IMSI;
	private java.lang.String ROUTE_INFO;
	private java.lang.String ROUTING_DIGIT;

	public CheckNumberPortabilityByPhoneNumberResponse() {}
	public CheckNumberPortabilityByPhoneNumberResponse(java.lang.String TRANSACTIONID, java.lang.String SEQUENCENO, Errordetail ERRORDETAIL, java.lang.String returnCode, java.lang.String returnDesc, java.lang.String IMSI, java.lang.String ROUTE_INFO, java.lang.String ROUTING_DIGIT) {
		this.TRANSACTIONID = TRANSACTIONID;
		this.SEQUENCENO = SEQUENCENO;
		this.ERRORDETAIL = ERRORDETAIL;
		this.returnCode = returnCode;
		this.returnDesc = returnDesc;
		this.IMSI = IMSI;
		this.ROUTE_INFO = ROUTE_INFO;
		this.ROUTING_DIGIT = ROUTING_DIGIT;
	}

	/**
	 * Gets the TRANSACTIONID value for this
	 * CheckNumberPortabilityByPhoneNumberResponse.
	 * 
	 * @return TRANSACTIONID
	 */
	public java.lang.String getTRANSACTIONID() {
		return TRANSACTIONID;
	}

	/**
	 * Sets the TRANSACTIONID value for this
	 * CheckNumberPortabilityByPhoneNumberResponse.
	 * 
	 * @param TRANSACTIONID
	 */
	public void setTRANSACTIONID(java.lang.String TRANSACTIONID) {
		this.TRANSACTIONID = TRANSACTIONID;
	}

	/**
	 * Gets the SEQUENCENO value for this
	 * CheckNumberPortabilityByPhoneNumberResponse.
	 * 
	 * @return SEQUENCENO
	 */
	public java.lang.String getSEQUENCENO() {
		return SEQUENCENO;
	}

	/**
	 * Sets the SEQUENCENO value for this
	 * CheckNumberPortabilityByPhoneNumberResponse.
	 * 
	 * @param SEQUENCENO
	 */
	public void setSEQUENCENO(java.lang.String SEQUENCENO) {
		this.SEQUENCENO = SEQUENCENO;
	}

	/**
	 * Gets the ERRORDETAIL value for this
	 * CheckNumberPortabilityByPhoneNumberResponse.
	 * 
	 * @return ERRORDETAIL
	 */
	public Errordetail getERRORDETAIL() {
		return ERRORDETAIL;
	}

	/**
	 * Sets the ERRORDETAIL value for this
	 * CheckNumberPortabilityByPhoneNumberResponse.
	 * 
	 * @param ERRORDETAIL
	 */
	public void setERRORDETAIL(Errordetail ERRORDETAIL) {
		this.ERRORDETAIL = ERRORDETAIL;
	}

	/**
	 * Gets the returnCode value for this
	 * CheckNumberPortabilityByPhoneNumberResponse.
	 * 
	 * @return returnCode
	 */
	public java.lang.String getReturnCode() {
		return returnCode;
	}

	/**
	 * Sets the returnCode value for this
	 * CheckNumberPortabilityByPhoneNumberResponse.
	 * 
	 * @param returnCode
	 */
	public void setReturnCode(java.lang.String returnCode) {
		this.returnCode = returnCode;
	}

	/**
	 * Gets the returnDesc value for this
	 * CheckNumberPortabilityByPhoneNumberResponse.
	 * 
	 * @return returnDesc
	 */
	public java.lang.String getReturnDesc() {
		return returnDesc;
	}

	/**
	 * Sets the returnDesc value for this
	 * CheckNumberPortabilityByPhoneNumberResponse.
	 * 
	 * @param returnDesc
	 */
	public void setReturnDesc(java.lang.String returnDesc) {
		this.returnDesc = returnDesc;
	}

	/**
	 * Gets the IMSI value for this CheckNumberPortabilityByPhoneNumberResponse.
	 * 
	 * @return IMSI
	 */
	public java.lang.String getIMSI() {
		return IMSI;
	}

	/**
	 * Sets the IMSI value for this CheckNumberPortabilityByPhoneNumberResponse.
	 * 
	 * @param IMSI
	 */
	public void setIMSI(java.lang.String IMSI) {
		this.IMSI = IMSI;
	}

	/**
	 * Gets the ROUTE_INFO value for this
	 * CheckNumberPortabilityByPhoneNumberResponse.
	 * 
	 * @return ROUTE_INFO
	 */
	public java.lang.String getROUTE_INFO() {
		return ROUTE_INFO;
	}

	/**
	 * Sets the ROUTE_INFO value for this
	 * CheckNumberPortabilityByPhoneNumberResponse.
	 * 
	 * @param ROUTE_INFO
	 */
	public void setROUTE_INFO(java.lang.String ROUTE_INFO) {
		this.ROUTE_INFO = ROUTE_INFO;
	}

	/**
	 * Gets the ROUTING_DIGIT value for this
	 * CheckNumberPortabilityByPhoneNumberResponse.
	 * 
	 * @return ROUTING_DIGIT
	 */
	public java.lang.String getROUTING_DIGIT() {
		return ROUTING_DIGIT;
	}

	/**
	 * Sets the ROUTING_DIGIT value for this
	 * CheckNumberPortabilityByPhoneNumberResponse.
	 * 
	 * @param ROUTING_DIGIT
	 */
	public void setROUTING_DIGIT(java.lang.String ROUTING_DIGIT) {
		this.ROUTING_DIGIT = ROUTING_DIGIT;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof CheckNumberPortabilityByPhoneNumberResponse))
			return false;
		CheckNumberPortabilityByPhoneNumberResponse other = (CheckNumberPortabilityByPhoneNumberResponse) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.TRANSACTIONID == null && other.getTRANSACTIONID() == null)
						|| (this.TRANSACTIONID != null && this.TRANSACTIONID.equals(other.getTRANSACTIONID())))
				&& ((this.SEQUENCENO == null && other.getSEQUENCENO() == null)
						|| (this.SEQUENCENO != null && this.SEQUENCENO.equals(other.getSEQUENCENO())))
				&& ((this.ERRORDETAIL == null && other.getERRORDETAIL() == null)
						|| (this.ERRORDETAIL != null && this.ERRORDETAIL.equals(other.getERRORDETAIL())))
				&& ((this.returnCode == null && other.getReturnCode() == null)
						|| (this.returnCode != null && this.returnCode.equals(other.getReturnCode())))
				&& ((this.returnDesc == null && other.getReturnDesc() == null)
						|| (this.returnDesc != null && this.returnDesc.equals(other.getReturnDesc())))
				&& ((this.IMSI == null && other.getIMSI() == null)
						|| (this.IMSI != null && this.IMSI.equals(other.getIMSI())))
				&& ((this.ROUTE_INFO == null && other.getROUTE_INFO() == null)
						|| (this.ROUTE_INFO != null && this.ROUTE_INFO.equals(other.getROUTE_INFO())))
				&& ((this.ROUTING_DIGIT == null && other.getROUTING_DIGIT() == null)
						|| (this.ROUTING_DIGIT != null && this.ROUTING_DIGIT.equals(other.getROUTING_DIGIT())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getTRANSACTIONID() != null) {
			_hashCode += getTRANSACTIONID().hashCode();
		}
		if (getSEQUENCENO() != null) {
			_hashCode += getSEQUENCENO().hashCode();
		}
		if (getERRORDETAIL() != null) {
			_hashCode += getERRORDETAIL().hashCode();
		}
		if (getReturnCode() != null) {
			_hashCode += getReturnCode().hashCode();
		}
		if (getReturnDesc() != null) {
			_hashCode += getReturnDesc().hashCode();
		}
		if (getIMSI() != null) {
			_hashCode += getIMSI().hashCode();
		}
		if (getROUTE_INFO() != null) {
			_hashCode += getROUTE_INFO().hashCode();
		}
		if (getROUTING_DIGIT() != null) {
			_hashCode += getROUTING_DIGIT().hashCode();
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

}
