/**
 * Sofaultmessage.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */
package com.olleh.pHubCMS.api.model.oif2118;

@SuppressWarnings("serial")
public class Sofaultmessage extends org.apache.axis.AxisFault implements java.io.Serializable {
	private java.lang.String transactionid;
	private java.lang.String sequenceno;
	private com.olleh.pHubCMS.api.model.oif2118.Errordetail errordetail;
	private java.lang.String returnCode;
	private java.lang.String returnDesc;
	private com.olleh.pHubCMS.api.model.oif2118.Arbitrary[] arbitraries;

	public Sofaultmessage() {
	}

	public Sofaultmessage(java.lang.String transactionid, java.lang.String sequenceno,
			com.olleh.pHubCMS.api.model.oif2118.Errordetail errordetail, java.lang.String returnCode,
			java.lang.String returnDesc, com.olleh.pHubCMS.api.model.oif2118.Arbitrary[] arbitraries) {
		this.transactionid = transactionid;
		this.sequenceno = sequenceno;
		this.errordetail = errordetail;
		this.returnCode = returnCode;
		this.returnDesc = returnDesc;
		this.arbitraries = arbitraries;
	}

	/**
	 * Gets the transactionid value for this Sofaultmessage.
	 * 
	 * @return transactionid
	 */
	public java.lang.String getTransactionid() {
		return transactionid;
	}

	/**
	 * Sets the transactionid value for this Sofaultmessage.
	 * 
	 * @param transactionid
	 */
	public void setTransactionid(java.lang.String transactionid) {
		this.transactionid = transactionid;
	}

	/**
	 * Gets the sequenceno value for this Sofaultmessage.
	 * 
	 * @return sequenceno
	 */
	public java.lang.String getSequenceno() {
		return sequenceno;
	}

	/**
	 * Sets the sequenceno value for this Sofaultmessage.
	 * 
	 * @param sequenceno
	 */
	public void setSequenceno(java.lang.String sequenceno) {
		this.sequenceno = sequenceno;
	}

	/**
	 * Gets the errordetail value for this Sofaultmessage.
	 * 
	 * @return errordetail
	 */
	public com.olleh.pHubCMS.api.model.oif2118.Errordetail getErrordetail() {
		return errordetail;
	}

	/**
	 * Sets the errordetail value for this Sofaultmessage.
	 * 
	 * @param errordetail
	 */
	public void setErrordetail(com.olleh.pHubCMS.api.model.oif2118.Errordetail errordetail) {
		this.errordetail = errordetail;
	}

	/**
	 * Gets the returnCode value for this Sofaultmessage.
	 * 
	 * @return returnCode
	 */
	public java.lang.String getReturnCode() {
		return returnCode;
	}

	/**
	 * Sets the returnCode value for this Sofaultmessage.
	 * 
	 * @param returnCode
	 */
	public void setReturnCode(java.lang.String returnCode) {
		this.returnCode = returnCode;
	}

	/**
	 * Gets the returnDesc value for this Sofaultmessage.
	 * 
	 * @return returnDesc
	 */
	public java.lang.String getReturnDesc() {
		return returnDesc;
	}

	/**
	 * Sets the returnDesc value for this Sofaultmessage.
	 * 
	 * @param returnDesc
	 */
	public void setReturnDesc(java.lang.String returnDesc) {
		this.returnDesc = returnDesc;
	}

	/**
	 * Gets the arbitraries value for this Sofaultmessage.
	 * 
	 * @return arbitraries
	 */
	public com.olleh.pHubCMS.api.model.oif2118.Arbitrary[] getArbitraries() {
		return arbitraries;
	}

	/**
	 * Sets the arbitraries value for this Sofaultmessage.
	 * 
	 * @param arbitraries
	 */
	public void setArbitraries(com.olleh.pHubCMS.api.model.oif2118.Arbitrary[] arbitraries) {
		this.arbitraries = arbitraries;
	}

	public com.olleh.pHubCMS.api.model.oif2118.Arbitrary getArbitraries(int i) {
		return this.arbitraries[i];
	}

	public void setArbitraries(int i, com.olleh.pHubCMS.api.model.oif2118.Arbitrary _value) {
		this.arbitraries[i] = _value;
	}

	private java.lang.Object __equalsCalc = null;

	public synchronized boolean equals(java.lang.Object obj) {
		if (!(obj instanceof Sofaultmessage))
			return false;
		Sofaultmessage other = (Sofaultmessage) obj;
		if (obj == null)
			return false;
		if (this == obj)
			return true;
		if (__equalsCalc != null) {
			return (__equalsCalc == obj);
		}
		__equalsCalc = obj;
		boolean _equals;
		_equals = true
				&& ((this.transactionid == null && other.getTransactionid() == null)
						|| (this.transactionid != null && this.transactionid.equals(other.getTransactionid())))
				&& ((this.sequenceno == null && other.getSequenceno() == null)
						|| (this.sequenceno != null && this.sequenceno.equals(other.getSequenceno())))
				&& ((this.errordetail == null && other.getErrordetail() == null)
						|| (this.errordetail != null && this.errordetail.equals(other.getErrordetail())))
				&& ((this.returnCode == null && other.getReturnCode() == null)
						|| (this.returnCode != null && this.returnCode.equals(other.getReturnCode())))
				&& ((this.returnDesc == null && other.getReturnDesc() == null)
						|| (this.returnDesc != null && this.returnDesc.equals(other.getReturnDesc())))
				&& ((this.arbitraries == null && other.getArbitraries() == null) || (this.arbitraries != null
						&& java.util.Arrays.equals(this.arbitraries, other.getArbitraries())));
		__equalsCalc = null;
		return _equals;
	}

	private boolean __hashCodeCalc = false;

	public synchronized int hashCode() {
		if (__hashCodeCalc) {
			return 0;
		}
		__hashCodeCalc = true;
		int _hashCode = 1;
		if (getTransactionid() != null) {
			_hashCode += getTransactionid().hashCode();
		}
		if (getSequenceno() != null) {
			_hashCode += getSequenceno().hashCode();
		}
		if (getErrordetail() != null) {
			_hashCode += getErrordetail().hashCode();
		}
		if (getReturnCode() != null) {
			_hashCode += getReturnCode().hashCode();
		}
		if (getReturnDesc() != null) {
			_hashCode += getReturnDesc().hashCode();
		}
		if (getArbitraries() != null) {
			for (int i = 0; i < java.lang.reflect.Array.getLength(getArbitraries()); i++) {
				java.lang.Object obj = java.lang.reflect.Array.get(getArbitraries(), i);
				if (obj != null && !obj.getClass().isArray()) {
					_hashCode += obj.hashCode();
				}
			}
		}
		__hashCodeCalc = false;
		return _hashCode;
	}

	/**
	 * Writes the exception data to the faultDetails
	 */
	public void writeDetails(javax.xml.namespace.QName qname, org.apache.axis.encoding.SerializationContext context)
			throws java.io.IOException {
		context.serialize(qname, null, this);
	}
}
