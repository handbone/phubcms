package com.olleh.pHubCMS.api.service;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.common.components.Constant;
import com.olleh.pHubCMS.common.components.SysPrmtManage;
import com.olleh.pHubCMS.common.exception.BizException;
import com.olleh.pHubCMS.common.model.ResultVo;
import com.olleh.pHubCMS.common.scheduler.utils.CryptoUtil;
import com.olleh.pHubCMS.common.utils.JsonUtil;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 기프티쇼 외부 Http(s) 연동 클래스
 * 
 * @author CREDIF
 */
@Service("GiftiShowRestHttpService")
public class GiftiShowRestHttpService extends HttpService {
	private static final String SEND_PLC = "PH";
	private static final String RECV_PLC = "GS";
	
	private static final String GS_GOODS_URL = "/goods";
	
	private static final String HEADER_API_CODE = "api_code";						//요청할API번호
	private static final String HEADER_CUSTOM_AUTH_CODE = "custom_auth_code";		//매체코드
	private static final String HEADER_CUSTOM_AUTH_TOKEN = "custom_auth_token";		//암호화된매체코드값
	private static final String HEADER_CUSTOM_ENC_FLAG = "custom_enc_flag";			//파라미터/응답 정보의 암호화 여부
	
	private static final String RES_CODE = "resCode";	//결과코드
	private static final String RES_MSG = "resMsg";		//코드메시지
	private static final String RES_GOODS_LIST = "goodsList";
	
	/** 기프티쇼 성공 기본 코드 */
	public static final String GS_SUCS_CD = "0000";
	
	/** 포인트허브 성공 기본 코드 */
	public static final String PHUB_SUCS_CD = "00";
	
	/** 기프티쇼 개행문자 */
	public static final String CR_LF = "\r\n";
	
	
	@Autowired
    SysPrmtManage sysPrmtManage;
	
	@Autowired
    LogService logService;
	
	/**
	 * <pre>상품 정보 리스트 (0101) GET</pre>
	 * 
	 * @param params
	 * @return resultVo
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ResultVo sendHttpPostGiftiShowGoods() throws BizException, Exception {
		String methodName = getClass().getName() +"."+ "sendHttpPostGiftiShowGoods";
		log.debug(methodName, "Start!");
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(Constant.API_LOG_WRITE_YN, "N");
		
		String sendUrl = sysPrmtManage.getSysPrmtVal("GS_DOMAIN") + GS_GOODS_URL;
		ResultVo resultVo = new ResultVo();
		resultVo.setCaller(methodName);
		try {
			// 0. 파라미터 세팅
			resultVo.setReqBody(new HashMap());
			
			// 1. 헤더 정의
			HttpHeaders headers = getHeaders();
			headers.add(HEADER_API_CODE, "0101");
			
			// 2. 통신
			resultVo = sendHttpGet(sendUrl, headers, resultVo, params);
			
			// 3. 결과 세팅
			Map response = JsonUtil.JsonToMap(StringUtil.nvl(resultVo.getRstBody()));
			String resCode = StringUtil.nvl(response.get(RES_CODE));
			resultVo.setRstCd(resCode);
			resultVo.setRstMsg(StringUtil.nvl(response.get(RES_MSG)));
			if (!resCode.equals(GS_SUCS_CD)) {
				// 실패 시
				resultVo.setSucYn("N");
			} else {
				// 성공 시 상품 정보 세팅
				List<Map> list = (List<Map>)response.get(RES_GOODS_LIST);
				resultVo.setList(list);
			}
			log.debug(methodName, "sucYn: "+ resultVo.getSucYn());
			log.debug(methodName, "rstCd: "+ resultVo.getRstCd());
			log.debug(methodName, "rstMsg: "+ resultVo.getRstMsg());
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
		} finally {
			// log write
			//insertAdmApiIfLog(sendUrl, "", methodName, resultVo);
		}
		
		return resultVo;
	}
	
	
	/**
	 * <pre>기프티쇼 전문 송/수신 Log기록</pre>
	 * 
	 * @param sendUrl
	 * @param phubTrNo
	 * @param rgstUserId
	 * @param resultVo
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void insertAdmApiIfLog(String sendUrl, String phubTrNo, String rgstUserId, ResultVo resultVo) {
		String methodName = getClass().getName() +"."+ "insertAdmApiIfLog";
		
		// declare
	    Map<String, Object> logMap = new HashMap<String, Object>();
	    
	    try {
	    	Map tmpReq = (Map)resultVo.getReqBody();
	    	if (tmpReq == null) {
	    		tmpReq = new HashMap();
	    	}
	    	// 연동이 성공일 경우 포인트허브 성공 공통코드로 세팅
	    	if (resultVo.getSucYn().equals("Y")) {
	    		resultVo.setRstCd(PHUB_SUCS_CD);
	    	}
		    logMap.put("actn_uri",     sendUrl);
		    logMap.put("send_plc",     SEND_PLC);
		    logMap.put("rcv_plc",      RECV_PLC);
		    logMap.put("phub_tr_no",   phubTrNo);
		    logMap.put("req_data",     JsonUtil.MapToJson(tmpReq));
		    logMap.put("res_data",     StringUtil.nvl(resultVo.getRstBody()).toString());
		    logMap.put("rply_cd",      resultVo.getRstCd());
		    logMap.put("rply_msg",     resultVo.getRstMsg());
		    logMap.put("rgst_user_id", "GiftiShowRest");
		    
		    // insert log
		    logService.insertAdmApiIfLog(logMap);
	    } catch(Exception e) {
	    	log.error(methodName, JsonUtil.MapToJson(logMap));
	    	log.printStackTracePH(methodName, e);
	    }
	}
	
	/**
	 * 공통 헤더 정의
	 * 
	 * @return
	 */
	private HttpHeaders getHeaders() {
		String mdCode = sysPrmtManage.getSysPrmtVal("GS_MDCODE");
		HttpHeaders headers = new HttpHeaders();
		headers.add(HEADER_CUSTOM_AUTH_CODE, mdCode);
		headers.add(HEADER_CUSTOM_AUTH_TOKEN, encrytoAES256(mdCode));
		headers.add(HEADER_CUSTOM_ENC_FLAG, "N");
		headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_UTF8_VALUE);
		return headers;
	}
	
	/**
	 * <pre>AES256 암호화</pre>
	 * 
	 * @param str
	 */
	private String encrytoAES256(String str) {
		String methodName = getClass().getName() +"."+ "encrytoAES256";
		String ret = "";
		try {
			String gsKey = sysPrmtManage.getSysPrmtVal("GS_AES_KEY");
			ret = CryptoUtil.encAES(StringUtil.nvl(str), gsKey);
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | UnsupportedEncodingException | IllegalBlockSizeException | BadPaddingException e) {
			log.debug(methodName, "exception msg=" + StringUtil.nvl(e.getMessage()));
		}
		return ret;
	}

	/**
	 * <pre>AES256 복호화</pre>
	 * 
	 * @param str
	 * @return
	 */
	@SuppressWarnings("unused")
	private String descryptoAES256(String str) {
		String methodName = getClass().getName() +"."+ "descryptoAES256";
		String ret = "";
		try {
			String gsKey = sysPrmtManage.getSysPrmtVal("GS_AES_KEY");
			ret = CryptoUtil.decAES(StringUtil.nvl(str), gsKey);
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | UnsupportedEncodingException | IllegalBlockSizeException | BadPaddingException e) {
			log.debug(methodName, "exception msg=" + StringUtil.nvl(e.getMessage()));
		}
		return ret;
	}
}
