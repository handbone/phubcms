package com.olleh.pHubCMS.api.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.StringUtils;
import org.oasis_open.docs.wss._2004._01.oasis_200401_wss_wssecurity_secext_1_0_xsd.Security;
import org.oasis_open.docs.wss._2004._01.oasis_200401_wss_wssecurity_secext_1_0_xsd.UsernameToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.api.model.oif2118.MessageSendSMSReportNoNetCharge;
import com.olleh.pHubCMS.api.model.oif2118.MessageSendSMSReportNoNetChargeResponse;
import com.olleh.pHubCMS.api.model.oif2118.WSDL_MessageSendSMSReportNoNetChargeSOAPStub;
import com.olleh.pHubCMS.api.model.oif2118.WSDL_MessageSendSMSReportNoNetCharge_ServiceLocator;
import com.olleh.pHubCMS.api.model.oif552.CheckNumberPortabilityByPhoneNumberRequest;
import com.olleh.pHubCMS.api.model.oif552.CheckNumberPortabilityByPhoneNumberResponse;
import com.olleh.pHubCMS.api.model.oif552.NonBssCapriManagerServiceLocator;
import com.olleh.pHubCMS.api.model.oif552.NonBssCapriManagerSoapBindingStub;
import com.olleh.pHubCMS.common.components.SysPrmtManage;
import com.olleh.pHubCMS.common.exception.ResponseException;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.ResultVo;

/**
 * SHUB 통신
 */
@Service("ShubService")
public class ShubService {
	private Logger log = new Logger(this.getClass());
	
	private static final int SOAP_TIMEOUT = 10000;
	private static final String OIF_552_URL = "/nonBssCapri/NonBssCapriManager";
	private static final String OIF_2118_URL = "/MESG";
	
	public static final String OIF_PARAM_ROUTING_DIGIT = "ROUTING_DIGIT";
	public static final String OIF_PARAM_ROUTE_INFO = "ROUTE_INFO";
	
	@Autowired
	SysPrmtManage sysPrmtManage;
	
	/**
	 * Setting Header OIF_552
	 * 
	 * @param checkCom
	 * @return
	 * @throws ResponseException
	 */
	private NonBssCapriManagerSoapBindingStub shub552SetHeader(NonBssCapriManagerSoapBindingStub checkCom) throws ResponseException {
		try {
			java.net.URL url = new java.net.URL(sysPrmtManage.getSysPrmtVal("SHUB552_DOMAIN") + OIF_552_URL);
			javax.xml.rpc.Service svervice = new NonBssCapriManagerServiceLocator();
			checkCom = new NonBssCapriManagerSoapBindingStub(url, svervice);
			
			UsernameToken shubtoken = new UsernameToken();
			shubtoken.setUsername(sysPrmtManage.getSysPrmtVal("SHUB_USERNAME"));
			shubtoken.setPassword(sysPrmtManage.getSysPrmtVal("SHUB_PASSWORD"));
			
			Security header = new Security(shubtoken);
			checkCom.setHeader(new NonBssCapriManagerServiceLocator().getServiceName().getNamespaceURI(), "Security", header);
			
		} catch (Exception e) {
			throw new ResponseException(2, e.getMessage());
		}
		
		return checkCom;
	}
	
	/**
	 * Setting Header OIF_2118
	 * 
	 * @param sms2118
	 * @return
	 * @throws ResponseException
	 */
	private WSDL_MessageSendSMSReportNoNetChargeSOAPStub shub2118SetHeader(WSDL_MessageSendSMSReportNoNetChargeSOAPStub sms2118) throws ResponseException {
		try {
			java.net.URL url = new java.net.URL(sysPrmtManage.getSysPrmtVal("SHUB2118_DOMAIN") + OIF_2118_URL);
			javax.xml.rpc.Service svervice = new WSDL_MessageSendSMSReportNoNetCharge_ServiceLocator();
			sms2118 = new WSDL_MessageSendSMSReportNoNetChargeSOAPStub(url, svervice);
			
			UsernameToken shubtoken = new UsernameToken();
			shubtoken.setUsername(sysPrmtManage.getSysPrmtVal("SHUB_USERNAME"));
			shubtoken.setPassword(sysPrmtManage.getSysPrmtVal("SHUB_PASSWORD"));
			
			Security header = new Security(shubtoken);
			sms2118.setHeader(new WSDL_MessageSendSMSReportNoNetCharge_ServiceLocator().getServiceName().getNamespaceURI(), "Security", header);
		} catch (Exception e) {
			throw new ResponseException(2, e.getMessage());
		}
		return sms2118;
	}
	
	/**
	 * SHUB OIF_552 통신 [번호이동성조회]
	 * 
	 * @param phoneNo
	 * @return
	 */
	public ResultVo shub552(String phoneNo) throws ResponseException, Exception {
		String method = "shub552";
		ResultVo resultVo = new ResultVo();
		resultVo.setCaller(method);
		resultVo.setSucYn("N");
		CheckNumberPortabilityByPhoneNumberResponse shub552Response = null;
		try {
			NonBssCapriManagerSoapBindingStub stub = shub552SetHeader(new NonBssCapriManagerSoapBindingStub());
			CheckNumberPortabilityByPhoneNumberRequest shub552Request = new CheckNumberPortabilityByPhoneNumberRequest();
			shub552Request.setCALL_CTN(phoneNo);
			shub552Response = stub.checkNumberPortabilityByPhoneNumber(shub552Request);
			
			String returnCode = shub552Response.getReturnCode(); // OIF_552 결과코드 1 성공, 0 실패
			if (StringUtils.equals(returnCode, "1")) {
				resultVo.setSucYn("Y");
				resultVo.setRstCd(returnCode);
				resultVo.setRstMsg(shub552Response.getReturnDesc());
				
				Map<String, Object> map = new HashMap<String, Object>();
				map.put(OIF_PARAM_ROUTING_DIGIT, shub552Response.getROUTING_DIGIT());
				map.put(OIF_PARAM_ROUTE_INFO, shub552Response.getROUTE_INFO());
				resultVo.setMap(map);
			} else {
				resultVo.setRstCd(shub552Response.getERRORDETAIL().getErrorcode());
				resultVo.setRstMsg(shub552Response.getERRORDETAIL().getErrordescription());
			}
			log.debug(method, "transactionId: "+ shub552Response.getTRANSACTIONID() +  ", rstCode: "+ resultVo.getRstCd() + ", rstMsg: "+ resultVo.getRstMsg());
		} catch (ResponseException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}
		return resultVo;
	}
	
	/**
	 * SHUB OIF_2118 통신 [SMS발송]
	 * 
	 * @param phoneNo
	 * @param msgContent
	 * @return
	 * @throws ResponseException
	 * @throws Exception
	 */
	public ResultVo shub2118(String phoneNo, String msgContent) throws ResponseException, Exception {
		String method = "shub2118";
		ResultVo resultVo = new ResultVo();
		resultVo.setCaller(method);
		resultVo.setSucYn("N");
		MessageSendSMSReportNoNetChargeResponse shub2118Response = null;
		try {
			WSDL_MessageSendSMSReportNoNetChargeSOAPStub stub = shub2118SetHeader(new WSDL_MessageSendSMSReportNoNetChargeSOAPStub());
			MessageSendSMSReportNoNetCharge shub2118Request = new MessageSendSMSReportNoNetCharge();
			shub2118Request.setMSG_TYPE("S001");
			shub2118Request.setPFORM_TYPE("1");
			shub2118Request.setSERVICE_TYPE("F");
			shub2118Request.setCALLBACK_CTN(sysPrmtManage.getSysPrmtVal("SHUB_CALLBACK_CTN"));
			shub2118Request.setCALL_CTN(sysPrmtManage.getSysPrmtVal("SHUB_CALL_CTN"));
			shub2118Request.setRCV_CTN(phoneNo);
			shub2118Request.setMSG_CONTENT(msgContent);
			shub2118Response = stub.BS_MessageSendSMSReportNoNetCharge(shub2118Request);
			
			String rt = shub2118Response.getRT();
			if (StringUtils.equals(rt, "1")) {
				resultVo.setSucYn("Y");
				resultVo.setRstCd(rt);
				resultVo.setRstMsg(shub2118Response.getRT_MSG());
			} else {
				resultVo.setRstCd(shub2118Response.getERROR_CODE());
				resultVo.setRstMsg(shub2118Response.getERROR_DESCRIPTION());
			}
			log.debug(method, "transactionId: "+ shub2118Response.getTRANSACTIONID() +  ", rt: "+ resultVo.getRstCd() + ", rt_msg: "+ resultVo.getRstMsg());
		} catch (ResponseException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}
		return resultVo;
	}
}