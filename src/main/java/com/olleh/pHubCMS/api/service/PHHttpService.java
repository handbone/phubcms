/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pHubCMS.api.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.common.components.Constant;
import com.olleh.pHubCMS.common.components.SysPrmtManage;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.ResultVo;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 포인트허브 http통신 서비스 (내부 테스트용)
 * @Class Name : PHHttpService
 * @author lys
 * @since 2018.07.30
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.07.30   lys        최초생성
 * </pre>
 */
@Service("PHHttpService")
public class PHHttpService extends HttpService {
	private final Logger log = new Logger(this.getClass());
	
    @Autowired
    SysPrmtManage sysPrmtManage;
    
    @Autowired
    ShubService shubService;

	/**
	 * <pre> 포인트허브 http post방식 통신 </pre>
	 * 
	 * @param Map<String,Object> params
	 * @param HttpServletRequest request
	 * @return ModelAndView
	 * @see <pre>
	 *      view를 리턴하는 예제
	 *      </pre>
	 */
	public ResultVo sendHttpPostPH(String uri, Map<String, Object> params) {
		String methodName = "sendHttpPost";
		log.debug(methodName, "Start!");
		
		// set url
		String sendUrl = getDomain(uri);
		
		// set header
	    Map<String, Object> hmap = new HashMap<String, Object>();
	    hmap.put("authorization", getAuthorization());
	    hmap.put("appType", "json");
	    
	    // send
	    ResultVo resultVo = null;
		try {
			resultVo = sendHttpPost(sendUrl, hmap, params);
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
		}
	    resultVo.setCaller("SbankHttpService.sendHttpPostSB");
			
		// result
		return resultVo;
	}
	
	/**
	 * <pre> 포인트허브 http post방식 통신 </pre>
	 * 
	 * @param url
	 * @param params
	 * @return
	 */
	public ResultVo sendHttpPostCustom(String url, HttpHeaders headers, ResultVo resultVo) {
		String methodName = "sendHttpPostCustom";
		String sendUrl = getDomain(url);
		try {
			log.debug(methodName, "Start!");
			resultVo = sendHttpPost(sendUrl, headers, resultVo);
		} finally {
			log.debug(methodName, "End!");
		}
		return resultVo;
	}
	
	/**
	 * OTP 발송
	 * 
	 * @param phoneNo
	 * @return
	 */
	public String sendOTP(String phoneNo, String otpNo) {
		String method = "sendOTP";
		String ret = Constant.FAIL_CODE;
		ResultVo resultVo = null;
		Map<String, Object> resultMap = null;
		try {
			phoneNo = phoneNo.replaceAll("-", "");
			
			// 1. OIF_552 통신
			resultVo = shubService.shub552(phoneNo);
			if (!StringUtils.equals(resultVo.getSucYn(), "Y")) {
				throw new Exception("SHUB 번호 이동성 조회 연동에 실패하였습니다");
			}
			resultMap = resultVo.getMap();
			// ROUTE_INFO = 6 => KT
			String routeInfo = StringUtil.nvl(resultMap.get(ShubService.OIF_PARAM_ROUTE_INFO));
			log.debug(method, "ROUTE_INFO:"+ routeInfo);
			if (!StringUtils.equals(routeInfo, "6")) {
				return "KT 가입자 명의 휴대폰에서만 OTP 문자가 발송됩니다";
			}
			
			// 메시지 정의
			String msgContent = sysPrmtManage.getSysPrmtVal("OTP_MESSAGE");
			msgContent = msgContent.replaceAll("[$]\\{otpNo\\}", otpNo);
			log.debug(method, msgContent);
			resultVo = shubService.shub2118(phoneNo, msgContent);
			if (!StringUtils.equals(resultVo.getSucYn(), "Y")) {
				throw new Exception("SHUB 문자 발송 연동에 실패하였습니다");
			}
			
			ret = Constant.SUCCESS_CODE;
		} catch (Exception e) {
			log.error(method, e.getClass().toString());
			log.printStackTracePH(method, e);
		}
		
		return ret;
	}
	
	/**
	 * <pre> 포인트허브 http post방식 통신 </pre>
	 * 
	 * @param Map<String,Object> params
	 * @param HttpServletRequest request
	 * @return ModelAndView
	 * @see <pre>
	 *      view를 리턴하는 예제
	 *      </pre>
	 */	
	private String getDomain(String uri){
		String domain = sysPrmtManage.getSysPrmtVal("PH_CONN_IP") + uri;
		return StringUtil.nvl(domain);
	}
	
	/**
	 * <pre> 포인트허브 http post방식 통신 </pre>
	 * 
	 * @param Map<String,Object> params
	 * @param HttpServletRequest request
	 * @return ModelAndView
	 * @see <pre>
	 *      view를 리턴하는 예제
	 *      </pre>
	 */	
	private String getAuthorization(){
		String authorization = "";
		return StringUtil.nvl(authorization);
	}
}
