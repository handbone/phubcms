/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pHubCMS.api.service;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.olleh.pHubCMS.api.dao.LogDAO;

/**
 * API 공통 Log 처리 Service
 * @Class Name : LogService
 * @author : lys
 * @since 2018.08.06
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자           수정내용
 * ----------  --------  -----------------
 * 2018.08.06  lys       최초생성
 * </pre>
 */
@Service
public class LogService {
	
	@Resource(name="logDAO")
	private LogDAO logDAO;	


    /**
     * @description I/F 로그를 기록 한다.
     * @param  Map
     * @return int
     * @see
     *  1. 동립적인 트랜젝션을 관리 한다. (트랜젝션 새로 생성)
     *  2. 호출자 메소드에서 로직상 실패가 뜨더라고 log 테이블은 무조건 기록 된다.
     */
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public int insertAdmApiIfLog(Map<String, Object> params){
		//메소드명, 로그내용
		String methodName = "insertAdmApiIfLog";
		//log.debug(methodName, "Start!");
		return logDAO.insertAdmApiIfLog(params);
	}
	
    /**
     * @description I/F 로그를 갱신 한다.
     * @param  Map
     * @return int
     * @see
     *  1. 동립적인 트랜젝션을 관리 한다. (트랜젝션 새로 생성)
     *  2. 호출자 메소드에서 로직상 실패가 뜨더라고 log 테이블은 무조건 기록 된다.
     */
	@Transactional(propagation=Propagation.REQUIRES_NEW)
	public int updateAdmApiIfLog(Map<String, Object> params){
		//메소드명, 로그내용
		String methodName = "updateAdmApiIfLog";
		//log.debug(methodName, "Start!");
		
		// 추후 파라미터에 value에 Object가 넘어오면 적용해야 한다.
		Map sendParam = new HashMap<>(params);
		
		// return
		return logDAO.updateAdmApiIfLog(sendParam);
	}	
}
