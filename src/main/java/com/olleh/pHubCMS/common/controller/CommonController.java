/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pHubCMS.common.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.olleh.pHubCMS.common.components.CodeManage;
import com.olleh.pHubCMS.common.components.Constant;
import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.components.SysPrmtManage;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.AdminLoginVO;
import com.olleh.pHubCMS.common.scheduler.job.ClipToPHubJob;
import com.olleh.pHubCMS.common.scheduler.job.PHubStatJob;
import com.olleh.pHubCMS.common.spring.annotation.LoginUncheck;
import com.olleh.pHubCMS.common.spring.annotation.RefererCheck;
import com.olleh.pHubCMS.common.utils.JsonUtil;
import com.olleh.pHubCMS.common.utils.SessionUtils;
import com.olleh.pHubCMS.common.utils.StringUtil;


/**
 * 공통기능 처리 컨트롤러
 * @Class Name : CommonController
 * @author lys
 * @since 2018.08.17
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.17   lys        최초생성
 * </pre>
 */
@Controller
public class CommonController {
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	CodeManage codeManage;
	
	@Autowired
	SysPrmtManage sysPrmtManage;
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
	ClipToPHubJob clipToPHubJob;
	
	@Autowired
	PHubStatJob pHubStatJob;	
	

	
	/**
	 * <pre>[ajax]공통코드를 조회한후 json object 타입으로 반환한다.</pre>
	 * 
	 * @param Map<String,Object> params
	 * @param HttpServletRequest request
	 * @return Map<String, Object>
	 * @see <pre>
	 * </pre>
	 */	
	@RequestMapping(value="/common/getCode.hnc", method=RequestMethod.POST)
	@ResponseBody
	@LoginUncheck	
	public Map<String, Object> getCode(@RequestParam Map<String,Object> params, HttpServletRequest request) throws Exception {
		Map<String, Object> jsonObject = new HashMap<String, Object>();
		jsonObject.put("eCode", "0");
		
		// List<CmnCdVO> list = codeManage.getCodeList(commCd);
		final String type = (String)params.get("type");
		if( !StringUtils.isEmpty(type) ) {
			jsonObject.put("result", codeManage.getCodeList(type));
		}
		
		return jsonObject;
	}

	
	/**
	 * 404, 405, 500 에러 화면으로 이동한다.
	 * @param params
	 * @param request
	 * @return
	 * @throws Exception
	 */
	
	/**
	 * <pre>404, 405, 500 에러 화면</pre>
	 * 
	 * @param Map<String,Object> params
	 * @param HttpServletRequest request
	 * @return ModelAndView
	 * @see <pre>
	 * </pre>
	 */	
	@RequestMapping(value={"/error.do", "/error400.do", "/error401.do", "/error403.do", "/error404.do", "/error405.do", "/error500.do"})
	@RefererCheck
	@LoginUncheck
	public ModelAndView error(@RequestParam Map<String,Object> params, HttpServletRequest request) throws Exception {
		log.debug("error", "Start!");
		log.debug("error", "requestUri=" +StringUtil.nvl(request.getRequestURI()));
		log.debug("error", "refererUri=" +StringUtil.nvl(request.getHeader("referer")));
		
		// TODO
		// 에러 발생시 사용자의 welcom 페이지로 이동 시킨다. (세션이 없으면 에러 페이지로 로그인 페이지로 이동) : 에러페이지 완성되면 에러페이지로 이동 - 2018.10.16. lys 
		AdminLoginVO adminLoginVO = SessionUtils.getAdminLoginVO(request);
		//String reDirectUri = StringUtil.nvl(adminLoginVO.getWelcomUri(), "/login/login.do");
		
		ModelAndView mv = new ModelAndView("common/error");	
		//ModelAndView mv = new ModelAndView(new RedirectView(reDirectUri));
		final String requestUri = StringUtil.nvl(request.getRequestURI());
		if( requestUri.indexOf("404") != -1 ) {
			mv.addObject("eCode", "404");
		} else if( requestUri.indexOf("405") != -1 ) {
			mv.addObject("eCode", "405");
		} else if( requestUri.indexOf("500") != -1 ) {
			mv.addObject("eCode", "500");
		} else if ( requestUri.indexOf("/error.do") != -1 ) {
			mv.addObject("eCode", StringUtil.nvl(params.get("eCode")));
		}
		
		return mv;
	}
	
	@RequestMapping(value={"/errorCustom.do"})
	@RefererCheck
	@LoginUncheck
	public ModelAndView errorCustom(@RequestParam Map<String,Object> params, HttpServletRequest request) throws Exception {
		log.debug("errorCustom", "Start!");
		
		// declare
		String errCd = "";
		String eMsg  = "";
		
		// get
		Map<String, ?> flashMap = RequestContextUtils.getInputFlashMap(request);
		log.debug("errorCustom", "flashMap=" +StringUtil.nvl(JsonUtil.toJson(flashMap)));
		
		if( flashMap != null ) {
			errCd = StringUtil.nvl(flashMap.get("eCode"), "F999");
			eMsg  = StringUtil.nvl(flashMap.get("eMsg"),  "정의되지 않은 에러가 발생하였습니다.");					
		} else {
			errCd = StringUtil.nvl(request.getAttribute("eCode"), "F999");
			eMsg  = StringUtil.nvl(request.getAttribute("eMsg"),  "정의되지 않은 에러가 발생하였습니다.");
		}
		
		// set
		ModelAndView mv = new ModelAndView("common/errorCustom");
		mv.addObject("eCode", errCd);
		mv.addObject("eMsg",  eMsg);
		
		return mv;
	}	
	
	/**
	 * <pre> 클립포인트 대사 재처리 </pre>
	 * 
	 * @param Map<String,Object> params
	 * @param HttpServletRequest request
	 * @return String
	 * @throws Exception
	 * @see <pre>
	 *      1. 대사일자별로 클립포인트대사를 재처리 한다. 
	 *      </pre>
	 */
	@RequestMapping(value = "/comm/btch/reProcCmprClipPoint.do", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> reProcCmprClipPoint(@RequestParam Map<String, Object> params, HttpServletRequest request) throws Exception {
		//메소드명, 로그내용
		String methodName = "reProcCmprClipPoint";
		log.debug(methodName, "Start!");
		
		// Declare
		Map<String, Object> resMap = new HashMap<String, Object>();
		
		// get 로그인 정보
		AdminLoginVO adminLoginVO = SessionUtils.getAdminLoginVO(request);		
		String userId = StringUtil.nvl(adminLoginVO.getUserId());
		
		if( "".equals(userId) ) {
			resMap.put(Constant.SUCCESS_YN, Constant.FAIL);
			resMap.put(Constant.RET_CODE,   "E999");
			resMap.put(Constant.RET_MSG,    "사용자정보가 존재하지 않습니다.");
		} else {
			//클립포인트 대사 실행
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("cmprDd", params.get("cmprDd"));
			param.put("userId", userId);
			
			// 재처리
			clipToPHubJob.cmprProc(param);
			
			// 결과셋팅 : 성공
			resMap.put(Constant.SUCCESS_YN, Constant.SUCCESS);
			resMap.put(Constant.RET_CODE,   Constant.SUCCESS_CODE);
			resMap.put(Constant.RET_MSG,    Constant.SUCCESS_MSG);			
		}
		
		// return
		log.debug(methodName, "End!");
		return resMap;
	}	
}
