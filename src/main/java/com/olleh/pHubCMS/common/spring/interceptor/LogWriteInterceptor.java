/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pHubCMS.common.spring.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.olleh.pHubCMS.admin.connHist.service.SysUseLogServiceImpl;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.utils.StringUtil;
import com.olleh.pHubCMS.common.utils.SystemUtils;


/**
 * 사용자의 행위를 로그로 기록하는 인터셉터 정의
 * @Class Name : LogWriteInterceptor Controller
 * @author lys
 * @since 2018.09.10
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.10   lys        최초생성
 * </pre>
 */
public class LogWriteInterceptor extends HandlerInterceptorAdapter {
	private Logger log = new Logger(this.getClass());

	
	@Autowired
	SysUseLogServiceImpl sysUseLogServiceImpl;
	
	
	/**
	 * <pre> 유저Log Write</pre>
	 * 
	 * @param Map<String,Object> params
	 * @param HttpServletRequest request
	 * @return ModelAndView
	 * @see <pre>
	 *      view를 리턴하는 예제
	 *      </pre>
	 */	
	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		
		// set log
		String methodName = "afterCompletion";
		log.debug(methodName, "--- LogWriteInterceptor 시작 ---");
		
		try {	
			// declare
			boolean isException = true;
			boolean isAjax      = SystemUtils.isAjaxRequest(request);
			
			// Exception 여부 체크
			if( ex == null ) {
				isException = false;
			} else {
				log.debug(methodName, "is Exception true="+StringUtil.nvl(ex.getMessage()));
			}
			
			// ajax인 경우
//			if( isAjax ) {
//				//
//				String eCode = (String)request.getAttribute(Constant.JSON_OBJECT);
//				
//				// eCode가 0이 아니라면 무조건 에러 이다.
//				if( !(Constant.SUCCESS_CODE).equals(StringUtil.nvl((eCode))) ) {
//					isException = true;
//				}
//			};
			
			// 세션소멸여부 체크
			//log.debug(methodName, "request.getSession(false)="+StringUtil.nvl(request.getSession(false)));
			if( (request.getSession(false) != null) && (!"/login/login.do".equals(StringUtil.nvl(request.getRequestURI()))) ) {
				// 어드민 사용 로그 기록
				sysUseLogServiceImpl.userLogWrite(request, isException);	
			}
			log.debug(methodName, "--- LogWriteInterceptor 종료 ---");
		} catch(Exception e) {
			// log
			log.printStackTracePH(methodName, e);
			log.debug(methodName, "[Exception] e="+StringUtil.nvl(e.getMessage()));
			
		} finally {
			// call super
			super.afterCompletion(request, response, handler, ex);			
		}
	}
}
