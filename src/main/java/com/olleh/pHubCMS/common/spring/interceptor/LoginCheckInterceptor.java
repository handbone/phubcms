/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 * Revision History
 * Author            Date              Description
 * ------            -----             ------------
 * lys               2018.06.28        사용자의 로그인 여부 처리를 위한 인터셉터 정의
 */
package com.olleh.pHubCMS.common.spring.interceptor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.olleh.pHubCMS.admin.login.service.LoginService;
import com.olleh.pHubCMS.common.components.MenuActionInfo;
import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.exception.UserException;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.ErrorInfoVO;
import com.olleh.pHubCMS.common.model.MenuActionInfoVO;
import com.olleh.pHubCMS.common.spring.annotation.LoginUncheck;
import com.olleh.pHubCMS.common.spring.annotation.RefererCheck;
import com.olleh.pHubCMS.common.utils.SessionUtils;
import com.olleh.pHubCMS.common.utils.StringUtil;
import com.olleh.pHubCMS.common.utils.SystemUtils;


/**
 * <pre>사용자의 로그인 여부 처리를 위한 인터셉터 정의</pre>
 * 
 * @Class Name : LoginCheckInterceptor.java
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
public class LoginCheckInterceptor extends HandlerInterceptorAdapter {
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	LoginService loginService;
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
	MenuActionInfo menuActionInfo;
	
	/**
	 * 공통 오류 처리를 위한 코드값
	 */
	int LOIN_CHECK_FAILD  = 979;
	int SESSION_TIMED_OUT = 901;
	int NO_AUTHORITY      = 906;
	/**
	 * 공통 오류 페이지 
	 */
	String redirectPath = "/errorCustom.do";
	
	/**
	 * 로그인 페이지 url 
	 */
//	String loginPath = "/login/login.do";
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		//메소드명, 로그내용
		String methodName = "preHandle";
		log.debug(methodName, "Start!");
		
		// declare
		String stepCd = "001";
		int errCode = LOIN_CHECK_FAILD;
		String errCd = "";
		String eMsg  = "";
		Map<String, Object> tmpMap = new HashMap<String, Object>();			// temp 맵
		String contextPath = request.getContextPath();
		
		//호출 되는 controller 의 method 에 정의된 @LoginCheck annotation 체크.
		if( handler instanceof HandlerMethod ) { 
			//log.debug("--- LoginCheckInterceptor 수행 ---");
			HandlerMethod handlerMethod = (HandlerMethod)handler;
			LoginUncheck loginUncheck = handlerMethod.getMethodAnnotation(LoginUncheck.class);
			RefererCheck lefererCheck = handlerMethod.getMethodAnnotation(RefererCheck.class);
			
			//****************************************************************
			//* 1. 접속정보 추출
			//***************************************************************//*
			String reqUri = request.getRequestURI().substring(contextPath.length());
			String referer = StringUtil.nvl(request.getHeader("Referer"));
			
			try {

				//mnuCd = HNTTrans.trim(request.getParameter("cur_mnu_id"));
				//lastActDt = cookieUtil.getCookie(request, "lastActDt");
				//1. header & parameter 
		        log.debug(methodName, "Header Check. (getRemoteAddr=" + StringUtil.nvl(request.getRemoteAddr()) + ")");
				
				log.debug(methodName, "Referer Check Start! (Referer=" + StringUtil.nvl(request.getHeader("Referer")) + ")");
				log.debug(methodName, "Referer Check success! (URI=" + request.getRequestURI() + ")");
				//log.debug(methodName, "Referer Check success! (Parameter=" + request.getParameterMap() + ")");
				
				// 레퍼럴체크 (레퍼럴 어노테이션이 없고, ajax 통신이 아닌경우만 체크 한다.)
				if( lefererCheck == null && !SystemUtils.isAjaxRequest(request)) {
					if( "".equals(StringUtil.nvl(referer)) ) {
						// 레퍼럴체크 실패 :세션소멸 후 로그인 페이지로 이동
						stepCd  = "002";
						errCode = LOIN_CHECK_FAILD;
						log.debug(methodName, "Referer Check Fail! (Referer=" + StringUtil.nvl(request.getHeader("Referer")) + ")");
						throw new UserException(stepCd, messageManage.getMsgVOY("AD_INFO_159"));
					}
				}
				
				// 로그인여부체크 (loginUncheck 어노테이션 걸려있는 controller는 제외)
				if( loginUncheck != null && loginUncheck.required() == true ) {
					// 로그인 체크 필요없는 메소드들 처리
					stepCd  = "003";
				} else {
					stepCd = "004";
					if( SessionUtils.isLogin(request) ) {
						//AdminLoginVO adminLoginVO = SessionUtils.getAdminLoginVO(request);
						
						// 세션타임아웃체크 (eCode 값을 체크해서 리다이렉션 순환 오류가 발생하지 않도록 한다.)
						if( request.getRequestedSessionId() != null 
								&& !request.isRequestedSessionIdValid() 
								&& StringUtils.isEmpty(request.getParameter("eCode")) ) {

							//
							errCode = SESSION_TIMED_OUT;
							log.debug(methodName, "Session TimeOut Check Fail!");
							throw new UserException(stepCd, messageManage.getMsgVOY("SY_INFO_901"));
						}
						
						// 메뉴체크 (메뉴조회)
					} else {
						// 로그인되어 있지 않습니다 : 세션소멸 후 로그인 페이지로 이동
						stepCd = "005";
						errCode = LOIN_CHECK_FAILD;
						log.debug(methodName, "Not Login!");
						throw new UserException(stepCd, messageManage.getMsgVOY("SY_INFO_901"));						
					}
					
					// URI 권한체크
					boolean isAuth = false;
					stepCd = "004";
					MenuActionInfoVO menuActionInfoVO = menuActionInfo.getMenuActionVOYByUri(reqUri);	// uri로 메뉴코드 조회
					List<Map> userMenuList = SessionUtils.getUserMenuList(request);						// 사용자 메뉴목록 조회
					
					// 로그아웃은 권한체크를 하지 않는다.
					if( !reqUri.equals("/login/logOut.do") ) {
						
						// 사용자 메뉴목록에서 uri 메뉴코드로 match 확인 
						for(Map mnu : userMenuList) {
							if( StringUtil.nvl(menuActionInfoVO.getMenuId()).equals(mnu.get("menu_id")) ) {
								isAuth = true;
								SessionUtils.setUserMenu(request, mnu);		
								break;
							}
						}

						if( !isAuth ) {
							errCode = NO_AUTHORITY;
							log.debug(methodName, "No authority!");
							throw new UserException(stepCd, messageManage.getMsgVOY("AD_INFO_906"));						
						}
					}
				}
				
				// 성공
				errCode = 0;
				
			} catch(UserException e) {
				// 에러 로그인 페이지로 이동
				ErrorInfoVO errorInfoVO = e.getErrorInfo();
				errCd   = errorInfoVO.getErrCd();
				eMsg    = errorInfoVO.getErrMsg();
				
			} catch(Exception e) {
				log.printStackTracePH(methodName, e);
				errCode = -1;
				errCd   = messageManage.getMsgCd("SY_ERROR_900");
				eMsg    = messageManage.getMsgTxt("SY_ERROR_900");
				
			} finally {
				log.debug(methodName, "--- LoginCheckInterceptor End (errCode="+errCode+") ---");
			}
			
			// 에러발생시
			if( errCode != 0 ) {
				// 레퍼럴 페이지로 이동
				//if( (errCode == NO_AUTHORITY) && (referer != "") ) {
					//response.setStatus(307);
					//response.setHeader("Location", referer);
				//} else {
					// 세션 소멸 후 로그인 페이지로 이동
					//tmpMap.put("sessionProcTyp", "1");	// 세션처리 유형(1:세션소멸, 2:로그인세션 셋팅)
					//Map<String, Object> sessionMap = loginService.loginSessionMng(tmpMap, request);
					
					// 로그인 페이지로 이동
					if( SystemUtils.isAjaxRequest(request) ) {
						response.sendError(errCode);
					} else {
						//response.sendRedirect(contextPath+loginPath + "?eCode=" + "error.eCode." + errCode);
						request.setAttribute("eCode", errCd);
						request.setAttribute("eMsg",  eMsg);							
						RequestDispatcher dispatcher = request.getRequestDispatcher(contextPath+redirectPath);
						dispatcher.forward(request, response);							
					}						
				//}
				return false;
			}			
		}
		
		return super.preHandle(request, response, handler);
	}
}
