/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 * Revision History
 * Author            Date              Description
 * ------            -----             ------------
 * lys               2018.06.28        파라메터 입력값에 대한 Cross-Site Scripting 공격에 대한 필터링 처리 구현
 */
package com.olleh.pHubCMS.common.spring.filter;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;


/**
 * 파라메터 입력값에 대한 Cross-Site Scripting 공격에 대한 필터링 처리 구현
 * @Class Name : XssRequestWrapper
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
public class XssRequestWrapper extends HttpServletRequestWrapper {

	public XssRequestWrapper(HttpServletRequest request) {
		super(request);
	}

	@Override
	public String[] getParameterValues(String name) {
		String[] values = super.getParameterValues(name);
		if( values == null ) {
			return null;
		}
		int count = values.length;
		String[] encodedValues = new String[count];
		for( int i=0; i<count; i++ ) {
			encodedValues[i] = filter(values[i], name);
		}
		return encodedValues;
	}
	
	@Override
	public String getParameter(String name) {
		String value = super.getParameter(name);
		
		if( value == null ) {
			return null;
		}
		return filter(value, name);
	}

	@Override
	public String getHeader(String name) {
		String value = super.getHeader(name);
		if( value == null ) {
			return null;
		}
		
		return filter(value);
	}
	
	@Override
	public Map getParameterMap() {
		Map<String,Object[]> params = super.getParameterMap();
		Map<String,Object[]> changeParams = new HashMap<String, Object[]>();
		
		for( String key : params.keySet()) {
			changeParams.put(key, getParameterValues(key));
		}
		
		return changeParams;
	}
	
	private String filter(String input)  {
		if( input == null ) {
			return null;
		}
		return new XssHtmlInputFilter(false).filter(input);
	}
	
	private String filter(String input, String key) {
		if (input == null) {
			return null;
		}
		return new XssHtmlInputFilter(false).filter(input, key);
	}
}
