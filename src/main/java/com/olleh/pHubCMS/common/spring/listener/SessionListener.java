/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 * Revision History
 * Author            Date              Description
 * ------            -----             ------------
 * lys               2018.06.28        세션 생성과 소멸시 이벤트를 처리하기 위한 리스너 정의 
 */
package com.olleh.pHubCMS.common.spring.listener;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.FrameworkServlet;

import com.olleh.pHubCMS.admin.login.service.LoginService;
import com.olleh.pHubCMS.admin.system.service.UserInfoService;
import com.olleh.pHubCMS.common.components.Constant;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.AdminLoginVO;
import com.olleh.pHubCMS.common.utils.JsonUtil;
import com.olleh.pHubCMS.common.utils.SessionUtils;
import com.olleh.pHubCMS.common.utils.StringUtil;
import com.olleh.pHubCMS.common.utils.SystemUtils;


/**
 * 세션관리 Listener 
 * @Class Name : SessionListener
 * @author lys
 * @since 2018.09.10
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.10   lys        최초생성
 * </pre>
 */
public class SessionListener implements HttpSessionListener, HttpSessionAttributeListener {
	private Logger log = new Logger(this.getClass());
	
    //싱글턴패턴 사용을 위한 클래스변수
    public static SessionListener sessionListener = null;
    
    //세션객체를 저장할 Hashtable
    //동기화를 위해 Hashtable로 선언
    //public static Hashtable<String, Object> sessionRepository;
    private static ConcurrentHashMap<String, Object> sessionRepository;
    
    //생성자
    public SessionListener() {        
        if(sessionRepository == null) {
            sessionRepository = new ConcurrentHashMap<>();
        }
    }
    
    //싱글턴패턴으로 객체 생성 후 리턴.
    public static synchronized SessionListener getInstance() {
        if(sessionListener == null) {
        	sessionListener = new SessionListener();
        }
        
        return sessionListener;
    }
    
    //세션중복체크 후 중복된 세션은 만료
    public boolean expireDuplicatedSession(String userId, String sessionId) {
        String methodName = "expireDuplicatedSession";
        boolean result = false;
        
        Enumeration<Object> enumeration = sessionRepository.elements();
        log.debug(methodName, "Active session count : " + sessionRepository.size());
        
        while(enumeration.hasMoreElements()) {
            
            HttpSession session = (HttpSession) enumeration.nextElement();
            
			AdminLoginVO vo = (AdminLoginVO) session.getAttribute(SessionUtils.ADMINLOGIN_VO);
			log.debug("sessionDestroyed",  "vo="+JsonUtil.toJson(vo));
			
			// 사용자 정보가 존재하고
			if( (vo != null) && (!"".equals(StringUtil.nvl(vo.getUserId()))) ) {
				
                //해당 사용자 정보와 파라미터로 넘어온 사용자 아이디가 같을경우
                if( userId.equals(vo.getUserId()) ) {
                    
                    //세션아이디를 체크, 세션아이디가 다를 경우 중복된 로그인.
                    if( !sessionId.equals(session.getId()) ) {
                    	log.debug(methodName, "duplicate login - user : " + vo.getUserId() + ", session id : " + session.getId());
                        
                        //기존의 중복된 세션은 만료처리
                    	session.invalidate();
                        
                        result = true;
                    }
                }
			}
        }
        
        return result;
    }

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		String methodName = "sessionDestroyed";
		log.debug(methodName, "sessionDestroyed");
		//new SingleSignOn().popSession(event.getSession().getId());
		//String userLogInHistYN = StringUtil.nvl(SessionUtils.getShareMapValue("userLogInHistYN", SystemUtils.getCurrentRequest()), "N");
		
		try {
			// declare
			String uri = "";
			// get request
			HttpServletRequest request = SystemUtils.getCurrentRequest();
			
			// get session
			HttpSession session = se.getSession();
			
			// spring web application context
			ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(
					se.getSession().getServletContext(), FrameworkServlet.SERVLET_CONTEXT_PREFIX + "appServlet" );
			
			LoginService loginService = context.getBean(LoginService.class);
			
			
			/***************************************************************
			 * 1. 세션에서 AdminLoginVO 정보 추출
			 ***************************************************************/
			AdminLoginVO vo = (AdminLoginVO) session.getAttribute(SessionUtils.ADMINLOGIN_VO);
			log.debug(methodName,  "vo="+JsonUtil.toJson(vo));
			
			if( (vo != null) && (!"".equals(StringUtil.nvl(vo.getUserId()))) ) {
				// uri 추출
				if( request != null ) uri = request.getRequestURI();
				
				// 로그인페이지가 아닌경우만 
				if( !"/login/login.do".equals(uri) ) {
					/***************************************************************
					 * 1. 로그아웃 이력 기록
					 ***************************************************************/
					Map<String, Object> loginHisMap = new HashMap<String, Object>();
					loginHisMap.put("userId",      StringUtil.nvl(vo.getUserId()));
					loginHisMap.put("loginIp",     StringUtil.nvl(vo.getLoginIp()));
					loginHisMap.put("loginStat",   Constant.LOGIN_STAT_CD_OS);
					loginHisMap.put("loginErrCnt", 0);
					
					UserInfoService userInfoService = context.getBean(UserInfoService.class);					
					userInfoService.modifyLoginN(loginHisMap);
					
					log.debug(methodName,  "loginHisMap="+JsonUtil.toJson(loginHisMap));
					
					// insert ADM_USER_LOGIN_HIST
					loginService.createUserLoginHist(loginHisMap);					
				}
			}
			
			// 세션객체 소멸
	        synchronized (sessionRepository) {
	            sessionRepository.remove(session.getId());
	            log.debug(methodName, "session has destroyed by SesssionEventListener - session id : " + session.getId());
	        }			
						
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
		}
		
		log.debug(methodName, "finish");		
	}

    //세션 attribute add 이벤트
    //session created 이벤트가 아닌 attribute add 이벤트를 사용한 이유는 시스템상 세션 생성시간이 아닌 로그인 정보가 세션에 담기는 시점에 수행되어야한다.	
	@Override
	public void attributeAdded(HttpSessionBindingEvent se) {

        if( (SessionUtils.ADMINLOGIN_VO).equals(se.getName()) ) {
            
            HttpSession session = se.getSession();
            
            synchronized (sessionRepository) {
                sessionRepository.put(session.getId(), session);
                log.debug("attributeAdded", "session registed. this session will managed by SesssionEventListener - session id :" + session.getId());                
            }
        }		
	}

	@Override
	public void attributeRemoved(HttpSessionBindingEvent se) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void attributeReplaced(HttpSessionBindingEvent se) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void sessionCreated(HttpSessionEvent se) {
		//log.debug("sessionCreated", "sessionCreated");
	}
}
