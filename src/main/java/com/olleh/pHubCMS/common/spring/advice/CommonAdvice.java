/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 */
package com.olleh.pHubCMS.common.spring.advice;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import com.olleh.pHubCMS.common.components.Constant;
import com.olleh.pHubCMS.common.exception.BizException;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.utils.JsonUtil;
import com.olleh.pHubCMS.common.utils.StringUtil;
import com.olleh.pHubCMS.common.utils.SystemUtils;


/**
 * 공통 Advice
 * @Class Name : CommonAdvice
 * @author lys
 * @since 2018.08.23
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.23   lys        최초생성
 * </pre>
 */
@ControllerAdvice
public class CommonAdvice implements ResponseBodyAdvice<Object> {
	private Logger log = new Logger(this.getClass());
	
	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		return true;
	}

	
	/**
	 * <pre> server-front ajax통신시 서버에서 프론트로 리턴되는 메시지값을 변환한다. </pre>
	 * 
	 * @param Map<String,Object> params
	 * @param HttpServletRequest request
	 * @return String
	 * @throws Exception 
	 * @see <pre></pre>
	 */	
	@Override
	public Object beforeBodyWrite(Object pBody, MethodParameter returnType, MediaType selectedContentType,
			Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request,
			ServerHttpResponse response) {
		// TODO Auto-generated method stub
		Object body = pBody;
		
		try {
			// type 확인
			String typeName = body.getClass().getName();
			log.debug("postHandle", "typeName=" + typeName);		
			
			// declare		
			Map<String, Object> tmpBody = null;
			String retCode = "";
			String retMsg = "";
			
			
			// Ajax 통신일때만
			if( isAjaxRequest(request) ) {
				//log.debug("postHandle", "--- is Ajax ---");
				// 형변환
				if( "java.lang.String".equals(typeName) ) {
					tmpBody = JsonUtil.JsonToMap(String.valueOf(body));
				
				} else if( "java.util.HashMap".equals(typeName) ) {
					tmpBody = new HashMap<String, Object>((Map<String, Object>) body);
				}
				
				// ret_code\":\"00\",\"ret_msg
				retCode = (String)tmpBody.get(Constant.RET_CODE);
				retMsg = (String)tmpBody.get(Constant.RET_MSG);
				
				// 리턴코드별 에러 메시지 정의
				if( !(Constant.SUCCESS_CODE).equals(retCode) ) {
					log.debug("postHandle", "--- beforeBodyWrite 수행 ---");
					
					
					tmpBody.put(Constant.RET_CODE, "-999");
					if(retMsg.equals("")){
						tmpBody.put(Constant.RET_MSG, "서비스 이용에 불편을 드려 죄송합니다.\n잠시 후 다시 이용하여 주시기 바랍니다.[errCode="+retCode+"]");
					}					
					
					log.info("postHandle", "beforeBodyWrite.bef="+JsonUtil.toJson(body));
					log.info("postHandle", "beforeBodyWrite.aft="+JsonUtil.toJson(tmpBody));					
					
					// 리턴값 형변환
					if( "java.lang.String".equals(typeName) ) {
						body = JsonUtil.MapToJson(tmpBody);
					
					} else if( "java.util.HashMap".equals(typeName) ) {
						body = tmpBody;
					}
				}
			}			
		} catch (Exception e)  {
			log.error("postHandle", "[Exception]msg=" + StringUtil.nvl(e.getMessage()));
		}

		
		//log.debug("postHandle", "--- beforeBodyWrite 종료 ---");
		return body;
	}
	
	
	/**
	 * <pre> 익셉션 핸들링 </pre>
	 * 
	 * @param  <?>
	 * @return String
	 * @throws Exception 
	 * @see <pre></pre>
	 */		
    @ResponseStatus(HttpStatus.BAD_REQUEST)  
    @ExceptionHandler(value = BizException.class)  
    public ModelAndView handleBaseException(BizException e){
    	log.debug("BaseException", "Start!");
    	log.debug("BaseException", "getErrMsg=" + StringUtil.nvl(e.getErrMsg()));
    	log.debug("BaseException", "getMessage=" + StringUtil.nvl(e.getMessage()));
    	
    	// set ModelAndView
    	ModelAndView mv = new ModelAndView("comm/error");
    	
    	// ajax 통신여부 체크
    	if( SystemUtils.isAjaxRequest(SystemUtils.getCurrentRequest()) ) {
    		mv.setViewName("jsonView");
    		mv.addObject(Constant.RET_CODE, StringUtil.nvl(e.getErrCd(), "F997"));
    		mv.addObject(Constant.RET_MSG,  "Fail!");    		
    	} else {
    		mv.addObject("eCode", StringUtil.nvl(e.getErrCd(), "F997"));
    		mv.addObject("eMsg",  "Fail!");
    	}
    	
    	// return 
    	//log.debug("Exception]", "mv=" + JsonUtil.toJson(mv));
    	return mv;
    }  
      
    @ExceptionHandler(value = Exception.class)  
    public ModelAndView handleException(Exception e) throws Exception {
    	log.printStackTracePH("handleException", e);
    	log.debug("Exception]", "getMessage=" + StringUtil.nvl(e.getMessage()));
    	
    	// set ModelAndView
    	ModelAndView mv = new ModelAndView("common/error");
    	
    	// ajax 통신여부 체크
    	if( SystemUtils.isAjaxRequest(SystemUtils.getCurrentRequest()) ) {
    		mv.setViewName("jsonView");
    		mv.addObject(Constant.RET_CODE, "F998");
    		mv.addObject(Constant.RET_MSG,  "Fail!");
    	} else {
    		mv.addObject("eCode", "F998");
    		mv.addObject("eMsg",  "Fail!");
    	}
    	
    	// return 
    	//log.debug("Exception]", "mv=" + JsonUtil.toJson(mv));
    	return mv;
    }   

	/**
	 * ajax 요청이 맞는지 체크후 결과를 반환한다.
	 * @param request
	 * @return
	 */
	private boolean isAjaxRequest(ServerHttpRequest request) {
		
		List<String> header = request.getHeaders().get(Constant.AJAX_HEADER);
		if( header != null && "true".equals(header.get(0)) ) {
			return true;
		}
		return false;
 	}
}
