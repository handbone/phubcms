/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 * Revision History
 * 
 * @desc 
 * 현재 spring mobile 라이브러리를 적용하여 user-agent 값에 따라 jsp 페이지의 경로를 pc 와 mb 로 분기 시킨다.
 * 만약 pc 와 mb 양쪽에서 공통적으로 사용하는 jsp가 필요한 경우에는 현재 설정만으로는 어렵다.<br/>
 * 이 문제를 해결하기 위해 해당 class 를 상속받아서 jsp 의 prefix,suffix 정보를 조건에 따라 재설정한다.
 * 
 * Author            Date              Description
 * ------            -----             ------------
 * lys               2018.06.28        PC, 모바일 구분을 위한 user-agent 체크시 추가적인 키워드 처리 
 */
package com.olleh.pHubCMS.common.spring.view;

import javax.servlet.http.HttpServletRequest;

import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DeviceUtils;
import org.springframework.mobile.device.site.SitePreference;
import org.springframework.mobile.device.site.SitePreferenceUtils;
import org.springframework.mobile.device.util.ResolverUtils;
import org.springframework.mobile.device.view.LiteDeviceDelegatingViewResolver;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ViewResolver;


/**
 * PC, 모바일 구분을 위한 user-agent 체크시 추가적인 키워드 처리
 * @Class Name : CustomLiteDeviceDelegatingViewResolver
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << desc >>
 * 현재 spring mobile 라이브러리를 적용하여 user-agent 값에 따라 jsp 페이지의 경로를 pc 와 mb 로 분기 시킨다.
 * 만약 pc 와 mb 양쪽에서 공통적으로 사용하는 jsp가 필요한 경우에는 현재 설정만으로는 어렵다.<br/>
 * 이 문제를 해결하기 위해 해당 class 를 상속받아서 jsp 의 prefix,suffix 정보를 조건에 따라 재설정한다.
 * 
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
public class CustomLiteDeviceDelegatingViewResolver extends LiteDeviceDelegatingViewResolver {

	private org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(this.getClass());
	
	private String normalPrefix = "pc/";
	private String mobilePrefix = "mb/";
	private String tabletPrefix = "mb/";
	private String commonPrefix = "cm/";
	
	public CustomLiteDeviceDelegatingViewResolver(ViewResolver delegate) { super(delegate); }
	
	@Override
	protected String getDeviceViewNameInternal(String viewName) {
		RequestAttributes attrs = RequestContextHolder.getRequestAttributes();
		Assert.isInstanceOf(ServletRequestAttributes.class, attrs);
		HttpServletRequest request = ((ServletRequestAttributes) attrs).getRequest();
		Device device = DeviceUtils.getCurrentDevice(request);
		SitePreference sitePreference = SitePreferenceUtils.getCurrentSitePreference(request);
		String resolvedViewName = viewName;
		
		log.debug("sitePreference : {}", sitePreference); //site_preference=normal, site_preference=mobile
		if( StringUtils.startsWithIgnoreCase(viewName, commonPrefix) ) {
			resolvedViewName = viewName;
		} else {
			if (ResolverUtils.isNormal(device, sitePreference)) {
				resolvedViewName = normalPrefix + viewName;
			} else if (ResolverUtils.isMobile(device, sitePreference)) {
				resolvedViewName = mobilePrefix + viewName;
			} else if (ResolverUtils.isTablet(device, sitePreference)) {
				resolvedViewName = tabletPrefix + viewName;
			}
		}
		log.debug("resolvedViewName : {}", resolvedViewName);
		return stripTrailingSlash(resolvedViewName);
	}

	private String stripTrailingSlash(String viewName) {
		if (viewName.endsWith("//")) {
			return viewName.substring(0, viewName.length() - 1);
		}
		return viewName;
	}
}