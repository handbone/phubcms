/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 * Revision History
 * Author            Date              Description
 * ------            -----             ------------
 * lys               2018.06.28        PC, 모바일 구분을 위한 user-agent 체크시 추가적인 키워드 처리 
 */
package com.olleh.pHubCMS.common.spring.view;

import org.springframework.mobile.device.LiteDeviceResolver;


/**
 * PC, 모바일 구분을 위한 user-agent 체크시 추가적인 키워드 처리
 * @Class Name : CustomLiteDeviceResolver
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
public class CustomLiteDeviceResolver extends LiteDeviceResolver {

	@Override
	protected void init() {
		super.init();
		getMobileUserAgentKeywords().add("iphone");
		getMobileUserAgentKeywords().add("android");
	}
}
