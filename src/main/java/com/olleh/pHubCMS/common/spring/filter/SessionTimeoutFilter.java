/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 * Revision History
 * Author            Date              Description
 * ------            -----             ------------
 * lys               2018.06.28        ajax 또는 form submit 요청시 세션이 만료되었을 경우 오류 메세지를 반환하도록 구현
 */
package com.olleh.pHubCMS.common.spring.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.StringUtils;

import com.olleh.pHubCMS.common.utils.SystemUtils;


/**
 * 세션타임아웃체크 Filter
 * @Class Name : SessionTimeoutFilter
 * @author lys
 * @since 2018.08.23
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.23   lys        최초생성
 * </pre>
 */
public class SessionTimeoutFilter implements Filter {
	private org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(this.getClass());
	/**
	 * 공통 오류 처리를 위한 코드값
	 */
	int SESSION_TIMED_OUT = 901;
	/**
	 * 공통 오류 페이지 
	 */
	String redirectPath = "/index.jsp";
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
//		log.debug("--- SessionTimeoutFilter 수행 ---");
		HttpServletRequest httpServletRequest = (HttpServletRequest)request;
		HttpServletResponse httpServletResponse = (HttpServletResponse)response;

		//eCode 값을 체크해서 리다이렉션 순환 오류가 발생하지 않도록 한다.
		if( httpServletRequest.getRequestedSessionId() != null && !httpServletRequest.isRequestedSessionIdValid() && StringUtils.isEmpty(httpServletRequest.getParameter("eCode")) ) {
			if( SystemUtils.isAjaxRequest(httpServletRequest) ) {
				httpServletResponse.sendError(SESSION_TIMED_OUT);
			} else {
				//httpServletResponse.sendRedirect(redirectPath + "?eCode=" + "error.eCode." + SESSION_TIMED_OUT);
				httpServletResponse.sendRedirect(redirectPath);
			}
		} else {
			chain.doFilter(httpServletRequest, httpServletResponse);
		}
	}

	@Override
	public void destroy() {}

}
