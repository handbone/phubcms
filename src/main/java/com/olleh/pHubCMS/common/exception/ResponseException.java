package com.olleh.pHubCMS.common.exception;

import org.springframework.http.HttpStatus;

/**
 * Response Exception
 *
 * @version 1.0.0
 * @see RuntimeException
 * @since 7.0
 */
public class ResponseException extends RuntimeException {

	private static final long serialVersionUID = 9017831796437492002L;

	/**
	 * HTTP Status
	 */
	private final HttpStatus status;

	/**
	 * Result Code
	 */
	private final int code;

	/**
	 * Default Response
	 */
	@SuppressWarnings("all")
	private final Object object;

	/**
	 * Response Exception
	 */
	public ResponseException() {
		super();
		this.status = HttpStatus.INTERNAL_SERVER_ERROR;
		this.code = HttpStatus.INTERNAL_SERVER_ERROR.value();
		this.object = null;
	}

	/**
	 * Response Exception
	 *
	 * @param status
	 *            HttpStatus
	 */
	public ResponseException(final HttpStatus status) {
		super();
		this.status = status;
		this.code = status.value();
		this.object = null;
	}

	/**
	 * Response Exception
	 *
	 * @param status
	 *            HttpStatus
	 * @param code
	 *            Result Code
	 */
	public ResponseException(final HttpStatus status, final int code) {
		super();
		this.status = status;
		this.code = code;
		this.object = null;
	}

	/**
	 * Response Exception
	 *
	 * @param message
	 *            Result Message
	 */
	public ResponseException(final String message) {
		super(message);
		this.status = HttpStatus.INTERNAL_SERVER_ERROR;
		this.code = HttpStatus.INTERNAL_SERVER_ERROR.value();
		this.object = null;
	}

	/**
	 * Response Exception
	 *
	 * @param code
	 *            Result Code
	 * @param message
	 *            Result Message
	 */
	public ResponseException(final int code, final String message) {
		super(message);
		this.status = HttpStatus.INTERNAL_SERVER_ERROR;
		this.code = code;
		this.object = null;
	}

	/**
	 * Response Exception
	 *
	 * @param status
	 *            HttpStatus
	 * @param message
	 *            Result Message
	 */
	public ResponseException(final HttpStatus status, final String message) {
		super(message);
		this.status = status;
		this.code = HttpStatus.INTERNAL_SERVER_ERROR.value();
		this.object = null;
	}

	/**
	 * Response Exception
	 *
	 * @param status
	 *            HttpStatus
	 * @param code
	 *            Result Code
	 * @param message
	 *            Result Message
	 */
	public ResponseException(final HttpStatus status, final int code, final String message) {
		super(message);
		this.status = status;
		this.code = code;
		this.object = null;
	}

	/**
	 * Response Exception
	 *
	 * @param message
	 *            Result Message
	 * @param cause
	 *            Throwable
	 */
	public ResponseException(final String message, final Throwable cause) {
		super(message, cause);
		this.status = HttpStatus.INTERNAL_SERVER_ERROR;
		this.code = HttpStatus.INTERNAL_SERVER_ERROR.value();
		this.object = null;
	}

	/**
	 * Response Exception
	 *
	 * @param code
	 *            Result Code
	 * @param message
	 *            Result Message
	 * @param cause
	 *            Throwable
	 */
	public ResponseException(final int code, final String message, final Throwable cause) {
		super(message, cause);
		this.status = HttpStatus.INTERNAL_SERVER_ERROR;
		this.code = code;
		this.object = null;
	}

	/**
	 * Response Exception
	 *
	 * @param status
	 *            HttpStatus
	 * @param message
	 *            Result Message
	 * @param cause
	 *            Throwable
	 */
	public ResponseException(final HttpStatus status, final String message, final Throwable cause) {
		super(message, cause);
		this.status = status;
		this.code = status.value();
		this.object = null;
	}

	/**
	 * Response Exception
	 *
	 * @param status
	 *            HttpStatus
	 * @param code
	 *            Result Code
	 * @param message
	 *            Result Message
	 * @param cause
	 *            Throwable
	 */
	public ResponseException(final HttpStatus status, final int code, final String message, final Throwable cause) {
		super(message, cause);
		this.status = status;
		this.code = code;
		this.object = null;
	}

	/**
	 * Response Exception
	 *
	 * @param cause
	 *            Throwable
	 */
	public ResponseException(final Throwable cause) {
		super(cause);
		this.status = HttpStatus.INTERNAL_SERVER_ERROR;
		this.code = HttpStatus.INTERNAL_SERVER_ERROR.value();
		this.object = null;
	}

	/**
	 * Response Exception
	 *
	 * @param code
	 *            Result Code
	 * @param cause
	 *            Throwable
	 */
	public ResponseException(final int code, final Throwable cause) {
		super(cause);
		this.status = HttpStatus.INTERNAL_SERVER_ERROR;
		this.code = code;
		this.object = null;
	}

	/**
	 * Response Exception
	 *
	 * @param status
	 *            HttpStatus
	 * @param cause
	 *            Throwable
	 */
	public ResponseException(final HttpStatus status, final Throwable cause) {
		super(cause);
		this.status = status;
		this.code = status.value();
		this.object = null;
	}

	/**
	 * Response Exception
	 *
	 * @param status
	 *            HttpStatus
	 * @param code
	 *            Result Code
	 * @param cause
	 *            Throwable
	 */
	public ResponseException(final HttpStatus status, final int code, final Throwable cause) {
		super(cause);
		this.status = status;
		this.code = code;
		this.object = null;
	}

	/**
	 * Response Exception
	 *
	 * @param obj
	 *            Object
	 */
	public ResponseException(final Object obj) {
		super();
		this.status = HttpStatus.INTERNAL_SERVER_ERROR;
		this.code = HttpStatus.INTERNAL_SERVER_ERROR.value();
		this.object = obj;
	}

	/**
	 * Response Exception
	 *
	 * @param obj
	 *            Object
	 * @param cause
	 *            Throwable
	 */
	public ResponseException(final Object obj, final Throwable cause) {
		super(cause);
		this.status = HttpStatus.INTERNAL_SERVER_ERROR;
		this.code = HttpStatus.INTERNAL_SERVER_ERROR.value();
		this.object = obj;
	}

	/**
	 * Response Exception
	 *
	 * @param status
	 *            HttpStatus
	 * @param obj
	 *            Object
	 */
	public ResponseException(final HttpStatus status, final Object obj) {
		super();
		this.status = status;
		this.code = status.value();
		this.object = obj;
	}

	/**
	 * Response Exception
	 *
	 * @param status
	 *            HttpStatus
	 * @param obj
	 *            Object
	 * @param cause
	 *            Throwable
	 */
	public ResponseException(final HttpStatus status, final Object obj, final Throwable cause) {
		super(cause);
		this.status = status;
		this.code = status.value();
		this.object = obj;
	}

	/**
	 * Http Status
	 *
	 * @return the HttpStatus
	 */
	public HttpStatus getStatus() {
		return status;
	}

	/**
	 * Result Code
	 *
	 * @return the code
	 */
	public int getCode() {
		return code;
	}

	/**
	 * Default Response.
	 *
	 * @return the response
	 */
	public Object getObject() {
		return object;
	}

}
