/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 * Revision History
 * Author            Date              Description
 * ------            -----             ------------
 * lys               2018.06.28        System Exception 
 */
package com.olleh.pHubCMS.common.exception;

import com.olleh.pHubCMS.common.model.ErrorInfoVO;
import com.olleh.pHubCMS.common.model.MsgInfoVO;
import com.olleh.pHubCMS.common.utils.DateUtils;

/**
 * 개발자 정의 Exception
 * @Class Name : UserException
 * @author : lys
 * @since : 2018.08.15
 * @version : 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.15   lys        최초생성
 * </pre>
 */
public class UserException extends Exception {	
	/**
	 * 
	 */
	private static final long serialVersionUID = 622111594007932131L;
	
	private String datePattern = "yyyyMMdd";
	
	/** 에러정보 객체  */
	private ErrorInfoVO errorInfo =  new ErrorInfoVO();
	
//	MsgInfoVO msgInfoVO = messageManage.getMsgVOY("IF_INFO_104");
//	card.setRetCode((String)msgInfoVO.getMsgCd());
//	card.setRetMsg((String)msgInfoVO.getMsgNm());
	
	/**
     * UserException
     */
    public UserException() {
        super();
        errorInfo.setDateTime(DateUtils.getDateTimeByPattern(datePattern));   
    }
    
    /**
     * UserException
     * @param e     예외
     */
    public UserException(java.lang.Exception e) {
    	super(e);
    	errorInfo.setDateTime(DateUtils.getDateTimeByPattern(datePattern));
    	errorInfo.setOriginException(e);
    }
    
    /**
     * UserException 
     * @param stepCd 스텝코드
     */
    public UserException(String stepCd) {
    	super();
    	errorInfo.setDateTime(DateUtils.getDateTimeByPattern(datePattern));
    	errorInfo.setStepCd(stepCd);
    }
    
    /**
     * UserException
     * @param e     예외
     */
    public UserException(String stepCd, java.lang.Exception e) {
    	super(e);
    	errorInfo.setDateTime(DateUtils.getDateTimeByPattern(datePattern));
    	errorInfo.setStepCd(stepCd);
    	errorInfo.setOriginException(e);
    }    
    
    /**
     * UserException 
     * @param stepCd 스텝코드
     */
    public UserException(String stepCd, MsgInfoVO msgInfoVO, java.lang.Exception e) {
    	super();
    	errorInfo.setDateTime(DateUtils.getDateTimeByPattern(datePattern));
    	errorInfo.setStepCd(stepCd);
    	errorInfo.setErrCd(msgInfoVO.getMsgCd());
    	errorInfo.setErrMsg(msgInfoVO.getMsgNm());   
    	errorInfo.setOriginException(e);
    }
    
    /**
     * UserException 
     * @param stepCd 스텝코드
     */
    public UserException(String stepCd, MsgInfoVO msgInfoVO) {
    	super();
    	errorInfo.setDateTime(DateUtils.getDateTimeByPattern(datePattern));
    	errorInfo.setStepCd(stepCd);
    	errorInfo.setErrCd(msgInfoVO.getMsgCd());
    	errorInfo.setErrMsg(msgInfoVO.getMsgNm());        
    }    
	
    /**
     * UserException 
     * @param className 클래스명
     * @param methodName 메소드명
     * @param errId 에러아이디   
     */
    public UserException(String className, String methodName, String stepCd, String errCd, String errMsg) {
    	super();
    	errorInfo.setDateTime(DateUtils.getDateTimeByPattern(datePattern));
    	errorInfo.setClassName(className);
    	errorInfo.setMethodName(methodName);    	
    	errorInfo.setStepCd(stepCd);
    	errorInfo.setErrCd(errCd);
    	errorInfo.setErrMsg(errMsg);
    } 
    
    /**
     * UserException 
     * @param className 클래스명
     * @param methodName 메소드명
     * @param errId 에러아이디   
     * @param e 예외 
     */
    public UserException(String className, String methodName, 
    		String errorCode, Exception originException) {
    	super(originException);
    	errorInfo.setDateTime(DateUtils.getDateTimeByPattern(datePattern));
    	errorInfo.setClassName(className);
    	errorInfo.setMethodName(methodName);
    	errorInfo.setErrCd(errorCode);
    	//errorInfo.setErrorMessage(MessageMng.getMessage(errorCode));
    	errorInfo.setOriginException(originException);
    } 
        
    /**
     * UserException 
     * @param className 클래스명
     * @param methodName 메소드명
     * @param errId 에러아이디   
     * @param paramObj 파라미터OBJ   
     * @param e     예외
     */
    public UserException(String className, String methodName, 
    		String errorCode, String errorMsg, Exception originException) {
    	super(originException);
    	errorInfo.setDateTime(DateUtils.getDateTimeByPattern(datePattern));
    	errorInfo.setClassName(className);
    	errorInfo.setMethodName(methodName);
    	errorInfo.setErrCd(errorCode);
    	errorInfo.setErrMsg(errorMsg);
    	//errorInfo.setDateTime(DateTimeUtil.getDateTime());
    	errorInfo.setOriginException(originException);    	
    }    
    
    /**
     * UserException 
     * @param className 클래스명
     * @param methodName 메소드명
     * @param errId 에러아이디   
     * @param paramObj 파라미터OBJ   
     * @param e     예외
     */
    public UserException(String className, String methodName, 
    		String errorCode, Object paramObj, Exception originException) {
    	super(originException);
    	errorInfo.setDateTime(DateUtils.getDateTimeByPattern(datePattern));
    	errorInfo.setClassName(className);
    	errorInfo.setMethodName(methodName);
    	errorInfo.setErrCd(errorCode);
    	//errorInfo.setErrorMessage(MessageMng.getMessage(errorCode));
    	//errorInfo.setDateTime(DateTimeUtil.getDateTime());
    	errorInfo.setParamObj(paramObj);
    	errorInfo.setOriginException(originException);    	
    }    
    
    /**
	 * 에러정보객체 취득
	 * @return ErrorInfo 에러정보객체
	 */
	public ErrorInfoVO getErrorInfo() {
		return errorInfo;
	}
	
	/**
	 * 에러정보객체 취득
	 * @param errorInfo 에러정보객체
	 */
	public void setErrorInfo(ErrorInfoVO errorInfo) {
		this.errorInfo = errorInfo;
	}	
}