package com.olleh.pHubCMS.common.scheduler.job;

import java.io.File;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.scheduler.dao.PHubDAO;
import com.olleh.pHubCMS.common.scheduler.task.GetDealInfoTask;
import com.olleh.pHubCMS.common.scheduler.task.GetWorkIndPGTask;
import com.olleh.pHubCMS.common.scheduler.task.MakeCmprFileTask;
import com.olleh.pHubCMS.common.scheduler.task.SaveResultTask;
import com.olleh.pHubCMS.common.scheduler.task.SendCmprFileTask;
import com.olleh.pHubCMS.common.scheduler.utils.CommonUtil;
import com.olleh.pHubCMS.common.utils.JsonUtil;
import com.olleh.pHubCMS.common.utils.StringUtil;
import com.olleh.pHubCMS.scheduler.model.PHubToPGVO;


/**
 * 포인트허브에서 PG사로의 연동 처리 job
 * @Class Name : PHubToPGJob
 * @author mason
 * @since 2018.08.22
 * @version 0.1
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.21   mason      최초생성
 * </pre>
 */
@Service("pHubToPGJob")
public class PHubToPGJob {
	private Logger log = new Logger(this.getClass());
	
	@Resource
	PHubDAO pHubDAO;	
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
	CommonUtil commonUtil;
	
	@Autowired
	GetWorkIndPGTask getWorkIndPGTask;
	
	@Autowired
	SaveResultTask saveResultTask;
	
	@Autowired
	SendCmprFileTask sendCmprFileTask;
	
	@Autowired
	MakeCmprFileTask makeCmprFileTask;
	
	@Autowired
	GetDealInfoTask getDealInfoTask;
	
	/* 작업 Task 선언 */
	//private GetWorkIndPGTask   workIndTask;  //대사처리여부 체크 Task
	//private GetDealInfoTask    dealInfTask;  //거래내역 조회 Task
	//private MakeCmprFileTask   makeFileTask; //대사파일 생성 Task
	//private SendCmprFileTask   sendFileTask; //생성된 파일 전송 Task
	//private SaveResultTask     saveResTask;  //작업결과 저장 Task
	
	/**
	 * <pre> job 실행 함수 </pre>
	 * 
	 * @param Job 실행 콘텍스트
	 * @return void
	 * @see
	 */		
	public void execute() throws JobExecutionException {
		//
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("cmprDd", commonUtil.getCurrentDate());
		param.put("userId", "btch");
		cmprProc(param);
	}	
	
	/**
	 * <pre> job 실행 함수 </pre>
	 * 
	 * @param Job 실행 콘텍스트
	 * @return void
	 * @see
	 */		
	public void cmprProc(Map<String,Object> params) {
		//현재 실행중인 함수
		String methodName = "cmprProc";
		log.debug(methodName, "Start cmprProc! (params=" + JsonUtil.toJson(params) + ")");
		
//		log.debug(methodName,"WAS 실행 루트:"+System.getProperty("user.dir"));
		/* ----------------------------------------------------------------------------------------------- */
		// get param
		String cmprDd = StringUtil.nvl(params.get("cmprDd"), commonUtil.getCurrentDate());	// 대사일(yyyymmdd) : 현재일자 기준 -1일이 기준일자
		String userId = StringUtil.nvl(params.get("userId"), "btch");						// 작업자
		
		//0 PG일대사번호 조회
		String ddCmprNo = this.getSeqCmId();
		String wrknNm   = this.getClass().getSimpleName();
		String pgCmpnId = "PG0001";
		String fileName = pgCmpnId +"_" + cmprDd + ".dat";
		/* ----------------------------------------------------------------------------------------------- */
		//로그마스터 생성
		Map<String,Object> logMstMap = new HashMap<String,Object>();
		logMstMap.put("dd_cmpr_no", ddCmprNo);
		logMstMap.put("cmpr_dd",    cmprDd);
		logMstMap.put("wrkn_nm",    wrknNm);
		logMstMap.put("user_id",    userId);
		int iLogM = this.saveAdmBatchLogM(logMstMap);
		
		//로그 상세 파라미터 생성
		Map<String,Object> logMap = this.getLogMap(ddCmprNo, cmprDd, userId);
		/* ----------------------------------------------------------------------------------------------- */
		// 2018-09-18 Business summary
		// 1.대사처리여부 체크(일대사M에 해당 대사일자 기준 데이타가 존재하는지 체크) 
		// 2.일대사정보 저장(PG_CMPR_INFO_M,PG_CMPR_INFO_D)
		// 3.대사정보 조회
		// 4.대사파일 생성
		// 5.대사파일 전송
		/* ----------------------------------------------------------------------------------------------- */
		//1.대사처리여부 체크 결과
		Map resMap1 = null;
		String workInd = "";
		//2.일대사정보 저장(PG_CMPR_INFO_M,PG_CMPR_INFO_D)
		Map<String,Object> saveMap = new HashMap<String,Object>();  //저장처리 변수Map
		Map<String,Object> saveRes = new HashMap<String,Object>();	//저장결과 리턴Map	
		//3.대사정보 조회 및 대사파일 생성
		Map<String,Object> makeMap = new HashMap<String,Object>();
		Map<String,Object> makeRes = new HashMap<String,Object>();
		String resFileName = "";
		String resFullName = "";
		String resTgtPath  = "";
		String resCmprPath = "";
		int    iCount      = 0;
		Map<String,Object> sftpRes = new HashMap<String,Object>();	//전송결과 리턴Map
		//4.대사파일 전송
		try{
			/* ------------------------------------------------------------------------------------------- */
			// 1.대사처리여부 체크 로그 시작
			this.insertAdmBatchLog(logMap);
			/* ------------------------------------------------------------------------------------------- */
			//0.이전 데이타 삭제 처리
			Map<String,Object> oldMap = new HashMap<String,Object>();
			oldMap.put("deal_dd", cmprDd);
			int iOld = this.deleteOldData(oldMap);
			/* ------------------------------------------------------------------------------------------- */
			// 1.대사처리여부 체크
			resMap1 = this.getWorkInd(cmprDd);
			log.debug(methodName,"1.대사처리여부 체크 결과 :"+resMap1.toString());
			if((resMap1 != null)&&(resMap1.isEmpty() == false)){
				workInd = String.valueOf(resMap1.get("work_ind"));
			}
			/* ------------------------------------------------------------------------------------------- */
			if(workInd.equals("N")){
				/* --------------------------------------------------------------------------------------- */
				//1.대사처리여부 체크 로그 끝
				logMap.put("rslt_code", messageManage.getMsgCd("SY_INFO_00"));
				logMap.put("rslt_msg",  messageManage.getMsgTxt("SY_INFO_00"));
				logMap.put("tgt_cnt",   1);
				logMap.put("cmpr_cnt",  1);	
				this.updateAdmBatchLog(logMap);
				/* --------------------------------------------------------------------------------------- */
				//2.일대사정보 저장(PG_CMPR_INFO_M,PG_CMPR_INFO_D) 로그 시작
				logMap.put("task_id", "2.GetDealInfoTask");
				this.insertAdmBatchLog(logMap);
				//2.일대사정보 저장(PG_CMPR_INFO_M,PG_CMPR_INFO_D)
				saveMap.put("dd_cmpr_no",   ddCmprNo);
				saveMap.put("user_id",      userId);
				saveMap.put("cmpr_dd",      cmprDd);
				saveMap.put("pg_cmpn_id",   pgCmpnId);
				saveMap.put("file_path",    commonUtil.getFilePath("cp"));
				saveMap.put("file_nm",      fileName);
				saveMap.put("trtm_rslt_cd", messageManage.getMsgCd("SY_INFO_00")); 
				saveMap.put("rslt_msg",     messageManage.getMsgTxt("SY_INFO_00"));
				
				saveRes = this.saveResult(saveMap);
				log.debug(methodName,"2.일대사정보 저장 결과 :"+saveRes.toString());
				/* --------------------------------------------------------------------------------------- */
				if( ("00".equals(StringUtil.nvl(saveRes.get("rslt_code")))) ||
					("F104".equals(StringUtil.nvl(saveRes.get("rslt_code")))) ) {					
					//2.일대사정보 저장(PG_CMPR_INFO_M,PG_CMPR_INFO_D) 로그 끝
					logMap.put("rslt_code", saveRes.get("rslt_code"));
					logMap.put("rslt_msg",  saveRes.get("rslt_msg"));
					logMap.put("tgt_cnt",   saveRes.get("tgt_cnt"));
					logMap.put("cmpr_cnt",  saveRes.get("cmpr_cnt"));
					this.updateAdmBatchLog(logMap);
					/* ----------------------------------------------------------------------------------- */
					//3.대사정보 조회 및 대사파일 생성 로그 시작
					logMap.put("task_id", "3.MakeCmprFileTask");
					this.insertAdmBatchLog(logMap);
					//3.대사정보 조회 및 대사파일 생성
					makeMap.put("dd_cmpr_no", ddCmprNo);
					makeMap.put("cmpr_dd",    cmprDd);
					makeMap.put("file_nm",    fileName);
					makeRes = this.makeFile(makeMap);
					log.debug(methodName, "3.대사정보 조회 및 대사파일 생성:"+makeRes.get("file_exists"));
					resFileName = String.valueOf(makeRes.get("file_nm"));
					resFullName=  String.valueOf(makeRes.get("full_file_nm"));
					resTgtPath  = String.valueOf(makeRes.get("tgt_file_path"));
				    resCmprPath = String.valueOf(makeRes.get("cmpr_file_path"));
					iCount      = Integer.parseInt(String.valueOf(makeRes.get("data_rows")));
					/* ----------------------------------------------------------------------------------- */
					if((resFileName!=null)&&(!resFileName.equals(""))){
						//3.대사정보 조회 및 대사파일 생성 로그 끝
						logMap.put("rslt_code", messageManage.getMsgCd("SY_INFO_00"));
						logMap.put("rslt_msg",  messageManage.getMsgTxt("SY_INFO_00"));
						logMap.put("tgt_cnt",   iCount);
						logMap.put("cmpr_cnt",  iCount);
						logMap.put("tgt_file_path",   resTgtPath);
						logMap.put("cmpr_file_path",  resCmprPath);
						this.updateAdmBatchLog(logMap);
						
						//배치 로그마스터 업데이트
						logMstMap.put("rslt_code",   messageManage.getMsgCd("SY_INFO_00"));
						logMstMap.put("rslt_msg",    messageManage.getMsgTxt("SY_INFO_00"));
						logMstMap.put("doc_file_nm", resFileName);
						this.updateAdmBatchLogM(logMstMap);
						
						/* ------------------------------------------------------------------------------- */
						//4.대사파일 전송 로그 시작
						logMap.put("task_id", "4.SendCmprFileTask");
						logMap.put("tgt_file_path",  commonUtil.getFilePath("cm")); 
						logMap.put("cmpr_file_path", commonUtil.getFilePath("cp"));
						this.insertAdmBatchLog(logMap);
						
						sftpRes = this.sendFile(resFullName);
						log.debug("sftpRes", "sftpRes:"+sftpRes.toString());
						//4.대사파일 전송 로그 끝
						logMap.put("rslt_code", sftpRes.get("retcode"));
						logMap.put("rslt_msg",  sftpRes.get("retmsg"));
						logMap.put("tgt_cnt",   1);
						if( (sftpRes.get("retcode")!=null) && ("00".equals(StringUtil.nvl(sftpRes.get("retcode")))) ){
							logMap.put("cmpr_cnt",  1);
							
							moveFile(resFileName); //파일 복사
						}else{
							logMap.put("cmpr_cnt",  0);	
						}
						this.updateAdmBatchLog(logMap);
						//로그마스터 업데이트
						logMstMap.put("rslt_code",    sftpRes.get("retcode"));
						logMstMap.put("rslt_msg",     sftpRes.get("retmsg"));
						logMstMap.put("ttl_deal_cnt", iCount);	
						this.updateAdmBatchLogM(logMstMap);	
						log.debug(methodName, "PG대사처리 완료!");
					}else{
						/* ------------------------------------------------------------------------------- */
						//file not found
						log.debug(methodName, messageManage.getMsgTxt("CM_ERROR_603"));
						//3.대사정보 조회 및 대사파일 생성 로그 끝
						logMap.put("rslt_code", messageManage.getMsgCd("CM_ERROR_603"));
						logMap.put("rslt_msg",  messageManage.getMsgTxt("CM_ERROR_603"));
						logMap.put("tgt_cnt",   1);
						logMap.put("tgt_file_path",   resTgtPath);
						logMap.put("cmpr_file_path",  resCmprPath);
						this.updateAdmBatchLog(logMap);
						//배치 로그마스터 업데이트
						logMstMap.put("rslt_code",    messageManage.getMsgCd("CM_ERROR_603"));
						logMstMap.put("rslt_msg",     messageManage.getMsgTxt("CM_ERROR_603"));
						logMstMap.put("ttl_deal_cnt", 1);
						this.updateAdmBatchLogM(logMstMap);
					}
				}else{
					/* ----------------------------------------------------------------------------------- */
					//db_operation_fail
					log.debug(methodName, messageManage.getMsgTxt("SY_ERROR_800"));
					//2.일대사정보 저장(PG_CMPR_INFO_M,PG_CMPR_INFO_D) 로그 끝
					logMap.put("rslt_code", messageManage.getMsgCd("SY_ERROR_800"));
					logMap.put("rslt_msg",  messageManage.getMsgTxt("SY_ERROR_800"));
					logMap.put("tgt_cnt",   saveRes.get("tgt_cnt"));
					logMap.put("cmpr_cnt",  saveRes.get("cmpr_cnt"));	
					this.updateAdmBatchLog(logMap);
				}
			}else{
				/* --------------------------------------------------------------------------------------- */
				//해당 대사일자에 대해 대사처리 기완료
				log.debug(methodName, messageManage.getMsgTxt("CM_ERROR_601"));
				//1.대사처리여부 체크 로그 끝
				logMap.put("rslt_code", messageManage.getMsgCd("CM_ERROR_601"));
				logMap.put("rslt_msg",  messageManage.getMsgTxt("CM_ERROR_601"));
				logMap.put("tgt_cnt",   1);
				this.updateAdmBatchLog(logMap);
				//로그마스터 끝
				logMstMap.put("rslt_code",    messageManage.getMsgCd("CM_ERROR_601"));
				logMstMap.put("rslt_msg",     messageManage.getMsgTxt("CM_ERROR_601"));
				logMstMap.put("ttl_deal_cnt", 1);
				this.updateAdmBatchLogM(logMstMap);
				/* --------------------------------------------------------------------------------------- */
			}
		}catch(Exception e){
			log.error(methodName, e.getMessage());
		}		
	}
	
	/**
	 * <pre> PG대사처리 저장 </pre>
	 * 
	 * @param 거래내역 리스트, 
	 * @return 처리결과
	 * @see
	 */		
	public Map<String,Object> saveResult(Map<String,Object> paramMap) throws Exception{
		//현재 실행중인 함수
		String methodName = "saveResult";
		Map<String,Object> rtnMap = new HashMap<String,Object>();
		try{
			rtnMap      = saveResultTask.saveResult(paramMap);
		}catch(Exception e){
			log.error(methodName, e.getMessage());
		}
		return rtnMap;
	}
	/**
	 * <pre> 사용안함 </pre>
	 * 
	 * @param  
	 * @return 
	 * @see
	 */	
	public Map<String,Object> saveResult(List<PHubToPGVO> list,Map<String,Object> paramMap) throws Exception{
//		//현재 실행중인 함수
//		String methodName = "saveResult";
//		Map<String,Object> rtnMap = new HashMap<String,Object>();
//		try{
//			rtnMap      = saveResultTask.saveResult(list, paramMap);
//		}catch(Exception e){
//			log.error(methodName, e.getMessage());
//		}
		return null;
	}
	
	/**
	 * <pre> PG대사파일 전송 </pre>
	 * 
	 * @param 대사파일명
	 * @return 처리결과
	 * @see
	 */	
	public Map<String,Object> sendFile(String fileName) throws Exception{
		//현재 실행중인 함수
		String methodName = "sendFile";
		Map<String,Object> resMap = new HashMap<String,Object>();
		try{
			resMap = sendCmprFileTask.sendFile(fileName);
			log.debug(methodName, "sendFile2:"+resMap.toString());
		}catch(Exception e){
			log.printStackTracePH(methodName, e);
			resMap.put("retcode", messageManage.getMsgCd("CM_ERROR_605"));
			resMap.put("retmsg",  messageManage.getMsgTxt("CM_ERROR_605"));
		}
		log.debug(methodName, "sendFile:"+resMap.toString());
		return resMap;
	}
	/**
	 * <pre> PG대사파일 생성 </pre>
	 * 
	 * @param 거래내역 리스트
	 * @return 대사파일명
	 * @see
	 */	
	public Map<String,Object> makeFile(Map<String,Object> params) throws Exception{
		//현재 실행중인 함수
		String methodName = "makeFile";
		Map<String,Object> resMap = new HashMap<String,Object>();
		
		try{
			resMap     = makeCmprFileTask.makeFile(params);
		}catch(Exception e){
			log.printStackTracePH(methodName, e);
		}
		return resMap;   
	}
	
	/**
	 * <pre> 대사일자에 대한 거래내역 조회 </pre>
	 * 
	 * @param 대사일자
	 * @return 거래내역 리스트
	 * @see
	 */		
	public List<PHubToPGVO> getPhubToPG(Map<String,Object> params) throws Exception{
		//현재 실행중인 함수
		String methodName = "getPhubToPG";
		
		List<PHubToPGVO> dataList = new ArrayList<PHubToPGVO>();
		try{
			dataList = getDealInfoTask.getPhubToPG(params);			
		}catch(Exception e){
			log.printStackTracePH(methodName, e);
		}
		return dataList;
	}
	
	/**
	 * <pre> 대사일자별 PG 대사처리 여부,대사일자 </pre>
	 * 
	 * @param 
	 * @return 처리여부 구분값 Y/N,대사일자
	 * @see
	 */	
	public Map getWorkInd(String cmprDd) throws Exception{
		//현재 실행중인 함수
		String methodName = "getWorkInd";
		
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("deal_dd", cmprDd);
		
		Map resMap = new HashMap();
		
		try{
			resMap = getWorkIndPGTask.getWorkInd(params);
		}catch(Exception e){
			log.error(methodName, e.getMessage());
		}
		return resMap;
	}

	/**
	 * <pre> 파일생성일자 가져오기 </pre>
	 * 
	 * @param 
	 * @return 파일생성일자
	 * @see
	 */		
	public String getFileBuildDate(){
		String resDate = "";
		Calendar calendar = Calendar.getInstance();		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		resDate = dateFormat.format(calendar.getTime());

		return resDate;		
	}
	/**
	 * <pre> PG 일대사번호 조회 </pre>
	 * 
	 * @param 
	 * @return PG 일대사번호
	 * @see
	 */
	public String getSeqCmId(){
		String resStr = null;
		Map dataMap = new HashMap();
		try{
			dataMap = pHubDAO.getSeqCmId();
			if(dataMap!=null){
				resStr = String.valueOf(dataMap.get("DD_CMPR_NO"));
			}
		}catch(Exception e){			
			log.printStackTracePH("getSeqCmId", e);
		}
		return resStr;
	}	
	/**
	 * <pre> 배치실행로그 저장 </pre>
	 * 
	 * @param 로그번호, 일대사번호, 일대사일자, task아이디,대상건수,처리건수,대상파일경로,처리파일경로,결과코드,결과메시지,등록자ID
	 * @return 
	 * @see
	 */	
	public void insertAdmBatchLog(Map<String,Object> params){
		try{
			pHubDAO.insertAdmBatchLog(params);
		}catch(Exception e){
			log.printStackTracePH("insertAdmBatchLog", e);
		}
	}
	/**
	 * <pre> 배치실행로그 업데이트 </pre>
	 * 
	 * @param 결과코드,결과메시지,대상건수,처리건수,(key:일대사번호, 일대사일자, task아이디)
	 * @return void
	 * @see
	 */		
	public void updateAdmBatchLog(Map<String,Object> params){
		try{
			pHubDAO.updateAdmBatchLog(params);
		}catch(Exception e){
			log.printStackTracePH("updateAdmBatchLog", e);
		}
	}
	/**
	 * <pre> 로그 상세 파라미터 세팅 </pre>
	 * 
	 * @param 
	 * @return 로그파라미터 map
	 * @see
	 */		
	public Map<String,Object> getLogMap(String ddCmprNo, String cmprDd, String userId){
		Map<String,Object> logMap = new HashMap<String,Object>();
		try{
			//로그 파라미터 세팅
			logMap.put("dd_cmpr_no",     ddCmprNo);
			logMap.put("cmpr_dd",        cmprDd);
			logMap.put("task_id",        "1.GetWorkIndPGTask");
			logMap.put("tgt_cnt",        0);
			logMap.put("cmpr_cnt",       0);
			logMap.put("tgt_file_path",  commonUtil.getFilePath("cm")); //세틀뱅크 대상 경로
			logMap.put("cmpr_file_path", commonUtil.getFilePath("cp")); //클립포인트 대상 경로
			logMap.put("rslt_code",      "");
			logMap.put("rslt_msg",       "");
			logMap.put("user_id",        userId);
		}catch(Exception e){
			log.printStackTracePH("getLogMap", e);
		}
		return logMap;
	}
	/**
	 * <pre> 로그 마스터 저장 </pre>
	 * 
	 * @param 일대사번호,대사일자,작업명,사용자아이디
	 * @return 처리결과
	 * @see
	 */		
	public int saveAdmBatchLogM(Map<String,Object> params){
		int iRes = 0;
		try{
			iRes = pHubDAO.insertAdmBatchLogM(params);
			log.debug("saveClCmprInfo", "배치작업로그M 생성 결과 : " + iRes);
		}catch(Exception e){
			log.printStackTracePH("saveAdmBatchLogM", e);
		}
		return iRes;
	}	
	/**
	 * <pre> 로그 마스터 수정 </pre>
	 * 
	 * @param 대사파일명, 불일치건수, 일치건수, 전체건수, 결과코드, 결과메시지, 일대사번호, 대사일자, 작업명
	 * @return 처리결과
	 * @see
	 */		
	public int updateAdmBatchLogM(Map<String,Object> params){
		int iRes = 0;
		try{
			iRes = pHubDAO.updateAdmBatchLogM(params);
			log.debug("saveClCmprInfo", "배치작업로그M 수정 결과 : " + iRes);
		}catch(Exception e){
			log.printStackTracePH("updateAdmBatchLogM", e);
		}
		return iRes;
	}	
	/**
	 * <pre> PG대사결과 기존 데이타 삭제 </pre>
	 * 
	 * @param 
	 * @return 처리결과
	 * @see
	 */		
	public int deleteOldData(Map<String,Object> params){
		int iDtl = 0, iMst = 0, iSum = 0;
		
		try{
			iDtl = getWorkIndPGTask.deletePgCmprInfoD(params);
			iMst = getWorkIndPGTask.deletePgCmprInfoM(params);
			iSum = iDtl + iMst;
		}catch(Exception e){
			log.printStackTracePH("deleteOldData", e);
		}
		return iSum;
	}
	/**
	 * <pre> SFTP가 정상적으로 전송 완료되면 해당 파일을 백업 폴더로 이동 </pre>
	 * 
	 * @param 
	 * @return 처리결과
	 * @see
	 */		
	public int moveFile(String fileName){
		int iRes = 0;
		String srcFullPath = "";
		String tgtFilePath = "";
		
		try{
			srcFullPath = commonUtil.getPgFileBaseDir() + File.separator + fileName;//
			tgtFilePath = commonUtil.getPgFileBackDir() + File.separator + fileName;
			
			Path from = Paths.get(srcFullPath);
			Path to   = Paths.get(tgtFilePath);
			
			FileChannel fromChannel = FileChannel.open(from, StandardOpenOption.READ);
			FileChannel toChannel   = FileChannel.open(to,   StandardOpenOption.CREATE,StandardOpenOption.WRITE);
			
			ByteBuffer buffer = ByteBuffer.allocate(100);
			
			int byteCount = 0;
			
			while(true){
				buffer.clear();
				byteCount = fromChannel.read(buffer);
				
				if(byteCount==-1)
					break;
				
				buffer.flip();
				toChannel.write(buffer);
			}
			
			fromChannel.close();
			toChannel.close();
			
			File fileFrm = new File(srcFullPath);
			File fileTo  = new File(tgtFilePath);
			boolean bTo  = fileTo.exists();
			if(bTo==true){
				log.debug("moveFile", "기존 파일 백업 폴더로 복사 성공");
				if(fileFrm.delete()){
					log.debug("moveFile", "기존 파일삭제 성공");
					iRes = 1;
				}else{
					log.debug("moveFile", "File Not Found");
				}
			}else{
				log.debug("moveFile", "기존 파일 백업 폴더로 복사 실패");
			}
			//log.debug("moveFile", f.exists()+","+f.getAbsolutePath());
		}catch(Exception e){
			log.printStackTracePH("moveFile", e);
		}
		
		return iRes;
	}
}
