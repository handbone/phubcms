package com.olleh.pHubCMS.common.scheduler.task;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.exception.BizException;
import com.olleh.pHubCMS.common.exception.UserException;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.ErrorInfoVO;
import com.olleh.pHubCMS.common.scheduler.dao.PHubDAO;
import com.olleh.pHubCMS.common.scheduler.task.settleBank.GetWorkIndClipTask;
import com.olleh.pHubCMS.common.utils.DateUtils;
import com.olleh.pHubCMS.common.utils.JsonUtil;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 클립 포인트 대사 결과를 저장
 * @Class Name : SaveClipResultTask
 * @author mason
 * @since 2018.08.24
 * @version 0.1
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.24   mason      최초생성
 * </pre>
 */
@Service
public class SaveClipResultTask {
	private Logger log = new Logger(this.getClass());
	
	@Resource
	PHubDAO pHubDAO;
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
	GetWorkIndClipTask getWorkIndClipTask;
	
	
	/**
	 * <pre> 클립데이타 대사처리 저장 </pre>
	 * 
	 * @param 대사일자,전문파일명 
	 * @return retcode, retmsg
	 * @see
	 */	
	public Map<String,Object> saveClipResult(Map<String,Object> param){
		log.debug("클립데이타 대사처리 저장", param.toString());
		
		//현재 실행중인 함수
		String methodName = "saveClipResult";
		String stepCd = "00";
		
		// declare
		int prevCmprCnt = 0;
		int iTgt   = 0;
		int iSucs  = 0;
		int iFail  = 0;
		int iPhub  = 0;
		int iClip  = 0;
		int iCmpr  = 0;
		int iPoint = 0;
		int iMst   = 0;
		int iCmp   = 0;
		
		Map<String,Object> resMap = new HashMap<String,Object>();
		try{
			// get
			prevCmprCnt = Integer.parseInt(StringUtil.nvl(param.get("cmprCnt"), "0"));	// 이전단계 대사건수
			
			// set
			param.put("cmpr_dd",    param.get("deal_dd"));	// 일반카드 대사일자
			param.put("sm_cmpr_dd", DateUtils.calcDate(StringUtil.nvl(param.get("deal_dd")), -1));	// 삼성카드 대사일자 : 삼성카드는 대사일자-1로 체크 한다. (그외 카드는 대사일자로 비교)
			
			//대사결과 성공 건수 저장 
			if( prevCmprCnt > 0 ) {
				stepCd = "01";
				iSucs  = this.saveClipSucs(param);
				
				// 대사 결과 성공 건수 저장 (매입취소)
				iSucs += this.saveClipPuchasCnclSucs(param);
				
				//대사결과 실패 건수 저장
				stepCd = "02";
				iFail  = this.saveClipFail(param);
				
				//포인트허브에 없는 거래건에 대한 메시지 업데이트 처리 : 포인트허브에 없거나, ci, 금액, 카드사코드, 이전포인트거래번호 등이 다를때.
				stepCd = "03";
				iPhub  = this.updateNoExistInPhub(param);
				if( iPhub < 0 ) {
					log.error(methodName, "대사 실패건에 대한 실패사유 갱신 처리 실패!");
					throw new UserException(stepCd, messageManage.getMsgVOY("CM_ERROR_613"));				
				}
				
				//클립 대사파일에 없는 거래건 실패테이블에 등록 처리
				stepCd = "04";
				iClip  = this.insertNoExistInClip(param);

				//클립포인트 대사파일에는 존재하나, 포인트허브 거래원장에는 존재하지 않는 실패건에 대해 D-2까지 재비교
				stepCd = "05";
				
				//대사결과 카드별 저장
				stepCd = "06";
				iPoint = this.saveClipPoint(param);				
			}
			
			//대사결과 마스터 업데이트
			stepCd = "07";
			iMst   = this.updateClCmprInfo2(param); 

			log.debug("saveClipResult", "iCmpr:"+iCmpr);
			log.debug("saveClipResult", "iSucs:"+iSucs+",iFail:"+iFail+",iClip:"+iClip+",iPhub:"+iPhub+",iPoint:"+iPoint+",iMst:"+iMst);
			
			stepCd = "08";
			Map<String,Object> tgtMap = new HashMap<String,Object>();
			tgtMap.put("dd_cmpr_no", param.get("dd_cmpr_no"));
			tgtMap.put("cmpr_dd", param.get("cmpr_dd"));
			tgtMap.put("work_ind", "tgt_cnt");
			
			stepCd = "09";
			iTgt = this.getClCmprInfo(tgtMap);
			
			stepCd = "10";
			Map<String,Object> cmprMap = new HashMap<String,Object>();
			cmprMap.put("dd_cmpr_no", param.get("dd_cmpr_no"));
			cmprMap.put("cmpr_dd", param.get("cmpr_dd"));
			cmprMap.put("work_ind", "cmpr_cnt");
			
			stepCd = "11";
			iCmp = this.getClCmprInfo(cmprMap);
			if(iMst>0){
				resMap.put("retcode", messageManage.getMsgCd("SY_INFO_00"));
				resMap.put("retmsg", messageManage.getMsgTxt("SY_INFO_00"));
	    		resMap.put("tgt_cnt", iTgt);
	    		resMap.put("cmpr_cnt", iCmp);
	    		resMap.put("no_acr_cnt", iFail+iClip);
	    		resMap.put("mtch_cnt", iSucs+iCmpr);
			}else{
				// internal exception
				stepCd = "12";
				log.error(methodName, "["+stepCd+"]대사결과 마스터 업데이트 실패!");
				throw new UserException(stepCd, messageManage.getMsgVOY("SY_ERROR_900"));		
			}
			
		} catch (UserException e) {
			ErrorInfoVO errorInfoVO = e.getErrorInfo();
			resMap.put("retcode",  errorInfoVO.getErrCd());
			resMap.put("retmsg",   errorInfoVO.getErrMsg());	
			resMap.put("tgt_cnt",  1);
    		resMap.put("cmpr_cnt", 0);			
			
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
		}
		
		return resMap;
	}
	/**
	 * <pre> 클립포인트 대상건수/작업건수 조회 </pre>
	 * 
	 * @param 대사일자,일대사번호,작업구분
	 * @return 클립포인트 대상건수/작업건수
	 * @see
	 */		
	public int getClCmprInfo(Map<String,Object> params){
		int iRes = 0;
		Map tempMap = new HashMap();
		String destInd = String.valueOf(params.get("work_ind"));
		try{
			tempMap = pHubDAO.getClCmprInfo(params);
			if((tempMap!=null)&&(tempMap.isEmpty()==false)){
				if(destInd.equals("tgt_cnt")){
					iRes = Integer.parseInt(tempMap.get("tgt_cnt").toString());
				}else if(destInd.equals("cmpr_cnt")){
					iRes = Integer.parseInt(tempMap.get("cmpr_cnt").toString());
				} 
			}else{
				iRes = 0;
			}
		}catch(Exception e){
			log.printStackTracePH("getClCmprInfo", e);
		}
		return iRes;
	}
	
	/**
	 * <pre> 클립포인트 대사 마스터 저장 </pre>
	 * 
	 * @param 대사일자 
	 * @return 1 : 성공, 0 : 실패
	 * @see
	 */		
	public int saveClipM(Map<String,Object> param){
		//현재 실행중인 함수
		String methodName = "saveClipM";			
		int iMst  = 0;
		Map<String,Object> tempMap = new HashMap<String,Object>();
		try{
			tempMap.put("cmpr_dd", param.get("deal_dd"));
			tempMap.put("doc_file_nm", param.get("file_name"));
			tempMap.put("dd_cmpr_no", param.get("dd_cmpr_no"));
			
			iMst  = pHubDAO.updateClCmprInfo(tempMap);
		}catch(Exception e){
			log.printStackTracePH(methodName, e);
		}
		return iMst;
	}	
	/**
	 * <pre> 클립포인트 대사 카드사별 저장 </pre>
	 * 
	 * @param 대사일자 
	 * @return 1 : 성공, 0 : 실패
	 * @see
	 */		
	public int saveClipPoint(Map<String,Object> param){
		//현재 실행중인 함수
		String methodName = "saveClipPoint";			
		int iPoint  = 0;
		
		try{
			iPoint  = pHubDAO.insertClCmprPoint(param);
		}catch(Exception e){
			log.printStackTracePH(methodName, e);
		}
		return iPoint;
	}	
	/**
	 * <pre> 클립포인트 대사 실패 저장 </pre>
	 * 
	 * @param 대사일자 
	 * @return 1 : 성공, 0 : 실패
	 * @see
	 */		
	public int saveClipFail(Map<String,Object> param){
		//현재 실행중인 함수
		String methodName = "saveClipFail";			
		int iFail  = 0;
		
		try{
			iFail  = pHubDAO.insertClCmprFail(param);
		}catch(Exception e){
			log.printStackTracePH(methodName, e);
		}
		return iFail;
	}
	/**
	 * <pre> 클립포인트 대사 성공 저장 </pre>
	 * 
	 * @param 대사일자 
	 * @return 1 : 성공, 0 : 실패
	 * @see
	 */		
	public int saveClipSucs(Map<String,Object> param){
		//현재 실행중인 함수
		String methodName = "saveClipSucs";			
		int iSucs  = 0;
		
		try{
			iSucs  = pHubDAO.insertClCmprSucs(param);
		}catch(Exception e){
			log.printStackTracePH(methodName, e);
		}
		return iSucs;
	}
	/**
	 * <pre> 클립일대사결과성공D_P (매입취소) 저장 </pre>
	 * 
	 * @param params
	 * @return
	 */
	public int saveClipPuchasCnclSucs(Map<String, Object> params) {
		//현재 실행중인 함수
		String methodName = "saveClipPuchasCnclSucs";			
		int iSucs  = 0;
		
		try{
			iSucs  = pHubDAO.insertClipPuchasCnclSucs(params);
		}catch(Exception e){
			log.printStackTracePH(methodName, e);
		}
		return iSucs;
	}
	/**
	 * <pre> 클립 대사파일에 없는 거래건에 대한 메시지 업데이트 처리 </pre>
	 * 
	 * @param 대사일자 
	 * @return 1 : 성공, 0 : 실패
	 * @see
	 */		
	public int insertNoExistInClip(Map<String,Object> param){
		//현재 실행중인 함수
		String methodName = "insertNoExistInClip";		
		int iRes  = 0;
		
		try{
			//"This transaction does not messageManageipPoint."
			param.put("err_cd", messageManage.getMsgCd("CM_ERROR_608"));
			param.put("err_msg", messageManage.getMsgTxt("CM_ERROR_608"));
			iRes  = pHubDAO.insertNoExistInClip(param);
		}catch(Exception e){
			log.printStackTracePH(methodName, e);
		}
		return iRes;
	}	
	/**
	 * <pre> 포인트허브에 없는 거래건에 대한 메시지 업데이트 처리 </pre>
	 * 
	 * @param 대사일자 
	 * @return 1 : 성공, -1 : 실패
	 * @see
	 */		
	@SuppressWarnings("rawtypes")
	public int updateNoExistInPhub(Map<String,Object> param) {
		//현재 실행중인 함수
		String methodName = "updateNoExistInPhub";			
		int iRes  = 0;
		
		try {
			// 클립포인트 대사 실패건 조회
			List failList = pHubDAO.selectClCmprFailByDdCmprNo(param);
			
			// 실패건을 읽어서 건별로 체크 한다.
			// 포인트허브에 없거나, ci, 금액, 카드사코드, 이전포인트거래번호 등이 다를때.
			iRes = this.updateNoExistInPhubResult(failList, param);
			
			// 클립포인트 대사 실패건 조회 (매입취소)
			List failPuchasCnclList = pHubDAO.selectClCmprFailByPuchasCncl(param);
			
			// 실패건을 읽어서 건별로 체크 한다.
			// 포인트허브에 없거나, ci, 금액, 카드사코드, 이전포인트거래번호 등이 다를때.
			iRes += this.updateNoExistInPhubResult(failPuchasCnclList, param);
			
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
			iRes = -1;			
		}
		
		return iRes;
	}	
	public int updateClCmprInfo2(Map<String,Object> param){
		return pHubDAO.updateClCmprInfo2(param);
	}
	
	/**
	 * 대사 실패 건 유효성 체크
	 * 
	 * @param failList
	 * @param params
	 * @return
	 * @throws BizException
	 */
	@SuppressWarnings("rawtypes")
	private int updateNoExistInPhubResult(List failList, Map<String,Object> params) throws BizException {
		String methodName = "updateNoExistInPhubResult";
		
		int ret = 0;
		try {
			for(int i=0; i < failList.size(); i++) {
				Map failData = (Map) failList.get(i);
				log.debug(methodName, "failData="+JsonUtil.toJson(failData));
				
				String errCd  = "";
				String errMsg = "";
				
				// 1. 포인트허브 존재여부 체크
				if( "".equals(StringUtil.nvl(failData.get("phub_tr_no"))) ) {
					// "This transaction does not exist in pointHub."
					errCd  = messageManage.getMsgCd("CM_ERROR_607");
					errMsg = messageManage.getMsgTxt("CM_ERROR_607");
				}
				
				// 3. 카드사코드 일치여부 체크
				if( ("".equals(errCd)) &&
						(!(StringUtil.nvl(failData.get("pnt_cd"))).equals(StringUtil.nvl(failData.get("cmpr_pnt_cd")))) ) {
					//
					errCd  = messageManage.getMsgCd("CM_INFO_610");
					errMsg = messageManage.getMsgTxt("CM_INFO_610");
				}
				
				// 4. 포인트(금액) 일치여부 체크
				if( ("".equals(errCd)) &&
						(!(StringUtil.nvl(failData.get("ans_pnt"))).equals(StringUtil.nvl(failData.get("cmpr_ans_pnt")))) ) {
					//
					errCd  = messageManage.getMsgCd("CM_INFO_611");
					errMsg = messageManage.getMsgTxt("CM_INFO_611");
				}
				
				// 5. 이전포인트거래번호 일치여부 체크
				if( ("".equals(errCd)) &&
						(!(StringUtil.nvl(failData.get("ori_pnt_tr_no"))).equals(StringUtil.nvl(failData.get("cmpr_ori_pnt_tr_no")))) ) {
					//
					errCd  = messageManage.getMsgCd("CM_INFO_612");
					errMsg = messageManage.getMsgTxt("CM_INFO_612");
				}
				
				// 2. CI값 일치여부 체크
				if( ("".equals(errCd)) &&
						(!(StringUtil.nvl(failData.get("cust_ci"))).equals(StringUtil.nvl(failData.get("cmpr_cust_ci")))) ) {
					//
					errCd  = messageManage.getMsgCd("CM_INFO_609");
					errMsg = messageManage.getMsgTxt("CM_INFO_609");
				}				
				
				params.put("errCd",    errCd);
				params.put("errMsg",   errMsg);
				params.put("ddCmprNo", StringUtil.nvl(failData.get("dd_cmpr_no")));
				params.put("seq",      Integer.parseInt(StringUtil.nvl(failData.get("seq"))));
				
				ret = pHubDAO.updateClCmprFailForErrInfo(params);
			}
		} catch (Exception e) {
			throw new BizException(e);
		}
		
		return ret;
	}
}
