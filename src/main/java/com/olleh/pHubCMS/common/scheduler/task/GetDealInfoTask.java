package com.olleh.pHubCMS.common.scheduler.task;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.common.scheduler.dao.PHubDAO;
import com.olleh.pHubCMS.scheduler.model.PHubToPGVO;

/**
 * PG 거래내역 조회
 * @Class Name : GetDealInfoTask
 * @author mason
 * @since 2018.08.23
 * @version 0.1
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.23   mason      최초생성
 * </pre>
 */
@Service
public class GetDealInfoTask {
	
	@Resource
	PHubDAO pHubDAO;	
	
	/**
	 * <pre> 대사일자별 거래내역 조회 </pre>
	 * 
	 * @param 대사일자
	 * @return 거래내역 리스트
	 * @see
	 */		
	public List<PHubToPGVO> getPhubToPG(Map<String,Object> params) throws Exception{
		//현재 실행중인 함수명
		String methodName = "getPhubToPG";
		return pHubDAO.getPhubToPG(params);
	}
}
