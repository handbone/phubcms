package com.olleh.pHubCMS.common.scheduler.job;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.common.components.CodeManage;
import com.olleh.pHubCMS.common.components.MenuActionInfo;
import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.components.SysPrmtManage;
import com.olleh.pHubCMS.common.components.UrlManage;
import com.olleh.pHubCMS.common.dao.CommonDAO;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.SysPrmtInfoVO;
import com.olleh.pHubCMS.common.utils.StringUtil;
import com.olleh.pHubCMS.common.utils.SystemUtils;
/**
 * 시스템 job
 * @Class Name : SystemJob
 * @author mason
 * @since 2018.10.16
 * @version 0.1
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.16   lys      최초생성
 * </pre>
 */
@Service("systemJob")
public class SystemJob {
	private Logger log = new Logger(this.getClass());
	
	@Resource
	CommonDAO commonDAO;
	
	@Autowired
	CodeManage codeManage;
	
	@Autowired
	MenuActionInfo menuActionInfo;
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
	SysPrmtManage sysPrmtManage;
	
	@Autowired
	UrlManage urlManage;
	

	/**
	 * <pre> 시스템데이터 was메모리 리로드 처리 </pre>
	 * 
	 * @param Job 실행 콘텍스트
	 * @return void
	 * @see
	 */	
	public void sysDataReload() throws JobExecutionException {
		// 메소드명, 로그내용
		String methodName = "sysDataReload";
		log.debug(methodName, "Start sysDataReload!");
		
		// declare
		String phMemReloadYnKey = "ONM_MEM_RELOAD_YN";
		
		// ip체크
		// get host addr
		String hostAddr = SystemUtils.getHostServerIP();
		log.debug(methodName, "hostAddr="+hostAddr);
		
		// get was1 addr : ONM 시스템은 was가 한대라서 ip체크를 할 필요가 없다.
		/**
		Map<String, Object> sendParam = new HashMap<String, Object>();
		sendParam.put("prmtCd", "ONM_CONN_IP");
		SysPrmtInfoVO sysPrmtInfoVO = commonDAO.getSysPrmtY(sendParam);
		
		String was1Addr = StringUtil.nvl(sysPrmtInfoVO.getPrmtVal());
		log.debug(methodName, "was1Addr="+was1Addr);
		
		// check
		if( hostAddr.equals(was1Addr) ) {
			phMemReloadYnKey = "ONM_MEM_RELOAD_YN";		// was1 호기
		} else {
			phMemReloadYnKey = "ONM_MEM_RELOAD_YN";		// was2 호기
		}
		*/		
		
		// was 메모리 리로드 여부
		// PH_MEM_RELOAD_YN
		// ONM_MEM_RELOAD_YN
		Map<String, Object> sendParam = new HashMap<String, Object>();
		sendParam.put("prmtCd", phMemReloadYnKey);
		SysPrmtInfoVO sysPrmtInfoVO = commonDAO.getSysPrmtY(sendParam);
		
		String memReloadYn = StringUtil.nvl(sysPrmtInfoVO.getPrmtVal(), "N");
		log.debug(methodName, "memReloadYn="+memReloadYn);
		
		if( "Y".equals(memReloadYn) ) {
			// 공통코드
			codeManage.reset();
			
			// 메뉴별 uri 관리 콤포넌트
			menuActionInfo.reset();
			
			// 메시지정보
			messageManage.reset();
			
			// 시스템파라미터
			sysPrmtManage.reset();
			
			// url 정보
			urlManage.reset();
			
			// 시스템 파라미터 값 N으로 갱신
			sendParam.put("prmtVal", "N");
			sendParam.put("userId", "btch");
			int uptCnt = commonDAO.updateSysprmtVal(sendParam);
			log.debug(methodName, "uptCnt="+uptCnt);
			
			// 배치로그 마스터 기록?
			
			// 배치로그 상세 기록?
			
			log.info(methodName, "End sysDataReload!");			
		}
	}
}
