package com.olleh.pHubCMS.common.scheduler.utils;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.olleh.pHubCMS.common.components.SysPrmtManage;
import com.olleh.pHubCMS.common.log.Logger;
/**
 * 배치처리에서 공통적으로 사용되는 기능
 * @Class Name : CommonUtil
 * @author mason
 * @since 2018.08.30
 * @version 0.1
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.30   mason      최초생성
 * </pre>
 */
@Component
public class CommonUtil {
	//private String METHOD_NAME = "";
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	SysPrmtManage sysPrmtManage;
	
	//private PHubDAO pHubDAO;
	//private static Map<String, SysPrmtInfoVO> admSysPrmtMap;
	//private String delimiter = ";";
	/**
	 * <pre> 클립포인트에서 전송된 파일을 sftp로 다운받아 파싱등 작업을 하는 경로</br> 
	 * (로컬 예제) C:\PHDev\STS_371\clippoint-compare (TB) /home/jboss/clippoint-compare
	 * </pre>
	 * @param 
	 * @return Sftp전송이 완료된 다음, 생성된 PG대사파일을 옮기는 백업 경로
	 * @see
	 */	
	public String getClCmprTgtDir(){
		//로컬이 아닌 경우에는 전체경로로 써야 됨.
		return sysPrmtManage.getSysPrmtVal("CP_CMPR_TGT_DIR")+File.separator; //실제 반영되는 경로
//		return "."+sysPrmtManage.getSysPrmtVal("CP_CMPR_TGT_DIR")+File.separator; //로컬 테스트 경로
	}
	/**
	 * <pre> 클립포인트 대사처리가 완료된 다음 작업 파일을 백업경로로 이동</br> 
	 * (로컬 예제) C:\PHDev\STS_371\clippoint-compare\backup (TB) /home/jboss/clippoint-compare/backup
	 * </pre>
	 * @param 
	 * @return 클립포인트 대사처리가 완료된 다음 작업 파일을 백업경로로 이동
	 * @see
	 */		
	public String getClipFileBackDir(){
		//로컬이 아닌 경우에는 전체경로로 써야 됨.
		return sysPrmtManage.getSysPrmtVal("CP_CMPR_BAK_DIR");
//		return "."+sysPrmtManage.getSysPrmtVal("CP_CMPR_BAK_DIR");
	}
	/**
	 * <pre> 클립포인트 대사처리가 완료된 다음 작업 파일이 위치한 경로</br> 
	 * (로컬 예제) C:\PHDev\STS_371\clippoint-compare\ (TB) /home/jboss/clippoint-compare/
	 * </pre>
	 * @param 
	 * @return 클립포인트 대사처리가 완료된 다음 작업 파일이 위치한 경로
	 * @see
	 */		
	public String getClipFileBaseDir(){
		//로컬이 아닌 경우에는 전체경로로 써야 됨.
		return sysPrmtManage.getSysPrmtVal("CP_CMPR_REL_DIR"); 
//		return "."+sysPrmtManage.getSysPrmtVal("CP_CMPR_TGT_DIR")+File.separator;
	}
	/**
	 * <pre> Sftp전송이 완료된 다음, 생성된 PG대사파일을 옮기는 백업 경로</br> 
	 * (로컬) ~/
	 * </pre>
	 * @param 
	 * @return Sftp전송이 완료된 다음, 생성된 PG대사파일을 옮기는 백업 경로
	 * @see
	 */			
	public String getPgFileBackDir(){
		//로컬이 아닌 경우에는 전체경로로 써야 됨.
		return sysPrmtManage.getSysPrmtVal("SB_CMPR_TGT_BAK_DIR");
//		return "C:/PHDev/STS_371/settlebank-compare/backup";
	}
	/**
	 * <pre> PG대사파일이 생성되는 경로 </pre>
	 * 
	 * @param 
	 * @return PG대사파일이 생성되는 경로
	 * @see
	 */		
	public String getPgFileBaseDir(){
		return sysPrmtManage.getSysPrmtVal("SB_CMPR_TGT_DIR");
		//return "C:/PHDev/STS_371/settlebank-compare";
	}
	/**
	 * <pre> PG/클립포인트 대상 경로 </pre>
	 * 
	 * @param 대상구분(cm:세틀뱅크,cp:클립포인트
	 * @return PG/클립포인트 대상 경로
	 * @see
	 */	
	public String getFilePath(String destInd){
		Map<String,Object> tempMap = null;
		String resStr = null;
		try{
			if(destInd.equals("cm")){
				tempMap = this.getPropPG();
				if(tempMap!=null){
					resStr = String.valueOf(tempMap.get("cm_dir"));
				}
			}else if(destInd.equals("cp")){
				tempMap = this.getPropClip();
				if(tempMap!=null){
					resStr = String.valueOf(tempMap.get("cp_dir"));
				}				
			}
		}catch(Exception e){
			log.printStackTracePH("getFilePath", e);
		}
		return resStr;
	}
	/**
	 * <pre> PG와의 SFTP 연동을 위한 세팅 정보 조회 </pre>
	 * 
	 * @param 
	 * @return 호스트IP, 포트, 사용자계정, 비밀번호, 대상 폴더
	 * @throws IOException 
	 * @see
	 */		
	public Map<String,Object> getPropPG() throws IOException{
		String methodName = "getPropPG";
		
		Map<String,Object> resMap = null;
		try{
			String[] cmArr = sysPrmtManage.getSysPrmtAry("SB_CMPR_CONN_IP");
			
			resMap = new HashMap<String,Object>();
			resMap.put("cm_host", cmArr[0]);
			resMap.put("cm_port", cmArr[1]);
			resMap.put("cm_user", sysPrmtManage.getSysPrmtVal("SB_CMPR_CONN_ID"));
			resMap.put("cm_pwd",  sysPrmtManage.getSysPrmtVal("SB_CMPR_CONN_PW"));
			resMap.put("cm_dir",  sysPrmtManage.getSysPrmtVal("SB_CMPR_FILE_DIR"));	
			log.debug("getPropPG",resMap.toString());
		}catch(Exception e){
			log.printStackTracePH(methodName, e);
		}
		
		return resMap;
	}
	/**
	 * <pre> 클립포인트와의 SFTP 연동을 위한 세팅 정보 조회 </pre>
	 * 
	 * @param 
	 * @return 호스트IP, 포트, 사용자계정, 비밀번호, 대상 폴더
	 * @throws IOException 
	 * @see
	 */		
	public Map<String,Object> getPropClip() throws IOException{
		String methodName = "getPropClip";
		
		Map<String,Object> resMap = null;

		try{
			String[] cmArr = sysPrmtManage.getSysPrmtAry("CP_CMPR_CONN_IP");
			
			resMap = new HashMap<String,Object>();
			resMap.put("cp_host", cmArr[0]);
			resMap.put("cp_port", cmArr[1]);
			resMap.put("cp_user", sysPrmtManage.getSysPrmtVal("CP_CMPR_CONN_ID"));
			resMap.put("cp_pwd",  sysPrmtManage.getSysPrmtVal("CP_CMPR_CONN_PW"));
			resMap.put("cp_dir",  sysPrmtManage.getSysPrmtVal("CP_CMPR_FILE_DIR"));
			log.debug("getPropClip",resMap.toString());
		}catch(Exception e){
			log.printStackTracePH(methodName, e);
		}		
		return resMap;
	}
	/**
	 * <pre> 시스템 현재일자 가져오기,년-월-일 시:분:초 </pre>
	 * 
	 * @param 
	 * @return 시스템 현재일자
	 * @see
	 */	
	public String getCurrentTime(){
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		return dateFormat.format(calendar.getTime());
	}
	/**
	 * <pre> 시스템 현재일자 가져오기 </pre>
	 * 
	 * @param 
	 * @return 시스템 현재일자
	 * @see
	 */	
	public String getCurrentDate(){
		String resDate = "";
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, -1); //현재일자에서 D-1
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		resDate = dateFormat.format(calendar.getTime());
		return resDate;
	}	
	/**
	 * <pre> 파일생성일자 가져오기 </pre>
	 * 
	 * @param 
	 * @return 파일생성일자
	 * @see
	 */		
	public String getFileBuildDate(){
		String resDate = "";
		Calendar calendar = Calendar.getInstance();		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		resDate = dateFormat.format(calendar.getTime());

		return resDate;		
	}
	
	/**
	 * <pre> 날짜 포멧 변환 </pre>
	 * 
	 * @param 
	 * @return 시스템 현재일자
	 * @see
	 */		
	public String getFormatDate(String param) throws ParseException{
		String resDate = "";
		Date to = new SimpleDateFormat("yyyyMMdd").parse(param);
		resDate = new SimpleDateFormat("yyyy-MM-dd").format(to);
		return resDate;
	}	
}
