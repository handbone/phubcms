package com.olleh.pHubCMS.common.scheduler.service;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.scheduler.dao.PHubDAO;

/**
 * 포인트허브 통계데이터 생성 Service
 * @Class Name : PHubStatService
 * @author lys
 * @since 2018.10.23
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.23   lys        최초생성
 * </pre>
 */
@Service
public class PHubStatService {
	private Logger log = new Logger(this.getClass());
	
	@Resource
	PHubDAO pHubDAO;
	
	
	/**
	 * <pre> [통계]통계 데이터 초기화 
	 * </pre>
	 * @param  통계일자(yyymmdd)
	 * @return 
	 * @see
	 */		
	public int initPHubStatData(Map<String,Object> params){
		log.debug("initPHubStatData", "통계 데이터 초기화");
		
		// declare
		int iSum  = 0;
		int iDeal = 0;
		int iDtl  = 0;
	    int iIF   = 0;
	    int iUse  = 0;
	    int iCprt = 0;
	    int iPnt  = 0;
	    int iUser = 0;
	    int iPUser = 0;
	    
	    // call
		// 거래내역 통계 데이터 삭제
		iDeal = pHubDAO.deleteStDealInfoByDealDd(params);
		
		// 거래내역상세 통계 데이터 삭제
		iDtl  = pHubDAO.deleteStDealDtlByDealDd(params);		
		
		// I/F 오류내역 통계 데이터 삭제
		iIF   = pHubDAO.deleteStIfErrInfoByIfDd(params);
		
		// 결제단계별 이용건수/이용자수 삭제
		iUse  = pHubDAO.deleteStUseByStepByDealDd(params);
		
		// 가맹점별 결제 실패 원인별 이용건수/이용자수 삭제
		iCprt = pHubDAO.deleteStPayErrByCprtByDealDd(params);
		
		// 포인트제공처별(카드사) 결제 실패 원인별 이용건수/이용자수 삭제
		iPnt = pHubDAO.deleteStPayErrByPntByDealDd(params);
		
		// 일별 이용자수 통계 삭제
		iUser = pHubDAO.deleteStDealReqUsersByDealDd(params);
		
		// 제공처별 거래 이용자수 통계 삭제
		iPUser = pHubDAO.deleteStDealReqPntUsersByDealDd(params);
		
		//합계
		iSum = iDeal + iDtl + iIF + iUse + iCprt + iPnt + iUser + iPUser;
		
		return iSum;
	}
	
	/**
	 * <pre> [통계]일별 거래내역 통계 생성 쿼리 
	 * </pre>
	 * @param  from 통계일자(yyymmdd), to 통계일자(yyymmdd), 작업자
	 * @return 등록건수
	 * @see
	 */		
	public int makeStDealInfoByDay(Map<String,Object> params){
		log.debug("makeStDealInfoByDay", "일별 거래내역 통계 생성");
		return pHubDAO.insertStDealInfoByDay(params);
	}
	
	/**
	 * <pre> [통계]일별 거래내역상세 통계 생성 쿼리 
	 * </pre>
	 * @param  from 통계일자(yyymmdd), to 통계일자(yyymmdd), 작업자
	 * @return 등록건수
	 * @see
	 */		
	public int makeStDealDtlByDay(Map<String,Object> params){
		log.debug("makeStDealDtlByDay", "일별 거래내역상세 통계 생성");
		return pHubDAO.insertStDealDtlByDay(params);
	}	
	
	/**
	 * <pre> [통계]일별 I/F오류내역 통계 생성 쿼리 
	 * </pre>
	 * @param  from 통계일자(yyymmdd), to 통계일자(yyymmdd), 작업자
	 * @return 등록건수
	 * @see
	 */		
	public int makeStIfErrInfoByDay(Map<String,Object> params){
		log.debug("makeStIfErrInfoByDay", "일별 I/F오류내역 통계 생성");
		return pHubDAO.insertStIfErrInfoByDay(params);
	}
	
	/**
	 * <pre> [통계]일별 결제단계별 이용건수/이용자수 통계 데이터 생성 쿼리 
	 * </pre>
	 * @param  from 통계일자(yyymmdd), to 통계일자(yyymmdd), 작업자
	 * @return 등록건수
	 * @see
	 */	
	public int makeStUseByStepByDay(Map<String,Object> params) {
		log.debug("makeStUseByStepByDay", "결제단계별 이용건수/이용자수 통계 데이터 생성");
		return pHubDAO.insertStUseByStepByDay(params);
	}
	
	/**
	 * <pre> [통계]일별 가맹점별 결제 실패 원인별 이용건수/이용자수 통계 데이터 생성 쿼리 
	 * </pre>
	 * @param  from 통계일자(yyymmdd), to 통계일자(yyymmdd), 작업자
	 * @return 등록건수
	 * @see
	 */
	public int makeStPayErrByCprtByDay(Map<String,Object> params) {
		log.debug("makeStPayErrByCprtByDay", "가맹점별 결제 실패 원인별 이용건수/이용자수 통계 데이터 생성");
		return pHubDAO.insertStPayErrByCprtByDay(params);
	}
	
	/**
	 * <pre> [통계]일별 포인트제공처별(카드사) 결제 실패 원인별 이용건수/이용자수 통계 데이터 생성 쿼리 
	 * </pre>
	 * @param  from 통계일자(yyymmdd), to 통계일자(yyymmdd), 작업자
	 * @return 등록건수
	 * @see
	 */
	public int makeStPayErrByPntByDay(Map<String,Object> params) {
		log.debug("makeStPayErrByPntByDay", "포인트제공처별(카드사) 결제 실패 원인별 이용건수/이용자수 통계 데이터 생성");
		return pHubDAO.insertStPayErrByPntByDay(params);
	}
	
	/**
	 * <pre> [통계]일별 이용자수 통계 데이터 생성 쿼리 
	 * </pre>
	 * @param  from 통계일자(yyymmdd), to 통계일자(yyymmdd), 작업자
	 * @return 등록건수
	 * @see
	 */
	public int makeStDealReqUsersByDay(Map<String, Object> params) {
		log.debug("makeStDealReqUsersByDay", "일별 이용자수 통계 데이터 생성");
		return pHubDAO.insertStDealReqUsersByDay(params);
	}
	
	/**
	 * <pre> [통계]제공처별 거래 이용자수 통계 데이터 생성 쿼리 
	 * </pre>
	 * @param  from 통계일자(yyymmdd), to 통계일자(yyymmdd), 작업자
	 * @return 등록건수
	 * @see
	 */
	public int makeStDealReqPntUsersByDay(Map<String, Object> params) {
		log.debug("makeStDealReqPntUsersByDay", "제공처별 거래 이용자수 통계 데이터 생성");
		return pHubDAO.insertStDealReqPntUsersByDay(params);
	}
}
