package com.olleh.pHubCMS.common.scheduler.task;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.scheduler.dao.PHubDAO;
import com.olleh.pHubCMS.scheduler.model.PHubToPGVO;

@Service
public class SaveResultTask {
	private Logger log = new Logger(this.getClass());
	
	@Resource
	PHubDAO pHubDAO;
	
	@Autowired
	MessageManage messageManage;
	
	/**
	 * <pre> PG대사처리 저장 </pre>
	 * 
	 * @param 거래내역 리스트, 파일전송Map
	 * @return 처리결과
	 * @see
	 */	
	public Map<String,Object> saveResult(Map<String,Object> dataMap){
		String methodName = "saveResult";
		
		Map<String,Object> resMap = new HashMap<String,Object>();
		
		Map<String,Object> chkMap = new HashMap<String,Object>();
		Map<String,Object> dtlMap = new HashMap<String,Object>();
		Map<String,Object> mstMap = new HashMap<String,Object>();
    	try{
    		chkMap.put("cmpr_dd",    String.valueOf(dataMap.get("cmpr_dd")));
    		
    		int iChk = this.getCountPG(chkMap);
    		
    		if(iChk > 0){
    				
    		dtlMap.put("dd_cmpr_no", String.valueOf(dataMap.get("dd_cmpr_no")));
    		dtlMap.put("user_id",    String.valueOf(dataMap.get("user_id")));
    		dtlMap.put("cmpr_dd",    String.valueOf(dataMap.get("cmpr_dd")));
    		
    		int iDtl = pHubDAO.insertPgCmprInfoD(dtlMap);
    		
    		mstMap.put("dd_cmpr_no",   String.valueOf(dataMap.get("dd_cmpr_no")));
    		mstMap.put("pg_cmpn_id",   String.valueOf(dataMap.get("pg_cmpn_id")));
    		mstMap.put("file_path",    String.valueOf(dataMap.get("file_path")));
    		mstMap.put("file_nm",      String.valueOf(dataMap.get("file_nm")));
    		mstMap.put("trtm_rslt_cd", String.valueOf(dataMap.get("trtm_rslt_cd")));
    		mstMap.put("rslt_msg",     String.valueOf(dataMap.get("rslt_msg")));
    		mstMap.put("user_id",      String.valueOf(dataMap.get("user_id")));
    		mstMap.put("cmpr_dd",      String.valueOf(dataMap.get("cmpr_dd")));
    		
    		int iMst = pHubDAO.insertPgCmprInfoM(mstMap);
	    		if((iDtl>0)&&(iMst>0)){
	    			resMap.put("rslt_code", messageManage.getMsgCd("SY_INFO_00"));
	    			resMap.put("rslt_msg",  messageManage.getMsgTxt("SY_INFO_00"));
	    			resMap.put("tgt_cnt",   iChk);
	    			resMap.put("cmpr_cnt",  iChk);
	    		}else{
	    			//db operation fail
	    			resMap.put("rslt_code", messageManage.getMsgCd("SY_ERROR_800"));
	    			resMap.put("rslt_msg",  messageManage.getMsgTxt("SY_ERROR_800"));  
	    			resMap.put("tgt_cnt",   0);
	    			resMap.put("cmpr_cnt",  0);
	    		}    		
    		}else{
    			//No Data Found
    			resMap.put("rslt_code", messageManage.getMsgCd("IF_INFO_104"));
    			resMap.put("rslt_msg",  messageManage.getMsgTxt("IF_INFO_104"));
    			resMap.put("tgt_cnt",   0);
    			resMap.put("cmpr_cnt",  0);
    		}

    	}catch(Exception e){
    		
    	}
    	
		return resMap;
	}	
	/**
	 * <pre> PG 대사파일 생성대상 조회 </pre>
	 * 
	 * @param  대사일자
	 * @return 조회수(CNT)
	 * @see
	 */		
	public int getCountPG(Map<String,Object> params){
		int iRes = 0;
		try{
			Map map = new HashMap();
			map = pHubDAO.selectCountPG(params);
			if((map!=null)&&(map.isEmpty()==false)){
				iRes = Integer.parseInt(String.valueOf(map.get("cnt")));
			}			
		}catch(Exception e){
			log.printStackTracePH("getCountPG", e);
		}
		return iRes;
	}
}
