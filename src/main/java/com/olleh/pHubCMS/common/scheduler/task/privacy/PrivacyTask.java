package com.olleh.pHubCMS.common.scheduler.task.privacy;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.olleh.pHubCMS.common.scheduler.dao.PrivacyDAO;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 개인정보 보호를 위한 데이타 이관 백업처리
 * @Class Name : PrivacyTask
 * @author cisohn
 * @since 2018.08.24
 * @version 0.1
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일        수 정 자       수정내용
 * ----------   --------   -----------------------------
 * 2019.04.11   cisohn         최초생성
 * </pre>
 */
@Service
public class PrivacyTask {

	@Autowired
	PrivacyDAO dao;
	
	/**
	 * <pre> [가족지원현황: fp_fmly_supot]
	 * </pre>
	 * @param  기준일(예:365 [1년간 미접속자 처리]
	 * @return 처리건수
	 * @see
	 */
	@Transactional(rollbackFor=Exception.class)
	public int backupFpFmlySupot(Map<String,Object> params){
		int i = dao.insertExFpFmlySupot(params);
		if(i>0) dao.deleteFpFmlySupot(params);
		
		return i; 
	}
	
	/**
	 * <pre> [mms전송이력: ph_mms_send_hist]  
	 * </pre>
	 * @param  기준일(예:365 [1년간 미접속자 처리]
	 * @return 처리건수
	 * @see
	 */
	@Transactional(rollbackFor=Exception.class)
	public int backupPhMmsSendHist(Map<String,Object> params){
		int i = dao.insertExPhMmsSendHist(params);
		if(i>0) dao.deletePhMmsSendHist(params);
		
		return i; 
	}
	
	/**
	 * <pre> [쿠폰상태변경이력: ph_cpn_chng_hist]  
	 * </pre>
	 * @param  기준일(예:365 [1년간 미접속자 처리]
	 * @return 처리건수
	 * @see
	 */
	@Transactional(rollbackFor=Exception.class)
	public int backupPhCpnChngHist(Map<String,Object> params){
		int i = dao.insertExPhCpnChngHist(params);
		if(i>0) dao.deletePhCpnChngHist(params);
		
		return i; 
	}
	
	/**
	 * <pre> [쿠폰발행정보: ph_cpn_isue_info]  
	 * </pre>
	 * @param  기준일(예:365 [1년간 미접속자 처리]
	 * @return 처리건수
	 * @see
	 */
	@Transactional(rollbackFor=Exception.class)
	public int backupPhCpnIsueInfo(Map<String,Object> params){
		int i = dao.insertExPhCpnIsueInfo(params);
		if(i>0) dao.deletePhCpnIsueInfo(params);
		
		return i; 
	}
	
	/**
	 * <pre> [거래원장상세: ph_deal_dtl]  
	 * </pre>
	 * @param  기준일(예:365 [1년간 미접속자 처리]
	 * @return 처리건수
	 * @see
	 */
	@Transactional(rollbackFor=Exception.class)
	public int backupPhDealDtl(Map<String,Object> params){
		int i = dao.insertExPhDealDtl(params);
		if(i>0) dao.deletePhDealDtl(params);
		
		return i; 
	}
	
	/**
	 * <pre> [거래원장: ph_deal_info]
	 * </pre>
	 * @param  기준일(예:365 [1년간 미접속자 처리]
	 * @return 처리건수
	 * @see
	 */
	@Transactional(rollbackFor=Exception.class)
	public int backupPhDealInfo(Map<String,Object> params){
		int i = dao.insertExPhDealInfo(params);
		if(i>0) dao.deletePhDealInfo(params);
		
		return i; 
	}
	
	/**
	 * <pre> [거래요청상세: ph_deal_req_dtl]  
	 * </pre>
	 * @param  기준일(예:365 [1년간 미접속자 처리]
	 * @return 처리건수
	 * @see
	 */
	@Transactional(rollbackFor=Exception.class)
	public int backupPhDealReqDtl(Map<String,Object> params){
		int i = dao.insertExPhDealReqDtl(params);
		if(i>0) dao.deletePhDealReqDtl(params);
		
		return i; 
	}
	
	/**
	 * <pre> [거래요청마스터: ph_deal_req_info]
	 * </pre>
	 * @param  기준일(예:365 [1년간 미접속자 처리]
	 * @return 처리건수
	 * @see
	 */
	@Transactional(rollbackFor=Exception.class)
	public int backupPhDealReqInfo(Map<String,Object> params){
		int i = dao.insertExPhDealReqInfo(params);
		if(i>0) dao.deletePhDealReqInfo(params);
		
		return i; 
	}
	
	/**
	 * <pre> [포인트고객CTN: ph_pnt_cust_ctn]
	 * </pre>
	 * @param  기준일(예:365 [1년간 미접속자 처리]
	 * @return 처리건수
	 * @see
	 */
	@Transactional(rollbackFor=Exception.class)
	public int backupPhPntCustCtn(Map<String,Object> params){
		int i = dao.insertExPhPntCustCtn(params);
		if(i>0) dao.deletePhPntCustCtn(params);
		
		return i; 
	}
	
	/**
	 * <pre> [고객정보: ph_pnt_cust_info]
	 * </pre>
	 * @param  기준일(예:365 [1년간 미접속자 처리]
	 * @return 처리건수
	 * @see
	 */
	@Transactional(rollbackFor=Exception.class)
	public int backupPhPntCustInfo(Map<String,Object> params){
		int i = dao.insertExPhPntCustInfo(params);
		if(i>0) dao.deletePhPntCustInfo(params);
		
		return i; 
	}
	
	/**
	 * <pre> [시스템사용자 정보: adm_user_info]
	 * </pre>
	 * @param  기준일(예:365 [1년간 미접속자 처리]
	 * @return 처리건수
	 * @see
	 */
	@Transactional(rollbackFor=Exception.class)
	public int backupAdmUserInfo(Map<String,Object> params){
		int i = dao.insertExAdmUserInfo(params);
		if (i>0) dao.deleteAdmUserInfo(params);
		
		return i; 
	}
	
	/**
	 * <pre> 배치마스터 seq 조회 </pre>
	 * 
	 * @param 
	 * @return String(배치마스터 키)
	 * @see
	 * 		ADM_BATCH_LOG_M.DD_CMPR_NO
	 * 		SEQ_BA_ID
	 */		
	public String getSeqBaId() throws Exception{
		String methodName = "getSeqBaId";
		String resStr = "";
		
		Map dataMap = dao.getSeqBaId();
		if(dataMap!=null){
			resStr = StringUtil.nvl(dataMap.get("LOG_NO"));
		}
		
		return resStr;
	}
	
}