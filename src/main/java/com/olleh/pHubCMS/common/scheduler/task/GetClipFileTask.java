package com.olleh.pHubCMS.common.scheduler.task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.common.components.SysPrmtManage;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.scheduler.utils.CommonUtil;
import com.olleh.pHubCMS.common.scheduler.utils.SFtpUtil;
/**
 * 원격 저장소에 있는 클립 대사 파일을 로컬저장소에 다운로드
 * @Class Name : GetClipFileTask
 * @author mason
 * @since 2018.08.24
 * @version 0.1
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.24   mason      최초생성
 * </pre>
 */
@Service
public class GetClipFileTask {
	private Logger log = new Logger(this.getClass());

	@Autowired
	CommonUtil cmmn;
	
	@Autowired
	SFtpUtil sFtpUtil;
	
	@Autowired
	SysPrmtManage sysPrmtManage;
	
	/**
	 * <pre> 원격 저장소에 있는 클립 대사 파일을 로컬저장소에 다운로드 </pre>
	 * 
	 * @param 대사일자
	 * @return 대사일자, 다운로드된 파일명 리스트
	 * @see
	 */		
	public List<Map<String,Object>> getClipFile(Map<String,Object> dataMap){
		String methodName = "getClipFile";
		List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();

		try{
    		//클립포인트와의 SFtp 연동을 위한 파라미터 조회
    		Map<String,Object> dat = cmmn.getPropClip();
    		//조회조건 세팅
    		Map<String,Object> params = new HashMap<String,Object>();
        	params.put("host",       dat.get("cp_host"));
        	params.put("port",       Integer.parseInt(dat.get("cp_port").toString()));
        	params.put("user",       dat.get("cp_user"));
        	params.put("password",   dat.get("cp_pwd"));
        	params.put("dir",        dat.get("cp_dir"));
        	params.put("deal_dd",    dataMap.get("deal_dd"));
        	params.put("local_path", dataMap.get("local_path"));
    		log.debug(methodName, params.toString());
    		dataList = sFtpUtil.getFile(params);

    	}catch(Exception e){
    		log.printStackTracePH(methodName, e);
    	}
    	
		return dataList;
	}
	
	public Map<String,Object> delFile(String[] fileName){
		String methodName = "delFile";
		
		Map<String,Object> resMap = new HashMap<String,Object>();
		try{
			//클립포인트와의 SFtp 연동을 위한 파라미터 조회
    		Map<String,Object> dat = cmmn.getPropClip();
    		//조회조건 세팅
    		Map<String,Object> reqMap = new HashMap<String,Object>();
    		reqMap.put("host",       dat.get("cp_host"));
    		reqMap.put("port",       Integer.parseInt(dat.get("cp_port").toString()));
    		reqMap.put("user",       dat.get("cp_user"));
    		reqMap.put("password",   dat.get("cp_pwd"));
    		reqMap.put("destDir",    dat.get("cp_dir"));
    		reqMap.put("fileName",   fileName);
    		
    		resMap = sFtpUtil.delFiles(reqMap);
		}catch(Exception e){
			log.printStackTracePH(methodName, e);
		}
		return resMap;
	}
}
