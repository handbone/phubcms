package com.olleh.pHubCMS.common.scheduler.job;

import java.io.File;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.exception.UserException;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.ErrorInfoVO;
import com.olleh.pHubCMS.common.scheduler.dao.PHubDAO;
import com.olleh.pHubCMS.common.scheduler.service.CmprClipPointService;
import com.olleh.pHubCMS.common.scheduler.service.CmprCommService;
import com.olleh.pHubCMS.common.scheduler.task.GetClipFileTask;
import com.olleh.pHubCMS.common.scheduler.task.GetClipParsingTask;
import com.olleh.pHubCMS.common.scheduler.task.SaveClipResultTask;
import com.olleh.pHubCMS.common.scheduler.utils.CommonUtil;
import com.olleh.pHubCMS.common.utils.CommonUtils;
import com.olleh.pHubCMS.common.utils.DateUtils;
import com.olleh.pHubCMS.common.utils.JsonUtil;
import com.olleh.pHubCMS.common.utils.StringUtil;
/**
 * 클립포인트에서 포인트허브로의 대사처리 job
 * @Class Name : ClipToPHubJob
 * @author mason
 * @since 2018.08.22
 * @version 0.1
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.21   mason      최초생성
 * </pre>
 */
@Service("clipToPHubJob")
public class ClipToPHubJob {
	private Logger log = new Logger(this.getClass());
	
	@Resource
	PHubDAO pHubDAO;
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
	CommonUtil commonUtil;
	
	@Autowired
	SaveClipResultTask saveClipResultTask;
	
	@Autowired
	GetClipParsingTask getClipParsingTask;
	
	@Autowired
	GetClipFileTask getClipFileTask;
	
	@Autowired
	CmprClipPointService cmprClipPointService;
	
	@Autowired
	CmprCommService cmprCommService;
	

	
	/* 작업 Task 선언 */
	//primessageManagekIndClipTask workIndTask;    //대사처리여부 체크
	//private GetClipFileTask    clipFileTask;   //원격저장소 파일 다운로드
	//private GetClipParsingTask clipParseTask;  //로컬저장소 파일 파싱 및 저장
	//private SaveClipResultTask clipResultTask; //대사결과 저장
	
	/**
	 * <pre> job 실행 함수 </pre>
	 * 
	 * @param Job 실행 콘텍스트
	 * @return void
	 * @see
	 */		
	public void execute() throws JobExecutionException {
		//
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("cmprDd", commonUtil.getCurrentDate());
		param.put("userId", "btch");
		cmprProc(param);
	}
	
	/**
	 * <pre> 일대사 처리 </pre>
	 * 
	 * @param Job 실행 콘텍스트
	 * @return void
	 * @see
	 */	
	public void cmprProc(Map<String,Object> params) {
		String methodName = "cmprProc";
		log.debug(methodName, "Start cmprProc! (params=" + JsonUtil.toJson(params) + ")");
		
		//현재 실행중인 함수
		String stepCd = "0";
		String strDt  = "";	
		
		// set valiable
		String wrknNm       = this.getClass().getSimpleName();	// 배치작업명
		String ddCmprNo     = "";								// 일대사번호
		String cmprDd       = StringUtil.nvl(params.get("cmprDd"), commonUtil.getCurrentDate());	// 대사일(yyyymmdd) : 현재일자 기준 -1일이 기준일자
		String userId       = StringUtil.nvl(params.get("userId"), "btch");							// 작업자
		String ptHbDateTime = ""; 							//파일생성일시
		//log.debug(methodName,"cmprDd : " + runCmprDd);
		
		int tgtCnt       = 0;		// 원대상건수
		int cmprCnt      = 0;		// 대사결과 건수
		int ttlDealCnt   = 0;		// 총거래 건수
		int noAcrCnt     = 0;		// 성공건수
		int mtchCnt      = 0;		// 실패건수
		String rsltCodeV = "";		// 결과코드  value
		String rsltMsgV  = "";		// 결과메시지 value
		
		// 파일맵
		Map<String, Object> fileMap = new HashMap<String, Object>();
		
		/* -----Set Parameter ---------------------------------------------------------------------------- */
		int idx = 0;
		Map<Integer, String> batchProcMap = new HashMap<Integer, String>();
		batchProcMap.put(idx++, "1.InitClipCmprTask");
		batchProcMap.put(idx++, "2.GetClipFileTask");
		batchProcMap.put(idx++, "3.GetClipParsingTask");
		batchProcMap.put(idx++, "4.SaveClipResultTask");		
		batchProcMap.put(idx++, "5.PuchCnclResultTask");
		
		// 대사진행단계
		int cmprProcStep = 0;
		
		try {			
			// 1. 일대사번호 추출
			stepCd   = "1";
			ddCmprNo = cmprClipPointService.getDdCmprNo();
			
			// 에러 처리 해야 한다.
			if( "".equals(ddCmprNo) ) {
				log.debug(methodName,"일대사번호 추출 오류");
				throw new UserException(stepCd, messageManage.getMsgVOY("IF_INFO_104"));
			}
			
			/* -----INSERT INTO ADM_BATCH_LOG_M -------------------------------------------------------------- */
			//3. 로그 마스터 생성
			Map<String,Object> logMstMap = new HashMap<String,Object>();
			logMstMap.put("dd_cmpr_no", ddCmprNo);
			logMstMap.put("cmpr_dd",    cmprDd);
			logMstMap.put("wrkn_nm",    wrknNm);
			logMstMap.put("user_id",    userId);
			int iLogM = cmprCommService.saveAdmBatchLogM(logMstMap);

			// 단계별 대사 처리
			for(int i=0; i < idx; i++) {
				// init
				cmprProcStep++;
				strDt = DateUtils.defaultDate() + DateUtils.defaultTime();
				
				// 대사결과 셋팅 (성공)
				rsltCodeV = messageManage.getMsgCd("SY_INFO_00");
				rsltMsgV  = messageManage.getMsgTxt("SY_INFO_00");
				
				tgtCnt  = 1;
				cmprCnt = 1;
				
			    switch(i) {
		        case 0 : 
					// 1. 해당 대사일자의 기존 대사데이터 모두 삭제 후 재등록
					Map<String,Object> initMap = new HashMap<String,Object>();
					initMap.put("cmpr_dd", cmprDd);
					int iVal = cmprClipPointService.setClipCmprInit(initMap);
					
					/* -----INSERT INTO CL_CMPR_INFO ----------------------------------------------------------------- */
					//2. 클립포인트일대사결과M 생성
					Map<String,Object> mst = new HashMap<String,Object>();
					mst.put("dd_cmpr_no", ddCmprNo);
					mst.put("cmpr_dd",    cmprDd);
					mst.put("user_id",    userId);
					int iMst = cmprClipPointService.saveClCmprInfo(mst);
					log.debug(methodName,"INSERT CL_CMPR_INFO iMst="+iMst);					
					break;
					
		        case 1 : 
		        	// 2.GetClipFileTask
					/* ----------------------------------------------------------------------------------- */
					//2.원격저장소에 있는 클립포인트 대사 파일을 로컬저장소에 다운로드					
					Map<String,Object> paramMap = new HashMap<String,Object>();
					paramMap.put("deal_dd", cmprDd); 
					paramMap.put("local_path", commonUtil.getClCmprTgtDir());
					
					// 파일맵객체에 저장 : 다음단계에서도 공유하여 사용 한다.
					fileMap = cmprClipPointService.getClipFile(paramMap);
					/* ----------------------------------------------------------------------------------- */
					
					if( !"".equals(CommonUtils.isEmpty(fileMap, "")) && !"".equals(StringUtil.nvl(fileMap.get("file_name"))) ) {
						/* --------------------------------------------------------------------------------*/
						//2.원격저장소에 있는 클립포인트 대사 파일을 로컬저장소에 다운로드 로그 끝
						log.debug(methodName, "원격저장소에 있는 대사 파일을 로컬저장소로 다운로드 성공! ");

					}else{
						/* ------------------------------------------------------------------------------- */
						//2.원격저장소에 있는 클립포인트 대사 파일을 로컬저장소에 다운로드 실패
						log.error(methodName, "원격저장소에 대사파일이 존재하지 않음.");
						throw new UserException(stepCd, messageManage.getMsgVOY("CM_ERROR_603"));
					}
					break;
					
		        case 2 : 
		            // 3.GetClipParsingTask
					//3.로컬저장소 파일 파싱 및 저장
					Map<String,Object> paramMap2 = new HashMap<String, Object>();
					paramMap2.put("dd_cmpr_no", ddCmprNo);
					paramMap2.put("cmpr_dd",    cmprDd);
					paramMap2.put("deal_dd",    cmprDd);
					paramMap2.put("file_name",  fileMap.get("file_name"));
					paramMap2.put("full_path",  fileMap.get("full_path"));
					
					// 파일파싱처리
					Map parseMap = cmprClipPointService.getClipParsing(paramMap2, userId);
					log.debug("resMap2", "getClipParsing:"+parseMap.toString());
					/* ----------------------------------------------------------------------------*/
					cmprCnt    = Integer.parseInt(StringUtil.nvl(parseMap.get("cmprCnt"), "0"));
					ttlDealCnt = cmprCnt;
					rsltCodeV  = StringUtil.nvl(parseMap.get("retcode"));
					rsltMsgV   = StringUtil.nvl(parseMap.get("retmsg"));					
					
					if(parseMap.get("retcode").equals("00")){
						//클립포인트일대사결과M 업데이트
						ptHbDateTime = String.valueOf(parseMap.get("pt_hb_date_time"));
					}else{
						log.error(methodName, "파일데이터 파싱처리 실패!");
						throw new UserException(wrknNm, methodName, stepCd, rsltCodeV, rsltMsgV);
					}
					break;
					
		        case 3 : 
		            // 4.SaveClipResultTask 
					//4.대사결과 저장
		        	// set
					Map<String,Object> paramMap3 = new HashMap<String, Object>();	        	
					paramMap3.put("dd_cmpr_no", ddCmprNo);
					paramMap3.put("user_id",    userId);
					paramMap3.put("deal_dd",    cmprDd);
					paramMap3.put("file_name",  fileMap.get("file_name"));
					paramMap3.put("full_path",  fileMap.get("full_path"));
					paramMap3.put("cmprCnt",    ttlDealCnt);
					
					// 처리
					Map<String, Object> resMap3 = cmprClipPointService.saveClipResult(paramMap3);
					log.debug(methodName, "4.대사결과 저장:"+JsonUtil.toJson(resMap3));
					if(resMap3.get("retcode").equals("00")){
						// 파일삭제
						moveFile(StringUtil.nvl(fileMap.get("file_name")));
					}
					
					/* ------------------------------------------------------------------------*/
					//4.대사결과 저장 로그 끝
					rsltCodeV = StringUtil.nvl(resMap3.get("retcode"));
					rsltMsgV  = StringUtil.nvl(resMap3.get("retmsg"));
	
					tgtCnt    = Integer.parseInt(StringUtil.nvl(resMap3.get("tgt_cnt"), "0"));
					cmprCnt   = Integer.parseInt(StringUtil.nvl(resMap3.get("cmpr_cnt"), "0"));
					noAcrCnt  = Integer.parseInt(StringUtil.nvl(resMap3.get("no_acr_cnt"), "0"));
					mtchCnt   = Integer.parseInt(StringUtil.nvl(resMap3.get("mtch_cnt"), "0"));
						
					/* ------------------------------------------------------------------------*/
					//로그마스터 업데이트
					ttlDealCnt = Integer.parseInt(StringUtil.nvl(resMap3.get("cmpr_cnt"), "0"));
					log.debug(methodName, "클립포인트 대사처리 완료!");
					break;
				
		        case 4 :
		        	//5.PuchasCnclResultTask 
		        	//5.매입취소 처리
		        	Map<String,Object> paramMap4 = new HashMap<String, Object>();	        	
		        	paramMap4.put("dd_cmpr_no", ddCmprNo);
		        	paramMap4.put("user_id", userId);
		        	
		        	Map<String, Object> resMap4 = cmprClipPointService.puchasCnclResult(paramMap4);
		        	log.debug(methodName, "5.대사결과 저장:"+JsonUtil.toJson(resMap4));
		        	
		        	/* ------------------------------------------------------------------------*/
		        	//5.대사결과 저장 로그 끝
		        	rsltCodeV = StringUtil.nvl(resMap4.get("retcode"));
		        	rsltMsgV  = StringUtil.nvl(resMap4.get("retmsg"));
		        	
		        	tgtCnt    = Integer.parseInt(StringUtil.nvl(resMap4.get("tgt_cnt"), "0"));
					cmprCnt   = Integer.parseInt(StringUtil.nvl(resMap4.get("cmpr_cnt"), "0"));
					/* ------------------------------------------------------------------------*/
		        	
		        	log.debug(methodName, "매입취소 처리 완료!");
		        	
		        	break;
					
		        default :
		        	log.debug(methodName, "default!");
			    }
			    
			    // 단계별 대사결과 log 저장 (startDt, endDt 셋팅해줘야 한다.)
			    Map<String,Object> logMap = cmprCommService.getLogMap(ddCmprNo, cmprDd, batchProcMap.get(i), rsltCodeV, rsltMsgV, userId);
				logMap.put("tgt_cnt",  tgtCnt);
				logMap.put("cmpr_cnt", cmprCnt);
				logMap.put("strt_dt",  strDt);
				// INSERT ADM_BATCH_LOG
				cmprCommService.insertAdmBatchLog(logMap);
				
				if( !"00".equals(rsltCodeV) ) {
					// 실패
					break;
				}
			}
			// End For Loop
			/////////////////////////////////////////
			// for문이 끝나면 로그마스터(성공 및 실패? - 루프문안에서 다 했는데?) 및 일대사마스터?/클립포인트일대사결과M를 update 해줘야 한다. - lys
			/* --------------------------------------------------------------------------------------- */
			// 대사처리단계 flag 초기화
			cmprProcStep = 0;
			
		} catch (UserException e) {
			ErrorInfoVO errorInfoVO = e.getErrorInfo();
			rsltCodeV = errorInfoVO.getErrCd();
			rsltMsgV  = errorInfoVO.getErrMsg();
			
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
			rsltCodeV = messageManage.getMsgCd("SY_ERROR_900");
			rsltMsgV  = messageManage.getMsgTxt("SY_ERROR_900");
			
		} finally {
			// 배치 로그마스터 업데이트
			Map<String,Object> logMstMap = new HashMap<String,Object>();
			logMstMap.put("dd_cmpr_no",   ddCmprNo);
			logMstMap.put("cmpr_dd",      cmprDd);
			logMstMap.put("wrkn_nm",      wrknNm);
			logMstMap.put("user_id",      userId);
			logMstMap.put("ttl_deal_cnt", ttlDealCnt);
			logMstMap.put("no_acr_cnt",   noAcrCnt);
			logMstMap.put("mtch_cnt",     mtchCnt);
			logMstMap.put("doc_file_nm",  StringUtil.nvl(fileMap.get("file_name")));
			logMstMap.put("rslt_code",    rsltCodeV);
			logMstMap.put("rslt_msg",     rsltMsgV);
			// UPDATE ADM_BATCH_LOG_M
		    cmprCommService.updateAdmBatchLogM(logMstMap);
		    
			// 클립포인트일대사결과M 업데이트
			Map<String,Object> mst = new HashMap<String,Object>();
			mst.put("dd_cmpr_no", ddCmprNo);
			mst.put("cmpr_dd",    cmprDd);
			mst.put("user_id",    userId);				
			mst.put("pt_hb_date_time", ptHbDateTime);
			mst.put("doc_file_nm",     StringUtil.nvl(fileMap.get("file_name")));
			// UPDATE CL_CMPR_INFO
			cmprClipPointService.updateClCmprInfo(mst);		    
		    
			// 실패했을때만 일대사마스터에 업데이트 한다. 정상처리 됬을때는 4단계에서 update 처리 한다.
			if( !"00".equals(rsltCodeV) ) {

				
				// 배치로그 등록
				if( cmprProcStep > 0 ) {
				    Map<String,Object> logMap = cmprCommService.getLogMap(ddCmprNo, cmprDd, batchProcMap.get(cmprProcStep-1), rsltCodeV, rsltMsgV, userId);
					logMap.put("tgt_cnt",  tgtCnt);
					logMap.put("cmpr_cnt", cmprCnt);
					logMap.put("strt_dt",  strDt);
					// INSERT ADM_BATCH_LOG
					cmprCommService.insertAdmBatchLog(logMap);					
				}
			}
		}
		
		log.debug(methodName, "End cmprProc! (rsltCodeV=" + rsltCodeV + ")");
	}


	
	public Map<String,Object> delFile(String[] fileName){
		return getClipFileTask.delFile(fileName);
	}

	/**
	 * <pre> SFTP가 정상적으로 전송 완료되면 해당 파일을 백업 폴더로 이동 </pre>
	 * 
	 * @param 
	 * @return 처리결과
	 * @see
	 */		
	public int moveFile(String fileName){
		int iRes = 0;
		String srcFullPath = "";
		String tgtFilePath = "";
		Map<String,Object> resMap = new HashMap<String,Object>();
		try{
			srcFullPath = commonUtil.getClipFileBaseDir() + File.separator + fileName;//
			tgtFilePath = commonUtil.getClipFileBackDir() + File.separator + fileName;
			
			Path from = Paths.get(srcFullPath);
			Path to   = Paths.get(tgtFilePath);
			
			FileChannel fromChannel = FileChannel.open(from, StandardOpenOption.READ);
			FileChannel toChannel   = FileChannel.open(to,   StandardOpenOption.CREATE,StandardOpenOption.WRITE);
			
			ByteBuffer buffer = ByteBuffer.allocate(100);
			
			int byteCount = 0;
			
			while(true){
				buffer.clear();
				byteCount = fromChannel.read(buffer);
				
				if(byteCount==-1)
					break;
				
				buffer.flip();
				toChannel.write(buffer);
			}
			
			fromChannel.close();
			toChannel.close();
			
			//File fileOri = new File(fileName);
			File fileFrm = new File(srcFullPath);
			File fileTo  = new File(tgtFilePath);
			boolean bTo  = fileTo.exists();
			if(bTo==true){
				log.debug("moveFile", "기존 작업 파일 백업 폴더로 복사 성공");

				if(fileFrm.delete()){
					log.debug("moveFile", "기존 작업 파일삭제 성공");
					// 실제파일, 체크파일 삭제 (yyyymmdd_POINTHUB, yyyymmdd_POINTHUB.END)
					String[] delFileNames = {fileName, fileName + ".END"};
					resMap = this.delFile(delFileNames);
					log.debug("moveFile", "resMap:"+resMap.toString());					
					iRes = 1;
				}else{
					log.debug("moveFile", "File Not Found");
				}
			}else{
				log.debug("moveFile", "기존 작업 파일 백업 폴더로 복사 실패");
			}
			//log.debug("moveFile", f.exists()+","+f.getAbsolutePath());
		}catch(Exception e){
			log.printStackTracePH("moveFile", e);
		}
		
		return iRes;
	}
}
