package com.olleh.pHubCMS.common.scheduler.task;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.scheduler.dao.PHubDAO;
import com.olleh.pHubCMS.common.scheduler.utils.CommonUtil;
import com.olleh.pHubCMS.common.utils.StringUtil;
import com.olleh.pHubCMS.scheduler.model.PHubToPGVO;

/**
 * 대사파일 생성
 * @Class Name : MakeCmprFileTask
 * @author mason
 * @since 2018.08.21
 * @version 0.1
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.21   mason      최초생성
 * </pre>
 */
@Service
public class MakeCmprFileTask {
	private Logger log = new Logger(this.getClass());
	
	@Resource
	PHubDAO pHubDAO;
	
	@Autowired
	MessageManage messageManage;	
	
	@Autowired
	CommonUtil cmmn;
	
	/**
	 * <pre> 조회된 거래내역 리스트로 대사용 파일 생성 </pre>
	 * 
	 * @param 거래내역 리스트
	 * @return 대사 파일명
	 * @see
	 */		
	public Map<String,Object> makeFile(Map<String,Object> params) throws Exception{
		String methodName = "makeFile";
		
		Map<String,Object> resMap = new HashMap<String,Object>();
		
		String ddCmprNo = "";
		String cmprDd   = "";
		String pgFileBaseDir = "";
		String fileName = "";
		String fullPathFile = "";
		List<PHubToPGVO> dataList = new ArrayList<PHubToPGVO>();
		
		PrintWriter pw  = null;
		PrintWriter pw2 = null;
		PrintWriter pw3 = null;
		
		StringBuffer headStr = new StringBuffer();
		StringBuffer bodyStr = new StringBuffer();
		StringBuffer tailStr = new StringBuffer();
		
		PHubToPGVO vo1 = new PHubToPGVO();
		PHubToPGVO vo2 = new PHubToPGVO();
		PHubToPGVO vo3 = new PHubToPGVO();
		
		try{
			log.debug(methodName, "거래내역조회:"+params.toString());
			dataList = pHubDAO.getPgCmprInfo(params);
			
			ddCmprNo = String.valueOf(params.get("dd_cmpr_no"));
			cmprDd   = String.valueOf(params.get("cmpr_dd"));
			pgFileBaseDir = cmmn.getPgFileBaseDir();			
			fileName = String.valueOf(params.get("file_nm"));
			log.debug(methodName, "pgFileBaseDir:"+pgFileBaseDir);
			fullPathFile = pgFileBaseDir + File.separator+fileName;
			log.debug(methodName, "fullPathFile:"+fullPathFile);
			if((dataList!=null)&&(dataList.isEmpty()==false)){
				//data exists
				//헤더
				vo1 = dataList.get(0);
//				pw = new PrintWriter(fileName);
				pw = new PrintWriter(fullPathFile);
				
				headStr.append(vo1.getPtHbVl1())
			           .append(vo1.getPtHbVer())
			           .append(vo1.getPtHbCmprNo())
    		           .append(vo1.getPtHbCmprDd())
    		           .append(vo1.getPtHbFer1()); 
    		
				pw.println(headStr.toString());
				if (pw != null) {
					pw.close();
				}
				//바디
				try {
//					pw2 = new PrintWriter(new FileWriter(fileName,true));
					pw2 = new PrintWriter(new FileWriter(fullPathFile,true));
					String oriStr = "";
					for(int i = 0 ; i < dataList.size() ; i++){
						vo2 = dataList.get(i);
						bodyStr = new StringBuffer();
						//sql상으로는 분명히 space가 들어갔는데, 실제 리턴되는 값은 empty space상태
						if(vo2.getPtHbUqeN().length()==0){
							oriStr = this.pad(vo2.getPtHbUqeN(), ' ', 16, true);
						}else{
							oriStr = vo2.getPtHbUqeN();
						}
//						log.debug("vo2", "getPtHbUqeN:"+oriStr.length()+",012345678901234567890");
//						log.debug("vo2", "getPtHbUqeN:"+oriStr.length()+","+oriStr+"|");
						bodyStr.append(vo2.getPtHbVl2())
						       .append(vo2.getPtHbSeqnum())
						       .append(vo2.getPtHbDealInd())
						       .append(vo2.getPtHbUqeNo())
						       .append(vo2.getPtHbOthNo())
						       .append(vo2.getPtHbGoodsPrice())
						       .append(vo2.getPtHbRiPnt())
						       .append(vo2.getPtHbRmndAmt())
						       .append(oriStr)
						       .append(vo2.getPtHbShopCd())
						       .append(vo2.getPtHbFer2());
						pw2.println(bodyStr.toString());
						pw2.flush();
					}
										
				} catch(IOException e) {
					//
					log.debug(methodName, "[IOException]pw2="+StringUtil.nvl(e.getMessage()));
				} finally {
					if (pw2 != null) {
						pw2.close();
					}
				}

				//테일
				vo3 = dataList.get(0);
//				pw3 = new PrintWriter(new FileWriter(fileName,true));
				pw3 = new PrintWriter(new FileWriter(fullPathFile,true));
				tailStr.append(vo3.getPtHbVl3())
 		               .append(vo3.getPtHbCt())
 		               .append(vo3.getPtHbDateTime())
 		               .append(vo3.getPtHbFer3());

				pw3.print(tailStr.toString());
				if (pw3 != null) {
					pw3.close();
				}
			}else{
				//no data found
				//헤더
				pw = new PrintWriter(fullPathFile);
				headStr.append("HD")
				       .append("SB01")
				       .append(ddCmprNo)
				       .append(cmprDd)
				       .append(this.pad("", ' ', 474, true));
				pw.println(headStr.toString());
				if (pw != null) {
					pw.close();
				}
				//테일
				try {
					pw3 = new PrintWriter(new FileWriter(fullPathFile,true));
					tailStr.append("TR")
					       .append(this.pad("", '0', 10, true))
					       .append(cmprDd)
					       .append("060000")
					       .append(this.pad("", ' ', 474, true));
					pw3.print(tailStr.toString());
										
				} catch(IOException e) {
					log.debug(methodName, "[IOException]pw3="+StringUtil.nvl(e.getMessage()));
				} finally {
					if (pw3 != null) {
						pw3.close();
					}
				}
			}
			File f = new File(fullPathFile);
			log.debug(methodName, "파일생성여부:"+f.exists()+",filename:"+f.getName());
			resMap.put("tgt_file_path", pgFileBaseDir);
			resMap.put("cmpr_file_path", pgFileBaseDir);
			resMap.put("full_file_nm", fullPathFile);
			resMap.put("file_nm",      fileName);
			resMap.put("data_rows",    dataList.size());
			resMap.put("file_exists",  f.exists());
		}catch(Exception e){
			log.printStackTracePH(methodName, e);
			resMap.put("tgt_file_path", pgFileBaseDir);
			resMap.put("cmpr_file_path", pgFileBaseDir);
			resMap.put("full_file_nm", "");
			resMap.put("file_nm",      "");
			resMap.put("data_rows",    0);
			resMap.put("file_exists",  false);
		} finally {
			if (pw2 != null) pw2.close();
			if (pw3 != null) pw3.close();
		}
		return resMap;
	}
	/**
	 * <pre> PG 대사파일 생성일자(D-Day) </pre>
	 * 
	 * @param
	 * @return String 클립포인트 대사파일 생성일자(D-Day)
	 * @see
	 */
	public String getFileBuildDate(){
		String resDate = "";
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		resDate = dateFormat.format(calendar.getTime());
		return resDate;
	}		
	/**
	 * <pre> 문자열 padding처리 </pre>
	 * 
	 * @param source(원본 문자열), fill(패딩처리시 채워질 문자), length(길이), right(true:left padding,false:right padding)
	 * @return 문자열 padding처리
	 * @see
	 */	
	public String pad(String source,char fill, int length, boolean right){
		if(source.length()>length) return source;
		char[] out = new char[length];
		if(right){
			 System.arraycopy(source.toCharArray(), 0, out, 0, source.length());
		        Arrays.fill(out, source.length(), length, fill);
		}else{
	        int sourceOffset = length - source.length();
	        System.arraycopy(source.toCharArray(), 0, out, sourceOffset, source.length());
	        Arrays.fill(out, 0, sourceOffset, fill);
	    }
	    return new String(out);
	}
}
