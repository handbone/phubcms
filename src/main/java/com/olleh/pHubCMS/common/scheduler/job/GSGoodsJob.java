package com.olleh.pHubCMS.common.scheduler.job;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.common.components.Constant;
import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.exception.UserException;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.ErrorInfoVO;
import com.olleh.pHubCMS.common.scheduler.service.CmprCommService;
import com.olleh.pHubCMS.common.scheduler.task.gsGoods.GSGoodsTask;
import com.olleh.pHubCMS.common.scheduler.utils.CommonUtil;
import com.olleh.pHubCMS.common.utils.DateUtils;
import com.olleh.pHubCMS.common.utils.JsonUtil;
import com.olleh.pHubCMS.common.utils.StringUtil;
/**
 * 기프티쇼 상품정보 현행화 job
 * @Class Name : GSGoodsJob
 * @author hwlee
 * @since 2019.04.10
 * @version 0.1
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2019.04.10   hwlee      최초생성
 * </pre>
 */
@Service("gsGoodsJob")
public class GSGoodsJob {
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
	CommonUtil commonUtil;
	
	@Autowired
	GSGoodsTask gsGoodsTask;
	
	@Autowired
	CmprCommService cmprCommService;
	

	/**
	 * <pre> job 실행 함수 </pre>
	 * 
	 * @param Job 실행 콘텍스트
	 * @return void
	 * @see
	 */		
	public void execute() throws JobExecutionException {
		//
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("cmprDd", commonUtil.getCurrentDate());
		param.put("userId", "btch");
		goodsProc(param);
	}
	
	/**
	 * <pre> 상품 정보 리스트 조회 </pre>
	 * 
	 * @param Job 실행 콘텍스트
	 * @return void
	 * @see
	 */	
	public void goodsProc(Map<String,Object> params) {
		String methodName = "statProc";
		log.debug(methodName, "Start statProc! (params=" + JsonUtil.toJson(params) + ")");
		
		//현재 실행중인 함수
		String stepCd = "0";
		String strDt  = "";	
		
		// set valiable
		String wrknNm       = this.getClass().getSimpleName();	// 배치작업명
		String ddCmprNo     = "";								// 일대사번호
		String cmprDd       = StringUtil.nvl(params.get("cmprDd"), commonUtil.getCurrentDate());	// 대사일(yyyymmdd) : 현재일자 기준 -1일이 기준일자
		String userId       = StringUtil.nvl(params.get("userId"), "btch");							// 작업자
		//log.debug(methodName,"cmprDd : " + runCmprDd);
		
		int tgtCnt       = 0;		// 원대상건수
		int cmprCnt      = 0;		// 대사결과 건수
		int ttlDealCnt   = 0;		// 총거래 건수
		String rsltCodeV = "";		// 결과코드  value
		String rsltMsgV  = "";		// 결과메시지 value
		
		/* -----Set Parameter ---------------------------------------------------------------------------- */
		int idx = 0;
		Map<Integer, String> batchProcMap = new HashMap<Integer, String>();
		batchProcMap.put(idx++, "1.InitTmpData");				// 상품 정보 tmp 테이블 현행화
		batchProcMap.put(idx++, "2.InitGoodsData");				// 상품 정보 테이블 현행화
		
		// 대사진행단계
		int cmprProcStep = 0;
		
		try {
			// DECLARE
			Map<String,Object> paramMap = new HashMap<String,Object>();
			
			// 1. 일대사번호 추출
			stepCd   = "1";
			ddCmprNo = cmprCommService.getSeqCmId();
			
			// 에러 처리 해야 한다.
			if( "".equals(ddCmprNo) ) {
				log.debug(methodName,"일대사번호 추출 오류");
				throw new UserException(stepCd, messageManage.getMsgVOY("IF_INFO_104"));
			}
			
			/* -----INSERT INTO ADM_BATCH_LOG_M -------------------------------------------------------------- */
			//3. 로그 마스터 생성
			Map<String,Object> logMstMap = new HashMap<String,Object>();
			logMstMap.put("dd_cmpr_no", ddCmprNo);
			logMstMap.put("cmpr_dd",    cmprDd);
			logMstMap.put("wrkn_nm",    wrknNm);
			logMstMap.put("user_id",    userId);
			int iLogM = cmprCommService.saveAdmBatchLogM(logMstMap);

			Map<String, Object> resultMap = new HashMap<String, Object>();
			// 단계별 대사 처리
			for(int i=0; i < idx; i++) {
				// init
				cmprProcStep++;
				strDt = DateUtils.defaultDate() + DateUtils.defaultTime();
				
				// 대사결과 셋팅 (성공)
				rsltCodeV = messageManage.getMsgCd("SY_INFO_00");
				rsltMsgV  = messageManage.getMsgTxt("SY_INFO_00");
				
				tgtCnt  = 1;
				cmprCnt = 1;
				
			    switch(i) {
		        case 0 : 
					// 1. 상품 정보 tmp 테이블 현행화
					resultMap = gsGoodsTask.initTmpData();
					String rstCd = StringUtil.nvl(resultMap.get("rst_cd"));
					String rstMsg = StringUtil.nvl(resultMap.get("rst_msg"));
					
					//기프티쇼 결과코드가 성공이 아닐 경우 코드, 메시지 변경
					if(!"0000".equals(rstCd)){
						rsltCodeV = rstCd;
						rsltMsgV  = rstMsg;
					}
					log.debug(methodName,"initTmpData rstCd : " + rstCd);
					log.debug(methodName,"initTmpData rstMsg : " + rstMsg);
					break;
		        case 1 : 
		        	// 2. 상품 정보 테이블 현행화
		        	// 첫번째 Task 성공일 경우에만 동작
		        	if("00".equals(rsltCodeV)){
		        		gsGoodsTask.initGoodsData();
		        	}
					break;
		        default :
		        	log.debug(methodName, "default!");
			    }
			    
			    // 단계별 대사결과 log 저장 (startDt, endDt 셋팅해줘야 한다.)
			    Map<String,Object> logMap = cmprCommService.getLogMap(ddCmprNo, cmprDd, batchProcMap.get(i), rsltCodeV, rsltMsgV, userId);
			    cmprCnt = tgtCnt;
				logMap.put("tgt_cnt",  tgtCnt);
				logMap.put("cmpr_cnt", cmprCnt);
				logMap.put("strt_dt",  strDt);
				logMap.put("tgt_file_path",  "");	// 통계생성은 대상파일 경로가 없다.
				logMap.put("cmpr_file_path", ""); 	// 통계생성은 정산파일 경로가 없다.				
				
				// INSERT ADM_BATCH_LOG
				cmprCommService.insertAdmBatchLog(logMap);
				
				if( !"00".equals(rsltCodeV) ) {
					// 실패
					break;
				}
			}
			// End For Loop
			// 대사처리단계 flag 초기화
			cmprProcStep = 0;
			
		} catch (UserException e) {
			ErrorInfoVO errorInfoVO = e.getErrorInfo();
			rsltCodeV = errorInfoVO.getErrCd();
			rsltMsgV  = errorInfoVO.getErrMsg();
			
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
			rsltCodeV = messageManage.getMsgCd("SY_ERROR_900");
			rsltMsgV  = messageManage.getMsgTxt("SY_ERROR_900");
			
		} finally {
			// 배치 로그마스터 업데이트
			Map<String,Object> logMstMap = new HashMap<String,Object>();
			logMstMap.put("dd_cmpr_no",   ddCmprNo);
			logMstMap.put("cmpr_dd",      cmprDd);
			logMstMap.put("wrkn_nm",      wrknNm);
			logMstMap.put("user_id",      userId);
			logMstMap.put("ttl_deal_cnt", ttlDealCnt);
			logMstMap.put("no_acr_cnt",   0);
			logMstMap.put("mtch_cnt",     0);
			logMstMap.put("doc_file_nm",  "");
			logMstMap.put("rslt_code",    rsltCodeV);
			logMstMap.put("rslt_msg",     rsltMsgV);
			// UPDATE ADM_BATCH_LOG_M
		    cmprCommService.updateAdmBatchLogM(logMstMap);		    
		    
			// 실패했을때만 일대사마스터에 업데이트 한다. 정상처리 됬을때는 4단계에서 update 처리 한다.
			if( !"00".equals(rsltCodeV) ) {
				
				// 배치로그 등록
				if( cmprProcStep > 0 ) {
				    Map<String,Object> logMap = cmprCommService.getLogMap(ddCmprNo, cmprDd, batchProcMap.get(cmprProcStep-1), rsltCodeV, rsltMsgV, userId);
				    cmprCnt = tgtCnt;
					logMap.put("tgt_cnt",  tgtCnt);
					logMap.put("cmpr_cnt", cmprCnt);
					logMap.put("strt_dt",  strDt);
					// INSERT ADM_BATCH_LOG
					cmprCommService.insertAdmBatchLog(logMap);					
				}
			}
		}
		
		log.debug(methodName, "End cmprProc! (rsltCodeV=" + rsltCodeV + ")");
	}
}
