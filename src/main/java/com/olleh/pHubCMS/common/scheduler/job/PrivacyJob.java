package com.olleh.pHubCMS.common.scheduler.job;
import java.util.HashMap;
import java.util.Map;

import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.components.SysPrmtManage;
import com.olleh.pHubCMS.common.exception.UserException;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.ErrorInfoVO;
import com.olleh.pHubCMS.common.scheduler.service.CmprCommService;
import com.olleh.pHubCMS.common.scheduler.task.privacy.PrivacyTask;
import com.olleh.pHubCMS.common.scheduler.utils.CommonUtil;
import com.olleh.pHubCMS.common.utils.DateUtils;
import com.olleh.pHubCMS.common.utils.JsonUtil;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 휴면고객 개인정보 분리보관 job
 * @Class Name : PrivacyJob
 * @author cisohn
 * @since 2018.06.23
 * @version 0.1
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일        수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2019.04.10   cisohn        최초생성
 * </pre>
 */

@Service("privacyJob")
public class PrivacyJob {
	
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	PrivacyTask piTask;
	
	@Autowired
	CommonUtil commonUtil;
	
	@Autowired
	CmprCommService cmprCommService;	
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
	SysPrmtManage sysprmtMange;
	
	/**
	 * <pre> job 실행 함수 </pre>
	 * 
	 * @param Job 실행 콘텍스트
	 * @return void
	 * @see
	 */		
	public void execute() throws JobExecutionException {
		String methodName = "execute";
		Map<String, Object> param = new HashMap<String, Object>();
		int term =	Integer.valueOf(StringUtil.nvl(sysprmtMange.getSysPrmtVal("PRIVACY_BK_TERM"), "365"));
		param.put("DAYS", term);

		//test
		//param.put("DAYS", 50);
		
		//privacyProc(param);
		
	}
	
	/**
	 * <pre> 일대사 처리 </pre>
	 * 
	 * @param Job 실행 콘텍스트
	 * @return void
	 * @see
	 */	
	public void privacyProc(Map<String,Object> params) {
		String methodName = "privacyProc";
		log.debug(methodName, "Start privacyProc! (params=" + JsonUtil.toJson(params) + ")");
		
		//현재 실행중인 함수
		String stepCd = "0";
		String strDt  = "";	
		
		// set valiable
		String wrknNm       = this.getClass().getSimpleName();	// 배치작업명
		String ddCmprNo     = "";								// 일대사번호
		String cmprDd       = StringUtil.nvl(params.get("cmprDd"), commonUtil.getCurrentDate());
		String userId       = StringUtil.nvl(params.get("userId"), "pibackup");
		
		int tgtCnt       = 0;		// 원대상건수
		int cmprCnt      = 0;		// 대사결과 건수
		int ttlDealCnt   = 0;		// 총거래 건수
		String rsltCodeV = "";		// 결과코드  value
		String rsltMsgV  = "";		// 결과메시지 value
		
		// 파일맵
		Map<String, Object> fileMap = new HashMap<String, Object>();
		
		/* -----Set Parameter ---------------------------------------------------------------------------- */
		int idx = 0;
		Map<Integer, String> batchProcMap = new HashMap<Integer, String>();
		
		batchProcMap.put(idx++, "1.bkFpFmlySupot");   // [가족지원현황: fp_fmly_supot]
		batchProcMap.put(idx++, "2.bkPhMmsSendHist"); // [mms전송이력: ph_mms_send_hist]
		batchProcMap.put(idx++, "3.bkPhCpnChngHist"); // [쿠폰상태변경이력: ph_cpn_chng_hist] 
		batchProcMap.put(idx++, "4.bkPhCpnIsueInfo"); // [쿠폰발행정보: ph_cpn_isue_info]
		batchProcMap.put(idx++, "5.bkPhDealDtl");     // [거래원장상세: ph_deal_dtl]
		batchProcMap.put(idx++, "6.bkPhDealInfo");    // [거래원장: ph_deal_info]
		batchProcMap.put(idx++, "7.bkPhDealReqDtl");  // [거래요청상세: ph_deal_req_dtl]
		batchProcMap.put(idx++, "8.bkPhDealReqInfo"); // [거래요청마스터: ph_deal_req_info]
		batchProcMap.put(idx++, "9.bkPhPntCustCtn");  // [포인트고객CTN: ph_pnt_cust_ctn]
		batchProcMap.put(idx++, "10.bkPhPntCustInfo");// [고객정보: ph_pnt_cust_info]
		batchProcMap.put(idx++, "11.bkAdmUserInfo");  // [시스템사용자 정보: adm_user_info]

		// 백업진행단계
		int bkProcStep = 0;
		
		try {
			// DECLARE
			Map<String,Object> paramMap = new HashMap<String,Object>();
			
			// 1. 배치번호 추출
			stepCd   = "1";
			ddCmprNo = cmprCommService.getSeqCmId();
			
			// 에러 처리 해야 한다.
			if( "".equals(ddCmprNo) ) {
				log.debug(methodName,"배치번호 추출 오류");
				throw new UserException(stepCd, messageManage.getMsgVOY("IF_INFO_104"));
			}
			
			/* -----INSERT INTO ADM_BATCH_LOG_M -------------------------------------------------------------- */
			//3. 로그 마스터 생성
			Map<String,Object> logMstMap = new HashMap<String,Object>();
			logMstMap.put("dd_cmpr_no", ddCmprNo);
			logMstMap.put("cmpr_dd",    cmprDd);
			logMstMap.put("wrkn_nm",    wrknNm);
			logMstMap.put("user_id",    userId);
			int iLogM = cmprCommService.saveAdmBatchLogM(logMstMap);

			// 단계별 대사 처리
			for(int i=0; i < idx; i++) {
				// init
				bkProcStep++;
				strDt = DateUtils.defaultDate() + DateUtils.defaultTime();
				
				// 대사결과 셋팅 (성공)
				rsltCodeV = messageManage.getMsgCd("SY_INFO_00");
				rsltMsgV  = messageManage.getMsgTxt("SY_INFO_00");
				
				tgtCnt  = 1;
				cmprCnt = 1;
				
			    switch(i) {
		        case 0 : 
					// 1. 가족지원현황
					tgtCnt = piTask.backupFpFmlySupot(params);
					log.debug(methodName,"backupFpFmlySupot Result="+tgtCnt);					
					break;
		        case 1 : 
		        	// 2.mms전송이력
					tgtCnt = piTask.backupPhMmsSendHist(params);
					log.debug(methodName,"backupPhMmsSendHist Result="+tgtCnt);					
					ttlDealCnt += tgtCnt;
					break;
		        case 2 : 
		        	// 3.쿠폰상태변경이력
					tgtCnt = piTask.backupPhCpnChngHist(params);
					log.debug(methodName,"backupPhCpnChngHist Result="+tgtCnt);					
					ttlDealCnt += tgtCnt;
					break;				
		        case 3 : 
		        	// 4.쿠폰발행정보
					tgtCnt = piTask.backupPhCpnIsueInfo(params);
					log.debug(methodName,"backupPhCpnIsueInfo Result="+tgtCnt);					
					ttlDealCnt += tgtCnt;
					break;
		        case 4:
		        	// 5.거래원장상세
					tgtCnt = piTask.backupPhDealDtl(params);
					log.debug(methodName,"backupPhDealDtl Result="+tgtCnt);					
					ttlDealCnt += tgtCnt;
					break;
		        case 5:
		        	// 6.거래원장
					tgtCnt = piTask.backupPhDealInfo(params);
					log.debug(methodName,"backupPhDealInfo Result="+tgtCnt);					
					ttlDealCnt += tgtCnt;
					break;
		        case 6:
		        	// 7.거래요청상세
					tgtCnt = piTask.backupPhDealReqDtl(params);
					log.debug(methodName,"backupPhDealReqDtl Result="+tgtCnt);					
					ttlDealCnt += tgtCnt;
					break;
		        case 7:
		        	// 8.거래요청 마스터
					tgtCnt = piTask.backupPhDealReqInfo(params);
					log.debug(methodName,"backupPhDealReqInfo Result="+tgtCnt);					
					ttlDealCnt += tgtCnt;
					break;
		        case 8:
		        	// 9.포인트고객CTN
					tgtCnt = piTask.backupPhPntCustCtn(params);
					log.debug(methodName,"backupPhPntCustCtn Result="+tgtCnt);					
					ttlDealCnt += tgtCnt;
					break;
		        case 9:
		        	// 10.포인트고객정보
					tgtCnt = piTask.backupPhPntCustInfo(params);
					log.debug(methodName,"backupPhPntCustInfo Result="+tgtCnt);					
					ttlDealCnt += tgtCnt;
					break;
		        case 10:
		        	// 11.시스템사용자정보 백업
					tgtCnt = piTask.backupAdmUserInfo(params);
					log.debug(methodName,"backupAdmUserInfo Result="+tgtCnt);					
					ttlDealCnt += tgtCnt;
					break;					
		        default :
		        	log.debug(methodName, "default!");
			    }
			    
			    // 단계별 대사결과 log 저장 (startDt, endDt 셋팅해줘야 한다.)
			    Map<String,Object> logMap = cmprCommService.getLogMap(ddCmprNo, cmprDd, batchProcMap.get(i), rsltCodeV, rsltMsgV, userId);
			    cmprCnt = tgtCnt;
				logMap.put("tgt_cnt",  tgtCnt);
				logMap.put("cmpr_cnt", cmprCnt);
				logMap.put("strt_dt",  strDt);
				logMap.put("tgt_file_path",  "");
				logMap.put("cmpr_file_path", "");				
				
				// INSERT ADM_BATCH_LOG
				cmprCommService.insertAdmBatchLog(logMap);
				
				if( !"00".equals(rsltCodeV) ) {
					// 실패
					break;
				}
			}
			// End For Loop
			// 대사처리단계 flag 초기화
			bkProcStep = 0;
			
		} catch (UserException e) {
			ErrorInfoVO errorInfoVO = e.getErrorInfo();
			rsltCodeV = errorInfoVO.getErrCd();
			rsltMsgV  = errorInfoVO.getErrMsg();
			
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
			rsltCodeV = messageManage.getMsgCd("SY_ERROR_900");
			rsltMsgV  = messageManage.getMsgTxt("SY_ERROR_900");
			
		} finally {
			// 배치 로그마스터 업데이트
			Map<String,Object> logMstMap = new HashMap<String,Object>();
			logMstMap.put("dd_cmpr_no",   ddCmprNo);
			logMstMap.put("cmpr_dd",      cmprDd);
			logMstMap.put("wrkn_nm",      wrknNm);
			logMstMap.put("user_id",      userId);
			logMstMap.put("ttl_deal_cnt", ttlDealCnt);
			logMstMap.put("no_acr_cnt",   0);
			logMstMap.put("mtch_cnt",     0);
			logMstMap.put("doc_file_nm",  "");
			logMstMap.put("rslt_code",    rsltCodeV);
			logMstMap.put("rslt_msg",     rsltMsgV);
			// UPDATE ADM_BATCH_LOG_M
		    cmprCommService.updateAdmBatchLogM(logMstMap);		    
		    
			// 실패했을때만 일대사마스터에 업데이트 한다. 정상처리 됬을때는 4단계에서 update 처리 한다.
			if( !"00".equals(rsltCodeV) ) {
				
				// 배치로그 등록
				if( bkProcStep > 0 ) {
				    Map<String,Object> logMap = cmprCommService.getLogMap(ddCmprNo, cmprDd, batchProcMap.get(bkProcStep-1), rsltCodeV, rsltMsgV, userId);
				    cmprCnt = tgtCnt;
					logMap.put("tgt_cnt",  tgtCnt);
					logMap.put("cmpr_cnt", cmprCnt);
					logMap.put("strt_dt",  strDt);
					// INSERT ADM_BATCH_LOG
					cmprCommService.insertAdmBatchLog(logMap);					
				}
			}
		}
		
		log.debug(methodName, "End PrivacyBackupProc! (rsltCodeV=" + rsltCodeV + ")");
	}		
	
	
}
