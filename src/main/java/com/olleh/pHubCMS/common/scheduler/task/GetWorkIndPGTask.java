package com.olleh.pHubCMS.common.scheduler.task;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.scheduler.dao.PHubDAO;

/**
 * 1.대사파일 생성 여부 조회
 * @Class Name : ChkWorkIndToPGTask
 * @author mason
 * @since 2018.08.21
 * @version 0.1
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.21   mason      최초생성
 * </pre>
 */
@Service
public class GetWorkIndPGTask {	
	private Logger log = new Logger(this.getClass());
	
	@Resource
	PHubDAO pHubDAO;	

	/**
	 * <pre> 대사처리여부 체크 </pre>
	 * 
	 * @param 대사일자
	 * @return 대사작업여부 Y/N
	 * @see
	 */		
	public Map getWorkInd(Map<String,Object> params){
		//현재 실행중인 함수명
		return pHubDAO.selectWorkIndPG(params);
	}
	
	public int deletePgCmprInfoD(Map<String,Object> params){
		return pHubDAO.deletePgCmprInfoD(params);
	}
	public int deletePgCmprInfoM(Map<String,Object> params){
		return pHubDAO.deletePgCmprInfoM(params);
	}
}
