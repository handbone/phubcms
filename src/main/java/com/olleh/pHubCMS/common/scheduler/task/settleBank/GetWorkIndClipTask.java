package com.olleh.pHubCMS.common.scheduler.task.settleBank;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.scheduler.dao.PHubDAO;
/**
 * 대사처리여부 체크
 * @Class Name : ChkWorkIndToPGTask
 * @author mason
 * @since 2018.08.24
 * @version 0.1
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.24   mason      최초생성
 * </pre>
 */
@Service
public class GetWorkIndClipTask {
	private Logger log = new Logger(this.getClass());
	
	@Resource
	PHubDAO pHubDAO;	
	
	/**
	 * <pre> 대사처리여부 체크 </pre>
	 * 
	 * @param 대사일자
	 * @return 대사작업여부 Y/N
	 * @see
	 *    기작업여부 체크하는 메소드인데 현재는 필요 없다.
	 *    배치재실행되면 기작업데이터 모두 삭제처리하고 새로 돌기 때문이다. - 2018.10.08. - lys 
	 */	
	public Map getWorkInd(Map<String,Object> params){
		//현재 실행중인 함수명
		String methodName = "getWorkInd";
		Map dataMap = new HashMap();
		try{
			dataMap = pHubDAO.selectWorkIndClip(params);
		}catch(Exception e){			
			log.printStackTracePH(methodName, e);
		}
		return dataMap;
	}

	/**
	 * <pre> 클립포인트 기존 대사내역 삭제 </pre>
	 * 
	 * @param 대사일자
	 * @return 처리결과
	 * @see
	 */			
	public int setClipCmprInit(Map<String,Object> params){
		int iSum = 0;
		int iMst = 0, iFile = 0, iSucs = 0, iFail = 0, iPoint = 0;
		try{
			//클립포인트 일대사 마스터 삭제
			iMst   = pHubDAO.deleteClCmprInfo(params);
			//클립포인트 대사파일 파싱자료 삭제
			iFile  = pHubDAO.deleteClCmprFileInfo(params);
			//클립포인트 대사결과 성공건 삭제
			iSucs  = pHubDAO.deleteClCmprSucs(params);
			//클립포인트 대사결과 실패건 삭제
			iFail  = pHubDAO.deleteClCmprFail(params); 
			//클립포인트 대사결과 카드별 삭제
			iPoint = pHubDAO.deleteClCmprPoint(params);
			//합계
			iSum = iMst + iFile + iSucs + iFail + iPoint;
		}catch(Exception e){
			log.printStackTracePH("setClipCmprInit", e);
			iSum = 0;
		}
		
		return iSum;
	}
}
