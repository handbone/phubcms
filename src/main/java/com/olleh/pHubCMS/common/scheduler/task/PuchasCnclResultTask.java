package com.olleh.pHubCMS.common.scheduler.task;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.api.service.PHHttpService;
import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.components.SysPrmtManage;
import com.olleh.pHubCMS.common.exception.UserException;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.ErrorInfoVO;
import com.olleh.pHubCMS.common.model.ResultVo;
import com.olleh.pHubCMS.common.scheduler.dao.PHubDAO;
import com.olleh.pHubCMS.common.utils.JsonUtil;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 매입취소 처리
 * @Class Name 	: PuchasCnclResultTask
 * @author 		: mgbang
 * @since 		: 2019.03.13
 * @version 	: 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.03.13   mgbang     최초생성
 * </pre>
 */
@Service
public class PuchasCnclResultTask {
	
	private Logger log = new Logger(this.getClass());
	
	@Resource
	PHubDAO pHubDAO;
	
	@Autowired
    PHHttpService pHHttpService;
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
    SysPrmtManage sysPrmtManage;
	
	/**
	 * <pre> 매입취소 처리 </pre>
	 * 
	 * @param params
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked", "unused" })
	public Map<String,Object> puchasCnclResult(Map<String,Object> params) {
		String methodName = "puchasCnclResult";
		log.debug("매입취소 처리", params.toString());
		
		String stepCd = "00";
		ResultVo resultVo = new ResultVo();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(new MediaType(MediaType.APPLICATION_FORM_URLENCODED, StandardCharsets.UTF_8));
		Map<String,Object> ret = new HashMap<String,Object>();
		try {
			resultVo.setCaller(getClass().getSimpleName() +"."+ methodName);
			
			stepCd = "01";
			List<Map> pCnclList = pHubDAO.selectPchasCnclByCmprNo(params);
			
			int phCnt = 0;		// 포인트허브 연동 전체 건수
			int fpCnt = 0;		// 패밀리포인트 연동 전체 건수
			int phSucCnt = 0;	// 포인트허브 연동 성공 건수
			int fpSucCnt = 0;	// 패밀리포인트 연동 성공 건수
			List<Map> phFails = new ArrayList<Map>();
			List<Map> fpFails = new ArrayList<Map>();
			for (Map map : pCnclList) {
				Map postData = new HashMap(params);
				postData.put("phub_tr_no"	, StringUtil.nvl(map.get("phub_tr_no")));	//포인트허브거래번호
				postData.put("pg_tr_no"		, StringUtil.nvl(map.get("pg_deal_no")));	//PG사ID
				postData.put("tr_div"		, "CA");									//거래구분 - CA: 취소일반
				postData.put("cncl_ind"		, "CB");									//취소구분 - CB: 매입취소
				postData.put("if_yn"		, "N");										//외부연동여부
				postData.put("cncl_yn"		, "A");										//취소여부
				
				stepCd = "02";
				String serviceId = StringUtil.nvl(map.get("service_id"));
				boolean isFmlyPnt = StringUtils.equals(serviceId, "SVC_FP") ? true : false;
				String sendUrl = isFmlyPnt ? "/fp/giftishow/chgCpnStatus" : "/cms/cancel.do";
				if (isFmlyPnt) {
				// 패밀리포인트
					// Post Data Settings
					postData.put("mdcode"		, sysPrmtManage.getSysPrmtVal("GS_MDCODE"));//매체코드
					postData.put("tr_id"		, StringUtil.nvl(map.get("phub_tr_no")));	//포인트허브거래번호
					postData.put("trade_type"	, "A01");									//거래타입 - A01: CMS핀상태변경
					postData.put("trade_code"	, "A01");									//거래코드 - A01: 발행취소(내부취소)
					postData.put("pin_no"		, StringUtil.nvl(map.get("pin_no")));		//상품권번호
					postData.put("trade_date"	, new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));	//취소일시
					resultVo.setReqBody(postData);
					// (Http / Https) Post 통신
					resultVo = sendHttpPost(sendUrl, headers, resultVo);
					Map<String, Object> response = JsonUtil.JsonToMap(StringUtil.nvl(resultVo.getRstBody()));
					if (StringUtil.nvl(response.get("resCode")).equals("1000")) {
						fpSucCnt++;
					} else {
						resultVo.setSucYn("N");
						fpFails.add(postData);
					}
					resultVo.setRstCd(StringUtil.nvl(response.get("resCode")));
					resultVo.setRstMsg(StringUtil.nvl(response.get("resMsg")));
					fpCnt++;
				} else {
				// 포인트허브
					// Post Data Settings
					resultVo.setReqBody(postData);
					// (Http / Https) Post 통신 
					resultVo = sendHttpPost(sendUrl, headers, resultVo);
					Map<String, Object> response = JsonUtil.JsonToMap(StringUtil.nvl(resultVo.getRstBody()));
					if (StringUtil.nvl(response.get("ret_code")).equals("00") || StringUtil.nvl(response.get("ret_code")).equals("I156")) {
						phSucCnt++;
					} else {
						resultVo.setSucYn("N");
						phFails.add(postData);
					}
					resultVo.setRstCd(StringUtil.nvl(response.get("ret_code")));
					resultVo.setRstMsg(StringUtil.nvl(response.get("ret_msg")));
					phCnt++;
				}
			}
			stepCd = "04";
			// 매입취소 처리 성공 여부 세팅
			ret.put("retcode", messageManage.getMsgCd("SY_INFO_00"));
			ret.put("retmsg", messageManage.getMsgTxt("SY_INFO_00"));
			// 결과 모니터링
			log.debug(methodName, "[매입취소]포인트허브   전체 건수: "+ phCnt);
			log.debug(methodName, "[매입취소]포인트허브   연동 성공 건수: "+ phSucCnt);
			log.debug(methodName, "[매입취소]포인트허브   연동 실패 건수: "+ phFails.size());
			log.debug(methodName, "[매입취소]포인트허브   연동 실패: "+ phFails);
			log.debug(methodName, "[매입취소]패밀리포인트 전체 건수: "+ fpCnt);
			log.debug(methodName, "[매입취소]패밀리포인트 연동 성공 건수: "+ fpSucCnt);
			log.debug(methodName, "[매입취소]패밀리포인트 연동 실패 건수: "+ fpFails.size());
			log.debug(methodName, "[매입취소]패밀리포인트 연동 실패: "+ fpFails);
			// 결과 건수 세팅 (대상건수, 처리건수)
			ret.put("tgt_cnt",  phCnt + fpCnt);
			ret.put("cmpr_cnt", phSucCnt + fpSucCnt);
		} catch (UserException e) {
			ErrorInfoVO errorInfoVO = e.getErrorInfo();
			ret.put("retcode",  errorInfoVO.getErrCd());
			ret.put("retmsg",   errorInfoVO.getErrMsg());
			ret.put("tgt_cnt",  1);
			ret.put("cmpr_cnt", 0);
			log.error(methodName, "Exception : "+ e);
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
		}
		
		return ret;
	}
	
	/**
	 * (Http / Https) Post 통신
	 * 
	 * @param sendUrl
	 * @param header
	 * @param resultVo
	 * @return
	 */
	private ResultVo sendHttpPost(String sendUrl, HttpHeaders headers, ResultVo resultVo) throws UserException {
		resultVo = pHHttpService.sendHttpPostCustom(sendUrl, headers, resultVo);
		// HTTP 통신 성공 여부 체크
		if (!StringUtils.equals(resultVo.getSucYn(), "Y")) {
			throw new UserException("03", messageManage.getMsgVOY("CM_ERROR_613"));
		}
		return resultVo;
	}
}
