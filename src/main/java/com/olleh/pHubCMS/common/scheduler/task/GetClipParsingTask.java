package com.olleh.pHubCMS.common.scheduler.task;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.scheduler.dao.PHubDAO;

/**
 * 클립 포인트 대사파일에 대해 파싱, 파싱한 결과를 저장
 * @Class Name : GetClipParsingTask
 * @author mason
 * @since 2018.08.24
 * @version 0.1
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.24   mason      최초생성
 * </pre>
 */
@Service
public class GetClipParsingTask {	
	private Logger log = new Logger(this.getClass());

	@Resource
	PHubDAO pHubDAO;	
	
	@Autowired
	MessageManage messageManage;
	
	/**
	 * <pre> 로컬저장소에 내려받은 대사 파일 파싱 </pre>
	 * 
	 * @param 저장 파일명, 대사일자
	 * @return retcode, retmsg
	 * @see
	 */		
	public Map<String,Object> getClipParse(Map<String,Object> dataMap, String userId) {
		//현재 실행중인 함수
		String methodName = "getClipParse";
		
		Map<String,Object> resMap = new HashMap<String,Object>();
		Map<String,Object> pasMap = null; //파싱결과 담는 Map
		String fileName = "";
		File file = null;
		String ddCmprNo = String.valueOf(dataMap.get("dd_cmpr_no"));
		
		BufferedReader br = null;
		
		List dataList = new ArrayList<Object>(); //파싱결과를 row레벨로 담는 리스트
		
		try{
			if((dataMap!=null)&&(dataMap.size()>0)){
//				fileName = String.valueOf(dataMap.get("file_name"));
				fileName = String.valueOf(dataMap.get("full_path"));
				log.debug(methodName, "fileName:"+fileName);
				file = new File(fileName);
				
				if(file.exists()==true){
					try {
						br = new BufferedReader(new FileReader(file));
						
						String lineStr = null;
						
						while((lineStr=br.readLine())!=null){
							pasMap = new HashMap<String,Object>();
							pasMap.put("line_str", lineStr);
							dataList.add(pasMap);
						}
					} finally {
						br.close();
					}
					
					//파싱 결과를 데이타베이스에 저장(CL_CMPR_FILE_INFO)
					/* ------------------------------------------------------- */
					if((dataList!=null)&&(dataList.size()>0)){
						//클립포인트 대사파일 파싱결과 저장여부
						String workInd = this.getParseWorkInd(dataList,ddCmprNo);
						log.debug(methodName, "파싱결과 저장여부:"+workInd);
						if(workInd.equals("N")){
							//해당 일시에 대한 파싱처리 이전
							resMap = this.saveParseResult(dataList, String.valueOf(dataMap.get("dd_cmpr_no")), userId);
							//파싱된 파일 데이타에 본문이 없는 경우
							
							//파싱된 파일 데이타에 중복 여부
							Map<String,Object> dupMap = new HashMap<String,Object>();
							dupMap.put("dd_cmpr_no", ddCmprNo);
							String dupData = this.getDuplicatedClippoint(dupMap);
							
							if(dupData.equals("Y")){
								log.error(methodName, "클립포인트 대사파일 내에 중복건이 있습니다.");
								resMap.put("retcode", messageManage.getMsgCd("SY_ERROR_900"));
								resMap.put("retmsg", "클립포인트 대사파일 내에 중복건이 있습니다.");
								//int iDel = this.deleteClCmprFileInfo(dataMap);
								//log.debug("iDel","iDel:"+iDel);
							} else {
								int iChk = this.getCountClippoint(dupMap);
								if(iChk==0){
									//data not found
									//resMap.put("retcode", messageManage.getMsgCd("IF_INFO_104"));
									resMap.put("retmsg",  messageManage.getMsgTxt("IF_INFO_104"));
								}								
							}
							//retcode,retmsg,pt_hb_date_time getCountClippoint
							log.debug(methodName, "파싱결과 저장결과:"+String.valueOf(resMap.get("retmsg")));
						}else{
							//해당 일시에 이미 파싱처리 했거나 혹은 다른 오류
							log.error(methodName, messageManage.getMsgTxt("SY_ERROR_900"));
							resMap.put("retcode", messageManage.getMsgCd("SY_ERROR_900"));
							resMap.put("retmsg", messageManage.getMsgTxt("SY_ERROR_900"));	  
						}
					}else{
						//클립포인트 대사파일 파싱 실패
						log.error(methodName, messageManage.getMsgTxt("SY_ERROR_900"));
						resMap.put("retcode", messageManage.getMsgCd("SY_ERROR_900"));
						resMap.put("retmsg", messageManage.getMsgTxt("SY_ERROR_900"));	  
					}
				}else{
					//File Not Found
					log.error(methodName, messageManage.getMsgTxt("CM_ERROR_603"));
					resMap.put("retcode", messageManage.getMsgCd("CM_ERROR_603"));
					resMap.put("retmsg", messageManage.getMsgTxt("CM_ERROR_603"));	
				}
			}else{
				//invalid parameter
				log.error(methodName, messageManage.getMsgTxt("IF_INFO_103"));
				resMap.put("retcode", messageManage.getMsgCd("IF_INFO_103"));
				resMap.put("retmsg", messageManage.getMsgTxt("IF_INFO_103"));	
			}
		}catch(Exception e){
			//internal exception
			log.error(methodName, messageManage.getMsgTxt("SY_ERROR_900"));
			resMap.put("retcode", messageManage.getMsgCd("SY_ERROR_900"));
			resMap.put("retmsg", messageManage.getMsgTxt("SY_ERROR_900"));
		}
		
		return resMap;
	}
	/**
	 * <pre> 클립데이타 파싱결과 중복 발생시 해당 대사일자에 대한 파싱결과 삭제 </pre>
	 * 
	 * @param 일대사번호
	 * @return 처리결과
	 * @see
	 */		
	public int deleteClCmprFileInfo(Map<String,Object> params){
		return pHubDAO.deleteClCmprFileInfo(params);
	}
	/**
	 * <pre> 클립데이타 파싱결과 중복 여부 조회 </pre>
	 * 
	 * @param 일대사번호
	 * @return work_ind 작업여부(Y/N)
	 * @see
	 */		
	public String getDuplicatedClippoint(Map<String,Object> params){
		String resStr = "N";
		Map map = new HashMap();//Map dataMap = new HashMap();
		try{
			map = pHubDAO.getDuplicatedClippoint(params);
			
			if((map!=null)&&(map.isEmpty()==false)){
				resStr = String.valueOf(map.get("dup_cnt"));				
			}			
		}catch(Exception e){
			log.printStackTracePH("getDuplicatedClippoint", e);
		}
		return resStr;
	}
	/**
	 * <pre> 클립데이타 파싱결과 저장처리 여부 조회 </pre>
	 * 
	 * @param 파일생성일시
	 * @return work_ind 작업여부(Y/N)
	 * @see
	 */	
	public String getParseWorkInd(List dataList, String ddCmprNo){
		//현재 실행중인 함수
		String methodName = "getParseWorkInd";
				
		String resStr = null;
		String ptHbDateTime = null; //포인트허브 생성일시
		try{
			for(int i=0;i<dataList.size();i++){
				Map<String,Object> oriMap   = (Map<String, Object>) dataList.get(i);
				String lineStr              = String.valueOf(oriMap.get("line_str"));
				
				if(lineStr.substring(0,2).equals("HD")){
					ptHbDateTime = lineStr.substring(6,20);					
				}
			}
			
			if((ptHbDateTime!=null)&&(!ptHbDateTime.equals(""))){
				Map<String,Object> param = new HashMap<String,Object>();
				param.put("dd_cmpr_no", ddCmprNo);
				log.debug("클립데이타 파싱결과 저장처리 여부 조회", param.toString());
				resStr = pHubDAO.selectClCmprFileInfo(param);
			}
		}catch(Exception e){
			log.printStackTracePH(methodName, e);
		}
		return resStr;
	}
	/**
	 * <pre> 파싱 결과 저장 </pre>
	 * 
	 * @param 저장 파일명, 대사일자
	 * @return retcode, retmsg
	 * @see
	 */		
	public Map<String,Object> saveParseResult(List dataList, String ddCmprNo, String userId){
		//현재 실행중인 함수
		String methodName = "saveParseResult";	
		
		Map<String,Object> resMap   = new HashMap<String,Object>();
		Map<String,Object> oriMap   = null;
		String lineStr              = null; //라인단위 파싱 결과
		Map<String,Object> tmpMap   = null; //본문 데이타
		Map<String,Object> paramMap = null;
		
		int ptHbCt          = 0;    //포인트허브 건수
		String ptHbVer      = null; //포인트허브 전문버전
		String ptHbDateTime = null; //포인트허브 생성일시
		
		int iCount          = 0;    //처리결과, 1 or 0
		int iSum            = 0;    //처리건수, iCount의 합계
	
		try{
			for(int j=0;j<dataList.size();j++){
				oriMap = (Map<String, Object>) dataList.get(j); 
				lineStr = String.valueOf(oriMap.get("line_str"));
				
				if(lineStr.substring(0,2).equals("TR")){
					ptHbCt = Integer.parseInt(lineStr.substring(2,12));
				}else if(lineStr.substring(0,2).equals("HD")){
					ptHbVer = lineStr.substring(2,6);
					ptHbDateTime = lineStr.substring(6,20);
				}
			}
				
			for(int i = 0 ; i < dataList.size() ; i++){
				tmpMap  = new HashMap<String,Object>();
				tmpMap  = (Map<String, Object>) dataList.get(i);
				lineStr = String.valueOf(tmpMap.get("line_str"));
				//본문 파싱
				if(lineStr.substring(0,2).equals("DT")){
					paramMap = new HashMap<String,Object>();
					//paramMap.put("pt_hb_date_time", ptHbDateTime);
					paramMap.put("dd_cmpr_no",      ddCmprNo);
	    			paramMap.put("pt_hb_ver",       ptHbVer);
					paramMap.put("pt_hb_seqnum",    Integer.parseInt(lineStr.substring(2,12)));
					paramMap.put("pt_hb_bne_cd",    lineStr.substring(12,14));
					paramMap.put("pt_hb_uqe_n",     lineStr.substring(14,34));     
					paramMap.put("pt_hb_partner",   (lineStr.substring(34,54)).trim());
					paramMap.put("pt_hb_ucd",       lineStr.substring(54,64).trim());
					paramMap.put("pt_hb_apv_pnt",   Integer.parseInt(lineStr.substring(64,74)));
					paramMap.put("pt_hb_ri_pnt",    Integer.parseInt(lineStr.substring(74,84)));
					paramMap.put("pt_hb_ori_uqe_n", (lineStr.substring(84,104)).trim());    
					paramMap.put("pt_hb_ci",        lineStr.substring(104,192));
					paramMap.put("pt_hb_ct",        ptHbCt);
					paramMap.put("user_id",         userId); 
					//log.debug("saveParseResult", paramMap.toString());
					iCount = pHubDAO.insertClCmprFileInfo(paramMap);
					iSum = iSum + iCount;
				}
			}//for~loop
			//결과 데이타셋 세팅
			resMap.put("cmprCnt", iSum);
			if(iSum >= 0){
				resMap.put("retcode", messageManage.getMsgCd("SY_INFO_00"));
				resMap.put("retmsg", messageManage.getMsgTxt("SY_INFO_00"));
				resMap.put("pt_hb_date_time", ptHbDateTime);
			}else{				
				resMap.put("retcode", messageManage.getMsgCd("SY_ERROR_800"));
				resMap.put("retmsg", messageManage.getMsgTxt("SY_ERROR_800"));
				resMap.put("pt_hb_date_time", "");
			}
		}catch(Exception e){
			log.error(methodName, e.getMessage());
			resMap.put("retcode", messageManage.getMsgCd("SY_ERROR_900"));
			resMap.put("retmsg", messageManage.getMsgTxt("SY_ERROR_900"));	 			
		}
		return resMap;
	}
	/**
	 * <pre> 클립포인트 대사파일 파싱결과 데이타row수 조회 </pre>
	 * 
	 * @param 일대사번호
	 * @return 조회수
	 * @see
	 */			
	public int getCountClippoint(Map<String,Object> params){
		int iRes = 0;
		try{
			Map map = new HashMap();
			map = pHubDAO.selectCountClipPoint(params);
			if((map!=null)&&(map.isEmpty()==false)){
				iRes = Integer.parseInt(String.valueOf(map.get("cnt")));
			}
		}catch(Exception e){
			log.printStackTracePH("getCountClippoint", e);
		}
		return iRes;
	}
}
