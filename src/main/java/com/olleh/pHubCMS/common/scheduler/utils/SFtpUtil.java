package com.olleh.pHubCMS.common.scheduler.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.utils.DateUtils;
import com.olleh.pHubCMS.common.utils.StringUtil;

@Service
public class SFtpUtil {
	private Session session = null;
	private Channel channel = null;
	private ChannelSftp channelSftp = null;
	
	private String destSvr  = "";
	private int    destPort = 0;
	private String destUser = "";
	private String destPwd  = "";
	
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
	CommonUtil cmmn;	
	
	/**
	 * <pre> sftp 초기화 </pre>
	 * 
	 * @param 대상서버IP, 포트, 사용자아이디, 패스워드
	 * @return void
	 * @see
	 */	
	public String init(String host, int port, String userName, String password) 
	throws JSchException,ConnectException
	{
		String methodName = "init";
		String rtnStr = "";
		JSch jsch = new JSch();

		try{
			session = jsch.getSession(userName, host, port);
			session.setPassword(password);
			
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			
			session.connect(3000); //세션 타임아웃 3초
			
			if(session.isConnected()){
				//연결 성공
				channel = session.openChannel("sftp");
	    		channel.connect();
	    		
	    		channelSftp = (ChannelSftp) channel;
	    		
	    		rtnStr = messageManage.getMsgCd("SY_INFO_00");
			}else{
				//연결 실패
				rtnStr = messageManage.getMsgCd("CM_ERROR_605");
			}
		}catch(JSchException je){
			log.printStackTracePH("init", je);
			rtnStr = messageManage.getMsgCd("CM_ERROR_605");
		}catch(Exception e){
			log.printStackTracePH("init", e);
			rtnStr = messageManage.getMsgCd("CM_ERROR_605");
		}
//		log.debug("init", "rtnStr::::"+rtnStr);
		return rtnStr;
	}

	/**
	 * <pre> sftp upload </pre>
	 * 
	 * @param 대상 폴더, 대상 파일명
	 * @return void
	 * @see
	 */		
	public String upload(String destDir, String fileName){
		String methodName = "upload";
		String rtnStr = "";
		FileInputStream fis = null;
		rtnStr = messageManage.getMsgCd("CM_ERROR_605");
		
		try{
			File tempFile = new File(fileName);

			fis = new FileInputStream(fileName);
			log.debug("upload", "dest Dir : "+destDir);
			channelSftp.cd(destDir);
			channelSftp.put(fis,tempFile.getName());
			
			rtnStr = messageManage.getMsgCd("SY_INFO_00");
		}catch(SftpException se){
			log.printStackTracePH("upload", se);
			log.debug("upload", se.getMessage());
		}catch(FileNotFoundException fe){
			log.printStackTracePH("upload", fe);
		}catch(Exception e){
			log.printStackTracePH("upload", e);
		}finally{
			try{
				if(fis!=null){
					fis.close();
				}
			}catch(Exception e){
				log.printStackTracePH("upload", e);
			}
		}
		return rtnStr;
	}
	
	/**
	 * <pre> sftp 종료 </pre>
	 * 
	 * @param 
	 * @return void
	 * @see
	 */	
	public void disconnect(){
		String methodName = "disconnect";
		if(session.isConnected()){
			channelSftp.disconnect();
			channel.disconnect();
			session.disconnect();
		}
	}
	
	/**
	 * <pre> sftp 파일 전송 </pre>
	 * 
	 * @param Map data(대상서버IP, 포트, 사용자아이디, 패스워드, 대상 폴더, 대상 파일명)
	 * @return void
	 * @see
	 */	
	public Map<String,Object> sendFile(Map<String,Object> dataMap)
	throws JSchException,ConnectException
	{
		String methodName = "sendFile";
		
		String destDir  = null; //대상 폴더
		String destFile = null; //대상 파일
		
		Map<String,Object> resMap = new HashMap<String,Object>();
		
		try{
			destSvr  = String.valueOf(dataMap.get("host"));
			destPort = Integer.parseInt(String.valueOf(dataMap.get("port")));
			destUser = String.valueOf(dataMap.get("userName"));
			destPwd  = String.valueOf(dataMap.get("password"));
			//연결처리
			//log.debug(methodName,"Connect...");
			log.debug(methodName, "destSvr="+destSvr+", destPort="+destPort+", destUser="+destUser+", destPwd="+destPwd);
			String connStr = this.init(destSvr, destPort, destUser, destPwd);
			
			destDir  =  String.valueOf(dataMap.get("destDir"));
			destFile =  String.valueOf(dataMap.get("fileName"));
			
			if(connStr.equals("00")){
				//업로드 처리			
				this.upload(destDir, destFile);
				//result
				resMap.put("file_path", destDir);
				resMap.put("file_nm", destFile);
				resMap.put("retcode", messageManage.getMsgCd("SY_INFO_00"));
				resMap.put("retmsg", messageManage.getMsgTxt("SY_INFO_00"));
			}else{
				resMap.put("file_path", destDir);
				resMap.put("file_nm", destFile);
				resMap.put("retcode", messageManage.getMsgCd("CM_ERROR_605"));
				resMap.put("retmsg", messageManage.getMsgTxt("CM_ERROR_605"));					
			}
		}catch(Exception e){
			log.printStackTracePH(methodName, e);
			resMap.put("file_path", destDir);
			resMap.put("file_nm", destFile);
			resMap.put("retcode", messageManage.getMsgCd("CM_ERROR_605"));
			resMap.put("retmsg", messageManage.getMsgTxt("CM_ERROR_605"));			
		}
		//종료
		//log.debug(METHOD_NAME,"DisConnect...");
		this.disconnect();
		
		return resMap;
	}
	
	/**
	 * <pre> sftp download </pre>
	 * 
	 * @param 대상 폴더, 저장 파일명, 저장되는 로컬 폴더
	 * @return void
	 * @see
	 */		
	public void download(String dir, String fileName,String path){
		String methodName = "download";
		InputStream is = null;
		FileOutputStream fos = null;
		
		log.debug(methodName, "dir="+dir+", fileName="+fileName+", path="+path);
		try{
			is = channelSftp.get(fileName);			
		}catch(SftpException se){
			log.printStackTracePH(methodName, se);
		}
		
		try{
			fos = new FileOutputStream(new File(path));

			int i;
			
			while((i=is.read())!=-1){
				fos.write(i);
			}
		}catch(IOException ie){
			log.printStackTracePH(methodName, ie);
		}catch(Exception e){
			log.printStackTracePH(methodName, e);
		}finally{
			try{
				if(fos!=null)fos.close();
				if(is!=null)is.close();
			}catch(Exception e){
				log.printStackTracePH(methodName, e);
			}
		}
	}
	/**
	 * <pre> sftp download </pre>
	 * 
	 * @param 호스트, 포트, 사용자아이디, 패스워드, 대상 폴더, 저장될 폴더, 저장 파일명
	 * @return 대사일자, 다운로드된 파일명 리스트
	 * @see
	 */			
	public List<Map<String,Object>> getFile(Map<String,Object> dataMap){
		String methodName = "getFile";
		List<Map<String,Object>> dataList = new ArrayList<Map<String,Object>>();
		Map<String,Object> resMap = null;
		String destDir  = null;
		String localDir = String.valueOf(dataMap.get("local_path"));
		String dealDd   = StringUtil.nvl(dataMap.get("deal_dd"));

		try{
			destSvr  = String.valueOf(dataMap.get("host"));
			destPort = Integer.parseInt(String.valueOf(dataMap.get("port")));
			destUser = String.valueOf(dataMap.get("user"));
			destPwd  = String.valueOf(dataMap.get("password"));
			//연결처리
			log.debug(methodName, "destSvr="+destSvr+", destPort="+destPort+", destUser="+destUser+", destPwd="+destPwd);
			
			String initRes = this.init(destSvr, destPort, destUser, destPwd);
			log.debug("initRes", initRes);
			destDir  =  String.valueOf(dataMap.get("dir"));
			log.debug("getFile","Dest Dir : "+destDir);
			channelSftp.cd(destDir);
			
			Vector<LsEntry> vector = null;
			int iSize = 0;
			
			try{
				vector = channelSftp.ls("*_POINTHUB");
				LsEntry newestEntry = null; 
				//대상 디렉토리 내에서 가장 최신일자 파일 추출
//				for(ChannelSftp.LsEntry entry : vector){
//					if (!entry.getFilename().equals(".") && !entry.getFilename().equals("..")){
//						if ((newestEntry == null) || (newestEntry.getAttrs().getMTime() < entry.getAttrs().getMTime())){
//							newestEntry = entry;
//						}
//					}
//				}
				// 대상 디렉토리 내에서 dealDd+1 파일명을 가진 파일을 찾는다.
				String calcDate = DateUtils.calcDate(dealDd, 1);
				log.debug("getFile","calcDate : "+calcDate);
				for(ChannelSftp.LsEntry entry : vector){
					if (!entry.getFilename().equals(".") && !entry.getFilename().equals("..")){
						if( entry.getFilename().startsWith(calcDate+"_") ) {
							newestEntry = entry;
							break;
						}
					}
				}
				if(newestEntry != null){
					//파일 다운로드
					log.debug(methodName, "[download]destDir="+destDir+", getFilename="+newestEntry.getFilename()+", localDir="+localDir);
					
					this.download(destDir, newestEntry.getFilename(), localDir+newestEntry.getFilename());
					
					resMap = new HashMap<String,Object>();
					resMap.put("deal_dd",   dealDd); //파라미터로 넘어온 D-1
					resMap.put("file_name", newestEntry.getFilename());
					resMap.put("full_path", localDir+newestEntry.getFilename());
					
					dataList.add(resMap);
				}
				/*
				String fileBuildDate = this.getFileBuildDate(); //대사파일과 대사일자는 서로 다르다.
			  //vector = channelSftp.ls(String.valueOf(dataMap.get("deal_dd"))+"*");
				vector = channelSftp.ls(fileBuildDate+"*");
				iSize  = vector.size(); //파일 갯수	
				log.debug("iSize", "iSize:"+iSize);
				if(iSize > 0){
					//대상 파일이 존재
					for(ChannelSftp.LsEntry entry : vector){
						if(!entry.getAttrs().isDir()){	
							//파일 다운로드
							this.download(destDir, entry.getFilename(), localDir+entry.getFilename());
							resMap = new HashMap<String,Object>();

							resMap.put("deal_dd", String.valueOf(dataMap.get("deal_dd")));
							resMap.put("file_name", localDir+entry.getFilename());
							
						}
						dataList.add(resMap);
					}
				}else{
					//대상 파일이 존재하지 않음.
					log.debug("getFile", messageManage.getMsgTxt("CM_ERROR_603"));
				}
				*/
			}catch(Exception e){
				log.printStackTracePH(methodName, e);
			}
		}catch(Exception e){
			log.printStackTracePH(methodName, e);
		}
		//종료
		//log.debug(METHOD_NAME,"DisConnect...");
		this.disconnect();
		
		return dataList;
	}
	/**
	 * <pre> 클립포인트 대사파일 생성일자(D-Day) </pre>
	 * 
	 * @param
	 * @return String 클립포인트 대사파일 생성일자(D-Day)
	 * @see
	 */
	public String getFileBuildDate(){
		String resDate = "";
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		resDate = dateFormat.format(calendar.getTime());
		return resDate;
	}
	
	/**
	 * <pre> PG대사파일이 정상적으로 대상서버 대상폴더에 업로드되었는지를 판단 </pre>
	 * 
	 * @param 저장 파일명
	 * @return 결과 1 or 0
	 * @see
	 */	
	public int chkFileToPG(Map<String,Object> infoMap){
		String methodName = "chkFileToPG";
		
		int iRes = 0;
		
		try{
			destSvr  = String.valueOf(infoMap.get("cm_host"));
			destPort = Integer.parseInt(String.valueOf(infoMap.get("cm_port")));
			destUser = String.valueOf(infoMap.get("cm_user"));
			destPwd  = String.valueOf(infoMap.get("cm_pwd"));
			
			String destDir  = String.valueOf(infoMap.get("cm_dir")); 
			String fileName = String.valueOf(infoMap.get("fileName"));
			log.debug(methodName, "destSvr="+destSvr+", destPort="+destPort+", destUser="+destUser+", destPwd="+destPwd+", destDir="+destDir+", fileName="+fileName);
			
			String initRes = this.init(destSvr, destPort, destUser, destPwd);
			
			if(initRes.equals("00")){
				channelSftp.cd(destDir);
			
				Vector<LsEntry> vector = null;
				vector = channelSftp.ls(fileName+"*");
			
				iRes = vector.size();
			}else{
				iRes = 0;
			}
		}catch(Exception e){
			log.printStackTracePH(methodName, e);
		}
		
		return iRes;
	}	
	
	public Map<String,Object> delFiles(Map<String,Object> params){
		String methodName = "delFile";
		
		Map<String,Object> resMap = new HashMap<String,Object>();
		String connStr = "";
		
		String destDir    = null; //대상 폴더
		String[] destFile = null; //대상 파일
		
		try{
			destSvr   = String.valueOf(params.get("host"));
			destPort  = Integer.parseInt(String.valueOf(params.get("port")));
			destUser  = String.valueOf(params.get("user"));
			destPwd   = String.valueOf(params.get("password"));
			//Sftp 접속
			connStr   = this.init(destSvr, destPort, destUser, destPwd);
			log.debug(methodName, "connStr:"+connStr);
			
			if(connStr.equals("00")){
				destDir  = String.valueOf(params.get("destDir")); 
				destFile = (String[])params.get("fileName");

				channelSftp.cd(destDir);
				
				for(String dFile : destFile) {
					channelSftp.rm(dFile);
				}
				
				resMap.put("retcode", messageManage.getMsgCd("SY_INFO_00"));
				resMap.put("retmsg",  messageManage.getMsgTxt("SY_INFO_00"));
			}else{
				resMap.put("retcode", messageManage.getMsgCd("IF_ERROR_104"));
				resMap.put("retmsg",  messageManage.getMsgTxt("IF_ERROR_104"));
			}
		}catch(Exception e){
			log.error(methodName, "Exception: "+ e.getMessage());
		}
		
		this.disconnect();
		
		return resMap;
	}
//	public Map<String,Object> delFile(Map<String,Object> params){
//		String methodName = "delFile";
//		
//		// fileName 유혀을 String 에서 String[]으로 변환
//		String oriFileName   = String.valueOf(params.get("fileName"));
//		String[] tmpFileName = {oriFileName};
//		
//		params.put("fileName", tmpFileName);
//		
//		
//		return delFiles(params);
//	}
}
