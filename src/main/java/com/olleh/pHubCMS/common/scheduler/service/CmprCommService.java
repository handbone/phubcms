package com.olleh.pHubCMS.common.scheduler.service;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.scheduler.dao.PHubDAO;
import com.olleh.pHubCMS.common.scheduler.task.settleBank.GetWorkIndClipTask;
import com.olleh.pHubCMS.common.scheduler.utils.CommonUtil;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 클립포인트 대사 Service
 * @Class Name : CmprClipPointService
 * @author lys
 * @since 2018.10.05
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.05   lys        최초생성
 * </pre>
 */
@Service
public class CmprCommService {
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	MessageManage messageManage;
	
	@Resource
	PHubDAO pHubDAO;
	
	@Autowired
	CommonUtil commonUtil;
	
	@Autowired
	GetWorkIndClipTask getWorkIndClipTask;
	
	/**
	 * <pre> 배치실행로그 저장 </pre>
	 * 
	 * @param 로그번호, 일대사번호, 일대사일자, task아이디,대상건수,처리건수,대상파일경로,처리파일경로,결과코드,결과메시지,등록자ID
	 * @return 
	 * @see
	 */	
	public int insertAdmBatchLog(Map<String,Object> params) {
		// INSERT ADM_BATCH_LOG
		return pHubDAO.insertAdmBatchLog(params);
	}
	
	/**
	 * <pre> 배치실행로그 업데이트 </pre>
	 * 
	 * @param 결과코드,결과메시지,대상건수,처리건수,(key:일대사번호, 일대사일자, task아이디)
	 * @return void
	 * @see
	 */		
	public int updateAdmBatchLog(Map<String,Object> params) throws Exception {
		// UPDATE ADM_BATCH_LOG
		return pHubDAO.updateAdmBatchLog(params);
	}
	
	/**
	 * <pre> 배치실행로그마스터 insert </pre>
	 * 
	 * @param 
	 * @return int
	 * @see
	 */		
	public int saveAdmBatchLogM(Map<String,Object> params){
		log.debug("saveClCmprInfo", "배치작업로그M insert");
		return pHubDAO.insertAdmBatchLogM(params);
	}	
	
	/**
	 * <pre> 배치실행로그마스터 업데이트 </pre>
	 * 
	 * @param 
	 * @return int
	 * @see
	 */		
	public int updateAdmBatchLogM(Map<String,Object> params) {
		int iRes = pHubDAO.updateAdmBatchLogM(params);
		log.debug("saveClCmprInfo", "배치작업로그M 수정 결과 : " + iRes);
		return iRes;
	}

	
	/**
	 * <pre> 로그파라미터 세팅 </pre>
	 * 
	 * @param 
	 * @return 로그파라미터 map
	 * @see
	 */		
	public Map<String,Object> getLogMap(String ddCmprNo, String cmprDd, String taskId, String rsltCode, String rsltMsg, String userId) {
		Map<String,Object> logMap = new HashMap<String,Object>();
		
		//로그 파라미터 세팅
		logMap.put("dd_cmpr_no",     ddCmprNo);
		logMap.put("cmpr_dd",        cmprDd);
		logMap.put("task_id",        taskId);
		logMap.put("tgt_cnt",        0);
		logMap.put("cmpr_cnt",       0);
		logMap.put("tgt_file_path",  commonUtil.getFilePath("cm")); //세틀뱅크 대상 경로
		logMap.put("cmpr_file_path", commonUtil.getFilePath("cp")); //클립포인트 대상 경로
		logMap.put("rslt_code",      StringUtil.nvl(rsltCode, messageManage.getMsgCd("SY_INFO_00")));
		logMap.put("rslt_msg",       StringUtil.nvl(rsltMsg,  messageManage.getMsgTxt("SY_INFO_00")));
		logMap.put("user_id",        StringUtil.nvl(userId));
		
		// return
		return logMap;
	}
	

	/**
	 * <pre> 배치마스터 seq 조회 </pre>
	 * 
	 * @param 
	 * @return 일대사번호 Map
	 * @see
	 * 		ADM_BATCH_LOG_M.DD_CMPR_NO
	 * 		SEQ_CM_ID
	 */		
	public String getSeqCmId() throws Exception{
		String methodName = "getSeqCmId";
		String resStr = "";
		
		Map dataMap = pHubDAO.getSeqCmId();
		if(dataMap!=null){
			resStr = StringUtil.nvl(dataMap.get("DD_CMPR_NO"));
		}
		
		return resStr;
	}	
}
