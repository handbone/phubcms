package com.olleh.pHubCMS.common.scheduler.dao;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;

@Repository("GSDAO")
public class GSDAO extends AbstractDAO{
	/**
	 * <pre> 상품 정보 tmp 테이블 데이터 삭제 </pre>
	 * 
	 * @param 
	 * @return 
	 * @see
	 */		
	public void deleteGoodsTmp(Object params){
		delete("mybatis.scheduler.deleteGoodsTmp", params);
	}
	
	/**
	 * <pre> 상품 정보 tmp 테이블 데이터 insert </pre>
	 * 
	 * @param 
	 * @return 
	 * @see
	 */
	public void insertGoodsTmp(Object params){
		insert("mybatis.scheduler.insertGoodsTmp", params);
	}
	
	/**
	 * <pre> 
	 * goods_tmp -> goods_tmp_tmp
	 * goods -> goods_tmp
	 * goods_tmp_tmp -> goods
	 * </pre>
	 * 
	 * @param 
	 * @return 
	 * @see
	 */
	public void renameGoodsTask(Object params){
		update("mybatis.scheduler.renameGoodsTmp", params);
		update("mybatis.scheduler.renameGoods", params);
		update("mybatis.scheduler.renameGoodsTmpTmp", params);
	}
}
