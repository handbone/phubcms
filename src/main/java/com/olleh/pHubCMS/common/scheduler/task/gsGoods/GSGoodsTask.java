package com.olleh.pHubCMS.common.scheduler.task.gsGoods;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.api.service.GiftiShowRestHttpService;
import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.ResultVo;
import com.olleh.pHubCMS.common.scheduler.dao.GSDAO;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 상품 정보 리스트 조회 TASK
 * @Class Name : GSGoodsTask
 * @author hwlee
 * @since 2019.04.10
 * @version 0.1
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2019.04.10   hwlee      최초생성
 * </pre>
 */

@Service
public class GSGoodsTask {
	private Logger log = new Logger(this.getClass());
	
	@Resource
	GSDAO gsDAO;
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
	GiftiShowRestHttpService gsRestHttpService;
	
	/**
	 * <pre> 상품 정보 리스트 조회 </pre>
	 * 
	 * @param 
	 * @return List
	 * @see
	 */	
	public Map<String, Object> getGSGoodsList(){
		String methodName = "getGSGoodsList";
		log.debug(methodName, "START");
		
		Map<String, Object> resultMap = new HashMap<String, Object>();
		List<Map<String, Object>> goodsList = new ArrayList<Map<String, Object>>();
		
		try {
			ResultVo resultVo = gsRestHttpService.sendHttpPostGiftiShowGoods();
			resultMap.put("rst_cd", resultVo.getRstCd());
			resultMap.put("rst_msg", resultVo.getRstMsg());
			
			//기프티쇼 결과코드 성공일 경우에만 return map에 리스트 저장
			if("0000".equals(resultVo.getRstCd())){
				goodsList = resultVo.getList();
				resultMap.put("goodsList", goodsList);
			}
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
		}
		
		log.debug(methodName, "END");
		return resultMap;
	}
	
	/**
	 * <pre> 상품 정보 tmp 테이블 현행화 </pre>
	 * 
	 * @param 
	 * @return void
	 * @see
	 */	
	public Map<String, Object> initTmpData(){
		String methodName = "initTmpData";
		log.debug(methodName, "START");
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			//1. 기프티쇼에서 상품 정보 수신
			map = getGSGoodsList();
			
			//기프티쇼 결과코드 성공일 경우에만 동작
			if("0000".equals(StringUtil.nvl(map.get("rst_cd")))){
				List<Map<String, Object>> goodsList = (List<Map<String, Object>>)map.get("goodsList");
				//2. 상품 정보 tmp 데이터 삭제
				gsDAO.deleteGoodsTmp("");
				
				//3. 상품 정보 tmp 데이터 insert
				paramMap.put("goodsList", goodsList);
				gsDAO.insertGoodsTmp(paramMap);
			}
		} catch (Exception e) {
			log.error(methodName, "[Exception]fail!");
    		log.error(methodName, "Message: " + StringUtil.nvl(e.getMessage()));
    		map.put("rst_cd", messageManage.getMsgCd("IF_ERROR_900"));
    		map.put("rst_msg", e.getMessage());
		}
		log.debug(methodName, "END");
		
		return map;
	}
	
	/**
	 * <pre> 상품 정보 테이블 현행화 </pre>
	 * 
	 * @param 
	 * @return void
	 * @see
	 */	
	public void initGoodsData(){
		String methodName = "initGoodsData";
		log.debug(methodName, "START");
		
		//1. goods_tmp -> goods_tmp_tmp
		//2. goods -> goods_tmp
		//3. goods_tmp_tmp -> goods
		gsDAO.renameGoodsTask("");
		
		log.debug(methodName, "END");
	}
	
}
