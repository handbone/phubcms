package com.olleh.pHubCMS.common.scheduler.task;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.scheduler.utils.CommonUtil;
import com.olleh.pHubCMS.common.scheduler.utils.SFtpUtil;

@Service
public class SendCmprFileTask {	
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	SFtpUtil sFtpUtil;
	
	@Autowired
	CommonUtil commonUtil;
	
	@Autowired
	MessageManage messageManage;
	
	/**
	 * <pre> 생성된 PG대사파일을 T-POINTHUBWEB으로 SFTP전송 </pre>
	 * 
	 * @param 거래내역 리스트
	 * @return 대사 파일명
	 * @see
	 */		
	public Map<String,Object> sendFile(String fileName) throws Exception{
		String methodName = "sendFile";
		
		Map<String,Object> resMap = new HashMap<String,Object>();
		
		try{
			//PG와의 SFtp 연동을 위한 파라미터 조회
    		Map<String,Object> dat = commonUtil.getPropPG();
    		
    		Map<String,Object> params = new HashMap<String,Object>();
        	params.put("host", dat.get("cm_host"));
        	params.put("port", Integer.parseInt(dat.get("cm_port").toString()));
        	params.put("userName", dat.get("cm_user"));
        	params.put("password", dat.get("cm_pwd"));
        	params.put("destDir", dat.get("cm_dir"));
        	params.put("fileName", fileName);
        	
    		resMap = sFtpUtil.sendFile(params);
    		log.debug(methodName, "sendFile:"+resMap.toString());
		}catch(Exception e){
			log.printStackTracePH(methodName, e);
			resMap.put("retcode", messageManage.getMsgCd("CM_ERROR_605"));
			resMap.put("retmsg",  messageManage.getMsgTxt("CM_ERROR_605"));
		}

		return resMap;
	}	
}
