package com.olleh.pHubCMS.common.scheduler.dao;

import java.util.Map;
import org.springframework.stereotype.Repository;
import com.olleh.pHubCMS.common.dao.AbstractDAO;

@Repository("privacyDAO")
public class PrivacyDAO extends AbstractDAO {

	/**
	 * <pre> 가족지원현황 백업 </pre>
	 * 
	 * @param  미접속기준일
	 * @return 처리건수
	 * @see
	 */	
	public int insertExFpFmlySupot   (Map<String, Object> params){   
		return insert("mybatis.scheduler.privacy.insertExFpFmlySupot", params); 
	}
	
	/**
	 * <pre> 가족지원현황 정리 </pre>
	 * 
	 * @param  미접속기준일
	 * @return 처리건수
	 * @see
	 */
	public int deleteFpFmlySupot(Map<String, Object> params){   
		return delete("mybatis.scheduler.privacy.deleteFpFmlySupot", params); 
	}
	
	/**
	 * <pre> mms전송이력 백업 </pre>
	 * 
	 * @param  미접속기준일
	 * @return 처리건수
	 * @see
	 */
	public int insertExPhMmsSendHist(Map<String, Object> params){   
		return insert("mybatis.scheduler.privacy.insertExPhMmsSendHist", params); 
	}
	
	/**
	 * <pre> mms전송이력 정리</pre>
	 * 
	 * @param  미접속기준일
	 * @return 처리건수
	 * @see
	 */
	public int deletePhMmsSendHist(Map<String, Object> params){   
		return delete("mybatis.scheduler.privacy.deletePhMmsSendHist", params); 
	}
	
	/**
	 * <pre> 쿠폰상태이력 백업 </pre>
	 * 
	 * @param  미접속기준일
	 * @return 처리건수
	 * @see
	 */
	public int insertExPhCpnChngHist(Map<String, Object> params){   
		return insert("mybatis.scheduler.privacy.insertExPhCpnChngHist", params); 
	}
	
	/**
	 * <pre> 쿠폰상태이력 정리</pre>
	 * 
	 * @param  미접속기준일
	 * @return 처리건수
	 * @see
	 */
	public int deletePhCpnChngHist(Map<String, Object> params){   
		return delete("mybatis.scheduler.privacy.deletePhCpnChngHist", params); 
	}
	
	/**
	 * <pre> 쿠폰발행 백업 </pre>
	 * 
	 * @param  미접속기준일
	 * @return 처리건수
	 * @see
	 */
	public int insertExPhCpnIsueInfo(Map<String, Object> params){   
		return insert("mybatis.scheduler.privacy.insertExPhCpnIsueInfo", params); 
	}
	
	/**
	 * <pre> 쿠폰발행 정리 </pre>
	 * 
	 * @param  미접속기준일
	 * @return 처리건수
	 * @see
	 */
	public int deletePhCpnIsueInfo(Map<String, Object> params){   
		return delete("mybatis.scheduler.privacy.deletePhCpnIsueInfo", params); 
	}
	
	/**
	 * <pre> 거래요청내역 백업 </pre>
	 * 
	 * @param  미접속기준일
	 * @return 처리건수
	 * @see
	 */
	public int insertExPhDealDtl(Map<String, Object> params){   
		return insert("mybatis.scheduler.privacy.insertExPhDealDtl", params); 
	}
	
	/**
	 * <pre> 거래요청내역 정리 </pre>
	 * 
	 * @param  미접속기준일
	 * @return 처리건수
	 * @see
	 */
	public int deletePhDealDtl(Map<String, Object> params){   
		return delete("mybatis.scheduler.privacy.deletePhDealDtl", params); 
	}	
	
	/**
	 * <pre> 거래원장 백업 </pre>
	 * 
	 * @param  미접속기준일
	 * @return 처리건수
	 * @see
	 */
	public int insertExPhDealInfo(Map<String, Object> params){   
		return insert("mybatis.scheduler.privacy.insertExPhDealInfo", params); 
	}	
	
	/**
	 * <pre> 거래원장 정리 </pre>
	 * 
	 * @param  미접속기준일
	 * @return 처리건수
	 * @see
	 */
	public int deletePhDealInfo(Map<String, Object> params){   
		return delete("mybatis.scheduler.privacy.deletePhDealInfo", params); 
	}
	
	/**
	 * <pre> 거래원장내역 백업 </pre>
	 * 
	 * @param  미접속기준일
	 * @return 처리건수
	 * @see
	 */
	public int insertExPhDealReqDtl(Map<String, Object> params){   
		return insert("mybatis.scheduler.privacy.insertExPhDealReqDtl", params); 
	}
	
	/**
	 * <pre> 거래원장내역 정리 </pre>
	 * 
	 * @param  미접속기준일
	 * @return 처리건수
	 * @see
	 */
	public int deletePhDealReqDtl(Map<String, Object> params){   
		return delete("mybatis.scheduler.privacy.deletePhDealReqDtl", params); 
	}
	
	/**
	 * <pre> 거래요청정보 백업 </pre>
	 * 
	 * @param  미접속기준일
	 * @return 처리건수
	 * @see
	 */
	public int insertExPhDealReqInfo(Map<String, Object> params){   
		return insert("mybatis.scheduler.privacy.insertExPhDealReqInfo", params); 
	}
	
	/**
	 * <pre> 거래요청정보 정리 </pre>
	 * 
	 * @param  미접속기준일
	 * @return 처리건수
	 * @see
	 */
	public int deletePhDealReqInfo(Map<String, Object> params){   
		return delete("mybatis.scheduler.privacy.deletePhDealReqInfo", params); 
	}
	
	/**
	 * <pre> 포인트고객 전번 백업 </pre>
	 * 
	 * @param  미접속기준일
	 * @return 처리건수
	 * @see
	 */
	public int insertExPhPntCustCtn(Map<String, Object> params){   
		return insert("mybatis.scheduler.privacy.insertExPhPntCustCtn", params); 
	}

	/**
	 * <pre> 포인트고객 전번 정리 </pre>
	 * 
	 * @param  미접속기준일
	 * @return 처리건수
	 * @see
	 */
	public int deletePhPntCustCtn(Map<String, Object> params){   
		return delete("mybatis.scheduler.privacy.deletePhPntCustCtn", params); 
	}
	
	/**
	 * <pre> 포인트고객 백업 </pre>
	 * 
	 * @param  미접속기준일
	 * @return 처리건수
	 * @see
	 */
	public int insertExPhPntCustInfo(Map<String, Object> params){   
		return insert("mybatis.scheduler.privacy.insertExPhPntCustInfo", params); 
	}
	
	/**
	 * <pre> 포인트 고객 정리 </pre>
	 * 
	 * @param  미접속기준일
	 * @return 처리건수
	 * @see
	 */
	public int deletePhPntCustInfo(Map<String, Object> params){   
		return delete("mybatis.scheduler.privacy.deletePhPntCustInfo", params); 
	}	

	/**
	 * <pre> 포인트고객 백업 </pre>
	 * 
	 * @param  미접속기준일
	 * @return 처리건수
	 * @see
	 */
	public int insertExAdmUserInfo(Map<String, Object> params){   
		return insert("mybatis.scheduler.privacy.insertExAdmUserInfo", params); 
	}
	
	/**
	 * <pre> 포인트 고객 정리 </pre>
	 * 
	 * @param  미접속기준일
	 * @return 처리건수
	 * @see
	 */
	public int deleteAdmUserInfo(Map<String, Object> params){   
		return delete("mybatis.scheduler.privacy.deleteAdmUserInfo", params); 
	}
	
	/**
	 * <pre> 일대사정보 시퀀스 </pre>
	 * 
	 * @param 
	 * @return 일대사번호(lys force)
	 * @see
	 */		
	public Map getSeqBaId(){
		return selectOne("mybatis.scheduler.selectSeqBaId");
	}	
	
}
