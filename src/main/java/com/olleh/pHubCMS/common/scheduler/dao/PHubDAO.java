package com.olleh.pHubCMS.common.scheduler.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;
import com.olleh.pHubCMS.common.utils.StringUtil;
import com.olleh.pHubCMS.scheduler.model.PHubToPGVO;

@Repository("pHubDAO")
@SuppressWarnings({"unchecked","rawtypes"})
public class PHubDAO extends AbstractDAO {
	//private ApplicationContext ctx = new ClassPathXmlApplicationContext("config/spring/context-*.xml");
	//private SqlSessionFactory factory = (SqlSessionFactory) ctx.getBean("sqlSession");
	//private SqlSession sqlSession = factory.openSession();
	
	
	/**
	 * <pre> Clip 대사처리여부 체크 </pre>
	 * 
	 * @param 거래기준일
	 * @return 대사처리 여부 work_ind (Y/N)
	 * @see
	 */		
	public Map selectWorkIndClip(Object params){
		return selectOne("mybatis.scheduler.selectWorkIndClip", params);
	}	
	/**
	 * <pre> 입력받은 대사일자로 자료생성처리가 되었는지 확인 </pre>
	 * 
	 * @param 거래기준일
	 * @return 대사처리 여부 work_ind (Y/N)
	 * @see
	 */		
	public Map selectWorkIndPG(Object params){
		return selectOne("mybatis.scheduler.selectWorkIndPG", params);
	}
	
	/**
	 * <pre> 포인트허브 거래내역 조회(포인트허브 to PG) </pre>
	 * 
	 * @param 거래기준일
	 * @return 포인트허브 거래내역 리스트
	 * @see
	 */	
	public List<PHubToPGVO> getPhubToPG(Map<String, Object> params){
		return selectList("mybatis.scheduler.getPHubToPG",params);
	}
	
	public List<PHubToPGVO> getPgCmprInfo(Map<String, Object> params){
		return selectList("mybatis.scheduler.getPgCmprInfo",params);
	}
	
	
	/**
	 * <pre> 포인트허브 일거래대사D </pre>
	 * 
	 * @param 저장조건Map
	 * @return 처리건수
	 * @see
	 */		
	public int insertPgCmprInfoD(Map<String,Object> params){
		return insert("mybatis.scheduler.insertPgCmprInfoD",params);
	}
	
	/**
	 * <pre> 포인트허브 일거래대사M </pre>
	 * 
	 * @param 저장조건Map
	 * @return 처리건수
	 * @see
	 */		
	public int insertPgCmprInfoM(Map<String,Object> params){
		return insert("mybatis.scheduler.insertPgCmprInfoM",params);
	}	
	
	/**
	 * <pre> 클립포인트 일대사문서 (클립 대사파일 파싱결과 저장) </pre>
	 * 
	 * @param 저장조건Map
	 * @return 처리건수
	 * @see
	 */		
	public int insertClCmprFileInfo(Map<String,Object> params){
		return insert("mybatis.scheduler.insertClCmprFileInfo",params);		
	}
	
	/**
	 * <pre> 클립포인트 대사파일 파싱처리 여부 </pre>
	 * 
	 * @param 파일생성일시
	 * @return work_ind(처리여부:Y/N) Map
	 * @see
	 */		
	public String selectClCmprFileInfo(Map<String,Object> params){
		Map map = selectOne("mybatis.scheduler.selectClCmprFileInfo",params);
		String resStr = StringUtil.nvl((map.get("work_ind")));
		return resStr;
	}
	
	/**
	 * <pre> 클립포인트 vs 포인트허브 대사 성공 건수 저장 </pre>
	 * 
	 * @param 거래일자
	 * @return 1 : 성공, 0 : 실패
	 * @see
	 */		
	public int insertClCmprSucs(Map<String,Object> params){
		return insert("mybatis.scheduler.insertClCmprSucs",params);
	}
	
	/**
	 * <pre> 클립일대사결과성공D_P (매입취소) 저장 </pre>
	 * 
	 * @param 거래일자
	 * @return 
	 * @see
	 */	
	public int insertClipPuchasCnclSucs(Map<String,Object> params) {
		return insert("mybatis.scheduler.insertClipPuchasCnclSucs",params);
	}
	
	/**
	 * <pre> 클립포인트 vs 포인트허브 대사 실패 건수 저장 </pre>
	 * 
	 * @param 거래일자
	 * @return 1 : 성공, 0 : 실패
	 * @see
	 */		
	public int insertClCmprFail(Map<String,Object> params){
		return insert("mybatis.scheduler.insertClCmprFail",params);
	}	
	
	/**
	 * <pre> 클립포인트 vs 포인트허브 대사 카드별 건수 저장 </pre>
	 * 
	 * @param 거래일자
	 * @return 1 : 성공, 0 : 실패
	 * @see
	 */		
	public int insertClCmprPoint(Map<String,Object> params){
		return insert("mybatis.scheduler.insertClCmprPoint",params);
	}	
	
	/**
	 * <pre> 클립포인트 vs 포인트허브 대사 마스터 저장 </pre>
	 * 
	 * @param 대사일자, 전문파일명, 불일치건수, 일치건수, 총거래건수
	 * @return 1 : 성공, 0 : 실패
	 * @see
	 */		
	public int insertClCmprInfo(Map<String,Object> params){
		return insert("mybatis.scheduler.insertClCmprInfo",params);
	}
	/**
	 * <pre> 클립 대사파일에 없는 거래건에 대한 메시지 insert 처리 </pre>
	 * 
	 * @param 대사일자
	 * @return 1 : 성공, 0 : 실패
	 * @see
	 */		
	public int insertNoExistInClip(Map<String,Object> params){
		return update("mybatis.scheduler.insertNoExistInClip",params);
	}	
	/**
	 * <pre> 포인트허브에 없는 거래건에 대한 메시지 업데이트 처리 </pre>
	 * 
	 * @param 대사일자
	 * @return 1 : 성공, 0 : 실패
	 * @see
	 */		
	public int updateNoExistInPhub(Map<String,Object> params){
		return update("mybatis.scheduler.updateNoExistInPhub",params);
	}
	/**
	 * <pre> 일대사정보 시퀀스 </pre>
	 * 
	 * @param 
	 * @return 일대사번호
	 * @see
	 */		
	public Map getDdCmprNO(){
		return selectOne("mybatis.scheduler.selectDdCmprNo");
	}
	/**
	 * <pre> 일대사정보 시퀀스 </pre>
	 * 
	 * @param 
	 * @return 일대사번호
	 * @see
	 */		
	public Map getSeqCmId(){
		return selectOne("mybatis.scheduler.selectSeqCmId");
	}

	/**
	 * <pre> 배치실행로그 저장 </pre>
	 * 
	 * @param 로그번호, 일대사번호, 일대사일자, task아이디,대상건수,처리건수,대상파일경로,처리파일경로,결과코드,결과메시지,등록자ID
	 * @return 저장결과
	 * @see
	 */		
	public int insertAdmBatchLog(Map<String,Object> params){
		return insert("mybatis.scheduler.insertAdmBatchLog",params);
	}
	/**
	 * <pre> 배치실행로그 수정 </pre>
	 * 
	 * @param 
	 * @return 저장결과
	 * @see
	 */		
	public int updateAdmBatchLog(Map<String,Object> params){
		return update("mybatis.scheduler.updateAdmBatchLog",params);
	}
	/**
	 * <pre> 클립포인트 대상건수/작업건수 조회 </pre>
	 * 
	 * @param 일대사번호, 대사일자
	 * @return 처리결과
	 * @see
	 */		
	public Map getClCmprInfo(Map<String,Object> params){
		return selectOne("mybatis.scheduler.getClCmprInfo",params);
	}
	/**
	 * <pre> 클립포인트 대사마스터 수정  </pre>
	 * 
	 * @param 
	 * @return 처리결과
	 * @see
	 */	
	public int updateClCmprInfo(Map<String,Object> params){
		return update("mybatis.scheduler.updateClCmprInfo",params);
	}
	/**
	 * <pre> 배치로그상세 생성 </pre>
	 * 
	 * @param 
	 * @return 처리결과  
	 * @see
	 */
	public int insertAdmBatchLogM(Map<String,Object> params){
		return insert("mybatis.scheduler.insertAdmBatchLogM",params);
	}
	/**
	 * <pre> 파일 작업후 배치로그 마스터에 결과값과 파일명 업데이트 </pre>
	 * 
	 * @param 일대사번호,대사일자,작업명,사용자아이디
	 * @return 처리결과
	 * @see
	 */
	public int updateAdmBatchLogM(Map<String,Object> params){
		return update("mybatis.scheduler.updateAdmBatchLogM",params);
	}
	/**
	 * <pre> 클립포인트 대사결과 마스터에 대한 업데이트 </pre>
	 * 
	 * @param 
	 * @return 처리결과
	 * @see
	 */
	public int updateClCmprInfo2(Map<String,Object> params){
		return update("mybatis.scheduler.updateClCmprInfo2",params);
	}
	/**
	 * <pre> 클립포인트 대사처리에서 배치로그 마스터 업데이트할때 사용(최종) </pre>
	 * 
	 * @param  결과코드,결과메시지,총거래건수
	 * @return 처리결과
	 * @see
	 */	
	public int updateAdmBatchLogM2(Map<String,Object> params){
		return update("mybatis.scheduler.updateAdmBatchLogM2",params);
	}
	/**
	 * <pre> PG일대사상세 데이타 삭제 </pre>
	 * 
	 * @param  대사일자
	 * @return 처리결과
	 * @see
	 */	
	public int deletePgCmprInfoD(Map<String,Object> params){
		return update("mybatis.scheduler.deletePgCmprInfoD",params);
	}
	/**
	 * <pre> PG일대사마스터 데이타 삭제 </pre>
	 * 
	 * @param  대사일자
	 * @return 처리결과
	 * @see
	 */		
	public int deletePgCmprInfoM(Map<String,Object> params){
		return update("mybatis.scheduler.deletePgCmprInfoM",params);
	}
	/**
	 * <pre> 클립포인트 대사파일에서 중복된 데이타 검색 </pre>
	 * 
	 * @param  대사일자
	 * @return 클립포인트 대사파일에서 중복된 데이타 리스트
	 * @see
	 */		
	public Map getDuplicatedClippoint(Map<String,Object> params){
		return selectOne("mybatis.scheduler.selectDuplicatedClippoint",params);
	}
	/**
	 * <pre> 클립포인트 대사마스터 삭제 </pre>
	 * 
	 * @param  대사일자
	 * @return 처리결과
	 * @see
	 */		
	public int deleteClCmprInfo(Map<String,Object> params){
		return update("mybatis.scheduler.deleteClCmprInfo",params);
	}
	/**
	 * <pre> 클립포인트 파싱자료 삭제 </pre>
	 * 
	 * @param  대사일자
	 * @return 처리결과
	 * @see
	 */			
	public int deleteClCmprFileInfo(Map<String,Object> params){
		return update("mybatis.scheduler.deleteClCmprFileInfo",params);
	}
	/**
	 * <pre> 클립포인트 대사성공 삭제 </pre>
	 * 
	 * @param  대사일자
	 * @return 처리결과
	 * @see
	 */			
	public int deleteClCmprSucs(Map<String,Object> params){
		return update("mybatis.scheduler.deleteClCmprSucs",params);
	}
	/**
	 * <pre> 클립포인트 대사실패 삭제 </pre>
	 * 
	 * @param  대사일자
	 * @return 처리결과
	 * @see
	 */			
	public int deleteClCmprFail(Map<String,Object> params){
		return update("mybatis.scheduler.deleteClCmprFail",params);
	}
	/**
	 * <pre> 클립포인트 대사카드별 삭제 </pre>
	 * 
	 * @param  대사일자
	 * @return 처리결과
	 * @see
	 */			
	public int deleteClCmprPoint(Map<String,Object> params){
		return update("mybatis.scheduler.deleteClCmprPoint",params);
	}
	/**
	 * <pre> 클립포인트 대사파일 파싱결과 데이타 row수 조회 </pre>
	 * 
	 * @param  일대사번호
	 * @return 조회수(CNT)
	 * @see
	 */		
	public Map selectCountClipPoint(Map<String,Object> params){
		return selectOne("mybatis.scheduler.selectCountClipPoint",params);
	}
	/**
	 * <pre> PG 대사파일 생성대상 조회 </pre>
	 * 
	 * @param  대사일자
	 * @return 조회수(CNT)
	 * @see
	 */		
	public Map selectCountPG(Map<String,Object> params){
		return selectOne("mybatis.scheduler.selectCountPG",params);
	}
	
	/**
	 * <pre> 클립포인트 대사 실패건 조회  </pre>
	 * 
	 * @param  대사번호
	 * @return 실패대사정보
	 * @see
	 */		
	public List selectClCmprFailByDdCmprNo(Map<String,Object> params){
		return selectList("mybatis.scheduler.selectClCmprFailByDdCmprNo",params);
	}
	
	/**
	 * <pre> 클립포인트 대사 실패건 조회 (매입취소)  </pre>
	 * 
	 * @param  대사번호
	 * @return 실패대사정보
	 * @see
	 */	
	public List selectClCmprFailByPuchasCncl(Map<String,Object> params) {
		return selectList("mybatis.scheduler.selectClCmprFailByPuchasCncl",params);
	}
	
	/**
	 * <pre> 클립포인트 대사 실패사유 갱신  </pre>
	 * 
	 * @param  대사번호
	 * @return 실패대사정보
	 * @see
	 */		
	public int updateClCmprFailForErrInfo(Map<String,Object> params){
		return update("mybatis.scheduler.updateClCmprFailForErrInfo",params);
	}
	
	
	/**
	 * <pre> [통계]일별 거래내역 통계 생성 쿼리  </pre>
	 * 
	 * @param  from 통계일자(yyymmdd), to 통계일자(yyymmdd), 작업자
	 * @return 등록건수
	 * @see
	 */		
	public int insertStDealInfoByDay(Map<String,Object> params){
		return update("mybatis.scheduler.insertStDealInfoByDay",params);
	}
	/**
	 * <pre> [통계]일별 거래내역 통계 삭제 </pre>
	 * 
	 * @param  통계일자
	 * @return 
	 * @see
	 */		
	public int deleteStDealInfoByDealDd(Map<String,Object> params){
		return delete("mybatis.scheduler.deleteStDealInfoByDealDd",params);
	}
	
	
	/**
	 * <pre> [통계]일별 거래내역상세 통계 생성 쿼리  </pre>
	 * 
	 * @param  from 통계일자(yyymmdd), to 통계일자(yyymmdd), 작업자
	 * @return 등록건수
	 * @see
	 */		
	public int insertStDealDtlByDay(Map<String,Object> params){
		return update("mybatis.scheduler.insertStDealDtlByDay",params);
	}
	/**
	 * <pre> [통계]일별 거래내역상세 통계 삭제 </pre>
	 * 
	 * @param  통계일자
	 * @return 
	 * @see
	 */		
	public int deleteStDealDtlByDealDd(Map<String,Object> params){
		return delete("mybatis.scheduler.deleteStDealDtlByDealDd",params);
	}	
	
	/**
	 * <pre> [통계]일별 I/F오류내역 통계 생성 쿼리  </pre>
	 * 
	 * @param  from 통계일자(yyymmdd), to 통계일자(yyymmdd), 작업자
	 * @return 등록건수
	 * @see
	 */		
	public int insertStIfErrInfoByDay(Map<String,Object> params){
		return update("mybatis.scheduler.insertStIfErrInfoByDay",params);
	}
	
	/**
	 * <pre> [통계]일별 I/F오류내역 통계 삭제 </pre>
	 * 
	 * @param  통계일자
	 * @return 
	 * @see
	 */		
	public int deleteStIfErrInfoByIfDd(Map<String,Object> params){
		return delete("mybatis.scheduler.deleteStIfErrInfoByIfDd",params);
	}	
	
	/**
	 * <pre> [통계]일별 결제단계별 이용건수/이용자수 통계 생성 쿼리  </pre>
	 * 
	 * @param  from 통계일자(yyymmdd), to 통계일자(yyymmdd), 작업자
	 * @return 등록건수
	 * @see
	 */		
	public int insertStUseByStepByDay(Map<String,Object> params) {
		return update("mybatis.scheduler.insertStUseByStepByDay", params);
	}
	
	/**
	 * <pre> [통계]결제단계별 이용건수/이용자수 삭제 </pre>
	 * 
	 * @param  통계일자
	 * @return 
	 * @see
	 */	
	public int deleteStUseByStepByDealDd(Map<String,Object> params) {
		return delete("mybatis.scheduler.deleteStUseByStepByDealDd",params);
	}
	
	/**
	 * <pre> [통계]일별 가맹점별 결제 실패 원인별 이용건수/이용자수 통계 데이터 생성 쿼리  </pre>
	 * 
	 * @param  from 통계일자(yyymmdd), to 통계일자(yyymmdd), 작업자
	 * @return 등록건수
	 * @see
	 */
	public int insertStPayErrByCprtByDay(Map<String,Object> params) {
		return update("mybatis.scheduler.insertStPayErrByCprtByDay", params);
	}
	
	/**
	 * <pre> [통계]가맹점별 결제 실패 원인별 이용건수/이용자수 삭제 </pre>
	 * 
	 * @param  통계일자
	 * @return 
	 * @see
	 */	
	public int deleteStPayErrByCprtByDealDd(Map<String,Object> params) {
		return delete("mybatis.scheduler.deleteStPayErrByCprtByDealDd",params);
	}
	
	/**
	 * <pre> [통계]일별 포인트제공처별(카드사) 결제 실패 원인별 이용건수/이용자수 통계 데이터 생성 쿼리  </pre>
	 * 
	 * @param  from 통계일자(yyymmdd), to 통계일자(yyymmdd), 작업자
	 * @return 등록건수
	 * @see
	 */
	public int insertStPayErrByPntByDay(Map<String,Object> params) {
		return update("mybatis.scheduler.insertStPayErrByPntByDay", params);
	}
	
	/**
	 * <pre> [통계]포인트제공처별(카드사) 결제 실패 원인별 이용건수/이용자수 삭제 </pre>
	 * 
	 * @param  통계일자
	 * @return 
	 * @see
	 */	
	public int deleteStPayErrByPntByDealDd(Map<String,Object> params) {
		return delete("mybatis.scheduler.deleteStPayErrByPntByDealDd",params);
	}
	
	/**
	 * <pre> [통계]일별 이용자수 통계 데이터 생성 쿼리  </pre>
	 * 
	 * @param  from 통계일자(yyymmdd), to 통계일자(yyymmdd), 작업자
	 * @return 등록건수
	 * @see
	 */
	public int insertStDealReqUsersByDay(Map<String, Object> params) {
		return update("mybatis.scheduler.insertStDealReqUsersByDay", params);
	}
	
	/**
	 * <pre> [통계]일별 이용자수 통계 삭제 </pre>
	 * 
	 * @param  통계일자
	 * @return 
	 * @see
	 */
	public int deleteStDealReqUsersByDealDd(Map<String, Object> params) {
		return delete("mybatis.scheduler.deleteStDealReqUsersByDealDd", params);
	}
	
	/**
	 * <pre> [통계]제공처별 거래 이용자수 통계 데이터 생성 쿼리  </pre>
	 * 
	 * @param  from 통계일자(yyymmdd), to 통계일자(yyymmdd), 작업자
	 * @return 등록건수
	 * @see
	 */
	public int insertStDealReqPntUsersByDay(Map<String, Object> params) {
		return update("mybatis.scheduler.insertStDealReqPntUsersByDay", params);
	}
	
	/**
	 * <pre> [통계]제공처별 거래 이용자수 통계 삭제 </pre>
	 * 
	 * @param  통계일자
	 * @return 
	 * @see
	 */
	public int deleteStDealReqPntUsersByDealDd(Map<String, Object> params) {
		return delete("mybatis.scheduler.deleteStDealReqPntUsersByDealDd", params);
	}
	
	/**
	 * <pre> 매입취소 건 목록 얻기 </pre>
	 * 
	 * @param params
	 * @return
	 */
	public List selectPchasCnclByCmprNo(Map<String, Object> params) {
		return selectList("mybatis.scheduler.selectPchasCnclByCmprNo",params);
	}
}
