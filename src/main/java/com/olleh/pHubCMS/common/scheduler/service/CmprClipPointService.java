package com.olleh.pHubCMS.common.scheduler.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.scheduler.dao.PHubDAO;
import com.olleh.pHubCMS.common.scheduler.task.GetClipFileTask;
import com.olleh.pHubCMS.common.scheduler.task.GetClipParsingTask;
import com.olleh.pHubCMS.common.scheduler.task.PuchasCnclResultTask;
import com.olleh.pHubCMS.common.scheduler.task.SaveClipResultTask;
import com.olleh.pHubCMS.common.scheduler.task.settleBank.GetWorkIndClipTask;
import com.olleh.pHubCMS.common.scheduler.utils.CommonUtil;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 클립포인트 대사 Service
 * @Class Name : CmprClipPointService
 * @author lys
 * @since 2018.10.05
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.05   lys        최초생성
 * </pre>
 */
@Service
public class CmprClipPointService {
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	GetWorkIndClipTask getWorkIndClipTask;
	
	@Autowired
	GetClipFileTask getClipFileTask;
	
	@Autowired
	GetClipParsingTask getClipParsingTask;
	
	@Autowired
	SaveClipResultTask saveClipResultTask;
	
	@Autowired
	PuchasCnclResultTask puchasCnclResultTask;
	
	@Autowired
	CommonUtil commonUtil;
	
	@Resource
	PHubDAO pHubDAO;
	
	/**
	 * <pre> 해당 대사일자에 해당되는 클립포인트 대사정보 모두 삭제
	 *       - CL_CMPR_INFO,CL_CMPR_FILE_INFO,CL_CMPR_SUCS,CL_CMPR_FAIL,CL_CMPR_POINT 삭제 
	 * </pre>
	 * @param  대사일자
	 * @return 처리결과
	 * @see
	 */		
	public int setClipCmprInit(Map<String,Object> params){
		return getWorkIndClipTask.setClipCmprInit(params);
	}
	
	/**
	 * <pre> 일대사번호 조회 </pre>
	 * 
	 * @param 
	 * @return 일대사번호 Map
	 * @see
	 */		
	public String getDdCmprNo() throws Exception{
		String resStr = "";
		Map dataMap = pHubDAO.getDdCmprNO();
		if(dataMap!=null){
			resStr = StringUtil.nvl(dataMap.get("DD_CMPR_NO"));
		}
		return resStr;
	}
	
	/**
	 * <pre> 원격저장소에 있는 클립포인트 대사 파일을 로컬저장소로 다운로드 </pre>
	 * 
	 * @param 대사일자
	 * @return 대사일자, 다운로드된 파일명에 대한 리스트
	 * @see
	 */		
	public Map<String,Object> getClipFile(Map<String,Object> dataMap) throws Exception {
		//현재 실행중인 함수
		Map<String,Object> fileMap = new HashMap<String,Object>();
		List<Map<String,Object>> dataList = getClipFileTask.getClipFile(dataMap);
		
		// 첫번째 파일만 리턴 한다.
		if( !dataList.isEmpty() && dataList.size() > 0 ) {
			fileMap = dataList.get(0);
		}
		
		return fileMap;
	}
	
	/**
	 * <pre> 로컬저장소에 다운로드된 파일에 대해 파싱, 파싱한 결과를 저장 </pre>
	 * 
	 * @param 파일명
	 * @return retcode, retmsg
	 * @see
	 */	
	public Map<String,Object> getClipParsing(Map<String,Object> dataMap, String userId){
		//현재 실행중인 함수
		String methodName = "getClipParsing";
		Map<String,Object> resMap = new HashMap<String,Object>();
		try{
			resMap = getClipParsingTask.getClipParse(dataMap, userId);
			log.debug(methodName, resMap.toString());
		}catch(Exception e){
			log.printStackTracePH(methodName, e);
		}
		return resMap;
	}
	
	
	public int saveClCmprInfo(Map<String,Object> params){
		log.debug("saveClCmprInfo", "클립포인트일대사결과M 생성");
		return pHubDAO.insertClCmprInfo(params);
	}
	
	/**
	 * <pre>클립포인트일대사결과M 업데이트</pre>
	 * 
	 * @param  
	 * @return 
	 * @see
	 */		
	public int updateClCmprInfo(Map<String,Object> params){
		return pHubDAO.updateClCmprInfo(params);
	}
	
	/**
	 * <pre> 클립포인트 대사처리 저장 </pre>
	 * 
	 * @param 파일명
	 * @return retcode, retmsg
	 * @see
	 */		
	public Map<String,Object> saveClipResult(Map<String,Object> dataMap){
		//현재 실행중인 함수
		String methodName ="saveClipResult";
		log.debug(methodName, "클립포인트 대사처리 저장:"+dataMap.toString());

		return saveClipResultTask.saveClipResult(dataMap);
	}
	
	/**
	 * <pre> 매입취소 처리 </pre>
	 * 
	 * @param params
	 * @return
	 */
	public Map<String, Object> puchasCnclResult(Map<String, Object> params) {
		//현재 실행중인 함수
		String methodName ="puchasCnclResult";
		log.debug(methodName, "매입취소 처리:"+ params.toString());
		
		return puchasCnclResultTask.puchasCnclResult(params);
	}
	
	/**
	 * <pre> 클립포인트 대사마스터 업데이트 </pre>
	 * 
	 * @param 사용자아이디,일대사번호,대사일자
	 * @return 처리결과
	 * @see
	 */		
	public int updateClCmprInfo2(Map<String,Object> params){
		return pHubDAO.updateClCmprInfo2(params);
	}
	
	/**
	 * <pre> 클립포인트 대사처리에서 배치로그 마스터 업데이트할때 사용(최종) </pre>
	 * 
	 * @param  결과코드,결과메시지,총거래건수
	 * @return 처리결과
	 * @see
	 */	
	public int updateAdmBatchLogM2(Map<String,Object> params){
		return pHubDAO.updateAdmBatchLogM2(params);
	}
}
