package com.olleh.pHubCMS.common.scheduler.job;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.exception.UserException;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.ErrorInfoVO;
import com.olleh.pHubCMS.common.scheduler.dao.PHubDAO;
import com.olleh.pHubCMS.common.scheduler.service.CmprCommService;
import com.olleh.pHubCMS.common.scheduler.service.PHubStatService;
import com.olleh.pHubCMS.common.scheduler.utils.CommonUtil;
import com.olleh.pHubCMS.common.utils.DateUtils;
import com.olleh.pHubCMS.common.utils.JsonUtil;
import com.olleh.pHubCMS.common.utils.StringUtil;
/**
 * 포인트허브 통계데이터 생성 job
 * @Class Name : pHubStatJob
 * @author lys
 * @since 2018.10.23
 * @version 0.1
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.23   lys        최초생성
 * </pre>
 */
@Service("pHubStatJob")
public class PHubStatJob {
	private Logger log = new Logger(this.getClass());
	
	@Resource
	PHubDAO pHubDAO;
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
	CommonUtil commonUtil;
	
	@Autowired
	PHubStatService pHubStatService;
	
	@Autowired
	CmprCommService cmprCommService;
	

	
	/* 작업 Task 선언 */
	//primessageManagekIndClipTask workIndTask;    //대사처리여부 체크
	//private GetClipFileTask    clipFileTask;   //원격저장소 파일 다운로드
	//private GetClipParsingTask clipParseTask;  //로컬저장소 파일 파싱 및 저장
	//private SaveClipResultTask clipResultTask; //대사결과 저장
	
	/**
	 * <pre> job 실행 함수 </pre>
	 * 
	 * @param Job 실행 콘텍스트
	 * @return void
	 * @see
	 */		
	public void execute() throws JobExecutionException {
		//
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("cmprDd", commonUtil.getCurrentDate());
		param.put("userId", "btch");
		statProc(param);
	}
	
	/**
	 * <pre> 일대사 처리 </pre>
	 * 
	 * @param Job 실행 콘텍스트
	 * @return void
	 * @see
	 */	
	public void statProc(Map<String,Object> params) {
		String methodName = "statProc";
		log.debug(methodName, "Start statProc! (params=" + JsonUtil.toJson(params) + ")");
		
		//현재 실행중인 함수
		String stepCd = "0";
		String strDt  = "";	
		
		// set valiable
		String wrknNm       = this.getClass().getSimpleName();	// 배치작업명
		String ddCmprNo     = "";								// 일대사번호
		String cmprDd       = StringUtil.nvl(params.get("cmprDd"), commonUtil.getCurrentDate());	// 대사일(yyyymmdd) : 현재일자 기준 -1일이 기준일자
		String userId       = StringUtil.nvl(params.get("userId"), "btch");							// 작업자
		//log.debug(methodName,"cmprDd : " + runCmprDd);
		
		int tgtCnt       = 0;		// 원대상건수
		int cmprCnt      = 0;		// 대사결과 건수
		int ttlDealCnt   = 0;		// 총거래 건수
		String rsltCodeV = "";		// 결과코드  value
		String rsltMsgV  = "";		// 결과메시지 value
		
		// 파일맵
		Map<String, Object> fileMap = new HashMap<String, Object>();
		
		/* -----Set Parameter ---------------------------------------------------------------------------- */
		int idx = 0;
		Map<Integer, String> batchProcMap = new HashMap<Integer, String>();
		batchProcMap.put(idx++, "1.InitStatData");				// 데이터 초기화 (재실행시 기존데이터 삭제 후 재생성)
		batchProcMap.put(idx++, "2.MakeDealInfoTask");			// 거래내역 통계 데이터 생성(일별)
		batchProcMap.put(idx++, "3.MakeDealDtlTask");			// 거래내역상세 통계 데이터 생성(일별)
		batchProcMap.put(idx++, "4.MakeIfErrTask");				// I/F 에러 통계 데이터 생성 (일별)
		batchProcMap.put(idx++, "5.makeUseByStepD");		// 결제단계별 이용건수/이용자수 통계 데이터 생성 (일별)
		batchProcMap.put(idx++, "6.makePayErrByCprtD"); 	// 가맹점별 결제 실패 원인별 이용건수/이용자수 통계 데이터 생성 (일별)
		batchProcMap.put(idx++, "7.makePayErrByPntD");		// 포인트제공처별(카드사) 결제 실패 원인별 이용건수/이용자수 통계 데이터 생성 (일별)
		batchProcMap.put(idx++, "8.makeDealReqUserD");		// 이용자수 통계 데이터 생성 (일별)
		batchProcMap.put(idx++, "9.makeDealReqPUserD");		// 제공처별 거래 이용자수 통계 데이터 생성 (일별)
		
		// 대사진행단계
		int cmprProcStep = 0;
		
		try {
			// DECLARE
			Map<String,Object> paramMap = new HashMap<String,Object>();
			
			// 1. 일대사번호 추출
			stepCd   = "1";
			ddCmprNo = cmprCommService.getSeqCmId();
			
			// 에러 처리 해야 한다.
			if( "".equals(ddCmprNo) ) {
				log.debug(methodName,"일대사번호 추출 오류");
				throw new UserException(stepCd, messageManage.getMsgVOY("IF_INFO_104"));
			}
			
			/* -----INSERT INTO ADM_BATCH_LOG_M -------------------------------------------------------------- */
			//3. 로그 마스터 생성
			Map<String,Object> logMstMap = new HashMap<String,Object>();
			logMstMap.put("dd_cmpr_no", ddCmprNo);
			logMstMap.put("cmpr_dd",    cmprDd);
			logMstMap.put("wrkn_nm",    wrknNm);
			logMstMap.put("user_id",    userId);
			int iLogM = cmprCommService.saveAdmBatchLogM(logMstMap);

			// 단계별 대사 처리
			for(int i=0; i < idx; i++) {
				// init
				cmprProcStep++;
				strDt = DateUtils.defaultDate() + DateUtils.defaultTime();
				
				// 대사결과 셋팅 (성공)
				rsltCodeV = messageManage.getMsgCd("SY_INFO_00");
				rsltMsgV  = messageManage.getMsgTxt("SY_INFO_00");
				
				tgtCnt  = 1;
				cmprCnt = 1;
				
			    switch(i) {
		        case 0 : 
					// 1. 해당 대사일자의 기존 대사데이터 모두 삭제
					Map<String,Object> initMap = new HashMap<String,Object>();
					initMap.put("dealDd", cmprDd);
					
					/* -----DELETE ST_DEAL_INFO, ST_IF_ERR_INFO ----------------------------------------------------------------- */
					tgtCnt = pHubStatService.initPHubStatData(initMap);
					log.debug(methodName,"initPHubStatData Result="+tgtCnt);					
					break;
		        case 1 : 
		        	// 2.MakeDealInfoTask
		        	// 거래내역 통계 데이터 생성(일별)
					/* -----INSERT ST_DEAL_INFO ----------------------------------------------------------------- */
		        	paramMap.put("dealDt",   cmprDd);
		        	paramMap.put("dealDtTo", DateUtils.calcDate(cmprDd, 1));
		        	paramMap.put("user_id",  userId);
		        	
					tgtCnt = pHubStatService.makeStDealInfoByDay(paramMap);
					//ttlDealCnt += tgtCnt;	// 거래내역 통계는 건수에 count하지 않는다. (거래내역 상세통계만 반영)
					log.debug(methodName,"makeStDealInfoByDay Result="+tgtCnt);					
					break;
		        case 2 : 
		        	// 3.MakeDealDtlTask
		        	// 거래내역상세 통계 데이터 생성(일별)
					/* -----INSERT ST_DEAL_INFO ----------------------------------------------------------------- */
		        	paramMap.put("dealDt",   cmprDd);
		        	paramMap.put("dealDtTo", DateUtils.calcDate(cmprDd, 1));
		        	paramMap.put("user_id",  userId);
		        	
					tgtCnt = pHubStatService.makeStDealDtlByDay(paramMap);
					ttlDealCnt += tgtCnt;
					log.debug(methodName,"makeStDealDtlByDay Result="+tgtCnt);					
					break;					
		        case 3 : 
		        	// 4.MakeIfErrTask
		        	// I/F 에러 통계 데이터 생성 (일별)
					/* -----INSERT ST_IF_ERR_INFO ----------------------------------------------------------------- */
		        	// 이전 단계에서 셋팅된 파라미터 그대로 사용
		        	//paramMap.put("dealDt",   cmprDd);
		        	//paramMap.put("dealDtTo", DateUtils.calcDate(cmprDd, 1));
		        	//paramMap.put("user_id",  userId);		        	
					tgtCnt = pHubStatService.makeStIfErrInfoByDay(paramMap);
					ttlDealCnt += tgtCnt;
					log.debug(methodName,"makeStIfErrInfoByDay Result="+tgtCnt);
					break;
		        case 4:
		        	// 5.makeStUseByStepByDay
		        	// 결제단계별 이용건수/이용자수 통계 데이터 통계 데이터 생성 (일별)
					/* -----INSERT ST_USE_BY_STEP ----------------------------------------------------------------- */
		        	// 이전 단계에서 셋팅된 파라미터 그대로 사용
		        	//paramMap.put("dealDt",   cmprDd);
		        	//paramMap.put("dealDtTo", DateUtils.calcDate(cmprDd, 1));
		        	//paramMap.put("user_id",  userId);	
		        	tgtCnt = pHubStatService.makeStUseByStepByDay(paramMap);
		        	ttlDealCnt += tgtCnt;
		        	log.debug(methodName,"makeStUseByStepByDay Result="+tgtCnt);
		        	break;
		        case 5:
		        	// 6.makeStPayErrByCprtByDay
		        	// 가맹점별 결제 실패 원인별 이용건수/이용자수 통계 데이터 생성 (일별)
					/* -----INSERT ST_PAY_ERR_BY_CPRT ----------------------------------------------------------------- */
		        	// 이전 단계에서 셋팅된 파라미터 그대로 사용
		        	//paramMap.put("dealDt",   cmprDd);
		        	//paramMap.put("dealDtTo", DateUtils.calcDate(cmprDd, 1));
		        	//paramMap.put("user_id",  userId);	
		        	tgtCnt = pHubStatService.makeStPayErrByCprtByDay(paramMap);
		        	ttlDealCnt += tgtCnt;
		        	log.debug(methodName,"makeStPayErrByCprtByDay Result="+tgtCnt);
		        	break;
		        case 6:
		        	// 7.makeStPayErrByPntByDay
		        	// 포인트제공처별(카드사) 결제 실패 원인별 이용건수/이용자수 통계 데이터 생성 (일별)
					/* -----INSERT ST_PAY_ERR_BY_PNT ----------------------------------------------------------------- */
		        	// 이전 단계에서 셋팅된 파라미터 그대로 사용
		        	//paramMap.put("dealDt",   cmprDd);
		        	//paramMap.put("dealDtTo", DateUtils.calcDate(cmprDd, 1));
		        	//paramMap.put("user_id",  userId);	
		        	tgtCnt = pHubStatService.makeStPayErrByPntByDay(paramMap);
		        	ttlDealCnt += tgtCnt;
		        	log.debug(methodName,"makeStPayErrByPntByDay Result="+tgtCnt);
		        	break;
		        case 7:
		        	// 8.makeStDealReqUsersByDay
		        	// 이용자수 통계 데이터 생성 (일별)
					/* -----INSERT ST_DEAL_REQ_USERS ----------------------------------------------------------------- */
		        	// 이전 단계에서 셋팅된 파라미터 그대로 사용
		        	paramMap.put("dealDt",   cmprDd);
		        	paramMap.put("dealDtTo", DateUtils.calcDate(cmprDd, 1));
		        	paramMap.put("user_id",  userId);	
		        	tgtCnt = pHubStatService.makeStDealReqUsersByDay(paramMap);
		        	ttlDealCnt += tgtCnt;
		        	log.debug(methodName,"makeStDealReqUsersByDay Result="+tgtCnt);
		        	break;
		        case 8:
		        	// 9.makeStDealReqPntUsersByDay
		        	// 제공처별 거래 이용자수 통계 데이터 생성 (일별)
					/* -----INSERT ST_DEAL_REQ_PNT_USERS ----------------------------------------------------------------- */
		        	// 이전 단계에서 셋팅된 파라미터 그대로 사용
		        	paramMap.put("dealDt",   cmprDd);
		        	paramMap.put("dealDtTo", DateUtils.calcDate(cmprDd, 1));
		        	paramMap.put("user_id",  userId);
		        	tgtCnt = pHubStatService.makeStDealReqPntUsersByDay(paramMap);
		        	ttlDealCnt += tgtCnt;
		        	log.debug(methodName,"makeStDealReqPntUsersByDay Result="+tgtCnt);
		        	break;
		        default :
		        	log.debug(methodName, "default!");
			    }
			    
			    // 단계별 대사결과 log 저장 (startDt, endDt 셋팅해줘야 한다.)
			    Map<String,Object> logMap = cmprCommService.getLogMap(ddCmprNo, cmprDd, batchProcMap.get(i), rsltCodeV, rsltMsgV, userId);
			    cmprCnt = tgtCnt;
				logMap.put("tgt_cnt",  tgtCnt);
				logMap.put("cmpr_cnt", cmprCnt);
				logMap.put("strt_dt",  strDt);
				logMap.put("tgt_file_path",  "");	// 통계생성은 대상파일 경로가 없다.
				logMap.put("cmpr_file_path", ""); 	// 통계생성은 정산파일 경로가 없다.				
				
				// INSERT ADM_BATCH_LOG
				cmprCommService.insertAdmBatchLog(logMap);
				
				if( !"00".equals(rsltCodeV) ) {
					// 실패
					break;
				}
			}
			// End For Loop
			// 대사처리단계 flag 초기화
			cmprProcStep = 0;
			
		} catch (UserException e) {
			ErrorInfoVO errorInfoVO = e.getErrorInfo();
			rsltCodeV = errorInfoVO.getErrCd();
			rsltMsgV  = errorInfoVO.getErrMsg();
			
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
			rsltCodeV = messageManage.getMsgCd("SY_ERROR_900");
			rsltMsgV  = messageManage.getMsgTxt("SY_ERROR_900");
			
		} finally {
			// 배치 로그마스터 업데이트
			Map<String,Object> logMstMap = new HashMap<String,Object>();
			logMstMap.put("dd_cmpr_no",   ddCmprNo);
			logMstMap.put("cmpr_dd",      cmprDd);
			logMstMap.put("wrkn_nm",      wrknNm);
			logMstMap.put("user_id",      userId);
			logMstMap.put("ttl_deal_cnt", ttlDealCnt);
			logMstMap.put("no_acr_cnt",   0);
			logMstMap.put("mtch_cnt",     0);
			logMstMap.put("doc_file_nm",  "");
			logMstMap.put("rslt_code",    rsltCodeV);
			logMstMap.put("rslt_msg",     rsltMsgV);
			// UPDATE ADM_BATCH_LOG_M
		    cmprCommService.updateAdmBatchLogM(logMstMap);		    
		    
			// 실패했을때만 일대사마스터에 업데이트 한다. 정상처리 됬을때는 4단계에서 update 처리 한다.
			if( !"00".equals(rsltCodeV) ) {
				
				// 배치로그 등록
				if( cmprProcStep > 0 ) {
				    Map<String,Object> logMap = cmprCommService.getLogMap(ddCmprNo, cmprDd, batchProcMap.get(cmprProcStep-1), rsltCodeV, rsltMsgV, userId);
				    cmprCnt = tgtCnt;
					logMap.put("tgt_cnt",  tgtCnt);
					logMap.put("cmpr_cnt", cmprCnt);
					logMap.put("strt_dt",  strDt);
					// INSERT ADM_BATCH_LOG
					cmprCommService.insertAdmBatchLog(logMap);					
				}
			}
		}
		
		log.debug(methodName, "End cmprProc! (rsltCodeV=" + rsltCodeV + ")");
	}
}
