/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 */
package com.olleh.pHubCMS.common.utils;
import java.util.Map;


/**
 * 공통 페이징 처리 기능(hasNext 값을 이용하거나 페이지 넘버링 처리 하는 경우)
 * @Class Name : Paging
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
public class Paging
{
	private static org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(Paging.class);

	/**
     * 페이징처리를 위해 첫 페이지와 끝 페이지 값을 reqMap 에 설정한다.
     * 
     * @param Map reqMap 리퀘스트 파라메터 정보를 가지는 맵
     * @param int pageNum 현재 페이지 번호
     * @param int totalRowCnt 전문에서 조회 된 row 의 총 개수
     * @param int listNum 페이지당 출력되는 row 수
     * @return int void (reqMap 객체에 페이지 정보들을 설정)
     */
    public static void setPageFLnum(Map reqMap, int pageNum, int totalRowCnt, int listNum) {
    	
		int firstPageNum = 1;																			//처음 페이지 값
		int lastPageNum = (int)((totalRowCnt-1.0f)/listNum) + 1;	//끝 페이지 값
		
		//이전 페이지가 있는지 여부
		if( pageNum <= firstPageNum ) {
			reqMap.put("prevPageYn", "N");
		} else {
			reqMap.put("prevPageYn", "Y");
		}
		//다음 페이지가 있는지 여부
		if( pageNum < lastPageNum ) {
			reqMap.put("nextPageYn", "Y");
		} else {
			reqMap.put("nextPageYn", "N");
		}
		
		int pagingNumCnt = 5;						//페이징 넘버 갯수
		int startPagingNum = (int)((pageNum-1.0f) / pagingNumCnt) * pagingNumCnt + 1;
		int endPagingNum = startPagingNum + pagingNumCnt - 1;
		if(lastPageNum < endPagingNum) endPagingNum = lastPageNum;
		//int startPagingNum = 1;						//페이징 넘버 시작값
		//int endPagingNum = startPagingNum + pagingNumCnt - 1;		//페이징 넘버 종료값
		
		//페이징 넘버 증가 폭
		//int pagingStep = pageNum / pagingNumCnt;
		//log.debug("pagingStep : " + pagingStep);
		
		//현재 페이지 번호와 페이징 넘버 리스트 갯수와 비교해서 현재 페이지 번호가 더 크면 페이징 넘버 start, end 값 변경
		//if( pageNum > endPagingNum ) {
		//	startPagingNum = (pagingStep * pagingNumCnt) + 1;
		//	endPagingNum = startPagingNum + endPagingNum - 1;
		//}
		//페이징 번호의 최대값이 마지막 페이지 번호보다 크면 마지막 페이지 번호를 최대값으로 설정
		//if( lastPageNum < endPagingNum ) {
		//	endPagingNum = lastPageNum;
		//}
				
		reqMap.put("pageNum", pageNum);
		reqMap.put("firstPageNum", String.valueOf(firstPageNum));
		reqMap.put("lastPageNum", String.valueOf(lastPageNum));
		reqMap.put("startPagingNum", String.valueOf(startPagingNum));
		reqMap.put("endPagingNum", String.valueOf(endPagingNum));
		
		log.debug("totalRowCnt : " + totalRowCnt);
    	log.debug("listNum : " + listNum);
		log.debug("prevPageYn : " + reqMap.get("prevPageYn"));
		log.debug("nextPageYn : " + reqMap.get("nextPageYn"));
		log.debug("pageNum : " + reqMap.get("pageNum"));
		log.debug("firstPageNum : " + reqMap.get("firstPageNum"));
		log.debug("lastPageNum : " + reqMap.get("lastPageNum"));
		log.debug("startPagingNum : " + reqMap.get("startPagingNum"));
		log.debug("endPagingNum : " + reqMap.get("endPagingNum"));
    }
    
    /**
     * 페이징처리 hasNext 처리를 위해 첫 페이지와 끝 페이지 값을 reqMap 에 설정한다.
     * 
     * @param Map reqMap 리퀘스트 파라메터 정보를 가지는 맵
     * @param int pageNum 현재 페이지 번호
     * @param String hasNext 다음 페이지 존재여부
     * @return void (reqMap 객체에 페이지 정보들을 설정)
     */
    public static void setPageHasNext(Map reqMap, int pageNum, String hasNext) {
    	
		int firstPageNum = 1;			//처음 페이지 값
		
		//이전 페이지가 있는지 여부
		if( pageNum <= firstPageNum ) {
			reqMap.put("prevPageYn", "N");
		} else {
			reqMap.put("prevPageYn", "Y");
		}
		//다음 페이지가 있는지 여부
		if( "1".equals(hasNext) ) {
			reqMap.put("nextPageYn", "Y");
		} else {
			reqMap.put("nextPageYn", "N");
		}
		
		reqMap.put("pageNum", pageNum);
		
		log.debug("prevPageYn : " + reqMap.get("prevPageYn"));
		log.debug("nextPageYn : " + reqMap.get("nextPageYn"));
		log.debug("pageNum : " + reqMap.get("pageNum"));
    }
    
    
    /** JQGrid 용 페이징 처리
     * 페이징 처리를 위해 페이지 정보를 reqMap 에 설정한다.
     * 
     * @param Map params 리퀘스트 파라메터 정보를 가지는 맵
     * @param Map pageMap 페이징 정보를 가지는 맵
     * @param int pageNum 현재 페이지 번호
     * @param int limit 한 페이지당 보여줄 개수
     * @param int countRow 총 데이터 개수(쿼리 결과)
     * @return void (reqMap 객체에 페이지 정보들을 설정)
     */
    public static void setPageMap(Map params, Map pageMap, int pPageNum, int limit, int countRow) {    	
    	int start     = 0;
		int totalpage = 0;	
		int pageNum   = pPageNum;
		
		if(countRow > 0){
			totalpage = (int) Math.ceil((double)countRow / (double)limit);
		}
		
		if(pageNum > totalpage){
			pageNum = totalpage;
		}
		start = (int) (limit*pageNum - limit);
		
		//쿼리에 전달할 param 셋팅을 위한 map 정보
		params.put("START", start );
		params.put("LIMIT", limit);
		
		//jQrid에 전달할 response 셋팅을 위한 map 정보
		pageMap.put("page", pageNum );
		pageMap.put("total", totalpage);		
		pageMap.put("records", countRow);	
    }    
    
}
