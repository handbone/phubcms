/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 * Revision History
 * Author            Date              Description
 * ------            -----             ------------
 * lys               2018.06.28        공통 유틸리티 정의 
 */
package com.olleh.pHubCMS.common.utils;

import java.text.DecimalFormat;
import java.util.UUID;
import java.util.regex.Pattern;

import org.apache.commons.codec.binary.Base64;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

public class CommonUtils {
	public static ObjectMapper objectMapper = new ObjectMapper();

	
    /**
     * 랜덤 문자열을 생성한다.
     *
     * @param  
     * @return String
     */
	public static String getRandomString() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
	
    /**
     * 요청된 길이의 랜덤 숫자를 생성한다.
     *
     * @param  int size
     * @return int
     */	
	public static int getRandom(int size) {
	    int x = (int)(System.currentTimeMillis() % size);
	    return x;
	}
	
	/**
	 * 대상이 null 이거나 "" 인 경우, 지정한 기본값을 리턴한다.
	 * 
	 * @param Object obj 검사할 대상
	 * @param String value 대상이 null 또는 "" 인 경우 리턴할 기본값
	 * @return
	 */
	public static String isEmpty(Object obj, String value) {
		return StringUtils.isEmpty(obj) ? value:String.valueOf(obj);
	}
	
    /**
     * DB 조회후, 컬럼명 변경을 위해 사용한다.
     * 사용예시) convertString("test_field_name", "_") -> testFieldName
     * @param s 변경할 컬럼명
     * @param delim 기존 컬럼명 구분자
     * @return 
     */
    public static String convertDbFieldName(String s, String delim) {
        if(StringUtil.nvl(s, "").equals("")) { return ""; }
        
        String[] keyArray = s.split(delim);
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < keyArray.length; i++) {
            if(i == 0) {
                sb.append(keyArray[i]);
            } else {
                sb.append(keyArray[i].substring(0,1).toUpperCase()).append(keyArray[i].substring(1));
            }
        }
        return sb.toString();
    }
    
    /**
     * 주민등록번호를 가지고 남자 or 여자를 구분해서 반환한다.
     * @param ssno 주민등록번호(13자리)
     * @return "M" 남자 "F" 여자
     */
    public static String getGender(String pSsno) {
    	String ssno = pSsno;
    	String  rtn = "";
    	if( ssno != null ) {
    		ssno = StringUtils.replace(ssno, "-", "");
    		if( ssno.length() == 13 ) {
    			switch(ssno.charAt(6)) {
	    			case '1' : 
	    			case '3' : 
	    				return "M";
	    			case '2' : 
	    			case '4' : 
	    				return "F";
	    			default :
    			}
    		}
    	}
    	return rtn;
    }
    
    /**
     * 주민등록번호를 생년월일로 변환해서 반환한다.
     * @param ssno 주민등록번호(13자리)
     * @return
     */
    public static String getTransBirth(String pSsno) {
    	String ssno   = pSsno;
    	String result = ssno;
    	String prefix = "";
    	if( ssno != null ) {
    		ssno = StringUtils.replace(ssno, "-", "");
    		if( ssno.length() == 13 ) {
    			switch(ssno.charAt(6)) {
    			case '1' : 
    			case '2' : 
    				prefix = "19"; break;
    			case '3' :
    			case '4' : 
    				prefix = "20"; break;
    			default :
    			}
    			
    			result = prefix + ssno.substring(0, 6);
    		}
    	}
    	
    	return result;
    }
    
    /**
     * 회원가입시 입력한 ID값이 유효한지 체크후 결과를 반환한다.
     * @param id
     * @return 0:정상 1:최소길이 2:최대길이 3:입력값 4:공백
     */
    public static String checkMemberId(String id) {
    	String result = "0";
    	final int minLen = 5;
    	final int maxLen = 12;
    	
    	if( id == null ) { return "3"; }
    	
    	if( id.length() < minLen ) {
    		result = "1";
    	} else if( id.length() > maxLen ) {
    		result = "2";
    	} else if( StringUtils.containsWhitespace(id) ) {
    		result = "4";
    	}
    	
    	return result;
    }
    
    /**
     * 회원가입시 입력한 ID값이 유효한지 체크후 결과를 반환한다.
     * @param memberId
     * @param min 최소길이
     * @param max 최대길이
     * @return 000:정상 001:최소길이 002:최대길이 003:입력값 004:공백 005:3개이상반복문자 006:3개이상연속문자 007:비밀번호에 아이디 포함
     */
    public static String checkMemberPassword(final String mbrIdnId, final String password, final int min, final int max) {
    	String result = "000";
    	final int minLen = min;
    	final int maxLen = max;
    	
    	if( password == null ) { return "3"; }
    	
    	if( password.length() < minLen ) {
    		result = "001";
    	} else if( password.length() > maxLen ) {
    		result = "002";
    	} else if( StringUtils.containsWhitespace(password) ) {
    		result = "004";
    	} else if( !StringUtils.isEmpty(mbrIdnId) && password.indexOf(mbrIdnId) != -1 ) {
    		result = "007";
    	} else {
    		int	samePass0 = 0;
    		int	samePass1 = 0;
    		int	samePass2 = 0;
    		
    	    for( int i=0; i<password.length()-1; i++) {
    	        char chr_pass_0 = password.charAt(i);
    	        char chr_pass_1 = password.charAt(i+1);
    	        
    	        if( chr_pass_0 == chr_pass_1 ) { //동일문자 카운트
    	            samePass0 = samePass0 + 1;
    	    	    if(samePass0 >= 2) {
    	    			result = "005";
    	    	    }
    	        } else {
    	        	samePass0 = 0;
    	        }
    	        
    	        if( i<password.length()-2 ) {
    		        char chr_pass_2 = password.charAt(i+2);
    		        if( chr_pass_0 - chr_pass_1 == 1 && chr_pass_1 - chr_pass_2 == 1 ) { //연속성(+) 카운드
    		            samePass1 = samePass1 + 1;
    		        }
    		        if(chr_pass_0 - chr_pass_1 == -1 && chr_pass_1 - chr_pass_2 == -1) { //연속성(-) 카운드
    		            samePass2 = samePass2 + 1;
    		        }
    	        }
    		    if(samePass1 >= 1 || samePass2 >= 1 ) {
    		    	result = "006";
    		    }		
    	    }
    	    String regx = "^[a-zA-Z]*$";
    	    Pattern p = Pattern.compile(regx);
    	    if( p.matcher(password).matches() ) { //영문자만 입력
    	    	result = "003";
    	    }
    	    regx = "^[0-9]*$";
    	    p = Pattern.compile(regx);
    	    if( p.matcher(password).matches() ) { //숫자만 입력
    	    	result = "003";
    	    }
    	    regx = "^[a-zA-Z0-9]*$";
    	    p = Pattern.compile(regx);
    	    if( !p.matcher(password).matches() ) { //영문+숫자 이외 입력
    	    	result = "003";
    	    }
    	}
    	return result;
    }
    
    /**
     * base64 인코딩 결과를 반환한다.
     * @param binaryData
     * @return
     */
    public static byte[] base64Encode(byte[] binaryData) {
    	return Base64.encodeBase64(binaryData);
    }
    
    /**
     * base64 디코딩 결과를 반환한다.
     * @param base64Data
     * @return
     */
    public static byte[] base64Decode(byte[] base64Data) {
    	return Base64.decodeBase64(base64Data);
    }
    
    /**
     * 전달된 주민등록번호를 마스킹 처리 해서 반환한다.
     * 
     * 앞6자리이상 입력시 처리되며, 미만인 경우 원본값을 그대로 반환한다.
     * @param ssno 주민등록번호
     * @return
     */
    public static String getMaskSSNO(final String ssno) {
    	String result = ssno;
    	if( !StringUtils.isEmpty(result) ) {
    		result = StringUtils.replace(ssno, "-", "");
        	if( result.length() >= 7 ) {
        		result = String.format("%s-%s******", result.substring(0, 6), result.substring(6, 7));
        	}
    	}
    	
    	return result;
    }
    
	/**
	 * 주민등록번호의 생년월일을 yyyyMMdd 형식으로 반환한다.
	 * @param ssno
	 * @return
	 */
	public static String getBirthDay(final String ssno) {
		String result = null;
		if( ssno != null ) {
			result = StringUtils.replace(ssno, "-", "");
			if( result.length() >= 13 ) {
				String yyyyMMdd = result.substring(0, 6); 
				String idf = result.substring(6, 7);
				if( Integer.parseInt(idf) > 2 ) {
					yyyyMMdd = String.format("%s%s", "20", yyyyMMdd);
				} else {
					yyyyMMdd = String.format("%s%s", "19", yyyyMMdd);
				}
				result = yyyyMMdd;
			}
		}
		return result;
	}
	
    /**
     * 주민번호에 숫자값을 제외한 값을 모두 제거 후 반환한다. 
     * @param msg
     * @return
     */
    public static String getCleanSsno(final String ssno) {
    	String result = ssno;
    	if( result != null ) {
    		result = result.replaceAll("[^0-9]", "");
    	}
    	return result;
    }
    
    /**
     * 절상, 절하, 반올림 처리 후 결과를 반환한다.
     * @param strMode CEIL:절상 FLOOR:절하 그외 반올림
     * @param nCalcVal 처리할 값(소수점 이하 데이터 포함)
     * @param nDigit 연산 기준 자릿수(-4:천단위 -3:백단위 -2:십단위, -1:원단위 0:소수점 1자리 1:소수점 2자리 ...)
     * @return
     */
    public static String calcMath(String strMode, double pCalcVal, int pDigit) {
    	double nCalcVal = pCalcVal;
    	int    nDigit   = pDigit;
    	if( "CEIL".equals(strMode) ) { //절상
    		if( nDigit < 0 ) {
    			nDigit = -(nDigit);
    			nCalcVal = Math.ceil(nCalcVal / Math.pow(10, nDigit)) * Math.pow(10, nDigit);
    		} else {
    			nCalcVal = Math.ceil(nCalcVal * Math.pow(10, nDigit)) / Math.pow(10, nDigit);
    		}
    	} else if( "FLOOR".equals(strMode) ) { //절하
    		if( nDigit < 0 ) {
    			nDigit = -(nDigit);
    			nCalcVal = Math.floor(nCalcVal / Math.pow(10, nDigit)) * Math.pow(10, nDigit);
    		} else {
    			nCalcVal = Math.floor(nCalcVal * Math.pow(10, nDigit)) / Math.pow(10, nDigit);
    		}
    	} else {
    		if( nDigit < 0 ) {
    			nDigit = -(nDigit);
    			nCalcVal = Math.round(nCalcVal / Math.pow(10, nDigit)) * Math.pow(10, nDigit);
    		} else {
    			nCalcVal = Math.round(nCalcVal * Math.pow(10, nDigit)) / Math.pow(10, nDigit);
    		}
    	}
    	
    	DecimalFormat df = new DecimalFormat(nDigit<0?"#":"#.#");
    	return df.format(nCalcVal);
    	//return String.valueOf(nCalcVal);
    }
    
    /**
     * 날짜에 포함 된 특수문자를 제거후 반환한다.
     * @param date
     * @return
     */
    public static String getCleanDateFormat(final String date) {
    	String result = date;
    	if( result != null ) {
    		result = StringUtils.replace(result, "-", "");
    		result = StringUtils.replace(result, ".", "");
    		result = StringUtils.replace(result, "/", "");
    		result = StringUtils.replace(result, ":", "");
    		result = StringUtils.replace(result, " ", "");
    	}
    	return result;
    }
    
    /**
     * date 값에서 yyyyMMdd 값을 뽑아서 반환한다.
     * @param date
     * @return
     */
    public static String getCleanYYYYMMDD(String date) {
    	String result = "";
    	if( !StringUtils.isEmpty(date) ) {
    		result = CommonUtils.getCleanDateFormat(date);
    		if( result.length() >= 8 ) {
    			result = result.substring(0, 8);
    		}
    	}
		return result;
    }
    
    /**
     * yyyyMMdd 형식의 문자열을 split 처리해서 반환한다. 
     * @param date
     * @return yyyy[0], MM[1], dd[2]
     */
    public static String[] getSplitYYYYMMDD(String pDate) {
    	String date     = pDate;
    	String[] result = new String[3];
    	
    	if( date != null ) {
    		date = CommonUtils.getCleanDateFormat(date);
    		if( date.length() >= 8 ) {
				result[0] = date.substring(0, 4);
				result[1] = date.substring(4, 6);
				result[2] = date.substring(6, 8);
			}
    	}
    	
    	return result;
    }
}