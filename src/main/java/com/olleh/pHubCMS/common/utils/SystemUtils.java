/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 * Revision History
 * Author            Date              Description
 * ------            -----             ------------
 * lys               2018.06.28        시스템 관련 유틸리티
 */
package com.olleh.pHubCMS.common.utils;

import java.io.File;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DeviceUtils;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.FrameworkServlet;

import com.olleh.pHubCMS.common.components.Constant;
import com.olleh.pHubCMS.common.spring.filter.XssHtmlInputFilter;

public class SystemUtils {
	public static XssHtmlInputFilter xssHtmlInputFilter = new XssHtmlInputFilter(false);	
		
	/**
	 * xss 필터 처리
	 * 
	 * @param html
	 * @return String
	 */
	public static String getCleanHtml(String html) {
		return xssHtmlInputFilter.filter(html);
	}
	
	/**
	 * 화면에서 입력되는 값 필터링 처리 후 반환한다.
	 * 
	 * @param str
	 * @return String
	 */
	public static String getCleanString(String pStr) {
		String str = pStr;
		if( !StringUtils.isEmpty(str) ) {
			str = xssHtmlInputFilter.filter(str);
			str = StringUtils.replace(str, "\"", "&quot;");
			str = StringUtils.replace(str, "'", "&apos;");
			str = StringUtils.replace(str, "<", "&lt;");
			str = StringUtils.replace(str, ">", "&gt;");
			str = StringUtils.replace(str, "(", "");
			str = StringUtils.replace(str, ")", "");
			str = StringUtils.replace(str, "=", "");
			str = StringUtils.replace(str, "%", "");
			str = StringUtils.replace(str, "-", "");
			str = StringUtils.replace(str, "--", "");
		}
		return str;
	}
	
	/**
	 * 화면에서 입력되는 값 필터링(DB) 처리 후 반환한다.
	 * @param str
	 * @return String
	 */
	public static String getCleanDBString(String pStr) {
		String str = pStr;
		if( !StringUtils.isEmpty(str) ) {
			str = SystemUtils.getCleanString(str);
			str = str.toLowerCase();
			
			str = StringUtils.replace(str, "select", "");
			str = StringUtils.replace(str, "from", "");
			str = StringUtils.replace(str, "all_tab_columns", "");
			str = StringUtils.replace(str, "all_tables", "");
			str = StringUtils.replace(str, "get_host_name", "");
			str = StringUtils.replace(str, "substr", "");
			str = StringUtils.replace(str, "when", "");
			str = StringUtils.replace(str, "else", "");
			str = StringUtils.replace(str, "dual", "");
			str = StringUtils.replace(str, "ascii", "");
			str = StringUtils.replace(str, "distinct", "");
			str = StringUtils.replace(str, "union", "");
			str = StringUtils.replace(str, "database_name", "");
		}
		return str;
	}
	
	/**
	 * contextPath 리턴
	 * 
	 * @return String
	 */
	public static String getContextPath() {
		HttpServletRequest request = getCurrentRequest();
		return request==null?null:request.getContextPath();
	}
	
	/**
	 * 현재 HttpServletRequest 반환
	 * 
	 * @return HttpServletRequest
	 */
	public static HttpServletRequest getCurrentRequest() {
		ServletRequestAttributes requestAttributes = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes());
		return requestAttributes==null?null:requestAttributes.getRequest();
	}
	
	/**
	 * 현재 스키마 + 도메인 정보를 반환한다.
	 * 
	 * @param HttpServletRequest request
	 * @return String
	 */
	public static String getCurrentDomain(HttpServletRequest request) {
		return String.format("%s://%s", request.getScheme(), request.getServerName());
	}
	
	/**
	 * request 에서 device 정보를 찾아 반환한다.
	 * 
	 * @param HttpServletRequest request
	 * @return Devicespring mybatis vo
	 */
	public static Device getCurrentDeviceN(HttpServletRequest request) {
		return DeviceUtils.getCurrentDevice(request);
	}
	
	/**
	 * client pc ip address 반환
	 * 
	 * @param HttpServletRequest request 
	 * @return String IP Address
	 */
	public static String getIpAddress(HttpServletRequest request) {
		
		String clientIp = null;
		if (clientIp == null) clientIp = request.getHeader("Proxy-Client-IP");
		if (clientIp == null) clientIp = request.getHeader("WL-Proxy-Client-IP");
		if (clientIp == null) clientIp = request.getHeader("HTTP_CLIENT_IP");
		if (clientIp == null) clientIp = request.getHeader("HTTP_X_FORWARDED_FOR");
		if (clientIp == null) clientIp = request.getHeader("X-FORWARDED-FOR");
		if (clientIp == null) clientIp = request.getHeader("X-Forwarded-For");
		if (clientIp == null) clientIp = request.getRemoteAddr();

		if(clientIp != null){
			clientIp = clientIp.split(",")[0]; // 값이 , 구분자로 한개 이상이 넘어오는 경우가 있으므로 아래 처리함.
		} else {
			clientIp = "";
		}
        return clientIp;
    }	
	
	/**
	 * 지정한 경로 아래에 있는 모든 폴더 및 파일을 삭제한다.
	 * 
	 * @param String rootPath 삭제 대상 경로
	 * @return boolean true:성공 false:실패
	 */
	public static boolean deleteFilePath(final String rootPath) {
		File file = new File(rootPath);
		return FileSystemUtils.deleteRecursively(file);
	}
	
	/**
	 * 지정한 경로 아래에 있는 모든 폴더 및 파일을 삭제한다.
	 * 
	 * @param File 파일
	 * @return boolean true:성공 false:실패
	 */
	public static boolean deleteFilePath(final File file) {
		return FileSystemUtils.deleteRecursively(file);
	}
	
	/**
	 * 파일 업로드시 사용된 file tag 의 name 값을 반환한다.
	 * 
	 * @param HttpServletRequest request
	 * @return List<String>
	 */
	public static List<String> getUploadFileTagNames(final HttpServletRequest request) {
		final List<String> result = new ArrayList<String>();
		if( request != null ) {
			MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest)request;
	        Iterator<String> iterator = multipartHttpServletRequest.getFileNames();
	        while(iterator.hasNext()) {
	        	String fileTagName = iterator.next();
	        	MultipartFile multipartFile = multipartHttpServletRequest.getFile(fileTagName);
	        	if( multipartFile != null && !multipartFile.isEmpty() ) {
	        		result.add(fileTagName);
	        	}
	        }
		}
		return result;
	}
	
	/**
	 * bean 객체가 아닌 곳에서 bean 관련 메소드 호출이 필요한 경우에 bean 으로 접근하기 위한 WebApplicationContext 를 반환한다.
	 * 
	 * web.xml 에 정의한 <servlet-name>smcweb</servlet-name> 을 파라메터로 지정해줘야 bean 메소드에 접근할수 있다.
	 * 
	 * @param 
	 * @return WebApplicationContext
	 */
	public static WebApplicationContext getWebApplicationContext() {
		return WebApplicationContextUtils.getWebApplicationContext(SystemUtils.getCurrentRequest().getSession().getServletContext(), FrameworkServlet.SERVLET_CONTEXT_PREFIX + "smcweb");
	}
	
	/**
	 * bean 객체가 아닌 곳에서 bean 관련 메소드 호출이 필요한 경우에 bean 으로 접근하기 위한 WebApplicationContext 를 반환한다.
	 * 
	 * web.xml 에 정의한 <servlet-name>smcweb</servlet-name> 을 파라메터로 지정해줘야 bean 메소드에 접근할수 있다.
	 * 
	 * @param HttpServletRequest request
	 * @return WebApplicationContext
	 */
	public static WebApplicationContext getWebApplicationContext(HttpServletRequest request) {
		return WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext(), FrameworkServlet.SERVLET_CONTEXT_PREFIX + "smcweb");
	}
	
	/**
	 * beanName 에 해당하는 bean 오브젝트를 반환한다.
	 * 
	 * @param String bean name
	 * @return Object
	 * @throws Exception
	 */
	public static Object getBean(String beanName) throws Exception {
		return getWebApplicationContext().getBean(beanName);
	}
	
	/**
	 * 서버에 저장된 파일의 내용을 byte[] 형태로 읽어서 반환한다.
	 * 
	 * @param String fileUploadPath 파일이 실제 저장된 최상위 경로
	 * @param String fileName 파일명
	 * @return byte[]
	 */
	public static byte[] getFileByteArray(final String fileUploadPath, final String fileName) {
		byte[] result = null;
		

		Constant constant = null;
		try
		{
			constant = ((Constant)SystemUtils.getBean("commonValue"));

			try {
				final String resourcePath = String.format("%s/", fileUploadPath);
				final String resourceName = StringUtils.replace(StringUtils.replace(fileName, constant.ACCESS_EXTERNAL_RESOURCE_ACTION_URL, ""), "_", "/");
				final File file = new File(resourcePath + resourceName);
				
				if( file != null && file.exists() ) {
					result = FileUtils.readFileToByteArray(file);
				}
			} catch(Exception e) {
				result = null;
			}
		}
		catch(Exception e)
		{
			result = null;
		}
		
		
		return result;
	}
    
    /**
	 * request header 에서 user-agent 값을 통해 브라우저가 IE인 경우 버전정보를 반환한다.
	 * 
	 * @param HttpServletRequest request
	 * @return Map<String, Object> true : 8버전이하인경우 false : 그외
	 */
	public static Map<String, Object> getCheckIEversion(final HttpServletRequest request) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		final String mapKey = "user-agent";
		final String userAgentValue = request.getHeader(mapKey);
		
		if( !StringUtils.isEmpty(userAgentValue) ) {
			Pattern pattern = Pattern.compile("MSIE ([0-9]{1,}[.0-9]{0,})");
			Matcher matcher = pattern.matcher(userAgentValue);
			if( matcher != null && matcher.find(1) ) {
				result.put("ieVer", matcher.group(1)); //IE 브라우저 버전
				
				pattern = Pattern.compile("Trident/([0-9]{1,}[.0-9]{0,})");
				matcher = pattern.matcher(userAgentValue);
				if( matcher != null && matcher.find(1) ) {
					result.put("trident", matcher.group(1)); //IE 호환성보기 인 경우 trident 값 체크 ( ie8 : 4, ie9 : 5, ie10 : 6 )
				}
			}
		}
		
		return result;
	}
	
	/**
	 * ajax 요청이 맞는지 체크후 결과를 반환한다.
	 * @param request
	 * @return
	 */
	public static boolean isAjaxRequest(HttpServletRequest request) {
		final String header = request.getHeader(Constant.AJAX_HEADER);
		if( header != null && header.equals("true") ) {
			return true;
		}
		return false;
 	}
	
	/**
	 * host server의 ip를 리턴 한다. (ipv4)
	 * @param 
	 * @return
	 */
	public static String getHostServerIP() {
		String hostAddr = "";

		try {
			Enumeration<NetworkInterface> nienum = NetworkInterface.getNetworkInterfaces();

			while ( nienum.hasMoreElements() ) {

				NetworkInterface ni         = nienum.nextElement();
				Enumeration<InetAddress> kk = ni.getInetAddresses();

				while (kk.hasMoreElements()) {
					InetAddress inetAddress = kk.nextElement();
					
					if (!inetAddress.isLoopbackAddress() && 
							!inetAddress.isLinkLocalAddress() && 
							inetAddress.isSiteLocalAddress()) {

						 hostAddr = inetAddress.getHostAddress().toString();
					}
				}
			}
		} catch (SocketException e) {
			hostAddr = "";
		}
		
		return hostAddr;
 	}	
	
	/**
	 * properties 파일에 정의된 code값과 mapping 되는 value 값을 반환한다.
	 * 
	 * @param code
	 * @param args
	 * @param defaultMessage
	 * @param locale
	 * @param applicationContext
	 * @return String
	 */
	public static String getMessage(final String code, final Object[] args, final String defaultMessage, final Locale locale, final ApplicationContext applicationContext) {
		return applicationContext.getMessage(code, args, defaultMessage, locale);
	}
	public static String getMessage(final String code, final ApplicationContext applicationContext) {
		return SystemUtils.getMessage(code, null, "", Locale.KOREA, applicationContext);
	}
	public static String getMessage(final String code, final Object[] args, final ApplicationContext applicationContext) {
		return SystemUtils.getMessage(code, args, "", Locale.KOREA, applicationContext);
	}
}
