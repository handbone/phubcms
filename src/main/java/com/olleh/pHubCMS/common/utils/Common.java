/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 * Revision History
 * Author            Date              Description
 * ------            -----             ------------
 * lys               2018.06.28        널여부 체크, Key Array 반환등 공통으로 사용하는 함수들을 제공한다.
 */
package com.olleh.pHubCMS.common.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

public class Common 
{
	//private static org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(Common.class);
	
	/**
	 * 문자열 변수가 널인지 여부를 반환한다.
	 * 
	 * @param v 문자열 변수
	 * @return 널여부 널이면 true
	 */
	public static boolean isNull(String v)
	{
		return v == null || v.equals("");
	}
	public static boolean isNull(Object v)
	{
		return v == null;
	}
	
	
	/**
	 * 문자열 변수가 널이면 ""을 반환한다.
	 * 
	 * @param v 문자열 변수
	 * @return 널이며 "" 반환, 아니면 원래 문자열 반환
	 */
	public static String Null(String v)
	{
		return isNull(v)?"":v;
	}

	public static Object Null(Object v)
	{
		return v == null ? "":v;
	}
	
	public static String Null(String v, String d)
	{
		return isNull(v)?d:v;
	}
	
	/**
	 * 문자열 변수의 길이를 반환한다.
	 * 
	 * @param String v 문자열 변수
	 * @return int
	 */	
	public static int len(String s)
	{
		return Null(s).length();
	}
	
	/**
	 * map의 key값 Array를 반환한다.
	 * 
	 * @param hm map변수
	 * @return keay값 array
	 */
	public static Object[] getKeys(LinkedHashMap<String, ?> hm)
	{
		if(hm == null) return null;
		Set<String> set = hm.keySet();
		Object[] hmKeys = set.toArray();

		return hmKeys;
	}
	
	/**
	 * 주민등록번호에 하이픈(-) 붙이기
	 * 
	 * @param ssno 주민등록번호
	 * @return 하이픈(-)이 붙은 번호
	 */
	public static String splitSSNO(String pSsno)
	{
		String ssno = Null(pSsno).replaceAll("-", "");
		if(ssno.length() == 13)
			return ssno.substring(0, 6) + "-" + ssno.substring(6, 13);
		return "";
	}
	
	/**
	 * 사업자번호에 하이픈(-) 붙이기
	 * 
	 * @param compNo 사업자번호
	 * @return 하이픈(-)이 붙은 번호.
	 */
	public static String splitCompNo(String pCompNo)
	{
		String compNo = Null(pCompNo).replaceAll("-", "");
		if(compNo.length() == 10) 
			return compNo.substring(0, 3) + "-" + compNo.substring(3, 5) + "-" + compNo.substring(5, 10);
		return "";
	}
	
	/**
	 * 주민등록 번호에 뒷6자리 *표 처리
	 * 
	 * @param ssno 주민등록번호
	 * @return *표 처리된 주민등록 번호
	 */
	public static String setSsnoAstar(String ssno)
	{
		if(!Common.isNull(ssno) && ssno.length() > 6)
			return ssno.substring(0, ssno.length()-6) + "******";
		
		return ssno;
	}

	/**
	 * 문자열을 바이트 기준으로 자른다.
	 * 
	 * @param String ss 문자열
	 * @param int sz 기준 길이
	 * @return String 자른 문자열
	 */
	public static String sliceByte(String ps, int pz)
	{
		String ss = ps;
		int    sz = pz;
		byte[] bss = ss.getBytes();
		byte[] tss = new byte[sz];
		if(bss.length > sz)
		{
			System.arraycopy(bss, 0, tss, 0, sz);
			ss = new String(tss);
			bss = null;
			tss = null;
		}
		return ss;
	}
	
	/**
	 * 숫자로된 금액을 문자로 변경하여 반환 한다.
	 * 
	 * @param Long money 숫자로된 금액
	 * @return String
	 */
	public static String money2Hangul(Long money) {
		String r = "";
		
		if(money > 0) {
			String[] han1 = {"", "일", "이", "삼", "사", "오", "육", "칠", "팔", "구"};
			String[] han2 = {"", "십", "백", "천"};
			String[] han3 = {"", "만", "억", "조", "경", "해"};
			String sMoney = String.valueOf(money);
			
			StringBuffer result = new StringBuffer();
			int len = sMoney.length();
			for(int i=len-1; i>=0; i--) {
				int cMoney = Integer.valueOf(sMoney.substring(len-i-1, len-i));
				
				result.append(han1[cMoney]);
				
				if(cMoney > 0) {
					result.append(han2[i % 4]);
				}
				
				if(i % 4 == 0) {
					result.append(han3[i / 4]);
				}
			}
			
			r = result.toString();
		}
		
		return r;
	}
	
	/**
	 * 숫자로된 금액을 문자로 변경하여 반환 한다.
	 * 
	 * @param Long money 숫자로된 금액
	 * @return String
	 */	
	public static String money2HangulEx(Long money) {
		String r = "";
		
		if(money > 0) {
			String[] han1 = {"", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
			String[] han2 = {"", "십", "백", "천"};
			String[] han3 = {"", "만", "억", "조", "경", "해"};
			
			String sMoney = String.valueOf(money);
			
			StringBuffer result = new StringBuffer();
			int len = sMoney.length();
			for(int i=len-1; i>=0; i--) {
				int cMoney = Integer.valueOf(sMoney.substring(len-i-1, len-i));
				
				result.append(han1[cMoney]);
				
				if(cMoney > 0) {
					result.append(han2[i % 4]);
				}
				
				if(i % 4 == 0) {
					result.append(han3[i / 4]);
				}
			}
			
			r = result.toString();
		}
		
		return r;
	}
	
	/**
	 * 카멜표기법의 변수를 스네이크 표기법으로 변환 한다.
	 * 
	 * @param String c 카멜표기법 변수
	 * @return String
	 */
	public static String camel2snake(String c)
	{
		if(isNull(c)) return c;
		
		String rtn = "";
		StringBuffer r = new StringBuffer();
		char cc;
		for(int i=0;i<c.length();i++)
		{
			cc = c.charAt(i);
			if(cc >= 0x41 && cc <= 0x5A) {r.append("_");}
			r.append(cc);
		}
		
		rtn = r.toString();
		return rtn.toUpperCase();
	}
	
	/**
	 * 스네이크 표기법의 변수를 카멜표기법으로 변환 한다.
	 * 
	 * @param String s 스네이크 표기법 변수
	 * @return String
	 */
	public static String snake2camel(String s)
	{
		if(isNull(s)) return s;
		
		StringBuffer r = new StringBuffer();
		char cs;
		for(int i=0;i<s.length();i++)
		{
			cs = s.charAt(i);
			if(cs == 0x5F) {r.append((""+cs).toUpperCase());}
			else r.append((""+cs).toLowerCase());
		}
		
		return r.toString();
	}

	/**
	 * SHA-256 변환
	 * 
	 * @param String input
	 * @return String
	 */	
	public static String sha1(String pInput)
	{
		String input = pInput;
		try
		{
			//byte[] salt = CommonUtils.getRandomString().getBytes();
			MessageDigest mDigest = MessageDigest.getInstance("SHA-256");
			mDigest.reset();
			//mDigest.update(salt);
			byte[] result = mDigest.digest(input.getBytes());
			StringBuffer sb = new StringBuffer();
			for(int i=0;i<result.length;i++)
			{
				sb.append(Integer.toString((result[i] % 0xff) + 0x100, 16).substring(1));
			}
			return sb.toString();
		}
		catch (NoSuchAlgorithmException e)
		{
			input = "";
		}
		return input;
	}
	
	/**
	 * String 을 숫자로 바꿔 Comma 처리
	 *  
	 * @param String str
	 * @return String
	 */
	public static String setComma(String str){
		int result = Integer.valueOf(str);
		DecimalFormat df = new DecimalFormat("#, ##0");
		return df.format(result);
	}
	
    /**
     * 랜덤 문자열을 생성한다.
     *
     * @param  
     * @return String
     * @see
     */
	public static String getRandomString() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
	
    /**
     * 요청된 길이의 랜덤 숫자를 생성한다.
     *
     * @param  int size
     * @return int
     * @see
     */	
	public static int getRandom(int size) {
	    int x = (int)(System.currentTimeMillis() % size);
	    return x;
	}	
}
