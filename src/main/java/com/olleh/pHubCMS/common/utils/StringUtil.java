/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pHubCMS.common.utils;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.util.StringUtils;

import com.olleh.pHubCMS.common.exception.UserException;
import com.olleh.pHubCMS.common.model.ErrorInfoVO;

/**
 * 공통 문자열 유틸리티 정의
 * @Class Name : StringUtil
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
public class StringUtil {
	//private final static Logger log = new Logger(StringUtil.class);
	
	
	/**
	 * 문자열 변수가 널인지 여부를 반환한다.
	 * 
	 * @param v 문자열 변수
	 * @return 널여부 널이면 true
	 * @see
	 */
	public static boolean isNull(String v)
	{
		return v == null || v.equals("");
	}
	
	/**
	 * 대상이 null 이거나 "" 인 경우, 지정한 기본값을 리턴한다.
	 * 
	 * @param String str 검사할 대상
	 * @param value 대상이 null 또는 "" 인 경우 리턴할 기본값
	 * @return String
	 * @see
	 */
	public static String nvl(Object obj, String value) {
		return StringUtils.isEmpty(obj) ? value:String.valueOf(obj);
	}
	
	/**
	 * 대상이 null 이거나 "" 인 경우, 지정한 기본값을 리턴한다.
	 * 
	 * @param String str 검사할 대상
	 * @param String value 대상이 null 또는 "" 인 경우 리턴할 기본값
	 * @return String
	 * @see
	 */
	public static String nvl(Object obj) {
		return nvl(obj, "");
	}
    
    /**
     * str 길이 만큼 마스킹 처리해서 반환한다.
     * @param str
     * @return
     * @see
     */
    public static String getMaskStr(final String str) {
    	StringBuffer result = new StringBuffer();
    	if( !StringUtils.isEmpty(str) ) {
    		for( int i=0; i<str.length(); i++ ) {
    			result.append("*");
    		}
    	}
    	
    	return result.toString();
    }
    
    /**
     * 유저정의 예외를 한곳에서 출력하기 위해 예외 객체에 담았던 에러 관련 정보를 일정한 형식으로 출력
     * 
     * @param ex 예외객체
     * @param message 메세지취득위한 객체
     * @return String 형식화된 문자열
     * @see
     */
    public static String printUserSysMessage(UserException ex) {
    	StringBuffer sbMessage = new StringBuffer();
    	ErrorInfoVO errorInfo = ex.getErrorInfo();
    	sbMessage.append("[");
    	sbMessage.append(errorInfo.getClassName());
    	sbMessage.append(".");
    	sbMessage.append(errorInfo.getMethodName());
    	sbMessage.append("]");
    	sbMessage.append("(");
    	sbMessage.append(errorInfo.getErrCd());
    	sbMessage.append(")");
    	sbMessage.append(errorInfo.getErrMsg());

    	return sbMessage.toString();
    }    
    
    /**
     * IP 주소 마스킹 처리 
     * ***.123.***.123
     * 
     * @param String IP 주소
     * @return String 마스킹 처리된 문자열
     * @see
     */
    public static String getMaskingIP(String str) {
    	StringBuffer result = new StringBuffer();
    	
		Matcher matcher = Pattern.compile("^([0-9]{1,3})\\.([0-9]{1,3})\\.([0-9]{1,3})\\.([0-9]{1,3})$").matcher(str);		
		if(matcher.matches()) {			
			for(int i=1;i<=matcher.groupCount();i++) {
				String replaceTarget = matcher.group(i);
				//첫째, 세번째 그룹 마스킹 처리
				if(i == 1 || i == 3) {
					char[] c = new char[replaceTarget.length()];
					Arrays.fill(c, '*');					
					result.append(String.valueOf(c));
				} else {
					//나머지 그룹 그대로 이어붙임
					result.append(replaceTarget);
				}
				if(i < matcher.groupCount()) { 
					result.append(".");
				}
			}			
		} else { //IP 형식에 맞지 않으면 모두 마스킹
			result.append("*.*.*.*");
		}
    	return result.toString();
    }  
    
    /**
     * 이름 마스킹 처리 
     * 홍길*
     * 
     * @param String 이름
     * @return String 마스킹 처리된 문자열
     * @see
     */
    public static String getMaskingName(String str) {    
    	StringBuffer result = new StringBuffer();
    	if (!StringUtils.isEmpty(StringUtil.nvl(str))) {
    		if (str.length() > 3) {
    			String masking = new String();
    			for (int i=0; i<str.length(); i++) {
    				if (i < 2) {
    					masking += str.charAt(i);
    					continue;
    				}
    				masking += "*";
    			}
    			result.append(masking);
    		} else {
    			result.append(str.substring(0, str.length()-1)).append("*");
    		}
    	}
    	return result.toString();
    }  
    
    /**
     * 휴대번호 마스킹
     * 01012***123
     * 
     * @param String 휴대번호
     * @return String 마스킹 처리된 문자열
     * @see
     */
	public static String getMaskingPhone(String str) {
    	  String phone = new String();
          // 공백제거
          phone = str.replaceAll(" ", "");
          
          // '-'가 포함되어있으면 모두 삭제
          if(phone.contains("-")){
        	  phone = phone.replaceAll("[^0-9]", "");
          }
          
          String masking = "";
          switch (phone.length()) {
          		case 10:
          			masking = phone.substring(0, 4) + "***" + phone.substring(7);
          			masking = masking.substring(0, 3) + "-" + masking.substring(3,6) + "-" + masking.substring(6);
          			break;
          		case 11:
          			masking = phone.substring(0, 5) + "***" + phone.substring(8);
          			masking = masking.substring(0, 3) + "-" + masking.substring(3,7) + "-" +masking.substring(7);
          			break;
          		default:
          			for (int i=0; i<phone.length(); i++) {
          				if (i < 3) {
          					masking += phone.charAt(i);
          					continue;
          				}
          				masking += "*";
          			}
          }
          return masking;
    }  
    
    /**
     * 아이디 마스킹
     * 91207***
     * 
     * @param String 아이디
     * @return String 마스킹 처리된 문자열
     * @see
     */
    public static String getMaskingId(String str) {
    	StringBuffer result = new StringBuffer();
    	if(str != null && str.length() > 3){
    		result.append(str.substring(0, str.length()-3)).append("***");
    	} else{
    		result.append("***");
    	}    	
    	return result.toString();
    }
    
    /**
	 * 랜덤한 숫자를 자리수에 맞게 리턴한다.
	 * 
	 * @return
	 */
	public static String getDocumentNum(int n, int len) {
		String rtnNum = null;
		Random ran = new Random();
		int num = ran.nextInt(n);
		if (num > 0) {
			rtnNum = String.valueOf(num);
			for (int i = rtnNum.length(); i < len; i++) {
				rtnNum = "0" + rtnNum;
			}
		} else {
			num = num * -1;
			rtnNum = String.valueOf(num);
			for (int i = rtnNum.length(); i < len; i++) {
				rtnNum = "0" + rtnNum;
			}
		}
		return rtnNum;
	}
	
	/**
	 * 주어지는 두 날짜의 간격 계산
	 * 
	 * @param time
	 * @param dateBegin
	 * @param dateEnd
	 * @return
	 */
	public static boolean timeMinuteYn(int time, String begin, String end) throws Exception {
        boolean ret = false;
        int timeChk = time * 60 * 60 * 1000 ;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        try {
            long diff = sdf.parse(end).getTime() - sdf.parse(begin).getTime();
            if ((timeChk >= diff) && (diff > 0) ) {
            	ret = true;
            }
        } catch(Exception e){
        	throw e;
        }
        return ret;
    }
}