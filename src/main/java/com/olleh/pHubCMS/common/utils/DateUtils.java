/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 * Revision History
 * Author            Date              Description
 * ------            -----             ------------
 * lys               2018.06.28        일시 관련 정의 Utility
 */
package com.olleh.pHubCMS.common.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.olleh.pHubCMS.common.log.Logger;

public class DateUtils {
	public static final Logger logger = new Logger(DateUtils.class);
	
	static String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
	static String DEFAULT_TIME_FORMAT = "HH:mm:ss";
	static String DEFAULT_DATE_TIME_FORMAT = DEFAULT_DATE_FORMAT +" "+ DEFAULT_TIME_FORMAT;
	
    /**
     * 현재 일자 (yyyyMMdd)
     *
     * @param  
     * @return String
     */
	public static String defaultDate() {
		return dateFormatString(new Date(), "yyyyMMdd");
	}
	
	/**
	 * 현재 시간 (HHmmss)
     *
     * @param  
     * @return String
	 */
	public static String defaultTime() {
		return dateFormatString(new Date(), "HHmmss");
	}
	
	/**
	 * String 일자 형식 변환 기본형 (yyyy-MM-dd)
     *
     * @param  String yyyyMMdd
     * @return String
	 */
	public static String defaultDateFormat(String yyyyMMdd) {
		return dateFormat(yyyyMMdd, DEFAULT_DATE_FORMAT);
	}
	
	/**
	 * String 일자 형식 변환
     *
     * @param  String yyyyMMdd
     * @param  String format
     * @return String
	 */
	public static String dateFormat(String yyyyMMdd, String format) {
		return dateFormat(stringFormatDateTime(yyyyMMdd + defaultTime()), format);
	}
	
	/**
	 * String 시간 형식 변환 기본형 (HH:mm:ss)
     *
     * @param  String HHmmss
     * @return String
	 */
	public static String defaultTimeFormat(String HHmmss) {
		return timeFormat(HHmmss, DEFAULT_TIME_FORMAT);
	}
	
	/**
	 * String 시간 형식 변환
     *
     * @param  String HHmmss
     * @param  String format
     * @return String
	 */
	public static String timeFormat(String HHmmss, String format) {
		return timeFormat(stringFormatDateTime(defaultDate() + HHmmss), format);
	}
	
	/**
	 * String 일시 형식 변환 기본형 (yyyy-MM-dd HH:mm:ss)
     *
     * @param  String yyyyMMddHHmmss
     * @return String
	 */
	public static String defaultDateTimeFormat(String yyyyMMddHHmmss) {
		return dateTimeFormat(yyyyMMddHHmmss, DEFAULT_DATE_TIME_FORMAT);
	}
	
	/**
	 * String 일시 형식 변환
	 *
     * @param  String yyyyMMddHHmmss
     * @param  String format
     * @return String
	 */
	public static String dateTimeFormat(String yyyyMMddHHmmss, String format) {
		return dateTimeFormat(stringFormatDateTime(yyyyMMddHHmmss), format);
	}
	
	
	/**
	 * Date 일자 형식 변환 기본형 (yyyy-MM-dd)
	 *
     * @param  Date date
     * @return String
	 */
	public static String defaultDateFormat(Date date) {
		return dateFormatString(date, DEFAULT_DATE_FORMAT);
	}
	/**
	 * Date 일자 형식 변환
	 *
     * @param  Date date
     * @param  String format
     * @return String
	 */
	public static String dateFormat(Date date, String format) {
		return dateFormatString(date, format);
	}
	
	/**
	 * Date 시간 형식 변환 기본형 (HH:mm:ss)
	 *
     * @param  Date date
     * @return String
	 */
	public static String defaultTimeFormat(Date date) {
		return dateFormatString(date, DEFAULT_DATE_FORMAT);
	}
	
	/**
	 * Date 시간 형식 변환
	 *
     * @param  Date date
     * @param  String format
     * @return String
	 */
	public static String timeFormat(Date date, String format) {
		return dateFormatString(date, format);
	}
	
	/**
	 * Date 일시 형식 변환 기본형 (yyyy-MM-dd HH:mm:ss)
	 *
     * @param  Date date
     * @return String
	 */
	public static String defaultDateTimeFormat(Date date) {
		return dateFormatString(date, DEFAULT_DATE_TIME_FORMAT);
	}
	
	/**
	 * Date 일시 형식 변환
	 *
     * @param  Date date
     * @param  String format
     * @return String
	 */
	public static String dateTimeFormat(Date date, String format) {
		return dateFormatString(date, format);
	}
	
	/**
	 * Date 일시 형식 변환
	 *
     * @param  Date date
     * @param  String format
     * @return String
	 */
	public static String calcDate(String yyyyMMdd, int days) {

	    DateFormat df = new SimpleDateFormat("yyyyMMdd");
        Date date;
        String rtnDate = "";
		try {
			date = df.parse(yyyyMMdd);
			
	        // 날짜 더하기
	        Calendar cal = Calendar.getInstance();
	        cal.setTime(date);
	        cal.add(Calendar.DATE, days);
			//date = cal.getTime();
			rtnDate = dateFormat(cal.getTime(), "yyyyMMdd");
	        //rtnDate = df.format(cal.getTime());
			
		} catch (ParseException e) {
			
		}
         
		return rtnDate;
	}
	
	/**
	 * Date 일시 → String 일시 변환
	 *
     * @param  Date date
     * @param  String format
     * @return String
	 */
	private static String dateFormatString(Date date, String format) {
		return new SimpleDateFormat(format).format(date);
	}
	
	/**
	 * String 일시 → Date 일시 변환
	 *
     * @param  String yyyyMMddHHmmss
     * @return String
	 */
	private static Date stringFormatDate(String yyyyMMdd) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(
			Integer.parseInt(yyyyMMdd.substring(0, 4)),		// year
			Integer.parseInt(yyyyMMdd.substring(4, 6)) - 1,	// month
			Integer.parseInt(yyyyMMdd.substring(6, 8))		// date
		);
		Date date = new Date();
		date = calendar.getTime();
		return date;
	}
	
	/**
	 * String 일시 → Date 일시 변환
	 *
     * @param  String yyyyMMddHHmmss
     * @return String
	 */
	private static Date stringFormatDateTime(String yyyyMMddHHmmss) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(
			Integer.parseInt(yyyyMMddHHmmss.substring(0, 4)),		// year
			Integer.parseInt(yyyyMMddHHmmss.substring(4, 6)) - 1,	// month
			Integer.parseInt(yyyyMMddHHmmss.substring(6, 8)),		// date
			Integer.parseInt(yyyyMMddHHmmss.substring(8, 10)),		// hour
			Integer.parseInt(yyyyMMddHHmmss.substring(10, 12)),		// minute
			Integer.parseInt(yyyyMMddHHmmss.substring(12, 14))		// second
		);
		Date date = new Date();
		date = calendar.getTime();
		return date;
	}
	
	/**
	 * 날짜를 파리미터로 넘겨준 형식대로 취득한다。
	 * 
	 * @param String pattern (YYMMDD날짜형식)
	 * @return String 취득한형식화된 날짜
	 */
    public static String getDateTimeByPattern(String pattern) {
        SimpleDateFormat formatter = new SimpleDateFormat(
                pattern, Locale.KOREAN);
        return formatter.format(new Date());
    }	
}
