/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 * Revision History
 * Author            Date              Description
 * ------            -----             ------------
 * lys               2018.06.28        세션과 관련된 처리를 위한 유틸리티 정의
 */
package com.olleh.pHubCMS.common.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.util.StringUtils;

import com.olleh.pHubCMS.common.model.AdminLoginVO;
import com.olleh.pHubCMS.common.model.ShareVO;

public class SessionUtils {
	private static org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(SessionUtils.class);
	
	/**
	 * 관리자 로그인 처리 결과를 get/set 할때 사용할 키값
	 */
	public static final String ADMINLOGIN_VO = "adminLoginVO";
	
	/**
	 * 공통적으로 사용되는 값들을 get/set 할때 사용할 키값
	 */
	public static final String SHARE_VO = "shareVO";
	
	/**
	 * 사용자메뉴리스트
	 */
	public static final String USER_MENU_LIST = "USER_MENU_LIST";
	
	/**
	 * 사용자메뉴
	 */
	public static final String USER_MENU = "USER_MENU";
	
	
	public static final String CAPTCH_AUTH = "capchaAuth";
	
	
	/**
	 * 현재 request 에서 session 정보를 취득한다.
	 * 
	 * @param request
	 * @return
	 */
	public static HttpSession getSession(final HttpServletRequest request) {
		return request==null?null:request.getSession();
	}
	
	/**
	 * session 에 key 에 해당하는 값이 존재하는지 확인한다.
	 * @return
	 */
	public static boolean isExist(final HttpServletRequest request, String key) {
		Object obj = getSession(request)==null?null:getSession(request).getAttribute(key);
		return obj == null ? false:true;
	}

	
	/**
	 * 공유되는 정보를 가지고 있는 ShareVO를 반환한다. 이전에 저장한 값이 있으면 그 값을 리턴하고, 없을 경우 생성해서 리턴한다.
	 * @param request
	 * @return
	 */
	public static ShareVO getShareVo(final HttpServletRequest request) {
		ShareVO vo = null;
		
		if( SessionUtils.isExist(request, SessionUtils.SHARE_VO) ) {
			vo = (ShareVO)getSession(request).getAttribute(SessionUtils.SHARE_VO);
		} else {
			vo = new ShareVO();
		}
		return vo;
	}

	/**
	 * 공유정보 ShareVO를 세션에 저장한다.
	 * @param request
	 * @param vo
	 */
	public static void setShareVo(final HttpServletRequest request, ShareVO vo) {
		getSession(request).setAttribute(SessionUtils.SHARE_VO, vo);
	}
	
	/**
	 * 공유정보 ShareVO를 세션에서 제거한다.
	 * @param request
	 */
	public static void removeShareVo(final HttpServletRequest request) {
		getSession(request).removeAttribute(SessionUtils.SHARE_VO);
	}
	
	/**
	 *  ShareVO 의 shareMap 에 key, value 값을 반환한다.
	 *  
	 * @param key
	 * @param request
	 * @return
	 */
	public static Object getShareMapValue(final String key, final HttpServletRequest request) {
		final Map<String, Object> shareMap = SessionUtils.getShareVo(request).getShareMap();
		if( shareMap != null ) {
			return shareMap.get(key);
		}
		return null;
	}
	
	/**
	 * ShareVO 의 shareMap 에 key, value 값을 추가한다.
	 * 
	 * @param key
	 * @param value
	 * @param request
	 */
	public static void putShareMapValue(final String key, final Object value, final HttpServletRequest request) {
		final ShareVO vo = SessionUtils.getShareVo(request);

		Map<String, Object> shareMap = vo.getShareMap();
		if( shareMap == null ) {
			shareMap = new HashMap<String, Object>();
		}
		
		shareMap.put(key, value);
		
		vo.setShareMap(shareMap);
		SessionUtils.setShareVo(request, vo);
	}
	
	/**
	 * 공유정보 shareMap을 세션에서 제거한다.
	 * @param request
	 */
	public static void removeShareMap(final HttpServletRequest request) {
		final ShareVO vo = SessionUtils.getShareVo(request);
		vo.setShareMap(null);
		SessionUtils.setShareVo(request, vo);
	}
	
	
	/**
	 * 사용자의 로그인 여부를 반환한다.
	 * @param request
	 * @return true: 로그인 false:비로그인
	 */
	public static boolean isLogin(final HttpServletRequest request) {
		boolean result = false;
		
		final AdminLoginVO vo = getAdminLoginVO(request);
		if( vo != null ) {
			final String loginYn = StringUtil.nvl(vo.getLoginYn(), "N");
			result = "Y".equals(loginYn) ? true:false;
		}
		
		return result;
	}
	
	/**
	 * 로그인한 id를 리턴한다.
	 * @param request
	 * @return
	 */
	public static String getUserId(final HttpServletRequest request) {
		String custNo = SessionUtils.getAdminLoginVO(request).getUserId();
		
		return StringUtil.nvl(custNo);
	}
	
	/**
	 * 로그인한 정보를 리턴한다.
	 * @param request
	 * @return
	 */
	public static String getUserNm(final HttpServletRequest request) {
		String userNm = SessionUtils.getAdminLoginVO(request).getUserNm();
		
		if( StringUtils.isEmpty(userNm) ) userNm = "???";
		
		return userNm;
	}

	/**
	 * 로그인 처리 결과 AdminLoginVO를 반환한다.
	 * 이전에 저장한 값이 있으면 그 값을 리턴하고, 없는 경우 생성해서 리턴한다.
	 */
	public static AdminLoginVO getAdminLoginVO(final HttpServletRequest request) {
		AdminLoginVO vo = null;
		
		if (SessionUtils.isExist(request, SessionUtils.ADMINLOGIN_VO)) {
			vo = (AdminLoginVO) getSession(request).getAttribute(SessionUtils.ADMINLOGIN_VO);
		} else {
			vo = new AdminLoginVO();
		}
		return vo;
	}
	
	/**
	 * 관리자 로그인 처리 결과 AdminLoginVO를 세션에 저장한다.
	 */
	public static void setAdminLoginVO(final HttpServletRequest request, AdminLoginVO vo) {
		getSession(request).setAttribute(SessionUtils.ADMINLOGIN_VO, vo);
	}
	
	
	/**
	 * 사용자 메뉴정보 UserMenuListVO 를 반환 한다.. 이전에 저장한 값이 있으면 그 값을 리턴하고, 없을 경우 생성해서 리턴한다.
	 * @param request
	 * @return
	 */
	public static List getUserMenuList(final HttpServletRequest request) {
		List vo = null;
		
		if( SessionUtils.isExist(request, SessionUtils.USER_MENU_LIST) ) {
			vo = (List)getSession(request).getAttribute(SessionUtils.USER_MENU_LIST);
		} else {
			vo = new ArrayList();
		}
		
		return vo;
	}
	
	/**
	 * 사용자메뉴정보 UserMenuListVO를 세션에 저장한다.
	 * @param request
	 * @param vo
	 */
	public static void setUserMenuList(final HttpServletRequest request, List vo) {
		getSession(request).setAttribute(SessionUtils.USER_MENU_LIST, vo);
	}
	
	/**
	 * 사용자메뉴정보 CertificationVO를 세션에서 제거한다.
	 * @param request
	 * @param vo
	 */
	public static void removeUserMenuList(final HttpServletRequest request) {
		getSession(request).removeAttribute(SessionUtils.USER_MENU_LIST);
	}	
	
	/**
	 * 현재 접근한 사용자 메뉴를 세션에 저장한다.
	 * @param request
	 * @param vo
	 */
	public static void setUserMenu(final HttpServletRequest request, Map<String, Object> map) {
		getSession(request).setAttribute(SessionUtils.USER_MENU, map);
	}
	
	/**
	 * 현재 접근한 사용자 메뉴를 세션에서 가져온다.
	 * @param request
	 * @param vo
	 */
	public static Map<String, Object> getUserMenu(final HttpServletRequest request) {
		Map<String, Object> map;
		
		if( SessionUtils.isExist(request, SessionUtils.USER_MENU) ) {
			map = (Map<String, Object>) getSession(request).getAttribute(SessionUtils.USER_MENU);	
		} else {
			map = null;
		}	
		return map;
	}
	
	/**
	 * Capcha 인증 성공 여부를 리턴한다
	 * 
	 * @param request
	 * @return
	 */
	public static String getCapchaAuth(final HttpServletRequest request) {
		String ret = null;
		if (SessionUtils.isExist(request, SessionUtils.USER_MENU) ) {
			ret = StringUtil.nvl(getSession(request).getAttribute(SessionUtils.CAPTCH_AUTH));
		} else {
			ret = "F";
		}
		return ret;
	}
}