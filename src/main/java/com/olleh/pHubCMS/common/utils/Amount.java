/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 * Revision History
 * Author            Date              Description
 * ------            -----             ------------
 * lys               2018.06.28        금액관련 유틸리티
 */
package com.olleh.pHubCMS.common.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;

import com.olleh.pHubCMS.common.log.Logger;

public class Amount {
	
	/** 메소드명칭 */
    static DecimalFormat dfNumber = new DecimalFormat("#,##0.####");
    static DecimalFormat dfNumberPoint1 = new DecimalFormat("#,##0.0");
    static DecimalFormat dfNumberPoint2 = new DecimalFormat("#,##0.00");
    static DecimalFormat dfNumberPoint3 = new DecimalFormat("#,##0.000");
    static DecimalFormat dfNumberPoint4 = new DecimalFormat("#,##0.0000");
    static DecimalFormat dfNumberPoint5 = new DecimalFormat("###0.##");
    static DecimalFormat dfNumberPoint6 = new DecimalFormat("###0");

    
    /**
     * 정수형 숫자를 #,###,### 타입의 문자열로 리턴 한다.
     *
     * @param  int iNumber
     * @return String
     */
    public static String num(int iNumber) {
        return dfNumber.format(iNumber);
    }

    public static String num(int iNumber, int i_point) {
        return num((double) iNumber, i_point);
    }

    /**
     * long형 숫자를 #,###,### 타입의 문자열로 리턴 한다.
     *
     * @param  Long lNumber
     * @return String 
     */
    public static String num(Long lNumber) {
        return dfNumber.format(lNumber);
    }
    public static String num(Long lNumber, int i_point) {
        return num((double) lNumber, i_point);
    }

    /**
     * 실수형 숫자를 #,###,### 타입의 문자열로 리턴 한다.
     *
     * @param  double dNumber
     * @return String 
     */
    public static String num(double dNumber) {
        return dfNumber.format(dNumber);
    }

    /**
     * 실수형 숫자를 정의된 타입의(ex)#,###,###) 문자열로 리턴 한다.
     *
     * @param  double dNumber
     * @param  int    i_point (소수점 자리수)
     * @return String
     */    
    public static String num(double dNumber, int i_point) {
        try {
	        switch (i_point) {
	            case 1:
	            return dfNumberPoint1.format(dNumber);
	            case 2:
	            return dfNumberPoint2.format(dNumber);
	            case 3:
	            return dfNumberPoint3.format(dNumber);
	            case 4:
	            return dfNumberPoint4.format(dNumber);
	            case 5:
	            return dfNumberPoint5.format(dNumber);
	            default :
	            return "";
	        }
        }catch(Exception e) {
        	return "";
        }
    }

    /**
     * 숫자형 문자열을 #,###,### 타입의 문자열로 리턴 한다.
     * 
     * @param  String strNumber
     * @return String
     */
    public static String num(String strNumber) {
        double dNumber = 0.;
        try {
            dNumber = Double.parseDouble(strNumber);
        } catch (Exception ne) {
            return "0";
        }
        return dfNumber.format(dNumber);
    }
    
    /**
     * 숫자형 문자열을 정의된 타입의(ex)#,###,###) 문자열로 리턴 한다.
     *
     * @param  String strNumber
     * @param  int    i_point (소수점 자리수)
     * @return String
     */    
    public static String num(String strNumber, int i_point) {
        double dNumber = 0.;
        try {
            dNumber = Double.parseDouble(strNumber);
        } catch (Exception ne) {
            return "0";
        }
        return num(dNumber, i_point);
    }

    /**
     * BigDecimal형을  #,###,### 타입의 문자열로 리턴 한다.
     * 
     * @param  BigDecimal bdNumber
     * @return String
     */
    public static String num(BigDecimal bdNumber) {
        double dNumber = bdNumber.doubleValue();
        return num(dNumber);
    }
    
    /**
     * BigDecimal형을 정의된 타입의(ex)#,###,###) 문자열로 리턴 한다.
     *
     * @param  BigDecimal bdNumber
     * @param  int    i_point (소수점 자리수)
     * @return String
     */ 
    public static String num(BigDecimal bdNumber, int i_point) {
        double dNumber = bdNumber.doubleValue();
        return num(dNumber, i_point);
    }
    
   /**
    * String 값을 #,###,###,##0.#0 형식으로 바꿈
    * 
    * @param String str
    * @return #,###,###,###.00
    */
    public static String fFormat(String str) {
        if (str == null)
            return "";
        if (str.length() == 0)
            return "";

        DecimalFormat df = new DecimalFormat("#,###,###,##0.00");

        try {
            return df.format(Long.parseLong(str));
        } catch (NumberFormatException nfe) {
            try {
            	Double dVal = Double.valueOf(str);
                return df.format(dVal);
            } catch (Exception ee) {
                return "0";
            }
        } catch (Exception e) {
            return "0";
        }
    }

    /**
     * 실수형 문자열을 정수형타입의 문자열로 리턴 한다.
    * 
    * @param String str
    * @return #,###,###,###.00
     */
    public static String intFormat(String str) {
        if (str == null)
            return "";
        if (str.length() == 0)
            return "";

        try {
            return dfNumberPoint6.format(Double.parseDouble(str));
        } catch (NumberFormatException nfe) {
            try {
            	Double dVal = Double.valueOf(str);
                return dfNumberPoint6.format(dVal);
            } catch (Exception ee) {
                return "0";
            }
        } catch (Exception e) {
            return "0";
        }
    }

}