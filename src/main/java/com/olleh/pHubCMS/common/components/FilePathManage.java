/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pHubCMS.common.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 파일경로(업로드/다운로드 등)관리 콤포넌트
 * @Class Name : FilePathManage
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
@Component
public class FilePathManage {
	
	@Autowired
	private Constant constant;
	
	
	/**
	 * <pre>메뉴별 상세페이지파일 업로드 경로</pre>
     *
     * @param  String dvcDvCd
     * @param  String mnuCd
     * @return String
     * @see
	 */
	public String getFilePathMnu(String dvcDvCd, String mnuCd) {
		final String rootPath = ("PC".equals(dvcDvCd))?constant.FILE_UPLOAD_CTNT:constant.FILE_UPLOAD_MCTNT;
		
		return rootPath + "/"+ mnuCd;
	}
	
	/**
	 * <pre>상세페이지파일 업로드 경로</pre>
     *
     * @param  String dvcDvCd
     * @param  String mnuCd
     * @return String
     * @see
	 */
	public String getFilePath(String dvcDvCd, String mnuCd) {
		final String rootPath = ("PC".equals(dvcDvCd))?constant.FILE_UPLOAD_CTNT:constant.FILE_UPLOAD_MCTNT;
		
		return rootPath;
	}
}