/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 */
package com.olleh.pHubCMS.common.components;

import org.springframework.stereotype.Component;

/**
 * 상수관리 콤포넌트
 * @Class Name : Constant
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
@Component
public class Constant {
	
	
	/**
	 * 결과 (성공/실패)
	 */	
	public static String SUCCESS      = "SUCCESS";
	public static String FAIL         = "FAIL";
	public static String SUCCESS_CODE = "0";
	public static String SUCCESS_MSG  = "성공";
	public static String FAIL_CODE    = "-1";
	public static String FAIL_MSG     = "처리에 실패했습니다.";
	public static String JSON_OBJECT  = "JSON_OBJECT";
	public static String MENU_OBJECT  = "MENU_OBJECT";
	
	/**
	 * 연동시 공통성공여부, 결과코드, 결과메시지
	 */
	public static String SUCCESS_YN = "successYn";
	//public static String RET_CODE   = "ret_code";
	//public static String RET_MSG    = "ret_msg";
	public static String RET_CODE   = "eCode";
	public static String RET_MSG    = "eMsg";
	
	/**
	 * 각종 상태
	 */	
	// 회원상태
	public static String STAT_CD_1000 = "1000";		// 재직
	public static String STAT_CD_2000 = "2000";		// 퇴사
	public static String STAT_CD_3000 = "3000";		// 휴면
	public static String STAT_CD_4000 = "4000";		// 잠금
	
	// 로그인 상태
	public static String LOGIN_STAT_CD_IS = "IS";	// 로그인 성공
	public static String LOGIN_STAT_CD_IF = "IF";	// 로그인 실패
	public static String LOGIN_STAT_CD_OS = "OS";	// 로그아웃 성공
	public static String LOGIN_STAT_CD_OF = "OF";	// 로그아웃 실패
	
	/**
	 * 업로드 한 파일에 접근 하기 위한 액션명
	 */
	public static String ACCESS_EXTERNAL_RESOURCE_ACTION_URL = "/data/res";
	
	/**
	 * 사용로그 기록
	 */	
	public static String LOG_WRITE_YN = "LOG_WRITE_YN";
	
	/**
	 * 파일포멧
	 */	
	public String FILE_TYPE_SEPARATOR = ",";
	public String FILE_TYPE_PDF       = "PDF";
	public String FILE_TYPE_ZIP       = "ZIP";
	public String FILE_TYPE_HTML      = "HTML";
	public String FILE_TYPE_EXCEL     = "EXCEL";
	public String FILE_TYPE_IMAGE     = "IMAGE";
	
	/**
	 * 파일 업로드 경로
	 */	
	public String FILE_UPLOAD_CTNT    = "contents";	// PC 콘텐츠 파일 업로드 최상위 경로
	public String FILE_UPLOAD_MCTNT   = "mcontents";	// 모바일 콘텐츠 파일 업로드 최상위 경로
	
	/**
	 * 파일업로드시 파일명 변환여부 항목
	 */	
	public String FILE_RENAME_YN      = "FILE_RENAME_YN";
	
	/**
	 * 비밀번호 오류체크 횟수 (5회 이상이면 제약조건을 수행한다.)
	 */	
	public static int PWD_ERR_LIMIT_CUNT = 5;
	
	/**
	 * ajax request header 값
	 * 해당값이 있을 경우 ajax 통신으로 판단하고, 관련 로직을 처리한다.
	 */	
	public static String AJAX_HEADER = "PH_AJAX";	
	
	public static String MENU_ROOT_ID = "ME00000000";
	
	public static String[] INQR_ACTION_URLS = new String[] {
		"/voc/jgSettleDealHist.do"
		, "/voc/jgCustInfo.do"
		, "/voc/jgAgrHist.do"
	};
	
	/**
	 * 값 필터 제외 파라미터 명
	 */
	public static String[] EXCP_PARAMTR_KEYS = {
		"PRMT_VAL"			// 시스템파라미터 관리 - 파라미터값
		, "PRMT_DESC"		// 시스템파라미터 관리 - 파라미터설명
		, "PNT_CD"			// 포인트 제공처 관리 - 포인트코드
		, "DP_UNIT"			// 포인트 제공처 관리 - 표시단위
		, "PNT_RULE"		// 포인트 제공처 관리 - 포인트규칙
		, "MSG_TITL"		// 기프티쇼 단말할인권 문자발송 정보 관리 - 메시지제목
		, "MSG_BODY"		// 기프티쇼 단말할인권 문자발송 정보 관리 - 메시지본문
		, "MSG_NM"			// 시스템 메시지 관리 - 메시지명
		, "RMRK"			// 공통코드 관리, 시스템 메시지 관리 - 비고
		, "USER_PW"			// 사용자정보 관리 - 비밀번호
		, "password"		// 로그인 - 비밀번호
	};
	
	/**
	 * API로그기록 여부(response값 로그기록여부만 제어 한다.)
	 */	
	public final static String API_LOG_WRITE_YN = "logWriteYN";	
}