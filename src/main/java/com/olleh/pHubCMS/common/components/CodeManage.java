/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 */
package com.olleh.pHubCMS.common.components;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.olleh.pHubCMS.common.dao.CommonDAO;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.CmnCdVO;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 공통코드 관리 콤포넌트
 * @Class Name : CodeManage
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
@Component
public class CodeManage {
	//private org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(this.getClass());
	private Logger log = new Logger(this.getClass());
	
	/**
	 * 공통코드맵
	 */	
	private static Map<String, List<CmnCdVO>> admCmnCdMap;
	//private static Map<String, List<Map>> admCmnCdMap;
	
	private List<Map<String, Object>> list;
	private Map<String, Object> map;
	
	@Autowired
	private CommonDAO commonDao;
	
	/**
	 * <pre>요청한 그룹코드의 상세코드리스트를 리턴 한다. (전체)</pre>
     *
     * @param  String code
     * @param  String name
     * @return 
     * @see
	 */
	public List<CmnCdVO> getCodeList(String indCd) {
		return get(indCd);
	}
	
	/**
	 * <pre>요청한 그룹코드의 상세코드리스트를 리턴 한다. (USE_YN=Y, DP_YN=Y 인 건만)</pre>
     *
     * @param  String code
     * @param  String name
     * @return 
     * @see
	 */
	public List<CmnCdVO> getCodeListY(String indCd) {
		// declare
		List<CmnCdVO> list = new ArrayList<CmnCdVO>();
		List<CmnCdVO> tmpList = get(indCd);

		// use_yn(사용여부), dp_yn(출력여부)가 Y인 데이터만 모아서 리턴 한다.
		for(CmnCdVO map : tmpList) {
			//if( "Y".equals(StringUtil.nvl(map.get("dp_yn"), "N")) ) list.add(map);
			if( ("Y".equals(StringUtil.nvl(map.getDpYn(), "N"))) && ("Y".equals(StringUtil.nvl(map.getUseYn(), "N"))) ) list.add(map);
		}
		
		return list;
	}
	
//	public List<CmnCdVO> getCodeList(String indCd) {
//		return admCmnCdMap.get(indCd);
//	}
	
	/**
	 * <pre>Map<String, Object> 설정</pre>
     *
     * @param  String code
     * @param  String name
     * @return 
     * @see
	 */
	private void setMap2(String code, String name) {
		map = new HashMap<String, Object>();
		map.put("code", code);
		map.put("name", name);
		
		list.add(map);
	}

	
	/**
	 * <pre>코드값 조회</pre>
     *
     * @param  
     * @return 
     * @see
	 */
	private Map<String, List<CmnCdVO>> getAllList() {
		String methodName = "getAllList";
		
		log.debug("getAllList", "getAllList() Start!");
		// declare
		Map<String, List<CmnCdVO>> map = new HashMap<String, List<CmnCdVO>>();
		List<CmnCdVO> tmpList          = new ArrayList<CmnCdVO>();		
		List<CmnCdVO> list             = new ArrayList<CmnCdVO>();
		
		String indCd    = "";	// 마스터코드
		String tmpIndCd = "";	// tmp마스터코드		
		
		try {
			// 전체 공통코드 조회
			list = commonDao.getAllCodeList();
			
			// 마스터코드별로 Map에 해당디테일코드List를 등록
			for(CmnCdVO vo : list) {
				//tmpIndCd = StringUtil.nvl(vo.get("ind_cd"));
				tmpIndCd = StringUtil.nvl(vo.getIndCd());
				
				if( !indCd.equals(tmpIndCd) ) {
					if( tmpList.size() > 0 ) map.put(indCd, new ArrayList<CmnCdVO>(tmpList));
					indCd = tmpIndCd;
					tmpList.clear();
				}
				
				tmpList.add(vo);
			}
			
			// 마지막건 처리
			if( tmpList.size() > 0 ) {
				map.put(indCd, new ArrayList<CmnCdVO>(tmpList));
			}
			
		} catch (Exception e) {
			log.printStackTracePH(methodName, e);
		}
		return map;
	}

	/**
	 * <pre>admCmnCdMap 설정</pre>
     *
     * @param  
     * @return 
     * @see <pre>
     *  1. 전체 공통코드(ADM_CMN_CD_M, ADM_CMN_CD_D)를 db에서 읽어서 admCmnCdMap에 저장 한다.
     *  </pre>
	 */	
	@PostConstruct
	public void start() {
		// 공통코드 메모리 로드
		admCmnCdMap = getAllList();
	}
	
	/**
	 * <pre>Map<String, Object> 설정</pre>
     *
     * @param  String code
     * @param  String name
     * @return 
     * @see
	 */
	public void reset() {
		admCmnCdMap = getAllList();
	}
	
	/**
	 * <pre>요청한 그룹코드의 상세코드 객체를 리턴 한다.</pre>
     *
     * @param  String indCd
     * @return List<CmnCdVO>
     * @see
	 */	
	private List<CmnCdVO> get(String indCd) {
		List<CmnCdVO> list = admCmnCdMap.get(indCd);
		
		if( list == null ) list = new ArrayList<CmnCdVO>();
		
		return list;
	}
}