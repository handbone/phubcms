/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 */
package com.olleh.pHubCMS.common.components;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.olleh.pHubCMS.common.dao.CommonDAO;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.MsgInfoVO;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 메시지관리 콤포넌트
 * @Class Name : MessageManage
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
@Component
public class MessageManage {
	private Logger log = new Logger(this.getClass());
	
	// 시스템메시지맵
	private static Map<String, MsgInfoVO> admMsgMap;
	//private static Map<String, Map<String, Object>> admMsgMap;
	
	@Autowired
	private CommonDAO commonDao;

	/**
	 * <pre>요청한 메시지ID의 메시지코드를 리턴 한다.</pre>
     *
     * @param  String msgId
     * @return 
     * @see
	 */
	public String getMsgCd(String msgId) {
		String rtn       = "";
		MsgInfoVO tmpMap = get(msgId);
		
		// 사용여부가 Y인것만 리턴한다.
		if( "Y".equals(StringUtil.nvl(tmpMap.getUseYn(), "N")) ) rtn = StringUtil.nvl(tmpMap.getMsgCd());
		
		return rtn;
	}
	
	/**
	 * <pre>요청한 메시지ID의 메시지를 리턴 한다.</pre>
     *
     * @param  String msgId
     * @return 
     * @see
	 */
	public String getMsgTxt(String msgId) {
		String rtn       = "";
		MsgInfoVO tmpMap = get(msgId);
		
		// 사용여부가 Y인것만 리턴한다.
		if( "Y".equals(StringUtil.nvl(tmpMap.getUseYn(), "N")) ) rtn = StringUtil.nvl(tmpMap.getMsgNm());
		
		return rtn;
	}
	/**
	 * <pre>요청한 메시지코드의 정보를 리턴 한다. (전체)</pre>
     *
     * @param  String msgId
     * @return 
     * @see
	 */
	public MsgInfoVO getMsgVO(String msgId) {
		return get(msgId);
	}	
	
	/**
	 * <pre>요청한 메시지코드의 정보를 리턴 한다. (USE_YN=Y 인 건만)</pre>
     *
     * @param  String msgId
     * @return 
     * @see
	 */
	public MsgInfoVO getMsgVOY(String msgId) {
		// declare
		MsgInfoVO map    = new MsgInfoVO();
		MsgInfoVO tmpMap = get(msgId);
		
		// use_yn(사용여부)가 Y인 데이터만 리턴 한다.
		//if( "Y".equals(StringUtil.nvl(tmpMap.get("use_yn"), "N")) ) map = tmpMap;
		if( "Y".equals(StringUtil.nvl(tmpMap.getUseYn(), "N")) ) map = tmpMap;
		
		return map;
	}
	
//	public List<CmnCdVO> getCodeList(String indCd) {
//		return admCmnCdMap.get(indCd);
//	}
	
	/**
	 * <pre>시스템메시지 조회</pre>
     *
     * @param  
     * @return
     * @see
	 */
	private Map<String, MsgInfoVO> getAllList() {
		log.debug("getAllList", "getAllList() Start!");
		// declare
		Map<String, MsgInfoVO> map = new HashMap<String, MsgInfoVO>();		
		List<MsgInfoVO> list       = new ArrayList<MsgInfoVO>();
		String cd      = "";	// 코드
		
		try {
			// 전체 시스템메시지 조회
			list = commonDao.getAllMsgList();
			
			// 메시지코드별로 Map에 해당정보 등록
			for(MsgInfoVO vo : list) {
				//cd = StringUtil.nvl(vo.get("msg_id"));
				cd = StringUtil.nvl(vo.getMsgId());
				
				map.put(cd, vo);
			}			
		} catch (Exception e) {
			log.printStackTracePH("getAllList", e);
		}
		return map;
	}
	
	/**
	 * <pre>admMsgMap 설정</pre>
     *
     * @param  
     * @return 
     * @see
     *  1. 전체 시스템메시지를(ADM_MSG_INFO)를 db에서 읽어서 admMsgMap에 저장 한다.
	 */	
	@PostConstruct
	public void start() {
		// 메시지정보 메모리 로드
		admMsgMap = getAllList();
	}
	
	/**
	 * <pre>admMsgMap 재설정</pre>
     *
     * @param  
     * @return
     * @see
	 */
	public void reset() {
		admMsgMap = getAllList();
	}
	
	/**
	 * <pre>요청한 메시지코드의 MsgInfoVO를 리턴 한다.</pre>
     *
     * @param  String msgId
     * @return MsgInfoVO
     * @see
	 */	
	private MsgInfoVO get(String msgId) {
		MsgInfoVO map = admMsgMap.get(msgId);
		
		if( map == null ) map = new MsgInfoVO();
		
		return map;
	}
}