/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pHubCMS.common.components;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Component;

import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * RSA 관련 Component
 * @Class Name : RsaManager
 * @author mgBang
 * @since 2019.04.11.
 * @version 1.0
 * @see <pre>
 * << 관련문서 (SVN Path) >>
 * \개발문서\9 참고\kt보안가이드\공통인증모듈-상세개발가이드_전체_v.1.5.pdf
 * \개발문서\9 참고\kt보안가이드\보안인증모듈_1.6.1_source.zip
 * 
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2019.04.11   mgBang     최초생성
 * </pre>
 */
@Component
public class RsaManager {
	public static final String RSA_INSTANCE_ID = "RSA";
	public static final String RSA_PUBLIC_KEY = "spublickey";
	public static final String RSA_PRIVATE_KEY = "sprivatekey";
	public static final String RSA_MODULUS_KEY = "publickeymodulus";
	public static final String RSA_EXPONENT_KEY = "publickeyexponent";
	
	private static final String CIPHER_INSTANCE_ID = "RSA/ECB/PKCS1Padding";
	
	private Logger log = new Logger(this.getClass());
	
	/**
	 * RSA 키 생성 및 저장
	 * 
	 * @param userId
	 * @return
	 */
	public Map<String, String> generator() {
		String method = "RsaMakeKey.generator";
		String stepCd = "0";
		SecureRandom secureRandom = new SecureRandom();
		Map<String, String> rtnMap = null;
		try {
			stepCd = "1";
			KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(RSA_INSTANCE_ID);
			keyPairGenerator.initialize(2048, secureRandom);
			// 공개키, 비밀키 생성
			stepCd = "2";
			KeyPair keyPair = keyPairGenerator.genKeyPair();
			PublicKey publicKey = keyPair.getPublic();
			PrivateKey privateKey = keyPair.getPrivate();
			// Modulus, Exponent 생성
			stepCd = "3";
			RSAPublicKeySpec publicSpec = (RSAPublicKeySpec) KeyFactory.getInstance(RSA_INSTANCE_ID).getKeySpec(publicKey, RSAPublicKeySpec.class);
			String modulus = publicSpec.getModulus().toString(16);
			String exponent = publicSpec.getPublicExponent().toString(16);
			// Map 세팅
			stepCd = "4";
			rtnMap = new HashMap<String, String>();
			rtnMap.put(RSA_PUBLIC_KEY, Base64.encodeBase64String(publicKey.getEncoded()));
			rtnMap.put(RSA_PRIVATE_KEY, Base64.encodeBase64String(privateKey.getEncoded()));
			rtnMap.put(RSA_MODULUS_KEY, modulus);
			rtnMap.put(RSA_EXPONENT_KEY, exponent);
		} catch (Exception e) {
			
			log.error(method, "[stepCd:" + stepCd + "] "+ e.getMessage());
		}
		
		return rtnMap;
	}
	
	/**
	 * RSA 암호화
	 * 
	 * @param pPublicKey
	 * @param value
	 * @return
	 * @throws Exception
	 */
	public String encrypt(String sPublicKey, String value) throws Exception {
		PublicKey publicKey = KeyFactory.getInstance(RSA_INSTANCE_ID).generatePublic(new X509EncodedKeySpec(Base64.decodeBase64(sPublicKey.getBytes())));

		Cipher cipher = Cipher.getInstance(CIPHER_INSTANCE_ID);
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);

		// 공개키 이용 암호화
		return Base64.encodeBase64String(cipher.doFinal(value.getBytes()));
	}
	
	/**
	 * RSA 복호화
	 * 
	 * @param value
	 * @return
	 * @throws Exception
	 */
	public String decrypt(String sPrivateKey,String value) throws Exception {
		if( "".equals(StringUtil.nvl(value)) ) {
			return "";
		} else {
			PrivateKey privateKey = KeyFactory.getInstance(RSA_INSTANCE_ID).generatePrivate(new PKCS8EncodedKeySpec(Base64.decodeBase64(sPrivateKey.getBytes())));

			Cipher cipher = Cipher.getInstance(CIPHER_INSTANCE_ID);
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			
			// 개인키 이용 복호화
			return new String(cipher.doFinal(Base64.decodeBase64(value.getBytes())));			
		}
	}
	
	/**
	 * WEB UI 용 RSA 복호화
	 * 
	 * @param value
	 * @return
	 * @throws Exception
	 */
	public String webDecrypt(String sPrivateKey, String value) throws Exception {
		if( "".equals(StringUtil.nvl(value)) ) {
			return "";
		} else {
			PrivateKey privateKey = KeyFactory.getInstance(RSA_INSTANCE_ID).generatePrivate(new PKCS8EncodedKeySpec(Base64.decodeBase64(sPrivateKey.getBytes())));

			Cipher cipher = Cipher.getInstance(CIPHER_INSTANCE_ID);
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			
			return new String(cipher.doFinal(hexToByArray(value)));			
		}
	}
	
	/**
	 * hexToByArray
	 * 
	 * @param value
	 * @return
	 */
	public static byte[] hexToByArray(String value) {
		if (value == null || value.length() % 2 != 0) {
			return new byte[] {};
		}
		byte[] valByte = new byte[value.length() / 2];
		for (int i = 0; i < value.length(); i += 2) {
			byte val = (byte) Integer.parseInt(value.substring(i, i + 2), 16);
			valByte[(int) Math.floor(i / 2)] = val;
		}
		return valByte;
	}
}
