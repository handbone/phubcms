/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pHubCMS.common.components;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;

import org.aopalliance.intercept.MethodInterceptor;
import org.springframework.aop.framework.ProxyFactoryBean;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.aop.support.NameMatchMethodPointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.utils.Common;
import com.olleh.pHubCMS.common.utils.JsonUtil;
import com.olleh.pHubCMS.common.utils.SystemUtils;

/**
 * 비즈니스 로직과는 별개의 유틸리티성 기능을 정의한다.
 * @Class Name : UtilsComponent
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
@Component
public class UtilsComponent  {
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	private Constant constant;
	
	/**
	 * @Value("#{cfg['fileUploadPath']}")
	 */	
	private String fileUploadPath = "D:\\upload\\";
	
	String transZipFileName = "upload.zip";
	
	
	/**
	 * <pre>ajax 통신시 메소드에서 jsonObject 리턴할때의 공통적인 후처리를 기술 한다.</pre>
     *
	 * @param request		HttpServletRequest
	 * @param jsonObject	리턴 jsonObject
	 * @param isFlag        request set 여부 (true일 경우에만 request에 set 한다.)
     * @return Map<String, List<String>>
     * @see
     *  1. 공통 키등을 추가처리 할 수 있다.
	 */
	public String afterResMap(HttpServletRequest request, Map<String, Object> jsonObject, boolean isFlag) throws Exception {
		
		if( request == null ) {
			log.debug("afterResponseBody", "request is Null.");
		}
		
		if( jsonObject == null ) {
			log.debug("afterResponseBody", "jsonObject is Null.");
		}		
		
		// 사용로그 처리를 위하여 request에 jsonObject를 set 한다.
		if( isFlag ) {
			request.setAttribute(constant.JSON_OBJECT, jsonObject.get(constant.RET_CODE));
		}
		//return jsonObject;
		return JsonUtil.MapToJson(jsonObject);
	}
	public String afterResMap(HttpServletRequest request, Map<String, Object> jsonObject) throws Exception {
		
		//return jsonObject;
		return afterResMap(request, jsonObject, false);
	}
	
	/**
	 * <pre>업로드된 결과 맵에서 원하는 필드의 파일 경로를 반환 한다.</pre>
	 * @param id  필드 name
	 * @param basePath  업로드 기본 경로
	 * @param result 업로드 결과 맵
	 * @return String
	 * @see
	 */
	public String makeTransPath(String id, String basePath, Map<String, List<String>> result) {
		return makeTransPath(id, 0, basePath, result);
	}
	/**
	 * <pre>업로드된 결과 맵에서 html형태의 원하는 필드의 파일 경로를 반환 한다.</pre>
	 * @param id  필드 name
	 * @param basePath  업로드 기본 경로
	 * @param result 업로드 결과 맵
	 * @return String
	 * @see
	 */
	public String makeHtmlTransPath(String id, String basePath, Map<String, List<String>> result) {
		return makeTransPath(id, -1, basePath, result);
	}
	/**
	 * <pre>업로드된 결과 맵에서 원하는 필드의 파일 경로를 반환 한다.</pre>
	 * @param id  필드 name
	 * @param index  필드 index  값이 0보다 작으면.. 파일 리스트중 zip파일과 index.html 파일이 있는지 검사해서 index.html 파일을 반환 한다. 없으면 0번째 파일을 반환 한다.
	 * @param basePath  업로드 기본 경로
	 * @param result 업로드 결과 맵
	 * @return
	 * @see
	 */
	public String makeTransPath(String id, int pIndex, String basePath, Map<String, List<String>> result) {
		int index = pIndex;
		if(result != null && !result.isEmpty() && !Common.isNull(id) && !Common.isNull(basePath)) {
			List<String> result2 = result.get(id);
			if(result2 != null && !result2.isEmpty()) {
				if(index == -1) {
					int c = 0;
					for(String ritm : result2) {
						if(ritm.lastIndexOf(".html") != -1 || ritm.lastIndexOf(".zip") != -1) {
							if(ritm.lastIndexOf(".html") != -1) {
								index = result2.indexOf(ritm);
							}
							c++;
						}
						
						if(c >= 2 && index >= 0) break;
					}
					if(index < 0) index = 0;
				}
				String fileName = result2.get(index);
				if(!Common.isNull(fileName)) {
					String transPath = StringUtils.replace(String.format("%s/%s", basePath, fileName), "/", "_");
					transPath = String.format("%s/%s", constant.ACCESS_EXTERNAL_RESOURCE_ACTION_URL, transPath);
					return transPath;
				}
			}
		}
		return "";
	}
	
	/**
	 * <pre>파일을 지정한 경로에 압축처리한다.</pre>
	 * @param targetFile 압축대상파일
	 * @param zipfile 압축파일명
	 * @return
	 * @see
	 */
	public boolean zip(final File targetFile, final String zipfile) {
		boolean result = false;
		FileOutputStream fos = null;
		ZipOutputStream zos = null;
		FileInputStream fis = null;
		
		try {
			fos = new FileOutputStream(zipfile);
			zos = new ZipOutputStream(fos);
			fis = new FileInputStream(targetFile);
			zos.putNextEntry(new ZipEntry(targetFile.getName()));			
			
			byte[] buffer = new byte[1024];
			int cnt = 0;
			while( (cnt = fis.read(buffer, 0, 1024)) != -1 ) {
				zos.write(buffer, 0, cnt);
			}
			zos.closeEntry();
			result = true;
		} catch(Exception e) {
			log.printStackTracePH("zip", e);
		} finally {
			try {
				if( fis != null ) { fis.close(); }
			} catch (IOException e) {
				log.exStackTrace("zip", e);
			}
			
			try {
				if( zos != null ) { zos.close(); }
			} catch (IOException e) {
				log.exStackTrace("zip", e);
			}			
			
			try {
				if( fos != null ) { fos.close(); }
			} catch (IOException e) {
				log.exStackTrace("zip", e);
			}
		}
		
		return result;
	}
	
	/**
	 * <pre>ProxyFactoryBean 을 사용하여 target object 에 부가 기능을 추가 처리 할때 사용한다.</pre>
	 * @param target 대상 오브젝트
	 * @param advice 부가 기능을 구현한 오브젝트
	 * @param mappedPattern 부가 기능이 적용될 메소드 이름 지정( "*" 사용가능 ) 
	 * @return 부가 기능이 적용된 대상 오브젝트
	 * @see
	 */
	public Object getPFBobjec(Object target, MethodInterceptor advice, String[] mappedPattern) {
		ProxyFactoryBean proxyFactoryBean = new ProxyFactoryBean();
		proxyFactoryBean.setProxyTargetClass(true);	//CGLIB 사용해서 인터페이스 없는 오브젝트도 위임 처리 할수 있도록 설정
													//CGLIB 사용시 target object 는 public 생성자를 가져야 한다.
		NameMatchMethodPointcut pointcut = new NameMatchMethodPointcut();

		proxyFactoryBean.setTarget(target);
		pointcut.setMappedNames(mappedPattern);
		proxyFactoryBean.addAdvisor(new DefaultPointcutAdvisor(pointcut, advice));

		return proxyFactoryBean.getObject();
	}
}
