/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 */
package com.olleh.pHubCMS.common.components;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.olleh.pHubCMS.common.dao.CommonDAO;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.MenuActionInfoVO;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 메뉴별 uri 관리 콤포넌트
 * @Class Name : MenuActionInfo
 * @author lys
 * @since 2018.09.10
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.10   lys        최초생성
 * </pre>
 */
@Component
public class MenuActionInfo {
	private Logger log = new Logger(this.getClass());
	
	/**
	 * 메뉴Action 정보
	 */	
	private static List<MenuActionInfoVO> menuActionMap;
	
	@Autowired
	private CommonDAO commonDao;
	
	/**
	 * <pre>요청한 메뉴ID의 Action리스트를 리턴 한다. (전체)</pre>
     *
     * @param  String menuId
     * @return 
     * @see
	 */
	public List<MenuActionInfoVO> getMenuActionListById(String menuId) {
		return getId(menuId);
	}
	
	/**
	 * <pre>요청한 메뉴ID의 Action리스트를 리턴 한다. (USE_YN=Y, DP_YN=Y 인 건만)</pre>
     *
     * @param  String menuId
     * @return 
     * @see
	 */
	public List<MenuActionInfoVO> getMenuActionListYById(String menuId) {
		// declare
		List<MenuActionInfoVO> list = new ArrayList<MenuActionInfoVO>();
		List<MenuActionInfoVO> tmpList = getId(menuId);

		// use_yn(사용여부), dp_yn(출력여부)가 Y인 데이터만 모아서 리턴 한다.
		for(MenuActionInfoVO map : tmpList) {
			if( ("Y".equals(StringUtil.nvl(map.getUseYn(), "N"))) ) list.add(map);
		}
		
		return list;
	}
	
	/**
	 * <pre>요청한 uri의 ActionVO를 리턴 한다. (USE_YN=Y 인 건만)</pre>
     *
     * @param  String uri
     * @return 
     * @see
	 */	
	public MenuActionInfoVO getMenuActionVOYByUri(String uri) {
		// declare
		MenuActionInfoVO vo = getUri(uri);
		
		// null 체크  or
		// use_yn(사용여부)가 N
		if( (vo == null) || ("N".equals(StringUtil.nvl(vo.getUseYn(), "N"))) ) vo = new MenuActionInfoVO();
		
		return vo;
	}
	

	
	/**
	 * <pre>메뉴Action정보 조회</pre>
     *
     * @param  
     * @return 
     * @see
	 */
	private List<MenuActionInfoVO> getAllList() {
		log.debug("getAllList", "getAllList() Start!");
		// declare		
		List<MenuActionInfoVO> list = new ArrayList<MenuActionInfoVO>();	
		
		try {
			// 전체 공통코드 조회
			list = commonDao.getAllMenuActionInfo();
			
		} catch (Exception e) {
			log.printStackTracePH("getAllList", e);
		}
		return list;
	}

	/**
	 * <pre>menuActionMap load</pre>
     *
     * @param  
     * @return 
     * @see <pre>
     *  1. 메뉴Action정보 (ADM_MENU_INFO, ADM_ACTION_INFO)를 db에서 읽어서 menuActionMap에 저장 한다.
     *  </pre>
	 */	
	@PostConstruct
	public void start() {
		// 메모리 로드
		menuActionMap = getAllList();
	}
	
	/**
	 * <pre>reroad 설정</pre>
     *
     * @param  
     * @return 
     * @see
	 */
	public void reset() {
		menuActionMap = getAllList();
	}
	
	/**
	 * <pre>요청한 메뉴코드의 vo list를 리턴 한다.</pre>
     *
     * @param  String menuId
     * @return List<CmnCdVO>
     * @see
	 */	
	private List<MenuActionInfoVO> getId(String menuId) {
		List<MenuActionInfoVO> tmpList = menuActionMap;
		List<MenuActionInfoVO> list    = new ArrayList<MenuActionInfoVO>();
	
		try {
			// 메뉴ID가 동일한 vo를 list에 넣어서 리턴 한다.
			for(MenuActionInfoVO vo : tmpList) {
				
				if( menuId.equals( StringUtil.nvl(vo.getMenuId())) ) list.add(vo);
			}
		} catch (Exception e) {
			log.printStackTracePH("getId", e);
		}	
		
		return list;
	}
	
	/**
	 * <pre>요청한 uri의 vo를 리턴 한다.</pre>
     *
     * @param  String uri
     * @return List<CmnCdVO>
     * @see
	 */	
	private MenuActionInfoVO getUri(String uri) {
		List<MenuActionInfoVO> tmpList = menuActionMap;
		MenuActionInfoVO rtnVo         = new MenuActionInfoVO();
	
		try {
			// uri가 동일한 vo를 찾으면 종료
			for(MenuActionInfoVO vo : tmpList) {
				
				if( uri.equals( StringUtil.nvl(vo.getUri())) ) {
					rtnVo = vo;
					break;
				}
			}
		} catch (Exception e) {
			log.printStackTracePH("getUri", e);
		}	
		
		return rtnVo;
	}	
}