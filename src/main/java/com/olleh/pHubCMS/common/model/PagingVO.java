/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 * Revision History
 * Author            Date              Description
 * ------            -----             ------------
 * lys               2018.06.28        페이징 VO 
 */
package com.olleh.pHubCMS.common.model;

/**
 * 페이징처리 vo
 * @Class Name : PagingVO
 * @author lys
 * @since 2018.07.16
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
public class PagingVO {
	/**
	 * 페이지 번호
	 */
	private String PAGE_NUMBER;
	/**
	 * 페이지 당 목록 개수
	 */
	private String PAGE_ROWS;
	/**
	 * 페이지 노출 개수
	 */
	private String PAGE_COUNT;
	/**
	 * 전체 목록 개수
	 */
	private String TOTAL_COUNT;
	
	
	public String getPAGE_NUMBER() {
		return PAGE_NUMBER;
	}
	public void setPAGE_NUMBER(String pAGE_NUMBER) {
		PAGE_NUMBER = pAGE_NUMBER;
	}
	public String getPAGE_ROWS() {
		return PAGE_ROWS;
	}
	public void setPAGE_ROWS(String pAGE_ROWS) {
		PAGE_ROWS = pAGE_ROWS;
	}
	public String getPAGE_COUNT() {
		return PAGE_COUNT;
	}
	public void setPAGE_COUNT(String pAGE_COUNT) {
		PAGE_COUNT = pAGE_COUNT;
	}
	public String getTOTAL_COUNT() {
		return TOTAL_COUNT;
	}
	public void setTOTAL_COUNT(String tOTAL_COUNT) {
		TOTAL_COUNT = tOTAL_COUNT;
	}
	
	public PagingVO() {
		PAGE_NUMBER = "1";	// 시작 페이지 : 1
		PAGE_ROWS   = "20";
		PAGE_COUNT  = "5";
	}
	
}
