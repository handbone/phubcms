/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pHubCMS.common.model;

import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 에러정보 VO
 * (1)User정의Exception 에서 생성해서 멤버변수로 처리함
 * (2)에러처리에 필요한 내용을 관리하므로 필요한 내용을 추가해서 관리.
 * @Class Name : ErrorInfoVO
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
public class ErrorInfoVO {
    
	/** 에러 발생 클래스 */
	private String className = "";
	/** 에러 발생 메소드 */
	private String methodName = "";
	/** 에러 코드 */
	private String stepCd = "";
	/** 에러 코드 */
	private String errCd = "";
	/** 에러 메세지 */
	private String errMsg = "";	
	/** 에러 발생 시간(6자리 문자열) */
	private String dateTime = "";
	/** 객체 */
	private Object paramObj = null;
	/** 에러 발생 원본 예외 */
	private Exception originException = null;
			
	/**
	 * 생성자
	 */
	public ErrorInfoVO() {		
	}
	
	/**
	 * 에러 발생 클래스를 취득한다
	 * @return String 에러 발생 클래스
	 */
	public String getClassName (){
		return className;
	}
	
	/**
	 * 에러 발생 클래스를 설정한다.
	 * @param str 에러 발생 클래스
	 */
	public void setClassName (String str){
		className = StringUtil.nvl(str);
	}
	
	/**
	 * 에러발생메소드를 취득한다
	 * @return String 에러발생메소드
	 */
	public String getMethodName (){
		return methodName;
	}
	
	/**
	 * 에러발생메소드를 설정한다.
	 * @param str 에러발생메소드
	 */
	public void setMethodName (String str){
		methodName = StringUtil.nvl(str);
	}
	
	/**
	 * 스텝코드를 취득한다
	 * @return String 스텝코드
	 */	
	public String getStepCd() {
		return stepCd;
	}
	/**
	 * 스텝코드를 설정한다.
	 * @param str 스텝코드
	 */
	public void setStepCd(String stepCd) {
		this.stepCd = stepCd;
	}

	/**
	 * 에러코드를 취득한다
	 * @return String 에러코드
	 */
	public String getErrCd (){
		return errCd;
	}
	
	/**
	 * 에러코드를 설정한다.
	 * @param str 에러코드
	 */
	public void setErrCd (String str){
		errCd = StringUtil.nvl(str);
	}
	
	/**
	 * 에러메세지를 취득한다
	 * @return String 에러메세지
	 */
	public String getErrMsg (){
		return errMsg;
	}
	
	/**
	 * 에러메세지를 설정한다.
	 * @param str 에러메세지
	 */
	public void setErrMsg (String str){
		errMsg = StringUtil.nvl(str);
	}
		
	/**
	 * 에러 발생 날짜를 취득한다
	 * @return String 에러 발생 날짜
	 */
	public String getDateTime (){
		return dateTime;
	}
	
	/**
	 * 에러 발생 날짜를 설정한다.
	 * @param str 에러 발생 날짜
	 */
	public void setDateTime (String str){
		dateTime = StringUtil.nvl(str);
	}	
	
	/**
	 * 파라미터OBJ  취득
	 * @return Object 파라미터OBJ
	 */
	public Object getParamObj() {
		return paramObj;
	}	
	
	/**
	 * 파라미터OBJ를 설정한다.
	 * @param obj 파라미터OBJ
	 */
	public void setParamObj (Object obj){
		paramObj = obj;
	}	
	
	/**
	 * 에러 발생 원본 예외을 취득한다 <br>
	 * (받는곳에서 instanceof를 조사해서 처리한다)
	 * @return Exception 에러 발생 원본 예외
	 */
	public Exception getOriginException (){
		return originException;
	}
	
	/**
	 * 에러 발생 원본 예외를 설정한다.
	 * @param Exception 에러 발생 원본 예외
	 */
	public void setOriginException (Exception except){
		originException = except;
	}	
}