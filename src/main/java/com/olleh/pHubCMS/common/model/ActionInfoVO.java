/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pHubCMS.common.model;

/**
 * 메뉴 Action정보 vo
 * @Class Name : ActionInfoVO
 * @author lys
 * @since 2018.07.16
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
public class ActionInfoVO {
	
	private String menuId;		// 메뉴ID
	private String uri;			// URI
	private String actnNm;		// Action명
	private String sendPlc;		// 발신처
	private String rcvPlc;		// 수신처
	private String uriMethod;	// URI METHOD
	
	
	public String getMenuId() {
		return menuId;
	}
	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public String getActnNm() {
		return actnNm;
	}
	public void setActnNm(String actnNm) {
		this.actnNm = actnNm;
	}
	public String getSendPlc() {
		return sendPlc;
	}
	public void setSendPlc(String sendPlc) {
		this.sendPlc = sendPlc;
	}
	public String getRcvPlc() {
		return rcvPlc;
	}
	public void setRcvPlc(String rcvPlc) {
		this.rcvPlc = rcvPlc;
	}
	public String getUriMethod() {
		return uriMethod;
	}
	public void setUriMethod(String uriMethod) {
		this.uriMethod = uriMethod;
	}
}
