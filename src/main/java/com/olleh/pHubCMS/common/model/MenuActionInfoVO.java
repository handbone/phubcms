/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pHubCMS.common.model;

/**
 * 메뉴별uri VO
 * @Class Name : MenuActionInfoVO
 * @author lys
 * @since 2018.09.10
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.10   lys        최초생성
 * </pre>
 */
public class MenuActionInfoVO {
	private String menuId;			// 메뉴ID
	private String menuNm;			// 메뉴명
	private String menuInd;			// 메뉴구분(CMS)
	private String menuUri;			// 메뉴 메인uri
	private String uprMenuId;		// 상위메뉴ID
	private String useYn;			// 사용여부 (Y/N)
	private String dpYn;			// 출력여부 (Y/N)
	private String uri;				// uri
	private String actnNm;			// Action명
	private String uriMethod; 		// URI구분(SRCH	조회, RGST	등록/수정, DEL	삭제, EXCD	엑셀다운로드)	
	private String logWriteYn;		// 사용로그 기록여부 (Y/N)
	
	
	public String getMenuId() {
		return menuId;
	}
	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}
	public String getMenuNm() {
		return menuNm;
	}
	public void setMenuNm(String menuNm) {
		this.menuNm = menuNm;
	}
	public String getMenuInd() {
		return menuInd;
	}
	public void setMenuInd(String menuInd) {
		this.menuInd = menuInd;
	}
	public String getMenuUri() {
		return menuUri;
	}
	public void setMenuUri(String menuUri) {
		this.menuUri = menuUri;
	}
	public String getUprMenuId() {
		return uprMenuId;
	}
	public void setUprMenuId(String uprMenuId) {
		this.uprMenuId = uprMenuId;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getDpYn() {
		return dpYn;
	}
	public void setDpYn(String dpYn) {
		this.dpYn = dpYn;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public String getActnNm() {
		return actnNm;
	}
	public void setActnNm(String actnNm) {
		this.actnNm = actnNm;
	}
	public String getUriMethod() {
		return uriMethod;
	}
	public void setUriMethod(String uriMethod) {
		this.uriMethod = uriMethod;
	}
	public String getLogWriteYn() {
		return logWriteYn;
	}
	public void setLogWriteYn(String logWriteYn) {
		this.logWriteYn = logWriteYn;
	}
}
