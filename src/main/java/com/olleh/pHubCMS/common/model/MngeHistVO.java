/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 * Revision History
 * Author            Date              Description
 * ------            -----             ------------
 * lys               2018.06.28        이력테이블 VO 
 */
package com.olleh.pHubCMS.common.model;

/**
 * 관리이력 VO
 * @Class Name : MngeHistVO
 * @author lys
 * @since 2018.09.10
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.10   lys        최초생성
 * </pre>
 */
public class MngeHistVO extends PagingVO {
	
	private String OCC_DTM;				// 발생일시
	private String SNO;					// 일련번호
	private String FRST_REGS_DTM;		// 최초등록일시
	private String FRST_REGS_USR_NO;	// 최초등록사용자번호
	private String LAST_REGS_DTM;		// 최종등록일시
	private String LAST_REGS_USR_NO;	// 최종등록사용자번호
	private String TRGT_TBL_NM;			// 대상테이블명
	private String DB_CMD_DV_CD;		// DB명령구분코드
	private String BAS_KY_CTNT;			// 기본키내용
	private String CHNG_CTNT;			// 변경내용
	
	private String TRGT_TBL;			// 대상테이블
	private String TRGT_TBL_ALL_YN;		// 대상테이블 전체여부
	private String LAST_REGS_STR_DT;	// 최종등록시작일자
	private String LAST_REGS_END_DT;	// 최종등록종료일자
	
	
	public String getOCC_DTM() {
		return OCC_DTM;
	}
	public void setOCC_DTM(String oCC_DTM) {
		OCC_DTM = oCC_DTM;
	}
	public String getSNO() {
		return SNO;
	}
	public void setSNO(String sNO) {
		SNO = sNO;
	}
	public String getFRST_REGS_DTM() {
		return FRST_REGS_DTM;
	}
	public void setFRST_REGS_DTM(String fRST_REGS_DTM) {
		FRST_REGS_DTM = fRST_REGS_DTM;
	}
	public String getFRST_REGS_USR_NO() {
		return FRST_REGS_USR_NO;
	}
	public void setFRST_REGS_USR_NO(String fRST_REGS_USR_NO) {
		FRST_REGS_USR_NO = fRST_REGS_USR_NO;
	}
	public String getLAST_REGS_DTM() {
		return LAST_REGS_DTM;
	}
	public void setLAST_REGS_DTM(String lAST_REGS_DTM) {
		LAST_REGS_DTM = lAST_REGS_DTM;
	}
	public String getLAST_REGS_USR_NO() {
		return LAST_REGS_USR_NO;
	}
	public void setLAST_REGS_USR_NO(String lAST_REGS_USR_NO) {
		LAST_REGS_USR_NO = lAST_REGS_USR_NO;
	}
	public String getTRGT_TBL_NM() {
		return TRGT_TBL_NM;
	}
	public void setTRGT_TBL_NM(String tRGT_TBL_NM) {
		TRGT_TBL_NM = tRGT_TBL_NM;
	}
	public String getDB_CMD_DV_CD() {
		return DB_CMD_DV_CD;
	}
	public void setDB_CMD_DV_CD(String dB_CMD_DV_CD) {
		DB_CMD_DV_CD = dB_CMD_DV_CD;
	}
	public String getBAS_KY_CTNT() {
		return BAS_KY_CTNT;
	}
	public void setBAS_KY_CTNT(String bAS_KY_CTNT) {
		BAS_KY_CTNT = bAS_KY_CTNT;
	}
	public String getCHNG_CTNT() {
		return CHNG_CTNT;
	}
	public void setCHNG_CTNT(String cHNG_CTNT) {
		CHNG_CTNT = cHNG_CTNT;
	}
	public String getTRGT_TBL() {
		return TRGT_TBL;
	}
	public void setTRGT_TBL(String tRGT_TBL) {
		TRGT_TBL = tRGT_TBL;
	}
	public String getTRGT_TBL_ALL_YN() {
		return TRGT_TBL_ALL_YN;
	}
	public void setTRGT_TBL_ALL_YN(String tRGT_TBL_ALL_YN) {
		TRGT_TBL_ALL_YN = tRGT_TBL_ALL_YN;
	}
	public String getLAST_REGS_STR_DT() {
		return LAST_REGS_STR_DT;
	}
	public void setLAST_REGS_STR_DT(String lAST_REGS_STR_DT) {
		LAST_REGS_STR_DT = lAST_REGS_STR_DT;
	}
	public String getLAST_REGS_END_DT() {
		return LAST_REGS_END_DT;
	}
	public void setLAST_REGS_END_DT(String lAST_REGS_END_DT) {
		LAST_REGS_END_DT = lAST_REGS_END_DT;
	}
	
	public MngeHistVO() {
		/**
		 * 대상테이블 전체 여부
		 *  - Y : 전체 테이블
		 *  - N : 백업용 테이블 제외
		 */
		TRGT_TBL_ALL_YN = "N";
	}
	
}
