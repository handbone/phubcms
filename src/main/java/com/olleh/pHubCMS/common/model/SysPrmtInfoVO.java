/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pHubCMS.common.model;

/**
 * 시스템파라미터 vo
 * @Class Name : SysPrmtInfoVO
 * @author lys
 * @since 2018.07.16
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
public class SysPrmtInfoVO {
	private String prmtCd;		// 파라미터코드
	private String useInd;		// 파라미터그룹코드
	private String prmtGrpCd;	// 용도구분
	private String prmtVal;		// 파라미터값
	private String useYn;		// 사용여부
	private String rfrnVal1;	// 참조값1
	private String rfrnVal2;	// 참조값2
	private String rfrnVal3;	// 참조값3
	
	public String getPrmtCd() {
		return prmtCd;
	}
	public void setPrmtCd(String prmtCd) {
		this.prmtCd = prmtCd;
	}
	public String getUseInd() {
		return useInd;
	}
	public void setUseInd(String useInd) {
		this.useInd = useInd;
	}
	public String getPrmtGrpCd() {
		return prmtGrpCd;
	}
	public void setPrmtGrpCd(String prmtGrpCd) {
		this.prmtGrpCd = prmtGrpCd;
	}
	public String getPrmtVal() {
		return prmtVal;
	}
	public void setPrmtVal(String prmtVal) {
		this.prmtVal = prmtVal;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getRfrnVal1() {
		return rfrnVal1;
	}
	public void setRfrnVal1(String rfrnVal1) {
		this.rfrnVal1 = rfrnVal1;
	}
	public String getRfrnVal2() {
		return rfrnVal2;
	}
	public void setRfrnVal2(String rfrnVal2) {
		this.rfrnVal2 = rfrnVal2;
	}
	public String getRfrnVal3() {
		return rfrnVal3;
	}
	public void setRfrnVal3(String rfrnVal3) {
		this.rfrnVal3 = rfrnVal3;
	}
}
