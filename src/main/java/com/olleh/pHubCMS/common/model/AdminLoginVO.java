/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 * Revision History
 * Author            Date              Description
 * ------            -----             ------------
 * lys               2018.06.28        관리자 로그인 VO 
 */
package com.olleh.pHubCMS.common.model;

import java.io.Serializable;


/**
 * 사용자로그인정보 vo
 * @Class Name : AdminLoginVO
 * @author lys
 * @since 2018.07.16
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
public class AdminLoginVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5748388869298375132L;
	
	/**
	 * 사원번호
	 */
	private String userId;
	/**
	 * 사원명
	 */
	private String userNm;
	/**
	 * 부서코드
	 */
	private String deptCd;

	/**
	 * 부서명
	 */
	private String deptNm;
	
	/**
	 * 상태
	 */
	private String statCd;	
	
	/**
	 * 최근접속일
	 */
	private String rcntJnngDd;
	
	/**
	 * 로그인 여부 구분값 ("Y":로그인)
	 */
	private String loginYn;
	
	/**
	 * 로그인 ip
	 */
	private String loginIp;

	/**
	 * 초기페이지
	 */
	private String welcomUri;	

	
	/**
	 * getter / setter
	 */
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserNm() {
		return userNm;
	}

	public void setUserNm(String userNm) {
		this.userNm = userNm;
	}

	public String getDeptCd() {
		return deptCd;
	}

	public void setDeptCd(String deptCd) {
		this.deptCd = deptCd;
	}

	public String getDeptNm() {
		return deptNm;
	}

	public void setDeptNm(String deptNm) {
		this.deptNm = deptNm;
	}

	public String getStatCd() {
		return statCd;
	}

	public void setStatCd(String statCd) {
		this.statCd = statCd;
	}

	public String getRcntJnngDd() {
		return rcntJnngDd;
	}

	public void setRcntJnngDd(String rcntJnngDd) {
		this.rcntJnngDd = rcntJnngDd;
	}

	public String getLoginYn() {
		return loginYn;
	}

	public void setLoginYn(String loginYn) {
		this.loginYn = loginYn;
	}

	public String getLoginIp() {
		return loginIp;
	}

	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}

	public String getWelcomUri() {
		return welcomUri;
	}

	public void setWelcomUri(String welcomUri) {
		this.welcomUri = welcomUri;
	}
}
