/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pHubCMS.common.dao;

import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 공통적인 DB 처리 기본로직을 정의한다.
 * @Class Name : AbstractDAO
 * @author lys
 * @since 2018.06.28
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.06.28   lys        최초생성
 * </pre>
 */
public class AbstractDAO {
	// private org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(this.getClass());
    
	@Autowired
	public SqlSessionTemplate sqlSession;
	
	public String KEY_SEPARATOR = "_";
	public String VAL_SEPARATOR = "_";
	
	
	/**
	 * <pre>DB insert</pre>
     *
     * @param  String queryId
     * @param  Object params
     * @return int
     * @see
	 */	
    public int insert(String queryId, Object params){
        return sqlSession.insert(queryId, params);
    }

	/**
	 * <pre>DB update</pre>
     *
     * @param  String queryId
     * @param  Object params
     * @return int
     * @see
	 */	    
    public int update(String queryId, Object params){
        return sqlSession.update(queryId, params);
    }
    
	/**
	 * <pre>DB delete</pre>
     *
     * @param  String queryId
     * @param  Object params
     * @return int
     * @see
	 */	    
    public int delete(String queryId, Object params){
        return sqlSession.delete(queryId, params);
    }
    
	/**
	 * <pre>DB select one</pre>
     *
     * @param  String queryId
     * @return Map
     * @see
	 */	    
    public Map selectOne(String queryId){
        return sqlSession.selectOne(queryId);
    }
    
	/**
	 * <pre>DB select one</pre>
     *
     * @param  String queryId
     * @return Map
     * @see
	 */	    
    public Map selectOne(String queryId, Object params){
        return sqlSession.selectOne(queryId, params);
    }
    
	/**
	 * <pre>DB select list</pre>
     *
     * @param  String queryId
     * @return List
     * @see
	 */	    
    public List selectList(String queryId){
        return sqlSession.selectList(queryId);
    }
    
	/**
	 * <pre>DB select list</pre>
     *
     * @param  String queryId
     * @param  Object params
     * @return List
     * @see
	 */	    
    public List selectList(String queryId, Object params){
        return sqlSession.selectList(queryId,params);
    }
    
	/**
	 * <pre>DB crud 이력 등록</pre>
     *
     * @param obj			VO 객체
     * @param usr_no		최초 / 최종 등록 사용자 번호
     * @param table_name	데이터 변경 대상 테이블 명
     * @param job_type		DB 명령구분코드	("INSERT" / "UPDATE" / "DELETE")
     * @param db_keys		PK (유니크 조건 되도록 값 구성 / 복수개 키는 구분자(';') 로 연결) 
     * @return int
     * @see
	 */	    
//	public int insert_MNGE_HIST(final Object obj, final String usr_no, final String table_name, final String job_type, final String db_keys) {
//		
//		String vo     = JsonUtil.toJson(obj).toString();
//		String change = "";
//		
//		MngeHistVO mngeHistVO = new MngeHistVO();
//		mngeHistVO.setFRST_REGS_USR_NO( usr_no ); // 최초등록사용자번호
//		mngeHistVO.setLAST_REGS_USR_NO( usr_no ); // 최종등록사용자번호
//		mngeHistVO.setTRGT_TBL_NM( table_name ); // 대상테이블명
//		mngeHistVO.setDB_CMD_DV_CD( job_type ); // DB명령구분코드
//		mngeHistVO.setBAS_KY_CTNT( db_keys ); // 기본키내용 ()
//		mngeHistVO.setCHNG_CTNT( change ); // 변경내용
////		log.debug("MngeHistVO : {}", CommonUtils.toJson(mngeHistVO));
////		return 0;
//		
//		final String queryId = "mybatis.admin.hist.insert_MNGE_HIST";
//		
//        return sqlSession.insert(queryId, mngeHistVO);
//	}
}