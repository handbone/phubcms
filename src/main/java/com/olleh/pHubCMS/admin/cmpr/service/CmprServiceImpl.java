package com.olleh.pHubCMS.admin.cmpr.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.olleh.pHubCMS.admin.cmpr.dao.CmprDao;
import com.olleh.pHubCMS.common.log.Logger;

/**
 * 대사 관리 Service
 * 
 * @Class Name : CmprService
 * @author ojh
 * @since 2018.08.22
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.22   ojh        최초생성
 * 
 */
@Service
public class CmprServiceImpl implements CmprService{
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	CmprDao cmprDao;
	
	@Override
	public List viewClipCmpr(Map<String, Object> params){
		return cmprDao.viewClipCmpr(params);
	}
	@Override
	public Map<String, Object> viewClipCmprTotal(Map<String, Object> params) {
		return cmprDao.viewClipCmprTotal(params);
	}
	@Override
	public List viewClipCmprExcel(Map<String, Object> params){
		return cmprDao.viewClipCmprExcel(params);
	}
	@Override
	@Transactional
	public int dealCancelReq(Map<String, Object> params){
		
		try{
			int result = 0;
			result = cmprDao.insertDealCancelReqInfo(params);
			if(result > 0){
				result = cmprDao.insertDealCancelReqDtl(params);
				if(result > 0){
					result = cmprDao.insertDealInfo(params);
					if(result > 0){
						result = cmprDao.insertDealDtl(params);
						if(result > 0){
							return cmprDao.updateOriDealInfo(params);
						} else{
							log.error("dealCancelReq", "[4] 거래 취소 오류");	
						}
					} else{
						log.error("dealCancelReq", "[3] 거래 취소 오류");	
					}					
				} else{
					log.error("dealCancelReq", "[2] 거래 취소 오류");	
				}	
			} else{
				log.error("dealCancelReq", "[1] 거래 취소 오류");	
			}			
		} catch (Exception e){
			log.error("dealCancelReq", "Exception : "+e.getMessage());		
		}
		return 0;
	}
	

}
