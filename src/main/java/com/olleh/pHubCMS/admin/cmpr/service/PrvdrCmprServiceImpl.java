package com.olleh.pHubCMS.admin.cmpr.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.cmpr.dao.PrvdrCmprDao;

/**
 * 포인트 제공처 수수료 정산 Service
 * 
 * @Class Name : PrvdrCmprService
 * @author ojh
 * @since 2018.10.17
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.17   ojh        최초생성
 * 
 */

@Service
public class PrvdrCmprServiceImpl implements PrvdrCmprService{

	@Autowired
	PrvdrCmprDao prvdrCmprDao;
	
	@Override
	public List viewPrvdrCmpr(Map<String, Object> params) {
		return prvdrCmprDao.viewPrvdrCmpr(params);
	}
	@Override
	public Map<String, Object> viewPrvdrCmprTotal(Map<String, Object> params) {
		return prvdrCmprDao.viewPrvdrCmprTotal(params);
	}
	@Override
	public List viewPrvdrCmprExcel(Map<String, Object> params) {
		return prvdrCmprDao.viewPrvdrCmprExcel(params);
	}
}
