package com.olleh.pHubCMS.admin.cmpr.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * PG사 수수료 정산조회 Dao
 * @Class Name : PgCmprDao
 * @author : ojh
 * @since : 2018.09.28
 * @version : 1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *      수정일                 수정자                수정내용
 *  ------------  -----------    -------------
 *  2018. 9. 28.      ojh          최초 생성
 * 
 */
@Repository
public class PgCmprDao extends AbstractDAO{
	
	public List viewPgCmpr(Map<String, Object> params) {
		return selectList("mybatis.cmpr.pgCmpr", params);
	}
	public Map<String, Object> viewPgCmprTotal(Map<String, Object> params) {
		return selectOne("mybatis.cmpr.pgCmprTotal", params);
	}
	public List viewPgCmprExcel(Map<String, Object> params) {
		return selectList("mybatis.cmpr.pgCmprExcel", params);
	}

}
