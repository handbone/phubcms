package com.olleh.pHubCMS.admin.cmpr.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.cmpr.service.ClipPntCmprHistService;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.utils.Paging;

/**
 * 클립포인트 일대사 내역 Controller
 * 
 * @Class Name : ClipPntCmprHistController
 * @author ojh
 * @since 2018.10.18
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.18   bmg        최초생성
 * 
 */
@Controller
public class ClipPntCmprHistController {
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	ClipPntCmprHistService clipPntCmprHistService;
	
	/**
	 * 클립포인트 일대사 내역 jsp 호출 및 초기값 설정
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/cmpr/viewClipPntCmprHist.do", method = RequestMethod.POST)
	public String viewClipPntCmprHist(Locale locale, Model model) {	
		String method = "viewClipPntCmprHist";
		log.debug(method, ">>> start");	
		
		log.debug(method, ">>>>>> end");	
		return "/cmpr/clipPntCmprHistView";
	}
	
	/**
	 * 클립포인트 일대사 내역 JQGrid 호출
	 * 
	 * @param params
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/cmpr/jgClipPntCmprHist.do", method = RequestMethod.POST)
	public Map<String, Object> jgClipPntCmprHist(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgClipPntCmprHist";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		int pageNum = Integer.parseInt(request.getParameter("page"));
		int limit = Integer.parseInt(request.getParameter("rows"));	
		log.debug(method,params.toString());
		
		count = clipPntCmprHistService.viewClipPntCmprHistTotal(params);
		int countRow = Integer.parseInt(count.get("cnt").toString());
		
		HashMap map = new HashMap();
		if (countRow > 0) {
			//HashMap에 페이징 정보 저장					
			Paging.setPageMap(params, map, pageNum, limit, countRow);	
			log.debug(method,map.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			log.debug(method,params.toString());
			resultList = clipPntCmprHistService.viewClipPntCmprHist(params);
			
			map.put("rows", resultList);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 클립포인트 일대사 내역  JQGrid 호출 엑셀
	 * 
	 * @param params
	 * @param req
	 * @param res
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/cmpr/jgClipPntCmprHistExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgClipPntCmprHistExcel(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgClipPntCmprHistExcel";
		log.debug(method, ">>> start");			
		HashMap map = new HashMap();
		try{
			//Request param
			log.debug(method,params.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			resultList = clipPntCmprHistService.viewClipPntCmprHistExcel(params);
			
			map.put("rows", resultList );
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * 클립포인트 전체 대사 내역 JQGrid 호출
	 * 
	 * @param params
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/cmpr/jgClipPntTtlCmprHist.do", method = RequestMethod.POST)
	public Map<String, Object> jgClipPntTtlCmprHist(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgClipPntTtlCmprHist";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		int pageNum = Integer.parseInt(request.getParameter("page"));
		int limit = Integer.parseInt(request.getParameter("rows"));	
		
		log.debug(method,params.toString());
		
		count = clipPntCmprHistService.clipPntTtlCmprHistTotal(params);
		int countRow = Integer.parseInt(count.get("cnt").toString());		 
		
		HashMap map = new HashMap();
		if (countRow > 0) {
			//HashMap에 페이징 정보 저장					
			Paging.setPageMap(params, map, pageNum, limit, countRow);	
			log.debug(method,map.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			log.debug(method,params.toString());
			resultList = clipPntCmprHistService.clipPntTtlCmprHist(params);
			
			map.put("rows", resultList);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 클립포인트 전체 대사 내역 JQGrid 호출 엑셀
	 * 
	 * @param params
	 * @param req
	 * @param res
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/cmpr/jgClipPntTtlCmprHistExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgClipPntTtlCmprHistExcel(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgClipPntTtlCmprHistExcel";
		log.debug(method, ">>> start");			
		HashMap map = new HashMap();
		try{
			//Request param
			log.debug(method,params.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			resultList = clipPntCmprHistService.clipPntTtlCmprHistExcel(params);
			
			map.put("rows", resultList );
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * 클립포인트 일치 내역 JQGrid 호출
	 * 
	 * @param params
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/cmpr/jgClipPntSucsHist.do", method = RequestMethod.POST)
	public Map<String, Object> jgClipPntSucsHist(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgClipPntSucsHist";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		int pageNum = Integer.parseInt(request.getParameter("page"));
		int limit = Integer.parseInt(request.getParameter("rows"));	
		log.debug(method,params.toString());
		
		count = clipPntCmprHistService.clipPntSucsHistTotal(params);
		int countRow = Integer.parseInt(count.get("cnt").toString());
		
		HashMap map = new HashMap();
		if (countRow > 0) {
			//HashMap에 페이징 정보 저장					
			Paging.setPageMap(params, map, pageNum, limit, countRow);	
			log.debug(method,map.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			log.debug(method,params.toString());
			resultList = clipPntCmprHistService.clipPntSucsHist(params);
			
			map.put("rows", resultList);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 클립포인트 일치 내역 JQGrid 호출 엑셀
	 * 
	 * @param params
	 * @param req
	 * @param res
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/cmpr/jgClipPntSucsHistExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgClipPntSucsHistExcel(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgClipPntSucsHistExcel";
		log.debug(method, ">>> start");			
		HashMap map = new HashMap();
		try{
			//Request param
			log.debug(method,params.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			resultList = clipPntCmprHistService.clipPntSucsHistExcel(params);
			
			map.put("rows", resultList );
		} catch (Exception e) {
			log.error(method,"Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * 클립포인트 불일치 내역 JQGrid 호출
	 * 
	 * @param params
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/cmpr/jgClipPntFailHist.do", method = RequestMethod.POST)
	public Map<String, Object> jgClipPntFailHist(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgClipPntFailHist";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		int pageNum = Integer.parseInt(request.getParameter("page"));
		int limit = Integer.parseInt(request.getParameter("rows"));	
		log.debug(method,params.toString());
		
		count = clipPntCmprHistService.clipPntFailHistTotal(params);
		int countRow = Integer.parseInt(count.get("cnt").toString());
		
		HashMap map = new HashMap();
		if (countRow > 0) {
			//HashMap에 페이징 정보 저장					
			Paging.setPageMap(params, map, pageNum, limit, countRow);	
			log.debug(method,map.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			log.debug(method,params.toString());
			resultList = clipPntCmprHistService.clipPntFailHist(params);
			
			map.put("rows", resultList);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 클립포인트 불일치 내역 JQGrid 호출 엑셀
	 * 
	 * @param params
	 * @param req
	 * @param res
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/cmpr/jgClipPntFailHistExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgClipPntFailHistExcel(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgClipPntFailHistExcel";
		log.debug(method, ">>> start");			
		HashMap map = new HashMap();
		try{
			//Request param
			log.debug(method,params.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			resultList = clipPntCmprHistService.clipPntFailHistExcel(params);
			
			map.put("rows", resultList );
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * 클립포인트 대사파일 내역 JQGrid 호출
	 * 
	 * @param params
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/cmpr/jgClipPntCmprFileHist.do", method = RequestMethod.POST)
	public Map<String, Object> jgClipPntCmprFileHist(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgClipPntCmprFileHist";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		int pageNum = Integer.parseInt(request.getParameter("page"));
		int limit = Integer.parseInt(request.getParameter("rows"));	
		log.debug(method,params.toString());
		
		count = clipPntCmprHistService.clipPntCmprFileHistTotal(params);
		int countRow = Integer.parseInt(count.get("cnt").toString());
		
		HashMap map = new HashMap();
		if (countRow > 0) {
			//HashMap에 페이징 정보 저장					
			Paging.setPageMap(params, map, pageNum, limit, countRow);	
			log.debug(method,map.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			log.debug(method,params.toString());
			resultList = clipPntCmprHistService.clipPntCmprFileHist(params);
			
			map.put("rows", resultList);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 클립포인트 대사파일 내역 JQGrid 호출 엑셀
	 * 
	 * @param params
	 * @param req
	 * @param res
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/cmpr/jgClipPntCmprFileHistExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgClipPntCmprFileHistExcel(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgClipPntCmprFileHistExcel";
		log.debug(method, ">>> start");			
		HashMap map = new HashMap();
		try{
			//Request param
			log.debug(method,params.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			resultList = clipPntCmprHistService.clipPntCmprFileHistExcel(params);
			
			map.put("rows", resultList );
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
}
