package com.olleh.pHubCMS.admin.cmpr.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * 포인트 제공처 수수료 정산 Dao
 * @Class Name : PrvdrCmprDao
 * @author : ojh
 * @since : 2018.10.17
 * @version : 1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *      수정일                 수정자                수정내용
 *  ------------  -----------    -------------
 *  2018. 10. 17.      ojh          최초 생성
 * 
 */

@Repository
public class PrvdrCmprDao extends AbstractDAO{
	
	public List viewPrvdrCmpr(Map<String, Object> params) {
		return selectList("mybatis.cmpr.prvdrCmpr", params);
	}
	public Map<String, Object> viewPrvdrCmprTotal(Map<String, Object> params) {
		return selectOne("mybatis.cmpr.prvdrCmprTotal", params);
	}
	public List viewPrvdrCmprExcel(Map<String, Object> params) {
		return selectList("mybatis.cmpr.prvdrCmprExcel", params);
	}	

}
