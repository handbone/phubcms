package com.olleh.pHubCMS.admin.cmpr.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.cmpn.service.PntPgCmpnServiceImpl;
import com.olleh.pHubCMS.admin.cmpn.service.PntUseCmpnServiceImpl;
import com.olleh.pHubCMS.admin.cmpr.service.PgCmprService;
import com.olleh.pHubCMS.common.components.CodeManage;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.CmnCdVO;
import com.olleh.pHubCMS.common.utils.Paging;

/**
 * PG사 수수료 정산조회 Controller
 * 
 * @Class Name : PgCmprController
 * @author ojh
 * @since 2018.09.28
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.28   ojh        최초생성
 * 
 */

@Controller
public class PgCmprController {
	private Logger log = new Logger(this.getClass());
	@Autowired
	CodeManage codeManage;	
	
	@Autowired
	PntPgCmpnServiceImpl pntPgCmpnService;
	
	@Autowired
	PgCmprService pgCmprService;
	
	
	/**
	 * PG사 수수료 정산조회
	 * jsp 호출 및 초기값 설정
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/cmpr/viewPgCmpr.do", method = RequestMethod.POST)
	public String viewClipCmpr(Locale locale, Model model) {	
		String method = "viewPgCmpr";
		log.debug(method, ">>> start");	
		
		//사용 'Y' 등록된 거래 구분 가져오기 
		List<CmnCdVO> indList = new ArrayList<CmnCdVO>();
		indList = codeManage.getCodeListY("DEAL_IND");
		model.addAttribute("DEAL_IND", indList);
		
		//PG사 가져오기
		List pgList = new ArrayList();
		pgList = pntPgCmpnService.getAllPgCmpn();
		model.addAttribute("PG_CMPN", pgList);
		
		// 서비스 ID 가져오기
		List svcCdList = codeManage.getCodeListY("PH_SVC_CD");
		model.addAttribute("PH_SVC_CD", svcCdList);

		log.debug(method, ">>>>>> end");	
		return "/cmpr/pgCmprView";
	}
	
	/**
	 * PG사 수수료 정산 조회
	 * JQGrid 호출
	 * 
	 * @param PG_CMPN_ID 	Select PG사명
	 * @param DEAL_IND 		Select 거래구분
	 * @param START_DATE 선택한 달력 값
	 * @param END_DATE 선택한 달력 값
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/cmpr/jgPgCmpr.do", method = RequestMethod.POST)
	public Map<String, Object> jgPgCmpr(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "jgPgCmpr";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		Map<String, Object> count = new HashMap<String, Object>();
		try{
			//Request param		
			int pageNum = Integer.parseInt(req.getParameter("page"));
			int limit = Integer.parseInt(req.getParameter("rows"));
			log.debug(method, params.toString());
			
			//총 Row 수 계산 		
			count = pgCmprService.viewPgCmprTotal(params);
			int countRow = Integer.parseInt(count.get("cnt").toString());			
			
			log.debug(method,"count : "+countRow);
			if(countRow > 0){
				//HashMap에 페이징 정보 저장					
				Paging.setPageMap(params, map, pageNum, limit, countRow);	
				log.debug(method,map.toString());
				
				//검색 결과 데이터 map 에 추가
				List resultList = new ArrayList();
				log.debug(method,params.toString());
				resultList = pgCmprService.viewPgCmpr(params);
				map.put("rows", resultList );
			}
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}
		
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * PG사 수수료 정산 조회
	 * JQGrid 호출 (엑셀용 데이터)
	 * 
	 * @param PG_CMPN_ID 	Select PG사명
	 * @param DEAL_IND 		Select 거래구분	
	 * @param START_DATE 	선택한 달력 값
	 * @param END_DATE 		선택한 달력 값
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/cmpr/jgPgCmprExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgPgCmprExcel(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "jgPgCmprExcel";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		Map<String, Object> count = new HashMap<String, Object>();
		try{
			//Request param		
			log.debug(method, params.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			resultList = pgCmprService.viewPgCmprExcel(params);
			map.put("rows", resultList );
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}
		
		log.debug(method, ">>>>>> end");	
		return map;
	}

}
