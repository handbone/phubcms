package com.olleh.pHubCMS.admin.cmpr.service;

import java.util.List;
import java.util.Map;

public interface PgCmprService {
	
	public List viewPgCmpr(Map<String, Object> params);
	public Map<String, Object> viewPgCmprTotal(Map<String, Object> params);
	public List viewPgCmprExcel(Map<String, Object> params);

}
