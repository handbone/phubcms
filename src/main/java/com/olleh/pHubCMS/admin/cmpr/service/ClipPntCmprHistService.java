package com.olleh.pHubCMS.admin.cmpr.service;

import java.util.List;
import java.util.Map;

public interface ClipPntCmprHistService {

	public Map<String, Object> viewClipPntCmprHistTotal(Map<String, Object> params);
	
	@SuppressWarnings("rawtypes")
	public List viewClipPntCmprHist(Map<String, Object> params);
	
	@SuppressWarnings("rawtypes")
	public List viewClipPntCmprHistExcel(Map<String, Object> params);
	
	public Map<String, Object> clipPntTtlCmprHistTotal(Map<String, Object> params);
	
	@SuppressWarnings("rawtypes")
	public List clipPntTtlCmprHist(Map<String, Object> params);
	
	@SuppressWarnings("rawtypes")
	public List clipPntTtlCmprHistExcel(Map<String, Object> params);
	
	public Map<String, Object> clipPntSucsHistTotal(Map<String, Object> params);
	
	@SuppressWarnings("rawtypes")
	public List clipPntSucsHist(Map<String, Object> params);
	
	@SuppressWarnings("rawtypes")
	public List clipPntSucsHistExcel(Map<String, Object> params);
	
	public Map<String, Object> clipPntFailHistTotal(Map<String, Object> params);
	
	@SuppressWarnings("rawtypes")
	public List clipPntFailHist(Map<String, Object> params);
	
	@SuppressWarnings("rawtypes")
	public List clipPntFailHistExcel(Map<String, Object> params);
	
	public Map<String, Object> clipPntCmprFileHistTotal(Map<String, Object> params);
	
	@SuppressWarnings("rawtypes")
	public List clipPntCmprFileHist(Map<String, Object> params);
	
	@SuppressWarnings("rawtypes")
	public List clipPntCmprFileHistExcel(Map<String, Object> params);
}
