package com.olleh.pHubCMS.admin.cmpr.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.cmpr.service.SettleBankCmprService;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.utils.Paging;

/**
 * 세틀뱅크 일대사 내역 Controller
 * 
 * @Class Name : SettleBankCmprController
 * @author ojh
 * @since 2018.11.13
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.11.13   bmg        최초생성
 * 
 */
@Controller
public class SettleBankCmprController {
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	SettleBankCmprService settleBankCmprService;
	
	/**
	 * 세틀뱅크 일대사 내역 jsp 호출 및 초기값 설정
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/cmpr/viewSettleBankCmpr.do", method = RequestMethod.POST)
	public String viewSettleBankCmpr(Locale locale, Model model) {	
		String method = "viewSettleBankCmpr";
		log.debug(method, ">>> start");	
		
		log.debug(method, ">>>>>> end");	
		return "/cmpr/settleBankCmprView";
	}
	
	/**
	 * 세틀뱅크 일대사 내역 JQGrid 호출
	 * 
	 * @param params
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/cmpr/jgSettleBankCmpr.do", method = RequestMethod.POST)
	public Map<String, Object> jgSettleBankCmpr(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgSettleBankCmpr";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		int pageNum = Integer.parseInt(request.getParameter("page"));
		int limit = Integer.parseInt(request.getParameter("rows"));	
		log.debug(method,params.toString());
		
		count = settleBankCmprService.settleBankCmprTotal(params);
		int countRow = Integer.parseInt(count.get("cnt").toString());
		
		HashMap map = new HashMap();
		if (countRow > 0) {
			//HashMap에 페이징 정보 저장					
			Paging.setPageMap(params, map, pageNum, limit, countRow);	
			log.debug(method,map.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			log.debug(method,params.toString());
			resultList = settleBankCmprService.settleBankCmpr(params);
			
			map.put("rows", resultList);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 세틀뱅크 일대사 내역 JQGrid 호출 엑셀
	 * 
	 * @param params
	 * @param req
	 * @param res
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/cmpr/jgSettleBankCmprExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgSettleBankCmprExcel(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgSettleBankCmprExcel";
		log.debug(method, ">>> start");			
		HashMap map = new HashMap();
		try{
			//Request param
			log.debug(method,params.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			resultList = settleBankCmprService.settleBankCmprExcel(params);
			
			map.put("rows", resultList );
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * 세틀뱅크 대사 파일 내역 팝업 JQGrid 호출
	 * 
	 * @param params
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/cmpr/jgSettleBankCmprFile.do", method = RequestMethod.POST)
	public Map<String, Object> jgSettleBankCmprFile(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgSettleBankCmprFile";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		int pageNum = Integer.parseInt(request.getParameter("page"));
		int limit = 10;
		log.debug(method,params.toString());
		
		count = settleBankCmprService.settleBankCmprFileTotal(params);
		int countRow = Integer.parseInt(count.get("cnt").toString());
		
		HashMap map = new HashMap();
		if (countRow > 0) {
			//HashMap에 페이징 정보 저장					
			Paging.setPageMap(params, map, pageNum, limit, countRow);	
			log.debug(method,map.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			log.debug(method,params.toString());
			resultList = settleBankCmprService.settleBankCmprFile(params);
			
			map.put("rows", resultList);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 세틀뱅크 대사 파일 내역 팝업 JQGrid 호출 엑셀
	 * 
	 * @param params
	 * @param req
	 * @param res
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/cmpr/jgSettleBankCmprFileExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgSettleBankCmprFileExcel(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgSettleBankCmprFileExcel";
		log.debug(method, ">>> start");			
		HashMap map = new HashMap();
		try{
			//Request param
			log.debug(method,params.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			resultList = settleBankCmprService.settleBankCmprFileExcel(params);
			
			map.put("rows", resultList );
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
}
