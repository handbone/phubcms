package com.olleh.pHubCMS.admin.cmpr.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * 대사 관리 Dao
 * @Class Name : CmprDao
 * @author : ojh
 * @since : 2018.08.24
 * @version : 1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *      수정일                 수정자                수정내용
 *  ------------  -----------    -------------
 *  2018. 8. 24.      ojh          최초 생성
 * 
 */

@Repository
public class CmprDao extends AbstractDAO{
	
	public List viewClipCmpr(Map<String, Object> params) {
		return selectList("mybatis.cmpr.clipCmpr", params);
	}
	public Map<String, Object> viewClipCmprTotal(Map<String, Object> params) {
		return selectOne("mybatis.cmpr.clipCmprTotal", params);
	}
	public List viewClipCmprExcel(Map<String, Object> params) {
		return selectList("mybatis.cmpr.clipCmprExcel", params);
	}
	
	public int insertDealCancelReqInfo(Map<String, Object> params) {
		return insert("mybatis.cmpr.insertDealCancelReqInfo", params);
	}
	public int insertDealCancelReqDtl(Map<String, Object> params) {
		return insert("mybatis.cmpr.insertDealCancelReqDtl", params);
	}
	public int insertDealInfo(Map<String, Object> params) {
		return insert("mybatis.cmpr.insertDealInfo", params);
	}
	public int insertDealDtl(Map<String, Object> params) {
		return insert("mybatis.cmpr.insertDealDtl", params);
	}
	public int updateOriDealInfo(Map<String, Object> params) {
		return update("mybatis.cmpr.updateOriDealInfo", params);
	}

}
