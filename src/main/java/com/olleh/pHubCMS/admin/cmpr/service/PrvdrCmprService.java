package com.olleh.pHubCMS.admin.cmpr.service;

import java.util.List;
import java.util.Map;

public interface PrvdrCmprService {
		
	public List viewPrvdrCmpr(Map<String, Object> params);
	public Map<String, Object> viewPrvdrCmprTotal(Map<String, Object> params);
	public List viewPrvdrCmprExcel(Map<String, Object> params);	
}
