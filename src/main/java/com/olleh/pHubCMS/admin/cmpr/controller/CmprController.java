package com.olleh.pHubCMS.admin.cmpr.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.cmpn.service.PntPrvdrServiceImpl;
import com.olleh.pHubCMS.admin.cmpr.service.CmprServiceImpl;
import com.olleh.pHubCMS.common.components.CodeManage;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.CmnCdVO;
import com.olleh.pHubCMS.common.utils.Paging;
import com.olleh.pHubCMS.common.utils.SessionUtils;
import com.olleh.pHubCMS.common.utils.StringUtil;



/**
 * 대사관리 Controller
 * 
 * @Class Name : CmprController
 * @author ojh
 * @since 2018.08.24
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.24   ojh        최초생성
 * 
 */


@Controller
public class CmprController {
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	CodeManage codeManage;
	
	@Autowired
	CmprServiceImpl cmprService;	
	
	/**
	 * 클립일대사 관리
	 * jsp 호출 및 초기값 설정
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/cmpr/viewClipCmpr.do", method = RequestMethod.POST)
	public String viewClipCmpr(Locale locale, Model model) {	
		String method = "viewClipCmpr";
		log.debug(method, ">>> start");	
		
		try{
			//사용 'Y' 등록된 거래 구분 가져오기 
			List<CmnCdVO> indList = new ArrayList<CmnCdVO>();
			indList = codeManage.getCodeListY("DEAL_IND");
			model.addAttribute("DEAL_IND", indList);
			
			// 서비스 ID 가져오기
			List svcCdList = codeManage.getCodeListY("PH_SVC_CD");
			model.addAttribute("PH_SVC_CD", svcCdList);
			
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}
		
		log.debug(method, ">>>>>> end");	
		return "/cmpr/clipCmprView";
	}
	

	
	/**
	 * 클립일대사 관리
	 * JQGrid 호출 
	 * 
	 * @param PHUB_TR_NO 	Input 입력한 포인트허브거래번호
	 * @param PNT_TR_NO 	Input 입력한 포인트별거래번호
	 * @param ERR_YN    	Select 선택한 오류여부 (Y:오류 N:정상)
	 * @param START_DATE 선택한 달력 값
	 * @param END_DATE 선택한 달력 값
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/cmpr/jgClipCmpr.do", method = RequestMethod.POST)
	public Map<String, Object> jgClipCmpr(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "jgClipCmpr";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		Map<String, Object> count = new HashMap<String, Object>();
		try{
			//Request param		
			int pageNum = Integer.parseInt(req.getParameter("page"));
			int limit = Integer.parseInt(req.getParameter("rows"));
			log.debug(method, params.toString());
			
			// 시작일자, 종료일자 replace
			String startDate = StringUtil.nvl(params.get("START_DATE")).replaceAll("-", "");
			params.put("START_DATE", startDate);
			String endDate = StringUtil.nvl(params.get("END_DATE")).replaceAll("-", "");
			params.put("END_DATE", endDate);
			
			//총 Row 수 계산 		
			count = cmprService.viewClipCmprTotal(params);	
			int countRow = Integer.parseInt(count.get("cnt").toString());			
			
			log.debug(method,"count : "+countRow);
			if(countRow > 0){
				//HashMap에 페이징 정보 저장					
				Paging.setPageMap(params, map, pageNum, limit, countRow);	
				log.debug(method,map.toString());
				
				//검색 결과 데이터 map 에 추가
				List resultList = new ArrayList();
				log.debug(method,params.toString());
				resultList = cmprService.viewClipCmpr(params);	
				
				map.put("rows", resultList );
			}
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}

		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * 클립일대사 관리
	 * JQGrid 호출 (엑셀용 데이터) 
	 * 
	 * @param PHUB_TR_NO 	Input 입력한 포인트허브거래번호
	 * @param PNT_TR_NO 	Input 입력한 포인트별거래번호
	 * @param ERR_YN    	Select 선택한 오류여부 (Y:오류 N:정상)
	 * @param START_DATE 선택한 달력 값
	 * @param END_DATE 선택한 달력 값
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/cmpr/jgClipCmprExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgClipCmprExcel(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "jgClipCmprExcel";
		log.debug(method, ">>> start");				
		HashMap map = new HashMap();
		try{
			//Request param		
			log.debug(method, params.toString());	
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			
			// 시작일자, 종료일자 replace
			String startDate = StringUtil.nvl(params.get("START_DATE")).replaceAll("-", "");
			params.put("START_DATE", startDate);
			String endDate = StringUtil.nvl(params.get("END_DATE")).replaceAll("-", "");
			params.put("END_DATE", endDate);
			
			resultList = cmprService.viewClipCmprExcel(params);	
							
			map.put("rows", resultList );
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}

		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * 클립일대사 관리
	 * 승인 취소
	 * 
	 * @param ORI_PHUB_TR_NO 	취소할 포인트허브거래번호
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/cmpr/dealCancelReq.do", method = RequestMethod.POST)
	public Map<String, Object> dealCancelReq(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "dealCancelReq";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		Map<String, Object> count = new HashMap<String, Object>();
		
		try {
			//현재 로그인한 세션의 아이디 추가
			params.put("RGST_USER_ID", SessionUtils.getUserId(req));
			//Request param		
			log.debug(method, params.toString());	
			
			if(cmprService.dealCancelReq(params) <= 0){
				log.error(method, "[5] 거래 취소 오류");
				map.put("eCode", "-1");
			} else{
				map.put("eCode", "0");
			}
			
		} catch (Exception e) {
			map.put("eCode", "-1");
			log.error(method, "Exception : "+e.getMessage());
		}
		
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
}
