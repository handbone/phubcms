package com.olleh.pHubCMS.admin.cmpr.service;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.cmpr.dao.SettleBankCmprDao;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 세틀뱅크 일대사 내역 Service
 * 
 * @Class Name : SettleBankCmprServiceImpl
 * @author bmg
 * @since 2018.11.13
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.11.13   bmg        최초생성
 */
@Service
public class SettleBankCmprServiceImpl implements SettleBankCmprService {

	@Autowired
	SettleBankCmprDao settleBankCmprDao;
	
	@Override
	public Map<String, Object> settleBankCmprTotal(Map<String, Object> params) {
		if (!StringUtils.isEmpty(StringUtil.nvl(params.get("TTL_CNCL_CNT")))) {
			params.put("TTL_CNCL_CNT", Integer.parseInt(params.get("TTL_CNCL_CNT").toString()));
		}
		params.put("START_DATE", StringUtil.nvl(params.get("START_DATE")).replaceAll("-", ""));
		params.put("END_DATE", StringUtil.nvl(params.get("END_DATE")).replaceAll("-", ""));
		return settleBankCmprDao.settleBankCmprTotal(params);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List settleBankCmpr(Map<String, Object> params) {
		if (!StringUtils.isEmpty(StringUtil.nvl(params.get("TTL_CNCL_CNT")))) {
			params.put("TTL_CNCL_CNT", Integer.parseInt(params.get("TTL_CNCL_CNT").toString()));
		}
		params.put("START_DATE", StringUtil.nvl(params.get("START_DATE")).replaceAll("-", ""));
		params.put("END_DATE", StringUtil.nvl(params.get("END_DATE")).replaceAll("-", ""));
		return settleBankCmprDao.settleBankCmpr(params);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List settleBankCmprExcel(Map<String, Object> params) {
		if (!StringUtils.isEmpty(StringUtil.nvl(params.get("TTL_CNCL_CNT")))) {
			params.put("TTL_CNCL_CNT", Integer.parseInt(params.get("TTL_CNCL_CNT").toString()));
		}
		params.put("START_DATE", StringUtil.nvl(params.get("START_DATE")).replaceAll("-", ""));
		params.put("END_DATE", StringUtil.nvl(params.get("END_DATE")).replaceAll("-", ""));
		return settleBankCmprDao.settleBankCmprExcel(params);
	}

	@Override
	public Map<String, Object> settleBankCmprFileTotal(Map<String, Object> params) {
		return settleBankCmprDao.settleBankCmprFileTotal(params);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List settleBankCmprFile(Map<String, Object> params) {
		return settleBankCmprDao.settleBankCmprFile(params);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List settleBankCmprFileExcel(Map<String, Object> params) {
		return settleBankCmprDao.settleBankCmprFileExcel(params);
	}
}
