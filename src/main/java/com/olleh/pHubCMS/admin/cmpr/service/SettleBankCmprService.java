package com.olleh.pHubCMS.admin.cmpr.service;

import java.util.List;
import java.util.Map;

@SuppressWarnings("rawtypes")
public interface SettleBankCmprService {

	public Map<String, Object> settleBankCmprTotal(Map<String, Object> params);
	
	public List settleBankCmpr(Map<String, Object> params);
	
	public List settleBankCmprExcel(Map<String, Object> params);
	
	public Map<String, Object> settleBankCmprFileTotal(Map<String, Object> params);
	
	public List settleBankCmprFile(Map<String, Object> params);
	
	public List settleBankCmprFileExcel(Map<String, Object> params);
}
