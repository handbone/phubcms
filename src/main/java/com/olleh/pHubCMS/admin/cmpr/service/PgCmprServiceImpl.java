package com.olleh.pHubCMS.admin.cmpr.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.cmpr.dao.PgCmprDao;
import com.olleh.pHubCMS.common.log.Logger;

/**
 * PG사 수수료 정산조회 Service
 * 
 * @Class Name : PgCmprService
 * @author ojh
 * @since 2018.09.28
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.28   ojh        최초생성
 * 
 */


@Service
public class PgCmprServiceImpl implements PgCmprService{
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	PgCmprDao pgCmprDao;
	
	@Override
	public List viewPgCmpr(Map<String, Object> params){
		return pgCmprDao.viewPgCmpr(params);		
	}
	
	@Override
	public Map<String, Object> viewPgCmprTotal(Map<String, Object> params){
		return pgCmprDao.viewPgCmprTotal(params);
	}
	
	@Override
	public List viewPgCmprExcel(Map<String, Object> params){
		return pgCmprDao.viewPgCmprExcel(params);
	}
	

}
