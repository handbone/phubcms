package com.olleh.pHubCMS.admin.cmpr.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * 클립포인트 일대사 내역 Dao
 * @Class Name : ClipPntCmprHistDao
 * @author : bmg
 * @since : 2018.10.18
 * @version : 1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *      수정일                 수정자                수정내용
 *  ------------  -----------    -------------
 *  2018.10.18      bmg          최초 생성
 * 
 */
@Repository
public class ClipPntCmprHistDao extends AbstractDAO {
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> viewClipPntCmprHistTotal(Map<String, Object> params) {
		return selectOne("mybatis.cmpr.clipPntCmprHistTotal", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List viewClipPntCmprHist(Map<String, Object> params) {
		return selectList("mybatis.cmpr.clipPntCmprHist", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List viewClipPntCmprHistExcel(Map<String, Object> params) {
		return selectList("mybatis.cmpr.clipPntCmprHistExcel", params);
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> clipPntTtlCmprHistTotal(Map<String, Object> params) {
		return selectOne("mybatis.cmpr.clipPntTtlCmprHistTotal", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List clipPntTtlCmprHist(Map<String, Object> params) {
		return selectList("mybatis.cmpr.clipPntTtlCmprHist", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List clipPntTtlCmprHistExcel(Map<String, Object> params) {
		return selectList("mybatis.cmpr.clipPntTtlCmprHistExcel", params);
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> clipPntSucsHistTotal(Map<String, Object> params) {
		return selectOne("mybatis.cmpr.clipPntSucsHistTotal", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List clipPntSucsHist(Map<String, Object> params) {
		return selectList("mybatis.cmpr.clipPntSucsHist", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List clipPntSucsHistExcel(Map<String, Object> params) {
		return selectList("mybatis.cmpr.clipPntSucsHistExcel", params);
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> clipPntFailHistTotal(Map<String, Object> params) {
		return selectOne("mybatis.cmpr.clipPntFailHistTotal", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List clipPntFailHist(Map<String, Object> params) {
		return selectList("mybatis.cmpr.clipPntFailHist", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List clipPntFailHistExcel(Map<String, Object> params) {
		return selectList("mybatis.cmpr.clipPntFailHistExcel", params);
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> clipPntCmprFileHistTotal(Map<String, Object> params) {
		return selectOne("mybatis.cmpr.clipPntCmprFileHistTotal", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List clipPntCmprFileHist(Map<String, Object> params) {
		return selectList("mybatis.cmpr.clipPntCmprFileHist", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List clipPntCmprFileHistExcel(Map<String, Object> params) {
		return selectList("mybatis.cmpr.clipPntCmprFileHistExcel", params);
	}
}
