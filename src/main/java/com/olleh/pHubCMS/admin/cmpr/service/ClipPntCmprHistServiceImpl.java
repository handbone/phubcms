package com.olleh.pHubCMS.admin.cmpr.service;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.cmpr.dao.ClipPntCmprHistDao;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 클립포인트 일대사 내역 Service
 * 
 * @Class Name : ClipPntCmprHistServiceImpl
 * @author bmg
 * @since 2018.10.18
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.18   bmg        최초생성
 */
@Service
public class ClipPntCmprHistServiceImpl implements ClipPntCmprHistService {
	
	@Autowired
	ClipPntCmprHistDao clipPntCmprHistDao;

	@Override
	public Map<String, Object> viewClipPntCmprHistTotal(Map<String, Object> params) {
		if (!StringUtils.isEmpty(StringUtil.nvl(params.get("NO_ACR_CNT")))) {
			params.put("NO_ACR_CNT", Integer.parseInt(params.get("NO_ACR_CNT").toString()));
		}
		params.put("START_DATE", StringUtil.nvl(params.get("START_DATE")).replaceAll("-", ""));
		params.put("END_DATE", StringUtil.nvl(params.get("END_DATE")).replaceAll("-", ""));
		return clipPntCmprHistDao.viewClipPntCmprHistTotal(params);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List viewClipPntCmprHist(Map<String, Object> params) {
		if (!StringUtils.isEmpty(StringUtil.nvl(params.get("NO_ACR_CNT")))) {
			params.put("NO_ACR_CNT", Integer.parseInt(params.get("NO_ACR_CNT").toString()));
		}
		params.put("START_DATE", StringUtil.nvl(params.get("START_DATE")).replaceAll("-", ""));
		params.put("END_DATE", StringUtil.nvl(params.get("END_DATE")).replaceAll("-", ""));
		return clipPntCmprHistDao.viewClipPntCmprHist(params);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List viewClipPntCmprHistExcel(Map<String, Object> params) {
		if (!StringUtils.isEmpty(StringUtil.nvl(params.get("NO_ACR_CNT")))) {
			params.put("NO_ACR_CNT", Integer.parseInt(params.get("NO_ACR_CNT").toString()));
		}
		params.put("START_DATE", StringUtil.nvl(params.get("START_DATE")).replaceAll("-", ""));
		params.put("END_DATE", StringUtil.nvl(params.get("END_DATE")).replaceAll("-", ""));
		return clipPntCmprHistDao.viewClipPntCmprHistExcel(params);
	}

	@Override
	public Map<String, Object> clipPntTtlCmprHistTotal(Map<String, Object> params) {
		return clipPntCmprHistDao.clipPntTtlCmprHistTotal(params);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List clipPntTtlCmprHist(Map<String, Object> params) {
		return clipPntCmprHistDao.clipPntTtlCmprHist(params);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List clipPntTtlCmprHistExcel(Map<String, Object> params) {
		return clipPntCmprHistDao.clipPntTtlCmprHistExcel(params);
	}

	@Override
	public Map<String, Object> clipPntSucsHistTotal(Map<String, Object> params) {
		return clipPntCmprHistDao.clipPntSucsHistTotal(params);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List clipPntSucsHist(Map<String, Object> params) {
		return clipPntCmprHistDao.clipPntSucsHist(params);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List clipPntSucsHistExcel(Map<String, Object> params) {
		return clipPntCmprHistDao.clipPntSucsHistExcel(params);
	}

	@Override
	public Map<String, Object> clipPntFailHistTotal(Map<String, Object> params) {
		return clipPntCmprHistDao.clipPntFailHistTotal(params);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List clipPntFailHist(Map<String, Object> params) {
		return clipPntCmprHistDao.clipPntFailHist(params);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List clipPntFailHistExcel(Map<String, Object> params) {
		return clipPntCmprHistDao.clipPntFailHistExcel(params);
	}
	
	@Override
	public Map<String, Object> clipPntCmprFileHistTotal(Map<String, Object> params) {
		return clipPntCmprHistDao.clipPntCmprFileHistTotal(params);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List clipPntCmprFileHist(Map<String, Object> params) {
		return clipPntCmprHistDao.clipPntCmprFileHist(params);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List clipPntCmprFileHistExcel(Map<String, Object> params) {
		return clipPntCmprHistDao.clipPntCmprFileHistExcel(params);
	}
}
