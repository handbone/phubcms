package com.olleh.pHubCMS.admin.cmpr.service;

import java.util.List;
import java.util.Map;

public interface CmprService {
	
	public List viewClipCmpr(Map<String, Object> params);
	public Map<String, Object> viewClipCmprTotal(Map<String, Object> params);
	public List viewClipCmprExcel(Map<String, Object> params);
	
	public int dealCancelReq(Map<String, Object> params);

}
