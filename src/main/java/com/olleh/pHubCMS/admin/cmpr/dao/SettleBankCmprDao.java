package com.olleh.pHubCMS.admin.cmpr.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * 세틀뱅크 일대사 내역 Dao
 * @Class Name : SettleBankCmprDao
 * @author : bmg
 * @since : 2018.11.13
 * @version : 1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *      수정일                 수정자                수정내용
 *  ------------  -----------    -------------
 *  2018.11.13      bmg          최초 생성
 * 
 */
@Repository
public class SettleBankCmprDao extends AbstractDAO {

	@SuppressWarnings("unchecked")
	public Map<String, Object> settleBankCmprTotal(Map<String, Object> params) {
		return selectOne("mybatis.cmpr.settleBankCmprTotal", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List settleBankCmpr(Map<String, Object> params) {
		return selectList("mybatis.cmpr.settleBankCmpr", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List settleBankCmprExcel(Map<String, Object> params) {
		return selectList("mybatis.cmpr.settleBankCmprExcel", params);
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> settleBankCmprFileTotal(Map<String, Object> params) {
		return selectOne("mybatis.cmpr.settleBankCmprFileTotal", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List settleBankCmprFile(Map<String, Object> params) {
		return selectList("mybatis.cmpr.settleBankCmprFile", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List settleBankCmprFileExcel(Map<String, Object> params) {
		return selectList("mybatis.cmpr.settleBankCmprFileExcel", params);
	}
}
