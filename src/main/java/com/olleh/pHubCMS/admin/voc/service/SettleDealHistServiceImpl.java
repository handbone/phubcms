package com.olleh.pHubCMS.admin.voc.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.voc.dao.SettleDealHistDao;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 거래내역(세틀뱅크) Service
 * 
 * @Class Name : SettleDealHistServiceImpl
 * @author : bmg
 * @since : 2019.01.09
 * @version : 1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *      수정일    수정자         수정내용
 *  ------------  -----------    -------------
 *  2019.01.09    bmg            최초 생성
 */
@Service
public class SettleDealHistServiceImpl implements SettleDealHistService {
	
	@Autowired
	SettleDealHistDao settleDealHistDao;

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List settleDealHist(Map<String, Object> params) {
		List rtn = settleDealHistDao.settleDealHist(params);
		for(int i=0; i<rtn.size(); i++) {
			Map map = (Map) rtn.get(i);		
			map.put("cust_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("cust_nm"))));
			rtn.set(i, map);
		}
		return rtn;
	}
	
}
