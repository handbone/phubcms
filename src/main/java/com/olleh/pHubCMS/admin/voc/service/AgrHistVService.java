package com.olleh.pHubCMS.admin.voc.service;

import java.util.List;
import java.util.Map;

public interface AgrHistVService {
	
	public Map<String, Object> viewAgrHistTotal(Map<String, Object> params);
	
	@SuppressWarnings("rawtypes")
	public List viewAgrHist(Map<String, Object> params);
	
	@SuppressWarnings("rawtypes")
	public List viewArgHistDtl(Map<String, Object> params);
}
