package com.olleh.pHubCMS.admin.voc.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.connHist.service.DealReqHistServiceImpl;
import com.olleh.pHubCMS.admin.connHist.service.SysUseLogService;
import com.olleh.pHubCMS.admin.system.service.UserInfoService;
import com.olleh.pHubCMS.admin.voc.service.SettleDealHistService;
import com.olleh.pHubCMS.common.components.CodeManage;
import com.olleh.pHubCMS.common.components.Constant;
import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.components.SysPrmtManage;
import com.olleh.pHubCMS.common.exception.UserException;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.CmnCdVO;
import com.olleh.pHubCMS.common.utils.SessionUtils;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 거래내역(세틀뱅크) Controller
 * 
 * @Class Name : SettleDealHistController
 * @author bmg
 * @since 2018.09.28
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.28   bmg        최초생성
 * 
 */
@Controller
@SuppressWarnings({"rawtypes", "unchecked"})
public class SettleDealHistController {
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	CodeManage codeManage;
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
	SysPrmtManage sysPrmtManage;
	
	@Autowired
	SettleDealHistService settleDealHistService;
	
	@Autowired
	UserInfoService userInfoService;
	
	@Autowired
	SysUseLogService sysUseLogService;
	
	@Autowired
	DealReqHistServiceImpl dealReqHistService;
	
	/**
	 * jsp 호출 및 초기값 설정
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/voc/viewSettleDealHist.do", method = RequestMethod.POST)
	public String viewSysPrmt(Locale locale, Model model) {	
		String method = "/voc/viewSettleDealHist";
		log.debug(method, ">>> start");	
		
		//사용 'Y' 등록된 거래 구분 가져오기 
		List<CmnCdVO> indList = new ArrayList<CmnCdVO>();
		indList = codeManage.getCodeListY("DEAL_IND");
		model.addAttribute("DEAL_IND", indList);
		
		//포인트허브 URL 가져오기
		String phUrl = sysPrmtManage.getSysPrmtVal("PH_DOMAIN");
		model.addAttribute("PH_DOMAIN", phUrl);
		
		// 서비스 ID 가져오기
		List svcCdList = codeManage.getCodeListY("PH_SVC_CD");
		model.addAttribute("PH_SVC_CD", svcCdList);
		
		log.debug(method, ">>>>>> end");	
		return "/voc/settleDealHistView";
	}
	
	/**
	 * 거래내역(세틀뱅크) JQGrid 호출
	 * 
	 * @param params
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/voc/jgSettleDealHist.do", method = RequestMethod.POST)
	public Map<String, Object> jgSettleDealHist(@RequestParam Map<String, Object> params, HttpServletRequest request, HttpServletResponse response) throws IOException {
		String method = "/voc/jgSettleDealHist";
		log.debug(method, ">>> start");	
		log.debug(method,params.toString());
		HashMap map = new HashMap();
		try {
			
			if (!isValidDate(StringUtil.nvl(params.get("START_DATE"), ""))) {
				throw new UserException(getClass().getName(), method, "1", Constant.FAIL_CODE, "시작일자가 유효한 날짜 형식이 아닙니다");
			}
			if (!isValidDate(StringUtil.nvl(params.get("END_DATE"), ""))) {
				throw new UserException(getClass().getName(), method, "1", Constant.FAIL_CODE, "종료일자가 유효한 날짜 형식이 아닙니다");
			}
			String sessionUserId = SessionUtils.getUserId(request);
			int maxInqrCnt = getMaxInqrCntToUserInfo(sessionUserId);
			int logCnt = getLogCntToUserLog(sessionUserId);
			List resultList = new ArrayList();
			if (maxInqrCnt <= logCnt) {
				log.error(method, "maxInqrCnt: "+ maxInqrCnt + ", logCnt: "+ logCnt);
				throw new UserException(getClass().getName(), method, "2", Constant.FAIL_CODE, "일일 조회 가능 횟수가 초과하였습니다");
			} else {
				resultList = settleDealHistService.settleDealHist(params);
			}
			map.put("rows", resultList);
			map.put("MAX_INQR_CNT", maxInqrCnt);
			map.put("LOG_CNT", logCnt);
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (UserException e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			map.put("errMsg", e.getErrorInfo().getErrMsg());
		} catch (Exception e) {
			log.printStackTracePH(method, e);
			throw new IOException(e);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 상세거래요청내역 조회
	 * 
	 * @param PHUB_TR_NO 포인트허브거래번호 
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/voc/ajaxDealReqDtl.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxDealReqDtl(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "ajaxDealReqDtl";
		log.debug(method, ">>> start");					
		HashMap map = new HashMap();
		try{
			//Request param		
			log.debug(method, "PHUB_TR_NO : "+params.get("PHUB_TR_NO"));
			
			//상세 거래내역 가져오기 		
			List resultList = new ArrayList();
			resultList = dealReqHistService.viewDealReqDtl(params);	
			log.debug(method, resultList.toString());	
			
			if(resultList!=null && resultList.size() > 0){	
				map.put("rows", resultList );
			}
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * 최대 조회 수 얻기
	 * 
	 * @param result
	 * @return
	 */
	private int getMaxInqrCntToUserInfo(String userId) throws IOException {
		int rtn = 0;
		Map<String, Object> paramMap = new HashMap<String, Object>();
		try {
			paramMap.put("USER_ID", userId);
			
			Map<String, Object> userInfo = userInfoService.getUserInfo(paramMap);
			try {
				rtn = Integer.parseInt(userInfo.get("max_inqr_cnt").toString());
			} catch (Exception e1) {}
		} catch (Exception e) {
			throw e;
		}
		return rtn;
	}
	
	/**
	 * log 카운트 수 얻기
	 * 
	 * @param result
	 * @return
	 */
	private int getLogCntToUserLog(String userId) throws IOException {
		int rtn = 0;
		Map<String, Object> paramMap = new HashMap<String, Object>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			paramMap.put("RGST_USER_ID", userId);
			paramMap.put("START_DATE", sdf.format(new Date()));
			paramMap.put("END_DATE", sdf.format(new Date()));
			paramMap.put("URIS", Constant.INQR_ACTION_URLS);
			
			Map<String, Object> cntInfo = sysUseLogService.viewSysUseLogTotal(paramMap);
			try {
				rtn = Integer.parseInt(cntInfo.get("cnt").toString());
			} catch (Exception e1) {}
		} catch (Exception e) {
			log.printStackTracePH("getLogCntToUserLog", e);
		}
		return rtn;
	}
	
	/**
	 * 날짜 유효성 체크
	 * 
	 * @param input
	 * @return
	 */
	private boolean isValidDate(String input) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			format.parse(input);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}