package com.olleh.pHubCMS.admin.voc.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * 거래내역(세틀뱅크) Dao
 * @Class Name : SettleDealHistDao
 * @author : bmg
 * @since : 2019.01.09
 * @version : 1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *      수정일    수정자         수정내용
 *  ------------  -----------    -------------
 *  2019.01.09    bmg            최초 생성
 */
@Repository
public class SettleDealHistDao extends AbstractDAO {
	
	@SuppressWarnings("rawtypes")
	public List settleDealHist(Map<String, Object> params) {
		return selectList("mybatis.voc.settleDealHist", params);
	}
}
