package com.olleh.pHubCMS.admin.voc.service;

import java.util.List;
import java.util.Map;

public interface CustInfoVService {
	
	@SuppressWarnings("rawtypes")
	public List viewCustInfo(Map<String, Object> params);
}
