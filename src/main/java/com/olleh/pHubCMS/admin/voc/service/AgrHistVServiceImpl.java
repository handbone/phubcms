package com.olleh.pHubCMS.admin.voc.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.voc.dao.AgrHistVDao;

/**
 * 약관 동의내역 Service
 * 
 * @Class Name : AgrHistService
 * @author 		bmg
 * @since 		2019.01.15
 * @version 	1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.01.15   bmg        최초생성
 */
@Service
public class AgrHistVServiceImpl implements AgrHistVService {
	
	@Autowired
	AgrHistVDao agrHistVDao;
	
	@Override
	public Map<String, Object> viewAgrHistTotal(Map<String, Object> params) {
		return agrHistVDao.agrHistTotal(params);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List viewAgrHist(Map<String, Object> params) {
		return agrHistVDao.agrHist(params);
	}	
	
	@Override
	@SuppressWarnings("rawtypes")
	public List viewArgHistDtl(Map<String, Object> params) {
		return agrHistVDao.agrHistDtl(params);
	}
}
