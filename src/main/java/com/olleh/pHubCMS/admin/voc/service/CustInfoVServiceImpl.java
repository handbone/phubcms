package com.olleh.pHubCMS.admin.voc.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.voc.dao.CustInfoVDao;

/**
 * 고객 정보 Service
 * 
 * @Class Name : CustInfoService
 * @author 	bmg
 * @since 	2019.01.15
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.01.15   bmg        최초생성
 */
@Service
public class CustInfoVServiceImpl implements CustInfoVService {

	@Autowired
	CustInfoVDao custInfoVDao;
	
	@Override
	@SuppressWarnings("rawtypes")
	public List viewCustInfo(Map<String, Object> params){
		return custInfoVDao.custInfo(params);		
	}	
}
