package com.olleh.pHubCMS.admin.voc.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * VOC 약관 동의내역 조회 Dao
 * @Class Name : AgrHistDao
 * @author : 	bmg
 * @since : 	2019.01.15
 * @version : 	1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *  수정일        수정자         수정내용
 *  ------------  -----------    -------------
 *  2019.01.15    bmg            최초 생성
 */
@Repository
public class AgrHistVDao extends AbstractDAO {

	@SuppressWarnings("unchecked")
	public Map<String, Object> agrHistTotal(Map<String, Object> params) {
		return selectOne("mybatis.voc.agrHistTotal", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List agrHist(Map<String, Object> params) {
		return selectList("mybatis.voc.agrHist", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List agrHistDtl(Map<String, Object> params) {
		return selectList("mybatis.voc.agrHistDtl", params);
	}
}
