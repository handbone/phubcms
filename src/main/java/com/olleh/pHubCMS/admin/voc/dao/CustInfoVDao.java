package com.olleh.pHubCMS.admin.voc.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * VOC 고객 정보 Dao
 * @Class Name : CustInfoDao
 * @author : 	bmg
 * @since : 	2019.01.15
 * @version : 	1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *  수정일        수정자         수정내용
 *  ------------  -----------    -------------
 *  2019.01.15    bmg            최초 생성
 */
@Repository
public class CustInfoVDao extends AbstractDAO {
	
	@SuppressWarnings("rawtypes")
	public List custInfo(Map<String, Object> params) {
		return selectList("mybatis.voc.custInfo", params);
	}
}
