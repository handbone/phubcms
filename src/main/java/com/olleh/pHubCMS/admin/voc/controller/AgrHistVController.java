package com.olleh.pHubCMS.admin.voc.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.connHist.service.SysUseLogService;
import com.olleh.pHubCMS.admin.system.service.UserInfoService;
import com.olleh.pHubCMS.admin.voc.service.AgrHistVService;
import com.olleh.pHubCMS.common.components.Constant;
import com.olleh.pHubCMS.common.exception.UserException;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.utils.Paging;
import com.olleh.pHubCMS.common.utils.SessionUtils;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 약관 동의내역 조회  Controller
 * 
 * @Class Name : AgrHistController
 * @author 	bmg
 * @since 	2019.01.15
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.01.15   bmg        최초생성
 */
@Controller
public class AgrHistVController {
	
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	AgrHistVService agrHistVService;
	
	@Autowired
	UserInfoService userInfoService;
	
	@Autowired
	SysUseLogService sysUseLogService;
	
	/**
	 * jsp 호출 및 초기값 설정
	 * 
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/voc/viewAgrHist.do", method = RequestMethod.POST)
	public String viewAgrHist(Locale locale, Model model) {	
		String method = "/voc/viewAgrHist";
		log.debug(method, ">>> start");	
			
		log.debug(method, ">>>>>> end");	
		return "/voc/agrHistView";
	}
	
	/**
	 * 약관 동의내역 조회
	 * 
	 * @param params
	 * @param req
	 * @param res
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/voc/jgAgrHist.do", method = RequestMethod.POST)
	public Map<String, Object> jgAgrHist(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {
		String method = "jgAgrHist";		
		log.debug(method, ">>> start");
		HashMap map = new HashMap();
		try{
			if (!isValidDate(StringUtil.nvl(params.get("START_DATE"), ""))) {
				throw new UserException(getClass().getName(), method, "1", Constant.FAIL_CODE, "시작일자가 유효한 날짜 형식이 아닙니다");
			}
			if (!isValidDate(StringUtil.nvl(params.get("END_DATE"), ""))) {
				throw new UserException(getClass().getName(), method, "1", Constant.FAIL_CODE, "종료일자가 유효한 날짜 형식이 아닙니다");
			}
			List resultList = new ArrayList();
			Map<String, Object> count = new HashMap<String, Object>();
			String sessionUserId = SessionUtils.getUserId(req);
			//Request param
			int pageNum = Integer.parseInt(req.getParameter("page"));
			int limit = Integer.parseInt(req.getParameter("rows"));	
			log.debug(method, params.toString());
			int maxInqrCnt = getMaxInqrCntToUserInfo(sessionUserId);
			int logCnt = getLogCntToUserLog(sessionUserId);
			if (maxInqrCnt <= logCnt) {
				log.error(method, "maxInqrCnt: "+ maxInqrCnt + ", logCnt: "+ logCnt);
				throw new UserException(getClass().getName(), method, "2", Constant.FAIL_CODE, "일일 조회 가능 횟수가 초과하였습니다");
			} else {
				//총 Row 수 계산 		
				count = agrHistVService.viewAgrHistTotal(params);	
				int countRow = Integer.parseInt(count.get("cnt").toString());
				log.debug(method,"count : "+countRow);
				if(countRow > 0) {
					//HashMap에 페이징 정보 저장					
					Paging.setPageMap(params, map, pageNum, limit, countRow);	
					log.debug(method,map.toString());
					//검색 결과 데이터 map 에 추가
					log.debug(method,params.toString());
					resultList = agrHistVService.viewAgrHist(params);		
					mappingAgrHist(resultList);
				}
			}
			map.put("rows", resultList );
			map.put("MAX_INQR_CNT", maxInqrCnt);
			map.put("LOG_CNT", logCnt);
		} catch (UserException e) {
			map.put("errMsg", e.getErrorInfo().getErrMsg());
		} catch (Exception e) {
			log.printStackTracePH(method, e);
			throw new IOException(e);
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}	
	
	/**
	 * 고객정보 조회 화면의 상세 약관동의내역 조회
	 * 
	 * @param params
	 * @param req
	 * @param res
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/voc/ajaxAgrHist.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxAgrHist(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "/voc/ajaxAgrHist";
		log.debug(method, ">>> start");					
		HashMap map = new HashMap();
		try{
			List resultList = new ArrayList();
			resultList = agrHistVService.viewArgHistDtl(params);
			log.debug(method, resultList.toString());	
			
			if(resultList!=null && resultList.size() > 0){	
				map.put("rows", resultList );
			}
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * 최대 조회 수 얻기
	 * 
	 * @param result
	 * @return
	 */
	private int getMaxInqrCntToUserInfo(String userId) throws IOException {
		int rtn = 0;
		Map<String, Object> paramMap = new HashMap<String, Object>();
		try {
			paramMap.put("USER_ID", userId);
			
			Map<String, Object> userInfo = userInfoService.getUserInfo(paramMap);
			try {
				rtn = Integer.parseInt(userInfo.get("max_inqr_cnt").toString());
			} catch (Exception e1) {}
		} catch (Exception e) {}
		return rtn;
	}
	
	/**
	 * log 카운트 수 얻기
	 * 
	 * @param result
	 * @return
	 */
	private int getLogCntToUserLog(String userId) throws IOException {
		int rtn = 0;
		Map<String, Object> paramMap = new HashMap<String, Object>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			paramMap.put("RGST_USER_ID", userId);
			paramMap.put("START_DATE", sdf.format(new Date()));
			paramMap.put("END_DATE", sdf.format(new Date()));
			paramMap.put("URIS", Constant.INQR_ACTION_URLS);
			
			Map<String, Object> cntInfo = sysUseLogService.viewSysUseLogTotal(paramMap);
			try {
				rtn = Integer.parseInt(cntInfo.get("cnt").toString());
			} catch (Exception e1) {}
		} catch (Exception e) {
			throw e;
		}
		return rtn;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void mappingAgrHist(List resultList){
		// 핸드폰 마스킹 처리
		for(int i=0; i<resultList.size(); i++){
			Map<String, Object> resultTmp = (Map<String, Object>) resultList.get(i);		
			resultTmp.put("cust_ctn", StringUtil.getMaskingPhone(StringUtil.nvl(resultTmp.get("cust_ctn"))));
			resultTmp.put("cust_nm", StringUtil.getMaskingName(StringUtil.nvl(resultTmp.get("cust_nm"))));
			resultList.set(i, resultTmp);
		}	
	}
	
	/**
	 * 날짜 유효성 체크
	 * 
	 * @param input
	 * @return
	 */
	private boolean isValidDate(String input) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			format.parse(input);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
