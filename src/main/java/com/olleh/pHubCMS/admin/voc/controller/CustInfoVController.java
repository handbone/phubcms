package com.olleh.pHubCMS.admin.voc.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.connHist.service.SysUseLogService;
import com.olleh.pHubCMS.admin.system.service.UserInfoService;
import com.olleh.pHubCMS.admin.voc.service.CustInfoVService;
import com.olleh.pHubCMS.common.components.Constant;
import com.olleh.pHubCMS.common.exception.UserException;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.utils.SessionUtils;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 고객정보  Controller
 * 
 * @Class Name : CustInfoController
 * @author 	bmg
 * @since 	2019.01.15
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.01.15   bmg        최초생성
 */
@Controller
public class CustInfoVController {
	
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	CustInfoVService custInfoVService;
	
	@Autowired
	UserInfoService userInfoService;
	
	@Autowired
	SysUseLogService sysUseLogService;
	
	/**
	 * jsp 호출 및 초기값 설정
	 * 
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/voc/viewCustInfo.do", method = RequestMethod.POST)
	public String viewCustInfo(Locale locale, Model model) {	
		String method = "/voc/viewCustInfo";
		log.debug(method, ">>> start");	
		
		log.debug(method, ">>>>>> end");	
		return "/voc/custInfoView";
	}
	
	/**
	 * JQGrid 호출
	 * 
	 * @param params
	 * @param req
	 * @param res
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/voc/jgCustInfo.do", method = RequestMethod.POST)
	public Map<String, Object> jgCustInfo(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {
		String method = "/voc/jgCustInfo";
		log.debug(method, ">>> start");	
		log.debug(method,params.toString());
		String sessionUserId = SessionUtils.getUserId(req);
		HashMap map = new HashMap();
		try {
			int maxInqrCnt = getMaxInqrCntToUserInfo(sessionUserId);
			int logCnt = getLogCntToUserLog(sessionUserId);
			List resultList = new ArrayList();
			if (maxInqrCnt <= logCnt) {
				log.error(method, "maxInqrCnt: "+ maxInqrCnt + ", logCnt: "+ logCnt);
				throw new UserException(getClass().getName(), method, "1", Constant.FAIL_CODE, "일일 조회 가능 횟수가 초과하였습니다");
			} else {
				resultList = custInfoVService.viewCustInfo(params);
				mappingCustInfo(resultList);
			}
			map.put("rows", resultList);
			map.put("MAX_INQR_CNT", maxInqrCnt);
			map.put("LOG_CNT", logCnt);
		} catch (UserException e) {
			map.put("errMsg", e.getErrorInfo().getErrMsg());
		} catch (Exception e) {
			log.printStackTracePH(method, e);
			throw new IOException(e);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 최대 조회 수 얻기
	 * 
	 * @param result
	 * @return
	 */
	private int getMaxInqrCntToUserInfo(String userId) throws IOException {
		int rtn = 0;
		Map<String, Object> paramMap = new HashMap<String, Object>();
		try {
			paramMap.put("USER_ID", userId);
			
			Map<String, Object> userInfo = userInfoService.getUserInfo(paramMap);
			try {
				rtn = Integer.parseInt(userInfo.get("max_inqr_cnt").toString());
			} catch (Exception e1) {}
		} catch (Exception e) {}
		return rtn;
	}
	
	/**
	 * log 카운트 수 얻기
	 * 
	 * @param result
	 * @return
	 */
	private int getLogCntToUserLog(String userId) throws IOException {
		int rtn = 0;
		Map<String, Object> paramMap = new HashMap<String, Object>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			paramMap.put("RGST_USER_ID", userId);
			paramMap.put("START_DATE", sdf.format(new Date()));
			paramMap.put("END_DATE", sdf.format(new Date()));
			paramMap.put("URIS", Constant.INQR_ACTION_URLS);
			
			Map<String, Object> cntInfo = sysUseLogService.viewSysUseLogTotal(paramMap);
			try {
				rtn = Integer.parseInt(cntInfo.get("cnt").toString());
			} catch (Exception e1) {}
		} catch (Exception e) {
			throw e;
		}
		return rtn;
	}
	
	
	/**
	 * 마스킹 처리
	 * 
	 * @param resultList
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void mappingCustInfo(List resultList){
		// 핸드폰 마스킹 처리
		for(int i=0; i<resultList.size(); i++){
			Map<String, Object> resultTmp = (Map<String, Object>) resultList.get(i);		
			resultTmp.put("cust_ctn", StringUtil.getMaskingPhone(StringUtil.nvl(resultTmp.get("cust_ctn"))));
			resultTmp.put("cust_nm", StringUtil.getMaskingName(StringUtil.nvl(resultTmp.get("cust_nm"))));
			resultList.set(i, resultTmp);
		}	
	}

}
