package com.olleh.pHubCMS.admin.dealHist.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * 단말할인권 문자발송이력 Dao
 * 
 * @Class 	DicntCpnMMSSendHistDao
 * @author	bmg
 * @since 	2019.03.05
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.03.05   bmg        최초생성
 */
@Repository
@SuppressWarnings({ "unchecked", "rawtypes" })
public class DicntCpnMMSSendHistDao extends AbstractDAO {
	
	public Map<String, Object> dicntCpnMMSSendHistTotal(Map<String, Object> params) {
		return selectOne("mybatis.dealHist.dicntCpnMMSSendHistTotal", params);
	}
	
	public List<Map> dicntCpnMMSSendHist(Map<String, Object> params) {
		return selectList("mybatis.dealHist.dicntCpnMMSSendHist", params);
	}
	
	public List<Map> dicntCpnMMSSendHistExcel(Map<String, Object> params) {
		return selectList("mybatis.dealHist.dicntCpnMMSSendHistExcel", params);
	}
	
	public List<Map> mmsFormInfo() {
		return selectList("mybatis.dealHist.mmsFormInfo");
	}
}
