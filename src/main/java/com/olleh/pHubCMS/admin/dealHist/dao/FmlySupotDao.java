package com.olleh.pHubCMS.admin.dealHist.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;
import com.olleh.pHubCMS.common.log.Logger;

/**
 * 조르기현황 내역 조회 Dao
 * @Class Name : fmlySupotDao
 * @author : ojh
 * @since : 2019.05.22
 * @version : 1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *      수정일                 수정자                수정내용
 *  ------------  -----------    -------------
 *  2019. 5. 22.      kimht          최초 생성
 * 
 */

@Repository
public class FmlySupotDao extends AbstractDAO {
	private Logger log = new Logger(this.getClass());
	
	public List viewSupotList(Map<String, Object> params) {
		return selectList("mybatis.fmlySupot.supotList", params);
	}	
	public Map<String, Object> viewSupotListTotal(Map<String, Object> params) {
		return selectOne("mybatis.fmlySupot.supotListTotal", params);
	}	
	public List viewfmlySupotExcel(Map<String, Object> params) {
		return selectList("mybatis.fmlySupot.fmlySupotExcel", params);
	}	
	

}
