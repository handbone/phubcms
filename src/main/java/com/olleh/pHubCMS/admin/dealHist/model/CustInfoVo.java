package com.olleh.pHubCMS.admin.dealHist.model;

/**
 * 고객 정보 VO
 * 
 * @Class Name : CustInfoVo
 * @author ojh
 * @since 2018.08.21
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.21   ojh        최초생성
 * 
 */
public class CustInfoVo {
	
	private String cust_id;			// 고객아이디
	private String cust_nm;			// 고객이름
	private String cust_ci;			// 고객CI
	private String clip_mmbr_yn;	// 클립멤버여부
	private String clip_sbsc_dt;	// 클립가입일
	private String clip_rtr_dd;		// 클립탈퇴일
	private String rgst_dt;			// 등록일시
	private String rgst_user_id;	// 등록자아이디
	private String mdfy_dt;			// 수정일시
	private String mdfy_user_id;	// 수정자아이디
	
	public String getCust_id() {
		return cust_id;
	}
	public void setCust_id(String cust_id) {
		this.cust_id = cust_id;
	}
	public String getCust_nm() {
		return cust_nm;
	}
	public void setCust_nm(String cust_nm) {
		this.cust_nm = cust_nm;
	}
	public String getCust_ci() {
		return cust_ci;
	}
	public void setCust_ci(String cust_ci) {
		this.cust_ci = cust_ci;
	}
	public String getClip_mmbr_yn() {
		return clip_mmbr_yn;
	}
	public void setClip_mmbr_yn(String clip_mmbr_yn) {
		this.clip_mmbr_yn = clip_mmbr_yn;
	}
	public String getClip_sbsc_dt() {
		return clip_sbsc_dt;
	}
	public void setClip_sbsc_dt(String clip_sbsc_dt) {
		this.clip_sbsc_dt = clip_sbsc_dt;
	}
	public String getClip_rtr_dd() {
		return clip_rtr_dd;
	}
	public void setClip_rtr_dd(String clip_rtr_dd) {
		this.clip_rtr_dd = clip_rtr_dd;
	}
	public String getRgst_dt() {
		return rgst_dt;
	}
	public void setRgst_dt(String rgst_dt) {
		this.rgst_dt = rgst_dt;
	}
	public String getRgst_user_id() {
		return rgst_user_id;
	}
	public void setRgst_user_id(String rgst_user_id) {
		this.rgst_user_id = rgst_user_id;
	}
	public String getMdfy_dt() {
		return mdfy_dt;
	}
	public void setMdfy_dt(String mdfy_dt) {
		this.mdfy_dt = mdfy_dt;
	}
	public String getMdfy_user_id() {
		return mdfy_user_id;
	}
	public void setMdfy_user_id(String mdfy_user_id) {
		this.mdfy_user_id = mdfy_user_id;
	}
	

}
