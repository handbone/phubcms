package com.olleh.pHubCMS.admin.dealHist.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;
import com.olleh.pHubCMS.common.log.Logger;

/**
 * 약관 동의내역 조회 Dao
 * @Class Name : AgrHistDao
 * @author : ojh
 * @since : 2018.09.28
 * @version : 1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *      수정일                 수정자                수정내용
 *  ------------  -----------    -------------
 *  2018. 9. 28.      ojh          최초 생성
 * 
 */

@Repository
public class AgrHistDao extends AbstractDAO {
	private Logger log = new Logger(this.getClass());
	
	public List viewAgrHist(Map<String, Object> params) {
		return selectList("mybatis.agrHist.agrHist", params);
	}	
	public Map<String, Object> viewAgrHistTotal(Map<String, Object> params) {
		return selectOne("mybatis.agrHist.agrHistTotal", params);
	}	
	public List viewAgrHistExcel(Map<String, Object> params) {
		return selectList("mybatis.agrHist.agrHistExcel", params);
	}	
	

}
