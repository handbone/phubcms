package com.olleh.pHubCMS.admin.dealHist.service;

import java.util.List;
import java.util.Map;

public interface CustInfoService {
	
	public List viewCustInfo(Map<String, Object> params);
	public Map<String, Object> viewCustInfoTotal(Map<String, Object> params);
	public List viewAgrHist(Map<String, Object> params);
	public List viewCustInfoExcel(Map<String, Object> params);
}
