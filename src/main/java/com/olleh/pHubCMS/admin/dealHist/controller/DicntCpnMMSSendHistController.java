package com.olleh.pHubCMS.admin.dealHist.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.dealHist.service.DicntCpnMMSSendHistService;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.utils.Paging;

/**
 * 단말할인권 문자발송이력 Controller
 * 
 * @Class 	DicntCpnMMSSendHistController
 * @author	bmg
 * @since 	2019.03.05
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.03.05   bmg        최초생성
 */
@Controller
@SuppressWarnings({"rawtypes", "unchecked"})
public class DicntCpnMMSSendHistController {
	
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	DicntCpnMMSSendHistService dicntCpnMMSSendHistService;
	
	/**
	 * jsp 호출 및 초기값 설정
	 * 
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/dealHist/viewDicntCpnMMSSendHist.do", method = RequestMethod.POST)
	public String viewDicntCpnMMSSendHist(Locale locale, Model model) {
		String method = "viewDicntCpnMMSSendHist";
		log.debug(method, ">>> start");	
		
		// 메시지 양식 조회
		model.addAttribute("MMS_FORM_INFO", dicntCpnMMSSendHistService.mmsFormInfo());
		
		log.debug(method, ">>>>>> end");	
		return "/dealHist/dicntCpnMMSSendHistView";
	}
	
	/**
	 * JQGrid 단말할인권 문자발송이력 호출
	 * 
	 * @param params
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/dealHist/jgDicntCpnMMSSendHist.do", method = RequestMethod.POST)
	public Map<String, Object> jgDicntCpnMMSSendHist(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgDicntCpnMMSSendHist";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		Map<String, Object> count = new HashMap<String, Object>();
		try {
			//Request param
			int pageNum = Integer.parseInt(request.getParameter("page"));
			int limit = Integer.parseInt(request.getParameter("rows"));	
			log.debug(method, params.toString());
			
			//총 Row 수 계산 		
			count = dicntCpnMMSSendHistService.dicntCpnMMSSendHistTotal(params);
			int countRow = Integer.parseInt(count.get("cnt").toString());
			log.debug(method,"count : "+countRow);
			
			if(countRow > 0){
				//HashMap에 페이징 정보 저장					
				Paging.setPageMap(params, map, pageNum, limit, countRow);	
				log.debug(method,map.toString());
				
				//검색 결과 데이터 map 에 추가
				List resultList = new ArrayList();
				log.debug(method,params.toString());
				resultList = dicntCpnMMSSendHistService.dicntCpnMMSSendHist(params);			
				
				map.put("rows", resultList );
			}
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * JQGrid 단말할인권 조르기 문자발송이력 호출 (엑셀용 전체데이터)
	 * 
	 * @param params
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/dealHist/jgDicntCpnMMSSendHistExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgDicntCpnMMSSendHistExcel(@RequestParam Map<String, Object> params) throws IOException {
		String method = "jgDicntCpnMMSSendHistExcel";
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		try{
			//Request param
			log.debug(method, params.toString());		
					
			List resultList = new ArrayList();
			resultList = dicntCpnMMSSendHistService.dicntCpnMMSSendHistExcel(params);
			int countRow = resultList.size();
			log.debug(method, "COUNT : "+countRow);	
			map.put("rows", resultList );
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
}
