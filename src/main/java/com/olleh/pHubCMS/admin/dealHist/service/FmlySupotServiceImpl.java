package com.olleh.pHubCMS.admin.dealHist.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.dealHist.dao.FmlySupotDao;
import com.olleh.pHubCMS.common.components.CodeManage;
import com.olleh.pHubCMS.common.model.CmnCdVO;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 *  조르기 현황 내역 Service
 * 
 * @Class Name : FmlysupotServiceImpl
 * @author kimht
 * @since 2019.05.21
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2019.05.21 			kimht				최초생성
 * 
 */

@Service
public class FmlySupotServiceImpl implements FmlySupotService {
	
	@Autowired
	FmlySupotDao fmlySupotDao;
	
	@Override
	public List viewSupotList(Map<String, Object> params) {
		
		return mappingCustInfo(fmlySupotDao.viewSupotList(params));
	}	
	
	@Override
	public Map<String, Object> viewSupotListTotal(Map<String, Object> params) {
		return fmlySupotDao.viewSupotListTotal(params);
	}	
	
	/**
	 * <pre> 고객정보 마스킹 처리 + 진행상태 코드 이름 출력</pre>
	 * 
	 * @param List params
	 * @return List
	 * @see 
	 * <pre>
	 *      1. 고객전화번호
	 *      2. 고객명
	 *      
	 *      
	 *      </pre>
	 */
	@Override
	public List mappingCustInfo(List params) 
	{
		List resultList = new ArrayList(params);
		CodeManage codeManage = new CodeManage();
		// 고객명, 전화번호 마스킹 처리
		// ex) 홍길*, 01091***822
		for(int i=0; i<resultList.size(); i++){
			Map<String, Object> resultTmp = (Map<String, Object>) resultList.get(i);
			String statCd=null, prgrCd=null, reqCd = null;
			//마스킹처리
			resultTmp.put("cust_ctn", StringUtil.getMaskingPhone(StringUtil.nvl(resultTmp.get("cust_ctn"))));
			resultTmp.put("cust_nm", StringUtil.getMaskingName(StringUtil.nvl(resultTmp.get("cust_nm"))));
			
			//코드리스트 인스턴스 생성
			List<CmnCdVO> statIndList = codeManage.getCodeList("CPN_STAT_IND");
			List<CmnCdVO> prgrList = codeManage.getCodeList("CPN_REQ_PRGRS");
			List<CmnCdVO> reqStatList = codeManage.getCodeList("CPN_REQ_STAT");
			
			
			//코드의 네임값을 가져오는 반복문
			for(int j=0; j<statIndList.size(); j++){
				if(statIndList.get(j).getDtlCd().equals(resultTmp.get("cpn_stat_cd")))
				{
					statCd = statIndList.get(j).getDtlCdNm();
				}
			}
			for(int j=0; j<prgrList.size(); j++){
				if(prgrList.get(j).getDtlCd().equals(resultTmp.get("prgr_stat")))
				{
					prgrCd = prgrList.get(j).getDtlCdNm();
				}
			}
			for(int j=0; j<reqStatList.size(); j++){
				if(reqStatList.get(j).getDtlCd().equals(resultTmp.get("req_stat_cd")))
				{
					reqCd = reqStatList.get(j).getDtlCdNm();
				}
			}

			resultTmp.put("cpn_stat_cd",statCd);
			resultTmp.put("prgr_stat",prgrCd);
			resultTmp.put("req_stat_cd",reqCd);
			resultList.set(i, resultTmp);
		}
		
		return resultList;
	}	
	
	@Override
	public List viewfmlySupotExcel(Map<String, Object> params) 
	{
		return mappingCustInfo(fmlySupotDao.viewfmlySupotExcel(params));
	}
}
