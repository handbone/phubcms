package com.olleh.pHubCMS.admin.dealHist.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.dealHist.dao.DicntCpnDealHistDao;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 단말할인권 거래내역 Service
 * 
 * @Class 	DicntCpnDealHistServiceImpl
 * @author	bmg
 * @since 	2019.03.05
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.03.05   bmg        최초생성
 */
@Service
@SuppressWarnings({"rawtypes", "unchecked"})
public class DicntCpnDealHistServiceImpl implements DicntCpnDealHistService {
	
	@Autowired
	DicntCpnDealHistDao dicntCpnDealHistDao;

	@Override
	public Map<String, Object> dicntCpnDealHistTotal(Map<String, Object> params) {
		return dicntCpnDealHistDao.dicntCpnDealHistTotal(params);
	}
	
	@Override
	public List<Map> dicntCpnDealHist(Map<String, Object> params) {
		return mappingCustInfo(dicntCpnDealHistDao.dicntCpnDealHist(params));
	}

	@Override
	public List<Map> dicntCpnDealHistExcel(Map<String, Object> params) {
		return mappingCustInfo(dicntCpnDealHistDao.dicntCpnDealHistExcel(params));
	}
	
	/**
	 * 고객명 마스킹 처리
	 * 
	 * @param params
	 * @return
	 */
	private List mappingCustInfo(List<Map> params) {
		int index = 0;
		for (Map map : params) {
			map.put("cust_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("cust_nm"))));
			params.set(index, map);
			index++;
		}
		return params;
	}
}
