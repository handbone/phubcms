package com.olleh.pHubCMS.admin.dealHist.model;


/**
 * 거래 내역 정보 VO
 * 
 * @Class Name : DealHistVo
 * @author ojh
 * @since 2018.08.14
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.14   ojh        최초생성
 * 
 */
public class DealHistVo {
	
	private String phub_tr_no;			// 포인트허브 거래번호
	private String cprt_cmpn_id;		// 사용처ID
	private String pg_cmpn_id;			// PG사 ID
	private String pg_send_po_id;		// PG제공사용처 ID
	private String cust_ci;				// 고객CI
	private String cust_id;				// 고객ID
	private String deal_ind;			// 거래구분
	private String deal_dd;				// 거래일자
	private String cncl_yn;				// 취소여부
	private String pay_method;			// 단일복합결제구분
	private String pg_deal_no;			// PG거래번호
	private String goods_nm;			// 상품명
	private String ori_phub_tr_no;		// 이전거래번호
	private String ori_pg_deal_no;		// 이전PG거래번호
	private String ttl_pnt;				// 총포인트
	private String ttl_pay_amt;			// 총결제금액
	private String ttl_pnt_amt;			// 총포인트전환금액
	private String ttl_rmnd_amt;		// 총잔여금액
	private String rgst_dt;				// 등록일시
	private String rgst_user_id;		// 등록자ID
	private String mdfy_dt;				// 수정일시
	private String mdfy_user_id;		// 수정자ID
	
	public String getPhub_tr_no() {
		return phub_tr_no;
	}
	public void setPhub_tr_no(String phub_tr_no) {
		this.phub_tr_no = phub_tr_no;
	}
	public String getCprt_cmpn_id() {
		return cprt_cmpn_id;
	}
	public void setCprt_cmpn_id(String cprt_cmpn_id) {
		this.cprt_cmpn_id = cprt_cmpn_id;
	}
	public String getPg_cmpn_id() {
		return pg_cmpn_id;
	}
	public void setPg_cmpn_id(String pg_cmpn_id) {
		this.pg_cmpn_id = pg_cmpn_id;
	}
	public String getPg_send_po_id() {
		return pg_send_po_id;
	}
	public void setPg_send_po_id(String pg_send_po_id) {
		this.pg_send_po_id = pg_send_po_id;
	}
	public String getCust_ci() {
		return cust_ci;
	}
	public void setCust_ci(String cust_ci) {
		this.cust_ci = cust_ci;
	}
	public String getCust_id() {
		return cust_id;
	}
	public void setCust_id(String cust_id) {
		this.cust_id = cust_id;
	}
	public String getDeal_ind() {
		return deal_ind;
	}
	public void setDeal_ind(String deal_ind) {
		this.deal_ind = deal_ind;
	}
	public String getDeal_dd() {
		return deal_dd;
	}
	public void setDeal_dd(String deal_dd) {
		this.deal_dd = deal_dd;
	}
	public String getCncl_yn() {
		return cncl_yn;
	}
	public void setCncl_yn(String cncl_yn) {
		this.cncl_yn = cncl_yn;
	}
	public String getPay_method() {
		return pay_method;
	}
	public void setPay_method(String pay_method) {
		this.pay_method = pay_method;
	}
	public String getPg_deal_no() {
		return pg_deal_no;
	}
	public void setPg_deal_no(String pg_deal_no) {
		this.pg_deal_no = pg_deal_no;
	}
	public String getGoods_nm() {
		return goods_nm;
	}
	public void setGoods_nm(String goods_nm) {
		this.goods_nm = goods_nm;
	}
	public String getOri_phub_tr_no() {
		return ori_phub_tr_no;
	}
	public void setOri_phub_tr_no(String ori_phub_tr_no) {
		this.ori_phub_tr_no = ori_phub_tr_no;
	}
	public String getOri_pg_deal_no() {
		return ori_pg_deal_no;
	}
	public void setOri_pg_deal_no(String ori_pg_deal_no) {
		this.ori_pg_deal_no = ori_pg_deal_no;
	}
	public String getTtl_pnt() {
		return ttl_pnt;
	}
	public void setTtl_pnt(String ttl_pnt) {
		this.ttl_pnt = ttl_pnt;
	}
	public String getTtl_pay_amt() {
		return ttl_pay_amt;
	}
	public void setTtl_pay_amt(String ttl_pay_amt) {
		this.ttl_pay_amt = ttl_pay_amt;
	}
	public String getTtl_pnt_amt() {
		return ttl_pnt_amt;
	}
	public void setTtl_pnt_amt(String ttl_pnt_amt) {
		this.ttl_pnt_amt = ttl_pnt_amt;
	}
	public String getTtl_rmnd_amt() {
		return ttl_rmnd_amt;
	}
	public void setTtl_rmnd_amt(String ttl_rmnd_amt) {
		this.ttl_rmnd_amt = ttl_rmnd_amt;
	}
	public String getRgst_dt() {
		return rgst_dt;
	}
	public void setRgst_dt(String rgst_dt) {
		this.rgst_dt = rgst_dt;
	}
	public String getRgst_user_id() {
		return rgst_user_id;
	}
	public void setRgst_user_id(String rgst_user_id) {
		this.rgst_user_id = rgst_user_id;
	}
	public String getMdfy_dt() {
		return mdfy_dt;
	}
	public void setMdfy_dt(String mdfy_dt) {
		this.mdfy_dt = mdfy_dt;
	}
	public String getMdfy_user_id() {
		return mdfy_user_id;
	}
	public void setMdfy_user_id(String mdfy_user_id) {
		this.mdfy_user_id = mdfy_user_id;
	}
}
