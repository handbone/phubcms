package com.olleh.pHubCMS.admin.dealHist.service;

import java.util.List;
import java.util.Map;

@SuppressWarnings("rawtypes")
public interface DicntCpnDealHistService {

	public Map<String, Object> dicntCpnDealHistTotal(Map<String, Object> params);
	
	public List<Map> dicntCpnDealHist(Map<String, Object> params);
	
	public List<Map> dicntCpnDealHistExcel(Map<String, Object> params);
}
