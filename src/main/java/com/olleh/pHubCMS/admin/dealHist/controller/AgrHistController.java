package com.olleh.pHubCMS.admin.dealHist.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.dealHist.service.AgrHistServiceImpl;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.utils.Paging;

/**
 * 약관 동의내역 조회  Controller
 * 
 * @Class Name : AgrHistController
 * @author ojh
 * @since 2018.09.28
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.28   ojh        최초생성
 * 
 */


@Controller
public class AgrHistController {
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	AgrHistServiceImpl agrHistService;	
	
	/**
	 * jsp 호출 및 초기값 설정
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/dealHist/viewAgrHist.do", method = RequestMethod.POST)
	public String viewAgrHist(Locale locale, Model model) {	
		String method = "viewAgrHist";
		log.debug(method, ">>> start");	
			
		log.debug(method, ">>>>>> end");	
		return "/dealHist/agrHistView";
	}
	
	/**
	 * 약관 동의내역 조회
	 * JQGrid 호출
	 * 
	 * 	param page 현재 보고자 하는 페이지 (JQgrid 처리)
	 * @param rows 한 페이지에 보여주고자 설정된 개수 (JQgrid 처리)
	 * @param CUST_ID		Input  		아이디
	 * @param CUST_NM		selectBox 	이름
	 * @param CUST_CTN 		selectBox 	핸드폰번호
	 * @param CLIP_MMBR_YN 	selectBox 	취소여부	 * 
	 * @param CLS_TITL		Input		약관명
	 * @param START_DATE 	선택한 달력 값
	 * @param END_DATE 		선택한 달력 값
	 * @param page 현재 보고자 하는 페이지 (JQgrid 처리)
	 * @param rows 한 페이지에 보여주고자 설정된 개수 (JQgrid 처리)
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/dealHist/jgAgrHist.do", method = RequestMethod.POST)
	public Map<String, Object> jgAgrHist(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "jgAgrHist";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		Map<String, Object> count = new HashMap<String, Object>();
		try{
			//Request param
			int pageNum = Integer.parseInt(req.getParameter("page"));
			int limit = Integer.parseInt(req.getParameter("rows"));	
			log.debug(method, params.toString());
			
			//총 Row 수 계산 		
			count = agrHistService.viewAgrHistTotal(params);	
			int countRow = Integer.parseInt(count.get("cnt").toString());
			log.debug(method,"count : "+countRow);
			
			if(countRow > 0){
				//HashMap에 페이징 정보 저장					
				Paging.setPageMap(params, map, pageNum, limit, countRow);	
				log.debug(method,map.toString());
				
				//검색 결과 데이터 map 에 추가
				List resultList = new ArrayList();
				log.debug(method,params.toString());
				resultList = agrHistService.viewAgrHist(params);					
				
				map.put("rows", resultList );
			}
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}	
	
	/**
	 * 약관 동의내역 조회
	 * JQGrid 호출
	 * 
	 * 	param page 현재 보고자 하는 페이지 (JQgrid 처리)
	 * @param rows 한 페이지에 보여주고자 설정된 개수 (JQgrid 처리)
	 * @param CUST_ID		Input  		아이디
	 * @param CUST_NM		selectBox 	이름
	 * @param CUST_CTN 		selectBox 	핸드폰번호
	 * @param CLIP_MMBR_YN 	selectBox 	취소여부	 * 
	 * @param CLS_TITL		Input		약관명
	 * @param START_DATE 	선택한 달력 값
	 * @param END_DATE 		선택한 달력 값
	 * @param page 현재 보고자 하는 페이지 (JQgrid 처리)
	 * @param rows 한 페이지에 보여주고자 설정된 개수 (JQgrid 처리)
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/dealHist/jgAgrHistExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgAgrHistExcel(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "jgAgrHistExcel";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		Map<String, Object> count = new HashMap<String, Object>();
		try{
			//Request param
			log.debug(method, params.toString());

			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			resultList = agrHistService.viewAgrHistExcel(params);		
			map.put("rows", resultList );
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}	

}
