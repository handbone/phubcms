package com.olleh.pHubCMS.admin.dealHist.service;

import java.util.List;
import java.util.Map;

@SuppressWarnings("rawtypes")
public interface DicntCpnMMSSendHistService {

	public Map<String, Object> dicntCpnMMSSendHistTotal(Map<String, Object> params) throws Exception;
	
	public List<Map> dicntCpnMMSSendHist(Map<String, Object> params) throws Exception;
	
	public List<Map> dicntCpnMMSSendHistExcel(Map<String, Object> params) throws Exception;
	
	public List<Map> mmsFormInfo();
	
}
