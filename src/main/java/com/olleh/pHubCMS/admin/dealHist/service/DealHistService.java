package com.olleh.pHubCMS.admin.dealHist.service;

import java.util.List;
import java.util.Map;

@SuppressWarnings("rawtypes")
public interface DealHistService {
	
	public List viewDealHist(Map<String, Object> params);
	public Map<String, Object> viewDealHistTotal(Map<String, Object> params);
	public List viewDealHistExcel(Map<String, Object> params);
	public List viewDealDtl(Map<String, Object> params);
	
	public List mappingCustInfo(List params);
	
}
