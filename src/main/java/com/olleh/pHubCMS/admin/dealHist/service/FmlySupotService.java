package com.olleh.pHubCMS.admin.dealHist.service;

import java.util.List;
import java.util.Map;

public interface FmlySupotService 
{
	
	public List viewSupotList(Map<String, Object> params);

	Map<String, Object> viewSupotListTotal(Map<String, Object> params);
	
	public List viewfmlySupotExcel(Map<String, Object> params);
	
	public List mappingCustInfo(List params); 
}
