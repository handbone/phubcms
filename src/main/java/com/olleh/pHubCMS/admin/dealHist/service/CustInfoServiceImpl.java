package com.olleh.pHubCMS.admin.dealHist.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.dealHist.dao.CustInfoDao;


/**
 * 고객 정보 Service
 * 
 * @Class Name : CustInfoService
 * @author ojh
 * @since 2018.08.21
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.21   ojh        최초생성
 * 
 */

@Service
public class CustInfoServiceImpl implements CustInfoService{
	@Autowired
	CustInfoDao custInfoDao;
	
	@Override
	public List viewCustInfo(Map<String, Object> params){
		return custInfoDao.viewCustInfo(params);		
	}	
	@Override
	public Map<String, Object> viewCustInfoTotal(Map<String, Object> params){
		return custInfoDao.viewCustInfoTotal(params);
	}
	@Override
	public List viewAgrHist(Map<String, Object> params) {
		return custInfoDao.viewAgrHist(params);
	}
	@Override
	public List viewCustInfoExcel(Map<String, Object> params) {
		return custInfoDao.viewCustInfoExcel(params);	
	}

}
