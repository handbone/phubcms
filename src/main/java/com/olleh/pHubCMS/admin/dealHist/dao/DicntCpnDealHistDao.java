package com.olleh.pHubCMS.admin.dealHist.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * 단말할인권 거래내역 Dao
 * 
 * @Class 	DicntCpnDealHistDao
 * @author	bmg
 * @since 	2019.03.05
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.03.05   bmg        최초생성
 */
@Repository
@SuppressWarnings({ "unchecked", "rawtypes" })
public class DicntCpnDealHistDao extends AbstractDAO {
	
	public Map<String, Object> dicntCpnDealHistTotal(Map<String, Object> params) {
		return selectOne("mybatis.dealHist.dicntCpnDealHistTotal", params);
	}
	
	public List<Map> dicntCpnDealHist(Map<String, Object> params) {
		return selectList("mybatis.dealHist.dicntCpnDealHist", params);
	}
	
	public List<Map> dicntCpnDealHistExcel(Map<String, Object> params) {
		return selectList("mybatis.dealHist.dicntCpnDealHistExcel", params);
	}
}
