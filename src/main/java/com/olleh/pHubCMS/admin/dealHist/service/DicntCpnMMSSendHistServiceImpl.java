package com.olleh.pHubCMS.admin.dealHist.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.dealHist.dao.DicntCpnMMSSendHistDao;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 단말할인권 문자발송이력 Service
 * 
 * @Class 	DicntCpnMMSSendHistServiceImpl
 * @author	bmg
 * @since 	2019.03.05
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.03.05   bmg        최초생성
 */
@Service
@SuppressWarnings({"rawtypes", "unchecked"})
public class DicntCpnMMSSendHistServiceImpl implements DicntCpnMMSSendHistService {

	@Autowired
	DicntCpnMMSSendHistDao dicntCpnMMSSendHistDao;
	
	@Override
	public Map<String, Object> dicntCpnMMSSendHistTotal(Map<String, Object> params) throws Exception {
		return dicntCpnMMSSendHistDao.dicntCpnMMSSendHistTotal(params);
	}

	@Override
	public List<Map> dicntCpnMMSSendHist(Map<String, Object> params) throws Exception {
		return maskingColumn(dicntCpnMMSSendHistDao.dicntCpnMMSSendHist(params));
	}

	@Override
	public List<Map> dicntCpnMMSSendHistExcel(Map<String, Object> params) throws Exception {
		return maskingColumn(dicntCpnMMSSendHistDao.dicntCpnMMSSendHistExcel(params));
	}
	
	@Override
	public List<Map> mmsFormInfo() {
		return dicntCpnMMSSendHistDao.mmsFormInfo();
	}

	/**
	 * 이름&전화번호 마스킹 처리
	 * 
	 * @param params
	 * @return
	 */
	private List maskingColumn(List<Map> params) {
		int index = 0;
		if (params == null) {
			return params;
		}
		for (Map map : params) {
			map.put("send_cust_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("send_cust_nm"))));
			map.put("send_cust_ctn", StringUtil.getMaskingPhone(StringUtil.nvl(map.get("send_cust_ctn"))));
			map.put("recv_cust_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("recv_cust_nm"))));
			map.put("recv_cust_ctn", StringUtil.getMaskingPhone(StringUtil.nvl(map.get("recv_cust_ctn"))));
			params.set(index, map);
			index++;
		}
		return params;
	}
}
