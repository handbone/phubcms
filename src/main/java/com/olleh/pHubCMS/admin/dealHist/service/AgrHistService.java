package com.olleh.pHubCMS.admin.dealHist.service;

import java.util.List;
import java.util.Map;

public interface AgrHistService {
	
	public List viewAgrHist(Map<String, Object> params);
	
	public Map<String, Object> viewAgrHistTotal(Map<String, Object> params);
	
	public List viewAgrHistExcel(Map<String, Object> params);
	
	public List mappingCustInfo(List params);
}
