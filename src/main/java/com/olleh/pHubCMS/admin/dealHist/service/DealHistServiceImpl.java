package com.olleh.pHubCMS.admin.dealHist.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.dealHist.dao.DealHistDao;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 거래 내역 정보 Service
 * 
 * @Class Name : DealHistService
 * @author ojh
 * @since 2018.08.14
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.14   ojh        최초생성
 * 
 */
@Service
@SuppressWarnings("rawtypes")
public class DealHistServiceImpl implements DealHistService{	
	@Autowired
	DealHistDao dealHistDao;
	
	@Override
	public List viewDealHist(Map<String, Object> params){
		return mappingCustInfo(dealHistDao.viewDealHist(params));	
		//return dealHistDao.viewDealHist(params);
	}
	@Override
	public Map<String, Object> viewDealHistTotal(Map<String, Object> params){
		return dealHistDao.viewDealHistTotal(params);	
	}
	@Override
	public List viewDealHistExcel(Map<String, Object> params) {
		return mappingCustInfo(dealHistDao.viewDealHistExcel(params));	
	}
	@Override
	public List viewDealDtl(Map<String, Object> params) {
		return dealHistDao.viewDealDtl(params);
	}
	/**
	 * <pre> 고객정보 마스킹 처리 </pre>
	 * 
	 * @param List params
	 * @return List
	 * @see <pre>
	 *      1. 고객명
	 *      </pre>
	 */		
	@Override
	@SuppressWarnings("unchecked")
	public List mappingCustInfo(List params) {
		List resultList = new ArrayList(params);
		// 고객명 마스킹 처리
		// ex) 홍길*
		for(int i=0; i<resultList.size(); i++){
			Map<String, Object> resultTmp = (Map<String, Object>) resultList.get(i);		
			resultTmp.put("cust_nm", StringUtil.getMaskingName(StringUtil.nvl(resultTmp.get("cust_nm"))));
			resultList.set(i, resultTmp);
		}
		
		return resultList;
	}	
}
