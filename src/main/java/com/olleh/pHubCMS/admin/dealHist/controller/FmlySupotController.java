package com.olleh.pHubCMS.admin.dealHist.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.dealHist.service.FmlySupotService;
import com.olleh.pHubCMS.common.components.CodeManage;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.utils.Paging;

/**
 * 조르기현황 내역 조회  Controller
 * 
 * @Class Name : FmlSupotController
 * @author kimht
 * @since 2019.05.21
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2019.05.21			kimht			최초생성
 * 
 */

@Controller
public class FmlySupotController 
{
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	CodeManage codeManage;
	
	@Autowired
	FmlySupotService fmlySupotService;
	/**
	 * jsp 호출 및 초기값 설정
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/dealHist/viewFmlySupot.do", method = RequestMethod.POST)
	public String viewAgrHist(Locale locale, Model model) {	
		String method = "viewfmlySupot";
		log.debug(method, ">>> start");	
			
		// 쿠폰 상태 가져오기
		model.addAttribute("CPN_STAT_IND", codeManage.getCodeListY("CPN_STAT_IND"));
		// 요청상태코드 가져오기
		model.addAttribute("CPN_REQ_STAT", codeManage.getCodeListY("CPN_REQ_STAT"));
		// 진행상태 가져오기
		model.addAttribute("CPN_REQ_PRGRS", codeManage.getCodeListY("CPN_REQ_PRGRS"));

		
		log.debug(method, ">>>>>> end");	
		return "/dealHist/fmlySupotView";
	}
	
	
	/**
	 * 조르기 내역 리스트 조회
	 * JQGrid 호출
	 * 
	 * 	param page 현재 보고자 하는 페이지 (JQgrid 처리)
	 * @param rows 한 페이지에 보여주고자 설정된 개수 (JQgrid 처리)
	 * @param PHUB_TR_NO		Input  		거래번호
	 * @param CUST_NM			Input 	이름
	 * @param CTN_NO			Input 	할인권번호
	 * @param PRGR_STAT 		SelectBox 	진행상태 
	 * @param REQ_STAT_CD		SelectBox		요청상태코드
	 * @param START_DATE 	선택한 달력 값
	 * @param END_DATE 		선택한 달력 값
	 * @param page 현재 보고자 하는 페이지 (JQgrid 처리)
	 * @param rows 한 페이지에 보여주고자 설정된 개수 (JQgrid 처리)
	 */
	@ResponseBody
	@RequestMapping(value = "/dealHist/jgSupotList.do", method = RequestMethod.POST)
	public Map<String, Object> jgSupotList(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "jgSupotList";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		Map<String, Object> count = new HashMap<String, Object>();
		try{
			//Request param
			int pageNum = Integer.parseInt(req.getParameter("page"));
			int limit = Integer.parseInt(req.getParameter("rows"));	
			log.debug(method, params.toString());
			
			//총 Row 수 계산 		
			count = fmlySupotService.viewSupotListTotal(params);	
			int countRow = Integer.parseInt(count.get("cnt").toString());
			log.debug(method,"count : "+countRow);
			
			if(countRow > 0){
				//HashMap에 페이징 정보 저장					
				Paging.setPageMap(params, map, pageNum, limit, countRow);	
				log.debug(method,map.toString());
				
				//검색 결과 데이터 map 에 추가
				List resultList = new ArrayList();
				log.debug(method,params.toString());
				resultList = fmlySupotService.viewSupotList(params);
				
				map.put("rows", resultList );
			}
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}	
	
	
	/**
	 * 조르기 내역 리스트 엑셀 다운로드
	 * JQGrid 호출
	 * 
	 * 	param page 현재 보고자 하는 페이지 (JQgrid 처리)
	 * @param rows 한 페이지에 보여주고자 설정된 개수 (JQgrid 처리)
	 * @param PHUB_TR_NO		Input  		거래번호
	 * @param CUST_NM			Input 	이름
	 * @param CTN_NO			Input 	할인권번호
	 * @param PRGR_STAT 		SelectBox 	진행상태 
	 * @param REQ_STAT_CD		SelectBox		요청상태코드
	 * @param START_DATE 		선택한 달력 값
	 * @param END_DATE 			선택한 달력 값
	 * @param page				현재 보고자 하는 페이지 (JQgrid 처리)
	 * @param rows 				한 페이지에 보여주고자 설정된 개수 (JQgrid 처리)
	 * @return void response 응답
	 */
	
	@ResponseBody
	@RequestMapping(value = "/dealHist/jgfmlySupotExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgfmlySupotExcel(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "jgfmlySupotExcel";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		Map<String, Object> count = new HashMap<String, Object>();
		try{
			//Request param
			log.debug(method, params.toString());

			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			resultList = fmlySupotService.viewfmlySupotExcel(params);		
			map.put("rows", resultList );
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}	

	
}
