package com.olleh.pHubCMS.admin.dealHist.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import com.olleh.pHubCMS.common.dao.AbstractDAO;
import com.olleh.pHubCMS.common.log.Logger;

/**
 * 고객 정보 Dao
 * @Class Name : CustInfoDao
 * @author : ojh
 * @since : 2018.08.21
 * @version : 1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *      수정일                 수정자                수정내용
 *  ------------  -----------    -------------
 *  2018. 8. 21.      ojh          최초 생성
 * 
 */

@Repository
public class CustInfoDao extends AbstractDAO{
	
	public List viewCustInfo(Map<String, Object> params) {
		return selectList("mybatis.dealHist.custInfo", params);
	}	
	public Map<String, Object> viewCustInfoTotal(Map<String, Object> params) {
		return selectOne("mybatis.dealHist.custInfoTotal", params);
	}	
	public List viewCustInfoExcel(Map<String, Object> params) {
		return selectList("mybatis.dealHist.custInfoExcel", params);
	}	
	public List viewAgrHist(Map<String, Object> params) {
		return selectList("mybatis.dealHist.agrHist", params);
	}
	
	

}
