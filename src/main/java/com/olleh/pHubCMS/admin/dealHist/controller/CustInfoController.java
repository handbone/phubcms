package com.olleh.pHubCMS.admin.dealHist.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.dealHist.service.CustInfoServiceImpl;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.CmnCdVO;
import com.olleh.pHubCMS.common.utils.JsonUtil;
import com.olleh.pHubCMS.common.utils.Paging;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 고객정보  Controller
 * 
 * @Class Name : CustInfoController
 * @author ojh
 * @since 2018.08.21
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.21   ojh        최초생성
 * 
 */


@Controller
public class CustInfoController {
	private Logger log = new Logger(this.getClass());
	
	
	@Autowired
	CustInfoServiceImpl custInfoService;
	
	/**
	 * jsp 호출 및 초기값 설정
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/dealHist/viewCustInfo.do", method = RequestMethod.POST)
	public String viewCustInfo(Locale locale, Model model) {	
		String method = "viewCustInfo";
		log.debug(method, ">>> start");	
		
		//사용 'Y' 등록된 거래 구분 가져오기 
/*		List<CmnCdVO> indList = new ArrayList<CmnCdVO>();
		indList = codeManage.getCodeListY("DEAL_IND");
		model.addAttribute("DEAL_IND", indList.toArray());*/
			
		log.debug(method, ">>>>>> end");	
		return "/dealHist/custInfoView";
	}
	
	/**
	 * JQGrid 호출
	 * 
	 * @param CUST_ID 		Input 아이디
	 * @param CLIP_MMBR_YN 	SELECT 클립멤버여부
	 * @param page 현재 보고자 하는 페이지 (JQgrid 처리)
	 * @param rows 한 페이지에 보여주고자 설정된 개수 (JQgrid 처리)
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/dealHist/jgCustInfo.do", method = RequestMethod.POST)
	public Map<String, Object> jgCustInfo(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "jgCustInfo";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		Map<String, Object> count = new HashMap<String, Object>();
		try{
			//Request param
			int pageNum = Integer.parseInt(req.getParameter("page"));
			int limit = Integer.parseInt(req.getParameter("rows"));	
			log.debug(method, params.toString());
			
			//총 Row 수 계산 	
			count = custInfoService.viewCustInfoTotal(params); 		
			int countRow = Integer.parseInt(count.get("cnt").toString());
			log.debug(method,"count : "+countRow);			
			
			if(countRow > 0){
				//HashMap에 페이징 정보 저장					
				Paging.setPageMap(params, map, pageNum, limit, countRow);	
				log.debug(method,map.toString());
				
				//검색 결과 데이터 map 에 추가
				List resultList = new ArrayList();
				log.debug(method,params.toString());
				resultList = custInfoService.viewCustInfo(params);			
				mappingCustInfo(resultList);
				map.put("rows", resultList );
			}
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}

		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * JQGrid 호출 (엑셀용 전체데이터)
	 * 
	 * @param CUST_ID 		Input 아이디
	 * @param CLIP_MMBR_YN 	SELECT 클립멤버여부
	 * 
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/dealHist/jgCustInfoExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgCustInfoExcel(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "jgCustInfoExcel";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();		
		List resultList = new ArrayList();
		Map<String, Object> count = new HashMap<String, Object>();
		
		try{
			//Request param
			log.debug(method, params.toString());
			resultList = custInfoService.viewCustInfoExcel(params);	
			mappingCustInfo(resultList);
			int countRow = resultList.size();
			log.debug(method, "COUNT : "+countRow);	
			map.put("rows", resultList );
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}
		
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * 약관동의내역 조회
	 * 
	 * @param CUST_ID 회원아이디
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/dealHist/ajaxAgrHist.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxAgrHist(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "ajaxAgrHist";
		log.debug(method, ">>> start");	
		
		HashMap map = new HashMap();		
		try{
			//Request param		
			log.debug(method, "CUST_ID : "+params.get("CUST_ID"));	
			//상세 거래내역 가져오기 		
			List resultList = new ArrayList();
			resultList = custInfoService.viewAgrHist(params);
			log.debug(method, resultList.toString());	
			
			if(resultList!=null && resultList.size() > 0){			
				map.put("rows", resultList );
			}
			map.put("eCode", "0");
			
		} catch (Exception e) {
			map.put("eCode", "-1");
			log.error(method, "Exception : "+e.getMessage());
		}
		
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * 마스킹 처리
	 * 
	 * @param List 고객 정보 리스트
	 * @return void 
	 */
	public void mappingCustInfo(List resultList){
		
		//핸드폰 마스킹 처리
		for(int i=0; i<resultList.size(); i++){
			Map<String, Object> resultTmp = (Map<String, Object>) resultList.get(i);		
			resultTmp.put("cust_ctn", StringUtil.getMaskingPhone(StringUtil.nvl(resultTmp.get("cust_ctn"))));
			resultTmp.put("cust_nm", StringUtil.getMaskingName(StringUtil.nvl(resultTmp.get("cust_nm"))));
			resultList.set(i, resultTmp);
		}	
		
	}

}
