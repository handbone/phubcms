package com.olleh.pHubCMS.admin.dealHist.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.dealHist.dao.AgrHistDao;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 약관 동의내역 Service
 * 
 * @Class Name : AgrHistService
 * @author ojh
 * @since 2018.09.28
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.28   ojh        최초생성
 * 
 */

@Service
public class AgrHistServiceImpl implements AgrHistService {
	
	@Autowired
	AgrHistDao agrHistDao;
	
	@Override
	public List viewAgrHist(Map<String, Object> params) {
		return mappingCustInfo(agrHistDao.viewAgrHist(params));
	}	
	
	@Override
	public Map<String, Object> viewAgrHistTotal(Map<String, Object> params) {
		return agrHistDao.viewAgrHistTotal(params);
	}	
	
	@Override
	public List viewAgrHistExcel(Map<String, Object> params) {
		return mappingCustInfo(agrHistDao.viewAgrHistExcel(params));
	}	

	/**
	 * <pre> 고객정보 마스킹 처리 </pre>
	 * 
	 * @param List params
	 * @return List
	 * @see <pre>
	 *      1. 고객전화번호
	 *      2. 고객명
	 *      </pre>
	 */	
	@Override
	public List mappingCustInfo(List params) {
		List resultList = new ArrayList(params);
		// 고객명, 전화번호 마스킹 처리
		// ex) 홍길*, 01091***822
		for(int i=0; i<resultList.size(); i++){
			Map<String, Object> resultTmp = (Map<String, Object>) resultList.get(i);
			resultTmp.put("cust_ctn", StringUtil.getMaskingPhone(StringUtil.nvl(resultTmp.get("cust_ctn"))));
			resultTmp.put("cust_nm", StringUtil.getMaskingName(StringUtil.nvl(resultTmp.get("cust_nm"))));
			resultList.set(i, resultTmp);
		}
		
		return resultList;
	}	
}
