package com.olleh.pHubCMS.admin.dealHist.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * 거래내역 Dao
 * @Class Name : DealHistDao
 * @author : ojh
 * @since : 2018.08.14
 * @version : 1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *      수정일                 수정자                수정내용
 *  ------------  -----------    -------------
 *  2018. 8. 14.      ojh          최초 생성
 * 
 */

@Repository
@SuppressWarnings({"rawtypes", "unchecked"})
public class DealHistDao extends AbstractDAO {
	
	public List viewDealHist(Map<String, Object> params) {
		return selectList("mybatis.dealHist.dealHist", params);
	}	
	public Map<String, Object> viewDealHistTotal(Map<String, Object> params) {
		return selectOne("mybatis.dealHist.dealHistTotal", params);
	}	
	public List viewDealHistExcel(Map<String, Object> params) {
		return selectList("mybatis.dealHist.dealHistExcel", params);
	}	
	public List viewDealDtl(Map<String, Object> params) {
		return selectList("mybatis.dealHist.dealDtl", params);
	}

}
