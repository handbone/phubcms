package com.olleh.pHubCMS.admin.dealHist.controller;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.cmpn.service.PntPgCmpnServiceImpl;
import com.olleh.pHubCMS.admin.cmpn.service.PntUseCmpnServiceImpl;
import com.olleh.pHubCMS.admin.dealHist.service.DicntCpnDealHistService;
import com.olleh.pHubCMS.api.service.PHHttpService;
import com.olleh.pHubCMS.common.components.CodeManage;
import com.olleh.pHubCMS.common.components.Constant;
import com.olleh.pHubCMS.common.components.SysPrmtManage;
import com.olleh.pHubCMS.common.exception.UserException;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.ResultVo;
import com.olleh.pHubCMS.common.utils.JsonUtil;
import com.olleh.pHubCMS.common.utils.Paging;
import com.olleh.pHubCMS.common.utils.SessionUtils;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 단말할인권 거래내역 Controller
 * 
 * @Class 	DicntCpnDealHistController
 * @author	bmg
 * @since 	2019.03.05
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.03.05   bmg        최초생성
 */
@Controller
@SuppressWarnings({"rawtypes", "unchecked"})
public class DicntCpnDealHistController {

	private static final SimpleDateFormat S_DATE_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");
	
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	CodeManage codeManage;
	
	@Autowired
	PntPgCmpnServiceImpl pntPgCmpnService;
	
	@Autowired
	PntUseCmpnServiceImpl pntUseCmpnService;
	
	@Autowired
	DicntCpnDealHistService dicntCpnDealHistService;
	
	@Autowired
    SysPrmtManage sysPrmtManage;
	
	@Autowired
    PHHttpService pHHttpService;

	/**
	 * jsp 호출 및 초기값 설정
	 *  
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/dealHist/viewDicntCpnDealHist.do", method = RequestMethod.POST)
	public String viewDicntCpnDealHist(Locale locale, Model model) {	
		String method = "viewDicntCpnDealHist";
		log.debug(method, ">>> start");	
		
		// 사용 'Y' 등록된 거래 구분 가져오기
		model.addAttribute("DEAL_IND", codeManage.getCodeListY("DEAL_IND"));
		
		// PG사 가져오기
		model.addAttribute("PG_CMPN", pntPgCmpnService.getAllPgCmpn());
		
		// 쿠폰 상태 가져오기
		model.addAttribute("CPN_STAT_IND", codeManage.getCodeListY("CPN_STAT_IND"));
		
		//가맹점 가져오기
		List cmpnList = new ArrayList();		
		cmpnList = pntUseCmpnService.getAllPntUseCmpn();
		model.addAttribute("USE_CMPN", cmpnList);	
		
		log.debug(method, ">>>>>> end");	
		return "/dealHist/dicntCpnDealHistView";
	}
	
	/**
	 * JQGrid 단말할인권 거래내역 호출
	 * 
	 * @param params
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/dealHist/jgDicntCpnDealHist.do", method = RequestMethod.POST)
	public Map<String, Object> jgDicntCpnDealHist(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgDicntCpnDealHist";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		Map<String, Object> count = new HashMap<String, Object>();

		//Request param
		int pageNum = Integer.parseInt(request.getParameter("page"));
		int limit = Integer.parseInt(request.getParameter("rows"));	
		log.debug(method, params.toString());
		
		//총 Row 수 계산 		
		count = dicntCpnDealHistService.dicntCpnDealHistTotal(params);
		int countRow = Integer.parseInt(count.get("cnt").toString());
		log.debug(method,"count : "+countRow);
		
		if(countRow > 0){
			//HashMap에 페이징 정보 저장					
			Paging.setPageMap(params, map, pageNum, limit, countRow);	
			log.debug(method,map.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			log.debug(method,params.toString());
			resultList = dicntCpnDealHistService.dicntCpnDealHist(params);					
			
			map.put("rows", resultList );
		}
		
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * JQGrid 단말할인권 거래내역 호출 (엑셀용 전체데이터)
	 * 
	 * @param params
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/dealHist/jgDicntCpnDealHistExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgDicntCpnDealHistExcel(@RequestParam Map<String, Object> params) throws IOException {
		String method = "jgDicntCpnDealHistExcel";
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		//Request param
		log.debug(method, params.toString());		
				
		List resultList = new ArrayList();
		resultList = dicntCpnDealHistService.dicntCpnDealHistExcel(params);
		log.debug(method, "COUNT : "+resultList.size());	
		map.put("rows", resultList );
		
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * 발행/사용 취소
	 * - A01: 발행취소
	 * - A02: 사용취소
	 * 
	 * @param params
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/dealHist/ajaxDicntCpnCancel.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxDicntCpnCancel(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "ajaxDicntCpnCancel";
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		ResultVo resultVo = new ResultVo();
		try{
			//caller setting
			resultVo.setCaller(getClass().getSimpleName() +"."+ method);
			
			//Request param
			params.put("mdcode", sysPrmtManage.getSysPrmtVal("GS_MDCODE"));	//매체코드
			params.put("trade_type", "A01");								//거래타입 고정 'A01'(CMS핀상태변경)
			params.put("trade_date", S_DATE_FORMAT.format(new Date()));		//사용취소일시
			params.put("user_id",  SessionUtils.getUserId(request));		//요청자ID
			log.debug(method, params.toString());
			resultVo.setReqBody(params);
			
			// 1. 헤더 정의
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(new MediaType(MediaType.APPLICATION_FORM_URLENCODED, StandardCharsets.UTF_8));
			
			// 2. 포인트허브 통신
			resultVo = pHHttpService.sendHttpPostCustom("/fp/giftishow/chgCpnStatus", headers, resultVo);
			if (!resultVo.getSucYn().equals("Y")) {
				throw new UserException(getClass().getName(), method, "2", "9001", "포인트허브 연동에 실패하였습니다");
			}
			Map<String, Object> response = JsonUtil.JsonToMap(StringUtil.nvl(resultVo.getRstBody()));
			// 3. 성공이 아닐경우 결과메시지 리턴
			if (!StringUtil.nvl(response.get("resCode")).equals("1000")) {
				throw new UserException(getClass().getName(), method, "2", StringUtil.nvl(response.get("resCode")), StringUtil.nvl(response.get("resMsg")));
			}
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (UserException ue) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			map.put(Constant.RET_MSG, ue.getErrorInfo().getErrMsg());
			log.error(method, "UserException : "+ue.getErrorInfo().getErrMsg());
		} catch (Exception e) {
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
}
