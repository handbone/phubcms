package com.olleh.pHubCMS.admin.statistic.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.cmpn.service.PntUseCmpnService;
import com.olleh.pHubCMS.admin.statistic.service.UseStepStatService;
import com.olleh.pHubCMS.common.components.CodeManage;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.utils.Paging;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 결제단계별 이용건수 통계 Controller
 * 
 * @Class Name : UseStepStatController
 * @author ojh
 * @since 2018.11.14
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.11.14    ojh        최초생성
 * 
 */
@Controller
public class UseStepStatController {
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	CodeManage codeManage;
	
	@Autowired
	PntUseCmpnService pntUseCmpnService;
	
	@Autowired
	UseStepStatService useStepStatService;
	
	/**
	 * 결제단계별 이용건수 통계	 *
	 * jsp 호출 및 초기값 설정
	 * 
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/statistic/viewUseStepStat.do", method = RequestMethod.POST)
	public String viewUseStepStat(Locale locale, Model model) {	
		String method = "viewUseStepStat";
		log.debug(method, ">>> start");	

		//사용처 가져오기
		List cmpnList = new ArrayList();		
		cmpnList = pntUseCmpnService.getAllPntUseCmpn();
		model.addAttribute("USE_CMPN", cmpnList);
		
		// 포인트허브 서비스 유형
		model.addAttribute("PH_SVC_CD", codeManage.getCodeListY("PH_SVC_CD"));
		
		log.debug(method, ">>>>>> end");	
		return "/statistic/useStepStatView";
	}
	
	/**
	 * 결제단계별 이용건수 통계
	 * JQGrid 호출
	 * 
	 * @param SRCH_IND		일월 구분
	 * @param CPRT_CMPN_ID  사용처 ID
	 * @param START_DATE 	선택한 달력 값
	 * @param END_DATE 		선택한 달력 값
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/statistic/jgUseStepStat.do", method = RequestMethod.POST)
	public Map<String, Object> jgUseStepStat(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgUseStepStat";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		int pageNum = Integer.parseInt(request.getParameter("page"));
		int limit = Integer.parseInt(request.getParameter("rows"));	
		log.debug(method, params.toString()); 
		
		String srchInd = StringUtil.nvl(params.get("SRCH_IND"));
		if(srchInd.equals("D")){
			count = useStepStatService.viewUseStepStatDayTotal(params);
		} else if(srchInd.equals("M")){
			count = useStepStatService.viewUseStepStatMonthTotal(params);
		}	
		int countRow = 0;
		if(count != null && count.size() > 0){
			countRow = Integer.parseInt(count.get("cnt").toString());
		}
		
		HashMap map = new HashMap();
		if (countRow > 0) {
			//HashMap에 페이징 정보 저장					
			Paging.setPageMap(params, map, pageNum, limit, countRow);	
			log.debug(method,map.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			log.debug(method,params.toString());
			
			if(srchInd.equals("D")){
				resultList = useStepStatService.viewUseStepStatDay(params);
			} else if(srchInd.equals("M")){
				resultList = useStepStatService.viewUseStepStatMonth(params);
			}			
			map.put("rows", resultList);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 결제단계별 이용건수 통계 (엑셀다운로드)
	 * JQGrid 호출
	 * 
	 * @param SRCH_IND		일월 구분
	 * @param CPRT_CMPN_ID  사용처 ID
	 * @param START_DATE 	선택한 달력 값
	 * @param END_DATE 		선택한 달력 값
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/statistic/jgUseStepStatExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgUseStepStatExcel(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgUseStepStatExcel";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		log.debug(method, params.toString()); 
		
		String srchInd = StringUtil.nvl(params.get("SRCH_IND"));
		if(srchInd.equals("D")){
			count = useStepStatService.viewUseStepStatDayTotal(params);
		} else if(srchInd.equals("M")){
			count = useStepStatService.viewUseStepStatMonthTotal(params);
		}	
		
		HashMap map = new HashMap();
		//검색 결과 데이터 map 에 추가
		List resultList = new ArrayList();
		
		if(srchInd.equals("D")){
			resultList = useStepStatService.viewUseStepStatDayExcel(params);
		} else if(srchInd.equals("M")){
			resultList = useStepStatService.viewUseStepStatMonthExcel(params);
		}			
		map.put("rows", resultList);
		log.debug(method, ">>>>>> end");
		return map;
	}
}
