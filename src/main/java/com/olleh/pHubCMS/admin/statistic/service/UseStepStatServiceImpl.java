package com.olleh.pHubCMS.admin.statistic.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.statistic.dao.UseStepStatDao;



@Service
public class UseStepStatServiceImpl implements UseStepStatService{	
	
	@Autowired
	UseStepStatDao useStepStatDao;
	public List viewUseStepStatDay(Map<String, Object> params){
		return useStepStatDao.viewUseStepStatDay(params);
	}
	public Map<String, Object> viewUseStepStatDayTotal(Map<String, Object> params){
		return useStepStatDao.viewUseStepStatDayTotal(params);
	}
	public List viewUseStepStatDayExcel(Map<String, Object> params){
		return useStepStatDao.viewUseStepStatDayExcel(params);
	}	
	
	public List viewUseStepStatMonth(Map<String, Object> params){
		return useStepStatDao.viewUseStepStatMonth(params);
	}
	public Map<String, Object> viewUseStepStatMonthTotal(Map<String, Object> params){
		return useStepStatDao.viewUseStepStatMonthTotal(params);		
	}
	public List viewUseStepStatMonthExcel(Map<String, Object> params){
		return useStepStatDao.viewUseStepStatMonthExcel(params);
	}

}
