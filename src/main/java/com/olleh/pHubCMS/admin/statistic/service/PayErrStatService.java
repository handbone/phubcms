package com.olleh.pHubCMS.admin.statistic.service;

import java.util.List;
import java.util.Map;

public interface PayErrStatService {
	
	public List viewPayErrCprtStatRank(Map<String, Object> params);
	
	public List viewPayErrCprtStatDay(Map<String, Object> params);
	public Map<String, Object> viewPayErrCprtStatDayTotal(Map<String, Object> params);
	public List viewPayErrCprtStatDayExcel(Map<String, Object> params);
	
	public List viewPayErrCprtStatMonth(Map<String, Object> params);
	public Map<String, Object> viewPayErrCprtStatMonthTotal(Map<String, Object> params);
	public List viewPayErrCprtStatMonthExcel(Map<String, Object> params);
	
	public List viewPayErrPntStatRank(Map<String, Object> params);
	
	public List viewPayErrPntStatDay(Map<String, Object> params);
	public Map<String, Object> viewPayErrPntStatDayTotal(Map<String, Object> params);
	public List viewPayErrPntStatDayExcel(Map<String, Object> params);
	
	public List viewPayErrPntStatMonth(Map<String, Object> params);
	public Map<String, Object> viewPayErrPntStatMonthTotal(Map<String, Object> params);
	public List viewPayErrPntStatMonthExcel(Map<String, Object> params);

}
