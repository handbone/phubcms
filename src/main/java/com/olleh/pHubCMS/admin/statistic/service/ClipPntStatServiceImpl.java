package com.olleh.pHubCMS.admin.statistic.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.statistic.dao.ClipPntStatDao;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 클립포인트 대사 통계 Service
 * 
 * @Class Name : ClipPntStatServiceImpl
 * @author bmg
 * @since 2018.10.05
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.05   bmg        최초생성
 * 
 */
@Service
public class ClipPntStatServiceImpl implements ClipPntStatService {

	@Autowired
	ClipPntStatDao clipPntStatDao;
	
	@Override
	public Map<String, Object> viewClipPntStatDayTotal(Map<String, Object> params) {
		params.put("START_DATE", StringUtil.nvl(params.get("START_DATE")).replaceAll("-", ""));
		params.put("END_DATE", StringUtil.nvl(params.get("END_DATE")).replaceAll("-", ""));
		return clipPntStatDao.viewClipPntStatDayTotal(params);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List viewClipPntStatDay(Map<String, Object> params) {
		params.put("START_DATE", StringUtil.nvl(params.get("START_DATE")).replaceAll("-", ""));
		params.put("END_DATE", StringUtil.nvl(params.get("END_DATE")).replaceAll("-", ""));
		return clipPntStatDao.viewClipPntStatDay(params);
	}
	
	@Override
	@SuppressWarnings("rawtypes")
	public List viewClipPntStatDayExcel(Map<String, Object> params) {
		params.put("START_DATE", StringUtil.nvl(params.get("START_DATE")).replaceAll("-", ""));
		params.put("END_DATE", StringUtil.nvl(params.get("END_DATE")).replaceAll("-", ""));
		return clipPntStatDao.viewClipPntStatDayExcel(params);
	}
	
	@Override
	@SuppressWarnings("rawtypes")
	public List clipPntStatDayFailDtl(Map<String, Object> params) {
		return clipPntStatDao.clipPntStatDayFailDtl(params);
	}

	@Override
	public Map<String, Object> viewClipPntStatMonthTotal(Map<String, Object> params) {
		return clipPntStatDao.viewClipPntStatMonthTotal(params);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List viewClipPntStatMonth(Map<String, Object> params) {
		return clipPntStatDao.viewClipPntStatMonth(params);
	}
	
	@Override
	@SuppressWarnings("rawtypes")
	public List viewClipPntStatMonthExcel(Map<String, Object> params) {
		return clipPntStatDao.viewClipPntStatMonthExcel(params);
	}
	
	@Override
	@SuppressWarnings("rawtypes")
	public List clipPntStatMonthFailDtl(Map<String, Object> params) {
		return clipPntStatDao.clipPntStatMonthFailDtl(params);
	}
}
