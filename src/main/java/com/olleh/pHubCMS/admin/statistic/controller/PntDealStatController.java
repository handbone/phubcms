package com.olleh.pHubCMS.admin.statistic.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.cmpn.service.PntPgCmpnService;
import com.olleh.pHubCMS.admin.cmpn.service.PntUseCmpnService;
import com.olleh.pHubCMS.admin.statistic.service.PntDealStatService;
import com.olleh.pHubCMS.common.components.CodeManage;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.utils.Paging;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 포인트거래 통계 Controller
 * 
 * @Class Name : PntDealStatController
 * @author ojh
 * @since 2018.10.10
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.10    ojh        최초생성
 * 
 */
@Controller
public class PntDealStatController {
	
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	CodeManage codeManage;
	
	@Autowired
	PntDealStatService pntDealStatService;
	
	@Autowired
	PntPgCmpnService pntPgCmpnService;
	
	@Autowired
	PntUseCmpnService pntUseCmpnService;
	
	/**
	 * 포인트거래 일별 통계
	 * jsp 호출 및 초기값 설정
	 * 
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/statistic/viewPntDealStatD.do", method = RequestMethod.POST)
	public String viewPntDealStatD(Locale locale, Model model) {	
		String method = "viewPntDealStatD";
		log.debug(method, ">>> start");	
		
		// 포인트허브 서비스 유형
		model.addAttribute("PH_SVC_CD", codeManage.getCodeListY("PH_SVC_CD"));
		
		log.debug(method, ">>>>>> end");	
		return "/statistic/pntDealStatDView";
	}
	
	/**
	 * 포인트거래 일별 통계 
	 * JQGrid 호출
	 * 
	 * @param START_DATE 	선택한 달력 값
	 * @param END_DATE 		선택한 달력 값
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/statistic/jgPntDealStatD.do", method = RequestMethod.POST)
	public Map<String, Object> jgPntDealStatD(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgPntDealStatD";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		int pageNum = Integer.parseInt(request.getParameter("page"));
		int limit = Integer.parseInt(request.getParameter("rows"));	
		log.debug(method, params.toString()); 
		
		String srchInd = StringUtil.nvl(params.get("SRCH_IND"));
		if(srchInd.equals("PG")){
			count = pntDealStatService.viewPntDealStatDayTotalPG(params);
		} else if(srchInd.equals("CD")){
			count = pntDealStatService.viewPntDealStatDayTotal(params);
		}	
		int countRow = Integer.parseInt(count.get("cnt").toString());
		
		HashMap map = new HashMap();
		if (countRow > 0) {
			//HashMap에 페이징 정보 저장					
			Paging.setPageMap(params, map, pageNum, limit, countRow);	
			log.debug(method,map.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			log.debug(method,params.toString());
			
			if(srchInd.equals("PG")){
				resultList = pntDealStatService.viewPntDealStatDayPG(params);
			} else if(srchInd.equals("CD")){
				resultList = pntDealStatService.viewPntDealStatDay(params);
			}			
			map.put("rows", resultList);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}

	
	/**
	 * 포인트거래 일별 통계 
	 * JQGrid 호출 (엑셀용 전체데이터)
	 * 
	 * @param START_DATE 	선택한 달력 값
	 * @param END_DATE 		선택한 달력 값
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/statistic/jgPntDealStatDExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgPntDealStatDExcel(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgPntDealStatDExcel";
		log.debug(method, ">>> start");	
		//Request param
		log.debug(method, params.toString());		
		
		HashMap map = new HashMap();		
		//검색 결과 데이터 map 에 추가
		List resultList = new ArrayList();
		
		String srchInd = StringUtil.nvl(params.get("SRCH_IND"));
		if(srchInd.equals("PG")){
			resultList = pntDealStatService.viewPntDealStatDayExcelPG(params);
		} else if(srchInd.equals("CD")){
			resultList = pntDealStatService.viewPntDealStatDayExcel(params);
		}			
		map.put("rows", resultList);
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	
	/**
	 * 포인트거래 월별 통계
	 * jsp 호출 및 초기값 설정
	 * 
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/statistic/viewPntDealStatM.do", method = RequestMethod.POST)
	public String viewPntDealStatM(Locale locale, Model model) {	
		String method = "viewPntDealStatM";
		log.debug(method, ">>> start");	
		
		// 포인트허브 서비스 유형
		model.addAttribute("PH_SVC_CD", codeManage.getCodeListY("PH_SVC_CD"));
		
		log.debug(method, ">>>>>> end");	
		return "/statistic/pntDealStatMView";
	}
	
	/**
	 * 포인트거래 월별 통계 
	 * JQGrid 호출
	 * 
	 * @param START_DATE 	선택한 달력 값
	 * @param END_DATE 		선택한 달력 값
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/statistic/jgPntDealStatM.do", method = RequestMethod.POST)
	public Map<String, Object> jgPntDealStatM(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgPntDealStatM";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		int pageNum = Integer.parseInt(request.getParameter("page"));
		int limit = Integer.parseInt(request.getParameter("rows"));	
		log.debug(method, params.toString()); 
		
		String srchInd = StringUtil.nvl(params.get("SRCH_IND"));
		if(srchInd.equals("PG")){
			count = pntDealStatService.viewPntDealStatMonthTotalPG(params);
		} else if(srchInd.equals("CD")){
			count = pntDealStatService.viewPntDealStatMonthTotal(params);
		}	
		
		int countRow = Integer.parseInt(count.get("cnt").toString());
		
		HashMap map = new HashMap();
		if (countRow > 0) {
			//HashMap에 페이징 정보 저장					
			Paging.setPageMap(params, map, pageNum, limit, countRow);	
			log.debug(method,map.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			log.debug(method,params.toString());
			if(srchInd.equals("PG")){
				resultList = pntDealStatService.viewPntDealStatMonthPG(params);
			} else if(srchInd.equals("CD")){
				resultList = pntDealStatService.viewPntDealStatMonth(params);
			}	
			
			map.put("rows", resultList);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	
	/**
	 * 포인트거래 월별 통계 
	 * JQGrid 호출 (엑셀용 전체데이터)
	 * 
	 * @param START_DATE 	선택한 달력 값
	 * @param END_DATE 		선택한 달력 값
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/statistic/jgPntDealStatMExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgPntDealStatMExcel(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgPntDealStatMExcel";
		log.debug(method, ">>> start");	
		//Request param
		log.debug(method, params.toString());		
		
		HashMap map = new HashMap();		
		//검색 결과 데이터 map 에 추가
		List resultList = new ArrayList();
		
		String srchInd = StringUtil.nvl(params.get("SRCH_IND"));
		if(srchInd.equals("PG")){
			resultList = pntDealStatService.viewPntDealStatMonthExcelPG(params);
		} else if(srchInd.equals("CD")){
			resultList = pntDealStatService.viewPntDealStatMonthExcel(params);
		}		
		map.put("rows", resultList);
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 포인트거래 사용처별 일별 통계
	 * jsp 호출 및 초기값 설정
	 * 
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/statistic/viewPntDealCmpnStatD.do", method = RequestMethod.POST)
	public String viewPntDealCmpnStatD(Locale locale, Model model) {	
		String method = "viewPntDealCmpnStatD";
		log.debug(method, ">>> start");	
		
		//PG사 가져오기
		List pgList = new ArrayList();
		pgList = pntPgCmpnService.getAllPgCmpn();
		model.addAttribute("PG_CMPN", pgList);
		
		//사용처 가져오기
		List cmpnList = new ArrayList();		
		cmpnList = pntUseCmpnService.getAllPntUseCmpn();
		model.addAttribute("USE_CMPN", cmpnList);
		
		// 포인트허브 서비스 유형
		model.addAttribute("PH_SVC_CD", codeManage.getCodeListY("PH_SVC_CD"));
		
		log.debug(method, ">>>>>> end");	
		return "/statistic/pntDealCmpnStatDView";
	}
	
	/**
	 * 포인트거래 사용처별 일별 통계 
	 * JQGrid 호출	 
	 * 
	 * @param START_DATE 	선택한 달력 값
	 * @param END_DATE 		선택한 달력 값
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/statistic/jgPntDealCmpnStatD.do", method = RequestMethod.POST)
	public Map<String, Object> jgPntDealCmpnStatD(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgPntDealCmpnStatD";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		int pageNum = Integer.parseInt(request.getParameter("page"));
		int limit = Integer.parseInt(request.getParameter("rows"));	
		log.debug(method, params.toString()); 
		
		count = pntDealStatService.viewPntDealCmpnStatDayTotalPG(params);
		int countRow = Integer.parseInt(count.get("cnt").toString());
		
		HashMap map = new HashMap();
		if (countRow > 0) {
			//HashMap에 페이징 정보 저장					
			Paging.setPageMap(params, map, pageNum, limit, countRow);	
			log.debug(method,map.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			log.debug(method,params.toString());
			resultList = pntDealStatService.viewPntDealCmpnStatDayPG(params);
			
			map.put("rows", resultList);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 포인트거래 사용처별 일별 통계 
	 * JQGrid 호출 (엑셀용 전체데이터)
	 * 
	 * @param START_DATE 	선택한 달력 값
	 * @param END_DATE 		선택한 달력 값
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/statistic/jgPntDealCmpnStatDExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgPntDealCmpnStatDExcel(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgPntDealCmpnStatDExcel";
		log.debug(method, ">>> start");	
		//Request param
		log.debug(method, params.toString());		
		
		HashMap map = new HashMap();		
		//검색 결과 데이터 map 에 추가
		List resultList = new ArrayList();
		resultList = pntDealStatService.viewPntDealCmpnStatDayExcelPG(params);		
		
		map.put("rows", resultList);
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 포인트거래 사용처별 월별 통계
	 * jsp 호출 및 초기값 설정
	 * 
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/statistic/viewPntDealCmpnStatM.do", method = RequestMethod.POST)
	public String viewPntDealCmpnStatM(Locale locale, Model model) {	
		String method = "viewPntDealCmpnStatM";
		log.debug(method, ">>> start");	
		
		//PG사 가져오기
		List pgList = new ArrayList();
		pgList = pntPgCmpnService.getAllPgCmpn();
		model.addAttribute("PG_CMPN", pgList);
		
		//사용처 가져오기
		List cmpnList = new ArrayList();		
		cmpnList = pntUseCmpnService.getAllPntUseCmpn();
		model.addAttribute("USE_CMPN", cmpnList);
		
		// 포인트허브 서비스 유형
		model.addAttribute("PH_SVC_CD", codeManage.getCodeListY("PH_SVC_CD"));
		
		log.debug(method, ">>>>>> end");	
		return "/statistic/pntDealCmpnStatMView";
	}
	
	/**
	 * 포인트거래 사용처별 월별 통계 
	 * JQGrid 호출	 
	 * 
	 * @param START_DATE 	선택한 달력 값
	 * @param END_DATE 		선택한 달력 값
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/statistic/jgPntDealCmpnStatM.do", method = RequestMethod.POST)
	public Map<String, Object> jgPntDealCmpnStatM(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgPntDealCmpnStatM";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		int pageNum = Integer.parseInt(request.getParameter("page"));
		int limit = Integer.parseInt(request.getParameter("rows"));	
		log.debug(method, params.toString());		
		
		count = pntDealStatService.viewPntDealCmpnStatMonthTotalPG(params);		
		int countRow = Integer.parseInt(count.get("cnt").toString());
		
		HashMap map = new HashMap();
		if (countRow > 0) {
			//HashMap에 페이징 정보 저장					
			Paging.setPageMap(params, map, pageNum, limit, countRow);	
			log.debug(method,map.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			log.debug(method,params.toString());
			resultList = pntDealStatService.viewPntDealCmpnStatMonthPG(params);
			
			map.put("rows", resultList);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 포인트거래 사용처별 월별 통계 
	 * JQGrid 호출 (엑셀용 전체데이터)
	 * 
	 * @param START_DATE 	선택한 달력 값
	 * @param END_DATE 		선택한 달력 값
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/statistic/jgPntDealCmpnStatMExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgPntDealCmpnStatMExcel(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgPntDealCmpnStatMExcel";
		log.debug(method, ">>> start");	
		//Request param
		log.debug(method, params.toString());		
		
		HashMap map = new HashMap();		
		//검색 결과 데이터 map 에 추가
		List resultList = new ArrayList();
		resultList = pntDealStatService.viewPntDealCmpnStatMonthExcelPG(params);
		
		map.put("rows", resultList);
		log.debug(method, ">>>>>> end");
		return map;
	}

}
