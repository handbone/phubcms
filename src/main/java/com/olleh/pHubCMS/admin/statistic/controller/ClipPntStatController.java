package com.olleh.pHubCMS.admin.statistic.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.statistic.service.ClipPntStatService;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.utils.Paging;

/**
 * 클립포인트 대사 통계 Controller
 * 
 * @Class Name : ClipPntStatController
 * @author bmg
 * @since 2018.10.05
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.05   bmg        최초생성
 * 
 */
@Controller
public class ClipPntStatController {
	private Logger log = new Logger(this.getClass());

	@Autowired
	ClipPntStatService clipPntStatService;
	
	/**
	 * 클립포인트 대사 일별 통계 jsp 호출 및 초기값 설정
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/statistic/viewClipPntStatD.do", method = RequestMethod.POST)
	public String viewClipPntStatD(Locale locale, Model model) {	
		String method = "viewClipPntStatD";
		log.debug(method, ">>> start");	
		
		log.debug(method, ">>>>>> end");	
		return "/statistic/clipPntStatDView";
	}
	
	/**
	 * 클립포인트 대사 월별 통계 jsp 호출 및 초기값 설정
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/statistic/viewClipPntStatM.do", method = RequestMethod.POST)
	public String viewClipPntStatM(Locale locale, Model model) {	
		String method = "viewClipPntStatM";
		log.debug(method, ">>> start");	
		
		log.debug(method, ">>>>>> end");	
		return "/statistic/clipPntStatMView";
	}
	
	/**
	 * 클립포인트 대사 일별 통계 JQGrid 호출
	 * 
	 * @param params
	 * @param req
	 * @param res
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/statistic/jgClipPntStatDay.do", method = RequestMethod.POST)
	public Map<String, Object> jgClipPntStatDay(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgClipPntStatDay";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		int pageNum = Integer.parseInt(request.getParameter("page"));
		int limit = Integer.parseInt(request.getParameter("rows"));	
		log.debug(method,params.toString());
		
		count = clipPntStatService.viewClipPntStatDayTotal(params);
		int countRow = Integer.parseInt(count.get("cnt").toString());
		
		HashMap map = new HashMap();
		if (countRow > 0) {
			//HashMap에 페이징 정보 저장					
			Paging.setPageMap(params, map, pageNum, limit, countRow);	
			log.debug(method,map.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			log.debug(method,params.toString());
			resultList = clipPntStatService.viewClipPntStatDay(params);
			
			map.put("rows", resultList);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 클립포인트 대사 월별 통계 JQGrid 호출
	 * 
	 * @param params
	 * @param req
	 * @param res
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/statistic/jgClipPntStatMonth.do", method = RequestMethod.POST)
	public Map<String, Object> jgClipPntStatMonth(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgClipPntStatMonth";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		int pageNum = Integer.parseInt(request.getParameter("page"));
		int limit = Integer.parseInt(request.getParameter("rows"));	
		log.debug(method,params.toString());
		
		count = clipPntStatService.viewClipPntStatMonthTotal(params);
		int countRow = Integer.parseInt(count.get("cnt").toString());
		
		HashMap map = new HashMap();
		if (countRow > 0) {
			//HashMap에 페이징 정보 저장					
			Paging.setPageMap(params, map, pageNum, limit, countRow);	
			log.debug(method,map.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			log.debug(method,params.toString());
			resultList = clipPntStatService.viewClipPntStatMonth(params);
			
			map.put("rows", resultList);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 클립포인트 대사 일별 통계 JQGrid 호출 엑셀
	 * 
	 * @param params
	 * @param req
	 * @param res
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/statistic/jgClipPntStatDayExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgClipPntStatDayExcel(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgClipPntStatDayExcel";
		log.debug(method, ">>> start");			
		HashMap map = new HashMap();
		try{
			//Request param
			log.debug(method,params.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			resultList = clipPntStatService.viewClipPntStatDayExcel(params);
			
			map.put("rows", resultList );
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * 클립포인트 대사 월별 통계 JQGrid 호출 엑셀
	 * 
	 * @param params
	 * @param req
	 * @param res
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/statistic/jgClipPntStatMonthExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgClipPntStatMonthExcel(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgClipPntStatMonthExcel";
		log.debug(method, ">>> start");			
		HashMap map = new HashMap();
		try{
			//Request param
			log.debug(method,params.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			resultList = clipPntStatService.viewClipPntStatMonthExcel(params);
			
			map.put("rows", resultList );
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * 클립포인트 대사 일별 통계 상세 불일치 내역 조회
	 * 
	 * @param params
	 * @param req
	 * @param res
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/statistic/ajaxClipPntStatDDtl.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxClipPntStatDDtl(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "ajaxClipPntStatDDtl";
		log.debug(method, ">>> start");	
		//Request param				
		log.debug(method,params.toString());
		
		HashMap map = new HashMap();
		List resultList = new ArrayList();
		try {
			resultList = clipPntStatService.clipPntStatDayFailDtl(params);
			map.put("rows", resultList);
			map.put("eCode", "0");
		} catch (Exception e) {
			map.put("eCode", "-1");
			log.error(method, "Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 클립포인트 대사 월별 통계 상세 불일치 내역 조회
	 * 
	 * @param params
	 * @param req
	 * @param res
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/statistic/ajaxClipPntStatMDtl.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxClipPntStatMDtl(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "ajaxClipPntStatMDtl";
		log.debug(method, ">>> start");	
		//Request param				
		log.debug(method,params.toString());
		
		HashMap map = new HashMap();
		List resultList = new ArrayList();
		try {
			resultList = clipPntStatService.clipPntStatMonthFailDtl(params);
			map.put("rows", resultList);
			map.put("eCode", "0");
		} catch (Exception e) {
			map.put("eCode", "-1");
			log.error(method, "Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
}
