package com.olleh.pHubCMS.admin.statistic.service;

import java.util.List;
import java.util.Map;

public interface UseStepStatService {
	
	public List viewUseStepStatDay(Map<String, Object> params);
	public Map<String, Object> viewUseStepStatDayTotal(Map<String, Object> params);
	public List viewUseStepStatDayExcel(Map<String, Object> params);	
	
	public List viewUseStepStatMonth(Map<String, Object> params);
	public Map<String, Object> viewUseStepStatMonthTotal(Map<String, Object> params);
	public List viewUseStepStatMonthExcel(Map<String, Object> params);

}
