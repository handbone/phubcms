package com.olleh.pHubCMS.admin.statistic.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * 포인트거래 통계 Dao
 * 
 * @Class Name : PntDealStatDao
 * @author ojh
 * @since 2018.10.10
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.10    ojh        최초생성
 * 
 */

@Repository
public class PntDealStatDao extends AbstractDAO{

	public List viewPntDealStatDay(Map<String, Object> params) {
		return selectList("mybatis.statistic.pntDealStatDay", params);
	}	
	public Map<String, Object> viewPntDealStatDayTotal(Map<String, Object> params) {
		return selectOne("mybatis.statistic.pntDealStatDayTotal", params);
	}	
	public List viewPntDealStatDayExcel(Map<String, Object> params) {
		return selectList("mybatis.statistic.pntDealStatDayExcel", params);
	}	
	
	public List viewPntDealStatMonth(Map<String, Object> params) {
		return selectList("mybatis.statistic.pntDealStatMonth", params);
	}	
	public Map<String, Object> viewPntDealStatMonthTotal(Map<String, Object> params) {
		return selectOne("mybatis.statistic.pntDealStatMonthTotal", params);
	}	
	public List viewPntDealStatMonthExcel(Map<String, Object> params) {
		return selectList("mybatis.statistic.pntDealStatMonthExcel", params);
	}	
	
	public List viewPntDealStatDayPG(Map<String, Object> params) {
		return selectList("mybatis.statistic.pntDealStatDayPG", params);
	}	
	public Map<String, Object> viewPntDealStatDayTotalPG(Map<String, Object> params) {
		return selectOne("mybatis.statistic.pntDealStatDayTotalPG", params);
	}	
	public List viewPntDealStatDayExcelPG(Map<String, Object> params) {
		return selectList("mybatis.statistic.pntDealStatDayExcelPG", params);
	}	
	
	public List viewPntDealStatMonthPG(Map<String, Object> params) {
		return selectList("mybatis.statistic.pntDealStatMonthPG", params);
	}	
	public Map<String, Object> viewPntDealStatMonthTotalPG(Map<String, Object> params) {
		return selectOne("mybatis.statistic.pntDealStatMonthTotalPG", params);
	}	
	public List viewPntDealStatMonthExcelPG(Map<String, Object> params) {
		return selectList("mybatis.statistic.pntDealStatMonthExcelPG", params);
	}
	
	
	public List viewPntDealCmpnStatDay(Map<String, Object> params) {
		return selectList("mybatis.statistic.pntDealCmpnStatDay", params);
	}	
	public Map<String, Object> viewPntDealCmpnStatDayTotal(Map<String, Object> params) {
		return selectOne("mybatis.statistic.pntDealCmpnStatDayTotal", params);
	}	
	public List viewPntDealCmpnStatDayExcel(Map<String, Object> params) {
		return selectList("mybatis.statistic.pntDealCmpnStatDayExcel", params);
	}
	
	public List viewPntDealCmpnStatMonth(Map<String, Object> params) {
		return selectList("mybatis.statistic.pntDealCmpnStatMonth", params);
	}	
	public Map<String, Object> viewPntDealCmpnStatMonthTotal(Map<String, Object> params) {
		return selectOne("mybatis.statistic.pntDealCmpnStatMonthTotal", params);
	}	
	public List viewPntDealCmpnStatMonthExcel(Map<String, Object> params) {
		return selectList("mybatis.statistic.pntDealCmpnStatMonthExcel", params);
	}
	
	public List viewPntDealCmpnStatDayPG(Map<String, Object> params) {
		return selectList("mybatis.statistic.pntDealCmpnStatDayPG", params);
	}	
	public Map<String, Object> viewPntDealCmpnStatDayTotalPG(Map<String, Object> params) {
		return selectOne("mybatis.statistic.pntDealCmpnStatDayTotalPG", params);
	}	
	public List viewPntDealCmpnStatDayExcelPG(Map<String, Object> params) {
		return selectList("mybatis.statistic.pntDealCmpnStatDayExcelPG", params);
	}
	
	public List viewPntDealCmpnStatMonthPG(Map<String, Object> params) {
		return selectList("mybatis.statistic.pntDealCmpnStatMonthPG", params);
	}	
	public Map<String, Object> viewPntDealCmpnStatMonthTotalPG(Map<String, Object> params) {
		return selectOne("mybatis.statistic.pntDealCmpnStatMonthTotalPG", params);
	}	
	public List viewPntDealCmpnStatMonthExcelPG(Map<String, Object> params) {
		return selectList("mybatis.statistic.pntDealCmpnStatMonthExcelPG", params);
	}

}
