package com.olleh.pHubCMS.admin.statistic.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * 결제단계별 이용건수 통계 Dao
 * 
 * @Class Name : UseStepStatDao
 * @author ojh
 * @since 2018.11.14
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.11.14    ojh        최초생성
 * 
 */

@Repository
public class UseStepStatDao extends AbstractDAO{
	
	
	public List viewUseStepStatDay(Map<String, Object> params) {
		return selectList("mybatis.statistic.useStepStatDay", params);
	}	
	public Map<String, Object> viewUseStepStatDayTotal(Map<String, Object> params) {
		return selectOne("mybatis.statistic.useStepStatDayTotal", params);
	}	
	public List viewUseStepStatDayExcel(Map<String, Object> params) {
		return selectList("mybatis.statistic.useStepStatDayExcel", params);
	}
	
	
	public List viewUseStepStatMonth(Map<String, Object> params) {
		return selectList("mybatis.statistic.useStepStatMonth", params);
	}	
	public Map<String, Object> viewUseStepStatMonthTotal(Map<String, Object> params) {
		return selectOne("mybatis.statistic.useStepStatMonthTotal", params);
	}	
	public List viewUseStepStatMonthExcel(Map<String, Object> params) {
		return selectList("mybatis.statistic.useStepStatMonthExcel", params);
	}
	
	

}
