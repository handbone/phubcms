package com.olleh.pHubCMS.admin.statistic.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * 가맹점별 결제실패 원인별 이용건수 통계 Dao
 * 
 * @Class Name : PayErrStatDao
 * @author ojh
 * @since 2018.11.16
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.11.16    ojh        최초생성
 * 
 */

@Repository
public class PayErrStatDao extends AbstractDAO{
	
	public List viewPayErrCprtStatRank(Map<String, Object> params) {
		return selectList("mybatis.statistic.payErrCprtStatRank", params);
	}
	
	public List viewPayErrCprtStatDay(Map<String, Object> params) {
		return selectList("mybatis.statistic.payErrCprtStatDay", params);
	}	
	public Map<String, Object> viewPayErrCprtStatDayTotal(Map<String, Object> params) {
		return selectOne("mybatis.statistic.payErrCprtStatDayTotal", params);
	}	
	public List viewPayErrCprtStatDayExcel(Map<String, Object> params) {
		return selectList("mybatis.statistic.payErrCprtStatDayExcel", params);
	}	
	
	public List viewPayErrCprtStatMonth(Map<String, Object> params) {
		return selectList("mybatis.statistic.payErrCprtStatMonth", params);
	}	
	public Map<String, Object> viewPayErrCprtStatMonthTotal(Map<String, Object> params) {
		return selectOne("mybatis.statistic.payErrCprtStatMonthTotal", params);
	}	
	public List viewPayErrCprtStatMonthExcel(Map<String, Object> params) {
		return selectList("mybatis.statistic.payErrCprtStatMonthExcel", params);
	}
	
	public List viewPayErrPntStatRank(Map<String, Object> params) {
		return selectList("mybatis.statistic.payErrPntStatRank", params);
	}
	
	public List viewPayErrPntStatDay(Map<String, Object> params) {
		return selectList("mybatis.statistic.payErrPntStatDay", params);
	}	
	public Map<String, Object> viewPayErrPntStatDayTotal(Map<String, Object> params) {
		return selectOne("mybatis.statistic.payErrPntStatDayTotal", params);
	}	
	public List viewPayErrPntStatDayExcel(Map<String, Object> params) {
		return selectList("mybatis.statistic.payErrPntStatDayExcel", params);
	}	
	
	public List viewPayErrPntStatMonth(Map<String, Object> params) {
		return selectList("mybatis.statistic.payErrPntStatMonth", params);
	}	
	public Map<String, Object> viewPayErrPntStatMonthTotal(Map<String, Object> params) {
		return selectOne("mybatis.statistic.payErrPntStatMonthTotal", params);
	}	
	public List viewPayErrPntStatMonthExcel(Map<String, Object> params) {
		return selectList("mybatis.statistic.payErrPntStatMonthExcel", params);
	}
	

}
