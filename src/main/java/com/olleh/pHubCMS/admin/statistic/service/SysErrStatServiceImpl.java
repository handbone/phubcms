package com.olleh.pHubCMS.admin.statistic.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.statistic.dao.SysErrStatDao;

/**
 * 연동시스템 오류 통계 Service
 * 
 * @Class Name : SysErrStatServiceImpl
 * @author ojh
 * @since 2018.10.15
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.15    ojh        최초생성
 * 
 */


@Service
public class SysErrStatServiceImpl implements SysErrStatService{
	
	@Autowired
	SysErrStatDao sysErrStatDao;
	
	public List viewSysErrStatDay(Map<String, Object> params){
		return sysErrStatDao.viewSysErrStatDay(params);
	}
	public Map<String, Object> viewSysErrStatDayTotal(Map<String, Object> params){
		return sysErrStatDao.viewSysErrStatDayTotal(params);
	}
	public List viewSysErrStatDayExcel(Map<String, Object> params){
		return sysErrStatDao.viewSysErrStatDayExcel(params);
	}
	
	public List ajaxSysErrStatDayDtl(Map<String, Object> params){
		return sysErrStatDao.ajaxSysErrStatDayDtl(params);
	}
	public Map<String, Object> ajaxSysErrStatDayDtlTotal(Map<String, Object> params){
		return sysErrStatDao.ajaxSysErrStatDayDtlTotal(params);
	}
	
	public List viewSysErrStatMonth(Map<String, Object> params){
		return sysErrStatDao.viewSysErrStatMonth(params);
	}
	public Map<String, Object> viewSysErrStatMonthTotal(Map<String, Object> params){
		return sysErrStatDao.viewSysErrStatMonthTotal(params);		
	}
	public List viewSysErrStatMonthExcel(Map<String, Object> params){
		return sysErrStatDao.viewSysErrStatMonthExcel(params);
	}
	
	public List ajaxSysErrStatMonthDtl(Map<String, Object> params){
		return sysErrStatDao.ajaxSysErrStatMonthDtl(params);
	}
	public Map<String, Object> ajaxSysErrStatMonthDtlTotal(Map<String, Object> params){
		return sysErrStatDao.ajaxSysErrStatMonthDtlTotal(params);
	}
	

}
