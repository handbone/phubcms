package com.olleh.pHubCMS.admin.statistic.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.cmpn.service.PntPrvdrServiceImpl;
import com.olleh.pHubCMS.admin.cmpn.service.PntUseCmpnService;
import com.olleh.pHubCMS.admin.cmpr.service.PrvdrCmprService;
import com.olleh.pHubCMS.admin.statistic.service.PayErrStatService;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.utils.Paging;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 결제실패 원인별 이용건수/이용자수 통계 Controller
 * 
 * @Class Name : PayErrStatController
 * @author ojh
 * @since 2018.11.15
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.11.15    ojh        최초생성
 * 
 */

@Controller
public class PayErrStatController {
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	PntUseCmpnService pntUseCmpnService;
	
	@Autowired
	PntPrvdrServiceImpl pntPrvdrService;	
	
	@Autowired
	PayErrStatService payErrStatService;
	
	/**
	 * 가맹점별 결제실패 원인별 이용건수/이용자수 통계	 *
	 * jsp 호출 및 초기값 설정
	 * 
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/statistic/viewPayErrCprtStat.do", method = RequestMethod.POST)
	public String viewPayErrCprtStat(Locale locale, Model model) {	
		String method = "viewPayErrCprtStat";
		log.debug(method, ">>> start");

		//사용처 가져오기
		List cmpnList = new ArrayList();		
		cmpnList = pntUseCmpnService.getAllPntUseCmpn();
		model.addAttribute("USE_CMPN", cmpnList);	
		
		log.debug(method, ">>>>>> end");	
		return "/statistic/payErrCprtStatView";
	}
	
	
	/**
	 * 가맹점별 결제실패 원인별 이용건수/이용자수 통계 
	 *  
	 * JQGrid 호출
	 * 
	 * @param SRCH_IND		일월 구분
	 * @param CPRT_CMPN_ID  사용처 ID
	 * @param START_DATE 	선택한 달력 값
	 * @param END_DATE 		선택한 달력 값
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/statistic/jgPayErrCprtStat.do", method = RequestMethod.POST)
	public Map<String, Object> jgPayErrCprtStat(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgPayErrCprtStat";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		int pageNum = Integer.parseInt(request.getParameter("page"));
		int limit = Integer.parseInt(request.getParameter("rows"));	
		log.debug(method, params.toString()); 
		
		String srchInd = StringUtil.nvl(params.get("SRCH_IND"));
		if(srchInd.equals("D")){
			count = payErrStatService.viewPayErrCprtStatDayTotal(params);
		} else if(srchInd.equals("M")){
			count = payErrStatService.viewPayErrCprtStatMonthTotal(params);
		}	
		int countRow = 0;
		if(count != null && count.size() > 0){
			countRow = Integer.parseInt(count.get("cnt").toString());
		}
		
		HashMap map = new HashMap();
		if (countRow > 0) {
			//HashMap에 페이징 정보 저장					
			Paging.setPageMap(params, map, pageNum, limit, countRow);	
			log.debug(method,map.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			log.debug(method,params.toString());
			
			List rankList = new ArrayList();
			rankList = payErrStatService.viewPayErrCprtStatRank(params);
			map.put("rank", rankList);
			
			if(srchInd.equals("D")){
				resultList = payErrStatService.viewPayErrCprtStatDay(params);
			} else if(srchInd.equals("M")){
				resultList = payErrStatService.viewPayErrCprtStatMonth(params);
			}			
			map.put("rows", resultList);
			
			
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 가맹점별 결제실패 원인별 이용건수/이용자수 통계 엑셀다운
	 *  
	 * JQGrid 호출
	 * 
	 * @param SRCH_IND		일월 구분
	 * @param CPRT_CMPN_ID  사용처 ID
	 * @param START_DATE 	선택한 달력 값
	 * @param END_DATE 		선택한 달력 값
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/statistic/jgPayErrCprtStatExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgPayErrCprtStatExcel(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgPayErrCprtStatExcel";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		int pageNum = Integer.parseInt(request.getParameter("page"));
		int limit = Integer.parseInt(request.getParameter("rows"));	
		log.debug(method, params.toString()); 
		
		HashMap map = new HashMap();
			
		//검색 결과 데이터 map 에 추가
		List resultList = new ArrayList();
		log.debug(method,params.toString());
		
		List rankList = new ArrayList();
		rankList = payErrStatService.viewPayErrCprtStatRank(params);
		map.put("rank", rankList);
		
		String srchInd = StringUtil.nvl(params.get("SRCH_IND"));
		if(srchInd.equals("D")){
			resultList = payErrStatService.viewPayErrCprtStatDayExcel(params);
		} else if(srchInd.equals("M")){
			resultList = payErrStatService.viewPayErrCprtStatMonthExcel(params);
		}		
		
		map.put("rows", resultList);
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 포인트제공처별 결제실패 원인별 이용건수/이용자수 통계	 *
	 * jsp 호출 및 초기값 설정
	 * 
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/statistic/viewPayErrPntStat.do", method = RequestMethod.POST)
	public String viewPayErrPntStat(Locale locale, Model model) {	
		String method = "viewPayErrPntStat";
		log.debug(method, ">>> start");

		//제공처(카드사) 가져오기
		List pvList = new ArrayList();		
		pvList = pntPrvdrService.getAllPrvdrCmpn();
		log.debug(method, pvList.toString());		
		model.addAttribute("PV_CMPN", pvList);	
		
		log.debug(method, ">>>>>> end");	
		return "/statistic/payErrPntStatView";
	}
	
	
	/**
	 * 포인트제공처별 결제실패 원인별 이용건수/이용자수 통계 
	 *  
	 * JQGrid 호출
	 * 
	 * @param SRCH_IND		일월 구분
	 * @param PNT_CD 		제공처코드
	 * @param START_DATE 	선택한 달력 값
	 * @param END_DATE 		선택한 달력 값
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/statistic/jgPayErrPntStat.do", method = RequestMethod.POST)
	public Map<String, Object> jgPayErrPntStat(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgPayErrPntStat";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		int pageNum = Integer.parseInt(request.getParameter("page"));
		int limit = Integer.parseInt(request.getParameter("rows"));	
		log.debug(method, params.toString()); 
		
		String srchInd = StringUtil.nvl(params.get("SRCH_IND"));
		if(srchInd.equals("D")){
			count = payErrStatService.viewPayErrPntStatDayTotal(params);
		} else if(srchInd.equals("M")){
			count = payErrStatService.viewPayErrPntStatMonthTotal(params);
		}	
		int countRow = 0;
		if(count != null && count.size() > 0){
			countRow = Integer.parseInt(count.get("cnt").toString());
		}
		
		HashMap map = new HashMap();
		if (countRow > 0) {
			//HashMap에 페이징 정보 저장					
			Paging.setPageMap(params, map, pageNum, limit, countRow);	
			log.debug(method,map.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			log.debug(method,params.toString());
			
			List rankList = new ArrayList();
			rankList = payErrStatService.viewPayErrPntStatRank(params);
			map.put("rank", rankList);
			
			if(srchInd.equals("D")){
				resultList = payErrStatService.viewPayErrPntStatDay(params);
			} else if(srchInd.equals("M")){
				resultList = payErrStatService.viewPayErrPntStatMonth(params);
			}			
			map.put("rows", resultList);
			
			
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 포인트제공처별 결제실패 원인별 이용건수/이용자수 통계 엑셀다운
	 *  
	 * JQGrid 호출
	 * 
	 * @param SRCH_IND		일월 구분
	 * @param PNT_CD 		제공처코드
	 * @param START_DATE 	선택한 달력 값
	 * @param END_DATE 		선택한 달력 값
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/statistic/jgPayErrPntStatExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgPayErrPntStatExcel(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgPayErrPntStatExcel";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		int pageNum = Integer.parseInt(request.getParameter("page"));
		int limit = Integer.parseInt(request.getParameter("rows"));	
		log.debug(method, params.toString()); 
		
		HashMap map = new HashMap();
			
		//검색 결과 데이터 map 에 추가
		List resultList = new ArrayList();
		log.debug(method,params.toString());
		
		List rankList = new ArrayList();
		rankList = payErrStatService.viewPayErrPntStatRank(params);
		map.put("rank", rankList);
		
		String srchInd = StringUtil.nvl(params.get("SRCH_IND"));
		if(srchInd.equals("D")){
			resultList = payErrStatService.viewPayErrPntStatDayExcel(params);
		} else if(srchInd.equals("M")){
			resultList = payErrStatService.viewPayErrPntStatMonthExcel(params);
		}		
		map.put("rows", resultList);
		log.debug(method, ">>>>>> end");
		return map;
	}
}
