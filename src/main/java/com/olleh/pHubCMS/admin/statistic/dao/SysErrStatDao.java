package com.olleh.pHubCMS.admin.statistic.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * 연동시스템 오류 통계 Dao
 * 
 * @Class Name : SysErrStatDao
 * @author ojh
 * @since 2018.10.15
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.15    ojh        최초생성
 * 
 */

@Repository
public class SysErrStatDao extends AbstractDAO {
	
	public List viewSysErrStatDay(Map<String, Object> params) {
		return selectList("mybatis.statistic.sysErrStatDay", params);
	}	
	public Map<String, Object> viewSysErrStatDayTotal(Map<String, Object> params) {
		return selectOne("mybatis.statistic.sysErrStatDayTotal", params);
	}	
	public List viewSysErrStatDayExcel(Map<String, Object> params) {
		return selectList("mybatis.statistic.sysErrStatDayExcel", params);
	}		
	
	public List ajaxSysErrStatDayDtl(Map<String, Object> params) {
		return selectList("mybatis.statistic.sysErrStatDayDtl", params);
	}	
	public Map<String, Object> ajaxSysErrStatDayDtlTotal(Map<String, Object> params) {
		return selectOne("mybatis.statistic.sysErrStatDayDtlTotal", params);
	}	
	
	
	public List viewSysErrStatMonth(Map<String, Object> params) {
		return selectList("mybatis.statistic.sysErrStatMonth", params);
	}	
	public Map<String, Object> viewSysErrStatMonthTotal(Map<String, Object> params) {
		return selectOne("mybatis.statistic.sysErrStatMonthTotal", params);
	}	
	public List viewSysErrStatMonthExcel(Map<String, Object> params) {
		return selectList("mybatis.statistic.sysErrStatMonthExcel", params);
	}		
	
	public List ajaxSysErrStatMonthDtl(Map<String, Object> params) {
		return selectList("mybatis.statistic.sysErrStatMonthDtl", params);
	}	
	public Map<String, Object> ajaxSysErrStatMonthDtlTotal(Map<String, Object> params) {
		return selectOne("mybatis.statistic.sysErrStatMonthDtlTotal", params);
	}
	

}
