package com.olleh.pHubCMS.admin.statistic.service;

import java.util.List;
import java.util.Map;

public interface ClipPntStatService {

	public Map<String, Object> viewClipPntStatDayTotal(Map<String, Object> params);
	
	@SuppressWarnings("rawtypes")
	public List viewClipPntStatDay(Map<String, Object> params);
	
	@SuppressWarnings("rawtypes")
	public List viewClipPntStatDayExcel(Map<String, Object> params);
	
	@SuppressWarnings("rawtypes")
	public List clipPntStatDayFailDtl(Map<String, Object> params);
	
	public Map<String, Object> viewClipPntStatMonthTotal(Map<String, Object> params);
	
	@SuppressWarnings("rawtypes")
	public List viewClipPntStatMonth(Map<String, Object> params);
	
	@SuppressWarnings("rawtypes")
	public List viewClipPntStatMonthExcel(Map<String, Object> params);
	
	@SuppressWarnings("rawtypes")
	public List clipPntStatMonthFailDtl(Map<String, Object> params);
}
