package com.olleh.pHubCMS.admin.statistic.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.statistic.dao.PntDealStatDao;

/**
 * 포인트거래 대사 통계 Service
 * 
 * @Class Name : PntDealStatServiceImpl
 * @author ojh
 * @since 2018.10.10
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.10    ojh        최초생성
 * 
 */
@Service
public class PntDealStatServiceImpl implements PntDealStatService {
	
	@Autowired
	PntDealStatDao pntDealStatDao;
	
	@Override
	public List viewPntDealStatDay(Map<String, Object> params){
		return pntDealStatDao.viewPntDealStatDay(params);
	}
	@Override
	public Map<String, Object> viewPntDealStatDayTotal(Map<String, Object> params){
		return pntDealStatDao.viewPntDealStatDayTotal(params);
	}
	@Override
	public List viewPntDealStatDayExcel(Map<String, Object> params){
		return pntDealStatDao.viewPntDealStatDayExcel(params);		
	}
	
	@Override
	public List viewPntDealStatMonth(Map<String, Object> params){
		return pntDealStatDao.viewPntDealStatMonth(params);
	}
	@Override
	public Map<String, Object> viewPntDealStatMonthTotal(Map<String, Object> params){
		return pntDealStatDao.viewPntDealStatMonthTotal(params);
	}
	@Override
	public List viewPntDealStatMonthExcel(Map<String, Object> params){
		return pntDealStatDao.viewPntDealStatMonthExcel(params);
	}
	
	@Override
	public List viewPntDealStatDayPG(Map<String, Object> params){
		return pntDealStatDao.viewPntDealStatDayPG(params);
	}
	@Override
	public Map<String, Object> viewPntDealStatDayTotalPG(Map<String, Object> params){
		return pntDealStatDao.viewPntDealStatDayTotalPG(params);
	}
	@Override
	public List viewPntDealStatDayExcelPG(Map<String, Object> params){
		return pntDealStatDao.viewPntDealStatDayExcelPG(params);		
	}
	
	@Override
	public List viewPntDealStatMonthPG(Map<String, Object> params){
		return pntDealStatDao.viewPntDealStatMonthPG(params);
	}
	@Override
	public Map<String, Object> viewPntDealStatMonthTotalPG(Map<String, Object> params){
		return pntDealStatDao.viewPntDealStatMonthTotalPG(params);
	}
	@Override
	public List viewPntDealStatMonthExcelPG(Map<String, Object> params){
		return pntDealStatDao.viewPntDealStatMonthExcelPG(params);
	}
	
	
	@Override
	public List viewPntDealCmpnStatDay(Map<String, Object> params){
		return pntDealStatDao.viewPntDealCmpnStatDay(params);
	}
	@Override
	public Map<String, Object> viewPntDealCmpnStatDayTotal(Map<String, Object> params){
		return pntDealStatDao.viewPntDealCmpnStatDayTotal(params);
	}
	@Override
	public List viewPntDealCmpnStatDayExcel(Map<String, Object> params){
		return pntDealStatDao.viewPntDealCmpnStatDayExcel(params);		
	}
	@Override
	public List viewPntDealCmpnStatMonth(Map<String, Object> params){
		return pntDealStatDao.viewPntDealCmpnStatMonth(params);
	}
	@Override
	public Map<String, Object> viewPntDealCmpnStatMonthTotal(Map<String, Object> params){
		return pntDealStatDao.viewPntDealCmpnStatMonthTotal(params);
	}
	@Override
	public List viewPntDealCmpnStatMonthExcel(Map<String, Object> params){
		return pntDealStatDao.viewPntDealCmpnStatMonthExcel(params);
	}
	
	@Override
	public List viewPntDealCmpnStatDayPG(Map<String, Object> params){
		return pntDealStatDao.viewPntDealCmpnStatDayPG(params);
	}
	@Override
	public Map<String, Object> viewPntDealCmpnStatDayTotalPG(Map<String, Object> params){
		return pntDealStatDao.viewPntDealCmpnStatDayTotalPG(params);
	}
	@Override
	public List viewPntDealCmpnStatDayExcelPG(Map<String, Object> params){
		return pntDealStatDao.viewPntDealCmpnStatDayExcelPG(params);		
	}
	@Override
	public List viewPntDealCmpnStatMonthPG(Map<String, Object> params){
		return pntDealStatDao.viewPntDealCmpnStatMonthPG(params);
	}
	@Override
	public Map<String, Object> viewPntDealCmpnStatMonthTotalPG(Map<String, Object> params){
		return pntDealStatDao.viewPntDealCmpnStatMonthTotalPG(params);
	}
	@Override
	public List viewPntDealCmpnStatMonthExcelPG(Map<String, Object> params){
		return pntDealStatDao.viewPntDealCmpnStatMonthExcelPG(params);
	}

}
