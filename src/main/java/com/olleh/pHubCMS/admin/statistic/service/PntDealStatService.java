package com.olleh.pHubCMS.admin.statistic.service;

import java.util.List;
import java.util.Map;

public interface PntDealStatService {
	
	public List viewPntDealStatDay(Map<String, Object> params);
	public Map<String, Object> viewPntDealStatDayTotal(Map<String, Object> params);
	public List viewPntDealStatDayExcel(Map<String, Object> params);
	
	public List viewPntDealStatMonth(Map<String, Object> params);
	public Map<String, Object> viewPntDealStatMonthTotal(Map<String, Object> params);
	public List viewPntDealStatMonthExcel(Map<String, Object> params);
	
	public List viewPntDealStatDayPG(Map<String, Object> params);
	public Map<String, Object> viewPntDealStatDayTotalPG(Map<String, Object> params);
	public List viewPntDealStatDayExcelPG(Map<String, Object> params);
	
	public List viewPntDealStatMonthPG(Map<String, Object> params);
	public Map<String, Object> viewPntDealStatMonthTotalPG(Map<String, Object> params);
	public List viewPntDealStatMonthExcelPG(Map<String, Object> params);
	
	
	
	public List viewPntDealCmpnStatDay(Map<String, Object> params);
	public Map<String, Object> viewPntDealCmpnStatDayTotal(Map<String, Object> params);	
	public List viewPntDealCmpnStatDayExcel(Map<String, Object> params);
	
	public List viewPntDealCmpnStatMonth(Map<String, Object> params);
	public Map<String, Object> viewPntDealCmpnStatMonthTotal(Map<String, Object> params);
	public List viewPntDealCmpnStatMonthExcel(Map<String, Object> params);
	
	public List viewPntDealCmpnStatDayPG(Map<String, Object> params);
	public Map<String, Object> viewPntDealCmpnStatDayTotalPG(Map<String, Object> params);	
	public List viewPntDealCmpnStatDayExcelPG(Map<String, Object> params);
	
	public List viewPntDealCmpnStatMonthPG(Map<String, Object> params);
	public Map<String, Object> viewPntDealCmpnStatMonthTotalPG(Map<String, Object> params);
	public List viewPntDealCmpnStatMonthExcelPG(Map<String, Object> params);
	
}
