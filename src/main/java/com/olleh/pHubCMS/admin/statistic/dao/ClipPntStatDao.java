package com.olleh.pHubCMS.admin.statistic.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * 클립포인트 대사 통계 Dao
 * 
 * @Class Name : ClipPntStatDao
 * @author bmg
 * @since 2018.10.05
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.05   bmg        최초생성
 * 
 */
@Repository
public class ClipPntStatDao extends AbstractDAO {
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> viewClipPntStatDayTotal(Map<String, Object> params) {
		return selectOne("mybatis.statistic.clipPntStatDayTotal", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List viewClipPntStatDay(Map<String, Object> params) {
		return selectList("mybatis.statistic.clipPntStatDay", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List viewClipPntStatDayExcel(Map<String, Object> params) {
		return selectList("mybatis.statistic.clipPntStatDayExcel", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List clipPntStatDayFailDtl(Map<String, Object> params) {
		return selectList("mybatis.statistic.clipPntStatDayFailDtl", params);
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> viewClipPntStatMonthTotal(Map<String, Object> params) {
		return selectOne("mybatis.statistic.clipPntStatMonthTotal", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List viewClipPntStatMonth(Map<String, Object> params) {
		return selectList("mybatis.statistic.clipPntStatMonth", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List viewClipPntStatMonthExcel(Map<String, Object> params) {
		return selectList("mybatis.statistic.clipPntStatMonthExcel", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List clipPntStatMonthFailDtl(Map<String, Object> params) {
		return selectList("mybatis.statistic.clipPntStatMonthFailDtl", params);
	}
}
