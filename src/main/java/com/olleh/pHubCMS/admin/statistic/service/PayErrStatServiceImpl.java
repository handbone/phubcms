package com.olleh.pHubCMS.admin.statistic.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.statistic.dao.PayErrStatDao;

@Service
public class PayErrStatServiceImpl implements PayErrStatService{
	
	@Autowired
	PayErrStatDao payErrStatDao;	
	
	public List viewPayErrCprtStatRank(Map<String, Object> params) {
		return payErrStatDao.viewPayErrCprtStatRank(params);
	}	
	public List viewPayErrCprtStatDay(Map<String, Object> params){
		return payErrStatDao.viewPayErrCprtStatDay(params);
	}
	public Map<String, Object> viewPayErrCprtStatDayTotal(Map<String, Object> params){
		return payErrStatDao.viewPayErrCprtStatDayTotal(params);
	}
	public List viewPayErrCprtStatDayExcel(Map<String, Object> params){
		return payErrStatDao.viewPayErrCprtStatDayExcel(params);
	}
	
	public List viewPayErrCprtStatMonth(Map<String, Object> params){
		return payErrStatDao.viewPayErrCprtStatMonth(params);
	}
	public Map<String, Object> viewPayErrCprtStatMonthTotal(Map<String, Object> params){
		return payErrStatDao.viewPayErrCprtStatMonthTotal(params);
	}
	public List viewPayErrCprtStatMonthExcel(Map<String, Object> params){
		return payErrStatDao.viewPayErrCprtStatMonthExcel(params);		
	}
	
	public List viewPayErrPntStatRank(Map<String, Object> params) {
		return payErrStatDao.viewPayErrPntStatRank(params);
	}	
	public List viewPayErrPntStatDay(Map<String, Object> params) {
		return payErrStatDao.viewPayErrPntStatDay(params);
	}	
	public Map<String, Object> viewPayErrPntStatDayTotal(Map<String, Object> params) {
		return payErrStatDao.viewPayErrPntStatDayTotal(params);
	}	
	public List viewPayErrPntStatDayExcel(Map<String, Object> params) {
		return payErrStatDao.viewPayErrPntStatDayExcel(params);
	}	
	
	public List viewPayErrPntStatMonth(Map<String, Object> params) {
		return payErrStatDao.viewPayErrPntStatMonth(params);
	}	
	public Map<String, Object> viewPayErrPntStatMonthTotal(Map<String, Object> params) {
		return payErrStatDao.viewPayErrPntStatMonthTotal(params);
	}	
	public List viewPayErrPntStatMonthExcel(Map<String, Object> params) {
		return payErrStatDao.viewPayErrPntStatMonthExcel(params);
	}

}
