package com.olleh.pHubCMS.admin.statistic.service;

import java.util.List;
import java.util.Map;

public interface SysErrStatService {
	
	public List viewSysErrStatDay(Map<String, Object> params);
	public Map<String, Object> viewSysErrStatDayTotal(Map<String, Object> params);
	public List viewSysErrStatDayExcel(Map<String, Object> params);
	
	public List ajaxSysErrStatDayDtl(Map<String, Object> params);
	public Map<String, Object> ajaxSysErrStatDayDtlTotal(Map<String, Object> params);
	
	public List viewSysErrStatMonth(Map<String, Object> params);
	public Map<String, Object> viewSysErrStatMonthTotal(Map<String, Object> params);
	public List viewSysErrStatMonthExcel(Map<String, Object> params);
	
	public List ajaxSysErrStatMonthDtl(Map<String, Object> params);
	public Map<String, Object> ajaxSysErrStatMonthDtlTotal(Map<String, Object> params);

}
