package com.olleh.pHubCMS.admin.statistic.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.statistic.service.SysErrStatService;
import com.olleh.pHubCMS.common.components.CodeManage;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.CmnCdVO;
import com.olleh.pHubCMS.common.utils.Paging;

/**
 * 연동시스템 오류 통계 Controller
 * 
 * @Class Name : SysErrStatController
 * @author ojh
 * @since 2018.10.15
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.15    ojh        최초생성
 * 
 */

@Controller
public class SysErrStatController {
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	CodeManage codeManage;
	
	@Autowired
	SysErrStatService sysErrStatService;
	
	/**
	 * 연동시스템 오류 일별 통계	 *
	 * jsp 호출 및 초기값 설정
	 * 
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/statistic/viewSysErrStatD.do", method = RequestMethod.POST)
	public String viewSysErrStatD(Locale locale, Model model) {	
		String method = "viewSysErrStatD";
		log.debug(method, ">>> start");	
		
		//사용 'Y' 등록된 시스템 구분 가져오기 
		List<CmnCdVO> indList = new ArrayList<CmnCdVO>();
		indList = codeManage.getCodeListY("MOT_USE_IND");
		model.addAttribute("MOT_USE_IND", indList);
		
		log.debug(method, ">>>>>> end");	
		return "/statistic/sysErrStatDView";
	}
	
	/**
	 * 연동시스템 오류 일별 통계
	 * JQGrid 호출
	 * 
	 * @param IF_SYS_CD		SELECT	연동시스템
	 * @param ERR_CD		INPUT 	오류코드
	 * @param START_DATE 	선택한 달력 값
	 * @param END_DATE 		선택한 달력 값
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/statistic/jgSysErrStatD.do", method = RequestMethod.POST)
	public Map<String, Object> jgSysErrStatD(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgSysErrStatD";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		int pageNum = Integer.parseInt(request.getParameter("page"));
		int limit = Integer.parseInt(request.getParameter("rows"));	
		log.debug(method, params.toString()); 
		
		count = sysErrStatService.viewSysErrStatDayTotal(params);
		int countRow = Integer.parseInt(count.get("cnt").toString());
		
		HashMap map = new HashMap();
		if (countRow > 0) {
			//HashMap에 페이징 정보 저장					
			Paging.setPageMap(params, map, pageNum, limit, countRow);	
			log.debug(method,map.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			log.debug(method,params.toString());
			resultList = sysErrStatService.viewSysErrStatDay(params);
			map.put("rows", resultList);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 연동시스템 오류 일별 통계 (엑셀용)
	 * JQGrid 호출
	 * 
	 * @param IF_SYS_CD		SELECT	연동시스템
	 * @param ERR_CD		INPUT 	오류코드
	 * @param START_DATE 	선택한 달력 값
	 * @param END_DATE 		선택한 달력 값
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/statistic/jgSysErrStatDExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgSysErrStatDExcel(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgSysErrStatDExcel";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		log.debug(method, params.toString()); 		
		
		HashMap map = new HashMap();	
		//검색 결과 데이터 map 에 추가
		List resultList = new ArrayList();
		resultList = sysErrStatService.viewSysErrStatDayExcel(params);
		map.put("rows", resultList);
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 연동시스템 오류 일별 통계 상세 조회
	 * JQGrid 호출
	 * 
	 * @param IF_DD			연동일자
	 * @param IF_SYS_CD		연동시스템
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/statistic/jgSysErrStatDDtl.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxSysErrStatDDtl(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "ajaxSysErrStatDDtl";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		int pageNum = Integer.parseInt(request.getParameter("page"));
		int limit = Integer.parseInt(request.getParameter("rows"));	
		log.debug(method, params.toString()); 
		
		count = sysErrStatService.ajaxSysErrStatDayDtlTotal(params);
		int countRow = Integer.parseInt(count.get("cnt").toString());
		
		HashMap map = new HashMap();
		if (countRow > 0) {
			//HashMap에 페이징 정보 저장					
			Paging.setPageMap(params, map, pageNum, limit, countRow);	
			log.debug(method,map.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			log.debug(method,params.toString());
			resultList = sysErrStatService.ajaxSysErrStatDayDtl(params);
			map.put("rows", resultList);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	
	/**
	 * 연동시스템 오류 월별 통계	 *
	 * jsp 호출 및 초기값 설정
	 * 
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/statistic/viewSysErrStatM.do", method = RequestMethod.POST)
	public String viewSysErrStatM(Locale locale, Model model) {	
		String method = "viewSysErrStatM";
		log.debug(method, ">>> start");	
		
		//사용 'Y' 등록된 시스템 구분 가져오기 
		List<CmnCdVO> indList = new ArrayList<CmnCdVO>();
		indList = codeManage.getCodeListY("MOT_USE_IND");
		model.addAttribute("MOT_USE_IND", indList);
		
		log.debug(method, ">>>>>> end");	
		return "/statistic/sysErrStatMView";
	}
	
	/**
	 * 연동시스템 오류 월별 통계
	 * JQGrid 호출
	 * 
	 * @param IF_SYS_CD		SELECT	연동시스템
	 * @param ERR_CD		INPUT 	오류코드
	 * @param START_DATE 	선택한 달력 값
	 * @param END_DATE 		선택한 달력 값
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/statistic/jgSysErrStatM.do", method = RequestMethod.POST)
	public Map<String, Object> jgSysErrStatM(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgSysErrStatM";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		int pageNum = Integer.parseInt(request.getParameter("page"));
		int limit = Integer.parseInt(request.getParameter("rows"));	
		log.debug(method, params.toString()); 
		
		count = sysErrStatService.viewSysErrStatMonthTotal(params);
		int countRow = Integer.parseInt(count.get("cnt").toString());
		
		HashMap map = new HashMap();
		if (countRow > 0) {
			//HashMap에 페이징 정보 저장					
			Paging.setPageMap(params, map, pageNum, limit, countRow);	
			log.debug(method,map.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			log.debug(method,params.toString());
			resultList = sysErrStatService.viewSysErrStatMonth(params);
			map.put("rows", resultList);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 연동시스템 오류 월별 통계 (엑셀용)
	 * JQGrid 호출
	 * 
	 * @param IF_SYS_CD		SELECT	연동시스템
	 * @param ERR_CD		INPUT 	오류코드
	 * @param START_DATE 	선택한 달력 값
	 * @param END_DATE 		선택한 달력 값
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/statistic/jgSysErrStatMExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgSysErrStatMExcel(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgSysErrStatMExcel";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		log.debug(method, params.toString()); 		
		
		HashMap map = new HashMap();	
		//검색 결과 데이터 map 에 추가
		List resultList = new ArrayList();
		resultList = sysErrStatService.viewSysErrStatMonthExcel(params);
		map.put("rows", resultList);
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 연동시스템 오류 월별 통계 상세 조회
	 * JQGrid 호출
	 * 
	 * @param IF_MM			연동월
	 * @param IF_SYS_CD		연동시스템
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/statistic/jgSysErrStatMDtl.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxSysErrStatMDtl(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "ajaxSysErrStatMDtl";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		int pageNum = Integer.parseInt(request.getParameter("page"));
		int limit = Integer.parseInt(request.getParameter("rows"));	
		log.debug(method, params.toString()); 
		
		count = sysErrStatService.ajaxSysErrStatMonthDtlTotal(params);
		int countRow = Integer.parseInt(count.get("cnt").toString());
		
		HashMap map = new HashMap();
		if (countRow > 0) {
			//HashMap에 페이징 정보 저장					
			Paging.setPageMap(params, map, pageNum, limit, countRow);	
			log.debug(method,map.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			log.debug(method,params.toString());
			resultList = sysErrStatService.ajaxSysErrStatMonthDtl(params);
			map.put("rows", resultList);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}

}
