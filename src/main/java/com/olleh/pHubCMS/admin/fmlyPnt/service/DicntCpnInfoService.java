package com.olleh.pHubCMS.admin.fmlyPnt.service;

import java.util.List;
import java.util.Map;

@SuppressWarnings("rawtypes")
public interface DicntCpnInfoService {
	
	public Map<String, Object> dicntCpnInfoTotal(Map<String, Object> params);
	
	public List<Map> dicntCpnInfo(Map<String, Object> params);
	
	public List<Map> dicntCpnInfoExcel(Map<String, Object> params);
}
