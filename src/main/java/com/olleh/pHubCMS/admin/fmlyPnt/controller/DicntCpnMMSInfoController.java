package com.olleh.pHubCMS.admin.fmlyPnt.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.fmlyPnt.service.DicntCpnMMSInfoService;
import com.olleh.pHubCMS.common.components.Constant;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.utils.Paging;
import com.olleh.pHubCMS.common.utils.SessionUtils;

/**
 * 단말할인권 문자발송 정보 Controller
 * 
 * @Class 	DicntCpnMMSInfoController
 * @author	bmg
 * @since 	2019.03.06
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.03.06   bmg        최초생성
 */
@Controller
@SuppressWarnings({"rawtypes", "unchecked"})
public class DicntCpnMMSInfoController {
	
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	DicntCpnMMSInfoService dicntCpnMMSInfoService;
	
	/**
	 * jsp 호출 및 초기값 설정
	 * 
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/fmlyPnt/viewDicntCpnMMSInfo.do", method = RequestMethod.POST)
	public String viewDicntCpnMMSInfo(Locale locale, Model model) {
		String method = "viewDicntCpnMMSInfo";
		log.debug(method, ">>> start");	
		
		
		log.debug(method, ">>>>>> end");	
		return "/fmlyPnt/dicntCpnMMSInfoView";
	}
	
	/**
	 * JQGrid 단말할인권 문자발송 정보 호출
	 * 
	 * @param params
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/fmlyPnt/jgDicntCpnMMSInfo.do", method = RequestMethod.POST)
	public Map<String, Object> jgDicntCpnMMSInfo(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgDicntCpnMMSInfo";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		Map<String, Object> count = new HashMap<String, Object>();
		//Request param
		int pageNum = Integer.parseInt(request.getParameter("page"));
		int limit = Integer.parseInt(request.getParameter("rows"));	
		log.debug(method, params.toString());
		
		//총 Row 수 계산 		
		count = dicntCpnMMSInfoService.dicntCpnMMSInfoTotal(params);
		int countRow = Integer.parseInt(count.get("cnt").toString());
		log.debug(method,"count : "+countRow);
		
		if(countRow > 0){
			//HashMap에 페이징 정보 저장					
			Paging.setPageMap(params, map, pageNum, limit, countRow);	
			log.debug(method,map.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			log.debug(method,params.toString());
			resultList = dicntCpnMMSInfoService.dicntCpnMMSInfo(params);
			
			map.put("rows", resultList );
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * JQGrid 단말할인권 문자발송 정보 호출 (엑셀용 전체데이터)
	 * 
	 * @param params
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/fmlyPnt/jgDicntCpnMMSInfoExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgDicntCpnMMSInfoExcel(@RequestParam Map<String, Object> params) throws IOException {
		String method = "jgDicntCpnMMSInfoExcel";
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		//Request param
		log.debug(method, params.toString());		
				
		List resultList = new ArrayList();
		resultList = dicntCpnMMSInfoService.dicntCpnMMSInfoExcel(params);
		int countRow = resultList.size();
		log.debug(method, "COUNT : "+countRow);	
		map.put("rows", resultList );
		
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * 단말할인권 문자발송 정보 등록
	 * 
	 * @param params
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/fmlyPnt/ajaxCreateDicntCpnMMSInfo.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxCreateDicntCpnMMSInfo(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "ajaxCreateDicntCpnMMSInfo";
		log.debug(method, ">>> start");	
		
		HashMap map = new HashMap();	
		try {
			// 현재 로그인한 세션의 아이디 추가
			params.put("RGST_USER_ID", SessionUtils.getUserId(request));
			
			//Request param		
			log.debug(method,params.toString());
			
			dicntCpnMMSInfoService.createDicntCpnMMSInfo(params);
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * 단말할인권 문자발송 정보 수정
	 * 
	 * @param params
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/fmlyPnt/ajaxModifyDicntCpnMMSInfo.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxModifyDicntCpnMMSInfo(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "ajaxModifyDicntCpnMMSInfo";
		log.debug(method, ">>> start");	
		
		HashMap map = new HashMap();	
		try {
			// 현재 로그인한 세션의 아이디 추가
			params.put("MDFY_USER_ID", SessionUtils.getUserId(request));
			
			//Request param		
			log.debug(method,params.toString());
			
			dicntCpnMMSInfoService.modifyDicntCpnMMSInfo(params);
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
}
