package com.olleh.pHubCMS.admin.fmlyPnt.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.fmlyPnt.dao.DicntCpnInfoDao;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 단말할인권 정보 관리 Service
 * 
 * @Class 	DicntCpnInfoServiceImpl
 * @author	bmg
 * @since 	2019.03.06
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.03.06   bmg        최초생성
 */
@Service
@SuppressWarnings({"rawtypes", "unchecked"})
public class DicntCpnInfoServiceImpl implements DicntCpnInfoService {

	@Autowired
	DicntCpnInfoDao dicntCpnInfoDao;
	
	@Override
	public Map<String, Object> dicntCpnInfoTotal(Map<String, Object> params) {
		return dicntCpnInfoDao.dicntCpnInfoTotal(params);
	}
	
	@Override
	public List<Map> dicntCpnInfo(Map<String, Object> params) {
		return maskingName(dicntCpnInfoDao.dicntCpnInfo(params));
	}

	@Override
	public List<Map> dicntCpnInfoExcel(Map<String, Object> params) {
		return maskingName(dicntCpnInfoDao.dicntCpnInfoExcel(params));
	}

	/**
	 * 이름 마스킹 처리
	 * 
	 * @param params
	 * @return
	 */
	private List maskingName(List<Map> params) {
		int index = 0;
		if (params == null) {
			return params;
		}
		for (Map map : params) {
			map.put("rgst_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("rgst_user_nm"))));
			params.set(index, map);
			index++;
		}
		return params;
	}
}
