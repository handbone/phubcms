package com.olleh.pHubCMS.admin.fmlyPnt.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.fmlyPnt.dao.DicntCpnMMSInfoDao;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 단말할인권 문자발송 정보 Service
 * 
 * @Class 	DicntCpnMMSInfoServiceImpl
 * @author	bmg
 * @since 	2019.03.06
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.03.06   bmg        최초생성
 */
@Service
@SuppressWarnings({"rawtypes", "unchecked"})
public class DicntCpnMMSInfoServiceImpl implements DicntCpnMMSInfoService {
	
	private static final String PARAM_MSG_BODY = "MSG_BODY";
	
	@Autowired
	DicntCpnMMSInfoDao dicntCpnMMSInfoDao;

	@Override
	public Map<String, Object> dicntCpnMMSInfoTotal(Map<String, Object> params) {
		return dicntCpnMMSInfoDao.dicntCpnMMSInfoTotal(params);
	}

	@Override
	public List<Map> dicntCpnMMSInfo(Map<String, Object> params) {
		return maskingName(dicntCpnMMSInfoDao.dicntCpnMMSInfo(params));
	}

	@Override
	public List<Map> dicntCpnMMSInfoExcel(Map<String, Object> params) {
		return maskingName(dicntCpnMMSInfoDao.dicntCpnMMSInfoExcel(params));
	}

	@Override
	public int createDicntCpnMMSInfo(Map<String, Object> params) {
		// MSG_BODY 개행문자 변환
		params.put(PARAM_MSG_BODY, StringUtil.nvl(params.get(PARAM_MSG_BODY)).replaceAll("\n", "<br />"));
		return dicntCpnMMSInfoDao.createDicntCpnMMSInfo(params);
	}

	@Override
	public int modifyDicntCpnMMSInfo(Map<String, Object> params) {
		// MSG_BODY 개행문자 변환
		params.put(PARAM_MSG_BODY, StringUtil.nvl(params.get(PARAM_MSG_BODY)).replaceAll("\n", "<br />"));
		return dicntCpnMMSInfoDao.modifyDicntCpnMMSInfo(params);
	}
	/**
	 * 이름 마스킹 처리
	 * 
	 * @param params
	 * @return
	 */
	private List maskingName(List<Map> params) {
		int index = 0;
		if (params == null) {
			return params;
		}
		for (Map map : params) {
			if (map.get("rgst_user_nm") != null) {
				map.put("rgst_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("rgst_user_nm"))));
			}
			if (map.get("mdfy_user_nm") != null) {
				map.put("mdfy_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("mdfy_user_nm"))));
			}
			params.set(index, map);
			index++;
		}
		return params;
	}
}
