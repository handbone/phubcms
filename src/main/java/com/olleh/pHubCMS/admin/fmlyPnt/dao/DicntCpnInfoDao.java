package com.olleh.pHubCMS.admin.fmlyPnt.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * 단말할인권 정보 관리 Dao
 * 
 * @Class 	DicntCpnInfoDao
 * @author	bmg
 * @since 	2019.03.06
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.03.06   bmg        최초생성
 */
@Repository
@SuppressWarnings({ "unchecked", "rawtypes" })
public class DicntCpnInfoDao extends AbstractDAO {
	
	public Map<String, Object> dicntCpnInfoTotal(Map<String, Object> params) {
		return selectOne("mybatis.fmlyPnt.dicntCpnInfoTotal", params);
	}
	
	public List<Map> dicntCpnInfo(Map<String, Object> params) {
		return selectList("mybatis.fmlyPnt.dicntCpnInfo", params);
	}
	
	public List<Map> dicntCpnInfoExcel(Map<String, Object> params) {
		return selectList("mybatis.fmlyPnt.dicntCpnInfoExcel", params);
	}
}
