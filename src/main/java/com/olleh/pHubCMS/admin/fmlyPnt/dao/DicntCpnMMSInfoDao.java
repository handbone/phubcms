package com.olleh.pHubCMS.admin.fmlyPnt.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * 단말할인권 문자발송 정보 Dao
 * 
 * @Class 	DicntCpnMMSInfoDao
 * @author	bmg
 * @since 	2019.03.06
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.03.06   bmg        최초생성
 */
@Repository
@SuppressWarnings({ "unchecked", "rawtypes" })
public class DicntCpnMMSInfoDao extends AbstractDAO {
	
	public Map<String, Object> dicntCpnMMSInfoTotal(Map<String, Object> params) {
		return selectOne("mybatis.fmlyPnt.dicntCpnMMSInfoTotal", params);
	}
	
	public List<Map> dicntCpnMMSInfo(Map<String, Object> params) {
		return selectList("mybatis.fmlyPnt.dicntCpnMMSInfo", params);
	}

	public List<Map> dicntCpnMMSInfoExcel(Map<String, Object> params) {
		return selectList("mybatis.fmlyPnt.dicntCpnMMSInfoExcel", params);
	}
	
	public int createDicntCpnMMSInfo(Map<String, Object> params) {
		return insert("mybatis.fmlyPnt.createDicntCpnMMSInfo", params);
	}
	
	public int modifyDicntCpnMMSInfo(Map<String, Object> params) {
		return update("mybatis.fmlyPnt.modifyDicntCpnMMSInfo", params);
	}
}
