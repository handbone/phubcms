package com.olleh.pHubCMS.admin.fmlyPnt.service;

import java.util.List;
import java.util.Map;

@SuppressWarnings("rawtypes")
public interface DicntCpnMMSInfoService {
	
	public Map<String, Object> dicntCpnMMSInfoTotal(Map<String, Object> params);
	
	public List<Map> dicntCpnMMSInfo(Map<String, Object> params);

	public List<Map> dicntCpnMMSInfoExcel(Map<String, Object> params);
	
	public int createDicntCpnMMSInfo(Map<String, Object> params);
	
	public int modifyDicntCpnMMSInfo(Map<String, Object> params);
}
