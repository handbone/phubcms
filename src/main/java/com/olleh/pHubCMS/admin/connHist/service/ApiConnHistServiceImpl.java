package com.olleh.pHubCMS.admin.connHist.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.connHist.dao.ApiConnHistDao;


/**
 * Api 연동이력  Service
 * 
 * @Class Name : ApiConnHistService
 * @author ojh
 * @since 2018.08.21
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.21   ojh        최초생성
 * 
 */
@Service
public class ApiConnHistServiceImpl implements ApiConnHistService{
	
	@Autowired
	ApiConnHistDao apiConnHistDao;	
	
	@Override
	public List viewApiConnHist(Map<String, Object> params){
		return apiConnHistDao.viewApiConnHist(params);
		
	}
	@Override
	public Map<String, Object> viewApiConnHistTotal(Map<String, Object> params){
		return apiConnHistDao.viewApiConnHistTotal(params);		
	}
	@Override
	public List viewApiConnHistExcel(Map<String, Object> params) {
		return apiConnHistDao.viewApiConnHistExcel(params);
	}

}
