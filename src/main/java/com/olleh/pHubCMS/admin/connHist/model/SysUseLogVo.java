package com.olleh.pHubCMS.admin.connHist.model;


/**
 * 시스템 사용로그 VO
 * 
 * @Class Name : SysUseLogVo
 * @author ojh
 * @since 2018.08.22
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.22   ojh        최초생성
 * 
 */
public class SysUseLogVo {
	
	private String log_no;			// 로그 번호
	private String menu_nm;			// 메뉴명
	private String actn_id;			// 액션ID
	private String uri;				// URI
	private String prmt;			// 파라미터
	private String jnng_ip;			// 접속아이피
	private String rgst_user_id;	// 등록자ID
	private String rgst_dt;			// 등록일시

}
