package com.olleh.pHubCMS.admin.connHist.service;

import java.util.List;
import java.util.Map;

import com.olleh.pHubCMS.admin.connHist.model.DealReqHistVo;

public interface DealReqHistService {

	public List viewDealReqHist(Map<String, Object> params);
	public Map<String, Object> viewDealReqHistTotal(Map<String, Object> params);
	public List viewDealReqHistExcel(Map<String, Object> params);
	public List viewDealReqDtl(Map<String, Object> params);
}
