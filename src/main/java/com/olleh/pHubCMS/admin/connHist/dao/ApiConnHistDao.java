package com.olleh.pHubCMS.admin.connHist.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;
import com.olleh.pHubCMS.common.log.Logger;

/**
 * API 연동이력 Dao
 * @Class Name : ApiConnHistDao
 * @author : ojh
 * @since : 2018.08.21
 * @version : 1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *      수정일                 수정자                수정내용
 *  ------------  -----------    -------------
 *  2018. 8. 21.      ojh          최초 생성
 * 
 */

@Repository
public class ApiConnHistDao extends AbstractDAO{
	
	public List viewApiConnHist(Map<String, Object> params) {
		return selectList("mybatis.connHist.apiConnHist", params);
	}	
	public Map<String, Object> viewApiConnHistTotal(Map<String, Object> params) {
		return selectOne("mybatis.connHist.apiConnHistTotal", params);
	}
	public List viewApiConnHistExcel(Map<String, Object> params) {
		return selectList("mybatis.connHist.apiConnHistExcel", params);
	}	
	
	
	
	

}
