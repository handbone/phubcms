package com.olleh.pHubCMS.admin.connHist.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.connHist.service.ApiConnHistServiceImpl;
import com.olleh.pHubCMS.common.components.CodeManage;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.CmnCdVO;
import com.olleh.pHubCMS.common.utils.Paging;


/**
 * Api 연동이력 Controller
 * 
 * @Class Name : ApiConnHistController
 * @author ojh
 * @since 2018.08.21
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.21   ojh        최초생성
 * 
 */

@Controller
public class ApiConnHistController {
	
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	CodeManage codeManage;
	
	@Autowired
	ApiConnHistServiceImpl apiConnHistService;
	
	/**
	 * jsp 호출 및 초기값 설정
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/connHist/viewApiConnHist.do", method = RequestMethod.POST)
	public String viewApiConnHist(Locale locale, Model model) {	
		String method = "viewApiConnHist";
		log.debug(method, ">>> start");		
		try{
			List<CmnCdVO> plcList = new ArrayList<CmnCdVO>();
			//사용 'Y' 등록된 거래요청 상태 가져오기 
			plcList = codeManage.getCodeListY("SEND_RECV_PLC");
			model.addAttribute("SEND_RECV_PLC", plcList);
			
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}
		
		log.debug(method, ">>>>>> end");	
		return "/connHist/apiConnHistView";
	}
	
	/**
	 * JQGrid 호출
	 * 
	 * @param page 현재 보고자 하는 페이지 (JQgrid 처리)
	 * @param rows 한 페이지에 보여주고자 설정된 개수 (JQgrid 처리)
	 * @param ACTN_URI		Input 	액션URI
	 * @param PHUB_TR_ID	Input 	포인트허브거래번호
	 * @param SEND_PLC		Select	발신처
	 * @param RCV_PLC		Select	수신처
	 * @param RPLY_CD		Input	응답코드
	 * @param START_DATE 선택한 달력 값
	 * @param END_DATE 선택한 달력 값
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/connHist/jgApiConnHist.do", method = RequestMethod.POST)
	public Map<String, Object> jgApiConnHist(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "jgApiConnHist";
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		Map<String, Object> count = new HashMap<String, Object>();
		try{
			//Request param
			int pageNum = Integer.parseInt(req.getParameter("page"));
			int limit = Integer.parseInt(req.getParameter("rows"));
			log.debug(method, params.toString());
			
			//총 Row 수 계산 		
			count = apiConnHistService.viewApiConnHistTotal(params);		
			int countRow = Integer.parseInt(count.get("cnt").toString());			
			
			log.debug(method,"count : "+countRow);
			if(countRow > 0){
				//HashMap에 페이징 정보 저장					
				Paging.setPageMap(params, map, pageNum, limit, countRow);	
				log.debug(method,map.toString());
				
				//검색 결과 데이터 map 에 추가
				List resultList = new ArrayList();
				log.debug(method,params.toString());
				resultList = apiConnHistService.viewApiConnHist(params);				
				map.put("rows", resultList );
			}
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * JQGrid 호출 (엑셀용 전체데이터)
	 * 
	 * @param ACTN_URI		Input 	액션URI
	 * @param PHUB_TR_ID	Input 	포인트허브거래번호
	 * @param SEND_PLC		Select	발신처
	 * @param RCV_PLC		Select	수신처
	 * @param RPLY_CD		Input	응답코드
	 * @param START_DATE 선택한 달력 값
	 * @param END_DATE 선택한 달력 값
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/connHist/jgApiConnHistExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgApiConnHistExcel(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "jgApiConnHistExcel";
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		try{
			//Request param
			log.debug(method, params.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			resultList = apiConnHistService.viewApiConnHistExcel(params);	
			int countRow = resultList.size();
			log.debug(method, "COUNT : "+countRow);		
			map.put("rows", resultList );

		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}

		log.debug(method, ">>>>>> end");	
		return map;
	}

}
