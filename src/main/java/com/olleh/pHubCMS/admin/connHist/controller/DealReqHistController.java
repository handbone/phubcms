package com.olleh.pHubCMS.admin.connHist.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.cmpn.service.PntPgCmpnServiceImpl;
import com.olleh.pHubCMS.admin.cmpn.service.PntUseCmpnServiceImpl;
import com.olleh.pHubCMS.admin.connHist.service.DealReqHistServiceImpl;
import com.olleh.pHubCMS.common.components.CodeManage;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.CmnCdVO;
import com.olleh.pHubCMS.common.utils.JsonUtil;
import com.olleh.pHubCMS.common.utils.Paging;
/**
 * 거래요청내역 Controller
 * 
 * @Class Name : DealReqHistController
 * @author ojh
 * @since 2018.08.10
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.10   ojh        최초생성
 * 
 */
@Controller
public class DealReqHistController {
	private Logger log = new Logger(this.getClass());
	@Autowired
	CodeManage codeManage;
	
	@Autowired
	PntPgCmpnServiceImpl pntPgCmpnService;

	@Autowired
	PntUseCmpnServiceImpl pntUseCmpnService;
	
	@Autowired
	DealReqHistServiceImpl dealReqHistService;

	
	/**
	 * jsp 호출 및 초기값 설정
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/connHist/viewDealReqHist.do", method = RequestMethod.POST)
	public String viewDealReqHist(Locale locale, Model model) {	
		String method = "viewDealReqHist";
		log.debug(method, ">>> start");	
		
		try{
			//사용 'Y' 등록된 거래 구분 가져오기 
			List<CmnCdVO> indList = new ArrayList<CmnCdVO>();
			indList = codeManage.getCodeListY("DEAL_IND");
			model.addAttribute("DEAL_IND", indList);
			
			//사용 'Y' 등록된 거래요청 상태 가져오기 
			indList = codeManage.getCodeListY("DEAL_REQ_STAT");
			model.addAttribute("DEAL_REQ_STAT", indList);
			
			//PG사 가져오기
			List pgList = new ArrayList();
			pgList = pntPgCmpnService.getAllPgCmpn();
			model.addAttribute("PG_CMPN", pgList);
			
			// 서비스 ID 가져오기
			List svcCdList = codeManage.getCodeListY("PH_SVC_CD");
			model.addAttribute("PH_SVC_CD", svcCdList);
			
			//가맹점 가져오기
			List cmpnList = new ArrayList();		
			cmpnList = pntUseCmpnService.getAllPntUseCmpn();
			model.addAttribute("USE_CMPN", cmpnList);	
			
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}
		
		log.debug(method, ">>>>>> end");	
		return "/connHist/dealReqHistView";
	}
	
	
	/**
	 * JQGrid 호출
	 * 
	 * @param page 현재 보고자 하는 페이지 (JQgrid 처리)
	 * @param rows 한 페이지에 보여주고자 설정된 개수 (JQgrid 처리)
	 * @param DEAL_IND selectBox 선택한 거래구분 
	 * @param DEAL_STAT selectBox 선택한 거래상태
	 * @param PHUB_TR_NO Input 입력한 포인트허브거래번호
	 * @param START_DATE 선택한 달력 값
	 * @param END_DATE 선택한 달력 값
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/connHist/jgDealReqHist.do", method = RequestMethod.POST)
	public Map<String, Object> jgDealReqHist(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "jgDealReqHist";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		Map<String, Object> count = new HashMap<String, Object>();
		try{
			//Request param
			int pageNum = Integer.parseInt(req.getParameter("page"));
			int limit = Integer.parseInt(req.getParameter("rows"));
			log.debug(method, params.toString());
			
			//총 Row 수 계산 		
			count = dealReqHistService.viewDealReqHistTotal(params); 		
			int countRow = Integer.parseInt(count.get("cnt").toString());			
			
			log.debug(method,"count : "+countRow);
			if(countRow > 0){
				//HashMap에 페이징 정보 저장					
				Paging.setPageMap(params, map, pageNum, limit, countRow);	
				log.debug(method,map.toString());
				
				//검색 결과 데이터 map 에 추가
				List resultList = new ArrayList();
				log.debug(method,params.toString());
				resultList = dealReqHistService.viewDealReqHist(params);	
				map.put("rows", resultList );
			}
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}

		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	
	/**
	 * JQGrid 호출 (엑셀용 전체데이터)
	 * 
	 * @param DEAL_IND selectBox 선택한 거래구분 
	 * @param DEAL_STAT selectBox 선택한 거래상태
	 * @param PHUB_TR_NO Input 입력한 포인트허브거래번호
	 * @param START_DATE 선택한 달력 값
	 * @param END_DATE 선택한 달력 값
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/connHist/jgDealReqHistExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgDealReqHistExcel(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "jgDealReqHistExcel";
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		
		try{
			List resultList = new ArrayList();
			resultList = dealReqHistService.viewDealReqHistExcel(params);		
			int countRow = resultList.size();
			log.debug(method, "COUNT : "+countRow);	
			map.put("rows", resultList );
			
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}
	
		log.debug(method, ">>>>>> end");
		return map;
	}

	/**
	 * 상세거래요청내역 조회
	 * - 거래내역 상세팝업
	 * - 단말할인권 거래내역 상세팝업
	 * 
	 * @param PHUB_TR_NO 포인트허브거래번호 
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/connHist/ajaxDealReqDtl.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxDealReqDtl(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "ajaxDealReqDtl";
		log.debug(method, ">>> start");					
		HashMap map = new HashMap();
		try{
			//Request param		
			log.debug(method, "PHUB_TR_NO : "+params.get("PHUB_TR_NO"));
			
			//상세 거래내역 가져오기 		
			List resultList = new ArrayList();
			resultList = dealReqHistService.viewDealReqDtl(params);	
			log.debug(method, resultList.toString());	
			
			if(resultList!=null && resultList.size() > 0){	
				map.put("rows", resultList );
			}
			map.put("eCode", "0");
		} catch (Exception e) {
			map.put("eCode", "-1");
			log.error(method, "Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
}
