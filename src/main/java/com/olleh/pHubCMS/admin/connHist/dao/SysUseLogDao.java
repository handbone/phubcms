package com.olleh.pHubCMS.admin.connHist.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;
import com.olleh.pHubCMS.common.log.Logger;

/**
 * 시스템 사용로그 Dao
 * @Class Name : SysUseLogDao
 * @author : ojh
 * @since : 2018.08.21
 * @version : 1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *      수정일                 수정자                수정내용
 *  ------------  -----------    -------------
 *  2018. 8. 21.      ojh          최초 생성
 * 
 */

@Repository
public class SysUseLogDao extends AbstractDAO{
	
	public List viewSysUseLog(Map<String, Object> params) {
		return selectList("mybatis.connHist.sysUseLog", params);
	}	
	public Map<String, Object> viewSysUseLogTotal(Map<String, Object> params) {
		return selectOne("mybatis.connHist.sysUseLogTotal", params);
	}
	public List viewSysUseLogExcel(Map<String, Object> params) {
		return selectList("mybatis.connHist.sysUseLogExcel", params);
	}	
	
	
	/**
	 * <pre> 사용자 사용로그 기록  </pre>
	 * 
	 * @param HttpServletRequest request
	 * @param boolean isException
	 * @return 
	 * @see <pre>
	 *      로그기록하다 에러가 발생해도 전체 프로세스에는 영향이 없어야 한다.
	 *      </pre>
	 */	
	public int createUserLog(Map<String, Object> params) {
		return insert("mybatis.system.userLog.createUserLog", params);
	}
}
