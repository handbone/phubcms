package com.olleh.pHubCMS.admin.connHist.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.connHist.dao.DealReqHistDao;
import com.olleh.pHubCMS.admin.connHist.model.DealReqHistVo;

/**
 * 거래요청 내역 정보 Service
 * 
 * @Class Name : DealReqHistService
 * @author ojh
 * @since 2018.08.10
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.10   ojh        최초생성
 * 
 */
@Service
public class DealReqHistServiceImpl implements DealReqHistService{
	
	@Autowired
	DealReqHistDao dealReqHistDao;
	
	@Override
	public List viewDealReqHist(Map<String, Object> params){
		return dealReqHistDao.viewDealReqHist(params);	
	}
	@Override
	public Map<String, Object> viewDealReqHistTotal(Map<String, Object> params){
		return dealReqHistDao.viewDealReqHistTotal(params);	
	}	
	@Override
	public List viewDealReqHistExcel(Map<String, Object> params) {
		return dealReqHistDao.viewDealReqHistExcel(params);	
	}
	@Override
	public List viewDealReqDtl(Map<String, Object> params){
		return dealReqHistDao.viewDealReqDtl(params);	
	}

}
