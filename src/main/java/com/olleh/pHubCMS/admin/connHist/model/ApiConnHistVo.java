package com.olleh.pHubCMS.admin.connHist.model;

/**
 * Api 연동이력 VO
 * 
 * @Class Name : ApiConnHistVo
 * @author ojh
 * @since 2018.08.21
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.21   ojh        최초생성
 * 
 */
public class ApiConnHistVo {
	
	private String log_no;			// 로그번호
	private String actn_uri;		// 액션 URI
	private String send_plc;		// 발신처
	private String rcv_plc;			// 수신처
	private String phub_tr_id;		// 포인트허브거래번호
	private String req_data;		// 송신데이타
	private String res_data;		// 수신데이타
	private String rply_cd;			// 응답코드
	private String rply_msg;		// 응답메시지		
	private String rgst_user_id;	// 등록자ID
	private String rgst_dt;			// 등록일시
	
	
	public String getLog_no() {
		return log_no;
	}
	public void setLog_no(String log_no) {
		this.log_no = log_no;
	}
	public String getActn_uri() {
		return actn_uri;
	}
	public void setActn_uri(String actn_uri) {
		this.actn_uri = actn_uri;
	}
	public String getSend_plc() {
		return send_plc;
	}
	public void setSend_plc(String send_plc) {
		this.send_plc = send_plc;
	}
	public String getRcv_plc() {
		return rcv_plc;
	}
	public void setRcv_plc(String rcv_plc) {
		this.rcv_plc = rcv_plc;
	}
	public String getPhub_tr_id() {
		return phub_tr_id;
	}
	public void setPhub_tr_id(String phub_tr_id) {
		this.phub_tr_id = phub_tr_id;
	}
	public String getReq_data() {
		return req_data;
	}
	public void setReq_data(String req_data) {
		this.req_data = req_data;
	}
	public String getRes_data() {
		return res_data;
	}
	public void setRes_data(String res_data) {
		this.res_data = res_data;
	}
	public String getRply_cd() {
		return rply_cd;
	}
	public void setRply_cd(String rply_cd) {
		this.rply_cd = rply_cd;
	}
	public String getRply_msg() {
		return rply_msg;
	}
	public void setRply_msg(String rply_msg) {
		this.rply_msg = rply_msg;
	}
	public String getRgst_user_id() {
		return rgst_user_id;
	}
	public void setRgst_user_id(String rgst_user_id) {
		this.rgst_user_id = rgst_user_id;
	}
	public String getRgst_dt() {
		return rgst_dt;
	}
	public void setRgst_dt(String rgst_dt) {
		this.rgst_dt = rgst_dt;
	}
	
	
	

}
