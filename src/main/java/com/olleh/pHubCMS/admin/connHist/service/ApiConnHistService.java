package com.olleh.pHubCMS.admin.connHist.service;

import java.util.List;
import java.util.Map;

public interface ApiConnHistService {
	
	public List viewApiConnHist(Map<String, Object> params);
	public Map<String, Object> viewApiConnHistTotal(Map<String, Object> params);
	public List viewApiConnHistExcel(Map<String, Object> params);

}
