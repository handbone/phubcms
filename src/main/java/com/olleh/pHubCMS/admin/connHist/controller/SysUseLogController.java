package com.olleh.pHubCMS.admin.connHist.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.connHist.service.SysUseLogServiceImpl;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.utils.Paging;

/**
 * 시스템 사용로그 Controller
 * 
 * @Class Name : SysUseLogController
 * @author ojh
 * @since 2018.08.22
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.22   ojh        최초생성
 * 
 */


@Controller
public class SysUseLogController {
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	SysUseLogServiceImpl sysUseLogService;
	
	
	/**
	 * jsp 호출 및 초기값 설정
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/connHist/viewSysUseLog.do", method = RequestMethod.POST)
	public String viewSysUseLog(Locale locale, Model model) {	
		String method = "viewSysUseLog";
		log.debug(method, ">>> start");			
		
		log.debug(method, ">>>>>> end");	
		return "/connHist/sysUseLogView";
	}
	
	/**
	 * JQGrid 호출
	 * 
	 * @param page 현재 보고자 하는 페이지 (JQgrid 처리)
	 * @param rows 한 페이지에 보여주고자 설정된 개수 (JQgrid 처리)
	 * @param URI			Input 	URI
	 * @param MENU_NM		Input 	메뉴명
	 * @param JNNG_IP		Input	접속아이피
	 * @param RGST_USER_ID	Input	등록자ID
	 * @param START_DATE 선택한 달력 값
	 * @param END_DATE 선택한 달력 값
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/connHist/jgSysUseLog.do", method = RequestMethod.POST)
	public Map<String, Object> jgSysUseLog(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "jgSysUseLog";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		int pageNum = Integer.parseInt(req.getParameter("page"));
		int limit = Integer.parseInt(req.getParameter("rows"));
		log.debug(method, params.toString());
		
		//총 Row 수 계산 		
		count = sysUseLogService.viewSysUseLogTotal(params);	
		int countRow = Integer.parseInt(count.get("cnt").toString());
		
		HashMap map = new HashMap();
		log.debug(method,"count : "+countRow);
		if(countRow > 0){
			//HashMap에 페이징 정보 저장					
			Paging.setPageMap(params, map, pageNum, limit, countRow);	
			log.debug(method,map.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			log.debug(method,params.toString());
			resultList = sysUseLogService.viewSysUseLog(params);	
		
			map.put("rows", resultList );
		}

		log.debug(method, ">>>>>> end");
		return map;
	}
	
	
	/**
	 * JQGrid 호출 (엑셀용 전체데이터)
	 * 
	 * @param URI			Input 	URI
	 * @param MENU_NM		Input 	메뉴명
	 * @param JNNG_IP		Input	접속아이피
	 * @param RGST_USER_ID	Input	등록자ID
	 * @param START_DATE 선택한 달력 값
	 * @param END_DATE 선택한 달력 값
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/connHist/jgSysUseLogExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgSysUseLogExcel(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "jgSysUseLogExcel";
		log.debug(method, ">>> start");	
		//Request param
		log.debug(method, params.toString());
		HashMap map = new HashMap();
		
		//검색 결과 데이터 map 에 추가
		List resultList = new ArrayList();
		resultList = sysUseLogService.viewSysUseLogExcel(params);
		int countRow = resultList.size();
		log.debug(method, "COUNT : "+countRow);
		map.put("rows", resultList );

		log.debug(method, ">>>>>> end");
		return map;
	}
	
}
