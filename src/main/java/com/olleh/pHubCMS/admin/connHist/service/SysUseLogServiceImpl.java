package com.olleh.pHubCMS.admin.connHist.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.connHist.dao.SysUseLogDao;
import com.olleh.pHubCMS.common.components.MenuActionInfo;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.AdminLoginVO;
import com.olleh.pHubCMS.common.model.MenuActionInfoVO;
import com.olleh.pHubCMS.common.utils.JsonUtil;
import com.olleh.pHubCMS.common.utils.SessionUtils;
import com.olleh.pHubCMS.common.utils.StringUtil;
import com.olleh.pHubCMS.common.utils.SystemUtils;

/**
 * 시스템 사용로그 Service
 * 
 * @Class Name : SysUseLogService
 * @author ojh
 * @since 2018.08.22
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.22   ojh        최초생성
 * 
 */
@Service
public class SysUseLogServiceImpl implements SysUseLogService {
	private Logger log = new Logger(this.getClass());
	
	
	@Autowired
	SysUseLogDao sysUseLogDao;
	
	@Autowired
	MenuActionInfo menuActionInfo;
	
	@Override
	public List viewSysUseLog(Map<String, Object> params){
		return sysUseLogDao.viewSysUseLog(params);
	}	
	@Override
	public Map<String, Object> viewSysUseLogTotal(Map<String, Object> params){
		return sysUseLogDao.viewSysUseLogTotal(params);		
	}
	@Override
	public List viewSysUseLogExcel(Map<String, Object> params) {
		return sysUseLogDao.viewSysUseLogExcel(params);
	}
	
	
	/**
	 * <pre> 사용자 사용로그 기록  </pre>
	 * 
	 * @param HttpServletRequest request
	 * @param boolean isException
	 * @return 
	 * @see <pre>
	 *      로그기록하다 에러가 발생해도 전체 프로세스에는 영향이 없어야 한다.
	 *      </pre>
	 */	
	@Override
	public void userLogWrite(HttpServletRequest request, boolean isException) {
		//메소드명, 로그내용
		String methodName = "userLogWrite";
		log.debug(methodName, "Start!");
		
		// 접속정보 추출
		String path    = request.getContextPath();
		String reqUrl  = request.getRequestURI().substring(path.length());
		log.debug(methodName, "reqUrl="+reqUrl);

		// url 매핑 정보 추출
		MenuActionInfoVO mnuVO = menuActionInfo.getMenuActionVOYByUri(reqUrl);
		log.debug(methodName, "mnuVO="+JsonUtil.toJson(mnuVO));
		
		// log기록여부 체크
		if( "Y".equals(StringUtil.nvl(mnuVO.getLogWriteYn(), "N")) ) {

			// 메뉴리스트
			String mnuNm  = mnuVO.getMenuNm();
			String actNm  = mnuVO.getActnNm();
			String uriMtd = mnuVO.getUriMethod();
			
			// Action명 취득 (url에 매핑되어 있는 액션명이 RGST인 경우에는 "save" 파라미터 구분에 따라 등록인지 수정인지 판단 한다.)
			if( "RGST".equals(uriMtd) ) {
				// 프론트의 등록 / 수정 구분
				String sEditDiv = StringUtil.nvl(request.getParameter("save"));
				uriMtd = sEditDiv;
			}
			
			// 메뉴명 + Action명
			mnuNm = "[" + mnuNm + "]" + actNm;
			
			// 현재로그인 유저 정보
			AdminLoginVO admVO = SessionUtils.getAdminLoginVO(request);
			
			// log VO 셋팅
			Map<String, Object> userLogMap = new HashMap<String, Object>();
			userLogMap.put("menuNm", mnuNm);											// 접근페이지명
			userLogMap.put("actnId", mnuVO.getMenuId());								// 메뉴ID
			userLogMap.put("uri",    reqUrl);											// uri
			userLogMap.put("prmt",   paramConverter(request.getParameterMap()));	// 변경내용
			userLogMap.put("jnngIp", SystemUtils.getIpAddress(request));				// 접근IP주소
			userLogMap.put("userId", StringUtil.nvl(admVO.getUserId(), 
													StringUtil.nvl(request.getParameter("userId"))));	// 작업자
			//usrLogVO.setTRX_RSLT_CTNT( (String)urlMap.get(UrlManage.CMD_CTNT) );		// 트랜잭션결과내용
			
			if( isException ) {
				userLogMap.put("errCd", "10");
				userLogMap.put("errMsg", "실패");
			} else {
				userLogMap.put("errCd", "00");
				userLogMap.put("errMsg", "성공");
				
				// 성공일 경우 중료 파라미터 변조 처리?
			}
			
			// log 기록
			//log.debug(methodName, "userLogMap=" + JsonUtil.toJson(userLogMap));
			sysUseLogDao.createUserLog(userLogMap);
		}
		
	}	
	
	//파라미터 변경 
	//비밀번호 마스킹 처리
	public String paramConverter(Map<String, Object> map){
		Map tmpMap = new HashMap(map);
		
		if(tmpMap.get("password") != null){
			tmpMap.put("password", "****");
		}
		return JsonUtil.MapToJson(tmpMap);
	}
}
