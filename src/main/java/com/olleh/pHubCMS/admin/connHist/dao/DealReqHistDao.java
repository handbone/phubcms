package com.olleh.pHubCMS.admin.connHist.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.admin.connHist.model.DealReqHistVo;
import com.olleh.pHubCMS.common.dao.AbstractDAO;
import com.olleh.pHubCMS.common.log.Logger;

/**
 * 거래요청내역 조회 Dao
 * @Class Name : DealReqHistDao
 * @author : ojh
 * @since : 2018.08.10
 * @version : 1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *      수정일                 수정자                수정내용
 *  ------------  -----------    -------------
 *  2018. 8. 10.      ojh          최초 생성
 * 
 */

@Repository
public class DealReqHistDao extends AbstractDAO{
	
	public List viewDealReqHist(Map<String, Object> params) {
		return selectList("mybatis.connHist.dealReqHist", params);
	}	
	public Map<String, Object> viewDealReqHistTotal(Map<String, Object> params) {
		return selectOne("mybatis.connHist.dealReqHistTotal", params);
	}		
	public List viewDealReqHistExcel(Map<String, Object> params) {
		return selectList("mybatis.connHist.dealReqHistExcel", params);
	}
	public List viewDealReqDtl(Map<String, Object> params) {
		return selectList("mybatis.connHist.dealReqDtl", params);
	}	
	
	
	
	

}
