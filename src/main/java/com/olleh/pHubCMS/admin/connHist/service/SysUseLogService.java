package com.olleh.pHubCMS.admin.connHist.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

public interface SysUseLogService {
	
	public List viewSysUseLog(Map<String, Object> params);
	public Map<String, Object> viewSysUseLogTotal(Map<String, Object> params);
	public List viewSysUseLogExcel(Map<String, Object> params);
	
	/**
	 * 사용자 사용로그 기록
	 */
	public void userLogWrite(HttpServletRequest request, boolean isException);
}
