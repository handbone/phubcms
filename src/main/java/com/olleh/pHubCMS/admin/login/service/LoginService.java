/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pHubCMS.admin.login.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.olleh.pHubCMS.common.exception.BizException;

/**
 * 로그인 service
 * @Class Name : LoginService
 * @author lys
 * @since 2018.08.05
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.05    lys        최초생성
 * </pre>
 */
public interface LoginService {	

	/**
	 * <pre> 로그인 처리 </pre>
	 */
	public Map<String, Object> loginProcOld(Map<String, Object> params, HttpServletRequest request) throws Exception;	
	
	/**
	 * <pre> 로그인 처리 </pre>
	 */
	public Map<String, Object> loginProc(Map<String, Object> params, HttpServletRequest request) throws Exception;
	
	/**
	 * <pre> 로그인사용자 조회 및 유효성 체크 </pre>
	 */
	public Map<String, Object> loginUserSrch(Map<String, Object> params, HttpServletRequest request) throws BizException, Exception;
	
	/**
	 * <pre> 로그인세션 관리 </pre>
	 */
	public Map<String, Object> loginSessionMng(Map<String, Object> params, HttpServletRequest request) throws Exception;
	
	/**
	 * <pre> 로그인결과 기록(오류카운트 리셋 or 누적) </pre>
	 */
	public void updateLoginProcRst(Map<String, Object> params) throws BizException, Exception;	
	
	/**
	 * <pre> 로그인이력 저장 처리 </pre>
	 */
	public void userLoginHistProc(Map<String, Object> params);
	
	/**
	 * <pre> 로그인이력 기록 </pre>
	 */
	public void createUserLoginHist(Map<String, Object> params) throws Exception;	
	
	/**
	 * <pre> Ldap 연결 시도 </pre>
	 */
	public Boolean tryLdapConnect(String loginID, String loginpwd) throws Exception;
	
	/**
	 * <pre> Ldap 연결 정보 조회 </pre>
	 */	
	public Map<String, Object> getLdapInfo(String loginID) throws Exception;
}
