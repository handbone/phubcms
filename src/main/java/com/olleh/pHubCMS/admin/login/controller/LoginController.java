
package com.olleh.pHubCMS.admin.login.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.captcha.botdetect.web.servlet.Captcha;
import com.olleh.pHubCMS.admin.login.service.LoginService;
import com.olleh.pHubCMS.admin.system.service.UserInfoService;
import com.olleh.pHubCMS.common.components.Constant;
import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.components.RsaManager;
import com.olleh.pHubCMS.common.components.SysPrmtManage;
import com.olleh.pHubCMS.common.exception.UserException;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.AdminLoginVO;
import com.olleh.pHubCMS.common.spring.annotation.LoginUncheck;
import com.olleh.pHubCMS.common.spring.annotation.RefererCheck;
import com.olleh.pHubCMS.common.utils.JsonUtil;
import com.olleh.pHubCMS.common.utils.SessionUtils;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 로그인 처리 콘트롤러
 * @Class Name : LogInController
 * @author : lys
 * @since : 2018.08.25
 * @version : 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.25   lys        최초생성
 * </pre>
 */

@Controller
public class LoginController {
	private Logger log = new Logger(this.getClass());
    
    @Autowired
    LoginService loginService;
    
    @Autowired
    UserInfoService userInfoService;
    
    @Autowired
	MessageManage messageManage;
    
    @Autowired
	SysPrmtManage sysPrmtManage;
    
    @Autowired
    RsaManager rsaManager;
	
    /**
	 * <pre>로그인페이지 이동</pre>
	 * 
	 * @param Map<String,Object> params
	 * @param HttpServletRequest request
	 * @return ModelAndView
	 * @see <pre>
	 *      </pre>
	 */
	@RequestMapping(value = "/", method = {RequestMethod.GET, RequestMethod.POST})
	@LoginUncheck
	public ModelAndView index(@RequestParam Map<String, Object> params, HttpServletRequest request) throws Exception {
		//메소드명, 로그내용
		String methodName = "index";
		log.debug(methodName, "Start!");
		String cPath    = request.getContextPath();
		
		// Declare
		ModelAndView mv = new ModelAndView();
		mv.setView(new RedirectView(cPath+"/index.jsp"));
		return mv;
	}
	
	@RequestMapping(value = "/login/login.do", method = RequestMethod.POST)
	@RefererCheck
	@LoginUncheck
	public ModelAndView login(@RequestParam Map<String, Object> params, HttpServletRequest request) throws Exception {
		//메소드명, 로그내용
		String methodName = "login";
		log.debug(methodName, "Start!");
		
		// Declare
		ModelAndView mv = new ModelAndView("login/login");
		
		try {
			/***************************************************************
			 * 1. 로그인 세션 모두 삭제 처리 후 로그인 페이지 리턴
			 ***************************************************************/		
			// 세션소멸 후 새로 생성 한다.
			params.put("sessionProcTyp", "1");	// 세션처리 유형(1:세션소멸, 2:로그인세션 셋팅)
			loginService.loginSessionMng(params, request);
			
			HttpSession session = SessionUtils.getSession(request);
			Map<String, String> rsa = rsaManager.generator();
			
			session.setAttribute(RsaManager.RSA_PUBLIC_KEY, rsa.get(RsaManager.RSA_PUBLIC_KEY));
			session.setAttribute(RsaManager.RSA_PRIVATE_KEY, rsa.get(RsaManager.RSA_PRIVATE_KEY));
			session.setAttribute(RsaManager.RSA_MODULUS_KEY, rsa.get(RsaManager.RSA_MODULUS_KEY));
			session.setAttribute(RsaManager.RSA_EXPONENT_KEY, rsa.get(RsaManager.RSA_EXPONENT_KEY));
		} catch( Exception e) {
			// 에러 페이지로 이동
			log.debug(methodName, "[Exception]msg="+StringUtil.nvl(e.getMessage()));
			mv.setView(new RedirectView("common/error.do"));
		}
		
		/***************************************************************
		 * 2. 리턴
		 ***************************************************************/
		log.debug(methodName, "mv="+JsonUtil.toJson(mv));
		return mv;
	}	
	
	
	/**
	 * <pre>[Ajax]로그인처리</pre>
	 * 
	 * @param Map<String,Object> params
	 * @param HttpServletRequest request
	 * @return ModelAndView
	 * @see <pre>
	 *      </pre>
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/login/loginProc.do", method = {RequestMethod.POST})
	@ResponseBody
	@LoginUncheck
	public Map<String, Object> loginProc(@RequestParam Map<String, Object> params, HttpServletRequest request) throws Exception {
		/***************************************************************
		 * 0. 메소드명, 로그기록
		 ***************************************************************/
		String methodName = "loginProc";
		log.debug(methodName, "Start!");
		
		/***************************************************************
		 * 1. declare
		 ***************************************************************/
		String stepCd              = "001";
		String cPath    = request.getContextPath();
//		String loginDupl = StringUtil.nvl(params.get("loginD"));
		
		Map<String, Object> resMap = new HashMap<String, Object>();
		
		try {
			/***************************************************************
			 * 2. 로그인 처리
			 ***************************************************************/		
			stepCd = "002";
			
			// 캡챠 인증 여부 세션 확인
			/**
			if (!StringUtils.equals(loginDupl, "Y")) {
				HttpSession session = SessionUtils.getSession(request);
				String capchaAuth = StringUtil.nvl(session.getAttribute(SessionUtils.CAPTCH_AUTH));
				log.debug(methodName, "capchaAuth: "+  capchaAuth);
				if (!capchaAuth.equals(Constant.SUCCESS)) {
					throw new UserException(stepCd, messageManage.getMsgVOY("AD_INFO_163"));
				}
			}
			*/
		
			stepCd = "003";
			Map<String, Object> loginMap = loginService.loginProc(params, request);
			resMap.put("eStat", (loginMap == null) ? "-1" : StringUtil.nvl(loginMap.get("eStat")));
			if (loginMap == null || !StringUtils.equals(StringUtil.nvl(loginMap.get(Constant.SUCCESS_YN)), Constant.SUCCESS)) {	
				resMap.put(Constant.SUCCESS_YN, Constant.FAIL);
				resMap.put(Constant.RET_CODE, loginMap.get(Constant.RET_CODE));
				resMap.put(Constant.RET_MSG, loginMap.get(Constant.RET_MSG));
			} else {
				// 로그인 성공
				resMap.put(Constant.MENU_OBJECT, SessionUtils.getUserMenuList(request));
				resMap.put(Constant.SUCCESS_YN, Constant.SUCCESS);
				resMap.put(Constant.RET_CODE,   Constant.SUCCESS_CODE);
				resMap.put(Constant.RET_MSG,    Constant.SUCCESS_MSG);
				
				// 프론트에서 첫번째 메뉴로 자동 이동 처리.
				List<Map> userMenuList = SessionUtils.getUserMenuList(request);
				String afterLoginUrl = "";
				for(Map mnuMap : userMenuList) {
					String mnuUri = StringUtil.nvl(mnuMap.get("menu_uri"));
					if (!StringUtils.isEmpty(mnuUri)) {
						afterLoginUrl = cPath + mnuUri;
						break;
					}
				}
				
				// 사용자vo객체에 셋팅
				AdminLoginVO adminLoginVO = SessionUtils.getAdminLoginVO(request);
				adminLoginVO.setWelcomUri(afterLoginUrl);
				SessionUtils.setAdminLoginVO(request, adminLoginVO); 
//				loginMap.put("userId", adminLoginVO.getUserId());
//				userInfoService.modifyLoginY(loginMap);
				
				// 프론트 전달 셋팅
				resMap.put("afterLoginUrl", afterLoginUrl);				
			}
			
			// 로그인 이력 기록
			loginService.userLoginHistProc(loginMap);

		} catch( Exception e) {
			// 에러 메시지 출력
			log.debug(methodName, "[Exception]stepCd="+stepCd+", e="+StringUtil.nvl(e.getMessage()));
			resMap.put(Constant.SUCCESS_YN, Constant.FAIL);
			resMap.put(Constant.RET_CODE,   "E999");
			resMap.put(Constant.RET_MSG,    "로그인에 실패 하였습니다.");
		}
		
		/***************************************************************
		 * 2. 리턴
		 ***************************************************************/
		log.debug(methodName, "resMap="+JsonUtil.toJson(resMap));
		return resMap;
	}
	
	/**
	 * <pre>[AjaxForm]captcha 체크</pre>
	 * 
	 * @param Map<String,Object> params
	 * @return ModelAndView
	 * @see <pre>
	 *      </pre>
	 */
	@RequestMapping(value = "/login/captchaProc.do", method = {RequestMethod.POST})
	@ResponseBody
	@LoginUncheck
	public Map<String, Object> captchaProc(@RequestParam Map<String, Object> params, HttpServletRequest request) throws Exception {
		/***************************************************************
		 * 0. 메소드명, 로그기록
		 ***************************************************************/
		String methodName = "captchaProc";
		log.debug(methodName, "Start!");
		log.debug(methodName, "params="+JsonUtil.MapToJson(params));
		
		/***************************************************************
		 * 1. declare
		 ***************************************************************/
		Map<String, Object> resMap = new HashMap<String, Object>();		
		
		/***************************************************************
		 * 2. captcha 인증
		 ***************************************************************/		
		try {
			
			Captcha captcha = Captcha.load(request, "loginCaptcha");					
		    boolean isHuman = captcha.validate(request.getParameter("captchaCode"));

		    HttpSession session = SessionUtils.getSession(request);
		    if (!isHuman) {
		    	log.debug(methodName,request.getParameter("captchaCode")+ " Captcha Fail");
		    	session.setAttribute(SessionUtils.CAPTCH_AUTH, Constant.FAIL);
		    	resMap.put(Constant.SUCCESS_YN, Constant.FAIL);
				resMap.put(Constant.RET_CODE,   "-1");
				resMap.put(Constant.RET_MSG,    "보안 문자가 일치하지 않습니다.");
		    } else{
		    	log.debug(methodName,"Captcha Success");
		    	session.setAttribute(SessionUtils.CAPTCH_AUTH, Constant.SUCCESS);
		    	resMap.put(Constant.SUCCESS_YN, Constant.SUCCESS);
				resMap.put(Constant.RET_CODE,   "0");
		    }			
		} catch( Exception e) {
			// 에러 메시지 출력
			resMap.put(Constant.SUCCESS_YN, Constant.FAIL);
			resMap.put(Constant.RET_CODE,   "E999");
			resMap.put(Constant.RET_MSG,    "오류가 발생하였습니다");
		}
		
		/***************************************************************
		 * 2. 리턴
		 ***************************************************************/
		log.debug(methodName, "resMap="+JsonUtil.toJson(resMap));
		return resMap;
	}
	
	
	/**
	 * <pre>로그인아웃 처리</pre>
	 * 
	 * @param Map<String,Object> params
	 * @param HttpServletRequest request
	 * @return ModelAndView
	 * @see <pre>
	 *      </pre>
	 */
	@RequestMapping(value = "/login/logOut.do", method = {RequestMethod.POST})
	@RefererCheck
	public ModelAndView logOut(@RequestParam Map<String, Object> params, HttpServletRequest request) throws Exception {
		//메소드명, 로그내용
		String methodName = "logOut";
		log.debug(methodName, "Start!");
		log.debug(methodName, "params="+JsonUtil.MapToJson(params));
		
		// Declare
		String path     = request.getContextPath();
		ModelAndView mv = new ModelAndView(new RedirectView(path+"/index.jsp"));
		
		try {
			/***************************************************************
			 * 1. 로그아웃 이력 기록 : 이력기록 실패해도 로그인은 진행 한다.
			 *    : 2번에서 세션이 소멸될때 SessionListener 에서 로그아웃 이력을 기록 한다.
			 ***************************************************************/
			
			/***************************************************************
			 * 2. 로그인 세션 모두 삭제 처리 후 로그인 페이지 리턴
			 ***************************************************************/
			params.put("sessionProcTyp", "1");	// 세션처리 유형(1:세션소멸, 2:로그인세션 셋팅)
			loginService.loginSessionMng(params, request);			
			
		} catch( Exception e) {
			log.printStackTracePH(methodName, e);
		}
		
		/***************************************************************
		 * 3. 리턴
		 ***************************************************************/
		log.debug(methodName, "mv="+JsonUtil.toJson(mv));
		return mv;
	}
	
	/**
	 * 로그인 과정 이전단계로 세팅 (OTP 재전송 시 활용)
	 * 
	 * @param params
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/login/previousStepsLogin.do", method = {RequestMethod.POST})
	@ResponseBody
	@LoginUncheck
	public Map<String, Object> previousStepsLogin(HttpServletRequest request) throws Exception {
		String methodName = "previousStepsLogin";
		log.debug(methodName, "Start!");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			int loginProcStep = Integer.valueOf(StringUtil.nvl(SessionUtils.getSession(request).getAttribute("LOGIN_PROC_STEP"), "0"));
			if (loginProcStep == 0) {
				throw new UserException();
			} else {
				loginProcStep--;
			}
			SessionUtils.getSession(request).setAttribute("LOGIN_PROC_STEP", loginProcStep);
			map.put(Constant.SUCCESS_YN, Constant.SUCCESS);
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
			map.put(Constant.RET_MSG, Constant.SUCCESS_MSG);
		} catch (Exception e) {
			map.put(Constant.SUCCESS_YN, Constant.FAIL);
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			map.put(Constant.RET_MSG, Constant.FAIL_MSG);
		}
		return map;
	}
}
