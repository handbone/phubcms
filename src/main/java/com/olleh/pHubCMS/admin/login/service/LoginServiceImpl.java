/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 * 
 */
package com.olleh.pHubCMS.admin.login.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.captcha.botdetect.web.servlet.Captcha;
import com.olleh.pHubCMS.admin.system.dao.MenuInfoDao;
import com.olleh.pHubCMS.admin.system.dao.UserInfoDao;
import com.olleh.pHubCMS.admin.system.model.UserInfoVO;
import com.olleh.pHubCMS.api.service.PHHttpService;
import com.olleh.pHubCMS.common.components.Constant;
import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.components.RsaManager;
import com.olleh.pHubCMS.common.components.SysPrmtManage;
import com.olleh.pHubCMS.common.exception.BizException;
import com.olleh.pHubCMS.common.exception.UserException;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.AdminLoginVO;
import com.olleh.pHubCMS.common.model.ErrorInfoVO;
import com.olleh.pHubCMS.common.spring.listener.SessionListener;
import com.olleh.pHubCMS.common.utils.JsonUtil;
import com.olleh.pHubCMS.common.utils.SessionUtils;
import com.olleh.pHubCMS.common.utils.StringUtil;
import com.olleh.pHubCMS.common.utils.SystemUtils;

import nets.ldap.ADUtilSSL;


/**
 * 클립포인트서버 연동 service
 * @Class Name : ClipPointService
 * @author lys
 * @since 2018.08.05
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.05    lys        최초생성
 * </pre>
 */
@Service
public class LoginServiceImpl implements LoginService {
	private Logger log = new Logger(this.getClass());
	
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");

	@Resource
	UserInfoDao userInfoDao;
	
	@Resource
	MenuInfoDao menuInfoDao;

	@Autowired
	SysPrmtManage sysPrmtManage;
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
	RsaManager rsaManager;
	
	@Autowired
	PHHttpService phHttpService;
	
	/**
	 * <pre> 로그인 처리 </pre>
	 * 
	 * @param Map<String, Object> params
	 * @return Map<String, Object>
	 * @see <pre>
	 *      1. 유저조회
	 *      2. 유저 유효성 체크
	 *      3. ldap 연동
	 *      </pre>
	 */
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map<String, Object> loginProcOld(Map<String, Object> params, HttpServletRequest request) throws Exception {
		/***************************************************************
		 * 0. 메소드명, 로그기록
		 ***************************************************************/		
		String methodName = "loginProcOld";
		log.debug(methodName, "Start!");
		

		/***************************************************************
		 * 1. declare
		 ***************************************************************/
		String stepCd                     = "001";									// step 코드 (익셉션 발생시 사용)
		String loginStat                  = Constant.LOGIN_STAT_CD_IF;				// 로그인이력상태 (IS:성공, IF:실패)
		Map<String, Object> tmpMap        = new HashMap<String, Object>();			// temp 맵
		Map<String, Object> returnMap     = new HashMap<String, Object>();			// 리턴맵
		
		try {
			/***************************************************************
			 * 2. 세션소멸
			 ***************************************************************/		
			// 세션소멸 후 새로 생성 한다.
			stepCd = "002";
			// 해당 내용 삽입하면 RSA 복호화 불가능
			// 주석처리하지만 로그인페이지 호출시 들어가는 세션소멸이 로그인처리 과정에도 왜 들어가는지 테스트 필요
//			tmpMap.put("sessionProcTyp", "1");	// 세션처리 유형(1:세션소멸, 2:로그인세션 셋팅)
//			Map<String, Object> sessionMap1 = loginSessionMng(tmpMap, request);
//			if (sessionMap1 == null || !StringUtils.equals(StringUtil.nvl(sessionMap1.get(Constant.SUCCESS_YN)), Constant.SUCCESS)) {
//				// 세션소멸 실패
//				throw new UserException(stepCd, messageManage.getMsgVOY("IF_INFO_104"));
//			}
			
			/***************************************************************
			 * 3. 로그인유저 조회 및 유효성 체크
			 ***************************************************************/			
			stepCd = "003";
			Map<String, Object> loginInfoMap = loginUserSrch(params, request);
			if (loginInfoMap == null) {
				// 로그인에 실패 하였습니다.
				throw new UserException(stepCd, messageManage.getMsgVOY("AD_INFO_163"));
			} else {
				if (!StringUtils.equals(StringUtil.nvl(loginInfoMap.get(Constant.SUCCESS_YN)), Constant.SUCCESS)) {
					// 로그인 실패 처리 (유효성 체크 실퍠)
					stepCd = "031";
					tmpMap.clear();
					tmpMap.put("loginErrYn", "Y");
					tmpMap.put("userId", StringUtil.nvl(params.get("userId")));
					tmpMap.put("userInfoVO", loginInfoMap.get("userInfoVO"));	// 로그인유저 정보
					
					String eStat = StringUtil.nvl(loginInfoMap.get("eStat"));
					if (!StringUtils.isEmpty(eStat)) {
						returnMap.put("eStat", eStat);
						int e = Integer.parseInt(eStat);
						switch(e) {
							case 1:
								// 로그인결과 기록(오류카운트 리셋 or 누적)
								updateLoginProcRst(tmpMap);
								// 계정잠김
								throw new UserException(stepCd, messageManage.getMsgVOY("AD_INFO_162"));
							case 2:
								// 중복로그인 세션처리				
								throw new UserException(stepCd, messageManage.getMsgVOY("AD_INFO_161"));
							default:
						}
					}
					// 로그인결과 기록(오류카운트 리셋 or 누적)
					updateLoginProcRst(tmpMap);
					// 로그인에 실패 하였습니다.
					throw new UserException(stepCd, messageManage.getMsgVOY("AD_INFO_163"));
				}
			}
			
			/***************************************************************
			 * 4. 로그인정보 세션 저장
			 ***************************************************************/
			stepCd = "004";
			tmpMap.put("sessionProcTyp", "2");							// 세션처리 유형(1:세션소멸, 2:로그인세션 셋팅)
			tmpMap.put("userInfoVO", loginInfoMap.get("userInfoVO"));	// 유저정보 세팅
			
			Map<String, Object> sessionMap2 = loginSessionMng(tmpMap, request);
			if (sessionMap2 == null || !StringUtils.equals(StringUtil.nvl(sessionMap2.get(Constant.SUCCESS_YN)), Constant.SUCCESS)) {
				// 세션저장 실패
				throw new UserException(stepCd, messageManage.getMsgVOY("IF_INFO_104"));
			}

			/***************************************************************
			 * 5. 로그인 성공 처리 (오류카운트 초기화)
			 ***************************************************************/
			stepCd = "005";
			tmpMap.clear();
			tmpMap.put("loginErrYn", "N");
			tmpMap.put("userId", StringUtil.nvl(params.get("userId")));
			updateLoginProcRst(tmpMap);
			
			/***************************************************************
			 * 6. 리턴코드 셋팅 : 정상
			 ***************************************************************/
			stepCd = "006";
			loginStat = Constant.LOGIN_STAT_CD_IS;
			returnMap.put(Constant.SUCCESS_YN, Constant.SUCCESS);
			returnMap.put(Constant.RET_CODE,   Constant.SUCCESS_CODE);
			returnMap.put(Constant.RET_MSG,    Constant.SUCCESS_MSG);
			log.debug(methodName, "success!");
		} catch (UserException e) {
			// 에러메시지 정의
			ErrorInfoVO errinfo = e.getErrorInfo();
			returnMap.put(Constant.SUCCESS_YN, Constant.FAIL);
			returnMap.put(Constant.RET_CODE,   errinfo.getErrCd());
			returnMap.put(Constant.RET_MSG,    errinfo.getErrMsg());			
			log.debug(methodName, "[UserException]stepCd="+stepCd+", errCd="+errinfo.getErrCd()+", errMsg="+errinfo.getErrMsg());
		} catch (BizException e) {
			// 에러메시지 정의
			returnMap.put("eStat", "4");
			returnMap.put(Constant.SUCCESS_YN, Constant.FAIL);
			returnMap.put(Constant.RET_CODE,   "E999");
			returnMap.put(Constant.RET_MSG,    "비밀번호가 만료되었습니다");			
		} catch( Exception e) {
			// 에러 메시지 출력
			log.printStackTracePH(methodName, e);
			returnMap.put(Constant.SUCCESS_YN, Constant.FAIL);
			returnMap.put(Constant.RET_CODE,   "E999");
			returnMap.put(Constant.RET_MSG,    StringUtil.nvl(e.getMessage()));
		} finally {
			/***************************************************************
			 * 7. 로그인 이력 기록 : 이력기록 실패해도 로그인은 진행 한다.
			 ***************************************************************/
			stepCd = "007";
			Map<String, Object> loginHisMap = new HashMap();
			loginHisMap.put("userId",      StringUtil.nvl(params.get("userId")));
			loginHisMap.put("loginIp",     SystemUtils.getIpAddress(request));
			loginHisMap.put("loginStat",   loginStat);
			
			// 로그인 실패 카운트 셋팅 : 성공이면 0, 실패면 조회정보+1
			if( (Constant.LOGIN_STAT_CD_IS).equals(loginStat) ) {
				loginHisMap.put("loginErrCnt", 0);
			} else {
				UserInfoVO user = tmpMap.get("userInfoVO") != null ? (UserInfoVO) tmpMap.get("userInfoVO") : null;
				if (user != null) {
					int errCnt = Integer.parseInt(StringUtil.nvl(user.getLoginErrCnt(), "0"));
					loginHisMap.put("loginErrCnt", (errCnt + 1));
				}
			}	
			
			//기존 로그인 세션 소멸 처리 - 중복로그인 Y, 로그인오류여부 N
			if (StringUtils.equals(StringUtil.nvl(params.get("loginD")), "Y") 
					&& StringUtils.equals(StringUtil.nvl(tmpMap.get("loginErrYn")), "N")) {
				SessionListener.getInstance().expireDuplicatedSession(StringUtil.nvl(params.get("userId")), SessionUtils.getSession(request).getId());
			}
			
			// insert ADM_USER_LOGIN_HIST
			this.createUserLoginHist(loginHisMap);	
		}
		/***************************************************************
		 * 7. 리턴 
		 ***************************************************************/
		log.debug(methodName, "returnMap="+JsonUtil.toJson(returnMap));
		return returnMap;
	}
	
	/**
	 * <pre> 로그인 처리 </pre>
	 * 
	 * @param Map<String, Object> params
	 * @return Map<String, Object>
	 * @see <pre>
	 *      1. 유저조회
	 *      2. 유저 유효성 체크
	 *      3. ldap 연동
	 *      </pre>
	 */
	@Override
	public Map<String, Object> loginProc(Map<String, Object> params, HttpServletRequest request) throws Exception {
		/***************************************************************
		 * 0. 메소드명, 로그기록
		 ***************************************************************/		
		String methodName = "loginProc";
		log.debug(methodName, "Start!");
		
		/***************************************************************
		 * 1. declare
		 ***************************************************************/
		String stepCd                     = "001";									// step 코드 (익셉션 발생시 사용)
		final String LOGIN_PROC_STEP      = "LOGIN_PROC_STEP";
		final String PH_OTP_NO 		      = "PH_OTP_NO";
		String loginStat                  = Constant.LOGIN_STAT_CD_IF;				// 로그인이력상태 (IS:성공, IF:실패)
		Map<String, Object> tmpMap        = new HashMap<String, Object>();			// temp 맵
		Map<String, Object> returnMap     = new HashMap<String, Object>();			// 리턴맵
		
		String loginpwd = StringUtil.nvl(params.get("encPassword"));
		String otpNumber = StringUtil.nvl(params.get("encOtpNo"));
		String tmpEStat       = "";
		String tmpSuccessYn   = Constant.SUCCESS;
		String tmpRetCode     = "";
		String tmpRetMsg      = "";
		String tmpLoginErrRsn = "";
		
		String sPublicKey = StringUtil.nvl(SessionUtils.getSession(request).getAttribute(RsaManager.RSA_PUBLIC_KEY));
		String sPrivateKey = StringUtil.nvl(SessionUtils.getSession(request).getAttribute(RsaManager.RSA_PRIVATE_KEY));
		int loginProcStep = Integer.valueOf(StringUtil.nvl(SessionUtils.getSession(request).getAttribute(LOGIN_PROC_STEP), "0"));
		
		// 회원정보 조회
		UserInfoVO userInfoVO = userInfoDao.getUserInfoVO(params);
		returnMap.put("userInfoVO", userInfoVO);
		
		// 로그인절차 수행
		for( int i = loginProcStep; i <= 10; i++ ) {
			//
			switch(i) {
			case 0:
				/***************************************************************
				 * 1. 회원정보 존재여부 체크
				 ***************************************************************/				
				if (userInfoVO == null) {
					// 회원정보가 존재하지 않습니다.
					tmpLoginErrRsn = "회원정보가 존재하지 않습니다.";
			    	tmpSuccessYn   = Constant.FAIL;
			    	tmpRetCode     = messageManage.getMsgCd("AD_INFO_163");
			    	tmpRetMsg      = messageManage.getMsgTxt("AD_INFO_163");
				}
				break;
				
			case 1:
				/***************************************************************
				 * 2. capcha 인증
				 ***************************************************************/				
				Captcha captcha = Captcha.load(request, "loginCaptcha");					
			    boolean isHuman = captcha.validate(request.getParameter("captchaCode"));

			    HttpSession session = SessionUtils.getSession(request);
			    if (!isHuman) {
			    	log.debug(methodName,request.getParameter("captchaCode")+ " Captcha Fail");
			    	tmpLoginErrRsn = "[Capcha]보안 문자가 일치하지 않습니다.";
			    	tmpSuccessYn   = Constant.FAIL;
			    	tmpRetCode     = "-1";
			    	tmpRetMsg      = "보안 문자가 일치하지 않습니다.";
			    } else{
			    	log.debug(methodName,"Captcha Success");
			    }
			    // 세션기록
			    session.setAttribute(SessionUtils.CAPTCH_AUTH, tmpSuccessYn);
			    break;
				
			case 2:
				/***************************************************************
				 * 3. ip체크
				 ***************************************************************/				
				String ipAddr = SystemUtils.getIpAddress(request);
				log.debug(methodName, "localIP="+ipAddr);
				log.debug(methodName, "dbIP="+StringUtil.nvl(userInfoVO.getIpAddr()));
				
				// local에서 구동할 경우 ip체크 Pass~~
				if (StringUtils.equals(ipAddr, "127.0.0.1")) {
					ipAddr = userInfoVO.getIpAddr();
				}
				if (!StringUtils.equals(ipAddr, StringUtil.nvl(userInfoVO.getIpAddr()))) {
					// return : 허용되지 않은 IP입니다.
					tmpLoginErrRsn = "허용되지 않은 IP입니다(RemoteIp : "+ipAddr+")";
			    	tmpSuccessYn   = Constant.FAIL;
			    	tmpRetCode     = messageManage.getMsgCd("AD_INFO_163");
			    	tmpRetMsg      = messageManage.getMsgTxt("AD_INFO_163");
				}
				break;
				
			case 3:
				/***************************************************************
				 * 4. 계정잠김 체크
				 ***************************************************************/				
				if (!StringUtils.equals(StringUtil.nvl(userInfoVO.getStatCd()), Constant.STAT_CD_1000)) {
					// return : 올바르지 않은 회원상태 입니다.
					//계정 잠김 상태 eStat = 1
					tmpLoginErrRsn = "계정 잠김 상태 입니다.";
					tmpEStat     = "1";
			    	tmpSuccessYn = Constant.FAIL;
			    	tmpRetCode   = messageManage.getMsgCd("AD_INFO_162");
			    	tmpRetMsg    = messageManage.getMsgTxt("AD_INFO_162");
				}
				break;
				
			case 4:
				/***************************************************************
				 * 5. 로그인실패횟수 체크
				 ***************************************************************/				
				log.debug(methodName, "getLoginErrCnt="+userInfoVO.getLoginErrCnt());
				log.debug(methodName, "getSysPrmtVal="+sysPrmtManage.getSysPrmtVal("USR_PW_ERR_CNT"));			
				int errCnt    = Integer.valueOf(userInfoVO.getLoginErrCnt());
				int sysErrCnt = Integer.valueOf(sysPrmtManage.getSysPrmtVal("USR_PW_ERR_CNT"));
				
				if (sysErrCnt <= errCnt) {
					// return : 로그인실패횟수를 초과 하였습니다.
					tmpLoginErrRsn = "로그인 실패 횟수를 초과 하였습니다.(errCnt="+errCnt+")";
			    	tmpSuccessYn = Constant.FAIL;
			    	tmpRetCode   = messageManage.getMsgCd("AD_INFO_163");
			    	tmpRetMsg    = messageManage.getMsgTxt("AD_INFO_163");
				}
				break;
				
			case 5:
				/***************************************************************
				 * 6. ldap 연동
				 ***************************************************************/				
				String loginID  = StringUtil.nvl(userInfoVO.getUserId());
				loginpwd = rsaManager.webDecrypt(sPrivateKey, loginpwd);
				Boolean bLogin  = false;
				// LDAP 인증여부 체크
				if( "N".equals(StringUtil.nvl(userInfoVO.getLdapAuthYn(), "Y"))) {
					// 임시 비밀번호 체크
 					log.debug(methodName, "LDAP Auth Pass~~~");
					/**
					if( !CryptoUtil.sha256(params.get("password").toString()).equals(userInfoVO.getJnngPwd()) ){
						// return : 비밀번호가 일치하지 않습니다.
				    	tmpSuccessYn = Constant.FAIL;
				    	tmpRetCode   = messageManage.getMsgCd("IF_INFO_104");
				    	tmpRetMsg    = messageManage.getMsgTxt("IF_INFO_104");
					}
					*/
				} else {
					// LDAP 연동
					try {
						bLogin = tryLdapConnect(loginID, loginpwd);
						if( !bLogin ) {
					    	tmpSuccessYn   = Constant.FAIL;
					    	tmpRetCode     = "E998";
					    	tmpRetMsg      = "LDAP 인증에 실패하였습니다.";
					    	tmpLoginErrRsn = "LDAP 인증에 실패하였습니다.";
						}
					} catch (Exception e) {
						// 에러메시지 정의
						log.error(methodName, "[Exception]stepCd="+stepCd+", e="+StringUtil.nvl(e.getMessage()));
						log.printStackTracePH(methodName, e);
						if(StringUtil.nvl(e.getMessage()).indexOf("data 0004") > 0){
							//비밀번호 만료 상태 eStat = 4
							tmpEStat       = "4";
					    	tmpSuccessYn   = Constant.FAIL;
					    	tmpRetCode     = "-1";
					    	tmpRetMsg      = "비밀번호가 만료되었습니다";
					    	tmpLoginErrRsn = "비밀번호가 만료되었습니다.";
						} else {
							tmpSuccessYn   = Constant.FAIL;
					    	tmpRetCode     = "E996";
					    	//tmpRetMsg      = "LDAP 연동에 문제가 발생하였습니다.";
					    	//tmpLoginErrRsn = "LDAP 연동에 문제가 발생하였습니다.";
					    	tmpRetMsg      = "존재하지 않는 ID이거나, 잘못된 비밀번호 입니다.(LDAP 인증 실패!)";
					    	tmpLoginErrRsn = "존재하지 않는 ID이거나, 잘못된 비밀번호 입니다.(LDAP 인증 실패!)";					    	
						}
					}
				}
				break;
				
			case 6:
				/***************************************************************
				 * 7. OTP 생성
				 ***************************************************************/				
				if( "N".equals(StringUtil.nvl(userInfoVO.getOtpAuthYn(), "Y"))) {
					log.debug(methodName, "OTP Auth Pass~~~");
				} else {
					/***************************************************************
					 * OTP 연동 개발해야 한다.
					 ***************************************************************/
					String otpNo = StringUtil.getDocumentNum(999999, 6);
					String yymmddhhmmss = DATE_FORMAT.format(new Date());
					SessionUtils.getSession(request).setAttribute(PH_OTP_NO, rsaManager.encrypt(sPublicKey, otpNo+yymmddhhmmss));	// 세션에 암호화된 OTP 정보 저장
					tmpSuccessYn = Constant.FAIL;
					String ret = phHttpService.sendOTP(userInfoVO.getCntPlc(), otpNo);
					switch(ret) {
					case "0":
						tmpEStat = "3";
						break;
					case "-1":
						tmpRetCode     = "E997";
				    	tmpRetMsg      = "OTP 문자 발송 중 오류가 발생하였습니다.";
				    	tmpLoginErrRsn = "OTP 문자 발송 중 오류가 발생하였습니다.";
						break;
					default:
						tmpRetCode     = "E998";
				    	tmpRetMsg      = ret;
				    	tmpLoginErrRsn = ret;
						break;
					}
				}
				break;
				
			case 7:
				/***************************************************************
				 * 8. OTP 체크
				 ***************************************************************/
				if( "N".equals(StringUtil.nvl(userInfoVO.getOtpAuthYn(), "Y"))) {
					log.debug(methodName, "OTP Auth Pass~~~");
				} else {
					String sessionOtpInfo = rsaManager.decrypt(sPrivateKey, StringUtil.nvl(SessionUtils.getSession(request).getAttribute(PH_OTP_NO)));
					String sessionOtpNo = sessionOtpInfo.substring(0, 6);
					String sessionOtpTimestemp = sessionOtpInfo.substring(6);
					if (!StringUtil.timeMinuteYn(5, sessionOtpTimestemp, DATE_FORMAT.format(new Date()))) {
						tmpSuccessYn = Constant.FAIL;
						tmpRetCode = messageManage.getMsgCd("AD_INFO_906");
						tmpRetMsg  = "OTP 인증에 실패하였습니다.\nOTP 입력 시간이 초과되었습니다";
						tmpLoginErrRsn = "OTP 인증에 실패하였습니다. OTP 입력 시간이 초과되었습니다.";
					} else {
						otpNumber = SessionUtils.isExist(request, RsaManager.RSA_PRIVATE_KEY) ? rsaManager.webDecrypt(sPrivateKey, otpNumber) : otpNumber;
						if (!StringUtils.equals(sessionOtpNo, otpNumber)) {
							tmpSuccessYn = Constant.FAIL;
							tmpRetCode = messageManage.getMsgCd("AD_INFO_906");
							tmpRetMsg  = "OTP 인증에 실패하였습니다.\nOTP 값이 일치하지 않습니다";
							tmpLoginErrRsn = "OTP 인증에 실패하였습니다. OTP 값이 일치하지 않습니다.";
						}
					}
				}
				break;
				
			case 8:
				/***************************************************************
				 * 9. 중복로그인 체크
				 ***************************************************************/				
				log.debug(methodName, "getLoginYn="+userInfoVO.getLoginYn());
				//loginYn 컬럼이 Y인지 (현재 로그인 상태) loginD가 N인지 (Y이면 중복로그인->기존 로그인 해제 후 로그인 시도)
//				if (StringUtils.equals(StringUtil.nvl(userInfoVO.getLoginYn()), "Y") 
//						&& StringUtils.equals(StringUtil.nvl(params.get("loginD")), "N")) {
				if (StringUtils.equals(StringUtil.nvl(userInfoVO.getLoginYn()), "Y")) {
					// return : 이미 로그인되어 있습니다.
					//이미 로그인 eStat = 2
					tmpEStat       = "2";
			    	tmpSuccessYn   = Constant.FAIL;
			    	tmpRetCode     = messageManage.getMsgCd("AD_INFO_161");
			    	tmpRetMsg      = messageManage.getMsgTxt("AD_INFO_161");
			    	tmpLoginErrRsn = "이미 로그인되어 있습니다.";
				}
				break;
				
			case 9:
				/***************************************************************
				 * 10. 로그인정보 세션 저장
				 ***************************************************************/
				stepCd = "004";
				tmpMap.put("sessionProcTyp", "2");							
				tmpMap.put("userInfoVO", userInfoVO);
				Map<String, Object> sessionMap2 = loginSessionMng(tmpMap, request); 
				if (sessionMap2 == null || 
						!StringUtils.equals(StringUtil.nvl(sessionMap2.get(Constant.SUCCESS_YN)), Constant.SUCCESS)) {
					
					// 세션저장 실패
			    	tmpSuccessYn   = Constant.FAIL;
			    	tmpRetCode     = messageManage.getMsgCd("AD_INFO_163");
			    	tmpRetMsg      = messageManage.getMsgTxt("AD_INFO_163");
			    	tmpLoginErrRsn = "로그인정보 세션 저장에 실패하였습니다.";
				}
				break;
				
			case 10:
				/***************************************************************
				 * 11. 로그인 성공 처리 (오류카운트 초기화, 로그인여부=Y)
				 ***************************************************************/
				stepCd = "005";
				// 중복로그인 체크하여, 기존 로그인세션 삭제(loginProcStep이 9로 시작하면, 중복로그인 처리이다.)
				if( loginProcStep == 9 ) {
					//기존 로그인 세션 소멸 처리 - 중복로그인 Y, 로그인오류여부 N
					SessionListener.getInstance().expireDuplicatedSession(StringUtil.nvl(params.get("userId")), SessionUtils.getSession(request).getId());
				}				
				
				// 성공결과 기록
				tmpMap.clear();
				tmpMap.put("loginErrYn", "N");
				tmpMap.put("userId", StringUtil.nvl(params.get("userId")));
				updateLoginProcRst(tmpMap);
				break;
				
			default:
		    	tmpSuccessYn   = Constant.FAIL;
		    	tmpRetCode     = "-1";
		    	tmpRetMsg      = "정의되지 않은 로그인절차 입니다!";
		    	tmpLoginErrRsn = "정의되지 않은 로그인절차 입니다!";
			}
			
			/***************************************************************
			 * for문 종료
			 ***************************************************************/
			
			// 공통에러 처리
			if( !tmpSuccessYn.equals(Constant.SUCCESS) ) {
				log.debug(methodName, "[i="+i+"]tmpSuccessYn="+tmpSuccessYn+", tmpRetCode="+tmpRetCode+", tmpRetMsg="+tmpRetMsg+", tmpEStat="+tmpEStat);
				
				if( "1".equals(tmpEStat) ) {
					// 1 :계정잠김
			    	tmpRetCode = messageManage.getMsgCd("AD_INFO_162");
			    	tmpRetMsg  = messageManage.getMsgTxt("AD_INFO_162");
				} else if( "2".equals(tmpEStat) ) {
					// 2 : 중복로그인 세션처리
					SessionUtils.getSession(request).setAttribute(LOGIN_PROC_STEP, i+1);
					
			    	tmpRetCode = messageManage.getMsgCd("AD_INFO_161");
			    	tmpRetMsg  = messageManage.getMsgTxt("AD_INFO_161");					
				} else if ( "3".equals(tmpEStat)) {
					// 3 : OTP 발송 처리
					SessionUtils.getSession(request).setAttribute(LOGIN_PROC_STEP, i+1);
				} else {
					//
					tmpRetCode = (StringUtils.isEmpty(tmpRetCode)) ? messageManage.getMsgCd("AD_INFO_163") : tmpRetCode;
					tmpRetMsg  = (StringUtils.isEmpty(tmpRetMsg))  ? messageManage.getMsgTxt("AD_INFO_163") : tmpRetMsg;
				}	    	
		    	
		    	// for문 종료
		    	break;
			} else {
				log.debug(methodName, i + "Step Success!");
			}
		}
		
		/***************************************************************
		 * 리턴값 셋팅 : 로그인 성공/실패 처리
		 ***************************************************************/
		
		if( StringUtils.equals(tmpSuccessYn, Constant.SUCCESS) ) {
			// 성공
			stepCd = "006";
			loginStat    = Constant.LOGIN_STAT_CD_IS;
			tmpSuccessYn = Constant.SUCCESS;
			tmpEStat     = Constant.SUCCESS_CODE;
			tmpRetCode   = Constant.SUCCESS_CODE;
			tmpRetMsg    = Constant.SUCCESS_MSG;
			log.debug(methodName, "success!");
		} else {
			// 로그인 실패 처리 (유효성 체크 실패)
			stepCd = "031";
			tmpMap.clear();
			tmpMap.put("loginErrYn", "Y");
			tmpMap.put("userId", StringUtil.nvl(params.get("userId")));
			//tmpMap.put("userInfoVO", loginInfoMap.get("userInfoVO"));	// 로그인유저 정보
			tmpMap.put("userInfoVO", userInfoVO);	// 로그인유저 정보
			//String eStat = StringUtil.nvl(loginInfoMap.get("eStat"));
			
			// 중복로그인과 OTP 생성일 경우 로그인결과를 기록하지 않는다.
			if( (userInfoVO != null) && (!( "2".equals(tmpEStat) || "3".equals(tmpEStat) )) ) {
				// 로그인결과 기록(오류카운트 리셋 or 누적)
				updateLoginProcRst(tmpMap);				
			}
		}
		
		// 리턴값 셋팅 공통
		returnMap.put("eStat",      tmpEStat);	
		returnMap.put("userId",     StringUtil.nvl(params.get("userId")));
		returnMap.put("loginStat",  loginStat);		// 로그인결과
		returnMap.put("loginErrYn", (tmpSuccessYn.equals(Constant.SUCCESS)?"N":"Y"));
		returnMap.put(Constant.SUCCESS_YN, tmpSuccessYn);
		returnMap.put(Constant.RET_CODE,   tmpRetCode);
		returnMap.put(Constant.RET_MSG,    tmpRetMsg);
		returnMap.put("loginErrRsn",       tmpLoginErrRsn);
		
		

		
		/***************************************************************
		 * Return 
		 ***************************************************************/
		log.debug(methodName, "returnMap="+JsonUtil.toJson(returnMap));
		return returnMap;
	}
	
	
	/**
	 * <pre> 로그인유저 조회 </pre>
	 * 
	 * @param Map<String, Object> params
	 * @return Map<String, Object>
	 * @see <pre>
	 *      1. 유저조회
	 *      2. 유저 유효성 체크
	 *      3. ldap 연동
	 *      </pre>
	 */	
	@Override
	public Map<String, Object> loginUserSrch(Map<String, Object> params, HttpServletRequest request) throws BizException, Exception {
		/***************************************************************
		 * 0. 메소드명, 로그기록
		 ***************************************************************/		
		String methodName = "loginUserSrch";
		log.debug(methodName, "Start!");
		

		/***************************************************************
		 * 1. declare
		 ***************************************************************/
		String stepCd                 = "001";									// step 코드 (익셉션 발생시 사용)
		Map<String, Object> returnMap = new HashMap<String, Object>();			// 리턴맵
		try {
			/***************************************************************
			 * 2. 회원정보 조회
			 ***************************************************************/
			// 회원테이블에서 회원정보 조회
			stepCd = "002";
			UserInfoVO userInfoVO = userInfoDao.getUserInfoVO(params);
			if (userInfoVO == null) {
				// 존재하지 않는 회원 입니다.
				throw new UserException(stepCd, messageManage.getMsgVOY("IF_INFO_104"));
			} else {
				returnMap.put("userInfoVO", userInfoVO);
			}
			
			/***************************************************************
			 * 3. 유효성 체크
			 ***************************************************************/			
			// ip체크
			stepCd = "003";
			String ipAddr = SystemUtils.getIpAddress(request);
			log.debug(methodName, "localIP="+ipAddr);
			log.debug(methodName, "dbIP="+StringUtil.nvl(userInfoVO.getIpAddr()));
			
			// local에서 구동할 경우 ip체크 Pass~~
			if (StringUtils.equals(ipAddr, "127.0.0.1")) {
				ipAddr = userInfoVO.getIpAddr();
			}
			if (!StringUtils.equals(ipAddr, StringUtil.nvl(userInfoVO.getIpAddr()))) {
				// return : 허용되지 않은 IP입니다.
				throw new UserException(stepCd, messageManage.getMsgVOY("IF_INFO_104"));
			}
			
			// 계정잠김 체크
			stepCd = "031";
			if (!StringUtils.equals(StringUtil.nvl(userInfoVO.getStatCd()), Constant.STAT_CD_1000)) {
				// return : 올바르지 않은 회원상태 입니다.
				//계정 잠김 상태 eStat = 1
				returnMap.put("eStat", "1");
				throw new UserException(stepCd, messageManage.getMsgVOY("AD_INFO_162"));
			}	
			
			// 로그인실패횟수 체크
			stepCd = "032";
			log.debug(methodName, "getLoginErrCnt="+userInfoVO.getLoginErrCnt());
			log.debug(methodName, "getSysPrmtVal="+sysPrmtManage.getSysPrmtVal("USR_PW_ERR_CNT"));			
			int errCnt    = Integer.valueOf(userInfoVO.getLoginErrCnt());
			int sysErrCnt = Integer.valueOf(sysPrmtManage.getSysPrmtVal("USR_PW_ERR_CNT"));
			
			if (sysErrCnt <= errCnt) {
				// return : 로그인실패횟수를 초과 하였습니다.
				throw new UserException(stepCd, messageManage.getMsgVOY("IF_INFO_104"));
			}		
			
			// 중복로그인 체크
			stepCd = "033";
			log.debug(methodName, "getLoginYn="+userInfoVO.getLoginYn());
			//loginYn 컬럼이 Y인지 (현재 로그인 상태) loginD가 N인지 (Y이면 중복로그인->기존 로그인 해제 후 로그인 시도)
			if (StringUtils.equals(StringUtil.nvl(userInfoVO.getLoginYn()), "Y") 
					&& StringUtils.equals(StringUtil.nvl(params.get("loginD")), "N")) {
				// return : 이미 로그인되어 있습니다.
				//이미 로그인 eStat = 2
				returnMap.put("eStat", "2");
				throw new UserException(stepCd, messageManage.getMsgVOY("AD_INFO_161"));
			}

			/***************************************************************
			 * 4. ldap 연동
			 ***************************************************************/			
			stepCd = "004";
			
			String loginID = StringUtil.nvl(params.get("userId"));
			String loginpwd = StringUtil.nvl(params.get("password"));
			
			if (SessionUtils.isExist(request, RsaManager.RSA_PRIVATE_KEY)) {
				String sPrivateKey = (String) SessionUtils.getSession(request).getAttribute(RsaManager.RSA_PRIVATE_KEY);
				loginpwd = rsaManager.webDecrypt(sPrivateKey, loginpwd);
			}
			
			//임시 비밀번호 체크
			/*if(!Common.sha1(params.get("password").toString()).equals(userInfoVO.getJnngPwd())){
				// return : 비밀번호가 일치하지 않습니다.
				throw new UserException(stepCd, messageManage.getMsgVOY("IF_INFO_104"));
			}*/		
			
			Boolean bLogin = tryLdapConnect(loginID, loginpwd);
//			Boolean bLogin = true;
			if (bLogin) {		
				/***************************************************************
				 * 5. 리턴코드 셋팅 : 정상
				 ***************************************************************/
				returnMap.put(Constant.SUCCESS_YN, Constant.SUCCESS);
				returnMap.put(Constant.RET_CODE,   Constant.SUCCESS_CODE);
				returnMap.put(Constant.RET_MSG,    Constant.SUCCESS_MSG);
			}
		} catch (UserException e) {
			// 에러메시지 정의
			ErrorInfoVO errinfo = e.getErrorInfo();
			returnMap.put(Constant.SUCCESS_YN, Constant.FAIL);
			returnMap.put(Constant.RET_CODE,   errinfo.getErrCd());
			returnMap.put(Constant.RET_MSG,    errinfo.getErrMsg());	
			log.error(methodName, "[UserException]stepCd="+stepCd+", errCd="+errinfo.getErrCd()+", errMsg="+errinfo.getErrMsg());
		} catch (Exception e) {
			// 에러메시지 정의
			log.error(methodName, "[Exception]stepCd="+stepCd+", e="+StringUtil.nvl(e.getMessage()));
			log.printStackTracePH(methodName, e);
			if(StringUtil.nvl(e.getMessage()).indexOf("data 0004") > 0){
				//비밀번호 만료 상태 eStat = 4
				throw new BizException(e);
			}
		}
		
		/***************************************************************
		 * 6. 리턴 
		 ***************************************************************/
		return returnMap;
	}

	
	/**
	 * <pre> 로그인세션 관리 </pre>
	 * 
	 * @param Map<String, Object> params
	 * @return Map<String, Object>
	 * @see <pre>
	 *      1. sessionProcTyp - 세션처리 유형(1:세션소멸, 2:로그인세션 셋팅)
	 *      </pre>
	 */	
	@Override
	public Map<String, Object> loginSessionMng(Map<String, Object> params, HttpServletRequest request) throws Exception {
		/***************************************************************
		 * 0. 메소드명, 로그기록
		 ***************************************************************/		
		String methodName = "loginSessionMng";
		log.debug(methodName, "Start!");
		

		/***************************************************************
		 * 1. declare
		 ***************************************************************/
		String stepCd                 = "001";											// step 코드 (익셉션 발생시 사용)
		String sessionProcTyp         = StringUtil.nvl(params.get("sessionProcTyp"));	// 세션처리 유형(1:세션소멸, 2:로그인세션 셋팅)
		Map<String, Object> tmpMap    = new HashMap<String, Object>();					// temp Map
		Map<String, Object> returnMap = new HashMap<String, Object>();					// 리턴맵
		
		
		try {
			/***************************************************************
			 * 2. 로그인 유저 세션 처리
			 ***************************************************************/
			if( "1".equals(sessionProcTyp) ) {
				// 세션소멸 후 새로 생성 한다.
				stepCd = "002";
				HttpSession session = SessionUtils.getSession(request);
				session.invalidate();
				request.getSession(true);
				
			} else if( "2".equals(sessionProcTyp) ) {
				// 세션소멸 후 새로 생성 한다.
				stepCd = "002";
				HttpSession session = SessionUtils.getSession(request);
				session.invalidate();
				request.getSession(true);

				// 로그인정보 셋팅
				stepCd = "003";
				UserInfoVO userInfoVO = (UserInfoVO) params.get("userInfoVO");
				
				// 메뉴정보 조회 및 세션저장
				stepCd = "004";
				tmpMap.put("userGrpId", userInfoVO.getUserGrpId());
				List userMenuList = menuInfoDao.getUserMenuList(tmpMap);
				SessionUtils.setUserMenuList(request, userMenuList);
				
				// 세션정보 셋팅
				stepCd = "005";
				AdminLoginVO adminLoginVO = new AdminLoginVO();
				adminLoginVO.setUserId(userInfoVO.getUserId());
				adminLoginVO.setUserNm(userInfoVO.getUserNm());
				adminLoginVO.setDeptCd(userInfoVO.getDeptCd());
				adminLoginVO.setDeptNm(userInfoVO.getDeptNm());
				adminLoginVO.setStatCd(userInfoVO.getStatCd());
				adminLoginVO.setRcntJnngDd(userInfoVO.getRcntJnngDd());
				//adminLoginVO.setLoginIp(userInfoVO.getIpAddr());
				adminLoginVO.setLoginIp(StringUtil.nvl(SystemUtils.getIpAddress(request)));
				adminLoginVO.setLoginYn("Y");
				
				// 로그인정보 세션 저장
				stepCd = "006";
				SessionUtils.setAdminLoginVO(request, adminLoginVO);
				
			} else {
				// 정의되지 않은 세션처리 유형 입니다.
				stepCd = "007";
				throw new UserException(stepCd, messageManage.getMsgVOY("IF_INFO_104"));
			}

			/***************************************************************
			 * 5. 리턴코드 셋팅 : 정상
			 ***************************************************************/
			returnMap.put(Constant.SUCCESS_YN, Constant.SUCCESS);
			returnMap.put(Constant.RET_CODE,   Constant.SUCCESS_CODE);
			returnMap.put(Constant.RET_MSG,    Constant.SUCCESS_MSG);
			
		} catch (UserException e) {
			// 에러메시지 정의
			ErrorInfoVO errinfo = e.getErrorInfo();
			returnMap.put(Constant.SUCCESS_YN, Constant.FAIL);
			returnMap.put(Constant.RET_CODE,   errinfo.getErrCd());
			returnMap.put(Constant.RET_MSG,    errinfo.getErrMsg());			
			
			log.debug(methodName, "[UserException]stepCd="+stepCd+", errCd="+errinfo.getErrCd()+", errMsg="+errinfo.getErrMsg());
			
		} catch (Exception e) {
			// 에러 발생
			log.printStackTracePH(methodName, e);
			returnMap.put(Constant.SUCCESS_YN, Constant.FAIL);
			returnMap.put(Constant.RET_CODE,   "E999");
			returnMap.put(Constant.RET_MSG,    StringUtil.nvl(e.getMessage()));
		}

		
		/***************************************************************
		 * 6. 리턴 
		 ***************************************************************/
		return returnMap;
	}
	
	/**
	 * <pre> 로그인결과 기록(오류카운트 리셋 or 누적) </pre>
	 * 
	 * @param Map<String, Object> params
	 * @return Map<String, Object>
	 * @see <pre>
	 *      1. 로그인성공 : 오류카운트 리셋
	 *      2. 로그인실패 : 오류카운트 누적
	 *      </pre>
	 */	
	@Override
	public void updateLoginProcRst(Map<String, Object> params) throws BizException, Exception {
		/***************************************************************
		 * 0. 메소드명, 로그기록
		 ***************************************************************/		
		String methodName = "updateLoginProcRst";
		log.debug(methodName, "Start! params="+params.toString());

		/***************************************************************
		 * 1. declare
		 ***************************************************************/
		String stepCd         = "001";										// step 코드 (익셉션 발생시 사용)
		String loginErrYn     = StringUtil.nvl(params.get("loginErrYn"));	// 로그인에러여부(N:로그인성공, Y:로그인실패)
		UserInfoVO userInfoVO = (UserInfoVO) params.get("userInfoVO");		// 로그인유저 정보
		
		/***************************************************************
		 * 2. 오류결과 기록
		 ***************************************************************/
		// 로그인처리결과가 실패일 경우만 statCd를 변경한다.
		if (StringUtils.equals(loginErrYn, "Y")) {
			int loginErrCnt = Integer.valueOf(userInfoVO.getLoginErrCnt());
		    int usrPwErrCnt = Integer.valueOf(sysPrmtManage.getSysPrmtVal("USR_PW_ERR_CNT"));
		    // 로그인실패횟수가 시스템에 정의된 임계치 인지 체크
		    if (usrPwErrCnt <= (loginErrCnt+1) 
		    		&& StringUtils.equals(StringUtil.nvl(userInfoVO.getStatCd()), Constant.STAT_CD_1000)) {
		    	// 상태코드가 재직일 경우 상태코드 잠금 상태로 변경
		    	params.put("statCd", Constant.STAT_CD_4000);
		    }
		}
		
		// update : ADM_USER_INFO
		int rstDao = userInfoDao.updateLoginProcRst(params);
		if (rstDao < 1) {
			// 결과기록 실패
			throw new BizException(stepCd, messageManage.getMsgVOY("IF_INFO_104"));				
		}
		
		log.debug(methodName, "finish!");
	}
	
	/**
	 * <pre> 로그인이력 저장 처리 </pre>
	 * 
	 * @param CPReqVo cPReqVo
	 * @return Map<String, Object>
	 * @see <pre> 
	 * 		1. 이력기록 실패해도 로그인은 진행 한다.
	 *      </pre>
	 */
	@Override
	public void userLoginHistProc(Map<String, Object> params) {
		/***************************************************************
		 * 0. 메소드명, 로그기록
		 ***************************************************************/		
		String methodName = "userLoginHistProc";
		log.debug(methodName, "Start!");
		String stepCd = "001";
		
		try {
			/***************************************************************
			 * 1. declare
			 ***************************************************************/
			stepCd = "002";
			HttpServletRequest request = SystemUtils.getCurrentRequest();
			String userId      = StringUtil.nvl(params.get("userId"));
			String loginStat   = StringUtil.nvl(params.get("loginStat"));
			//String loginErrYn  = StringUtil.nvl(params.get("loginErrYn"));
			String loginErrRsn = StringUtil.nvl(params.get("loginErrRsn"));
			UserInfoVO user   = params.get("userInfoVO") != null ? (UserInfoVO) params.get("userInfoVO") : null;
			
			// set param
			stepCd = "003";
			Map<String, Object> loginHisMap = new HashMap();
			loginHisMap.put("userId",      userId);
			loginHisMap.put("loginIp",     SystemUtils.getIpAddress(request));
			loginHisMap.put("loginStat",   loginStat);
			loginHisMap.put("loginErrRsn", loginErrRsn);
			
			// 로그인 실패 카운트 셋팅 : 성공이면 0, 실패면 조회정보+1
			stepCd = "004";
			if( (Constant.LOGIN_STAT_CD_IS).equals(loginStat) ) {
				loginHisMap.put("loginErrCnt", 0);
			} else {
				if (user != null) {
					int errCnt = Integer.parseInt(StringUtil.nvl(user.getLoginErrCnt(), "0"));
					loginHisMap.put("loginErrCnt", (errCnt + 1));
				}
			}
			
			// insert ADM_USER_LOGIN_HIST
			stepCd = "006";
			this.createUserLoginHist(loginHisMap);
			log.debug(methodName, "createUserLoginHist success!");
			
		} catch (Exception e) {
			// 에러메시지 정의
			log.debug(methodName, "[Exception]stepCd="+stepCd);
			log.printStackTracePH(methodName, e);
		}
	}	

	/**
	 * <pre> 로그인이력 기록 </pre>
	 * 
	 * @param CPReqVo cPReqVo
	 * @return Map<String, Object>
	 * @see <pre> 
	 *      </pre>
	 */
	@Override
	public void createUserLoginHist(Map<String, Object> params) throws Exception {
		/***************************************************************
		 * 0. 메소드명, 로그기록
		 ***************************************************************/		
		String methodName = "createUserLoginHist";
		log.debug(methodName, "Start!");
		

		/***************************************************************
		 * 1. declare
		 ***************************************************************/
		String stepCd = "001";	// step 코드 (익셉션 발생시 사용) 

		
		try {
			/***************************************************************
			 * 2. 로그인 이력 기록
			 ***************************************************************/
			stepCd = "002";
			int cnt = userInfoDao.createUserLoginHist(params);
			if( cnt < 1 ) {
				// 이력 기록 실패
				log.error(methodName, "createUserLoginHist.cnt="+StringUtil.nvl(cnt ));
			}

			log.debug(methodName, "createUserLoginHist success!");
			
		} catch (Exception e) {
			// 에러메시지 정의
			log.printStackTracePH(methodName, e);
		}
	}
	
	/**
	 * <pre> Ldap 연결 시도 </pre>
	 * 
	 * @param loginID, loginpwd
	 * @return Boolean
	 * @see <pre> 
	 *      </pre>
	 */
	@Override
	public Boolean tryLdapConnect(String loginID, String loginpwd) throws Exception {		
		String methodName = "tryLdapConnect";
		
		String connID = sysPrmtManage.getSysPrmtVal("LDAP_CONN_ID");
		String connPwd = sysPrmtManage.getSysPrmtVal("LDAP_CONN_PWD");
		String host = sysPrmtManage.getSysPrmtVal("LDAP_CONN_HOST");
		String port = sysPrmtManage.getSysPrmtVal("LDAP_CONN_PORT");
		String baseDN = sysPrmtManage.getSysPrmtVal("LDAP_CONN_BASE");
		
		log.debug(methodName, "host="+host);
		log.debug(methodName, "port="+port);
		log.debug(methodName, "baseDN="+baseDN);
		log.debug(methodName, "connID="+connID);
		//log.debug(methodName, "connPwd="+connPwd);
		
		Boolean bLogin = false;		
		Boolean isSSL = false;
		if (port.equals("389")) {
			isSSL = false;
		} else if(port.equals("636")) {
			isSSL = true;
		}			
		bLogin = ADUtilSSL.auth_loginPeriod(host, port, baseDN, loginID, loginpwd, connID, connPwd,isSSL);		
		
		return bLogin;		
	}	
	
	/**
	 * <pre> Ldap 연결 정보 조회 </pre>
	 * 
	 * @param loginID, loginpwd
	 * @return Boolean
	 * @see <pre> 
	 *      </pre>
	 */
	@Override
	public Map<String, Object> getLdapInfo(String loginID) throws Exception {		
		String methodName = "tryLdapConnect";
		
		String connID = sysPrmtManage.getSysPrmtVal("LDAP_CONN_ID");
		String connPwd = sysPrmtManage.getSysPrmtVal("LDAP_CONN_PWD");
		String host = sysPrmtManage.getSysPrmtVal("LDAP_CONN_HOST");
		String port = sysPrmtManage.getSysPrmtVal("LDAP_CONN_PORT");
		String baseDN = sysPrmtManage.getSysPrmtVal("LDAP_CONN_BASE");
		
		log.debug(methodName, "host="+host);
		log.debug(methodName, "port="+port);
		log.debug(methodName, "baseDN="+baseDN);
		log.debug(methodName, "connID="+connID);
		log.debug(methodName, "connPwd="+connPwd);
		
		Boolean bLogin = false;		
		Boolean isSSL = false;
		if (port.equals("389")){
			isSSL = false;
		} else if(port.equals("636")){
			isSSL = true;
		}		
		Map map = new HashMap();
		map = ADUtilSSL.query_userinfo(host, port, baseDN, loginID, connID, connPwd, isSSL);		
		
		return map;		
	}	
	
}