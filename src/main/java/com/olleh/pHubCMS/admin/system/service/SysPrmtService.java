package com.olleh.pHubCMS.admin.system.service;

import java.util.List;
import java.util.Map;

import org.springframework.dao.DuplicateKeyException;

import com.olleh.pHubCMS.common.exception.BizException;

public interface SysPrmtService {

	public Map<String, Object> viewSysPrmtTotal(Map<String, Object> params);

	@SuppressWarnings("rawtypes")
	public List viewSysPrmt(Map<String, Object> params);
	
	@SuppressWarnings("rawtypes")
	public List viewSysPrmtExcel(Map<String, Object> params);
	
	public Map<String, Object> getSysPrmt(Map<String, Object> params);
	
	public int modifySysPrmt(Map<String, Object> params);
	
	public int createSysPrmt(Map<String, Object> params) throws BizException, DuplicateKeyException;
}
