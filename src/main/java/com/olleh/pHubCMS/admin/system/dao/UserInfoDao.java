package com.olleh.pHubCMS.admin.system.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.admin.system.model.UserInfoVO;
import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * 사용자 관리 Dao
 * @Class Name : UserInfotDao
 * @author : ojh
 * @since : 2018.08.22
 * @version : 1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *      수정일                 수정자                수정내용
 *  ------------  -----------    -------------
 *  2018. 8. 22.      ojh          최초 생성
 * 
 */


@Repository
public class UserInfoDao extends AbstractDAO{	
	
	public List viewUserInfo(Map<String, Object> params) {
		return selectList("mybatis.system.userInfo", params);
	}	
	public Map<String, Object> viewUserInfoTotal(Map<String, Object> params) {
		return selectOne("mybatis.system.userInfoTotal", params);	
	}		
	public List viewUserInfoExcel(Map<String, Object> params) {
		return selectList("mybatis.system.userInfoExcel", params);
	}	
	public int createUserInfo(Map<String, Object> params) {
		return insert("mybatis.system.createUserInfo", params);
	}	
	public Map<String, Object> getUserInfo(Map<String, Object> params) {
		return selectOne("mybatis.system.getUserInfo", params);
	}
	public UserInfoVO getUserInfoVO(Map<String, Object> params) {
		return sqlSession.selectOne("mybatis.system.getUserInfoVO", params);
	}	
	public int modifyUserInfo(Map<String, Object> params) {
		return update("mybatis.system.modifyUserInfo", params);
	}
	public int modifyErrCnt(Map<String, Object> params) {
		return update("mybatis.system.modifyErrCnt", params);
	}
	
	/**
	 * <pre>로그인결과 기록(오류카운트 리셋 or 누적)</pre>
	 * @param params
	 * @return
	 */
	public int updateLoginProcRst(Map<String, Object> params) {		
		return update("mybatis.system.setLoginProcRst", params);
	}	
	
	/**
	 * <pre>사용자로그인이력 기록</pre>
	 * @param params
	 * @return
	 */
	public int createUserLoginHist(Map<String, Object> params) {
		return insert("mybatis.system.userLoginHist.createUserLoginHist", params);
	}
	
	/**
	 * <pre>로그인 상태 Y 변경</pre>
	 * @param params
	 * @return
	 */
	public int modifyLoginY(Map<String, Object> params) {
		return update("mybatis.system.modifyLoginY", params);
	}
	
	/**
	 * <pre>로그인 상태 N 변경</pre>
	 * @param params
	 * @return
	 */
	public int modifyLoginN(Map<String, Object> params) {
		return update("mybatis.system.modifyLoginN", params);
	}
}
