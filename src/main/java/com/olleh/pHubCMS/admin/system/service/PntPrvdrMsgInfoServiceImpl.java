package com.olleh.pHubCMS.admin.system.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectUpdateSemanticsDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.olleh.pHubCMS.admin.system.dao.PntPrvdrMsgInfoDao;
import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.exception.BizException;

/**
 * 연동시스템 응답코드 정의 Service
 * 
 * @Class Name 	: PntPrvdrMsgInfoService
 * @author 		: jungkjae
 * @since 		: 2019.06.17
 * @version 	: 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.06.17   jungukjae  최초생성
 * 
 */
@Service
@SuppressWarnings({"rawtypes","unchecked"})
public class PntPrvdrMsgInfoServiceImpl implements PntPrvdrMsgInfoService {

	@Autowired
	MessageManage messageManage;
	
	@Autowired
	PntPrvdrMsgInfoDao pntPrvdrMsgInfoDao;
	
	@Override
	public List viewPntPrvdrMsg(Map<String, Object> params) {
		return pntPrvdrMsgInfoDao.viewPntPrvdrMsg(params);
	}
	
	@Override
	public Map<String, Object> viewPntPrvdrMsgTotal(Map<String, Object> params) {
		return pntPrvdrMsgInfoDao.viewPntPrvdrMsgTotal(params);
	}
	
	@Override
	public List viewPntPrvdrMsgExcel(Map<String, Object> params) {
		return pntPrvdrMsgInfoDao.viewPntPrvdrMsgExcel(params);
	}
	
	@Override
	public Map<String, Object> getPntPrvdrMsg(Map<String, Object> params) {
		return pntPrvdrMsgInfoDao.getPntPrvdrMsg(params);
	}
	
	@Override
	@Transactional(rollbackFor={BizException.class, IncorrectUpdateSemanticsDataAccessException.class})
	public int createPntPrvdrMsg(Map<String, Object> params) {
		return pntPrvdrMsgInfoDao.createPntPrvdrMsg(params);
	}
	
	@Override
	@Transactional(rollbackFor={BizException.class, IncorrectUpdateSemanticsDataAccessException.class})
	public int modifyPntPrvdrMsg(Map<String, Object> params) {
		return pntPrvdrMsgInfoDao.modifyPntPrvdrMsg(params);
	}
	
	@Override
	public int checkPntPrvdrMsgKey(Map<String, Object> params) {
		//Return변수		
		int keyCount;
		List keyLi = pntPrvdrMsgInfoDao.checkPntPrvdrMsgKey(params);		
		keyCount = keyLi.size();
		System.out.println("---------------------keyCount : " + keyCount);
		
		return keyCount;
	}
	
	@Override
	@Transactional(rollbackFor={BizException.class, IncorrectUpdateSemanticsDataAccessException.class})
	public int deletePntPrvdrMsg(Map<String, Object> params) {
		return pntPrvdrMsgInfoDao.deletePntPrvdrMsg(params);
	}
	
	
	
}
