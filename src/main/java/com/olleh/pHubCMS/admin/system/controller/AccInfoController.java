package com.olleh.pHubCMS.admin.system.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.IncorrectUpdateSemanticsDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.cmpn.service.PntPgCmpnServiceImpl;
import com.olleh.pHubCMS.admin.cmpn.service.PntPrvdrServiceImpl;
import com.olleh.pHubCMS.admin.cmpn.service.PntUseCmpnServiceImpl;
import com.olleh.pHubCMS.admin.system.service.AccInfoServiceImpl;
import com.olleh.pHubCMS.common.components.CodeManage;
import com.olleh.pHubCMS.common.components.Constant;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.CmnCdVO;
import com.olleh.pHubCMS.common.utils.SessionUtils;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 접근제어 정보 Controller
 * 
 * @Class Name : AccInfoController
 * @author ojh
 * @since 2018.08.23
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.23   ojh        최초생성
 * 
 */
@Controller
@SuppressWarnings({"rawtypes","unchecked"})
public class AccInfoController {
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	CodeManage codeManage;
	
	@Autowired
	AccInfoServiceImpl accInfoService;
	
	@Autowired
	PntPgCmpnServiceImpl pntPgCmpnService;
	
	@Autowired
	PntUseCmpnServiceImpl pntUseCmpnService;
	
	@Autowired
	PntPrvdrServiceImpl pntPrvdrService;
	
	/**
	 * jsp 호출 및 초기값 설정
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/system/viewAccInfo.do", method = RequestMethod.POST)
	public String viewAccInfo(Locale locale, Model model) {	
		String method = "viewAccInfo";
		log.debug(method, ">>> start");	
		try{
			//사용 'Y' 관계사 구분 가져오기
			List<CmnCdVO> indList = new ArrayList<CmnCdVO>();
			indList = codeManage.getCodeListY("CMPN_IND");
			model.addAttribute("CMPN_IND", indList);
			
			//사용 'Y' 제어 상태 코드 가져오기
			List<CmnCdVO> statList = new ArrayList<CmnCdVO>();
			statList = codeManage.getCodeListY("CTRL_IND");
			model.addAttribute("CTRL_IND", statList);
		} catch (Exception e) {
			log.printStackTracePH(method, e);
		}
		
		log.debug(method, ">>>>>> end");	
		return "/system/accInfoView";
	}
	
	/**
	 * 접근제어 관리 : 조회 
	 * 
	 * @param CTRL_ID 		Input	접근제어ID
	 * @param RLTN_CORP_IND Select	관계사구분
	 * @param CTRL_STAT 	Select	접근제어상태
	 * @param IP_ADDR 		Input	접속가능IP
	 * 
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxAccInfo.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxAccInfo(@RequestParam Map<String, Object> params) throws IOException {	
		String method = "ajaxAccInfo";
		log.debug(method, ">>> start");	
		
		HashMap map = new HashMap();	
		try {
			//Request param				
			log.debug(method,params.toString());
			
			List resultList = new ArrayList();			
			resultList = accInfoService.viewAccInfo(params);
			
			map.put("rows", resultList);
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * 접근제어 관리 : 상세정보 조회 
	 * 
	 * @param CTRL_ID 		Input	접근제어ID
	 * 
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxAccInfoDtl.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxAccInfoDtl(@RequestParam Map<String, Object> params) throws IOException {	
		String method = "ajaxAccInfoDtl";
		log.debug(method, ">>> start");	
		
		HashMap map = new HashMap();	
		try {
			//Request param				
			log.debug(method,params.toString());
			
			List resultList = new ArrayList();			
			resultList = accInfoService.viewAccInfoDtl(params);	
			
			map.put("rows", resultList);
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * 접근제어 관리 : 구분별 관계사 ID 가져오기 
	 * 
	 * @param RLTN_CORP_IND Select	관계사구분
	 * 
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxGetCorpInd.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxGetCorpInd(@RequestParam Map<String, Object> params) throws IOException {	
		String method = "ajaxGetCorpInd";
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();	
		try {
			//Request param				
			log.debug(method,params.toString());
			
			List resultList = new ArrayList();
			
			String rltnCorpInd = StringUtil.nvl(params.get("RLTN_CORP_IND"));
			if(rltnCorpInd.equals("PG")){
				resultList = pntPgCmpnService.getAllPgCmpn();
			} else if(rltnCorpInd.equals("CL")){
				resultList = pntUseCmpnService.getAllPntUseCmpn();
			} else if(rltnCorpInd.equals("PV")){
				resultList = pntPrvdrService.getAllPrvdrCmpn();
			} 
			//else if(params.get("RLTN_CORP_IND").equals("PN")){}			
			map.put("rows", resultList);
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}	
	
	/**
	 * 접근제어 관리 : 등록
	 * 
	 * @param RLTN_CORP_IND Select	관계사구분
	 * @param RLTN_CORP_ID 	Select	관계사 ID
	 * @param CTRL_STAT 	Select	제어상태
	 * 
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxCreateAccInfo.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxCreateAccInfo(@RequestParam Map<String, Object> params, HttpServletRequest req) throws IOException {	
		String method = "ajaxCreateAccInfo";
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();	
		try {			
			//Request param				
			log.debug(method,params.toString());
			//현재 로그인한 세션의 아이디 추가
			params.put("RGST_USER_ID", SessionUtils.getUserId(req));
			
			int ret = accInfoService.createAccInfo(params);
			if (ret > 0) {
				map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
			} else {
				throw new IncorrectUpdateSemanticsDataAccessException("저장에 실패하였습니다");
			}
		} catch (DuplicateKeyException | IncorrectUpdateSemanticsDataAccessException e) {
 			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			map.put(Constant.RET_MSG, e.getMessage());
			log.error(method, "Exception : "+ e);
 		} catch (Exception e) {
 			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
 			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * 접근제어 관리 : 수정
	 * 
	 * @param RLTN_CORP_IND Select	관계사구분
	 * @param RLTN_CORP_ID 	Select	관계사 ID
	 * @param CTRL_STAT 	Select	제어상태
	 * 
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxModifyAccInfo.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxModifyAccInfo(@RequestParam Map<String, Object> params, HttpServletRequest req) throws IOException {	
		String method = "ajaxModifyAccInfo";
		log.debug(method, ">>> start");					
		HashMap map = new HashMap();	
		try {			
			//현재 로그인한 세션의 아이디 추가	
			params.put("MDFY_USER_ID", SessionUtils.getUserId(req));
			//Request param		
			log.debug(method,params.toString());
			
			//제휴사 수정
			int ret = accInfoService.modifyAccInfo(params);
			if (ret > 0) {
				map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
			} else {
				throw new IncorrectUpdateSemanticsDataAccessException("저장에 실패하였습니다");
			}
		} catch (DuplicateKeyException | IncorrectUpdateSemanticsDataAccessException e) {
 			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
 			map.put(Constant.RET_MSG, e.getMessage());
 			log.error(method, "Exception : "+ e);
 		} catch (Exception e) {
 			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
 			log.printStackTracePH(method, e);
		}
		//Json Response 응답
		log.debug(method, ">>>>>> end");
		return map;			
	}
	
	
	/**
	 * 접근제어 관리 : 상세 정보 등록
	 * 
	 * @param CTRL_ID			접근제어ID 
	 * @param IP_ADDR 	Input	관계사 ID
	 * @param IP_STAT 	Select	제어상태
	 * 
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxCreateAccInfoDtl.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxCreateAccInfoDtl(@RequestParam Map<String, Object> params, HttpServletRequest req) throws IOException {	
		String method = "ajaxCreateAccInfoDtl";
		log.debug(method, ">>> start");	
				
		HashMap map = new HashMap();	
	
		try {		
			//현재 로그인한 세션의 아이디 추가
			params.put("RGST_USER_ID", SessionUtils.getUserId(req));
			//Request param	
			log.debug(method,params.toString());
			
			int ret = accInfoService.createAccInfoDtl(params);
			if (ret > 0) {
				map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
			} else {
				throw new IncorrectUpdateSemanticsDataAccessException("저장에 실패하였습니다");
			}
		} catch (DuplicateKeyException | IncorrectUpdateSemanticsDataAccessException e) {
 			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			map.put(Constant.RET_MSG, e.getMessage());
			log.error(method, "Exception : "+ e);
 		} catch (Exception e) {
 			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
 			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * 접근제어 관리 : 상세정보 수정
	 * 
	 * @param CTRL_ID  제어정보 ID
	 * 
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxModifyAccInfoDtl.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxModifyAccInfoDtl(@RequestParam Map<String, Object> params, HttpServletRequest req) throws IOException {	
		String method = "ajaxModifyAccInfoDtl";
		log.debug(method, ">>> start");
				
		HashMap map = new HashMap();	
		try {			
			//현재 로그인한 세션의 아이디 추가	
			params.put("MDFY_USER_ID", SessionUtils.getUserId(req));
			//Request param	
			log.debug(method,params.toString());
			
			int ret = accInfoService.modifyAccInfoDtl(params);
			if (ret > 0) {
				map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
			} else {
				throw new IncorrectUpdateSemanticsDataAccessException("저장에 실패하였습니다");
			}
		} catch (DuplicateKeyException | IncorrectUpdateSemanticsDataAccessException e) {
 			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			map.put(Constant.RET_MSG, e.getMessage());
			log.error(method, "Exception : "+ e);
 		} catch (Exception e) {
 			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
 			log.printStackTracePH(method, e);
		}
		//Json Response 응답
		log.debug(method, ">>>>>> end");
		return map;			
	}
}
