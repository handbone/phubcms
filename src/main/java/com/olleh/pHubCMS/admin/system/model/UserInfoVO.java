package com.olleh.pHubCMS.admin.system.model;

import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 사용자 관리 VO
 * 
 * @Class Name : UserInfoVo
 * @author ojh
 * @since 2018.08.22
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.22   ojh        최초생성
 * 
 */
public class UserInfoVO {
	
	private String userId;			// 사용자ID
	private String userNm;			// 사용자명
	private String statCd;			// 상태코드
	private String efctStrtDd;		// 유효시작일
	private String efctDndDd;		// 유효종료일
	private String cntPlc;			// 연락처
	private String loginErrCnt;		// 로그인오류카운트
	private String userGrpId;		// 사용자그룹
	private String deptCd;			// 부서코드
	private String deptNm;			// 부서명
	private String jnngPwd;			// ?
	private String ipAddr;			// IP주소
	private String loginYn;			// 로그인 여부
	private String rcntJnngDd;		// 최근 접속일
	private String rgstDt;			// 등록일시
	private String rgstUserId;		// 등록자ID
	private String mdfyDt;			// 수정일시
	private String mdfYUserId;		// 수정자ID
	private String otpAuthYn;		// OTP 인증여부(Y/N)
	private String captchaAuthYn;	// CAPCHA 인증여부(Y/N)
	private String ldapAuthYn;		// LDAP 인증여부(Y/N)
	
	/**
	 * getter / setter
	 */
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserNm() {
		return userNm;
	}
	public void setUserNm(String userNm) {
		this.userNm = userNm;
	}
	public String getStatCd() {
		return statCd;
	}
	public void setStatCd(String statCd) {
		this.statCd = statCd;
	}
	public String getEfctStrtDd() {
		return efctStrtDd;
	}
	public void setEfctStrtDd(String efctStrtDd) {
		this.efctStrtDd = efctStrtDd;
	}
	public String getEfctDndDd() {
		return efctDndDd;
	}
	public void setEfctDndDd(String efctDndDd) {
		this.efctDndDd = efctDndDd;
	}
	public String getCntPlc() {
		return cntPlc;
	}
	public void setCntPlc(String cntPlc) {
		this.cntPlc = cntPlc;
	}
	public String getLoginErrCnt() {
		return StringUtil.nvl(loginErrCnt, "0");
	}
	public void setLoginErrCnt(String loginErrCnt) {
		this.loginErrCnt = loginErrCnt;
	}
	public String getUserGrpId() {
		return userGrpId;
	}
	public void setUserGrpId(String userGrpId) {
		this.userGrpId = userGrpId;
	}
	public String getDeptCd() {
		return deptCd;
	}
	public void setDeptCd(String deptCd) {
		this.deptCd = deptCd;
	}	
	public String getDeptNm() {
		return deptNm;
	}
	public void setDeptNm(String deptNm) {
		this.deptNm = deptNm;
	}
	public String getJnngPwd() {
		return jnngPwd;
	}
	public void setJnngPwd(String jnngPwd) {
		this.jnngPwd = jnngPwd;
	}
	public String getIpAddr() {
		return ipAddr;
	}
	public void setIpAddr(String ipAddr) {
		this.ipAddr = ipAddr;
	}
	public String getRcntJnngDd() {
		return rcntJnngDd;
	}
	public void setRcntJnngDd(String rcntJnngDd) {
		this.rcntJnngDd = rcntJnngDd;
	}
	public String getRgstDt() {
		return rgstDt;
	}
	public void setRgstDt(String rgstDt) {
		this.rgstDt = rgstDt;
	}
	public String getRgstUserId() {
		return rgstUserId;
	}
	public void setRgstUserId(String rgstUserId) {
		this.rgstUserId = rgstUserId;
	}
	public String getMdfyDt() {
		return mdfyDt;
	}
	public void setMdfyDt(String mdfyDt) {
		this.mdfyDt = mdfyDt;
	}
	public String getMdfYUserId() {
		return mdfYUserId;
	}
	public void setMdfYUserId(String mdfYUserId) {
		this.mdfYUserId = mdfYUserId;
	}
	public String getLoginYn() {
		return loginYn;
	}
	public void setLoginYn(String loginYn) {
		this.loginYn = loginYn;
	}
	public String getOtpAuthYn() {
		return otpAuthYn;
	}
	public void setOtpAuthYn(String otpAuthYn) {
		this.otpAuthYn = otpAuthYn;
	}
	public String getCaptchaAuthYn() {
		return captchaAuthYn;
	}
	public void setCaptchaAuthYn(String captchaAuthYn) {
		this.captchaAuthYn = captchaAuthYn;
	}
	public String getLdapAuthYn() {
		return ldapAuthYn;
	}
	public void setLdapAuthYn(String ldapAuthYn) {
		this.ldapAuthYn = ldapAuthYn;
	}
}
