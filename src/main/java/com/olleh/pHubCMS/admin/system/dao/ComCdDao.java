package com.olleh.pHubCMS.admin.system.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * 공통코드 관리 Dao
 * @Class Name : ComCdDao
 * @author : bmg
 * @since : 2018.09.11
 * @version : 1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *      수정일                 수정자                수정내용
 *  ------------  -----------    -------------
 *  2018. 9. 11.      bmg          최초 생성
 * 
 */
@Repository
public class ComCdDao extends AbstractDAO {
	
	@SuppressWarnings("rawtypes")
	public List viewComCd(Map<String, Object> params) {
		return selectList("mybatis.system.comCd", params);
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> getComCd(Map<String, Object> params) {
		return selectOne("mybatis.system.getComCd", params);
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> getComCdDuplTotal(Map<String, Object> params) {
		return selectOne("mybatis.system.getComCdDuplTotal", params);
	}
	
	public int createComCd(Map<String, Object> params) {
		return insert("mybatis.system.createComCd", params);
	}
	
	public int modifyComCd(Map<String, Object> params) {
		return update("mybatis.system.modifyComCd", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List viewDtlCd(Map<String, Object> params) {
		return selectList("mybatis.system.dtlCd", params);
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> getDtlCd(Map<String, Object> params) {
		return selectOne("mybatis.system.getDtlCd", params);
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> getDtlCdDuplTotal(Map<String, Object> params) {
		return selectOne("mybatis.system.getDtlCdDuplTotal", params);
	}
	
	public int createDtlCd(Map<String, Object> params) {
		return insert("mybatis.system.createDtlCd", params);
	}
	
	public int modifyDtlCd(Map<String, Object> params) {
		return update("mybatis.system.modifyDtlCd", params);
	}
}
