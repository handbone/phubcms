package com.olleh.pHubCMS.admin.system.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * 메뉴 정보 Dao
 * @Class Name : MenuInfoDao
 * @author : ojh
 * @since : 2018.08.24
 * @version : 1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *      수정일                 수정자                수정내용
 *  ------------  -----------    -------------
 *  2018. 8. 24      ojh          최초 생성
 * 
 */
@Repository
public class MenuInfoDao extends AbstractDAO{
	
	public List getMenuInfo(Map<String, Object> params) {
		return selectList("mybatis.system.getMenuInfo", params);
	}	
	public List getMenuInfoLvl(Map<String, Object> params) {
		return selectList("mybatis.system.getMenuInfoLvl", params);
	}	
	public List getMenuInfoUpr(Map<String, Object> params) {
		return selectList("mybatis.system.getMenuInfoUpr", params);
	}
	public int chkMenuAuth(Map<String, Object> params) {
		return insert("mybatis.system.chkMenuAuth", params);
	}	
	public int unChkMenuAuth(Map<String, Object> params) {
		return delete("mybatis.system.unChkMenuAuth", params);
	}	
	
	@SuppressWarnings("rawtypes")
	public List menuInfoView(Map<String, Object> params) {
		return selectList("mybatis.system.menuInfoView", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List uprMenuInfo(Map<String, Object> params) {
		return selectList("mybatis.system.uprMenuInfo", params);
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> getMenuInfoDuplTotal(Map<String, Object> params) {
		return selectOne("mybatis.system.getMenuInfoDuplTotal", params);
	}
	
	public int createMenuInfo(Map<String, Object> params) {
		return insert("mybatis.system.createMenuInfo", params);
	}
	
	public int modifyMenuInfo(Map<String, Object> params) {
		return update("mybatis.system.modifyMenuInfo", params);
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> getMenuInfoView(Map<String, Object> params) {
		return selectOne("mybatis.system.getMenuInfoView", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List actnInfoView(Map<String, Object> params) {
		return selectList("mybatis.system.actnInfoView", params);
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> getActnInfo(Map<String, Object> params) {
		return selectOne("mybatis.system.getActnInfo", params);
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> getActnInfoDuplTotal(Map<String, Object> params) {
		return selectOne("mybatis.system.getActnInfoDuplTotal", params);
	}
	
	public int createActnInfo(Map<String, Object> params) {
		return insert("mybatis.system.createActnInfo", params);
	}
	
	public int modifyActnInfo(Map<String, Object> params) {
		return update("mybatis.system.modifyActnInfo", params);
	}
	
	/**
	 * <pre> 사용자메뉴리스트조회 </pre>
	 * 
	 * @param Map<String, Object> params
	 * @return List
	 * @see <pre>
	 *      1. 조회 파라미터로 "userGrpId"를 넘긴다.
	 *      </pre>
	 */		
	public List getUserMenuList(Map<String, Object> params) {
		return selectList("mybatis.system.getMenuListByUsrGrpId", params);
	}
}
