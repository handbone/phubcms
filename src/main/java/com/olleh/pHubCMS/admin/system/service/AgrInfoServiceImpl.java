package com.olleh.pHubCMS.admin.system.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.system.dao.AgrInfoDao;
import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.exception.BizException;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 약관 정보 관리 Service
 * 
 * @Class Name 	: AgrInfoService
 * @author 		: bmg
 * @since 		: 2019.05.07
 * @version 	: 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.05.07   bmg        최초생성
 * 
 */
@Service
@SuppressWarnings({"rawtypes","unchecked"})
public class AgrInfoServiceImpl implements AgrInfoService {
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
	AgrInfoDao agrInfoDao;

	@Override
	public List viewAgrInfo(Map<String, Object> params) {
		return agrInfoDao.viewAgrInfo(params);
	}

	@Override
	public Map agrInfo(Map<String, Object> params) {
		return maskingName(agrInfoDao.agrInfo(params));
	}

	@Override
	public List<Map> viewPrvdrItem(Map<String, Object> params) {
		return agrInfoDao.viewPrvdrItem(params);
	}

	@Override
	public Map prvdrItem(Map<String, Object> params) {
		return maskingName(agrInfoDao.prvdrItem(params));
	}

	@Override
	public int insertAgrInfo(Map<String, Object> params) throws BizException, DuplicateKeyException {
		int ret = 0;
		try {
			Map<String, Object> map = agrInfoDao.agrInfo(params);
			if (map != null) {
				throw new DuplicateKeyException(messageManage.getMsgTxt("AD_INFO_157"));
			}
			params.put("APLY_STRT_DD", StringUtil.nvl(params.get("APLY_STRT_DD")).replaceAll("-", ""));
			params.put("APLY_END_DD", StringUtil.nvl(params.get("APLY_END_DD")).replaceAll("-", ""));
			params.put("SORT_ORD", Integer.parseInt(StringUtil.nvl(params.get("SORT_ORD"), "0")));
			ret = agrInfoDao.insertAgrInfo(params);
		} catch (DuplicateKeyException de) {
			throw de;
		} catch (Exception e) {
			throw new BizException(e);
		}
		return ret;
	}

	@Override
	public int updateAgrInfo(Map<String, Object> params) {
		params.put("APLY_STRT_DD", StringUtil.nvl(params.get("APLY_STRT_DD")).replaceAll("-", ""));
		params.put("APLY_END_DD", StringUtil.nvl(params.get("APLY_END_DD")).replaceAll("-", ""));
		params.put("SORT_ORD", Integer.parseInt(StringUtil.nvl(params.get("SORT_ORD"), "0")));
		return agrInfoDao.updateAgrInfo(params);
	}

	@Override
	public int insertPrvdrItem(Map<String, Object> params) throws BizException, DuplicateKeyException {
		int ret = 0;
		try {
			Map<String, Object> map = agrInfoDao.prvdrItem(params);
			if (map != null) {
				throw new DuplicateKeyException(messageManage.getMsgTxt("AD_INFO_157"));
			}
			params.put("SORT_ORD", Integer.parseInt(StringUtil.nvl(params.get("SORT_ORD"), "0")));
			ret = agrInfoDao.insertPrvdrItem(params);
		} catch (DuplicateKeyException de) {
			throw de;
		} catch (Exception e) {
			throw new BizException(e);
		}
		return ret;
	}

	@Override
	public int updatePrvdrItem(Map<String, Object> params) {
		params.put("SORT_ORD", Integer.parseInt(StringUtil.nvl(params.get("SORT_ORD"), "0")));
		return agrInfoDao.updatePrvdrItem(params);
	}
	/**
	 * 이름 마스킹 처리
	 * 
	 * @param params
	 * @return
	 */
	private Map maskingName(Map map) {
		if (map != null) {
			if (map.get("rgst_user_nm") != null) {
				map.put("rgst_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("rgst_user_nm"))));
			}
			if (map.get("mdfy_user_nm") != null) {
				map.put("mdfy_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("mdfy_user_nm"))));
			}
		}
		return map;
	}
}
