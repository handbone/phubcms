package com.olleh.pHubCMS.admin.system.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.login.service.LoginService;
import com.olleh.pHubCMS.admin.system.service.UserInfoServiceImpl;
import com.olleh.pHubCMS.common.components.CodeManage;
import com.olleh.pHubCMS.common.components.SysPrmtManage;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.CmnCdVO;
import com.olleh.pHubCMS.common.utils.Common;
import com.olleh.pHubCMS.common.utils.Paging;
import com.olleh.pHubCMS.common.utils.SessionUtils;
import com.olleh.pHubCMS.common.utils.StringUtil;


/**
 * 사용자 정보 Controller
 * 
 * @Class Name : UserInfoController
 * @author ojh
 * @since 2018.08.22
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.22   ojh        최초생성
 * 
 */


@Controller
public class UserInfoController {
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	CodeManage codeManage;	
	
	@Autowired
	SysPrmtManage sysPrmtManage;
	
	@Autowired
	UserInfoServiceImpl userInfoService;
	
	@Autowired
	LoginService loginService;
	
	
	/**
	 * jsp 호출 및 초기값 설정
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/system/viewUserInfo.do", method = RequestMethod.POST)
	public String viewUserInfo(Locale locale, Model model) {	
		String method = "viewUserInfo";
		log.debug(method, ">>> start");	
		
		try{
			//사용 'Y' 사용자 그룹 코드 가져오기
			List<CmnCdVO> indList = new ArrayList<CmnCdVO>();
			indList = codeManage.getCodeListY("USER_GRP");
			model.addAttribute("USER_GRP", indList);
					
			//사용 'Y' 부서코드 가져오기
			List<CmnCdVO> deptList = new ArrayList<CmnCdVO>();
			deptList = codeManage.getCodeListY("USER_DEPT");
			model.addAttribute("USER_DEPT", deptList);	
			
			//사용 'Y' 상태코드 가져오기
			List<CmnCdVO> statList = new ArrayList<CmnCdVO>();
			statList = codeManage.getCodeListY("STAT_CD");
			model.addAttribute("STAT_CD", statList);
			
			//로그인 오류 횟수 가져오기
			model.addAttribute("USR_PW_ERR_CNT", sysPrmtManage.getSysPrmtVal("USR_PW_ERR_CNT"));
			
			
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}
		
		log.debug(method, ">>>>>> end");	
		return "/system/userInfoView";
	}

	
	/**
	 * JQGrid 호출
	 * 
	 * @param page 현재 보고자 하는 페이지 (JQgrid 처리)
	 * @param rows 한 페이지에 보여주고자 설정된 개수 (JQgrid 처리)
	 * @param USER_ID		Input 사용자아이디
	 * @param USER_NM		Input 사용자 명
	 * @param USER_GRP_ID	SELECT 유저 그룹
	 * @param STAT_CD		SELECT 사용자상태 코드
	 * 
	 * @return map 정보
	 */
	@ResponseBody
	@RequestMapping(value = "/system/jgUserInfo.do", method = RequestMethod.POST)
	public Map<String, Object> jgUserInfo(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "jgUserInfo";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		Map<String, Object> count = new HashMap<String, Object>();
		try{
			//Request param
			int pageNum = Integer.parseInt(req.getParameter("page"));
			int limit = Integer.parseInt(req.getParameter("rows"));	
			log.debug(method,params.toString());
			
			//총 Row 수 계산 		
			count = userInfoService.viewUserInfoTotal(params);		
			int countRow = Integer.parseInt(count.get("cnt").toString());
			
			
			if(countRow > 0){
				//HashMap에 페이징 정보 저장					
				Paging.setPageMap(params, map, pageNum, limit, countRow);	
				log.debug(method,map.toString());
				
				//검색 결과 데이터 map 에 추가
				List resultList = new ArrayList();
				log.debug(method,params.toString());
				resultList = userInfoService.viewUserInfo(params);	
				
				mappingUserInfo(resultList);
				map.put("rows", resultList );
			}
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}
		
		log.debug(method, ">>>>>> end");	
		return map;
	}	
	
	/**
	 * JQGrid 호출
	 * 
	 * @param page 현재 보고자 하는 페이지 (JQgrid 처리)
	 * @param rows 한 페이지에 보여주고자 설정된 개수 (JQgrid 처리)
	 * @param USER_ID		Input 사용자아이디
	 * @param USER_NM		Input 사용자 명
	 * @param USER_GRP_ID	SELECT 유저 그룹
	 * @param STAT_CD		SELECT 사용자상태 코드
	 * 
	 * @return map 정보
	 */
	@ResponseBody
	@RequestMapping(value = "/system/jgUserInfoExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgUserInfoExcel(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "jgUserInfoExcel";
		log.debug(method, ">>> start");			
		HashMap map = new HashMap();
		try{
			//Request param
			log.debug(method,params.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			resultList = userInfoService.viewUserInfo(params);	
			
			mappingUserInfo(resultList);
			map.put("rows", resultList );
			
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}

	/**
	 * 사용자 관리 : 등록
	 * 
	 * @param USER_ID 		Input 	사용자아이디
	 * @param USER_NM 		Input 	사용자명
	 * @param USER_GRP_ID   Select  사용자그룹아이디
	 * @param STAT_CD 		Select  상태코드
	 * @param CNT_PLC 		Input   연락처
	 * @param DEPT_CD 		Select  부서코드
	 * @param IP_ADDR 		Input   IP주소
	 * @param EFCT_STRT_DD 	달력 선택  사용가능 일자
	 * @param EFCT_END_DD	달력 선택  사용가능 일자
	 *  
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxCreateUserInfo.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxCreateUserInf(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "ajaxCreateUserInf";
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		try {
			
			Map<String, Object> user = userInfoService.getUserInfo(params);
			
			if(user == null){
				//현재 로그인한 세션의 아이디 추가
				params.put("RGST_USER_ID", SessionUtils.getUserId(req));
				//Request param				
				log.debug(method,params.toString());			
				userInfoService.createUserInfo(params);
				map.put("eStat", "0");				
			} else{
				map.put("eStat", "1");	
			}
			map.put("eCode", "0");
		} catch (Exception e) {
			map.put("eCode", "-1");
			log.error(method, "Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * 사용자 관리 : 상세 조회 (수정)
	 * 
	 * @param USER_ID 사용자 아이디
	 * 
	 * @return map 정보
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxGetUserInfo.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxGetUserInfo(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "ajaxGetUserInfo";
		log.debug(method, ">>> start");			
		HashMap map = new HashMap();	
		Map<String, Object> user = new HashMap<String, Object>();
		try {		
			//Request param				
			log.debug(method,params.toString());
			
			user = userInfoService.getUserInfo(params);
			
			map.put("row", user);
			map.put("eCode", "0");
		} catch (Exception e) {
			map.put("eCode", "-1");
			log.error(method, "Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");
		return map;
	}	
	
	/**
	 * 사용자 관리 수정 저장
	 * 
	 * @param USER_ID 		Input 	사용자아이디
	 * @param USER_NM 		Input 	사용자명
	 * @param USER_GRP_ID   Select  사용자그룹아이디
	 * @param STAT_CD 		Select  상태코드
	 * @param CNT_PLC 		Input   연락처
	 * @param DEPT_CD 		Select  부서코드
	 * @param IP_ADDR 		Input   IP주소
	 * @param EFCT_STRT_DD 	달력 선택  사용가능 일자
	 * @param EFCT_END_DD	달력 선택  사용가능 일자
	 * 
	 * @return map 정보
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxModifyUserInfo.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxModifyUserInfo(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "ajaxModifyUserInfo";
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		try {
			//현재 로그인한 세션의 아이디 추가
			params.put("MDFY_USER_ID", SessionUtils.getUserId(req));		
			if(params.get("JNNG_PWD")!=null && !params.get("JNNG_PWD").equals("")){
				//비밀번호 sha-256 암호화
				params.put("JNNG_PWD_SHA", Common.sha1(params.get("JNNG_PWD").toString()));
			}			
			//Request param		
			log.debug(method,params.toString());
		
			userInfoService.modifyUserInfo(params);
			map.put("eCode", "0");
		} catch (Exception e) {
			map.put("eCode", "-1");
			log.error(method,"Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	
	/**
	 * 로그인 실패횟수 초기화
	 * 
	 * @param USER_ID 		Input 	사용자아이디
	 * 
	 * @return map 정보
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxModifyErrCnt.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxModifyErrCnt(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "ajaxModifyErrCnt";
		log.debug(method, ">>> start");	
		
		HashMap map = new HashMap();	
		try {
			//Request param		
			log.debug(method,params.toString());
			
			userInfoService.modifyErrCnt(params);
			map.put("eCode", "0");
		} catch (Exception e) {
			map.put("eCode", "-1");
			log.error(method, "Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	
	/**
	 * Ldap 계정 연동 확인
	 * 
	 * @param USER_ID 		Input 	사용자아이디
	 * @param USER_PW 		Input 	비밀번호
	 * 
	 * @return map 정보
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxTestLdapId.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxTestLdapId(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "ajaxTestLdapId";
		log.debug(method, ">>> start");	
		
		Map<String, Object> map = new HashMap();	
		try {			
			Map<String, Object> user = userInfoService.getUserInfo(params);			
			if(user == null){		
				Boolean bLogin = loginService.tryLdapConnect(params.get("USER_ID").toString(), params.get("USER_PW").toString());	
				if(bLogin){
					map = loginService.getLdapInfo(params.get("USER_ID").toString());
					map.put("eStat", "0");
				} 			
			} else{
				//존재하는 아이디
				map.put("eStat", "1");	
			}
			map.put("eCode", "0");
		} catch (Exception e) {
			if(e.getMessage().indexOf("data 0004") > 0){
				//비밀번호 만료 상태 eStat = 4
				map.put("eCode","0");
				map.put("eStat","4");
				log.error(method, "Exception : "+ e.getMessage());
			} else if(e.getMessage().indexOf("data") > 0){
				map.put("eCode","0");
				map.put("eStat","2");
				log.error(method, "Exception : "+ e.getMessage());
			} else{
				map.put("eCode", "-1");
				log.printStackTracePH(method, e);
			}
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * 사용자 정보 관리 code -> 이름 변환
	 * 마스킹 처리
	 * 
	 * @param List 사용자 정보 리스트
	 * @return void 
	 */
	public void mappingUserInfo(List resultList){
		for(int i=0; (resultList != null) && (i < resultList.size()); i++){
			Map<String, Object> resultTmp = (Map<String, Object>) resultList.get(i);			
			//마스킹 처리
			resultTmp.put("cnt_plc", StringUtil.getMaskingPhone(resultTmp.get("cnt_plc").toString()));
			resultTmp.put("user_nm", StringUtil.getMaskingName(resultTmp.get("user_nm").toString()));
			resultTmp.put("m_user_id", StringUtil.getMaskingId(resultTmp.get("user_id").toString()));
			resultList.set(i, resultTmp);
		}	
		
	}
	
}
