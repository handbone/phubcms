package com.olleh.pHubCMS.admin.system.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.system.dao.SysPrmtDao;
import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.exception.BizException;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 시스템파라미터 Service
 * 
 * @Class Name : SysPrmtServiceImpl
 * @author bmg
 * @since 2018.09.10
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.10   bmg        최초생성
 */
@Service
@SuppressWarnings({"rawtypes", "unchecked"})
public class SysPrmtServiceImpl implements SysPrmtService {
	
	@Autowired
	SysPrmtDao sysPrmtDao;
	
	@Autowired
	MessageManage messageManage;
	
	@Override
	public Map<String, Object> viewSysPrmtTotal(Map<String, Object> params) {
		return sysPrmtDao.viewSysPrmtTotal(params);
	}
	
	@Override
	public List viewSysPrmt(Map<String, Object> params) {
		return maskingName(sysPrmtDao.viewSysPrmt(params));
	}

	@Override
	public List viewSysPrmtExcel(Map<String, Object> params) {
		return maskingName(sysPrmtDao.viewSysPrmtExcel(params));
	}

	@Override
	public Map<String, Object> getSysPrmt(Map<String, Object> params) {
		return maskingName(sysPrmtDao.getSysPrmt(params));
	}

	@Override
	public int modifySysPrmt(Map<String, Object> params) {
		return sysPrmtDao.modifySysPrmt(params);
	}

	@Override
	public int createSysPrmt(Map<String, Object> params) throws BizException, DuplicateKeyException {
		int ret = 0;
		try {
			Map<String, Object> dupl = sysPrmtDao.getSysPrmtDuplTotal(params);
			if (Integer.parseInt(dupl.get("cnt").toString()) > 0) {
				throw new DuplicateKeyException(messageManage.getMsgTxt("AD_INFO_157"));
			}
			ret = sysPrmtDao.createSysPrmt(params);
		} catch (DuplicateKeyException de) {
			throw de;
		} catch (Exception e) {
			throw new BizException(e);
		}
		return ret;
	}
	/**
	 * 이름 마스킹 처리
	 * 
	 * @param params
	 * @return
	 */
	private List maskingName(List<Map> params) {
		int index = 0;
		if (params == null) {
			return params;
		}
		for (Map map : params) {
			if (map == null) {
				continue;
			}
			if (map.get("rgst_user_nm") != null) {
				map.put("rgst_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("rgst_user_nm"))));
			}
			if (map.get("mdfy_user_nm") != null) {
				map.put("mdfy_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("mdfy_user_nm"))));
			}
			params.set(index, map);
			index++;
		}
		return params;
	}
	/**
	 * 이름 마스킹 처리
	 * 
	 * @param params
	 * @return
	 */
	private Map maskingName(Map map) {
		if (map != null) {
			if (map.get("rgst_user_nm") != null) {
				map.put("rgst_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("rgst_user_nm"))));
			}
			if (map.get("mdfy_user_nm") != null) {
				map.put("mdfy_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("mdfy_user_nm"))));
			}
		}
		return map;
	}
}
