package com.olleh.pHubCMS.admin.system.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.IncorrectUpdateSemanticsDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.system.service.SysPrmtService;
import com.olleh.pHubCMS.common.components.CodeManage;
import com.olleh.pHubCMS.common.components.Constant;
import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.CmnCdVO;
import com.olleh.pHubCMS.common.utils.Paging;
import com.olleh.pHubCMS.common.utils.SessionUtils;

/**
 * 시스템파라미터 Controller
 * 
 * @Class Name : SysPrmtController
 * @author bmg
 * @since 2018.09.07
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.07   bmg        최초생성
 * 
 */

@Controller
public class SysPrmtController {
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	CodeManage codeManage;
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
	SysPrmtService sysPrmtService;
	
	/**
	 * jsp 호출 및 초기값 설정
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/system/viewSysPrmt.do", method = RequestMethod.POST)
	public String viewSysPrmt(Locale locale, Model model) {	
		String method = "viewSysPrmt";
		log.debug(method, ">>> start");	
		
		List<CmnCdVO> prmtGrpList = new ArrayList<CmnCdVO>();
		prmtGrpList = codeManage.getCodeListY("PRMT_GRP");
		model.addAttribute("PRMT_GRP", prmtGrpList.toArray());
		
		log.debug(method, ">>>>>> end");	
		return "/system/sysPrmtView";
	}
	
	/**
	 * 시스템파라미터 JQGrid 호출
	 * 
	 * @param params
	 * @param req
	 * @param res
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/system/jgSysPrmt.do", method = RequestMethod.POST)
	public Map<String, Object> jgSysPrmt(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {
		String method = "jgSysPrmt";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		int pageNum = Integer.parseInt(req.getParameter("page"));
		int limit = Integer.parseInt(req.getParameter("rows"));	
		log.debug(method,params.toString());
		
		count = sysPrmtService.viewSysPrmtTotal(params);
		int countRow = Integer.parseInt(count.get("cnt").toString());
		
		HashMap map = new HashMap();
		if (countRow > 0) {
			//HashMap에 페이징 정보 저장					
			Paging.setPageMap(params, map, pageNum, limit, countRow);	
			log.debug(method,map.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			log.debug(method,params.toString());
			resultList = sysPrmtService.viewSysPrmt(params);
			
			mappingSysPrmt(resultList);
			map.put("rows", resultList);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 시스템파라미터 JQGrid 호출 엑셀
	 * 
	 * @param params
	 * @param req
	 * @param res
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/system/jgSysPrmtExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgSysPrmtExcel(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {
		String method = "jgSysPrmtExcel";
		log.debug(method, ">>> start");	
		//Request param
		log.debug(method,params.toString());
		HashMap map = new HashMap();
		//검색 결과 데이터 map 에 추가
		List resultList = new ArrayList();
		resultList = sysPrmtService.viewSysPrmtExcel(params);
		
		mappingSysPrmt(resultList);
		map.put("rows", resultList );
		
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * 시스템파라미터 상세 조회(수정)
	 * 
	 * @param params
	 * @param req
	 * @param res
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/system/ajaxGetSysPrmt.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxGetSysPrmt(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {
		String method = "ajaxGetSysPrmt";
		log.debug(method, ">>> start");	
		//Request param				
		log.debug(method,params.toString());
		
		HashMap map = new HashMap();	
		Map<String, Object> sysPrmt = new HashMap<String, Object>();
		try {
			sysPrmt = sysPrmtService.getSysPrmt(params);
			map.put("row", sysPrmt);
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 시스템파라미터 관리 수정
	 * 
	 * @param params
	 * @param req
	 * @param res
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/system/ajaxModifySysPrmt.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxModifySysPrmt(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {
		String method = "ajaxModifySysPrmt";
		log.debug(method, ">>> start");	
		//현재 로그인한 세션의 아이디 추가
		params.put("MDFY_USER_ID", SessionUtils.getUserId(req));
		//Request param		
		log.debug(method,params.toString());
		
		HashMap map = new HashMap();
		try {
			int ret = sysPrmtService.modifySysPrmt(params);
			if (ret > 0) {
				map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
			} else {
				throw new IncorrectUpdateSemanticsDataAccessException("저장에 실패하였습니다");
			}
		} catch (IncorrectUpdateSemanticsDataAccessException e) {
 			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
 			map.put(Constant.RET_MSG, e.getMessage());
 			log.error(method, "Exception : "+ e);
 		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 시스템파라미터 등록
	 * 
	 * @param params
	 * @param req
	 * @param res
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/system/ajaxCreateSysPrmt.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxCreateSysPrmt(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {
		String method = "ajaxCreateSysPrmt";
		log.debug(method, ">>> start");	
		//현재 로그인한 세션의 아이디 추가
		params.put("RGST_USER_ID", SessionUtils.getUserId(req));
		//Request param				
		log.debug(method,params.toString());
		
		HashMap map = new HashMap();
		try {
			int ret = sysPrmtService.createSysPrmt(params);
			if (ret > 0) {
				map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
			} else {
				throw new IncorrectUpdateSemanticsDataAccessException("저장에 실패하였습니다");
			}
		} catch (DuplicateKeyException | IncorrectUpdateSemanticsDataAccessException e) {
 			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			map.put(Constant.RET_MSG, e.getMessage());
			log.error(method, "Exception : "+ e);
 		} catch (Exception e) {
 			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
 			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 시스템파라미터 정보 code -> 이름 변환
	 * 
	 * @param resultList
	 */
	@SuppressWarnings({"rawtypes", "unchecked"})
	public void mappingSysPrmt(List resultList){
		for(int i=0; i<resultList.size(); i++) {
			Map<String, Object> resultTmp = (Map<String, Object>) resultList.get(i);
			if (resultTmp.get("use_yn") != null && resultTmp.get("use_yn").equals("Y")) {
				resultTmp.put("use_yn", "사용");
			} else {
				resultTmp.put("use_yn", "미사용");
			}
			resultList.set(i, resultTmp);
		}
	}
}
