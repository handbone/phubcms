package com.olleh.pHubCMS.admin.system.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.system.dao.MenuInfoDao;
import com.olleh.pHubCMS.common.components.Constant;
import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.exception.BizException;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 메뉴 정보 Service
 * 
 * @Class Name : MenuInfoServiceImpl
 * @author bmg
 * @since 2018.08.22
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.22   bmg        최초생성
 */
@Service
@SuppressWarnings({"rawtypes", "unchecked"})
public class MenuInfoServiceImpl implements MenuInfoService{
	
	@Autowired
	MenuInfoDao menuInfoDao;
	
	@Autowired
	MessageManage messageManage;
	
	@Override
	public List getMenuInfoLvl(Map<String, Object> params){
		return menuInfoDao.getMenuInfoLvl(params);		
	}
	@Override
	public List getMenuInfoUpr(Map<String, Object> params) {
		return menuInfoDao.getMenuInfoUpr(params);
	}
	@Override
	public List getMenuInfo(Map<String, Object> params) {
		return menuInfoDao.getMenuInfo(params);
	}
	@Override
	public int chkMenuAuth(Map<String, Object> params) {
		return menuInfoDao.chkMenuAuth(params);
	}
	@Override
	public int unChkMenuAuth(Map<String, Object> params) {
		return menuInfoDao.unChkMenuAuth(params);
	}
	
	@Override
	public List menuInfoView(Map<String, Object> params) {
		return menuInfoDao.menuInfoView(params);
	}
	
	@Override
	public List uprMenuInfo(Map<String, Object> params) {
		return menuInfoDao.uprMenuInfo(params);
	}
	
	@Override
	public int createMenuInfo(Map<String, Object> params) throws BizException, DuplicateKeyException {
		int ret = 0;
		try {
			Map<String, Object> dupl = menuInfoDao.getMenuInfoDuplTotal(params);
			if (Integer.parseInt(dupl.get("cnt").toString()) > 0) {
				throw new DuplicateKeyException(messageManage.getMsgTxt("AD_INFO_157"));
			}
			params.put("MENU_LVL", Integer.parseInt(params.get("MENU_LVL").toString()));
			params.put("MENU_ORD", Integer.parseInt(params.get("MENU_ORD").toString()));
			params.put("ROOT_MENU", Constant.MENU_ROOT_ID);
			ret = menuInfoDao.createMenuInfo(params);
		} catch (DuplicateKeyException de) {
			throw de;
		} catch (Exception e) {
			throw new BizException(e);
		}
		return ret;
	}
	
	@Override
	public int modifyMenuInfo(Map<String, Object> params) {
		params.put("MENU_LVL", Integer.parseInt(params.get("MENU_LVL").toString()));
		params.put("MENU_ORD", Integer.parseInt(params.get("MENU_ORD").toString()));
		return menuInfoDao.modifyMenuInfo(params);
	}
	
	@Override
	public Map<String, Object> getMenuInfoView(Map<String, Object> params) {
		return maskingName(menuInfoDao.getMenuInfoView(params));
	}
	
	@Override
	public List actnInfoView(Map<String, Object> params) {
		return maskingName(menuInfoDao.actnInfoView(params));
	}
	
	@Override
	public Map<String, Object> getActnInfo(Map<String, Object> params) {
		return maskingName(menuInfoDao.getActnInfo(params));
	}
	
	@Override
	public int createActnInfo(Map<String, Object> params) throws BizException, DuplicateKeyException {
		int ret = 0;
		try {
			Map<String, Object> dupl = menuInfoDao.getActnInfoDuplTotal(params);
			if (Integer.parseInt(dupl.get("cnt").toString()) > 0) {
				throw new DuplicateKeyException(messageManage.getMsgTxt("AD_INFO_157"));
			}
			ret = menuInfoDao.createActnInfo(params);
		} catch (DuplicateKeyException de) {
			throw de;
		} catch (Exception e) {
			throw new BizException(e);
		}
		return ret;
	}
	
	@Override
	public int modifyActnInfo(Map<String, Object> params) {
		return menuInfoDao.modifyActnInfo(params);
	}
	/**
	 * 이름 마스킹 처리
	 * 
	 * @param params
	 * @return
	 */
	private List maskingName(List<Map> params) {
		int index = 0;
		if (params == null) {
			return params;
		}
		for (Map map : params) {
			if (map.get("rgst_user_nm") != null) {
				map.put("rgst_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("rgst_user_nm"))));
			}
			if (map.get("mdfy_user_nm") != null) {
				map.put("mdfy_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("mdfy_user_nm"))));
			}
			params.set(index, map);
			index++;
		}
		return params;
	}
	/**
	 * 이름 마스킹 처리
	 * 
	 * @param params
	 * @return
	 */
	private Map maskingName(Map map) {
		if (map != null) {
			if (map.get("rgst_user_nm") != null) {
				map.put("rgst_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("rgst_user_nm"))));
			}
			if (map.get("mdfy_user_nm") != null) {
				map.put("mdfy_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("mdfy_user_nm"))));
			}
		}
		return map;
	}
}
