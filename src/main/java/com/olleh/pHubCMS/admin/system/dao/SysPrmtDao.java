package com.olleh.pHubCMS.admin.system.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * 시스템파라미터 관리 Dao
 * @Class Name : SysPrmtDao
 * @author : bmg
 * @since : 2018.09.10
 * @version : 1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *      수정일                 수정자                수정내용
 *  ------------  -----------    -------------
 *  2018. 9. 10.      bmg          최초 생성
 * 
 */

@Repository
public class SysPrmtDao extends AbstractDAO {
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> viewSysPrmtTotal(Map<String, Object> params) {
		return selectOne("mybatis.system.sysPrmtTotal", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List viewSysPrmt(Map<String, Object> params) {
		return selectList("mybatis.system.sysPrmt", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List viewSysPrmtExcel(Map<String, Object> params) {
		return selectList("mybatis.system.sysPrmtExcel", params);
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> getSysPrmt(Map<String, Object> params) {
		return selectOne("mybatis.system.getSysPrmt", params);
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> getSysPrmtDuplTotal(Map<String, Object> params) {
		return selectOne("mybatis.system.getSysPrmtDuplTotal", params);
	}
	
	public int modifySysPrmt(Map<String, Object> params) {
		return update("mybatis.system.modifySysPrmt", params);
	}
	
	public int createSysPrmt(Map<String, Object> params) {
		return insert("mybatis.system.createSysPrmt", params);
	}
}
