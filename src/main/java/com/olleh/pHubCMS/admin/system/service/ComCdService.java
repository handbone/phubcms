package com.olleh.pHubCMS.admin.system.service;

import java.util.List;
import java.util.Map;

import org.springframework.dao.DuplicateKeyException;

import com.olleh.pHubCMS.common.exception.BizException;

@SuppressWarnings("rawtypes")
public interface ComCdService {

	public List viewComCd(Map<String, Object> params);
	
	public Map<String, Object> getComCd(Map<String, Object> params);
	
	public int createComCd(Map<String, Object> params) throws BizException, DuplicateKeyException;
	
	public int modifyComCd(Map<String, Object> params);
	
	public List viewDtlCd(Map<String, Object> params);
	
	public Map<String, Object> getDtlCd(Map<String, Object> params);
	
	public int createDtlCd(Map<String, Object> params) throws BizException, DuplicateKeyException;
	
	public int modifyDtlCd(Map<String, Object> params);
}
