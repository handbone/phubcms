package com.olleh.pHubCMS.admin.system.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * 약관정보 관리 Dao
 * @Class Name 	: AgrInfoDao
 * @author 		: bmg
 * @since 		: 2019.05.07
 * @version 	: 1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *  수정일        수정자         수정내용
 *  ------------  -----------    -------------
 *  2019.05.07    bmg            최초 생성
 */
@Repository
@SuppressWarnings({"rawtypes","unchecked"})
public class AgrInfoDao extends AbstractDAO {
	
	public List<Map> viewAgrInfo(Map<String, Object> params) {
		return selectList("mybatis.system.viewAgrInfo", params);
	}
	
	public Map agrInfo(Map<String, Object> params) {
		return selectOne("mybatis.system.agrInfo", params);
	}
	
	public List<Map> viewPrvdrItem(Map<String, Object> params) {
		return selectList("mybatis.system.viewPrvdrItem", params);
	}
	
	public Map prvdrItem(Map<String, Object> params) {
		return selectOne("mybatis.system.prvdrItem", params);
	}
	
	public int insertAgrInfo(Map<String, Object> params) {
		return insert("mybatis.system.insertAgrInfo", params);
	}
	
	public int updateAgrInfo(Map<String, Object> params) {
		return update("mybatis.system.updateAgrInfo", params);
	}
	
	public int insertPrvdrItem(Map<String, Object> params) {
		return insert("mybatis.system.insertPrvdrItem", params);
	}
	
	public int updatePrvdrItem(Map<String, Object> params) {
		return update("mybatis.system.updatePrvdrItem", params);
	}
}
