package com.olleh.pHubCMS.admin.system.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.IncorrectUpdateSemanticsDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.system.service.AgrInfoService;
import com.olleh.pHubCMS.common.components.Constant;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.utils.SessionUtils;

/**
 * 약관정보 관리 Controller
 * 
 * @Class Name 	AgrInfoController
 * @author 		bmg
 * @since 		2019.05.07
 * @version 	1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.05.07   bmg        최초생성
 */
@Controller
@SuppressWarnings({"rawtypes","unchecked"})
public class AgrInfoController {
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	AgrInfoService agrInfoService;
	
	/**
	 * jsp 호출 및 초기값 설정
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/system/viewAgrInfo.do", method = RequestMethod.POST)
	public String viewAgrInfo(Locale locale, Model model) {	
		String method = "viewAgrInfo";
		log.debug(method, ">>> start");
		
		log.debug(method, ">>>>>> end");	
		return "/system/agrInfoView";
	}
	
	/**
	 * 약관정보 목록 조회
	 * 
	 * @param params
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxViewAgrInfo.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxViewAgrInfo(@RequestParam Map<String, Object> params) throws IOException {
		String method = "ajaxViewAgrInfo";
		log.debug(method, ">>> start");	
		//Request param				
		log.debug(method,params.toString());
		
		HashMap map = new HashMap();	
		List resultList = new ArrayList();
		try {
			resultList = agrInfoService.viewAgrInfo(params);
			map.put("rows", resultList);
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 약관정보 상세 조회
	 * 
	 * @param params
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxGetAgrInfo.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxGetAgrInfo(@RequestParam Map<String, Object> params) throws IOException {
		String method = "ajaxGetAgrInfo";
		log.debug(method, ">>> start");	
		//Request param				
		log.debug(method,params.toString());
		
		HashMap map = new HashMap();	
		Map<String, Object> agrInfo = new HashMap<String, Object>();
		try {
			agrInfo = agrInfoService.agrInfo(params);
			map.put("row", agrInfo);
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 약관정보 등록
	 * 
	 * @param params
	 * @param req
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxCreateAgrInfo.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxCreateAgrInfo(@RequestParam Map<String, Object> params, HttpServletRequest req) throws IOException {
		String method = "ajaxCreateAgrInfo";
		log.debug(method, ">>> start");	
		//현재 로그인한 세션의 아이디 추가
		params.put("RGST_USER_ID", SessionUtils.getUserId(req));
		//Request param				
		log.debug(method,params.toString());
		
		HashMap map = new HashMap();
		try {
			int ret = agrInfoService.insertAgrInfo(params);
			if (ret > 0) {
				map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
			} else {
				throw new IncorrectUpdateSemanticsDataAccessException("저장에 실패하였습니다");
			}
		} catch (DuplicateKeyException | IncorrectUpdateSemanticsDataAccessException e) {
 			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			map.put(Constant.RET_MSG, e.getMessage());
			log.error(method, "Exception : "+ e);
 		} catch (Exception e) {
 			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 약관정보 수정
	 * 
	 * @param params
	 * @param req
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxModifyAgrInfo.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxModifyAgrInfo(@RequestParam Map<String, Object> params, HttpServletRequest req) throws IOException {
		String method = "ajaxModifyAgrInfo";
		log.debug(method, ">>> start");	
		//현재 로그인한 세션의 아이디 추가
		params.put("MDFY_USER_ID", SessionUtils.getUserId(req));
		//Request param		
		log.debug(method,params.toString());
		
		HashMap map = new HashMap();
		try {
			int ret = agrInfoService.updateAgrInfo(params);
			if (ret > 0) {
				map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
			} else {
				throw new IncorrectUpdateSemanticsDataAccessException("저장에 실패하였습니다");
			}
		} catch (IncorrectUpdateSemanticsDataAccessException e) {
 			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
 			map.put(Constant.RET_MSG, e.getMessage());
 			log.error(method, "Exception : "+ e);
 		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 제공사항정보 목록 조회
	 * 
	 * @param params
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxViewPrvdrItem.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxViewPrvdrItem(@RequestParam Map<String, Object> params) throws IOException {
		String method = "ajaxViewPrvdrItem";
		log.debug(method, ">>> start");	
		//Request param				
		log.debug(method,params.toString());
		
		HashMap map = new HashMap();	
		List resultList = new ArrayList();
		try {
			resultList = agrInfoService.viewPrvdrItem(params);
			map.put("rows", resultList);
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 제공사항정보 상세 조회
	 * 
	 * @param params
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxGetPrvdrItem.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxGetPrvdrItem(@RequestParam Map<String, Object> params) throws IOException {
		String method = "ajaxGetPrvdrItem";
		log.debug(method, ">>> start");	
		//Request param				
		log.debug(method,params.toString());
		
		HashMap map = new HashMap();	
		Map<String, Object> prvdrItem = new HashMap<String, Object>();
		try {
			prvdrItem = agrInfoService.prvdrItem(params);
			map.put("row", prvdrItem);
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 제공사항정보 등록
	 * 
	 * @param params
	 * @param req
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxCreatePrvdrItem.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxCreatePrvdrItem(@RequestParam Map<String, Object> params, HttpServletRequest req) throws IOException {
		String method = "ajaxCreatePrvdrItem";
		log.debug(method, ">>> start");	
		//현재 로그인한 세션의 아이디 추가
		params.put("RGST_USER_ID", SessionUtils.getUserId(req));
		//Request param				
		log.debug(method,params.toString());
		
		HashMap map = new HashMap();
		try {
			int ret = agrInfoService.insertPrvdrItem(params);
			if (ret > 0) {
				map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
			} else {
				throw new IncorrectUpdateSemanticsDataAccessException("저장에 실패하였습니다");
			}
		} catch (DuplicateKeyException | IncorrectUpdateSemanticsDataAccessException e) {
 			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			map.put(Constant.RET_MSG, e.getMessage());
			log.error(method, "Exception : "+ e);
 		} catch (Exception e) {
 			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 제공사항정보 수정
	 * 
	 * @param params
	 * @param req
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxModifyPrvdrItem.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxModifyPrvdrItem(@RequestParam Map<String, Object> params, HttpServletRequest req) throws IOException {
		String method = "ajaxModifyPrvdrItem";
		log.debug(method, ">>> start");	
		//현재 로그인한 세션의 아이디 추가
		params.put("MDFY_USER_ID", SessionUtils.getUserId(req));
		//Request param		
		log.debug(method,params.toString());
		
		HashMap map = new HashMap();
		try {
			int ret = agrInfoService.updatePrvdrItem(params);
			if (ret > 0) {
				map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
			} else {
				throw new IncorrectUpdateSemanticsDataAccessException("저장에 실패하였습니다");
			}
		} catch (IncorrectUpdateSemanticsDataAccessException e) {
 			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
 			map.put(Constant.RET_MSG, e.getMessage());
 			log.error(method, "Exception : "+ e);
 		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
}
