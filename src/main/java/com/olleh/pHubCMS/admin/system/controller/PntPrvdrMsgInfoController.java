package com.olleh.pHubCMS.admin.system.controller;

import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.IncorrectUpdateSemanticsDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.system.service.PntPrvdrMsgInfoService;
import com.olleh.pHubCMS.common.components.Constant;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.utils.Paging;
import com.olleh.pHubCMS.common.utils.SessionUtils;
import com.olleh.pHubCMS.common.utils.StringUtil;


/**
 * 연동시스템 응답코드 정의 Controller
 * 
 * @Class Name 	PntPrvdrMsgInfoController
 * @author 		jungukjae
 * @since 		2019.06.17
 * @version 	1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.06.17   jungukjae  최초생성
 */
@Controller
@SuppressWarnings({"rawtypes","unchecked"})
public class PntPrvdrMsgInfoController {

	private Logger log = new Logger(this.getClass());

	@Autowired
	PntPrvdrMsgInfoService pntPrvdrMsgInfoService;
	
	/**
	 * jsp 호출 및 초기값 설정
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/system/viewPntPrvdrMsg.do", method = RequestMethod.POST)
	public String viewPntPrvdrMsgInfo(Locale locale, Model model) {	
		String method = "pntPrvdrMsgInfo";
		log.debug(method, ">>> start");
		
		log.debug(method, ">>>>>> end");	
		return "/system/pntPrvdrMsgView";
	} 

	/**
	 * JQGrid 호출
	 * 
	 * @param page 현재 보고자 하는 페이지 (JQgrid 처리)
	 * @param rows 한 페이지에 보여주고자 설정된 개수 (JQgrid 처리)
	 * @param PRVDR_ID 	연동시스템ID (검색 Input)
	 * @param PNT_CD 	포인트코드	 (검색 Input)
	 * @param MSG_ID   	응답코드	 (검색 Input)
	 * @param MSG   	응답메세지	 (검색 Input)
	 * @param RFRN_VAL1 참조값1  	 (검색 Input) 
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxViewPntPrvdrMsg.do", method = RequestMethod.POST)
	public Map<String, Object> jgPntPrvdrMsg(@RequestParam Map<String, Object> params, HttpServletRequest req) throws IOException {	
		String method = "jgPntPrvdrMsg";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		Map<String, Object> count = new HashMap<String, Object>();
		
		//Request param
		int pageNum = Integer.parseInt(req.getParameter("page"));
		int limit = Integer.parseInt(req.getParameter("rows"));	
		
		//총 Row 수 계산 		 
		count = pntPrvdrMsgInfoService.viewPntPrvdrMsgTotal(params);		
		int countRow = Integer.parseInt(count.get("cnt").toString());			
		
		if(countRow > 0){
			//HashMap에 페이징 정보 저장					
			Paging.setPageMap(params, map, pageNum, limit, countRow);	
			log.debug(method,map.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			resultList = pntPrvdrMsgInfoService.viewPntPrvdrMsg(params);	
			
			System.out.println(resultList.toString());
			
			map.put("rows", resultList );
		}  		
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * JQGrid 호출 (엑셀용)
	 * 
	 * @param PRVDR_ID 	연동시스템ID (검색 Input)
	 * @param PNT_CD 	포인트코드	 (검색 Input)
	 * @param MSG_ID   	응답코드	 (검색 Input)
	 * @param MSG   	응답메세지	 (검색 Input) 
	 * @param RFRN_VAL1 참조값  	 (검색 Input) 
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/system/jqPntPrvdrMsgExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgPntPrvdrMsgExcel(@RequestParam Map<String, Object> params) throws IOException {	
		String method = "jgPntPrvdrMsgExcel";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		
		//Request param			
		log.debug(method, params.toString());
	
		//검색 결과 데이터 map 에 추가
		List resultList = new ArrayList();
		resultList = pntPrvdrMsgInfoService.viewPntPrvdrMsgExcel(params);		
		map.put("rows", resultList );

		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 연동시스템 응답코드 수정 (상세)
	 * 
	 * @param PRVDR_ID 	연동시스템 ID 
	 * @param MSG_SEQ 	응답순번 
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxGetPntPrvdrMsgInfo.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxGetPntPrvdr(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "ajaxGetPntPrvdrMsg.do";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		Map<String, Object> prvdr = new HashMap<String, Object>();
		
		try{			
			//검색 결과 데이터 map 에 추가
			log.debug(method,params.toString());
			prvdr = pntPrvdrMsgInfoService.getPntPrvdrMsg(params);
			map.put("rows", prvdr );	
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}

		log.debug(method, ">>>>>> end");
		return map;
	}	
	
	/**
	 * 연동시스템 응답코드 Key(중복) 체크
	 * 
	 * @param PRVDR_ID 			input 연동시스템ID 
	 * @param PNT_CD  			Input 포인트코드 
	 * @param MSG_ID 			Input 응답코드
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value="/system/checkPntPrvdrMsgKey.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxCheckPntPrvdrMsgKey(@RequestParam Map<String, Object> params, HttpServletRequest req) throws IOException {
		String method = "ajaxGetPntPrvdrMsg.do";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		int keyCount;
		
		try {
			keyCount = pntPrvdrMsgInfoService.checkPntPrvdrMsgKey(params);		
			map.put("keyCount", keyCount); 
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		}catch (Exception e){
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
	

	/**
	 * 연동시스템 응답코드 추가
	 * 
	 * @param PRVDR_ID 			input 연동시스템ID 
	 * @param PNT_CD  			Input 포인트코드 
	 * @param MSG_ID 			Input 응답코드
	 * @param MSG 				Input 응답메세지
	 * @param RFRN_VAL1			Input 참조값1
	 * @param RFRN_VAL2			Input 참조값2
	 * @param RFRN_VAL3			Input 참조값3
	 * @return void response 응답
	 */
	@ResponseBody 
	@RequestMapping(value="/system/ajaxCreatePntPrvdrMsgInfo.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxCreatePntPrvdrMsg(@RequestParam Map<String, Object> params, HttpServletRequest req) throws IOException {	
		String method = "ajaxCreatePntPrvdrMsg.do";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();		
		try{			
			//현재 로그인한 세션의 아이디 추가
			params.put("MDFY_USER_ID", SessionUtils.getUserId(req));
			log.debug(method,params.toString());
			
			int ret = pntPrvdrMsgInfoService.createPntPrvdrMsg(params);
			if (ret > 0) {
				map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
			} else {
				throw new IncorrectUpdateSemanticsDataAccessException("저장에 실패하였습니다");
			}
		} catch (IncorrectUpdateSemanticsDataAccessException e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
 			map.put(Constant.RET_MSG, e.getMessage());
 			log.error(method, "Exception : "+ e);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}

		log.debug(method, ">>>>>> end");
		return map;
	}

	/**
	 * 연동시스템 응답코드 수정
	 * 
	 * @param PRVDR_ID 			input 연동시스템ID 
	 * @param PNT_CD  			Input 포인트코드 
	 * @param MSG_ID 			Input 응답코드
	 * @param MSG 				Input 응답메세지
	 * @param RFRN_VAL1			Input 참조값1
	 * @param RFRN_VAL2			Input 참조값2
	 * @param RFRN_VAL3			Input 참조값3
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxModifyPntPrvdrMsgInfo.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxModifyPntPrvdr(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "ajaxModifyPntPrvdr.do";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();		
		try{			
			//현재 로그인한 세션의 아이디 추가
			params.put("MDFY_USER_ID", SessionUtils.getUserId(req));
			log.debug(method,params.toString());
			
			int ret = pntPrvdrMsgInfoService.modifyPntPrvdrMsg(params);
			if (ret > 0) {
				map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
			} else {
				throw new IncorrectUpdateSemanticsDataAccessException("저장에 실패하였습니다");
			}
		} catch (IncorrectUpdateSemanticsDataAccessException e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
 			map.put(Constant.RET_MSG, e.getMessage());
 			log.error(method, "Exception : "+ e);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}

		log.debug(method, ">>>>>> end");
		return map;
	}	
	
	/**
	 * 연동시스템 응답코드 삭제 
	 * 
	 * @param PRVDR_ID 			input 연동시스템ID 
	 * @param PNT_CD  			Input 포인트코드 
	 * @param MSG_ID 			Input 응답코드
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxDeletePntPrvdrMsgInfo.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxDeletePntPrvdr(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "ajaxDeletePntPrvdr.do";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();		
		try{			
			int ret = pntPrvdrMsgInfoService.deletePntPrvdrMsg(params);
			if (ret > 0) {
				map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
			} else {
				throw new IncorrectUpdateSemanticsDataAccessException("삭제에 실패하였습니다");
			}
		} catch (IncorrectUpdateSemanticsDataAccessException e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
 			map.put(Constant.RET_MSG, e.getMessage());
 			log.error(method, "Exception : "+ e);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}	
	
}































