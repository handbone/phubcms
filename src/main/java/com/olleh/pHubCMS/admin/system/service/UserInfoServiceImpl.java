package com.olleh.pHubCMS.admin.system.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.system.dao.UserInfoDao;
import com.olleh.pHubCMS.common.utils.StringUtil;


/**
 * 사용자 정보 Service
 * 
 * @Class Name : UserInfoService
 * @author ojh
 * @since 2018.08.22
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.22   ojh        최초생성
 * 
 */
@Service
@SuppressWarnings({"rawtypes", "unchecked"})
public class UserInfoServiceImpl implements UserInfoService{
	@Autowired
	UserInfoDao userInfoDao;
	
	@Override
	public List viewUserInfo(Map<String, Object> params){
		return maskingName(userInfoDao.viewUserInfo(params));		
	}
	@Override
	public Map<String, Object> viewUserInfoTotal(Map<String, Object> params){
		return userInfoDao.viewUserInfoTotal(params);
	}
	@Override
	public List viewUserInfoExcel(Map<String, Object> params){
		return maskingName(userInfoDao.viewUserInfoExcel(params));		
	}
	@Override
	public int createUserInfo(Map<String, Object> params){
		if (params.get("MAX_INQR_CNT") != null) { 
			params.put("MAX_INQR_CNT", Integer.parseInt(params.get("MAX_INQR_CNT").toString()));
		}
		return userInfoDao.createUserInfo(params);
	}
	@Override
	public Map<String, Object> getUserInfo(Map<String, Object> params){
		return maskingName(userInfoDao.getUserInfo(params));
	}
	@Override
	public int modifyUserInfo(Map<String, Object> params){
		if (params.get("MAX_INQR_CNT") != null) { 
			params.put("MAX_INQR_CNT", Integer.parseInt(params.get("MAX_INQR_CNT").toString()));
		}
		return userInfoDao.modifyUserInfo(params);
	}
	@Override
	public int modifyErrCnt(Map<String, Object> params) {
		return userInfoDao.modifyErrCnt(params);
	}
	
	/**
	 * <pre>로그인 상태 Y 변경</pre>
	 * @param params
	 * @return
	 */
	@Override
	public int modifyLoginY(Map<String, Object> params){
		return userInfoDao.modifyLoginY(params);
	}
	
	/**
	 * <pre>로그인 상태 N 변경</pre>
	 * @param params
	 * @return
	 */
	@Override
	public int modifyLoginN(Map<String, Object> params){
		return userInfoDao.modifyLoginN(params);		
	}
	/**
	 * 이름 마스킹 처리
	 * 
	 * @param params
	 * @return
	 */
	private List maskingName(List<Map> params) {
		int index = 0;
		if (params == null) {
			return params;
		}
		for (Map map : params) {
			if (map.get("rgst_user_nm") != null) {
				map.put("rgst_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("rgst_user_nm"))));
			}
			if (map.get("mdfy_user_nm") != null) {
				map.put("mdfy_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("mdfy_user_nm"))));
			}
			params.set(index, map);
			index++;
		}
		return params;
	}
	/**
	 * 이름 마스킹 처리
	 * 
	 * @param params
	 * @return
	 */
	private Map maskingName(Map map) {
		if (map != null) {
			if (map.get("rgst_user_nm") != null) {
				map.put("rgst_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("rgst_user_nm"))));
			}
			if (map.get("mdfy_user_nm") != null) {
				map.put("mdfy_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("mdfy_user_nm"))));
			}
		}
		return map;
	}
}
