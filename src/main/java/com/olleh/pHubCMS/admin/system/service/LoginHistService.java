package com.olleh.pHubCMS.admin.system.service;

import java.util.List;
import java.util.Map;

/**
 * 로그인이력 Service
 * @Class Name : LoginHistService
 * @author bmg
 * @since 2018.09.20
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.20    bmg        최초생성
 * </pre>
 */
public interface LoginHistService {
	
	public Map<String, Object> viewLoginHistTotal(Map<String, Object> params);
	
	@SuppressWarnings("rawtypes")
	public List viewLoginHist(Map<String, Object> params);
	
	@SuppressWarnings("rawtypes")
	public List loginHistExcel(Map<String, Object> params);
}
