package com.olleh.pHubCMS.admin.system.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * 공통코드 관리 Dao
 * @Class Name : SysMsgDao
 * @author : bmg
 * @since : 2018.09.13
 * @version : 1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *      수정일                 수정자                수정내용
 *  ------------  -----------    -------------
 *  2018. 9. 13.      bmg          최초 생성
 * 
 */
@Repository
public class SysMsgDao extends AbstractDAO {
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> viewSysMsgTotal(Map<String, Object> params) {
		return selectOne("mybatis.system.sysMsgTotal", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List viewSysMsg(Map<String, Object> params) {
		return selectList("mybatis.system.sysMsg", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List viewSysMsgExcel(Map<String, Object> params) {
		return selectList("mybatis.system.sysMsgExcel", params);
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> getSysMsg(Map<String, Object> params) {
		return selectOne("mybatis.system.getSysMsg", params);
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> getSysMsgDuplTotal(Map<String, Object> params) {
		return selectOne("mybatis.system.getSysMsgDuplTotal", params);
	}
	
	public int modifySysMsg(Map<String, Object> params) {
		return update("mybatis.system.modifySysMsg", params);
	}
	
	public int createSysMsg(Map<String, Object> params) {
		return insert("mybatis.system.createSysMsg", params);
	}
	
	
}
