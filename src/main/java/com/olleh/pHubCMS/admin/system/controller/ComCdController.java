package com.olleh.pHubCMS.admin.system.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.IncorrectUpdateSemanticsDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.system.service.ComCdService;
import com.olleh.pHubCMS.common.components.CodeManage;
import com.olleh.pHubCMS.common.components.Constant;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.CmnCdVO;
import com.olleh.pHubCMS.common.utils.SessionUtils;

/**
 * 공통코드 관리 Controller
 * 
 * @Class Name : ComCdController
 * @author bmg
 * @since 2018.09.11
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.11   bmg        최초생성
 * 
 */
@Controller
public class ComCdController {
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	CodeManage codeManage;
	
	@Autowired
	ComCdService comCdService;
	
	/**
	 * jsp 호출 및 초기값 설정
	 * 
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/system/viewComCd.do", method = RequestMethod.POST)
	public String viewComCd(Locale locale, Model model) {
		String method = "viewComCd";
		log.debug(method, ">>> start");	
		
		List<CmnCdVO> prmtGrpList = new ArrayList<CmnCdVO>();
		prmtGrpList = codeManage.getCodeListY("COM_GRP");
		model.addAttribute("COM_GRP", prmtGrpList.toArray());
		
		log.debug(method, ">>>>>> end");	
		return "/system/comCdView";
	}
	
	/**
	 * 공통코드 목록 조회
	 * 
	 * @param params
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/system/ajaxViewComCd.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxViewComCd(@RequestParam Map<String, Object> params) throws IOException {
		String method = "ajaxGetComCd";
		log.debug(method, ">>> start");	
		//Request param				
		log.debug(method,params.toString());
		
		HashMap map = new HashMap();	
		List resultList = new ArrayList();
		try {
			resultList = comCdService.viewComCd(params);
			mappingComCd(resultList);
			map.put("rows", resultList);
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 공통코드 상세 조회(수정)
	 * 
	 * @param params
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/system/ajaxGetComCd.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxGetComCd(@RequestParam Map<String, Object> params) throws IOException {
		String method = "ajaxGetComCd";
		log.debug(method, ">>> start");	
		//Request param				
		log.debug(method,params.toString());
		
		HashMap map = new HashMap();	
		Map<String, Object> comCd = new HashMap<String, Object>();
		try {
			comCd = comCdService.getComCd(params);
			map.put("row", comCd);
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 공통코드 등록
	 * 
	 * @param params
	 * @param req
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/system/ajaxCreateComCd.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxCreateComCd(@RequestParam Map<String, Object> params, HttpServletRequest req) throws IOException {
		String method = "ajaxCreateComCd";
		log.debug(method, ">>> start");	
		//현재 로그인한 세션의 아이디 추가
		params.put("RGST_USER_ID", SessionUtils.getUserId(req));
		//Request param				
		log.debug(method,params.toString());
		
		HashMap map = new HashMap();
		try {
			int ret = comCdService.createComCd(params);
			if (ret > 0) {
				map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
			} else {
				throw new IncorrectUpdateSemanticsDataAccessException("저장에 실패하였습니다");
			}			
 		} catch (DuplicateKeyException | IncorrectUpdateSemanticsDataAccessException e) {
 			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			map.put(Constant.RET_MSG, e.getMessage());
			log.error(method, "Exception : "+ e);
 		} catch (Exception e) {
 			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 공통코드 수정
	 * 
	 * @param params
	 * @param req
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/system/ajaxModifyComCd.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxModifyComCd(@RequestParam Map<String, Object> params, HttpServletRequest req) throws IOException {
		String method = "ajaxModifyComCd";
		log.debug(method, ">>> start");	
		//현재 로그인한 세션의 아이디 추가
		params.put("MDFY_USER_ID", SessionUtils.getUserId(req));
		//Request param		
		log.debug(method,params.toString());
		
		HashMap map = new HashMap();
		try {
			int ret = comCdService.modifyComCd(params);
			if (ret > 0) {
				map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
			} else {
				throw new IncorrectUpdateSemanticsDataAccessException("저장에 실패하였습니다");
			}
		} catch (IncorrectUpdateSemanticsDataAccessException e) {
 			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
 			map.put(Constant.RET_MSG, e.getMessage());
 			log.error(method, "Exception : "+ e);
 		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 상세코드 목록 조회
	 * 
	 * @param params
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/system/ajaxViewDtlCd.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxViewDtlCd(@RequestParam Map<String, Object> params) throws IOException {
		String method = "ajaxGetDtlCd";
		log.debug(method, ">>> start");	
		//Request param				
		log.debug(method,params.toString());
		
		HashMap map = new HashMap();	
		List resultList = new ArrayList();
		try {
			resultList = comCdService.viewDtlCd(params);
			mappingComCd(resultList);
			map.put("rows", resultList);
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 상세코드 상세 조회(수정)
	 * 
	 * @param params
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/system/ajaxGetDtlCd.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxGetDtlCd(@RequestParam Map<String, Object> params) throws IOException {
		String method = "ajaxGetDtlCd";
		log.debug(method, ">>> start");	
		//Request param				
		log.debug(method,params.toString());
		
		HashMap map = new HashMap();	
		Map<String, Object> comCd = new HashMap<String, Object>();
		try {
			comCd = comCdService.getDtlCd(params);
			map.put("row", comCd);
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 상세코드 등록
	 * 
	 * @param params
	 * @param req
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/system/ajaxCreateDtlCd.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxCreateDtlCd(@RequestParam Map<String, Object> params, HttpServletRequest req) throws IOException {
		String method = "ajaxCreateDtlCd";
		log.debug(method, ">>> start");	
		//현재 로그인한 세션의 아이디 추가
		params.put("RGST_USER_ID", SessionUtils.getUserId(req));
		//Request param				
		log.debug(method,params.toString());
		
		HashMap map = new HashMap();
		try {
			int ret = comCdService.createDtlCd(params);
			if (ret > 0) {
				map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
			} else {
				throw new IncorrectUpdateSemanticsDataAccessException("저장에 실패하였습니다");
			}
		} catch (DuplicateKeyException | IncorrectUpdateSemanticsDataAccessException e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			map.put(Constant.RET_MSG, e.getMessage());
			log.error(method, "Exception : "+ e);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 상세코드 수정
	 * 
	 * @param params
	 * @param req
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/system/ajaxModifyDtlCd.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxModifyDtlCd(@RequestParam Map<String, Object> params, HttpServletRequest req) throws IOException {
		String method = "ajaxModifyDtlCd";
		log.debug(method, ">>> start");	
		//현재 로그인한 세션의 아이디 추가
		params.put("MDFY_USER_ID", SessionUtils.getUserId(req));
		//Request param		
		log.debug(method,params.toString());
		
		HashMap map = new HashMap();
		try {
			int ret = comCdService.modifyDtlCd(params);
			if (ret > 0) {
				map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
			} else {
				throw new IncorrectUpdateSemanticsDataAccessException("저장에 실패하였습니다");
			}
		} catch (IncorrectUpdateSemanticsDataAccessException e) {
 			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
 			map.put(Constant.RET_MSG, e.getMessage());
 			log.error(method, "Exception : "+ e);
 		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 공통코드 code -> 이름 변환
	 * 
	 * @param resultList
	 */
	@SuppressWarnings({"rawtypes", "unchecked"})
	public void mappingComCd(List resultList) {
		for(int i=0; i<resultList.size(); i++) {
			Map<String, Object> resultTmp = (Map<String, Object>) resultList.get(i);
			if (resultTmp.get("use_yn") != null && resultTmp.get("use_yn").equals("Y")) {
				resultTmp.put("use_yn", "사용");
			} else {
				resultTmp.put("use_yn", "미사용");
			}
			resultList.set(i, resultTmp);
		}
	}
}
