package com.olleh.pHubCMS.admin.system.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.system.dao.SysMsgDao;
import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.exception.BizException;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 시스템메시지 Service
 * 
 * @Class Name : SysPrmtServiceImpl
 * @author bmg
 * @since 2018.09.13
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.13   bmg        최초생성
 * 
 */
@Service
@SuppressWarnings({"rawtypes", "unchecked"})
public class SysMsgServiceImpl implements SysMsgService {

	@Autowired
	SysMsgDao sysMsgDao;
	
	@Autowired
	MessageManage messageManage;
	
	@Override
	public Map<String, Object> viewSysMsgTotal(Map<String, Object> params) {
		return sysMsgDao.viewSysMsgTotal(params);
	}

	@Override
	public List viewSysMsg(Map<String, Object> params) {
		return maskingName(sysMsgDao.viewSysMsg(params));
	}

	@Override
	public List viewSysMsgExcel(Map<String, Object> params) {
		return maskingName(sysMsgDao.viewSysMsgExcel(params));
	}

	@Override
	public Map<String, Object> getSysMsg(Map<String, Object> params) {
		return maskingName(sysMsgDao.getSysMsg(params));
	}

	@Override
	public Map<String, Object> getSysMsgDuplTotal(Map<String, Object> params) {
		return sysMsgDao.getSysMsgDuplTotal(params);
	}

	@Override
	public int modifySysMsg(Map<String, Object> params) {
		return sysMsgDao.modifySysMsg(params);
	}

	@Override
	public int createSysMsg(Map<String, Object> params) throws BizException, DuplicateKeyException {
		int ret = 0;
		try {
			Map<String, Object> dupl = sysMsgDao.getSysMsgDuplTotal(params);
			if (Integer.parseInt(dupl.get("cnt").toString()) > 0) {
				throw new DuplicateKeyException(messageManage.getMsgTxt("AD_INFO_157"));
			}
			String suffix = params.get("MSG_CD").toString().substring(1);
			params.put("MSG_ID", params.get("MSG_GRP").toString() + "_" + params.get("MSG_IND").toString() + "_" + suffix);
			ret = sysMsgDao.createSysMsg(params);
		} catch (DuplicateKeyException de) {
			throw de;
		} catch (Exception e) {
			throw new BizException(e);
		}
		return ret;
	}
	/**
	 * 이름 마스킹 처리
	 * 
	 * @param params
	 * @return
	 */
	private List maskingName(List<Map> params) {
		int index = 0;
		if (params == null) {
			return params;
		}
		for (Map map : params) {
			if (map.get("rgst_user_nm") != null) {
				map.put("rgst_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("rgst_user_nm"))));
			}
			if (map.get("mdfy_user_nm") != null) {
				map.put("mdfy_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("mdfy_user_nm"))));
			}
			params.set(index, map);
			index++;
		}
		return params;
	}
	/**
	 * 이름 마스킹 처리
	 * 
	 * @param params
	 * @return
	 */
	private Map maskingName(Map map) {
		if (map != null) {
			if (map.get("rgst_user_nm") != null) {
				map.put("rgst_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("rgst_user_nm"))));
			}
			if (map.get("mdfy_user_nm") != null) {
				map.put("mdfy_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("mdfy_user_nm"))));
			}
		}
		return map;
	}
}
