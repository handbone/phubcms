package com.olleh.pHubCMS.admin.system.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * 연동시스템 응답코드 정의 Dao
 * @Class Name : PntPrvdrMsgInfoDao
 * @author : jungukjae
 * @since : 2019.06.17
 * @version : 1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *     수정일       수정자         수정내용
 *  ------------  -----------    -------------
 *  2019. 6. 17    jungukjae       최초 생성
 * 
 */
@Repository
public class PntPrvdrMsgInfoDao extends AbstractDAO{

	
	public List viewPntPrvdrMsg(Map<String, Object> params) {
		return selectList("mybatis.system.pntPrvdrMsg", params);
	}

	public Map<String, Object> viewPntPrvdrMsgTotal(Map<String, Object> params) {
		return selectOne("mybatis.system.pntPrvdrMsgTotal", params);
	}
	
	public List viewPntPrvdrMsgExcel(Map<String, Object> params) {
		return selectList("mybatis.system.pntPrvdrMsgExcel", params);
	}

	public Map<String, Object> getPntPrvdrMsg(Map<String, Object> params) {
		return selectOne("mybatis.system.getPntPrvdrMsg", params);
	}

	public int createPntPrvdrMsg(Map<String, Object> params) {
		return insert("mybatis.system.createPntPrvdrMsg", params);
	}

	public int modifyPntPrvdrMsg(Map<String, Object> params) {
		return update("mybatis.system.modifyPntPrvdrMsg", params);
	}

	public List checkPntPrvdrMsgKey(Map<String, Object> params) {
		return selectList("mybatis.system.checkPntPrvdrMsgKey", params);
	}

	public int deletePntPrvdrMsg(Map<String, Object> params) {
		return delete("mybatis.system.deletePntPrvdrMsg", params);
	}

}
