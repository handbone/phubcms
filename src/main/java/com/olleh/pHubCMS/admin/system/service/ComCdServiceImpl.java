package com.olleh.pHubCMS.admin.system.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.system.dao.ComCdDao;
import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.exception.BizException;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 공통코드 Service
 * 
 * @Class Name : ComCdServiceImpl
 * @author bmg
 * @since 2018.09.11
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.11   bmg        최초생성
 * 
 */
@Service
@SuppressWarnings({"rawtypes", "unchecked"})
public class ComCdServiceImpl implements ComCdService {
	
	@Autowired
	ComCdDao comCdDao;
	
	@Autowired
	MessageManage messageManage;
	
	@Override
	public List viewComCd(Map<String, Object> params) {
		return maskingName(comCdDao.viewComCd(params));
	}

	@Override
	public Map<String, Object> getComCd(Map<String, Object> params) {
		return maskingName(comCdDao.getComCd(params));
	}

	@Override
	public int createComCd(Map<String, Object> params) throws BizException, DuplicateKeyException {
		int ret = 0;
		try {
			Map<String, Object> dupl = comCdDao.getComCdDuplTotal(params);
			if (Integer.parseInt(dupl.get("cnt").toString()) > 0) {
				throw new DuplicateKeyException(messageManage.getMsgTxt("AD_INFO_157"));
			}
			ret = comCdDao.createComCd(params);
		} catch (DuplicateKeyException de) {
			throw de;
		} catch (Exception e) {
			throw new BizException(e);
		}
		return ret;
	}
	
	@Override
	public int modifyComCd(Map<String, Object> params) {
		return comCdDao.modifyComCd(params);
	}

	@Override
	public List viewDtlCd(Map<String, Object> params) {
		return maskingName(comCdDao.viewDtlCd(params));
	}

	@Override
	public Map<String, Object> getDtlCd(Map<String, Object> params) {
		return maskingName(comCdDao.getDtlCd(params));
	}

	@Override
	public int createDtlCd(Map<String, Object> params) throws BizException, DuplicateKeyException {
		int ret = 0;
		try {
			Map<String, Object> dupl = comCdDao.getDtlCdDuplTotal(params);
			if (Integer.parseInt(dupl.get("cnt").toString()) > 0) {
				throw new DuplicateKeyException(messageManage.getMsgTxt("AD_INFO_157"));
			}
			params.put("SORT_ORD", Integer.parseInt(params.get("SORT_ORD").toString()));
			ret = comCdDao.createDtlCd(params);
		} catch (DuplicateKeyException de) {
			throw de;
		} catch (Exception e) {
			throw new BizException(e);
		}
		return ret;
	}

	@Override
	public int modifyDtlCd(Map<String, Object> params) {
		params.put("SORT_ORD", Integer.parseInt(params.get("SORT_ORD").toString()));
		return comCdDao.modifyDtlCd(params);
	}
	/**
	 * 이름 마스킹 처리
	 * 
	 * @param params
	 * @return
	 */
	private List maskingName(List<Map> params) {
		int index = 0;
		if (params == null) {
			return params;
		}
		for (Map map : params) {
			if (map.get("rgst_user_nm") != null) {
				map.put("rgst_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("rgst_user_nm"))));
			}
			if (map.get("mdfy_user_nm") != null) {
				map.put("mdfy_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("mdfy_user_nm"))));
			}
			params.set(index, map);
			index++;
		}
		return params;
	}
	/**
	 * 이름 마스킹 처리
	 * 
	 * @param params
	 * @return
	 */
	private Map maskingName(Map map) {
		if (map != null) {
			if (map.get("rgst_user_nm") != null) {
				map.put("rgst_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("rgst_user_nm"))));
			}
			if (map.get("mdfy_user_nm") != null) {
				map.put("mdfy_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("mdfy_user_nm"))));
			}
		}
		return map;
	}
}
