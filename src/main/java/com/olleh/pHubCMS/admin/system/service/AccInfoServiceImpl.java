package com.olleh.pHubCMS.admin.system.service;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.system.dao.AccInfoDao;
import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.exception.BizException;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 사용자 정보 Service
 * 
 * @Class Name : UserInfoService
 * @author ojh
 * @since 2018.08.22
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.22   ojh        최초생성
 * 
 */
@Service
@SuppressWarnings({"rawtypes", "unchecked"})
public class AccInfoServiceImpl implements AccInfoService{
	
	@Autowired
	AccInfoDao accInfoDao;
	
	@Autowired
	MessageManage messageManage;
	
	@Override
	public List viewAccInfo(Map<String, Object> params){
		return maskingName(accInfoDao.viewAccInfo(params));
	}
	@Override
	public List viewAccInfoDtl(Map<String, Object> params) {
		return maskingName(accInfoDao.viewAccInfoDtl(params));
	}
	@Override
	public int createAccInfo(Map<String, Object> params) throws BizException, DuplicateKeyException {
		int ret = 0;
	
		try {
			List list = accInfoDao.getCorpId(params);
			if (list != null && list.size() > 0) {
				throw new DuplicateKeyException(messageManage.getMsgTxt("AD_INFO_157"));
			}
			ret = accInfoDao.createAccInfo(params);
		} catch (DuplicateKeyException e) {
			throw e;
		} catch (Exception e) {
			throw new BizException(e);
		}
		return ret;
	}
	@Override
	public Map<String, Object> getAccInfo(Map<String, Object> params) {
		return maskingName(accInfoDao.getAccInfo(params));
	}
	@Override
	public int modifyAccInfo(Map<String, Object> params) throws BizException, DuplicateKeyException {
		int ret = 0;
		//ID 변경점 테스트를 위한 변수 선언
		String newId = params.get("RLTN_CORP_ID").toString();
		String oldId = params.get("RLTN_CORP_ID_OLD").toString();
		try {
			if (StringUtils.equals(newId, oldId)) {
				ret = accInfoDao.modifyAccInfo(params);
			}
			else {
				List list = accInfoDao.getCorpId(params);
				if (list != null && list.size() > 0) {
					throw new DuplicateKeyException(messageManage.getMsgTxt("AD_INFO_157"));
				}
				ret = accInfoDao.modifyAccInfo(params);
			}
			
		} catch (DuplicateKeyException e) {
			throw e;
		} catch (Exception e) {
			throw new BizException(e);
		}
		return ret;
	}
	@Override
	public int createAccInfoDtl(Map<String, Object> params) throws BizException, DuplicateKeyException {
		int ret = 0;
		try {
			List list = accInfoDao.getCorpIp(params);
			if (list != null && list.size() > 0) {
				throw new DuplicateKeyException(messageManage.getMsgTxt("AD_INFO_157"));
			}
			ret = accInfoDao.createAccInfoDtl(params);
		} catch (DuplicateKeyException e) {
			throw e;
		} catch (Exception e) {
			throw new BizException(e);
		}
		return ret;
	}
	@Override
	public int modifyAccInfoDtl(Map<String, Object> params) throws BizException, DuplicateKeyException {
		int ret = 0;
		//IP 변경점 테스트를 위한 변수 선언
		String newIp = params.get("IP_ADDR").toString();
		String oldIp = params.get("IP_ADDR_OLD").toString();
		try {
			if (StringUtils.equals(newIp, oldIp)){
				
				ret = accInfoDao.modifyAccInfoDtl(params);
			}
			else{
				List list = accInfoDao.getCorpIp(params);
				if (list != null && list.size() > 0) {
					throw new DuplicateKeyException(messageManage.getMsgTxt("AD_INFO_157"));
				}
				ret = accInfoDao.modifyAccInfoDtl(params);
			}
		} catch (DuplicateKeyException e) {
			throw e;
		} catch (Exception e) {
			throw new BizException(e);
		}
		return ret;
	}
	@Override
	public List getCorpId(Map<String, Object> params){
		return accInfoDao.getCorpId(params);
	}
	@Override
	public List getCorpIp(Map<String, Object> params) {
		return accInfoDao.getCorpIp(params);
	}
	/**
	 * 이름 마스킹 처리
	 * 
	 * @param params
	 * @return
	 */
	private List maskingName(List<Map> params) {
		int index = 0;
		if (params == null) {
			return params;
		}
		for (Map map : params) {
			if (map.get("rgst_user_nm") != null) {
				map.put("rgst_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("rgst_user_nm"))));
			}
			if (map.get("mdfy_user_nm") != null) {
				map.put("mdfy_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("mdfy_user_nm"))));
			}
			params.set(index, map);
			index++;
		}
		return params;
	}
	/**
	 * 이름 마스킹 처리
	 * 
	 * @param params
	 * @return
	 */
	private Map maskingName(Map map) {
		if (map != null) {
			if (map.get("rgst_user_nm") != null) {
				map.put("rgst_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("rgst_user_nm"))));
			}
			if (map.get("mdfy_user_nm") != null) {
				map.put("mdfy_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("mdfy_user_nm"))));
			}
		}
		return map;
	}
}
