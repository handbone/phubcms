package com.olleh.pHubCMS.admin.system.service;

import java.util.List;
import java.util.Map;

import org.springframework.dao.DuplicateKeyException;

import com.olleh.pHubCMS.common.exception.BizException;


public interface AccInfoService {
	
	public List viewAccInfo(Map<String, Object> params);
	
	public List viewAccInfoDtl(Map<String, Object> params);
	
	public int createAccInfo(Map<String, Object> params) throws BizException, DuplicateKeyException;
	
	public Map<String, Object> getAccInfo(Map<String, Object> params);
	
	public int modifyAccInfo(Map<String, Object> params) throws BizException, DuplicateKeyException;
	
	public int createAccInfoDtl(Map<String, Object> params) throws BizException, DuplicateKeyException;
	
	public int modifyAccInfoDtl(Map<String, Object> params) throws BizException, DuplicateKeyException;
	
	public List getCorpId(Map<String, Object> params);

	public List getCorpIp(Map<String, Object> params);
}
