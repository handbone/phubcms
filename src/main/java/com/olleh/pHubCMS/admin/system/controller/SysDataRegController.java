package com.olleh.pHubCMS.admin.system.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.system.service.SysDataRegService;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.utils.SessionUtils;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 시스템반영관리 Controller
 * 
 * @Class Name : SysDataRegController
 * @author ojh
 * @since 2018.10.18
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.18    ojh        최초생성
 * 
 */
@Controller
public class SysDataRegController {
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	SysDataRegService sysDataRegService;
	
	
	/**
	 * jsp 호출 및 초기값 설정
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/system/viewSysDataReg.do", method = RequestMethod.POST)
	public String viewSysDataReg(Locale locale, Model model) {	
		String method = "viewSysDataReg";
		log.debug(method, ">>> start");	
		
		log.debug(method, ">>>>>> end");	
		return "/system/sysDataRegView";
	}	

	/**
	 * 시스템데이터 반영
	 * 	 
	 * @param DATA_TYPE
	 * 
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxModifySysDataReg.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxModifySysDataReg(@RequestParam Map<String, Object> params, HttpServletRequest req) throws IOException {	
		String method = "ajaxModifySysDataReg";
		log.debug(method, ">>> start");					
		HashMap map = new HashMap();	
		try {			
			//Request param		
			log.debug(method,params.toString());	
	
			List prmtList = new ArrayList();
			
			String dataType = StringUtil.nvl(params.get("DATA_TYPE"));
			if(dataType.equals("PHUB")){
				prmtList.add("PH_MEM_RELOAD_YN1");
				prmtList.add("PH_MEM_RELOAD_YN2");
			} else if(dataType.equals("ONM")){
				prmtList.add("ONM_MEM_RELOAD_YN");
			}						
			map.put("prmtList", prmtList);
			map.put("MDFY_USER_ID", SessionUtils.getUserId(req));
			
			if(sysDataRegService.modifySysData(map) > 0){
				map.put("eCode", "0");
			} else{
				map.put("eCode", "-1");
			}	
			
		} catch (Exception e) {
			map.put("eCode", "-1");
			log.error(method, "Exception : "+e.getMessage());
		}
		//Json Response 응답
		log.debug(method, ">>>>>> end");
		return map;			
	}

}
