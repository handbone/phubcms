package com.olleh.pHubCMS.admin.system.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.IncorrectUpdateSemanticsDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.system.service.MenuInfoServiceImpl;
import com.olleh.pHubCMS.common.components.CodeManage;
import com.olleh.pHubCMS.common.components.Constant;
import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.CmnCdVO;
import com.olleh.pHubCMS.common.utils.SessionUtils;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 메뉴정보 Controller
 * 
 * @Class Name : MenuInfoController
 * @author ojh
 * @since 2018.08.27
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.27     ojh        최초생성
 * 
 */
@Controller
@SuppressWarnings({"rawtypes", "unchecked"})
public class MenuInfoController {

	private Logger log = new Logger(this.getClass());
	
	@Autowired
	CodeManage codeManage;
	
	@Autowired
	MessageManage messageManage;
	
	@Autowired
	MenuInfoServiceImpl menuInfoService;
	
	/**
	 * 메뉴관리 jsp 호출 및 초기값 설정
	 * 
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/system/viewMenuInfo.do", method = RequestMethod.POST)
	public String viewMenuInfo(Locale locale, Model model) {
		String method = "viewMenuInfo";
		log.debug(method, ">>> start");	
		
		List<CmnCdVO> menuInd = new ArrayList<CmnCdVO>();
		menuInd = codeManage.getCodeListY("MENU_IND");
		model.addAttribute("MENU_IND", menuInd.toArray());
		
		List<CmnCdVO> menulvl = new ArrayList<CmnCdVO>();
		menulvl = codeManage.getCodeListY("MENU_LVL");
		model.addAttribute("MENU_LVL", menulvl.toArray());
		
		List<CmnCdVO> uriMethod = new ArrayList<CmnCdVO>();
		uriMethod = codeManage.getCodeListY("URI_METHOD");
		model.addAttribute("URI_METHOD", uriMethod.toArray());
		
		log.debug(method, ">>>>>> end");	
		return "/system/menuInfoView";
	}
	
	/**
	 * 메뉴관리 목록 조회
	 * 
	 * @param params
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxViewMenuInfo.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxViewMenuInfo(@RequestParam Map<String, Object> params) throws IOException {
		String method = "ajaxViewMenuInfo";
		log.debug(method, ">>> start");	
		//Request param
		log.debug(method,params.toString());
		
		HashMap map = new HashMap();	
		List resultList = new ArrayList();
		try {
			resultList = menuInfoService.menuInfoView(params);
			map.put("rows", resultList);
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 상위 메뉴 ID 목록 얻기
	 * 
	 * @param params
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxUprMenuInfo.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxUprMenuInfo(@RequestParam Map<String, Object> params) throws IOException {
		String method = "ajaxUprMenuInfo";
		log.debug(method, ">>> start");	
		//Request param
		log.debug(method,params.toString());
		
		HashMap map = new HashMap();	
		List resultList = new ArrayList();
		try {
			resultList = menuInfoService.uprMenuInfo(params);
			map.put("rows", resultList);
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 메뉴 정보 등록
	 * 
	 * @param params
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxCreateMenuInfo.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxCreateMenuInfo(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "ajaxCreateMenuInfo";
		log.debug(method, ">>> start");	
		//현재 로그인한 세션의 아이디 추가
		params.put("RGST_USER_ID", SessionUtils.getUserId(request));
		//Request param				
		log.debug(method,params.toString());
		
		HashMap map = new HashMap();
		try {
			int ret = menuInfoService.createMenuInfo(params);
			if (ret > 0) {
				map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
			} else {
				throw new IncorrectUpdateSemanticsDataAccessException("저장에 실패하였습니다");
			}
		} catch (DuplicateKeyException | IncorrectUpdateSemanticsDataAccessException e) {
 			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			map.put(Constant.RET_MSG, e.getMessage());
			log.error(method, "Exception : "+ e);
 		} catch (Exception e) {
 			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
 			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 메뉴 정보 수정
	 * 
	 * @param params
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxModifyMenuInfo.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxModifyMenuInfo(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "ajaxCreateMenuInfo";
		log.debug(method, ">>> start");	
		//현재 로그인한 세션의 아이디 추가
		params.put("MDFY_USER_ID", SessionUtils.getUserId(request));
		//Request param				
		log.debug(method,params.toString());
		
		HashMap map = new HashMap();
		try {
			int ret = menuInfoService.modifyMenuInfo(params);
			if (ret > 0) {
				map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
			} else {
				throw new IncorrectUpdateSemanticsDataAccessException("저장에 실패하였습니다");
			}
		} catch (IncorrectUpdateSemanticsDataAccessException e) {
 			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
 			map.put(Constant.RET_MSG, e.getMessage());
 			log.error(method, "Exception : "+ e);
 		} catch (Exception e) {
 			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
 			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");
		return map;	
	}
	
	/**
	 * 메뉴 정보 상세 조회(수정)
	 * 
	 * @param params
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxGetMenuInfoView.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxGetMenuInfoView(@RequestParam Map<String, Object> params) throws IOException {
		String method = "ajaxGetMenuInfoView";
		log.debug(method, ">>> start");	
		//Request param				
		log.debug(method,params.toString());
		HashMap map = new HashMap();	
		Map<String, Object> menuInfo = new HashMap<String, Object>();
		List uprlist = new ArrayList();
		try {
			menuInfo = menuInfoService.getMenuInfoView(params);
			if (menuInfo != null) {
				params.put("MENU_IND", StringUtil.nvl(menuInfo.get("menu_ind")));
				params.put("MENU_LVL", Integer.parseInt(menuInfo.get("menu_lvl").toString()));
			}
			uprlist = menuInfoService.uprMenuInfo(params);
			map.put("row", menuInfo);
			map.put("upr", uprlist);
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
 			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * Action Request 목록 조회
	 * 
	 * @param params
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxViewActnInfo.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxViewActnInfo(@RequestParam Map<String, Object> params) throws IOException {
		String method = "ajaxViewActnInfo";
		log.debug(method, ">>> start");	
		//Request param
		log.debug(method,params.toString());
		
		HashMap map = new HashMap();	
		List resultList = new ArrayList();
		try {
			resultList = menuInfoService.actnInfoView(params);
			
			mappingActnInfo(resultList);
			map.put("rows", resultList);
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
 			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * Request 액션 정보 code -> 이름 변환
	 * 
	 * @param resultList
	 */
	public void mappingActnInfo(List resultList) {
		for(int i=0; i<resultList.size(); i++) {
			Map<String, Object> resultTmp = (Map<String, Object>) resultList.get(i);
			if (resultTmp.get("log_write_yn") != null && resultTmp.get("log_write_yn").equals("Y")) {
				resultTmp.put("log_write_yn", "사용");
			} else {
				resultTmp.put("log_write_yn", "미사용");
			}
			resultList.set(i, resultTmp);
		}
	}
	
	/**
	 * Request 액션 정보 정보 상세 조회(수정)
	 * 
	 * @param params
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxGetActnInfo.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxGetActnInfo(@RequestParam Map<String, Object> params) throws IOException {
		String method = "ajaxGetActnInfo";
		log.debug(method, ">>> start");	
		//Request param				
		log.debug(method,params.toString());
		HashMap map = new HashMap();
		Map<String, Object> actnInfo = new HashMap<String, Object>();
		try {
			actnInfo = menuInfoService.getActnInfo(params);
			
			map.put("row", actnInfo);
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
 			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * Request 액션 정보 등록
	 * 
	 * @param params
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxCreateActnInfo.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxCreateActnInfo(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "ajaxCreateActnInfo";
		log.debug(method, ">>> start");	
		//현재 로그인한 세션의 아이디 추가
		params.put("RGST_USER_ID", SessionUtils.getUserId(request));
		//Request param				
		log.debug(method,params.toString());
		
		HashMap map = new HashMap();
		try {
			int ret = menuInfoService.createActnInfo(params);
			if (ret > 0) {
				map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
			} else {
				throw new IncorrectUpdateSemanticsDataAccessException("저장에 실패하였습니다");
			}
		} catch (DuplicateKeyException | IncorrectUpdateSemanticsDataAccessException e) {
 			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			map.put(Constant.RET_MSG, e.getMessage());
			log.error(method, "Exception : "+ e);
 		} catch (Exception e) {
 			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
 			log.printStackTracePH(method, e);
		}
		
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * Request 액션 정보 수정
	 * 
	 * @param params
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxModifyActnInfo.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxModifyActnInfo(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "ajaxModifyActnInfo";
		log.debug(method, ">>> start");	
		//현재 로그인한 세션의 아이디 추가
		params.put("MDFY_USER_ID", SessionUtils.getUserId(request));
		//Request param				
		log.debug(method,params.toString());
		
		HashMap map = new HashMap();
		try {
			int ret = menuInfoService.modifyActnInfo(params);
			if (ret > 0) {
				map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
			} else {
				throw new IncorrectUpdateSemanticsDataAccessException("저장에 실패하였습니다");
			}
		} catch (IncorrectUpdateSemanticsDataAccessException e) {
 			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
 			map.put(Constant.RET_MSG, e.getMessage());
 			log.error(method, "Exception : "+ e);
 		} catch (Exception e) {
 			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
 			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");
		return map;	
	}
	
	/**
	 * jsp 호출 및 초기값 설정
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/system/viewAuthManage.do", method = RequestMethod.POST)
	public String viewAuthManage(Locale locale, Model model) {	
		String method = "viewAuthManage";
		log.debug(method, ">>> start");	

		try{			
			//사용 'Y' 사용자 그룹 코드 가져오기
			List<CmnCdVO> indList = new ArrayList<CmnCdVO>();
			indList = codeManage.getCodeListY("USER_GRP");
			model.addAttribute("USER_GRP", indList);
			
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}
		
		log.debug(method, ">>>>>> end");	
		return "/system/authManageView";
	}
	
	/**
	 * 메뉴 리스트 가져오기
	 * 
	 * @param USER_GRP_ID 	사용자 그룹 아이디
	 * 
	 * @return map 정보
	 */
	
	@ResponseBody
	@RequestMapping(value = "/system/ajaxGetMenuInfo.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxGetMenuInfo(@RequestParam Map<String, Object> params) throws IOException {	
		String method = "ajaxGetMenuInfo";		
		log.debug(method, ">>> start");			
		HashMap map = new HashMap();
		List resultList = new ArrayList();
		try {
			log.debug(method, params.toString());				
			//메뉴레벨 1인 메뉴 가져오기	
			params.put("MENU_LVL", 1);		
			List rootList = new ArrayList();
			rootList = menuInfoService.getMenuInfoLvl(params);
			if(rootList!=null && rootList.size() > 0){
				for(int i=0; i<rootList.size(); i++){
					Map<String, Object> rootTmp = (Map<String, Object>) rootList.get(i);				
					resultList.add(rootTmp);
					List menuList = new ArrayList();
					//메뉴레벨 1메뉴 다음으로 해당 메뉴ID를 상위 메뉴로 가지는 하위메뉴 가져오기
					params.put("UPR_MENU_ID", rootTmp.get("menu_id"));
					menuList = menuInfoService.getMenuInfoUpr(params);
					
					if(menuList!=null && menuList.size() > 0){
						for(int j=0; j<menuList.size(); j++){
							Map<String, Object> menuTmp = (Map<String, Object>) menuList.get(j);				
							
							
							List menuList2 = new ArrayList();
							//메뉴레벨 2메뉴 다음으로 해당 메뉴ID를 상위 메뉴로 가지는 하위메뉴 가져오기
							params.put("UPR_MENU_ID", menuTmp.get("menu_id"));
							menuList2 = menuInfoService.getMenuInfoUpr(params);
							if(menuList2!=null && menuList2.size() > 0){
								menuTmp.put("child", "Y");
								resultList.add(menuTmp);
								for(int k=0; k<menuList2.size(); k++){
									Map<String, Object> menuTmp2 = (Map<String, Object>) menuList2.get(k);
									if(k == menuList2.size()-1){
										menuTmp2.put("last", "Y");
									}
									resultList.add(menuTmp2);
								}
							} else{
								resultList.add(menuTmp);
							}
						}
					}
				}
			}
			log.debug(method, resultList.toString());
			map.put("rows", resultList);
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
 			log.printStackTracePH(method, e);
		}

		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	
	/**
	 * 권한 변경 적용하기
	 * 
	 * @return map 정보
	 */
	@ResponseBody
	@RequestMapping(value = "/system/ajaxSetMenuInfo.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxSetMenuInfo(@RequestParam Map<String, Object> params, HttpServletRequest req) throws IOException {	
		String method = "ajaxSetMenuInfo";		
		log.debug(method, ">>> start");		
		HashMap map = new HashMap();
		try {	
			log.debug(method,params.toString());
			//체크한 목록 가져오기
			//키값만 가져오기 
			List checkList = new ArrayList();
			Iterator iterator = params.entrySet().iterator();
			while(iterator.hasNext()){
				Entry entry = (Entry)iterator.next();
				if(!entry.getKey().equals("USER_GRP_ID")){
					checkList.add(entry.getKey());
				}
			}
			
			map.put("checkList", checkList);
			map.put("RGST_USER_ID", SessionUtils.getUserId(req));
			map.put("USER_GRP_ID", params.get("USER_GRP_ID"));
			
			menuInfoService.unChkMenuAuth(params);
			if(checkList!=null && checkList.size() > 0){
				menuInfoService.chkMenuAuth(map);
			}
			
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
 			log.printStackTracePH(method, e);
		}

		log.debug(method, ">>>>>> end");	
		return map;
	}
	
}
