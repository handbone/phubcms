package com.olleh.pHubCMS.admin.system.service;

import java.util.List;
import java.util.Map;

public interface UserInfoService {
	
	public List viewUserInfo(Map<String, Object> params);
	
	public Map<String, Object> viewUserInfoTotal(Map<String, Object> params);
	
	public List viewUserInfoExcel(Map<String, Object> params);
	
	public int createUserInfo(Map<String, Object> params);	
	
	public Map<String, Object> getUserInfo(Map<String, Object> params);
	
	public int modifyUserInfo(Map<String, Object> params);
	
	public int modifyErrCnt(Map<String, Object> params);
	
	/**
	 * <pre>로그인 상태 Y 변경</pre>
	 * @param params
	 * @return
	 */
	public int modifyLoginY(Map<String, Object> params);
	
	/**
	 * <pre>로그인 상태 N 변경</pre>
	 * @param params
	 * @return
	 */
	public int modifyLoginN(Map<String, Object> params);

}
