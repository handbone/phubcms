package com.olleh.pHubCMS.admin.system.service;

import java.util.List;
import java.util.Map;
import org.springframework.dao.DuplicateKeyException;
import com.olleh.pHubCMS.common.exception.BizException;

@SuppressWarnings("rawtypes")
public interface PntPrvdrMsgInfoService {

	//
	public List viewPntPrvdrMsg(Map<String, Object> params);

	public Map<String, Object> viewPntPrvdrMsgTotal(Map<String, Object> params);

	public Map<String, Object> getPntPrvdrMsg(Map<String, Object> params);

	public List viewPntPrvdrMsgExcel(Map<String, Object> params);

	public int createPntPrvdrMsg(Map<String, Object> params);

	public int modifyPntPrvdrMsg(Map<String, Object> params);

	public int checkPntPrvdrMsgKey(Map<String, Object> params);

	public int deletePntPrvdrMsg(Map<String, Object> params);



	
}
