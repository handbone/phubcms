package com.olleh.pHubCMS.admin.system.service;

import java.util.Map;

public interface SysDataRegService {
	
	public int modifySysData(Map<String, Object> params);

}
