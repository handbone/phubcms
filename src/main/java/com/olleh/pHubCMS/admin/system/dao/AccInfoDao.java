package com.olleh.pHubCMS.admin.system.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * 접근제어 관리 Dao
 * @Class Name : AccInfoDao
 * @author : ojh
 * @since : 2018.08.23
 * @version : 1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *      수정일                 수정자                수정내용
 *  ------------  -----------    -------------
 *  2018. 8. 23.      ojh          최초 생성
 * 
 */

@Repository
public class AccInfoDao extends AbstractDAO{
	
	public List viewAccInfo(Map<String, Object> params) {
		return selectList("mybatis.system.accInfo", params);
	}	
	public List viewAccInfoDtl(Map<String, Object> params) {
		return selectList("mybatis.system.accInfoDtl", params);
	}
	public int createAccInfo(Map<String, Object> params) {
		return insert("mybatis.system.createAccInfo", params);
	}
	public Map<String, Object> getAccInfo(Map<String, Object> params) {
		return selectOne("mybatis.system.getAccInfo", params);
	}
	public int modifyAccInfo(Map<String, Object> params) {
		return insert("mybatis.system.modifyAccInfo", params);
	}
	public int createAccInfoDtl(Map<String, Object> params) {
		return insert("mybatis.system.createAccInfoDtl", params);
	}
	public int modifyAccInfoDtl(Map<String, Object> params) {
		return insert("mybatis.system.modifyAccInfoDtl", params);
	}	
	public List getCorpId(Map<String, Object> params) {
		return selectList("mybatis.system.getCorpId", params);
	}	
	public List getCorpIp(Map<String, Object> params) {
		return selectList("mybatis.system.getCorpIp", params);
	}

}
