package com.olleh.pHubCMS.admin.system.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.system.dao.SysDataRegDao;

/**
 * 시스템데이터반영 Service
 * 
 * @Class Name : SysDataRegService
 * @author ojh
 * @since 2018.10.18
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.18   ojh        최초생성
 * 
 */

@Service
public class SysDataRegServiceImpl implements SysDataRegService{
	
	@Autowired
	SysDataRegDao sysDataRegDao;
	
	@Override
	public int modifySysData(Map<String, Object> params){
		return sysDataRegDao.modifySysData(params);
	}

}
