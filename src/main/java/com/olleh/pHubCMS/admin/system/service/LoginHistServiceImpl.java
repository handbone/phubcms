package com.olleh.pHubCMS.admin.system.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.system.dao.LoginHistDao;

/**
 * 로그인이력 Service Impl
 * @Class Name : LoginHistServiceImpl
 * @author bmg
 * @since 2018.09.20
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.20    bmg        최초생성
 * </pre>
 */
@Service
public class LoginHistServiceImpl implements LoginHistService {

	@Resource
	LoginHistDao loginHistDao;
	
	@Override
	public Map<String, Object> viewLoginHistTotal(Map<String, Object> params) {
		return loginHistDao.viewLoginHistTotal(params);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List viewLoginHist(Map<String, Object> params) {
		return loginHistDao.viewLoginHist(params);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List loginHistExcel(Map<String, Object> params) {
		return loginHistDao.loginHistExcel(params);
	}
	
	
}
