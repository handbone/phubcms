package com.olleh.pHubCMS.admin.system.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * 로그인이력 Dao
 * @Class Name : LoginHistDao
 * @author : bmg
 * @since : 2018.09.19
 * @version : 1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *      수정일                 수정자                수정내용
 *  ------------  -----------    -------------
 *  2018. 9. 19.      bmg          최초 생성
 * 
 */
@Repository
public class LoginHistDao extends AbstractDAO {
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> viewLoginHistTotal(Map<String, Object> params) {
		return selectOne("mybatis.system.userLoginHist.loginHistTotal", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List viewLoginHist(Map<String, Object> params) {
		return selectList("mybatis.system.userLoginHist.loginHist", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List loginHistExcel(Map<String, Object> params) {
		return selectList("mybatis.system.userLoginHist.loginHistExcel", params);
	}
}
