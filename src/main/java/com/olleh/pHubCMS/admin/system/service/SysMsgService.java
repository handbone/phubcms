package com.olleh.pHubCMS.admin.system.service;

import java.util.List;
import java.util.Map;

import org.springframework.dao.DuplicateKeyException;

import com.olleh.pHubCMS.common.exception.BizException;

public interface SysMsgService {

	public Map<String, Object> viewSysMsgTotal(Map<String, Object> params);
	
	@SuppressWarnings("rawtypes")
	public List viewSysMsg(Map<String, Object> params);
	
	@SuppressWarnings("rawtypes")
	public List viewSysMsgExcel(Map<String, Object> params);
	
	public Map<String, Object> getSysMsg(Map<String, Object> params);
	
	public Map<String, Object> getSysMsgDuplTotal(Map<String, Object> params);
	
	public int modifySysMsg(Map<String, Object> params);
	
	public int createSysMsg(Map<String, Object> params) throws BizException, DuplicateKeyException;
}
