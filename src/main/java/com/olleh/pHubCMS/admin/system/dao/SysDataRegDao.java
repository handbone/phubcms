package com.olleh.pHubCMS.admin.system.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * 시스템데이터반영 Dao
 * @Class Name : SysDataRegDao
 * @author : ojh
 * @since : 2018.10.18
 * @version : 1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *      수정일                 수정자                수정내용
 *  ------------  -----------    -------------
 *  2018. 10. 18      ojh          최초 생성
 * 
 */
@Repository
public class SysDataRegDao extends AbstractDAO {
	
	public int modifySysData(Map<String, Object> params) {
		return update("mybatis.system.modifySysData", params);
	}	

}
