package com.olleh.pHubCMS.admin.system.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.system.service.LoginHistService;
import com.olleh.pHubCMS.common.components.CodeManage;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.CmnCdVO;
import com.olleh.pHubCMS.common.utils.Paging;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 로그인이력 Controller
 * @Class Name : LoginHistController
 * @author : bmg
 * @since : 2018.09.20
 * @version : 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.20   bmg        최초생성
 * </pre>
 */

@Controller
public class LoginHistController {
	
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	CodeManage codeManage;
	
	@Autowired
	LoginHistService loginHistService;
	
	/**
	 * jsp 호출 및 초기값 설정
	 * 
	 * @param locale
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/system/viewLoginHist.do", method = RequestMethod.POST)
	public String viewLoginHist(Locale locale, Model model) {	
		String method = "viewLoginHist";
		log.debug(method, ">>> start");
		
		List<CmnCdVO> userGrp = new ArrayList<CmnCdVO>();
		userGrp = codeManage.getCodeListY("USER_GRP");
		model.addAttribute("USER_GRP", userGrp.toArray());
		
		log.debug(method, ">>>>>> end");	
		return "/system/loginHistView";
	}
	
	/**
	 * 로그인이력 JQGrid 호출
	 * 
	 * @param params
	 * @param req
	 * @param res
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/system/jgLoginHist.do", method = RequestMethod.POST)
	public Map<String, Object> jgLoginHist(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgLoginHist";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		int pageNum = Integer.parseInt(request.getParameter("page"));
		int limit = Integer.parseInt(request.getParameter("rows"));	
		log.debug(method,params.toString());
		
		count = loginHistService.viewLoginHistTotal(params);
		int countRow = Integer.parseInt(count.get("cnt").toString());
		
		HashMap map = new HashMap();
		if (countRow > 0) {
			//HashMap에 페이징 정보 저장					
			Paging.setPageMap(params, map, pageNum, limit, countRow);	
			log.debug(method,map.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			log.debug(method,params.toString());
			resultList = loginHistService.viewLoginHist(params);
			
			mappingLoginHist(resultList);
			map.put("rows", resultList);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 로그인이력 JQGrid 호출 엑셀
	 * 
	 * @param params
	 * @param req
	 * @param res
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/system/jgLoginHistExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgLoginHistExcel(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgLoginHistExcel";
		log.debug(method, ">>> start");			
		HashMap map = new HashMap();
		try {
			//Request param
			log.debug(method,params.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			resultList = loginHistService.loginHistExcel(params);
			
			mappingLoginHist(resultList);
			map.put("rows", resultList);
			
		} catch (Exception e) {
			log.error(method,"Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * 로그인 이력 code -> 이름 변환
	 * 마스킹 처리
	 * @param resultList
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void mappingLoginHist(List resultList) {
		for (int i=0; i<resultList.size(); i++) {
			Map<String, Object> resultTmp = (Map<String, Object>) resultList.get(i);
			if (resultTmp.get("user_nm") != null) {
				resultTmp.put("user_nm", StringUtil.getMaskingName(resultTmp.get("user_nm").toString()));
			}
			if (resultTmp.get("user_id") != null) {
				resultTmp.put("user_id", StringUtil.getMaskingId(resultTmp.get("user_id").toString()));
			}
			if (resultTmp.get("login_ip") != null) {
				resultTmp.put("login_ip", StringUtil.getMaskingIP(resultTmp.get("login_ip").toString()));
			}
			resultList.set(i, resultTmp);
		}
	}
}
