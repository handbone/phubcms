package com.olleh.pHubCMS.admin.system.service;

import java.util.List;
import java.util.Map;

import org.springframework.dao.DuplicateKeyException;

import com.olleh.pHubCMS.common.exception.BizException;

@SuppressWarnings("rawtypes")
public interface AgrInfoService {

	public List viewAgrInfo(Map<String, Object> params);
	
	public Map agrInfo(Map<String, Object> params);
	
	public List<Map> viewPrvdrItem(Map<String, Object> params);
	
	public Map prvdrItem(Map<String, Object> params);
	
	public int insertAgrInfo(Map<String, Object> params) throws BizException, DuplicateKeyException;
	
	public int updateAgrInfo(Map<String, Object> params);
	
	public int insertPrvdrItem(Map<String, Object> params) throws BizException, DuplicateKeyException;
	
	public int updatePrvdrItem(Map<String, Object> params);
}
