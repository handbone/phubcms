package com.olleh.pHubCMS.admin.system.model;

/**
 * 접근제어 정보 VO
 * 
 * @Class Name : AccInfoVo
 * @author ojh
 * @since 2018.08.23
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.23   ojh        최초생성
 * 
 */

public class AccInfoVo {
	
	private String ctrl_id;			// 제어ID
	private String rltn_corp_ind;	// 관계사 구분
	private String rltn_corp_id;	// 관계사아이디
	private String ctrl_stat;		// 제어상태
	private String rgst_dt;			// 등록일시
	private String rgst_user_id;	// 등록자ID
	private String mdfy_dt;			// 수정일시
	private String mdfy_user_id;	// 수정자ID
	
	
	private String ip_addr;			// IP주소
	private String ip_stat;			// IP상태
	

}
