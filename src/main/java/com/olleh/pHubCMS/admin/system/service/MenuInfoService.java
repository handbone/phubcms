package com.olleh.pHubCMS.admin.system.service;

import java.util.List;
import java.util.Map;

import org.springframework.dao.DuplicateKeyException;

import com.olleh.pHubCMS.common.exception.BizException;

@SuppressWarnings("rawtypes")
public interface MenuInfoService {
	
	public List getMenuInfoLvl(Map<String, Object> params);
	public List getMenuInfoUpr(Map<String, Object> params);
	public List getMenuInfo(Map<String, Object> params);
	public int chkMenuAuth(Map<String, Object> params);
	public int unChkMenuAuth(Map<String, Object> params);
	
	public List menuInfoView(Map<String, Object> params);
	
	public List uprMenuInfo(Map<String, Object> params);
	
	public int createMenuInfo(Map<String, Object> params) throws BizException, DuplicateKeyException;
	
	public int modifyMenuInfo(Map<String, Object> params);
	
	public Map<String, Object> getMenuInfoView(Map<String, Object> params);
	
	public List actnInfoView(Map<String, Object> params);
	
	public Map<String, Object> getActnInfo(Map<String, Object> params);
	
	public int createActnInfo(Map<String, Object> params) throws BizException;
	
	public int modifyActnInfo(Map<String, Object> params);
}
