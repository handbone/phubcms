package com.olleh.pHubCMS.admin.system.model;

/**
 * 사용자 메뉴리스트 정보 VO
 * 
 * @Class Name : UserMenuListVO
 * @author lys
 * @since 2018.08.27
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.27   lys        최초생성
 * 
 */
public class UserMenuListVO {
	
	private String menuId;		// 메뉴ID
	private String menuNm;		// 메뉴명
	private String menuInd;		// 메뉴구분(POINT HUB / CMS)
	private String menuUri;		// 메뉴 URI
	private String menuLvl;		// 메뉴 레벨
	private String useYn;		// 사용여부
	private String dpYn;		// 출력여부
	private String uprMenuId;	// 상위메뉴ID
	private String menuOrd;		// 정렬순서
	private String level;		// 레벨
	private String ordPath;		// 정렬순서 PATH
	private String mnuPath;		// 메뉴 PATH
	
	
	/**
	 * getter / setter
	 */	
	public String getMenuId() {
		return menuId;
	}
	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}
	public String getMenuNm() {
		return menuNm;
	}
	public void setMenuNm(String menuNm) {
		this.menuNm = menuNm;
	}
	public String getMenuInd() {
		return menuInd;
	}
	public void setMenuInd(String menuInd) {
		this.menuInd = menuInd;
	}
	public String getMenuUri() {
		return menuUri;
	}
	public void setMenuUri(String menuUri) {
		this.menuUri = menuUri;
	}
	public String getMenuLvl() {
		return menuLvl;
	}
	public void setMenuLvl(String menuLvl) {
		this.menuLvl = menuLvl;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getDpYn() {
		return dpYn;
	}
	public void setDpYn(String dpYn) {
		this.dpYn = dpYn;
	}
	public String getUprMenuId() {
		return uprMenuId;
	}
	public void setUprMenuId(String uprMenuId) {
		this.uprMenuId = uprMenuId;
	}
	public String getMenuOrd() {
		return menuOrd;
	}
	public void setMenuOrd(String menuOrd) {
		this.menuOrd = menuOrd;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getOrdPath() {
		return ordPath;
	}
	public void setOrdPath(String ordPath) {
		this.ordPath = ordPath;
	}
	public String getMnuPath() {
		return mnuPath;
	}
	public void setMnuPath(String mnuPath) {
		this.mnuPath = mnuPath;
	}
}
