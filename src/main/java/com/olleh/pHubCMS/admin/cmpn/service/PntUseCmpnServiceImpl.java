package com.olleh.pHubCMS.admin.cmpn.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.olleh.pHubCMS.admin.cmpn.dao.PntUseCmpnDao;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.utils.StringUtil;


/**
 * 포인트 사용처 관리 Service
 * 
 * @Class Name : PntUseCmpnService
 * @author ojh
 * @since 2018.08.14
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.14   ojh        최초생성
 * 
 */
@Service
@SuppressWarnings({"rawtypes", "unchecked"})
public class PntUseCmpnServiceImpl implements PntUseCmpnService {	
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	PntUseCmpnDao pntUseCmpnDao;
	
	@Override
	public List getAllPntUseCmpn(){
		return pntUseCmpnDao.getAllPntUseCmpn();
	}
	
	@Override
	public List viewPntUseCmpn(Map<String, Object> params){
		return maskingName(pntUseCmpnDao.viewPntUseCmpn(params));		
	}
	@Override
	public Map<String, Object> viewPntUseCmpnTotal(Map<String, Object> params){
		return pntUseCmpnDao.viewPntUseCmpnTotal(params);
	}
	
	@Override
	public List viewPntUseCmpnExcel(Map<String, Object> params){
		return maskingName(pntUseCmpnDao.viewPntUseCmpnExcel(params));		
	}
	
	@Override
	@Transactional
	public int createPntUseCmpn(Map<String, Object> params){
		int result = 0;
		try{
			//포인트 사용처 등록
			result = pntUseCmpnDao.createPntUseCmpn(params);
			if (result < 1) {
				throw new Exception("포인트 사용처 등록 중 처리되지 않았습니다.");
			}
			//PG사 제휴 등록
			result = pntUseCmpnDao.createPgCprt(params);
			if (result < 1) {
				throw new Exception("PG사 제휴 등록 중 처리되지 않았습니다.");
			}
			//사용처별제공처 등록
			result = pntUseCmpnDao.createPgCprtPrvdr(params);
			if (result < 1) {
				throw new Exception("사용처별제공처 등록 중 처리되지 않았습니다");
			}
			//수수료율 등록
			result = pntUseCmpnDao.createPgCprtCmsn(params);
			if (result < 1) {
				throw new Exception("수수료율 등록 중 처리되지 않았습니다.");
			}
		} catch (Exception e){
			log.error("createPntUseCmpn", "Exception : "+e.getMessage());		
		}
		return result;
	}
	
	@Override
	public Map<String, Object> getPntUseCmpn(Map<String, Object> params){
		return maskingName(pntUseCmpnDao.getPntUseCmpn(params));
	}
	
	@Override
	public List getPgCprtPrvdr(Map<String, Object> params) {
		return pntUseCmpnDao.getPgCprtPrvdr(params);
	}

	@Override
	@Transactional
	public int modifyUseCmpn(Map<String, Object> params) {	
		int result = 0;
		try{
			//포인트 사용처 수정
			result = pntUseCmpnDao.modifyUseCmpn(params);
			if (result == 0) {
				throw new Exception("포인트 사용처 수정 중 처리되지 않았습니다.");
			}
			//PG사 제휴 등록
			result = pntUseCmpnDao.modifyPgCprt(params);
			if (result == 0) {
				throw new Exception("PG사 제휴 수정 중 처리되지 않았습니다.");
			}
			//사용처별제공처 수정
			result = pntUseCmpnDao.deletePgCprtPrvdr(params);
			result = pntUseCmpnDao.createPgCprtPrvdr(params);
			if (result == 0) {
				throw new Exception("사용처별제공처 수정 중 처리되지 않았습니다");
			}
			//수수료율 등록
			if (params.get("MDFY_CMSN_DATE").equals("1")) {
				//수수료율 적용일자 변경시 
				result = pntUseCmpnDao.createPgCprtCmsnHist1(params);
				result = pntUseCmpnDao.createPgCprtCmsnHist2(params);
				if (result == 0) {
					throw new Exception("수수료율 수정 중 처리되지 않았습니다. - " + params.get("MDFY_CMSN_DATE"));
				}
			} else if (params.get("MDFY_CMSN_DATE").equals("2")) {
				//수수료율 적용일자 변경시(기존 수수료율 없을때)
				result = pntUseCmpnDao.createPgCprtCmsnHist2(params);
				if (result == 0) {
					throw new Exception("수수료율 수정 중 처리되지 않았습니다. - " + params.get("MDFY_CMSN_DATE"));
				}
			} else {
				//수수료율만 수정
				result = pntUseCmpnDao.modifyPgCprtCmsnHist(params);
				if (result == 0) {
					throw new Exception("수수료율 수정 중 처리되지 않았습니다. - " + params.get("MDFY_CMSN_DATE"));
				}
			}
		} catch (Exception e){
			log.error("modifyUseCmpn", "Exception : "+e.getMessage());		
		}
		return result;
	}
	@Override
	public Map<String, Object> getBizNo(Map<String, Object> params) {
		return pntUseCmpnDao.getBizNo(params);
	}
	/**
	 * 이름 마스킹 처리
	 * 
	 * @param params
	 * @return
	 */
	private List maskingName(List<Map> params) {
		int index = 0;
		if (params == null) {
			return params;
		}
		for (Map map : params) {
			if (map.get("rgst_user_nm") != null) {
				map.put("rgst_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("rgst_user_nm"))));
			}
			if (map.get("mdfy_user_nm") != null) {
				map.put("mdfy_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("mdfy_user_nm"))));
			}
			params.set(index, map);
			index++;
		}
		return params;
	}
	/**
	 * 이름 마스킹 처리
	 * 
	 * @param params
	 * @return
	 */
	private Map maskingName(Map map) {
		if (map != null) {
			if (map.get("rgst_user_nm") != null) {
				map.put("rgst_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("rgst_user_nm"))));
			}
			if (map.get("mdfy_user_nm") != null) {
				map.put("mdfy_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("mdfy_user_nm"))));
			}
		}
		return map;
	}
}
