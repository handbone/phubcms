package com.olleh.pHubCMS.admin.cmpn.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectUpdateSemanticsDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.cmpn.service.PntPrvdrServiceImpl;
import com.olleh.pHubCMS.common.components.Constant;
import com.olleh.pHubCMS.common.exception.UserException;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.utils.Paging;
import com.olleh.pHubCMS.common.utils.SessionUtils;

/**
 * 포인트 제공처 관리 Controller
 * 
 * @Class Name : PntPrvdrController
 * @author ojh
 * @since 2018.10.01
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.01   ojh        최초생성
 * 
 */
@Controller
@SuppressWarnings({ "rawtypes", "unchecked" })
public class PntPrvdrController {
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	PntPrvdrServiceImpl pntPrvdrService;
	
	/**
	 * jsp 호출 및 초기값 설정
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/cmpn/viewPntPrvdr.do", method = RequestMethod.POST)
	public String viewPntPrvdr(Locale locale, Model model) {	
		String method = "viewPntPrvdr";
		log.debug(method, ">>> start");		
		
		log.debug(method, ">>>>>> end");
		return "/cmpn/pntPrvdrView";
	}
	
	/**
	 * JQGrid 호출
	 * 
	 * @param page 현재 보고자 하는 페이지 (JQgrid 처리)
	 * @param rows 한 페이지에 보여주고자 설정된 개수 (JQgrid 처리)
	 * @param PRVDR_ID 	제공처 ID 	(검색 Input)
	 * @param PRVDR_NM 	제공처명 	(검색 Input)
	 * @param PNT_CD 	포인트코드	(검색 Input)
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/cmpn/jgPntPrvdr.do", method = RequestMethod.POST)
	public Map<String, Object> jgPntPrvdr(@RequestParam Map<String, Object> params, HttpServletRequest req) throws IOException {	
		String method = "jgPntPrvdr";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		Map<String, Object> count = new HashMap<String, Object>();
		
		//Request param
		int pageNum = Integer.parseInt(req.getParameter("page"));
		int limit = Integer.parseInt(req.getParameter("rows"));	
		
		//총 Row 수 계산 		
		count = pntPrvdrService.viewPntPrvdrTotal(params);		
		int countRow = Integer.parseInt(count.get("cnt").toString());			
		
		if(countRow > 0){
			//HashMap에 페이징 정보 저장					
			Paging.setPageMap(params, map, pageNum, limit, countRow);	
			log.debug(method,map.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			resultList = pntPrvdrService.viewPntPrvdr(params);		
			map.put("rows", resultList );
		}

		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * JQGrid 호출 (엑셀용)
	 * 
	 * @param page 현재 보고자 하는 페이지 (JQgrid 처리)
	 * @param rows 한 페이지에 보여주고자 설정된 개수 (JQgrid 처리)
	 * @param PRVDR_ID 	제공처 ID 	(검색 Input)
	 * @param PRVDR_NM 	제공처명 	(검색 Input)
	 * @param PNT_CD 	포인트코드	(검색 Input)
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/cmpn/jgPntPrvdrExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgPntPrvdrExcel(@RequestParam Map<String, Object> params) throws IOException {	
		String method = "jgPntPrvdrExcel";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		
		//Request param			
		log.debug(method, params.toString());
	
		//검색 결과 데이터 map 에 추가
		List resultList = new ArrayList();
		resultList = pntPrvdrService.viewPntPrvdrExcel(params);		
		map.put("rows", resultList );

		log.debug(method, ">>>>>> end");
		return map;
	}
	
	
	/**
	 * 포인트 제공처 수정 (상세)
	 * 
	 * @param page 현재 보고자 하는 페이지 (JQgrid 처리)
	 * @param rows 한 페이지에 보여주고자 설정된 개수 (JQgrid 처리)
	 * @param PRVDR_ID 	제공처 ID 
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/cmpn/ajaxGetPntPrvdr.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxGetPntPrvdr(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "ajaxGetPntPrvdr.do";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		Map<String, Object> prvdr = new HashMap<String, Object>();
		
		try{			
			//검색 결과 데이터 map 에 추가
			log.debug(method,params.toString());
			prvdr = pntPrvdrService.getPntPrvdr(params);
			map.put("rows", prvdr );	
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}

		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 포인트 제공처 추가
	 * 
	 * @param PRVDR_ID 			제공처 ID 
	 * @param PRVDR_NM 			Input 제공처명
	 * @param PNT_CD  			Input 포인트코드 
	 * @param PNT_NM 			Input 포인트명
	 * @param DP_UNIT 			Input 표시단위
	 * @param DEAL_UNIT 		Input 거래단위
	 * @param WINDOW_YN 		ChkBox 윈도우 지원
	 * @param ANDR_YN 			ChkBox 안드로이드 지원
	 * @param IOS_YN 			ChkBox IOS 지원
	 * @param ATHN_KEY			Input 인증키
	 * @param PNT_CMSN_RATE		Input 수수료율
	 * @param PNT_EXCH_RATE		Input 전환율
	 * @param RATE_APLY_STRT_DD_CMSN	달력	수수료율적용일자
	 * @param RATE_APLY_STRT_DD_EXCH    달력	전환율적용일자
	 * 
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/cmpn/ajaxCreatePntPrvdr.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxCreatePntPrvdr(@RequestParam Map<String, Object> params, HttpServletRequest req) throws IOException {	
		String method = "ajaxCreatePntPrvdr.do";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();		
		try{			
			//현재 로그인한 세션의 아이디 추가
			params.put("MDFY_USER_ID", SessionUtils.getUserId(req));
			log.debug(method,params.toString());
			
			int ret = pntPrvdrService.createPntPrvdr(params);
			if (ret > 0) {
				map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
			} else {
				throw new IncorrectUpdateSemanticsDataAccessException("저장에 실패하였습니다");
			}
		} catch (IncorrectUpdateSemanticsDataAccessException e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
 			map.put(Constant.RET_MSG, e.getMessage());
 			log.error(method, "Exception : "+ e);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}

		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 포인트 제공처 수정
	 * 
	 * @param page 현재 보고자 하는 페이지 (JQgrid 처리)
	 * @param rows 한 페이지에 보여주고자 설정된 개수 (JQgrid 처리)
	 * @param PRVDR_ID 			제공처 ID 
	 * @param PRVDR_NM 			Input 제공처명
	 * @param PNT_CD  			Input 포인트코드 
	 * @param PNT_NM 			Input 포인트명
	 * @param DP_UNIT 			Input 표시단위
	 * @param DEAL_UNIT 		Input 거래단위
	 * @param WINDOW_YN 		ChkBox 윈도우 지원
	 * @param ANDR_YN 			ChkBox 안드로이드 지원
	 * @param IOS_YN 			ChkBox IOS 지원
	 * @param ATHN_KEY			Input 인증키
	 * @param PNT_CMSN_RATE		Input 수수료율
	 * @param PNT_EXCH_RATE		Input 전환율
	 * @param RATE_APLY_STRT_DD_CMSN	달력	수수료율적용일자
	 * @param RATE_APLY_STRT_DD_EXCH    달력	전환율적용일자
	 * @param MDFY_CMSN_DATE	수수료 적용일자 변경유무
	 * @param MDFY_EXCH_DATE	전환율 적용일자 변경 유무
	 * 
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/cmpn/ajaxModifyPntPrvdr.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxModifyPntPrvdr(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "ajaxModifyPntPrvdr.do";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();		
		try{			
			//현재 로그인한 세션의 아이디 추가
			params.put("MDFY_USER_ID", SessionUtils.getUserId(req));
			log.debug(method,params.toString());
			
			int ret = pntPrvdrService.modifyPntPrvdr(params);
			if (ret > 0) {
				map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
			} else {
				throw new IncorrectUpdateSemanticsDataAccessException("저장에 실패하였습니다");
			}
		} catch (IncorrectUpdateSemanticsDataAccessException e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
 			map.put(Constant.RET_MSG, e.getMessage());
 			log.error(method, "Exception : "+ e);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}

		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 포인트 제공처 수수료 이력 조회 
	 * 
	 * @param PRVDR_ID 	제공처 ID 
	 * @return void response 응답
	 */	
	@ResponseBody
	@RequestMapping(value = "/cmpn/ajaxPntPrvdrCmsnHist.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxPntPrvdrCmsnHist(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "ajaxPntPrvdrCmsnHist.do";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		try {
			//검색 결과 데이터 map 에 추가
			log.debug(method,params.toString());
			List resultList = new ArrayList();
			resultList = pntPrvdrService.viewPntPrvdrCmsnHist(params);
			map.put("rows", resultList );	
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}

		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 포인트 제공처 전환율 이력 조회 
	 * 
	 * @param PRVDR_ID 	제공처 ID 
	 * @return void response 응답
	 */	
	@ResponseBody
	@RequestMapping(value = "/cmpn/ajaxPntPrvdrExchHist.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxPntPrvdrExchHist(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "ajaxPntPrvdrExchHist.do";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		try {			
			//검색 결과 데이터 map 에 추가
			log.debug(method,params.toString());
			List resultList = new ArrayList();
			resultList = pntPrvdrService.viewPntPrvdrExchHist(params);
			map.put("rows", resultList );	
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}

		log.debug(method, ">>>>>> end");
		return map;
	}

}
