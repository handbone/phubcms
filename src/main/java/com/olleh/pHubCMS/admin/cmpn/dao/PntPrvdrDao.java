package com.olleh.pHubCMS.admin.cmpn.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * 제공처 관리 Dao
 * @Class Name : PntPrvdrDao
 * @author : ojh
 * @since : 2018.08.27
 * @version : 1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *      수정일                 수정자                수정내용
 *  ------------  -----------    -------------
 *  2018. 8. 17.      ojh          최초 생성
 * 
 */

@Repository
public class PntPrvdrDao extends AbstractDAO{
	
	public List getAllPrvdrCmpn() {
		return selectList("mybatis.cmpn.allPrvdrCmpn");
	}
	
	public List viewPntPrvdr(Map<String, Object> params) {
		return selectList("mybatis.cmpn.pntPrvdr", params);
	}	
	public Map<String, Object> viewPntPrvdrTotal(Map<String, Object> params) {
		return selectOne("mybatis.cmpn.pntPrvdrTotal", params);
	}	
	public List viewPntPrvdrExcel(Map<String, Object> params) {
		return selectList("mybatis.cmpn.pntPrvdrExcel", params);
	}	
	public Map<String, Object> getPntPrvdr(Map<String, Object> params) {
		return selectOne("mybatis.cmpn.getPntPrvdr", params);
	}		
	public List viewPntPrvdrCmsnHist(Map<String, Object> params) {
		return selectList("mybatis.cmpn.pntPrvdrCmsnHist", params);
	}	
	public List viewPntPrvdrExchHist(Map<String, Object> params) {
		return selectList("mybatis.cmpn.pntPrvdrExchHist", params);
	}	
	
	public int createPntPrvdr(Map<String, Object> params) {
		return insert("mybatis.cmpn.createPntPrvdr", params);
	}		
	public int modifyPntPrvdr(Map<String, Object> params) {
		return update("mybatis.cmpn.modifyPntPrvdr", params);
	}	
	
	public int modifyPrvdrCmsnHist(Map<String, Object> params) {
		return update("mybatis.cmpn.modifyPrvdrCmsnHist", params);
	}		
	public int createPrvdrCmsnHist1(Map<String, Object> params) {
		return update("mybatis.cmpn.createPrvdrCmsnHist1", params);
	}	
	public int createPrvdrCmsnHist2(Map<String, Object> params) {
		return insert("mybatis.cmpn.createPrvdrCmsnHist2", params);
	}	
	public int modifyPrvdrExchHist(Map<String, Object> params) {
		return update("mybatis.cmpn.modifyPrvdrExchHist", params);
	}
	public int createPrvdrExchHist1(Map<String, Object> params) {
		return update("mybatis.cmpn.createPrvdrExchHist1", params);
	}	
	public int createPrvdrExchHist2(Map<String, Object> params) {
		return insert("mybatis.cmpn.createPrvdrExchHist2", params);
	}	
	public int createPgCprtPrvdrToPrvdr(Map<String, Object> params) {
		return insert("mybatis.cmpn.createPgCprtPrvdrToPrvdr", params);
	}
	public int modifyPgCprtPrvdrByImagLink(Map<String, Object> params) {
		return update("mybatis.cmpn.modifyPgCprtPrvdrByImagLink", params);
	}
}
