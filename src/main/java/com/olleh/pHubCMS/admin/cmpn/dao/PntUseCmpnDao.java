package com.olleh.pHubCMS.admin.cmpn.dao;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import com.google.gson.Gson;
import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * 포인트 사용처 관리 Dao
 * @Class Name : PntUseCmpnDao
 * @author : ojh
 * @since : 2018.08.14
 * @version : 1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *      수정일                 수정자                수정내용
 *  ------------  -----------    -------------
 *  2018. 8. 14.      ojh          최초 생성
 * 
 */
@Repository
public class PntUseCmpnDao extends AbstractDAO{
	
	@SuppressWarnings("rawtypes")
	public List getAllPntUseCmpn() {
		return selectList("mybatis.cmpn.allPntUseCmpn");
	}	
	@SuppressWarnings("rawtypes")
	public List viewPntUseCmpn(Map<String, Object> params) {
		return selectList("mybatis.cmpn.pntUseCmpn", params);
	}	
	@SuppressWarnings("unchecked")
	public Map<String, Object> viewPntUseCmpnTotal(Map<String, Object> params) {
		return selectOne("mybatis.cmpn.pntUseCmpnTotal", params);
	}	
	@SuppressWarnings("rawtypes")
	public List viewPntUseCmpnExcel(Map<String, Object> params) {
		return selectList("mybatis.cmpn.pntUseCmpnExcel", params);
	}	
	public int createPntUseCmpn(Map<String, Object> params) {
		return insert("mybatis.cmpn.createUseCmpn", params);
	}	
	public int createPgCprt(Map<String, Object> params) {
		return insert("mybatis.cmpn.createPgCprt", params);
	}	
	public int createPgCprtCmsn(Map<String, Object> params) {
		return insert("mybatis.cmpn.createPgCprtCmsn", params);
	}	
	@SuppressWarnings("unchecked")
	public Map<String, Object> getPntUseCmpn(Map<String, Object> params) {
		return selectOne("mybatis.cmpn.getPntUseCmpn", params);
	}
	@SuppressWarnings("rawtypes")
	public List getPgCprtPrvdr(Map<String, Object> params) {
		return selectList("mybatis.cmpn.getPgCprtPrvdr", params);
	}
	public int modifyUseCmpn(Map<String, Object> params) {
		return update("mybatis.cmpn.modifyUseCmpn", params);
	}	
	public int modifyPgCprt(Map<String, Object> params) {
		return update("mybatis.cmpn.modifyPgCprt", params);
	}
	public int modifyPgCprtCmsnHist(Map<String, Object> params) {
		return update("mybatis.cmpn.modifyPgCprtCmsnHist", params);
	}		
	public int createPgCprtCmsnHist1(Map<String, Object> params) {
		return update("mybatis.cmpn.createPgCprtCmsnHist1", params);
	}	
	public int createPgCprtCmsnHist2(Map<String, Object> params) {
		return insert("mybatis.cmpn.createPgCprtCmsnHist2", params);
	}	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public int createPgCprtPrvdr(Map<String, Object> params) {
		int i = 0;
		Gson gson = new Gson();
		List<Map> prvdrList = gson.fromJson(params.get("PRVDR_LIST").toString(), List.class);
		for (Map map : prvdrList) {
			if (!StringUtils.isEmpty(map.get("sort_ord").toString())) {
				map.put("sort_ord", Integer.parseInt(map.get("sort_ord").toString()));
				prvdrList.set(i, map);
			} else {
				map.put("sort_ord", (i+1));
			}
			i++;
		}
		params.put("PRVDR_LIST", prvdrList);
		return insert("mybatis.cmpn.createPgCprtPrvdr", params);
	}	
	public int deletePgCprtPrvdr(Map<String, Object> params) {
		return delete("mybatis.cmpn.deletePgCprtPrvdr", params);
	}	
	@SuppressWarnings("unchecked")
	public Map<String, Object> getBizNo(Map<String, Object> params) {
		return selectOne("mybatis.cmpn.getBizNo", params);
	}	
	

}
