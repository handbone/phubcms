package com.olleh.pHubCMS.admin.cmpn.model;

import java.util.ArrayList;

/**
 * 포인트 사용처 관리 VO
 * 
 * @Class Name : PntUseCmpnVo
 * @author ojh
 * @since 2018.08.14
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.14   ojh        최초생성
 * 
 */

public class PntUseCmpnVo {
	
	private String cprt_cmpn_id;		// 가맹점 ID
	private String cprt_cmpn_nm;		// 가맹점명
	private String biz_no;				// 사업자번호
	private String pg_cmpn_nm;			// PG사 명 
	private String pg_cmpn_id;			// PG제공 가맹점 ID
	private String rchr_pay_ind;		// 결제구분
	private String pg_send_po_id;		// pg제공 사용처 ID
	private String rate_aply_strt_dd;	// 수수료율 적용일자
	private String kt_cmsn_rate; 		// KT수수료율
	private String pg_cmsn_rate;		// PG수수료율
	private ArrayList<CprtPrvdrVo> prvdr_list;	//제공처 목록
	
	private String mdfy_cmsn_date;		// cmsn
	
	private String mdfy_user_id;		// 수정자 ID
	private String mdfy_dt;				// 수정일시
	
	public String getCprt_cmpn_id() {
		return cprt_cmpn_id;
	}
	public void setCprt_cmpn_id(String cprt_cmpn_id) {
		this.cprt_cmpn_id = cprt_cmpn_id;
	}
	public String getCprt_cmpn_nm() {
		return cprt_cmpn_nm;
	}
	public void setCprt_cmpn_nm(String cprt_cmpn_nm) {
		this.cprt_cmpn_nm = cprt_cmpn_nm;
	}
	public String getBiz_no() {
		return biz_no;
	}
	public void setBiz_no(String biz_no) {
		this.biz_no = biz_no;
	}
	public String getPg_cmpn_nm() {
		return pg_cmpn_nm;
	}
	public void setPg_cmpn_nm(String pg_cmpn_nm) {
		this.pg_cmpn_nm = pg_cmpn_nm;
	}
	public String getPg_cmpn_id() {
		return pg_cmpn_id;
	}
	public void setPg_cmpn_id(String pg_cmpn_id) {
		this.pg_cmpn_id = pg_cmpn_id;
	}
	public String getRchr_pay_ind() {
		return rchr_pay_ind;
	}
	public void setRchr_pay_ind(String rchr_pay_ind) {
		this.rchr_pay_ind = rchr_pay_ind;
	}
	public String getPg_send_po_id() {
		return pg_send_po_id;
	}
	public void setPg_send_po_id(String pg_send_po_id) {
		this.pg_send_po_id = pg_send_po_id;
	}
	public String getRate_aply_strt_dd() {
		return rate_aply_strt_dd;
	}
	public void setRate_aply_strt_dd(String rate_aply_strt_dd) {
		this.rate_aply_strt_dd = rate_aply_strt_dd;
	}
	public String getKt_cmsn_rate() {
		return kt_cmsn_rate;
	}
	public void setKt_cmsn_rate(String kt_cmsn_rate) {
		this.kt_cmsn_rate = kt_cmsn_rate;
	}
	public String getPg_cmsn_rate() {
		return pg_cmsn_rate;
	}
	public void setPg_cmsn_rate(String pg_cmsn_rate) {
		this.pg_cmsn_rate = pg_cmsn_rate;
	}
	public ArrayList<CprtPrvdrVo> getPrvdr_list() {
		return prvdr_list;
	}
	public void setPrvdr_list(ArrayList<CprtPrvdrVo> prvdr_list) {
		this.prvdr_list = prvdr_list;
	}
	public String getMdfy_user_id() {
		return mdfy_user_id;
	}
	public void setMdfy_user_id(String mdfy_user_id) {
		this.mdfy_user_id = mdfy_user_id;
	}
	public String getMdfy_dt() {
		return mdfy_dt;
	}
	public void setMdfy_dt(String mdfy_dt) {
		this.mdfy_dt = mdfy_dt;
	}
	public String getMdfy_cmsn_date() {
		return mdfy_cmsn_date;
	}
	public void setMdfy_cmsn_date(String mdfy_cmsn_date) {
		this.mdfy_cmsn_date = mdfy_cmsn_date;
	}

	

}
