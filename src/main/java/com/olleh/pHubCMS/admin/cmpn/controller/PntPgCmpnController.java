package com.olleh.pHubCMS.admin.cmpn.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.cmpn.service.PntPgCmpnServiceImpl;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.utils.Paging;
import com.olleh.pHubCMS.common.utils.SessionUtils;

/**
 * PG사 관리 Controller
 * 
 * @Class Name : PntPgCmpnController
 * @author ojh
 * @since 2018.10.04
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.04   ojh        최초생성
 * 
 */

@Controller
public class PntPgCmpnController {
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	PntPgCmpnServiceImpl pntPgCmpnService;

	/**
	 * jsp 호출 및 초기값 설정
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/cmpn/viewPntPgCmpn.do", method = RequestMethod.POST)
	public String viewPntPgCmpn(Locale locale, Model model) {	
		String method = "viewPntPgCmpn";
		log.debug(method, ">>> start");		
		
		log.debug(method, ">>>>>> end");
		return "/cmpn/pntPgCmpnView";
	}
	
	/**
	 * JQGrid 호출
	 * 
	 * @param page 현재 보고자 하는 페이지 (JQgrid 처리)
	 * @param rows 한 페이지에 보여주고자 설정된 개수 (JQgrid 처리)
	 * @param PG_CMPN_ID 	PG사 ID 	(검색 Input)
	 * @param PG_CMPN_NM 	PG사명 	(검색 Input)
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/cmpn/jgPntPgCmpn.do", method = RequestMethod.POST)
	public Map<String, Object> jgPntPgCmpn(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "jgPntPgCmpn";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		Map<String, Object> count = new HashMap<String, Object>();
		
		try{			
			//Request param
			int pageNum = Integer.parseInt(req.getParameter("page"));
			int limit = Integer.parseInt(req.getParameter("rows"));	
			
			//총 Row 수 계산 		
			count = pntPgCmpnService.viewPntPgCmpnTotal(params);
			int countRow = Integer.parseInt(count.get("cnt").toString());			
			
			if(countRow > 0){
				//HashMap에 페이징 정보 저장					
				Paging.setPageMap(params, map, pageNum, limit, countRow);	
				log.debug(method,map.toString());
				
				//검색 결과 데이터 map 에 추가
				List resultList = new ArrayList();
				resultList = pntPgCmpnService.viewPntPgCmpn(params);	
				map.put("rows", resultList );
			}			
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}

		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * JQGrid 호출 (엑셀용)
	 * 
	 * @param page 현재 보고자 하는 페이지 (JQgrid 처리)
	 * @param rows 한 페이지에 보여주고자 설정된 개수 (JQgrid 처리)
	 * @param PG_CMPN_ID 	PG사 ID 	(검색 Input)
	 * @param PG_CMPN_NM 	PG사명 	(검색 Input)
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/cmpn/jgPntPgCmpnExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgPntPgCmpnExcel(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "jgPntPgCmpnExcel";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		Map<String, Object> count = new HashMap<String, Object>();
		
		try{						
			log.debug(method,map.toString());			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			resultList = pntPgCmpnService.viewPntPgCmpn(params);	
			map.put("rows", resultList );			
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}

		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * PG사 수정 (데이터 가져오기)
	 * 
	 * @param PRVDR_ID 	제공처 ID 
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/cmpn/ajaxGetPntPgCmpn.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxGetPntPgCmpn(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "ajaxGetPntPgCmpn.do";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();
		Map<String, Object> pgCmpn = new HashMap<String, Object>();
		
		try{			
			//검색 결과 데이터 map 에 추가
			log.debug(method,params.toString());
			pgCmpn = pntPgCmpnService.getPntPgCmpn(params);
			map.put("rows", pgCmpn );	
			map.put("eCode", "0");
		} catch (Exception e) {
			map.put("eCode", "-1");
			log.error(method, "Exception : "+e.getMessage());
		}

		log.debug(method, ">>>>>> end");
		return map;
	}
	
	
	/**
	 * PG사 추가
	 * 
	 * @param PG_CMPN_NM 		Input PG사명
	 * @param PNT_APLY_STRT_DD  달력 사용가능일자 시작일
	 * @param PNT_APLY_END_DD 	달력 사용가능일자 종료일
	 * @param CHRGR_NM 			Input 담당자명
	 * @param CHRGR_TEL 		Input 담당자연락처
	 * @param ATHN_KEY	 		Input 인증키
	 * 
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/cmpn/ajaxCreatePntPgCmpn.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxCreatePntPgCmpn(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "ajaxCreatePntPgCmpn.do";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();		
		try{			
			//현재 로그인한 세션의 아이디 추가
			params.put("MDFY_USER_ID", SessionUtils.getUserId(req));
			log.debug(method,params.toString());
			
			int result = 0;
			result = pntPgCmpnService.createPntPgCmpn(params);
			if(result == 1){				
				map.put("eCode", "0");				
			} else{
				map.put("eCode", "-1");
			}
		} catch (Exception e) {
			map.put("eCode", "-1");
			log.error(method, "Exception : "+e.getMessage());
		}

		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * PG사 수정
	 * 
	 * @param PG_CMPN_NM 		Input PG사명
	 * @param PNT_APLY_STRT_DD  달력 사용가능일자 시작일
	 * @param PNT_APLY_END_DD 	달력 사용가능일자 종료일
	 * @param CHRGR_NM 			Input 담당자명
	 * @param CHRGR_TEL 		Input 담당자연락처
	 * @param ATHN_KEY	 		Input 인증키
	 * 
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/cmpn/ajaxModifyPntPgCmpn.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxModifyPntPgCmpn(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "ajaxModifyPntPgCmpn.do";		
		log.debug(method, ">>> start");	
		HashMap map = new HashMap();		
		try{			
			//현재 로그인한 세션의 아이디 추가
			params.put("MDFY_USER_ID", SessionUtils.getUserId(req));
			log.debug(method,params.toString());
			
			int result = 0;
			result = pntPgCmpnService.modifyPntPgCmpn(params);
			if(result == 1){				
				map.put("eCode", "0");				
			} else{
				map.put("eCode", "-1");
			}
		} catch (Exception e) {
			map.put("eCode", "-1");
			log.error(method, "Exception : "+e.getMessage());
		}

		log.debug(method, ">>>>>> end");
		return map;
	}
	
}
