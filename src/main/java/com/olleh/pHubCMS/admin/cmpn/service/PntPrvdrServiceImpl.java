package com.olleh.pHubCMS.admin.cmpn.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectUpdateSemanticsDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.olleh.pHubCMS.admin.cmpn.dao.PntPrvdrDao;
import com.olleh.pHubCMS.admin.cmpn.dao.PntUseCmpnDao;
import com.olleh.pHubCMS.common.exception.BizException;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 제공처 관리 Service
 * 
 * @Class Name : PntPrvdrCmpnService
 * @author ojh
 * @since 2018.08.27
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.27   ojh        최초생성
 * 
 */
@Service
@SuppressWarnings({"rawtypes", "unchecked"})
public class PntPrvdrServiceImpl implements PntPrvdrService{	
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	PntPrvdrDao pntPrvdrDao;
	
	@Autowired
	PntUseCmpnDao pntUseCmpnDao;
	
	@Override
	public List getAllPrvdrCmpn(){
		return pntPrvdrDao.getAllPrvdrCmpn();		
	}
	@Override
	public List viewPntPrvdr(Map<String, Object> params){
		return maskingName(pntPrvdrDao.viewPntPrvdr(params));
	}
	@Override
	public Map<String, Object> viewPntPrvdrTotal(Map<String, Object> params){
		return pntPrvdrDao.viewPntPrvdrTotal(params);
	}
	@Override
	public List viewPntPrvdrExcel(Map<String, Object> params){
		return maskingName(pntPrvdrDao.viewPntPrvdrExcel(params));		
	}
	@Override
	public Map<String, Object> getPntPrvdr(Map<String, Object> params) {
		return maskingName(pntPrvdrDao.getPntPrvdr(params));
	}
	@Override
	public List viewPntPrvdrCmsnHist(Map<String, Object> params){
		return maskingName(pntPrvdrDao.viewPntPrvdrCmsnHist(params));
	}
	@Override
	public List viewPntPrvdrExchHist(Map<String, Object> params){
		return maskingName(pntPrvdrDao.viewPntPrvdrExchHist(params));
	}	
	@Override
	@Transactional(rollbackFor={BizException.class, IncorrectUpdateSemanticsDataAccessException.class})
	public int createPntPrvdr(Map<String, Object> params) throws BizException, IncorrectUpdateSemanticsDataAccessException {
		int result = 0;
		try{
			// 1. 포인트제공처 등록
			result = pntPrvdrDao.createPntPrvdr(params);
			if (result < 1) {
				throw new IncorrectUpdateSemanticsDataAccessException("포인트 제공처 등록 중 처리되지 않았습니다.");
			}
			// 2-1. 제공처 수수료율 이력 등록
			result = pntPrvdrDao.createPrvdrCmsnHist2(params);
			if (result < 1) {
				throw new IncorrectUpdateSemanticsDataAccessException("제공처 수수료율 이력 등록 중 처리되지 않았습니다.");
			}
			// 2-2. 제공처 전환율 이력 등록
			result = pntPrvdrDao.createPrvdrExchHist2(params);
			if (result < 1) {
				throw new IncorrectUpdateSemanticsDataAccessException("제공처 전환율 이력 등록 중 처리되지 않았습니다.");
			}
			// 3. 기 등록 된 사용처에 추가된 제공처 매핑 정보 저장
			// 3-1. 사용처 정보 가져오기
			List<Map> pntUseCmpnList = pntUseCmpnDao.getAllPntUseCmpn();
			// 3-2. 포인트 사용처별 제공처 등록
			for (Map<String, Object> pntUseCmpn : pntUseCmpnList) {
				params.put("CPRT_CMPN_ID", pntUseCmpn.get("cprt_cmpn_id"));
				params.put("PG_CMPN_ID", pntUseCmpn.get("pg_cmpn_id"));
				result = pntPrvdrDao.createPgCprtPrvdrToPrvdr(params);
				if (result < 1) {
					throw new IncorrectUpdateSemanticsDataAccessException("포인트 사용처별 추가 등록 중 처리되지 않았습니다");
				}
			}
		} catch (IncorrectUpdateSemanticsDataAccessException e) {
			throw e;
		} catch (Exception e) {
			throw new BizException(e);
		}
		
		return result;
	}
	@Override
	@Transactional(rollbackFor={BizException.class, IncorrectUpdateSemanticsDataAccessException.class})
	public int modifyPntPrvdr(Map<String, Object> params) throws BizException, IncorrectUpdateSemanticsDataAccessException {
		int result = 0;
		try {
			// 1. 포인트 제공처 수정
			result = pntPrvdrDao.modifyPntPrvdr(params);
			if (result < 1) {
				throw new IncorrectUpdateSemanticsDataAccessException("포인트 제공처 수정 중 처리되지 않았습니다.");
			}
			// 2. 포인트 사용처별 제공처 이미지 링크 수정
			result = pntPrvdrDao.modifyPgCprtPrvdrByImagLink(params);
			// 포인트 사용처별 제공처가 없을수도 있기 때문에 체크 로직을 제거 한다. - 2019.06.11.lys
//			if (result < 1) {
//				throw new IncorrectUpdateSemanticsDataAccessException("포인트 사용처별 제공처 이미지 링크 수정 중 처리되지 않았습니다.");
//			}
			// 3. 수수료율 적용일자 수정
			if (params.get("MDFY_CMSN_DATE").equals("1")) {
				//수수료율적용일자 변경시 - 이전 이력 변경 및 신규 이력 추가
				result = createPrvdrCmsnHist(params);
				if (result < 1) {
					throw new IncorrectUpdateSemanticsDataAccessException("수수료율적용일자 변경 시 이전 이력 변경 및 신규 이력 추가 처리되지 않았습니다.");
				}
			} else {
				//수수료율적용일자 변경 없을시 - 기존 이력 수수료율만 변경
				result = modifyPrvdrCmsnHist(params);
				if (result < 1) {
					throw new IncorrectUpdateSemanticsDataAccessException("수수료율적용일자 변경 없을 시 기존 이력 수수료율만 변경 처리되지 않았습니다.");
				}
			}
			// 4. 전환율 적용일자 수정
			if (params.get("MDFY_EXCH_DATE").equals("1")) {
				result = createPrvdrExchHist(params);
				if (result < 1) {
					throw new IncorrectUpdateSemanticsDataAccessException("전환율적용일자 변경 시 이전 이력 변경 및 신규 이력 추가 처리되지 않았습니다.");
				}
			} else {
				result = modifyPrvdrExchHist(params);
				if (result < 1) {
					throw new IncorrectUpdateSemanticsDataAccessException("전환율적용일자 변경 없을 시 기존 이력 수수료율만 변경 처리되지 않았습니다.");
				}
			}
		} catch (IncorrectUpdateSemanticsDataAccessException e) {
			throw e;
		} catch (Exception e) {
			throw new BizException(e);
		}
		return result;
	}	
	@Override
	public int modifyPrvdrCmsnHist(Map<String, Object> params){
		return pntPrvdrDao.modifyPrvdrCmsnHist(params);
	}
	@Override
	public int createPrvdrCmsnHist(Map<String, Object> params) {
		int result = 0;
		result = pntPrvdrDao.createPrvdrCmsnHist1(params);
		result = pntPrvdrDao.createPrvdrCmsnHist2(params);
		return result;
	}
	@Override
	public int modifyPrvdrExchHist(Map<String, Object> params){
		return pntPrvdrDao.modifyPrvdrExchHist(params);
	}
	@Transactional
	@Override
	public int createPrvdrExchHist(Map<String, Object> params){
		int result = 0;
		result = pntPrvdrDao.createPrvdrExchHist1(params);
		result = pntPrvdrDao.createPrvdrExchHist2(params);
		return result;
	}
	/**
	 * 이름 마스킹 처리
	 * 
	 * @param params
	 * @return
	 */
	private List maskingName(List<Map> params) {
		int index = 0;
		if (params == null) {
			return params;
		}
		for (Map map : params) {
			if (map.get("rgst_user_nm") != null) {
				map.put("rgst_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("rgst_user_nm"))));
			}
			if (map.get("mdfy_user_nm") != null) {
				map.put("mdfy_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("mdfy_user_nm"))));
			}
			params.set(index, map);
			index++;
		}
		return params;
	}
	/**
	 * 이름 마스킹 처리
	 * 
	 * @param params
	 * @return
	 */
	private Map maskingName(Map map) {
		if (map != null) {
			if (map.get("rgst_user_nm") != null) {
				map.put("rgst_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("rgst_user_nm"))));
			}
			if (map.get("mdfy_user_nm") != null) {
				map.put("mdfy_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("mdfy_user_nm"))));
			}
		}
		return map;
	}
}
