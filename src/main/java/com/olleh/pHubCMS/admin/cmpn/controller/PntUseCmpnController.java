package com.olleh.pHubCMS.admin.cmpn.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.cmpn.service.PntPgCmpnService;
import com.olleh.pHubCMS.admin.cmpn.service.PntPrvdrService;
import com.olleh.pHubCMS.admin.cmpn.service.PntUseCmpnService;
import com.olleh.pHubCMS.common.components.CodeManage;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.utils.Paging;
import com.olleh.pHubCMS.common.utils.SessionUtils;

/**
 * 포인트 사용처 관리 Controller
 * 
 * @Class Name : PntUseCmpnController
 * @author ojh
 * @since 2018.08.14
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.14   ojh        최초생성
 * 
 */
@Controller
public class PntUseCmpnController {
	private Logger log = new Logger(this.getClass());
	@Autowired
	CodeManage codeManage;
	
	@Autowired
	PntUseCmpnService pntUseCmpnService;
	
	@Autowired
	PntPgCmpnService pntPgCmpnService;
	
	@Autowired
	PntPrvdrService pntPrvdrService;
	
	/**
	 * jsp 호출 및 초기값 설정
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/cmpn/viewPntUseCmpn.do", method = RequestMethod.POST)
	public String viewPntUseCmpn(Locale locale, Model model) {	
		String method = "viewPntUseCmpn";
		log.debug(method, ">>> start");	
		try{
			//PG사 가져오기
			List pgList = new ArrayList();		
			pgList = pntPgCmpnService.getAllPgCmpn();
			model.addAttribute("PG_CMPN", pgList);
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");
		return "/cmpn/pntUseCmpnView";
	}
	
	/**
	 * JQGrid 호출
	 * 
	 * @param page 현재 보고자 하는 페이지 (JQgrid 처리)
	 * @param rows 한 페이지에 보여주고자 설정된 개수 (JQgrid 처리)
	 * @param CPRT_CMPN_ID 사용처 ID (검색 Input)
	 * @param CPRT_CMPN_NM 사용처명 (검색 Input)
	 * @param BIZ_NO 사용처사업자번호 (검색 Input)
	 * @return void response 응답
	 */
	@ResponseBody
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/cmpn/jgPntUseCmpn.do", method = RequestMethod.POST)
	public Map<String, Object> jgPntUseCmpn(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "jgPntUseCmpn";		
		log.debug(method, ">>> start");	
		HashMap<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> count = new HashMap<String, Object>();
		
		try{
			//Request param
			int pageNum = Integer.parseInt(req.getParameter("page"));
			int limit = Integer.parseInt(req.getParameter("rows"));			
			log.debug(method, params.toString());
			
			//총 Row 수 계산 		
			count = pntUseCmpnService.viewPntUseCmpnTotal(params); 		
			int countRow = Integer.parseInt(count.get("cnt").toString());			
			
			if(countRow > 0){
				//HashMap에 페이징 정보 저장					
				Paging.setPageMap(params, map, pageNum, limit, countRow);	
				log.debug(method,map.toString());
				
				//검색 결과 데이터 map 에 추가
				List resultList = new ArrayList();
				log.debug(method,params.toString());
				resultList = pntUseCmpnService.viewPntUseCmpn(params);					
				map.put("rows", resultList );
			}
			
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}

		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * JQGrid 호출 (엑셀용 데이터
	 * 
	 * @param CPRT_CMPN_ID 사용처 ID (검색 Input)
	 * @param CPRT_CMPN_NM 사용처명 (검색 Input)
	 * @param BIZ_NO 사용처사업자번호 (검색 Input)
	 * @return void response 응답
	 */
	@ResponseBody
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/cmpn/jgPntUseCmpnExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgPntUseCmpnExcel(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "jgPntUseCmpnExcel";
		log.debug(method, ">>> start");	
		HashMap<String, Object> map = new HashMap<String, Object>();
		try{
			//Request param	
			log.debug(method, params.toString());
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			resultList = pntUseCmpnService.viewPntUseCmpnExcel(params);					
			map.put("rows", resultList);		
		} catch (Exception e) {
			log.error(method, "Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 포인트 사용처 관리 : 상세 조회 (수정)
	 * 
	 * @param CPRT_CMPN_ID 사용처ID
	 * 
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/cmpn/ajaxGetPntUseCmpn.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxGetPntUseCmpn(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "ajaxGetPntUseCmpn";
		log.debug(method, ">>> start");	
		HashMap<String, Object> map = new HashMap<String, Object>();
		try {
			//Request param				
			log.debug(method, params.toString());
			
			Map<String, Object> pntUseCmpn = pntUseCmpnService.getPntUseCmpn(params);
			map.put("rows", pntUseCmpn);
			map.put("eCode", "0");
		} catch (Exception e) {
			map.put("eCode", "-1");
			log.error(method, "Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * 포인트 사용처별 제공처 조회
	 * 
	 * @param params
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/cmpn/ajaxGetPgCprtPrvdr.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxGetPgCprtPrvdr(@RequestParam Map<String, Object> params) throws IOException {
		String method = "ajaxGetPgCprtPrvdr";
		log.debug(method, ">>> start");	
		HashMap<String, Object> map = new HashMap<String, Object>();	
		try {
			//Request param				
			log.debug(method, params.toString());
			
			List resultList = pntUseCmpnService.getPgCprtPrvdr(params);
			
			map.put("rows", resultList);
			map.put("eCode", "0");
		} catch (Exception e) {
			map.put("eCode", "-1");
			log.error(method, "Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * 포인트 사용처 관리 : 추가
	 * 
	 * @param CPRT_CMPN_NM 사용처명 (저장 Input)
	 * @param BIZ_NO 사업자번호 (저장 Input)
	 * @param RCHR_PAY_IND 결제구분 (선택 Radio)
	 * @param PG_SEND_PO_ID PG제공사용처ID (저장 Input)
	 * 
	 * @return void response 응답
	 */
	@ResponseBody
	@RequestMapping(value = "/cmpn/ajaxCreatePntUseCmpn.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxCreatePntUseCmpn(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {	
		String method = "ajaxCreatePntUseCmpn";
		log.debug(method, ">>> start");			
		HashMap<String, Object> map = new HashMap<String, Object>();
		try{
			int result;
			//Request param
//			params.put("prvdrList", prvdrList);
			log.debug(method, params.toString());	
			
			//사업자번호 중복 검사 : 사업자번호 중복등록 체크는 하지 않는다. null값 등록 가능하게 - 2019.03.22. lys (사업팀 임상현C 요청)
			//Map<String, Object> cmpn = pntUseCmpnService.getBizNo(params);
			//if(cmpn == null){
				map.put("eStat", "0");
				//현재 로그인한 세션의 아이디 추가
				params.put("RGST_USER_ID", SessionUtils.getUserId(req));
				result = pntUseCmpnService.createPntUseCmpn(params);	
				if(result > 0){
					map.put("eCode", "0");
				} else{
					map.put("eCode", "-1");
				}											
			//} else{				
				//map.put("eCode", "0");
				//map.put("eStat", "1");
			//}			
		} catch (Exception e) {
			map.put("eCode", "-1");
			log.error(method, "Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * 포인트 사용처 관리 수정 저장
	 * 
	 * @param CPRT_CMPN_ID   사용처ID     
	 * @param CPRT_CMPN_NM   사용처명          (저장 Input)
	 * @param PG_CMPN_ID     PG사 ID     (선택 Select)
	 * @param PG_SEND_PO_ID  PG제공사용처 ID(저장 Input)
	 * @param BIZ_NO         사업자번호        (저장 Input)
	 * 
	 * @return void response 응답
	 */
	@ResponseBody
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/cmpn/ajaxModifyPntUseCmpn.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxModifyPntUseCmpn(@RequestParam Map<String, Object> params, HttpServletRequest req, HttpServletResponse res) throws IOException {			
		String method = "ajaxModifyPntUseCmpn";
		log.debug(method, ">>> start");			
		HashMap<String, Object> map = new HashMap<String, Object>();
		//현재 로그인한 세션의 아이디 추가
		params.put("MDFY_USER_ID", SessionUtils.getUserId(req));
		//Request param		
		log.debug(method, params.toString());			
		try {			
			int result = pntUseCmpnService.modifyUseCmpn(params);
			if(result == 1){
				map.put("eCode", "0");
			} else{
				map.put("eCode", "-1");
			}
		} catch (Exception e) {
			map.put("eCode", "-1");
			log.error(method, "Exception : "+e.getMessage());
		}
		log.debug(method, ">>>>>> end");	
		return map;
	}
}
