package com.olleh.pHubCMS.admin.cmpn.model;


/**
 * 사용처 지원 포인트 제공처 목록  VO
 * 
 * @Class Name : CprtPrvdrVo
 * @author ojh
 * @since 2018.11.27
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.11.27   ojh        최초생성
 * 
 */

public class CprtPrvdrVo {
	
	private String prvdr_id;	// 제공처 ID
	private String ci_path;		// CI 경로
	private String order;		// 정렬순서
	
	public String getPrvdr_id() {
		return prvdr_id;
	}
	public void setPrvdr_id(String prvdr_id) {
		this.prvdr_id = prvdr_id;
	}
	public String getCi_path() {
		return ci_path;
	}
	public void setCi_path(String ci_path) {
		this.ci_path = ci_path;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	

}
