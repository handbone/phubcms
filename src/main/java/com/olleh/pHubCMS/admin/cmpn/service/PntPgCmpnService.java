package com.olleh.pHubCMS.admin.cmpn.service;

import java.util.List;
import java.util.Map;

public interface PntPgCmpnService {
	
	public List getAllPgCmpn();
	
	public List viewPntPgCmpn(Map<String, Object> params);
	public Map<String, Object> viewPntPgCmpnTotal(Map<String, Object> params);
	public List viewPntPgCmpnExcel(Map<String, Object> params);
	public Map<String, Object> getPntPgCmpn(Map<String, Object> params);	
	public int createPntPgCmpn(Map<String, Object> params);	
	public int modifyPntPgCmpn(Map<String, Object> params);
}
