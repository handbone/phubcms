package com.olleh.pHubCMS.admin.cmpn.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;
import com.olleh.pHubCMS.common.log.Logger;

/**
 * Pg사 관리 Dao
 * @Class Name : PntPgCmpnDao
 * @author : ojh
 * @since : 2018.08.17
 * @version : 1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *      수정일                 수정자                수정내용
 *  ------------  -----------    -------------
 *  2018. 8. 17.      ojh          최초 생성
 * 
 */
@Repository
public class PntPgCmpnDao extends AbstractDAO{
	
	public List getAllPgCmpn() {
		return selectList("mybatis.cmpn.allPgCmpn");
	}
	
	public List viewPntPgCmpn(Map<String, Object> params) {
		return selectList("mybatis.cmpn.pntPgCmpn", params);
	}	
	public Map<String, Object> viewPntPgCmpnTotal(Map<String, Object> params) {
		return selectOne("mybatis.cmpn.pntPgCmpnTotal", params);
	}	
	public List viewPntPgCmpnExcel(Map<String, Object> params) {
		return selectList("mybatis.cmpn.pntPgCmpnExcel", params);
	}	
	public Map<String, Object> getPntPgCmpn(Map<String, Object> params) {
		return selectOne("mybatis.cmpn.getPntPgCmpn", params);
	}	
	
	public int createPntPgCmpn(Map<String, Object> params) {
		return insert("mybatis.cmpn.createPntPgCmpn", params);
	}		
	public int modifyPntPgCmpn(Map<String, Object> params) {
		return update("mybatis.cmpn.modifyPntPgCmpn", params);
	}	
	
	
	
	
	

}
