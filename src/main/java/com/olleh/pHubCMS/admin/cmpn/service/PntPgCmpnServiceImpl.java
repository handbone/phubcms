package com.olleh.pHubCMS.admin.cmpn.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.cmpn.dao.PntPgCmpnDao;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * PG사 관리 Service
 * 
 * @Class Name : PntPgCmpnService
 * @author ojh
 * @since 2018.08.17
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.17   ojh        최초생성
 * 
 */
@Service
@SuppressWarnings({"rawtypes", "unchecked"})
public class PntPgCmpnServiceImpl implements PntPgCmpnService{
	
	@Autowired
	PntPgCmpnDao pntPgCmpnDao;
	
	@Override
	public List getAllPgCmpn(){
		return pntPgCmpnDao.getAllPgCmpn();
	};
	
	@Override
	public List viewPntPgCmpn(Map<String, Object> params){
		return maskingColumn(pntPgCmpnDao.viewPntPgCmpn(params));
	}
	@Override
	public Map<String, Object> viewPntPgCmpnTotal(Map<String, Object> params){
		return pntPgCmpnDao.viewPntPgCmpnTotal(params);
	}
	@Override
	public List viewPntPgCmpnExcel(Map<String, Object> params){
		return maskingColumn(pntPgCmpnDao.viewPntPgCmpnExcel(params));
	}
	@Override
	public Map<String, Object> getPntPgCmpn(Map<String, Object> params){
		return maskingName(pntPgCmpnDao.getPntPgCmpn(params));
	}
	@Override
	public int createPntPgCmpn(Map<String, Object> params){
		return pntPgCmpnDao.createPntPgCmpn(params);
	}
	@Override
	public int modifyPntPgCmpn(Map<String, Object> params){
		return pntPgCmpnDao.modifyPntPgCmpn(params);
	}
	/**
	 * 이름 마스킹 처리
	 * 
	 * @param params
	 * @return
	 */
	private List maskingColumn(List<Map> params) {
		int index = 0;
		if (params == null) {
			return params;
		}
		for (Map map : params) {
			if (map.get("rgst_user_nm") != null) {
				map.put("rgst_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("rgst_user_nm"))));
			}
			if (map.get("mdfy_user_nm") != null) {
				map.put("mdfy_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("mdfy_user_nm"))));
			}
			if (map.get("chrgr_nm") != null) {
				map.put("chrgr_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("chrgr_nm"))));
			}
			if (map.get("chrgr_tel") != null) {
				String chrgr_tel = StringUtil.nvl(map.get("chrgr_tel")).replaceAll("-", "");
				map.put("chrgr_tel", StringUtil.getMaskingPhone(chrgr_tel));
			}
			params.set(index, map); 
			index++;
		}
		return params;
	}
	/**
	 * 이름 마스킹 처리
	 * 
	 * @param params
	 * @return
	 */
	private Map maskingName(Map map) {
		if (map != null) {
			if (map.get("rgst_user_nm") != null) {
				map.put("rgst_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("rgst_user_nm"))));
			}
			if (map.get("mdfy_user_nm") != null) {
				map.put("mdfy_user_nm", StringUtil.getMaskingName(StringUtil.nvl(map.get("mdfy_user_nm"))));
			}
		}
		return map;
	}
	
}
