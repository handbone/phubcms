package com.olleh.pHubCMS.admin.cmpn.service;

import java.util.List;
import java.util.Map;

@SuppressWarnings("rawtypes")
public interface PntUseCmpnService {
	
	public List getAllPntUseCmpn();
	
	public List viewPntUseCmpn(Map<String, Object> params);	
	
	public Map<String, Object> viewPntUseCmpnTotal(Map<String, Object> params);
	
	public List viewPntUseCmpnExcel(Map<String, Object> params);
	
	public int createPntUseCmpn(Map<String, Object> params);
	
	public Map<String, Object> getPntUseCmpn(Map<String, Object> params);
	
	public List getPgCprtPrvdr(Map<String, Object> params);
	
	public int modifyUseCmpn(Map<String, Object> params);
	
	public Map<String, Object> getBizNo(Map<String, Object> params);
}
