package com.olleh.pHubCMS.admin.cmpn.model;

/**
 * PG사 관리 VO
 * 
 * @Class Name : PntPgCmpnVo
 * @author ojh
 * @since 2018.08.17
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.17   ojh        최초생성
 * 
 */

public class PntPgCmpnVo {
	
	private String pg_cmpn_id;			// PG사ID
	private String pg_cmpn_nm;			// PG사 이름
	private String pnt_aply_strt_dd;	// 포인트적용시작일
	private String pnt_aply_ene_dd;		// 포인트적용종료일
	private String chrgr_nm;			// 담당자명
	private String chrgr_tel;			// 담당자연락처
	private String athn_key;			// 인증키
	private String rgst_dt;				// 등록일시
	private String rgst_user_id;		// 등록자ID
	private String mdfy_dt;				// 수정일시
	private String mdfy_user_id;		// 수정자ID
	
	public String getPg_cmpn_id() {
		return pg_cmpn_id;
	}
	public void setPg_cmpn_id(String pg_cmpn_id) {
		this.pg_cmpn_id = pg_cmpn_id;
	}
	public String getPg_cmpn_nm() {
		return pg_cmpn_nm;
	}
	public void setPg_cmpn_nm(String pg_cmpn_nm) {
		this.pg_cmpn_nm = pg_cmpn_nm;
	}
	public String getPnt_aply_strt_dd() {
		return pnt_aply_strt_dd;
	}
	public void setPnt_aply_strt_dd(String pnt_aply_strt_dd) {
		this.pnt_aply_strt_dd = pnt_aply_strt_dd;
	}
	public String getPnt_aply_ene_dd() {
		return pnt_aply_ene_dd;
	}
	public void setPnt_aply_ene_dd(String pnt_aply_ene_dd) {
		this.pnt_aply_ene_dd = pnt_aply_ene_dd;
	}
	public String getChrgr_nm() {
		return chrgr_nm;
	}
	public void setChrgr_nm(String chrgr_nm) {
		this.chrgr_nm = chrgr_nm;
	}
	public String getChrgr_tel() {
		return chrgr_tel;
	}
	public void setChrgr_tel(String chrgr_tel) {
		this.chrgr_tel = chrgr_tel;
	}
	public String getAthn_key() {
		return athn_key;
	}
	public void setAthn_key(String athn_key) {
		this.athn_key = athn_key;
	}
	public String getRgst_dt() {
		return rgst_dt;
	}
	public void setRgst_dt(String rgst_dt) {
		this.rgst_dt = rgst_dt;
	}
	public String getRgst_user_id() {
		return rgst_user_id;
	}
	public void setRgst_user_id(String rgst_user_id) {
		this.rgst_user_id = rgst_user_id;
	}
	public String getMdfy_dt() {
		return mdfy_dt;
	}
	public void setMdfy_dt(String mdfy_dt) {
		this.mdfy_dt = mdfy_dt;
	}
	public String getmdfy_user_id() {
		return mdfy_user_id;
	}
	public void setmdfy_user_id(String mdfy_user_id) {
		this.mdfy_user_id = mdfy_user_id;
	}
	
}
