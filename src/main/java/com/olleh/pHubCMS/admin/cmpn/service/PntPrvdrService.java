package com.olleh.pHubCMS.admin.cmpn.service;

import java.util.List;
import java.util.Map;

import org.springframework.dao.IncorrectUpdateSemanticsDataAccessException;

import com.olleh.pHubCMS.common.exception.BizException;

@SuppressWarnings("rawtypes")
public interface PntPrvdrService {
	public List getAllPrvdrCmpn();
	
	public List viewPntPrvdr(Map<String, Object> params);
	public Map<String, Object> viewPntPrvdrTotal(Map<String, Object> params);
	public List viewPntPrvdrExcel(Map<String, Object> params);
	public Map<String, Object> getPntPrvdr(Map<String, Object> params);	
	
	public int createPntPrvdr(Map<String, Object> params) throws BizException, IncorrectUpdateSemanticsDataAccessException;
	public int modifyPntPrvdr(Map<String, Object> params) throws BizException, IncorrectUpdateSemanticsDataAccessException;
	
	public int modifyPrvdrCmsnHist(Map<String, Object> params);
	public int createPrvdrCmsnHist(Map<String, Object> params);
	public int modifyPrvdrExchHist(Map<String, Object> params);
	public int createPrvdrExchHist(Map<String, Object> params);
	
	public List viewPntPrvdrCmsnHist(Map<String, Object> params);
	public List viewPntPrvdrExchHist(Map<String, Object> params);
	
}
