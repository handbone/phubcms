package com.olleh.pHubCMS.admin.batch.service;

import java.util.List;
import java.util.Map;

public interface BatchHistService {

	public Map<String, Object> viewBatchHistTotal(Map<String, Object> params);
	
	@SuppressWarnings("rawtypes")
	public List viewBatchHist(Map<String, Object> params);
	
	@SuppressWarnings("rawtypes")
	public List viewBatchHistExcel(Map<String, Object> params);
	
	@SuppressWarnings("rawtypes")
	public List viewBatchHistLog(Map<String, Object> params);
	
	@SuppressWarnings("rawtypes")
	public List viewBatchHistDtlLog(Map<String, Object> params);
	
	@SuppressWarnings("rawtypes")
	public List viewBatchHistDtlLogExcel(Map<String, Object> params);

	@SuppressWarnings("rawtypes")
	public Map<String, Object> viewBatchHistDtlLogTotal(Map<String, Object> params);
}
