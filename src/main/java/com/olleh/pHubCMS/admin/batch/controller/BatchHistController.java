package com.olleh.pHubCMS.admin.batch.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.olleh.pHubCMS.admin.batch.service.BatchHistService;
import com.olleh.pHubCMS.common.components.CodeManage;
import com.olleh.pHubCMS.common.components.Constant;
import com.olleh.pHubCMS.common.components.MessageManage;
import com.olleh.pHubCMS.common.log.Logger;
import com.olleh.pHubCMS.common.model.CmnCdVO;
import com.olleh.pHubCMS.common.scheduler.job.ClipToPHubJob;
import com.olleh.pHubCMS.common.scheduler.job.PHubStatJob;
import com.olleh.pHubCMS.common.scheduler.job.PHubToPGJob;
import com.olleh.pHubCMS.common.utils.Paging;
import com.olleh.pHubCMS.common.utils.SessionUtils;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 배치작업이력 Controller
 * 
 * @Class Name : BatchHistController
 * @author bmg
 * @since 2018.09.28
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.28   bmg        최초생성
 * 
 */
@Controller
public class BatchHistController {
	private Logger log = new Logger(this.getClass());
	
	@Autowired
	CodeManage codeManage;

	@Autowired
	BatchHistService batchHistService;
	
	@Autowired
	ClipToPHubJob clipToPHubJob;
	
	@Autowired
	PHubToPGJob pHubToPGJob;
	
	@Autowired
	PHubStatJob pHubStatJob;
	
	@Autowired
	MessageManage messageManage;
	
	/**
	 * jsp 호출 및 초기값 설정
	 * 
	 * @param 
	 * @return jsp 매핑 String
	 */
	@RequestMapping(value = "/btch/viewBatchHist.do", method = RequestMethod.POST)
	public String viewSysPrmt(Locale locale, Model model) {	
		String method = "viewBatchHist";
		log.debug(method, ">>> start");	
		
		List<CmnCdVO> batchJobCd = new ArrayList<CmnCdVO>();
		batchJobCd = codeManage.getCodeListY("BTCH_JOB_CD");
		model.addAttribute("BTCH_JOB_CD", batchJobCd.toArray());
		
		log.debug(method, ">>>>>> end");	
		return "/batch/batchHistView";
	}
	
	/**
	 * 배치작업이력 JQGrid 호출
	 * 
	 * @param params
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/btch/jgBatchHist.do", method = RequestMethod.POST)
	public Map<String, Object> jgBatchHist(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgBatchHist";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		int pageNum = Integer.parseInt(request.getParameter("page"));
		int limit = Integer.parseInt(request.getParameter("rows"));	
		log.debug(method,params.toString());
		
		count = batchHistService.viewBatchHistTotal(params);
		int countRow = Integer.parseInt(count.get("cnt").toString());
		
		HashMap map = new HashMap();
		if (countRow > 0) {
			//HashMap에 페이징 정보 저장					
			Paging.setPageMap(params, map, pageNum, limit, countRow);	
			log.debug(method,map.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			log.debug(method,params.toString());
			resultList = batchHistService.viewBatchHist(params);
			
			map.put("rows", resultList);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 배치작업이력 JQGrid 호출 엑셀
	 * 
	 * @param params
	 * @param req
	 * @param res
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/btch/jgBatchHistExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgBatchHistExcel(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgBatchHistExcel";
		log.debug(method, ">>> start");			
		HashMap map = new HashMap();
		//Request param
		log.debug(method,params.toString());
		
		//검색 결과 데이터 map 에 추가
		List resultList = new ArrayList();
		resultList = batchHistService.viewBatchHistExcel(params);
		map.put("rows", resultList);
		
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * 배치작업이력 상세 로그 조회
	 * 
	 * @param params
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/btch/ajaxGetBatchHistDtl.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxGetBatchHistDtl(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "ajaxGetBatchHistDtl";
		log.debug(method, ">>> start");	
		//Request param				
		log.debug(method,params.toString());
		
		HashMap map = new HashMap();	
		List loglist = new ArrayList();
		try {
			loglist = batchHistService.viewBatchHistLog(params);
			map.put("rows", loglist);
			map.put(Constant.RET_CODE, Constant.SUCCESS_CODE);
		} catch (Exception e) {
			map.put(Constant.RET_CODE, Constant.FAIL_CODE);
			log.printStackTracePH(method, e);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	
	/**
	 * 배치작업이력 상세 로그 조회(DTL - depth 2)
	 * 
	 * @param params
	 * @param request
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/btch/ajaxGetBatchHistDtlLog.do", method = RequestMethod.POST)
	public Map<String, Object> ajaxGetBatchHistDtlLog(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {	
		
		String method = "ajaxGetBatchHistDtlLog";
		Map<String, Object> count = new HashMap<String, Object>();
		log.debug(method, ">>> start");	
		//Request param
		int pageNum = Integer.parseInt(request.getParameter("page"));
		int limit = 10;	
		log.debug(method,params.toString());
		
		count = batchHistService.viewBatchHistDtlLogTotal(params);
		int countRow = Integer.parseInt(count.get("cnt").toString());
		
		HashMap map = new HashMap();
		if (countRow > 0) {
			//HashMap에 페이징 정보 저장					
			Paging.setPageMap(params, map, pageNum, limit, countRow);	
			log.debug(method,map.toString());
			
			//검색 결과 데이터 map 에 추가
			List resultList = new ArrayList();
			log.debug(method,params.toString());
			resultList = batchHistService.viewBatchHistDtlLog(params);
			map.put("rows", resultList);
		}
		log.debug(method, ">>>>>> end");
		return map;
	}
	
	/**
	 * 배치작업이력 상세로그내역 JQGrid 호출 엑셀
	 * 
	 * @param params
	 * @param req
	 * @param res
	 * @return
	 * @throws IOException
	 */
	@ResponseBody
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/btch/jgBatchHistDtlExcel.do", method = RequestMethod.POST)
	public Map<String, Object> jgBatchHistDtlExcel(@RequestParam Map<String, Object> params, HttpServletRequest request) throws IOException {
		String method = "jgBatchHistDtlExcel";
		log.debug(method, ">>> start");			
		HashMap map = new HashMap();
		//Request param
		log.debug(method,params.toString());
		
		//검색 결과 데이터 map 에 추가
		List resultList = new ArrayList();
		resultList = batchHistService.viewBatchHistDtlLogExcel(params);
		map.put("rows", resultList);
		
		log.debug(method, ">>>>>> end");	
		return map;
	}
	
	/**
	 * <pre> 배치 수동 실행 </pre>
	 * 
	 * @param Map<String,Object> params
	 * @param HttpServletRequest request
	 * @return String
	 * @throws Exception
	 * @see <pre>
	 *      1. 배치 수동 실행 
	 *      </pre>
	 */
	@RequestMapping(value = "/btch/batchManProc.do", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> batchManProc(@RequestParam Map<String, Object> params, HttpServletRequest request) throws Exception {
		//메소드명, 로그내용
		String methodName = "batchManProc";
		log.debug(methodName, "Start!");
		
		// Declare
		// 결과셋팅 : 성공
		Map<String, Object> resMap = new HashMap<String, Object>();
		resMap.put(Constant.SUCCESS_YN, Constant.SUCCESS);
		resMap.put(Constant.RET_CODE,   Constant.SUCCESS_CODE);
		resMap.put(Constant.RET_MSG,    Constant.SUCCESS_MSG);
		
		// get
		String userId = SessionUtils.getUserId(request);
		String btchId = StringUtil.nvl(params.get("btchId"));
		
		//set
		params.put("userId", userId);
		
		// prams={cmprDd, userId}
		if( "ClipToPHubJob".equals(btchId) ) {
			// 클립포인트 일대사 배치
			clipToPHubJob.cmprProc(params);
			
		} else if( "PHubToPGJob".equals(btchId) ) {
			// 세틀뱅크 일대사 배치
			pHubToPGJob.cmprProc(params);
			
		} else if( "PHubStatJob".equals(btchId) ) {
			// 통계데이터 생성 배치
			pHubStatJob.statProc(params);
			
		} else {
			// 정의되지 않은 유형 : AD_ERROR_158	F158	AD	Undefined Type.	Y	ERROR	정의되지 않은 유형 입니다.
			log.debug(methodName, "정의되지 않은 유형 입니다.(btchId="+btchId+")");
			resMap.put(Constant.SUCCESS_YN, Constant.FAIL);
			resMap.put(Constant.RET_CODE,   messageManage.getMsgCd("AD_ERROR_158"));
			resMap.put(Constant.RET_MSG,    messageManage.getMsgTxt("AD_ERROR_158"));			
		}

		// return
		log.debug(methodName, "End!");
		return resMap;
	}
}