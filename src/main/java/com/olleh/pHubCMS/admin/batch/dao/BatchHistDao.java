package com.olleh.pHubCMS.admin.batch.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.olleh.pHubCMS.common.dao.AbstractDAO;

/**
 * 배치작업이력 Dao
 * @Class Name : BatchHistDao
 * @author : bmg
 * @since : 2018.09.28
 * @version : 1.0
 * @see
 * 		
 * << 개정이력(Modification Information) >>   
 *      수정일                 수정자                수정내용
 *  ------------  -----------    -------------
 *  2018. 9. 28.      bmg          최초 생성
 * 
 */
@Repository
public class BatchHistDao extends AbstractDAO {
	
	@SuppressWarnings("unchecked")
	public Map<String, Object> viewBatchHistTotal(Map<String, Object> params) {
		return selectOne("mybatis.batch.batchHistTotal", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List viewBatchHist(Map<String, Object> params) {
		return selectList("mybatis.batch.batchHist", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List viewBatchHistExcel(Map<String, Object> params) {
		return selectList("mybatis.batch.batchHistExcel", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List viewBatchHistLog(Map<String, Object> params) {
		return selectList("mybatis.batch.batchHistLog", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List viewBatchHistDtlLog(Map<String, Object> params) {
		return selectList("mybatis.batch.batchHistDtlLog", params);
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> viewBatchHistDtlLogTotal(Map<String, Object> params) {
		return selectOne("mybatis.batch.batchHistDtlLogTotal", params);
	}
	
	@SuppressWarnings("rawtypes")
	public List viewBatchHistDtlLogExcel(Map<String, Object> params) {
		return selectList("mybatis.batch.batchHistDtlLogExcel", params);
	}
	
}
