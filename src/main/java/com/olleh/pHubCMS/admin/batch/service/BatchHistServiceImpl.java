package com.olleh.pHubCMS.admin.batch.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.olleh.pHubCMS.admin.batch.dao.BatchHistDao;
import com.olleh.pHubCMS.common.utils.StringUtil;

/**
 * 배치작업이력 Service
 * 
 * @Class Name : SysPrmtServiceImpl
 * @author bmg
 * @since 2018.09.13
 * @version 1.0
 * @see
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.13   bmg        최초생성
 * 
 */
@Service
public class BatchHistServiceImpl implements BatchHistService {

	@Autowired
	BatchHistDao batchHistDao;

	@Override
	public Map<String, Object> viewBatchHistTotal(Map<String, Object> params) {
		params.put("START_DATE", StringUtil.nvl(params.get("START_DATE")).replaceAll("-", ""));
		params.put("END_DATE", StringUtil.nvl(params.get("END_DATE")).replaceAll("-", ""));
		return batchHistDao.viewBatchHistTotal(params);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List viewBatchHist(Map<String, Object> params) {
		params.put("START_DATE", StringUtil.nvl(params.get("START_DATE")).replaceAll("-", ""));
		params.put("END_DATE", StringUtil.nvl(params.get("END_DATE")).replaceAll("-", ""));
		return batchHistDao.viewBatchHist(params);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List viewBatchHistExcel(Map<String, Object> params) {
		params.put("START_DATE", StringUtil.nvl(params.get("START_DATE")).replaceAll("-", ""));
		params.put("END_DATE", StringUtil.nvl(params.get("END_DATE")).replaceAll("-", ""));
		return batchHistDao.viewBatchHistExcel(params);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List viewBatchHistLog(Map<String, Object> params) {
		return batchHistDao.viewBatchHistLog(params);
	}
	
	@Override
	@SuppressWarnings("rawtypes")
	public List viewBatchHistDtlLog(Map<String, Object> params) {
		return batchHistDao.viewBatchHistDtlLog(params);
	}

	@Override
	public Map<String, Object> viewBatchHistDtlLogTotal(Map<String, Object> params) {
		return batchHistDao.viewBatchHistDtlLogTotal(params);
	}

	@Override
	public List viewBatchHistDtlLogExcel(Map<String, Object> params) {
		params.put("START_DATE", StringUtil.nvl(params.get("START_DATE")).replaceAll("-", ""));
		params.put("END_DATE", StringUtil.nvl(params.get("END_DATE")).replaceAll("-", ""));
		return batchHistDao.viewBatchHistDtlLogExcel(params);
	}
}
