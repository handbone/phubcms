package com.olleh.pHubCMS.scheduler.model;
/**
 * Clip Point VS 포인트허브 거래내역 비교 VO(사용안함)
 * @Class Name : Clip2PHubVO
 * @author mason
 * @since 2018.08.20
 * @version 0.1
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.14   mason      최초생성
 * </pre>
 */
public class Clip2PHubVO {
//	private String deal_dd;      /* 거래일자 */
//	private String pnt_cd;       /* 카드사이름 */
//	private String pnt_tr_no;    /* 거래고유번호 */
//	private String deal_ind;     /* 거래구분 */
//	private String cust_ci;      /* 사용자CI */
//	private String cmpr_apv_pnt; /* 사용승인포인트 일치여부 */
//	private String cmpr_ri_pnt;  /* 거래처리포인트 일치여부 */
//	
//	public String getDeal_dd() {
//		return deal_dd;
//	}
//	public void setDeal_dd(String deal_dd) {
//		this.deal_dd = deal_dd;
//	}
//	public String getPnt_cd() {
//		return pnt_cd;
//	}
//	public void setPnt_cd(String pnt_cd) {
//		this.pnt_cd = pnt_cd;
//	}
//	public String getPnt_tr_no() {
//		return pnt_tr_no;
//	}
//	public void setPnt_tr_no(String pnt_tr_no) {
//		this.pnt_tr_no = pnt_tr_no;
//	}
//	public String getDeal_ind() {
//		return deal_ind;
//	}
//	public void setDeal_ind(String deal_ind) {
//		this.deal_ind = deal_ind;
//	}
//	public String getCust_ci() {
//		return cust_ci;
//	}
//	public void setCust_ci(String cust_ci) {
//		this.cust_ci = cust_ci;
//	}
//	public String getCmpr_apv_pnt() {
//		return cmpr_apv_pnt;
//	}
//	public void setCmpr_apv_pnt(String cmpr_apv_pnt) {
//		this.cmpr_apv_pnt = cmpr_apv_pnt;
//	}
//	public String getCmpr_ri_pnt() {
//		return cmpr_ri_pnt;
//	}
//	public void setCmpr_ri_pnt(String cmpr_ri_pnt) {
//		this.cmpr_ri_pnt = cmpr_ri_pnt;
//	}	
}
