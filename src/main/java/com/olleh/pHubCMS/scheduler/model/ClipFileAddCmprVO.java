package com.olleh.pHubCMS.scheduler.model;
/**
 * 클립포인트 대사파일에는 존재하나, 포인트허브 거래원장에는 존재하지 않는 실패건에 대해 D-2까지 재비교
 * @Class Name : ClipFileAddCmprVO
 * @author mason
 * @since 2018.09.07
 * @version 0.1
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.07   mason      최초생성
 * </pre>
 */
public class ClipFileAddCmprVO {
	private String cmprDd;
	private String pntCd;
	private String pntTrNo;
	private String ansPnt;
	private String dealInd;
	private String oriPntTrNo;
	
	public String getCmprDd() {
		return cmprDd;
	}
	public void setCmprDd(String cmprDd) {
		this.cmprDd = cmprDd;
	}
	public String getPntCd() {
		return pntCd;
	}
	public void setPntCd(String pntCd) {
		this.pntCd = pntCd;
	}
	public String getPntTrNo() {
		return pntTrNo;
	}
	public void setPntTrNo(String pntTrNo) {
		this.pntTrNo = pntTrNo;
	}
	public String getAnsPnt() {
		return ansPnt;
	}
	public void setAnsPnt(String ansPnt) {
		this.ansPnt = ansPnt;
	}
	public String getDealInd() {
		return dealInd;
	}
	public void setDealInd(String dealInd) {
		this.dealInd = dealInd;
	}
	public String getOriPntTrNo() {
		return oriPntTrNo;
	}
	public void setOriPntTrNo(String oriPntTrNo) {
		this.oriPntTrNo = oriPntTrNo;
	}
}
