package com.olleh.pHubCMS.scheduler.model;
/**
 * PG일대사D VO
 * @Class Name : PhDayCmprDVO
 * @author mason
 * @since 2018.08.17
 * @version 0.1
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.17   mason      최초생성
 * </pre>
 */
public class PhDayCmprDVO {
	private String ddCmprNo; /* 일대사번호 */
	private String seq;      /* 순번 */
	private String phubTrNo; /* 포인트허브거래번호 */
	private String pgTrNo;   /* PG사거래번호 */
	private String rmndAmt;  /* 결제잔여금액 */
	private String ansPnt;   /* 포인트 */
	private String dealInd;  /* 거래구분 */
	private String rgstDt;     /* 등록일시 */
	private String rgstUserId; /* 등록자ID */
    private String mdfyDt;     /* 수정일시 */
    private String mdfyUserid; /* 수정자ID */
    private String payAmt;     /* 결제금액 */
    
	public String getDdCmprNo() {
		return ddCmprNo;
	}
	public void setDdCmprNo(String ddCmprNo) {
		this.ddCmprNo = ddCmprNo;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getPhubTrNo() {
		return phubTrNo;
	}
	public void setPhubTrNo(String phubTrNo) {
		this.phubTrNo = phubTrNo;
	}
	public String getPgTrNo() {
		return pgTrNo;
	}
	public void setPgTrNo(String pgTrNo) {
		this.pgTrNo = pgTrNo;
	}
	public String getRmndAmt() {
		return rmndAmt;
	}
	public void setRmndAmt(String rmndAmt) {
		this.rmndAmt = rmndAmt;
	}
	public String getAnsPnt() {
		return ansPnt;
	}
	public void setAnsPnt(String ansPnt) {
		this.ansPnt = ansPnt;
	}
	public String getDealInd() {
		return dealInd;
	}
	public void setDealInd(String dealInd) {
		this.dealInd = dealInd;
	}
	public String getRgstDt() {
		return rgstDt;
	}
	public void setRgstDt(String rgstDt) {
		this.rgstDt = rgstDt;
	}
	public String getRgstUserId() {
		return rgstUserId;
	}
	public void setRgstUserId(String rgstUserId) {
		this.rgstUserId = rgstUserId;
	}
	public String getMdfyDt() {
		return mdfyDt;
	}
	public void setMdfyDt(String mdfyDt) {
		this.mdfyDt = mdfyDt;
	}
	public String getMdfyUserid() {
		return mdfyUserid;
	}
	public void setMdfyUserid(String mdfyUserid) {
		this.mdfyUserid = mdfyUserid;
	}
	public String getPayAmt() {
		return payAmt;
	}
	public void setPayAmt(String payAmt) {
		this.payAmt = payAmt;
	}    
    //
}
