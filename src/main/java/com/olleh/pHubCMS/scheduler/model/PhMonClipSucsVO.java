package com.olleh.pHubCMS.scheduler.model;
/**
 * Clip Point 비교한 결과 일치한 경우 VO(사용안함)
 * @Class Name : PhMonClipSucsVO
 * @author mason
 * @since 2018.08.20
 * @version 0.1
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.14   mason      최초생성
 * </pre>
 */
public class PhMonClipSucsVO {
//	private String cmpr_dd;    /* 대사일자 */
//	private String seq;        /* 순번 */
//	private String pntCd;      /* 포인트코드 */
//	private String phubTrNo;   /* 포인트허브 거래번호 */
//	private String pntTrNo;    /* 포인트별 거래번호 */
//	private String oriPntTrNo; /* 이전거래번호 */
//	private String ansPnt;     /* 포인트 */
//	
//	public String getCmpr_dd() {
//		return cmpr_dd;
//	}
//	public void setCmpr_dd(String cmpr_dd) {
//		this.cmpr_dd = cmpr_dd;
//	}
//	public String getSeq() {
//		return seq;
//	}
//	public void setSeq(String seq) {
//		this.seq = seq;
//	}
//	public String getPntCd() {
//		return pntCd;
//	}
//	public void setPntCd(String pntCd) {
//		this.pntCd = pntCd;
//	}
//	public String getPhubTrNo() {
//		return phubTrNo;
//	}
//	public void setPhubTrNo(String phubTrNo) {
//		this.phubTrNo = phubTrNo;
//	}
//	public String getPntTrNo() {
//		return pntTrNo;
//	}
//	public void setPntTrNo(String pntTrNo) {
//		this.pntTrNo = pntTrNo;
//	}
//	public String getOriPntTrNo() {
//		return oriPntTrNo;
//	}
//	public void setOriPntTrNo(String oriPntTrNo) {
//		this.oriPntTrNo = oriPntTrNo;
//	}
//	public String getAnsPnt() {
//		return ansPnt;
//	}
//	public void setAnsPnt(String ansPnt) {
//		this.ansPnt = ansPnt;
//	}
}
