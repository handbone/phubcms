package com.olleh.pHubCMS.scheduler.model;
/**
 * Clip Point parsing 결과 VO(사용안함)
 * @Class Name : PhClippointDocVO
 * @author mason
 * @since 2018.08.20
 * @version 0.1
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.14   mason      최초생성
 * </pre>
 */
public class PhClippointDocVO {
//	private String ptHbDateTime; /* 파일생성일시 */
//	private String ptHbSeqnum;   /* 거래일련번호 */
//	private String ptHbVer;      /* 전문버전 */
//	private String ptHbBneCd;    /* 거래구분 */
//	private String ptHbUqeN;     /* 거래고유번호 */
//	private String ptHbPartner;  /* 포인트코드 */
//	private String ptHbUcd;      /* 처리결과코드 */
//	private String ptHbAprvPnt;  /* 사용승인포인트 */
//	private String ptHbRiPnt;    /* 거래처리포인트 */
//	private String ptHbOriUqeN;  /* 취소거래고유번호 */ 
//	private String ptHbCi;       /* 사용자CI */
//	private String ptHbCt;       /* 총거래건수 */
//	private String rgstDt;       /* 등록일시 */
//	private String rgstUserId;   /* 등록자ID */
//	
//	public String getPtHbDateTime() {
//		return ptHbDateTime;
//	}
//	public void setPtHbDateTime(String ptHbDateTime) {
//		this.ptHbDateTime = ptHbDateTime;
//	}
//	public String getPtHbSeqnum() {
//		return ptHbSeqnum;
//	}
//	public void setPtHbSeqnum(String ptHbSeqnum) {
//		this.ptHbSeqnum = ptHbSeqnum;
//	}
//	public String getPtHbVer() {
//		return ptHbVer;
//	}
//	public void setPtHbVer(String ptHbVer) {
//		this.ptHbVer = ptHbVer;
//	}
//	public String getPtHbBneCd() {
//		return ptHbBneCd;
//	}
//	public void setPtHbBneCd(String ptHbBneCd) {
//		this.ptHbBneCd = ptHbBneCd;
//	}
//	public String getPtHbUqeN() {
//		return ptHbUqeN;
//	}
//	public void setPtHbUqeN(String ptHbUqeN) {
//		this.ptHbUqeN = ptHbUqeN;
//	}
//	public String getPtHbPartner() {
//		return ptHbPartner;
//	}
//	public void setPtHbPartner(String ptHbPartner) {
//		this.ptHbPartner = ptHbPartner;
//	}
//	public String getPtHbUcd() {
//		return ptHbUcd;
//	}
//	public void setPtHbUcd(String ptHbUcd) {
//		this.ptHbUcd = ptHbUcd;
//	}
//	public String getPtHbAprvPnt() {
//		return ptHbAprvPnt;
//	}
//	public void setPtHbAprvPnt(String ptHbAprvPnt) {
//		this.ptHbAprvPnt = ptHbAprvPnt;
//	}
//	public String getPtHbRiPnt() {
//		return ptHbRiPnt;
//	}
//	public void setPtHbRiPnt(String ptHbRiPnt) {
//		this.ptHbRiPnt = ptHbRiPnt;
//	}
//	public String getPtHbOriUqeN() {
//		return ptHbOriUqeN;
//	}
//	public void setPtHbOriUqeN(String ptHbOriUqeN) {
//		this.ptHbOriUqeN = ptHbOriUqeN;
//	}
//	public String getPtHbCi() {
//		return ptHbCi;
//	}
//	public void setPtHbCi(String ptHbCi) {
//		this.ptHbCi = ptHbCi;
//	}
//	public String getPtHbCt() {
//		return ptHbCt;
//	}
//	public void setPtHbCt(String ptHbCt) {
//		this.ptHbCt = ptHbCt;
//	}
//	public String getRgstDt() {
//		return rgstDt;
//	}
//	public void setRgstDt(String rgstDt) {
//		this.rgstDt = rgstDt;
//	}
//	public String getRgstUserId() {
//		return rgstUserId;
//	}
//	public void setRgstUserId(String rgstUserId) {
//		this.rgstUserId = rgstUserId;
//	}
}
