package com.olleh.pHubCMS.scheduler.model;
/**
 * PG일대사M VO
 * @Class Name : PhDayCmprMVO
 * @author mason
 * @since 2018.08.17
 * @version 0.1
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.17   mason      최초생성
 * </pre>
 */
public class PhDayCmprMVO {
	private String ddCmprNo;   /* 일대사번호 */
	private String pgCmpnId;   /* PG사ID */
	private String cmprDd;     /* 대사일자 */
	private String cmprTtlCnt; /* 대사총건수 */
	private String cmprTtlAmt; /* 대사총금액 */
	private String filePath;   /* 파일경로 */
	private String fileNm;     /* 파일명 */
	private String trtmRsltCd; /* 처리결과코드 */
	private String rsltMsg;    /* 결과메시지 */
	private String rgstDt;     /* 등록일시 */
	private String rgstUserId; /* 등록자ID */
    private String mdfyDt;     /* 수정일시 */
    private String mdfyUserid; /* 수정자ID */
    
	public String getDdCmprNo() {
		return ddCmprNo;
	}
	public void setDdCmprNo(String ddCmprNo) {
		this.ddCmprNo = ddCmprNo;
	}
	public String getPgCmpnId() {
		return pgCmpnId;
	}
	public void setPgCmpnId(String pgCmpnId) {
		this.pgCmpnId = pgCmpnId;
	}
	public String getCmprDd() {
		return cmprDd;
	}
	public void setCmprDd(String cmprDd) {
		this.cmprDd = cmprDd;
	}
	public String getCmprTtlCnt() {
		return cmprTtlCnt;
	}
	public void setCmprTtlCnt(String cmprTtlCnt) {
		this.cmprTtlCnt = cmprTtlCnt;
	}
	public String getCmprTtlAmt() {
		return cmprTtlAmt;
	}
	public void setCmprTtlAmt(String cmprTtlAmt) {
		this.cmprTtlAmt = cmprTtlAmt;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getFileNm() {
		return fileNm;
	}
	public void setFileNm(String fileNm) {
		this.fileNm = fileNm;
	}
	public String getTrtmRsltCd() {
		return trtmRsltCd;
	}
	public void setTrtmRsltCd(String trtmRsltCd) {
		this.trtmRsltCd = trtmRsltCd;
	}
	public String getRsltMsg() {
		return rsltMsg;
	}
	public void setRsltMsg(String rsltMsg) {
		this.rsltMsg = rsltMsg;
	}
	public String getRgstDt() {
		return rgstDt;
	}
	public void setRgstDt(String rgstDt) {
		this.rgstDt = rgstDt;
	}
	public String getRgstUserId() {
		return rgstUserId;
	}
	public void setRgstUserId(String rgstUserId) {
		this.rgstUserId = rgstUserId;
	}
	public String getMdfyDt() {
		return mdfyDt;
	}
	public void setMdfyDt(String mdfyDt) {
		this.mdfyDt = mdfyDt;
	}
	public String getMdfyUserid() {
		return mdfyUserid;
	}
	public void setMdfyUserid(String mdfyUserid) {
		this.mdfyUserid = mdfyUserid;
	}    
    //
}
