package com.olleh.pHubCMS.scheduler.model;

import com.olleh.pHubCMS.common.utils.StringUtil;
/**
 * 포인트허브 거래내역 VO
 * @Class Name : PHubToPGVO
 * @author mason
 * @since 2018.08.14
 * @version 0.1
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.14   mason      최초생성
 * </pre>
 */
public class PHubToPGVO {
	/** 헤더 **/
	private String ptHbVl1;        //포인트허브 구분값1
	private String ptHbVer;        //포인트허브 전문버전
	private String ptHbCmprNo;     //포인트허브 일대사번호 
	private String ptHbCmprDd;     //일대사기준일
	private String ptHbFer1;       //포인트허브 필러1
	
	/** 본문 **/
	private String ptHbVl2;        //포인트허브 구분값2
	private String ptHbSeqnum;     //포인트허브 거래일련번호
	private String ptHbDealInd;    //거래구분,PA:결제 CA:결제취소
	private String ptHbUqeNo;      //포인트허브 거래고유번호
	private String ptHbOthNo;      //타사 거래고유번호
	private String ptHbGoodsPrice; //상품금액
	private String ptHbRiPnt;      //포인트허브 거래처리포인트
	private String ptHbRmndAmt;    //포인트허브 잔여결제금액
	private String ptHbUqeN;       //포인트허브 취소거래고유번호
	private String ptHbShopCd;     //세틀뱅크 제공 거래처ID
	private String ptHbFer2;       //포인트허브 필러2
	
	/** Tail **/
	private String ptHbVl3;        //포인트허브 구분값3
	private String ptHbCt;         //포인트허브 건수
	private String ptHbDateTime;   //포인트허브 생성일시
	private String ptHbFer3;       //포인트허브 필러3
	
	private String ptHbPgCmpnId;   //PG사ID
	private String ptHbPgTrNo;     //PG사거래번호 
	
	public String getPtHbShopCd(){
		return ptHbShopCd;
	}
	public void setPtHbShopCd(String ptHbShopCd){
		this.ptHbShopCd = ptHbShopCd;
	}
	
	public String getPtHbPgTrNo() {
		return ptHbPgTrNo;
	}
	public void setPtHbPgTrNo(String ptHbPgTrNo) {
		this.ptHbPgTrNo = ptHbPgTrNo;
	}
	public String getPtHbPgCmpnId() {
		return ptHbPgCmpnId;
	}
	public void setPtHbPgCmpnId(String ptHbPgCmpnId) {
		this.ptHbPgCmpnId = ptHbPgCmpnId;
	}
	public String getPtHbVl1() {
		return StringUtil.nvl(ptHbVl1,"HD");
	}
	public void setPtHbVl1(String ptHbVl1) {
		this.ptHbVl1 = ptHbVl1;
	}
	public String getPtHbVer() {
		return ptHbVer;
	}
	public void setPtHbVer(String ptHbVer) {
		this.ptHbVer = ptHbVer;
	}
	public String getPtHbCmprNo() {
		return ptHbCmprNo;
	}
	public void setPtHbCmprNo(String ptHbCmprNo) {
		this.ptHbCmprNo = ptHbCmprNo;
	}
	public String getPtHbCmprDd() {
		return ptHbCmprDd;
	}
	public void setPtHbCmprDd(String ptHbCmprDd) {
		this.ptHbCmprDd = ptHbCmprDd;
	}
	public String getPtHbFer1() {
		return ptHbFer1;
	}
	public void setPtHbFer1(String ptHbFer1) {
		this.ptHbFer1 = ptHbFer1;
	}
	public String getPtHbVl2() {
		return StringUtil.nvl(ptHbVl2,"DT");
	}
	public void setPtHbVl2(String ptHbVl2) {
		this.ptHbVl2 = ptHbVl2;
	}
	public String getPtHbSeqnum() {
		return ptHbSeqnum;
	}
	public void setPtHbSeqnum(String ptHbSeqnum) {
		this.ptHbSeqnum = ptHbSeqnum;
	}
	public String getPtHbDealInd() {
		return ptHbDealInd;
	}
	public void setPtHbDealInd(String ptHbDealInd) {
		this.ptHbDealInd = ptHbDealInd;
	}
	public String getPtHbUqeNo() {
		return ptHbUqeNo;
	}
	public void setPtHbUqeNo(String ptHbUqeNo) {
		this.ptHbUqeNo = ptHbUqeNo;
	}
	public String getPtHbOthNo() {
		return ptHbOthNo;
	}
	public void setPtHbOthNo(String ptHbOthNo) {
		this.ptHbOthNo = ptHbOthNo;
	}
	public String getPtHbGoodsPrice() {
		return ptHbGoodsPrice;
	}
	public void setPtHbGoodsPrice(String ptHbGoodsPrice) {
		this.ptHbGoodsPrice = ptHbGoodsPrice;
	}
	public String getPtHbRiPnt() {
		return ptHbRiPnt;
	}
	public void setPtHbRiPnt(String ptHbRiPnt) {
		this.ptHbRiPnt = ptHbRiPnt;
	}
	public String getPtHbRmndAmt() {
		return ptHbRmndAmt;
	}
	public void setPtHbRmndAmt(String ptHbRmndAmt) {
		this.ptHbRmndAmt = ptHbRmndAmt;
	}
	public String getPtHbUqeN() {
		return StringUtil.nvl(ptHbUqeN,"");
	}
	public void setPtHbUqeN(String ptHbUqeN) {
		this.ptHbUqeN = ptHbUqeN;
	}
	public String getPtHbFer2() {
		return ptHbFer2;
	}
	public void setPtHbFer2(String ptHbFer2) {
		this.ptHbFer2 = ptHbFer2;
	}
	public String getPtHbVl3() {
		return StringUtil.nvl(ptHbVl3,"TR");
	}
	public void setPtHbVl3(String ptHbVl3) {
		this.ptHbVl3 = ptHbVl3;
	}
	public String getPtHbCt() {
		return ptHbCt;
	}
	public void setPtHbCt(String ptHbCt) {
		this.ptHbCt = ptHbCt;
	}
	public String getPtHbDateTime() {
		return ptHbDateTime;
	}
	public void setPtHbDateTime(String ptHbDateTime) {
		this.ptHbDateTime = ptHbDateTime;
	}
	public String getPtHbFer3() {
		return ptHbFer3;
	}
	public void setPtHbFer3(String ptHbFer3) {
		this.ptHbFer3 = ptHbFer3;
	}	
}
