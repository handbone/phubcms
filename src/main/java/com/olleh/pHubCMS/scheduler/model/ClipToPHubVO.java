package com.olleh.pHubCMS.scheduler.model;

import com.olleh.pHubCMS.common.utils.StringUtil;
/**
 * Clip Point 거래내역 VO(로컬 테스트용)
 * @Class Name : ClipToPHubVO
 * @author mason
 * @since 2018.08.14
 * @version 0.1
 * @see <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.14   mason      최초생성
 * </pre>
 */
public class ClipToPHubVO {
	private String ptHbVl1;     /* 포인트허브 구분값1 */
	private String ptHbVer;    /* 포인트허브 전문버전 */
	private String ptHbDateTime;/* 포인트허브 생성일시 */
	private String ptHbFer1;    /* 포인트허브 필러1 */
	private String ptHbVl2;     /* 포인트허브 구분값2 */
	private String ptHbSeqnum;  /* 포인트허브 거래일련번호 */
	private String ptHbBneCd;   /* 포인트허브 업무코드 */
	private String ptHbUqeNo;   /* 포인트허브 거래고유번호 */
	private String ptHbPartner; /* 포인트허브 카드사 이름 */
	private String ptHbUcd;     /* 포인트허브 처리결과코드 */
	private String ptHbApvPnt;  /* 포인트허브 사용승인포인트 */
	private String ptHbRiPnt;   /* 포인트허브 거래처리포인트 */
	private String ptHbUqeN;    /* 포인트허브 취소거래고유번호 */
	private String ptHbCi;      /* 포인트허브 유저 CI */
	private String ptHbFer2;    /* 포인트허브 필러2 */
	private String ptHbVl3;     /* 포인트허브 구분값3 */
	private String ptHbCt;      /* 포인트허브 건수 */
	private String ptHbFer3;    /* 포인트허브 필러3 */
	
	public String getPtHbVl1() {
		return StringUtil.nvl(ptHbVl1 , "HD");
	}
	public void setPtHbVl1(String ptHbVl1) {
		this.ptHbVl1 = ptHbVl1;
	}
	public String getPtHbVer() {
		return ptHbVer;
	}
	public void setPtHbVer(String ptHbVer) {
		this.ptHbVer = ptHbVer;
	}
	public String getPtHbDateTime() {
		return ptHbDateTime;
	}
	public void setPtHbDateTime(String ptHbDateTime) {
		this.ptHbDateTime = ptHbDateTime;
	}
	public String getPtHbFer1() {
		return ptHbFer1;
	}
	public void setPtHbFer1(String ptHbFer1) {
		this.ptHbFer1 = ptHbFer1;
	}
	public String getPtHbVl2() {
		return StringUtil.nvl(ptHbVl2 , "DT");
	}
	public void setPtHbVl2(String ptHbVl2) {
		this.ptHbVl2 = ptHbVl2;
	}
	public String getPtHbSeqnum() {
		return ptHbSeqnum;
	}
	public void setPtHbSeqnum(String ptHbSeqnum) {
		this.ptHbSeqnum = ptHbSeqnum;
	}
	public String getPtHbBneCd() {
		return ptHbBneCd;
	}
	public void setPtHbBneCd(String ptHbBneCd) {
		this.ptHbBneCd = ptHbBneCd;
	}
	public String getPtHbUqeNo() {
		return ptHbUqeNo;
	}
	public void setPtHbUqeNo(String ptHbUqeNo) {
		this.ptHbUqeNo = ptHbUqeNo;
	}
	public String getPtHbPartner() {
		return ptHbPartner;
	}
	public void setPtHbPartner(String ptHbPartner) {
		this.ptHbPartner = ptHbPartner;
	}
	public String getPtHbUcd() {
		return ptHbUcd;
	}
	public void setPtHbUcd(String ptHbUcd) {
		this.ptHbUcd = ptHbUcd;
	}
	public String getPtHbApvPnt() {
		return ptHbApvPnt;
	}
	public void setPtHbApvPnt(String ptHbApvPnt) {
		this.ptHbApvPnt = ptHbApvPnt;
	}
	public String getPtHbRiPnt() {
		return ptHbRiPnt;
	}
	public void setPtHbRiPnt(String ptHbRiPnt) {
		this.ptHbRiPnt = ptHbRiPnt;
	}
	public String getPtHbUqeN() {
		return StringUtil.nvl(ptHbUqeN , "");
	}
	public void setPtHbUqeN(String ptHbUqeN) {
		this.ptHbUqeN = ptHbUqeN;
	}
	public String getPtHbCi() {
		return ptHbCi;
	}
	public void setPtHbCi(String ptHbCi) {
		this.ptHbCi = ptHbCi;
	}
	public String getPtHbFer2() {
		return ptHbFer2;
	}
	public void setPtHbFer2(String ptHbFer2) {
		this.ptHbFer2 = ptHbFer2;
	}
	public String getPtHbVl3() {
		return StringUtil.nvl(ptHbVl3 , "TR");
	}
	public void setPtHbVl3(String ptHbVl3) {
		this.ptHbVl3 = ptHbVl3;
	}
	public String getPtHbCt() {
		return ptHbCt;
	}
	public void setPtHbCt(String ptHbCt) {
		this.ptHbCt = ptHbCt;
	}
	public String getPtHbFer3() {
		return ptHbFer3;
	}
	public void setPtHbFer3(String ptHbFer3) {
		this.ptHbFer3 = ptHbFer3;
	}
}
