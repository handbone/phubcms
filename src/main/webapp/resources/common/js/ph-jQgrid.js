/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 * Revision History
 * Author            Date              Description
 * ------            -----             ------------
 * ojh               2018.08.28         jQgrid 펑션 
 */
PHJQg = function(){
	this.isLog = true;
	this.ajaxProgress = true;
};

/**
* 
* jQgrid 초기화
*
* @param id: jQgrid id
* @param colNames: 테이블 colnames 헤더
* @param colModel: 테이블 행 데이터
*
* @create 2018.08.28. ojh
**/
PHJQg.prototype.load = function(id, pager, colNames, colModel, rowNum) {

	//초기화 부분
    $(".loader").show();
    $("#"+id).jqGrid({
    	rowNum : rowNum,  
    	datatype: "local",
        mtype: "POST",
        styleUI : 'Bootstrap',
        height : "100%",
        rowList: [10,15,20,25,30],         
        pager: "#"+pager, 
        shrinkToFit:false,
        cmTemplate: { sortable: false },
        viewrecords: true,      
        colNames: colNames,
        colModel: colModel,
        loadComplete: function () {
            $(".loader").hide();
        },
        loadError:function(xhr,status,error){
        	$(".loader").hide();
        	PHFnc.log("[loadError]조회 실패!");
        	PHFnc.log("[loadError]error="+error);
        	PHFnc.alert("[loadError]조회 실패!");
        },
        gridComplete:function(){
            //rowspan - 셀합치기
            var grid = this;            
            $('td[name="cellRowspan"]', grid).each(function() {
                var spans = $('td[rowspanid="'+this.id+'"]',grid).length+1;
                if(spans>1){
                 $(this).attr('rowspan',spans);
                }
            }); 
        }
    });
    jQuery("#"+id).jqGrid('setGridWidth', $("#content").width());
    
    //x축 스크롤바 제거
    //$('.ui-jqgrid .ui-jqgrid-bdiv').css('overflow-x', 'hidden');
    
    //header 2줄 표기시 css 변경
    $('.ui-jqgrid .ui-jqgrid-htable th div').css({
    	'height'			:'auto'
	    ,'overflow'			:'hidden'
	    ,'padding-right'	:'2px'
	    ,'padding-left'		:'2px'
	    ,'padding-top'		:'4px'
	    ,'padding-bottom'	:'4px'
	    ,'position'			:'relative'
	    ,'vertical-align'	:'text-top'
	    ,'white-space'		:'normal !important'
    });
    
    $("#"+pager).css("display", "none");
};

PHJQg.prototype.loadTotalRow = function(id, pager, colNames, colModel) {
	//초기화 부분
    $(".loader").show();
    $("#"+id).jqGrid({
    	datatype: "local",
        mtype: "POST",
        styleUI : 'Bootstrap',
        height : "100%",
        rowList: [10,15,20,25,30],         
        pager: "#"+pager, 
        shrinkToFit:false,
        cmTemplate: { sortable: false },
        viewrecords: true,      
        colNames: colNames,
        colModel: colModel,
        footerrow:true,
        userDataOnFooter:true,
        loadComplete: function () {
            $(".loader").hide();
            $('.ui-jqgrid-ftable').find('tr').attr('style','background-color:#dfeffc');
            $(this).jqGrid('footerData','set', {no:'Total'});
        },
        loadError:function(xhr,status,error){
        	$(".loader").hide();
        	PHFnc.log("[loadError]조회 실패!");
        	PHFnc.log("[loadError]error="+error);
        	PHFnc.alert("[loadError]조회 실패!");
        },
        gridComplete:function(){
            //rowspan - 셀합치기
            var grid = this;            
            $('td[name="cellRowspan"]', grid).each(function() {
                var spans = $('td[rowspanid="'+this.id+'"]',grid).length+1;
                if(spans>1){
                 $(this).attr('rowspan',spans);
                }
            }); 
        }
    });
    jQuery("#"+id).jqGrid('setGridWidth', $("#content").width());
    
    //x축 스크롤바 제거
    //$('.ui-jqgrid .ui-jqgrid-bdiv').css('overflow-x', 'hidden');
    
    //header 2줄 표기시 css 변경
    $('.ui-jqgrid .ui-jqgrid-htable th div').css({
    	'height'			:'auto'
	    ,'overflow'			:'hidden'
	    ,'padding-right'	:'2px'
	    ,'padding-left'		:'2px'
	    ,'padding-top'		:'4px'
	    ,'padding-bottom'	:'4px'
	    ,'position'			:'relative'
	    ,'vertical-align'	:'text-top'
	    ,'white-space'		:'normal !important'
    });
    
    $("#"+pager).css("display", "none");
};

/**
* jQgrid 리로드 (검색)
* 
* @param id: jQgrid id
* @param url
* @param postData 전달데이터 (검색조건)
* 
* @return boolean
* @create 2018-08-29
*/
PHJQg.prototype.reload = function(id, pager,url, postData) {
	$(".loader").show();	
	//그리드 초기화
	
	$("#"+id).jqGrid("setGridParam", true);		
	$("#"+id).jqGrid("clearGridData", true);
	
	//그리드 리로드
	$("#"+id).jqGrid('setGridParam',{
		url: url,
		datatype: "json",
		postData : postData,
		loadComplete: function () {
            $(".loader").hide();
        },
        loadError:function(xhr,status,error){
        	$(".loader").hide();
        	PHFnc.log("[loadError]조회 실패!");
        	PHFnc.log("[loadError]error="+error);
        	PHFnc.alert("[loadError]조회 실패!");
        }
	}).trigger('reloadGrid');
	
	$("#"+pager).css("display", "block");
};

PHJQg.prototype.reloadMessage = function(id, pager,url, postData) {
	$(".loader").show();	
	//그리드 초기화
	
	$("#"+id).jqGrid("setGridParam", true);		
	$("#"+id).jqGrid("clearGridData", true);
	
	//그리드 리로드
	$("#"+id).jqGrid('setGridParam',{
		url: url,
		datatype: "json",
		postData : postData,
		loadComplete: function (data) {
            $(".loader").hide();
            if (data.errMsg != null) {
            	$(this).jqGrid("clearGridData", true);
            	alert(data.errMsg);
            	return false;
            }
        },
        loadError:function(xhr,status,error){
        	$(".loader").hide();
        	PHFnc.log("[loadError]조회 실패!");
        	PHFnc.log("[loadError]error="+error);
        	PHFnc.alert("[loadError]조회 실패!");
        }
	}).trigger('reloadGrid');
	
	$("#"+pager).css("display", "block");
};

PHJQg.prototype.reloadCallback = function(id, pager,url, postData, callback) {
	$(".loader").show();	
	//그리드 초기화
	
	$("#"+id).jqGrid("setGridParam", true);		
	$("#"+id).jqGrid("clearGridData", true);
	
	//그리드 리로드
	$("#"+id).jqGrid('setGridParam',{
		url: url,
		datatype: "json",
		postData : postData,
		loadComplete: function (data) {
            $(".loader").hide();
            callback(this);
        },
        loadError:function(xhr,status,error){
        	$(".loader").hide();
        	PHFnc.log("[loadError]조회 실패!");
        	PHFnc.log("[loadError]error="+error);
        	PHFnc.alert("[loadError]조회 실패!");
        }
	}).trigger('reloadGrid');
	
	$("#"+pager).css("display", "block");
};

PHJQg.prototype.reloadCallback2 = function(id, pager,url, postData, callback, callback2) {
	$(".loader").show();	
	//그리드 초기화
	
	$("#"+id).jqGrid("setGridParam", true);		
	$("#"+id).jqGrid("clearGridData", true);
	
	//그리드 리로드
	$("#"+id).jqGrid('setGridParam',{
		url: url,
		datatype: "json",
		postData : postData,
		loadComplete: function (data) {			
            $(".loader").hide();
            callback(this);
            callback2(data);
        },
        loadError:function(xhr,status,error){
        	$(".loader").hide();
        	PHFnc.log("[loadError]조회 실패!");
        	PHFnc.log("[loadError]error="+error);
        	PHFnc.alert("[loadError]조회 실패!");
        }
	}).trigger('reloadGrid');
	
	$("#"+pager).css("display", "block");
};

PHJQg.prototype.colSum = function(obj, name, formatter) {
	return $(obj).jqGrid('getCol', name, false, 'sum');
}

/**
* jQgrid 엑셀다운로드 (검색)
* 
* @param id: jQgrid id
* @param url
* @param postData: 전달데이터 (검색조건)
* @param colNames: 테이블 colnames 헤더
* @param colModel: 테이블 행 데이터
* @param fileName: 생성할 엑셀파일명
* 
* @return boolean
* @create 2018-08-29
*/
PHJQg.prototype.excel = function(id, url, postData, colNames, colModel,fileName){
	$("#"+id).html("<table id='jqGridExcel'></table>");	
	$(".loader").show();
	$("#jqGridExcel").jqGrid({
    	url: url,
        datatype: "json",
        mtype: "POST",
        loadonce:true,
        postData : postData,
		colNames: colNames,
        colModel: colModel,
        loadComplete: function () {
        	$("#jqGridExcel").jqGrid("exportToExcel",{
        		includeLabels : true,
        		includeGroupHeader : true,
        		includeFooter: true,
        		fileName : fileName+".xlsx",
        		maxlength : 40 // maxlength for visible string data 
        	})
        	$(".loader").hide();
        }
	}); 
}


PHJQg.prototype.excelCallback = function(id, url, postData, colNames, colModel,fileName,callback){
	$("#"+id).html("<table id='jqGridExcel'></table>");	
	$(".loader").show();
	$("#jqGridExcel").jqGrid({
    	url: url,
        datatype: "json",
        mtype: "POST",
        loadonce:true,
        postData : postData,
		colNames: colNames,
        colModel: colModel,
        loadComplete: function (data) {
        	callback(data);
        	$("#jqGridExcel").jqGrid("exportToExcel",{
        		includeLabels : true,
        		includeGroupHeader : true,
        		includeFooter: true,
        		fileName : fileName+".xlsx",
        		maxlength : 40 // maxlength for visible string data 
        	})
        	$(".loader").hide();
        }
	}); 
}

PHJQg.prototype.excelPopup = function(div, table, url, postData, colNames, colModel,fileName){
	$("#"+div).html("<table id='"+ table +"'></table>");	
	$(".loader").show();
	$("#"+table).jqGrid({
    	url: url,
        datatype: "json",
        mtype: "POST",
        loadonce:true,
        postData : postData,
		colNames: colNames,
        colModel: colModel,
        loadComplete: function () {
        	$("#"+table).jqGrid("exportToExcel",{
        		includeLabels : true,
        		includeGroupHeader : true,
        		includeFooter: true,
        		fileName : fileName+".xlsx",
        		maxlength : 40 // maxlength for visible string data 
        	})
        	$(".loader").hide();
        }
	}); 
}

PHJQg.prototype.size = function(id) {
	return $('#'+id).getGridParam("reccount");
}

//클래스 생성
var PHJQg = new PHJQg();