/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 * Revision History
 * Author            Date              Description
 * ------            -----             ------------
 * lys               2018.07.07        공통 유효성 체크 
 */
PHValid = function(){};

/**
* 핸드폰
* @param
* @return boolean
* @create 2016-03-29
*/
PHValid.prototype.hpNo = function(val) {

	//var pattern = /^([0]{1}[0-9]{1,2})-?([1-9]{1}[0-9]{2,3})-?([0-9]{4})$/;
	var pattern = /^([0]{1}[0-9]{2,3})-?([1-9]{1}[0-9]{2,3})-?([0-9]{4})$/; // 국번 4자리
	var chkVal = PHUtil.nvl(val, this.val);
	var len = (chkVal).split("-").join("").length;
	
	//12자리 이상이면 false
	if (len > 12) {
		return false;
	}

	if (pattern.exec(chkVal)) {
		/*
		 * 4자리가 있을수 있음
		 */
		//if(RegExp.$1 == "011" || RegExp.$1 == "016" || RegExp.$1 == "017" || RegExp.$1 == "018" || RegExp.$1 == "019" || RegExp.$1 == "010") {
		if(chkVal.substring(0,1) == "0") {
			return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
};

/**
* 전화번호
* @param
* @return boolean
* @create 2016-03-29
*/
PHValid.prototype.telNo = function(val) {
	var pattern = /^[0-9]{2,4}-?[0-9]{3,4}-?[0-9]{4}$/;
	var chkVal = PHUtil.nvl(val, this.val);
	var len = (chkVal).split("-").join("").length;
	
	if (len > 11) {
		return false;
	}
	
	if ( pattern.exec(chkVal) ) {
		return true;
	} else {
		return false;
	}
};

/**
* 사업자 등록번호
* @param
* @return boolean
* @create 2016-03-30
*/
PHValid.prototype.bizNo = function(val) {
	var objstring = PHUtil.nvl(val, this.val);
	objstring = objstring.split("-").join("");
	var li_temp, li_lastid;
	
	biz_value = new Array(10); 
	if ( objstring.length != 10 ) return false;

    biz_value[0] = ( parseFloat(objstring.substring(0 ,1)) * 1 ) % 10; 
    biz_value[1] = ( parseFloat(objstring.substring(1 ,2)) * 3 ) % 10; 
    biz_value[2] = ( parseFloat(objstring.substring(2 ,3)) * 7 ) % 10; 
    biz_value[3] = ( parseFloat(objstring.substring(3 ,4)) * 1 ) % 10; 
    biz_value[4] = ( parseFloat(objstring.substring(4 ,5)) * 3 ) % 10; 
    biz_value[5] = ( parseFloat(objstring.substring(5 ,6)) * 7 ) % 10; 
    biz_value[6] = ( parseFloat(objstring.substring(6 ,7)) * 1 ) % 10; 
    biz_value[7] = ( parseFloat(objstring.substring(7, 8)) * 3 ) % 10; 
    li_temp = parseFloat(objstring.substring(8, 9)) * 5 + "0"; 
    biz_value[8] = parseFloat(li_temp.substring(0,1)) + parseFloat(li_temp.substring(1,2)); 
    biz_value[9] = parseFloat(objstring.substring(9,10)); 
    li_lastid = (10 - ( ( biz_value[0] + biz_value[1] + biz_value[2] + biz_value[3] + biz_value[4] + biz_value[5] + biz_value[6] + biz_value[7] + biz_value[8] ) % 10 ) ) % 10; 
    
    if (biz_value[9] != li_lastid) { 
        return false;
    } else {
        return true;
    }
};

/**
* 영어만 입력하게끔
* @param
* @return boolean
* @create 2016-03-30
*/
PHValid.prototype.eng = function(val) {

	//var pattern = /^[a-zA-Z0-9-\.\_]+$/;
	var pattern = /^[a-zA-Z-\.\_]+$/;
	var chkVal = PHUtil.nvl(val, this.val);
	
	//빈값은 성공
	if (pattern.exec(chkVal.split(" ").join(""))) {
		return true;
	} else {
		return false;
	}
};

/**
* 숫자만입력 체크
* @param
* @return boolean
* @create 2016-03-30
*/
PHValid.prototype.number = function(val) {
	var pattern = /[^0-9]/g;
	var chkVal = PHUtil.replaceAll(PHUtil.nvl(val, this.val), ',', '');
   
	if (pattern.exec(chkVal) == null) {
		return true;
	} else {
		return false;
	}
};

/**
* 이메일 체크
* @param
* @return boolean
* @create 2016-03-30
*/
PHValid.prototype.email = function(val) {
	var pattern = /^[_a-zA-Z0-9-\.\_]+@[\.a-zA-Z0-9-]+\.[a-zA-Z]+$/;
	var chkVal = PHUtil.nvl(val, this.val);
	
	return (pattern.test(chkVal)) ? true : false;
};

/**
 * 한글 입력 체크
 * @param
 * @return boolean
 * @create 2016-03-30
 */
PHValid.prototype.kor = function(val) {

	var pattern = /^[가-힣]+$/;
	var chkVal = PHUtil.nvl(val, this.val);

	if (pattern.test(chkVal) == true) {
		return true;
	} else {
		return false;
	}
};

/**
 * 날짜 정합성체크
 * @param
 * @return boolean
 * @create 2016-03-30
 */
PHValid.prototype.date = function(val) {
    
    var pattern = /^(\d{4})(\d{2})/;
    var chkVal = PHUtil.nvl(val, this.val);
    var currDate = chkVal.split(pattern).join("-").replace("-", "");
    //PHFnc.log("[PHValid.date]currDate="+currDate);
    try {
        $.datepicker.parseDate('yy-mm-dd', currDate);
        if(chkVal.length != 8) return false;
        return true;
    } catch (error) {
    	PHFnc.log(error.message);
        return false;
    }
};

/**
* IP 형식 체크
* @param
* @return boolean
* @create 2018-08-29
*/
PHValid.prototype.ip = function(val){	
	var filter = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/;
	
	if (filter.test(val) == true){
		return true;
	} else{
		return false;
	}
};

/**
* PHValid Class 생성
* @param 
* @return
*/
var PHValid = new PHValid();

