/**
 * Copyright (c) 2018 KT, Inc.
 * All right reserved.
 *
 * This software is the confidential and proprietary information of KT,
 * Inc. You shall not disclose such Confidential Information and
 * shall use it only in accordance with the terms of the license agreement
 * you entered into with KT.
 *
 * Revision History
 * Author            Date              Description
 * ------            -----             ------------
 * lys               2018.07.07        공통 펑션 
 */
PHFnc = function(){
	this.isLog = true;
	this.ajaxProgress = true;
};

// 메뉴 클릭 펑션
PHFnc.prototype.lnbDoAction = function(obj) {
	var url = $(obj).attr("href");
	PHFnc.log(url);
	
	if( (url == "\/") || (PHUtil.nvl(url) == '') ) {
		PHFnc.alert("현재 개발중인 화면 입니다.");
		return false;
	} else {
		//location.href = url;
		//PHFnc.submitLoading();
		PHFnc.doAction(url);
	}
};

/**
팝업펑션

url: URL주소
params: 파라미터(String, Object)
winName: 팝업창명
width: 팝업 넓이
height: 팝업 높이
winOpt: 팝업옵션 Object. (팝업속성, 파라미터등..)

2016.03.22. 이용수
**/
PHFnc.prototype.windowOpen = function(url, params, winName, width, height, winOpt) {
	var winHeight = (window.screen.height);
	var winWidth  = (window.screen.width);
	var frm = PHFnc.SetParamForm(params, "", false);
	var op_view;
	
	if((width == undefined) || (width == "")) width = winWidth;
	if((height == undefined) || (height == "")) height = winHeight;
	if((winOpt == undefined) || (winOpt == "")) winOpt = "";
	
	//팝업위치 계산
	winWidth  = (winWidth-width);
	winHeight = (winHeight-height);
	
	//Set config
	winName = (PHUtil.isEmpty(winName))?"popup":winName;
	
	//window Open
//	if (width == '' && height == '') {
//		op_view = window.open("", winName, getPopStat1());
//	} else {
		//op_view = window.open("", winName, "top="+ winHeight/2 +",left="+ winWidth/2 +",width="+width+",height="+height);	
		op_view = window.open("", winName, "top="+ winHeight/2 +",left="+ winWidth/2 +",width="+width+",height="+height+","+winOpt);
//	}
	
	$(frm).attr("target", winName);
	$(frm).attr("action", url);
	$(frm).attr("method", "post");
	$(frm).submit();
	
	if( !PHUtil.isEmpty(op_view) ) op_view.focus();	//팝업창 항상 위로 뜨게.
	//PHFnc.log("[windowOpen] "+ $(frm).html());
	
	return op_view;
};

/**
Get 팝업펑션

url: URL주소
winName: 팝업창명
width: 팝업 넓이
height: 팝업 높이
winOpt: 팝업옵션 Object. (팝업속성, 파라미터등..)

2016.03.22. 이용수
**/
PHFnc.prototype.windowOpenGet = function(url, winName, width, height, winOpt) {
	var winHeight = (window.screen.height);
	var winWidth  = (window.screen.width);
	var op_view;
	
	//팝업위치 계산
	winWidth  = (winWidth-width);
	winHeight = (winHeight-height);
	
	//팝업Open
	op_view = window.open(url, "chatPop", "top="+ winHeight/2 +",left="+ winWidth/2 +",width="+width+",height="+height+","+winOpt);
	if( !PHUtil.isEmpty(op_view) ) op_view.focus();	//팝업창 항상 위로 뜨게.
	
	return op_view;
};

/**
form element 생성 
2016.03.23. 이용수

inParam: 파라미터(String, Object)
frmObj: form Object
**/
PHFnc.prototype.SetParamForm = function(inParam, attrObj, isNppfs) {
//	PHFnc.log("PHFnc.SetParamForm");
	//form 생성
	var params = "";
	var tmpItm = {};
	var frmObj = (attrObj == undefined)?null:attrObj[PHFnc.FORM];
	
	if( frmObj == undefined ) frmObj = $("<form></form>").appendTo("body");
	
    //Set param
	switch (typeof(inParam)) {
	case "undefined":
		break;
		
	case "string":
		if( PHUtil.isEmpty(inParam) ) break;
		inParam.split("&").forEach(function(item, index, array) {
			tmpItm = item.split("=", 2);
			params += "<input type='hidden' name='"+ PHUtil.nvl(tmpItm[0]) +"' value='"+ PHUtil.nvl(tmpItm[1]) +"' />";
		});
		// PHFnc.log("[PHFnc.SetParamForm]string="+ inParam);
		break;
		
	case "object":
		$.each(inParam, function(key, value){
			params += "<input type='hidden' name='"+ PHUtil.nvl(key) +"' value='"+ PHUtil.nvl(value) +"' />";
		});
		
		break;
	default:
		break;
	}
	//html append
	//PHFnc.log(params);
	$(frmObj).append(params);
	
	//키보드 보안 항목 복사
	if( isNppfs != false ) {
		PHFnc.log("isNppfs=true");
		$("form[id='"+PHConst.NPPFS_FORM+"']>div[class='"+PHConst.NPPFS_ELEMENTS+"']").appendTo(frmObj);
	}
	
	return frmObj;
};

/**
GET/POST Action

url : URL 주소
params  : 파라미터. (String, Object)
frmObj  : form Object. (submit할 form element를 지정하고 싶다면 해당 form을 넘기면 된다. (없다면 "" 로 전달)
attrObj : 오브젝트
method  : GET or POST (기본값은 POST 이다.)
 
2016.03.23. 이용수
**/
PHFnc.prototype.doAction = function(url, params, attrObj) {
	PHFnc.log("[PHFnc.doAction]url="+url);
	var method = "POST";
	var passYn = "Y";

	//서버에러코드 공통 체크
	if ( (!PHUtil.isEmpty(attrObj)) && ("passYn" in attrObj) ) passYn = attrObj["passYn"];
	//PHFnc.log("passYn="+passYn);
	//PHFnc.log("eCode="+g_eCode);
	if( (passYn == "N") && (g_eCode != "0") ) {
		PHFnc.alert(g_eMsg);
		return false;
	}
	
	if (attrObj != undefined) method = attrObj[PHFnc.METHOD];
	
	//doAction은 무조건적인 페이지 이동이므로, 로딩바 출력후의 로딩바 제거는 아무도 못한다.
	this.ajaxProgress = true;
	PHFnc.progressbar(true, true);
	this.ajaxProgress = false;
	
	if( PHUtil.nvl(method, "POST").toUpperCase() == "POST" ) {
		PHFnc.doPost(url, params, attrObj);
	} else {
		PHFnc.doGet(url, params, attrObj);
	}
};

PHFnc.prototype.doGet = function(url, params, attrObj) {
	PHFnc.log("PHFnc.doGet");
	var frm = PHFnc.SetParamForm(params, attrObj);
	var target = (attrObj == undefined)?"":attrObj[PHFnc.TARGET];
	
	$(frm).attr("action", url);
	$(frm).attr("target", target);
	$(frm).submit();
	//PHFnc.log("[doGet] "+ $(frm).html());
};

PHFnc.prototype.doPost = function(url, params, attrObj) {
	PHFnc.log("PHFnc.doPost");
	var frm = PHFnc.SetParamForm(params, attrObj);
	var target = (attrObj == undefined)?"":attrObj[PHFnc.TARGET];
	target = PHUtil.nvl(target, "_self");
	
	$(frm).attr("action", url);
	$(frm).attr("target", target);
	$(frm).attr("method", "POST");
	$(frm).submit();
	//PHFnc.log("[PHFnc.doPost] frm="+ $(frm).html());
	/*alert("[PHFnc.doPost] target="+ target);*/
};


// 레이어 팝업 오픈
// /pHubCMS/src/main/webapp/resources/common/pubJs/script.js
// script.js 의 popupNum_01() 함수 호출

PHFnc.prototype.layerPopOpen = function(layerId) {
	if(layerId == 1){
		popupNum_01();
	} else if(layerId == 2){
		popupNum_02();
	} else if(layerId == 3){
		popupNum_03();
	} else if(layerId == 4){
		popupNum_04();
	} else if(layerId == 5){
		popupNum_up();
	} else if(layerId == 6){
		popupNum_up2()
	}
};

PHFnc.prototype.layerPopClose = function(layerId) {
	$("#"+ layerId).closest(".popup").hide();
	$("#"+ layerId).hide();
	//$("body").css("overflow", "auto");
	if ($('.popup .cont').length){
		$('.popup .cont').scrollLock('disable');
	}
	if ($('.popup .clause-txt').length){
		$('.popup .clause-txt').scrollLock('disable');
	}
};

/**
레이어팝업 Open 펑션 - Show

popId : 팝업 Element ID
popWidth : Width
popHeight : Height
attrs : 팝업 Attrs ({})
 
2016.04.20. 이용수
**/
PHFnc.prototype.openPop = function(popId, popWidth, popHeight, attrs) {
	var width     = "700";
	var height    = "600";
	var tmpWidth  = "";
	var tmpHeight = "";
	var popObj    = $("#"+popId);
	
	//set width
	tmpWidth  = PHUtil.nvl($(popObj).find(".body").attr("width"));
	tmpHeight = PHUtil.nvl($(popObj).find(".body").attr("height"));
	
	//팝업 Type별로 size속성값을 가져오는 방법이 다르다. 
	if( tmpWidth == "" )  tmpWidth  = PHUtil.nvl($(popObj).find("pre.clause-txt").attr("width"));
	if( tmpHeight == "" ) tmpHeight = PHUtil.nvl($(popObj).find("pre.clause-txt").attr("height"));
	
	if( tmpWidth == "" )  tmpWidth  = PHUtil.nvl($(popObj).find("div.cont").attr("width"));
	if( tmpHeight == "" ) tmpHeight = PHUtil.nvl($(popObj).find("div.cont").attr("height"));	
	
	width  = PHUtil.nvl( PHUtil.nvl(popWidth,  tmpWidth),  width);
	height = PHUtil.nvl( PHUtil.nvl(popHeight, tmpHeight), height);
	
	//background size
	$(popObj).find(".bg").width($(document).width());
	$(popObj).find(".bg").height($(document).height());

	// 사이즈에 대한 마진값 설정
	$(popObj).find(" .body").css("width", width+"px");
	$(popObj).find(" .body").css("height", height+"px");
	
	var posX = parseInt($("#"+popId+" .body").width())/2;
	var posY = parseInt($("#"+popId+" .body").height())/2;	

	//var contHeight = PHUtil.nvl(popHeight, "744") - ($("#"+popId+" .body > .tit").height());
	var contHeight = height - ($("#"+popId+" .body > .tit").outerHeight());
	$(popObj).find(" .cont").css("height", PHUtil.nvl(contHeight, "602px"));
	
	$(popObj).find(" .body").css("margin-top", (-posY) +"px");
	$(popObj).find(" .body").css("margin-left", (-posX) +"px");
	
	// 약관 관련 추가
	if($(popObj).find(".body > div.btn-fix").length){
		var bodyH = $(popObj).find(".body").height();
		var titH = 74; // pop tit height
		var btnH = 56; // pop btn height
		
		$(popObj).find(".clause-txt").css("height", parseInt(bodyH - titH - btnH));
		$(popObj).find(".clause-txt").css("overflow", "auto");
		
		$(popObj).find("div.cont").css("height", parseInt(bodyH - titH - btnH));
		$(popObj).find("div.cont").css("overflow", "auto");		
	}
	
	//팝업 Open
	$(popObj).show();
	
	//$("body").css("overflow", "hidden");
	if($('.popup .cont').length){
		$('.popup .cont').scrollLock('enable');
	}
	if($('.popup .clause-txt').length){
		$('.popup .clause-txt').scrollLock('enable');
	}
};

/**
레이어팝업 Close 펑션 - hide

popId : 팝업 Element ID
 
2016.04.20. 이용수
**/
PHFnc.prototype.closePop = function(popId) {
	$("#"+popId).hide();
	//$("body").css("overflow", "auto");
	if($('.popup .cont').length){
		$('.popup .cont').scrollLock('disable');
	}
	if($('.popup .clause-txt').length){
		$('.popup .clause-txt').scrollLock('disable');
	}
};


/**
 * CONSOLE LOG
 *   *** 운영기 이관 시 펑션 내부 전체 주석처리 할 것! ***
 * @param str
 */
PHFnc.prototype.log = function(str){
	if( this.isLog ) {
		if(window.console == undefined){
			console = {log : function(){}};
		}
		console.log(str);
	}
};

/***********************************************************************************************************
 * AJAX 관련 펑션
 ***********************************************************************************************************/
/**
비동기 AJAX 통신 펑션

@param :
	PHFnc.prototype.ajaxASync 참조

2016.04.15. 이용수
**/
PHFnc.prototype.ajax = function(url, param, mType, dType, successFn, failFn, isProgress, isPc){
	this.ajaxASync(url, param, mType, dType, successFn, failFn, isProgress, isPc, true);
};

/**
AJAX 통신 펑션

@param :
	url - URL주소
	params - 파라미터
	mType - method (get or post)
	dType - dataType (xml, json, script, html, text)
	successFn - 성공시 처리 펑션(data, textStatus, jqXHR)
	failFn - 실패시 처리 펑션(data, textStatus, jqXHR)
	isProgress - 프로그래시브바 출력여부 (true or false)
	isPc - true :PC / false :MOBILE
	isAsync - 동기/ 비동기 여부 (동기:false, 비동기:true)

2016.04.07. 이용수
**/
PHFnc.prototype.ajaxASync = function(url, param, mType, dType, successFn, failFn, isProgress, isPc, isAsync){
	if(mType == null){
		mType = "POST";
	}

	if(isProgress != true){
		isProgress = false;
	}

	if(isPc != false){
		isPc = true;
	}
	
	if(isAsync != false){
		isAsync = true;
	}
	
	(isProgress?PHFnc.progressbar(true, isPc):undefined);
	
	PHFnc.log("------------------------------------------------");
	PHFnc.log("isProgress:"+isProgress);
	PHFnc.log("url:");
	PHFnc.log(url);
	PHFnc.log("param:");
	PHFnc.log(param);
	PHFnc.log("------------------------------------------------");
	
	//Call AJAX
	$.ajax({
		url			: url
		,data		: param
		,dataType	: dType
		,method		: mType
		,async      : isAsync
		,headers 	: {'PH_AJAX':'true'}
		//,beforeSend : (isProgress?PHFnc.progressbar(true, isPc):undefined)
	}).always(function(data, textStatus, jqXHR){
		try{
			PHFnc.log("AJAX STATUS :: "+ textStatus);
			PHFnc.log(data);
			if(textStatus == "success") { //AJAX 통신 성공
				if(dType == "json"){
					var rsCd  = data.eCode;
					var rsMsg = data.eMsg;
					
					if (rsCd == "0") { //서버 처리 성공
						if(successFn != null || typeof(successFn)=="function"){
							successFn(data, textStatus, jqXHR);
						}
					} else { //서버 처리 실패
						if(failFn != null || typeof(failFn)=="function"){
							failFn(data, textStatus, jqXHR);
						}else{
							PHFnc.ajaxError(rsMsg, null);
						}					
					}					
				}else if(dType == "html"){
					if(successFn != null || typeof(successFn)=="function"){
						successFn(data, textStatus, jqXHR);
					}					
				}
				
			} else { //AJAX 통신 실패
				PHFnc.log("AJAX ERROR status :::::: "+ data.status);
				if(data.status == 901 ){
					PHFnc.alert("SESSION TIME OUT.");
					PHFnc.redirect('/index.jsp');
					
				}else if(data.status == 930 ){
					PHFnc.alert("다시 로그인 해주세요.");
					PHFnc.redirect('/index.jsp');
					
				}else if(data.status == 979 ){
					PHFnc.alert("로그인 정보를 확인 할 수 없습니다.");
					PHFnc.redirect('/index.jsp');
					
				}else if(data.status == 998 ){
					PHFnc.alert("영업일이 아닙니다.");
					
				}else if(data.status == 980 ){
					PHFnc.alert("허용된 접근이 아닙니다.");
					
				}else if(data.status == 909 ){
					PHFnc.alert("허용된 도메인이 아닙니다.");
					
				}else if(data.status == 906 ){
					PHFnc.alert("허용된 권한이 없습니다.");					
					
				}else{
					if(failFn != null || typeof(failFn)=="function"){
						failFn(data, textStatus, jqXHR);
					}else{
						PHFnc.ajaxError(null, null);
					}	
				}
			}
		}catch(J){
			PHFnc.log(J);
			PHFnc.ajaxError(null, data);
		};
		
		if (isProgress) PHFnc.progressbar(false, isPc);
	});
};

/**
AJAX 통신하는 동안의 프로그레시브바 처리 펑션
@param : 
	stat - 프로그레 시작/종료 여부 (true or false)
	isProgress - 프로그레시브바 출력여부 (true or flase)

2016.04.07. 이용수
**/
PHFnc.prototype.progressbar = function(stat, isPc){
	
	//로딩바 상태변경 여부가 false 이면 리턴.
	if( !this.ajaxProgress ) return;
	
	if( stat ){
		$(".loader").show();
		PHFnc.log("PROGRESS START =========>>>>>");
		
	}else{
		$(".loader").hide();
		PHFnc.log("PROGRESS END =========>>>>>");
	}
};

/**
ajaxError 펑션
@param : 
	msg - 알림창 메시지

2016.04.07. 이용수
**/
PHFnc.prototype.ajaxError = function(msg, jqXHR){
	PHFnc.ajaxProgress = true;
	if(msg !=null && msg != ""){
		PHFnc.alert(msg);
	}else{
//		alert("서비스 이용에 불편을 드려 죄송합니다.\n잠시 후 다시 이용하여 주시기 바랍니다.\n\n"+ jqXHR);
		PHFnc.alert("서비스 이용에 불편을 드려 죄송합니다.\n잠시 후 다시 이용하여 주시기 바랍니다.");
	}
};

/**
redirect 펑션
@param : 
	url - redirect url

2016.04.07. 이용수
**/
PHFnc.prototype.redirect = function(url){
	$(location).attr("href", url);
	PHFnc.submitLoading();
};

/**
로딩바 객체 생성
@param : 

2016.04.07. 이용수
**/
PHFnc.prototype.submitLoading = function(){
	this.progressbar(true);
};

/**
로딩바 객체 소멸
@param : 

2016.04.07. 이용수
**/
PHFnc.prototype.submitLoadingDestory = function(){
	this.progressbar(false);
};

/**
 * @REM - alert Layer 버전
 * @PARAM msg:alert 메시지, fnc:확인 버튼 클릭 후 실행될 펑션
 */
PHFnc.prototype.alert = function(msg, fnc) {
	//PHFnc.log(msg);
	alert(msg);
};

PHFnc.prototype.leadingZero = function(num, size) {
	var s = num + "";
	while (s.length < size) {
		s = '0' + s;
	}
	return s;
};

PHFnc.prototype.TARGET = "target";
PHFnc.prototype.FORM   = "form";
PHFnc.prototype.ACTION = "action";

// 클래스 생성
var PHFnc = new PHFnc();