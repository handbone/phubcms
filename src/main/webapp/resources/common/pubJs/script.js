$(document).ready(function() {

    /**
     *  UI 스크립트 init
     */

    project.init();

});



//textarea height auto로 맞추기

function resize(obj) {
    obj.style.height = "1px";
    obj.style.height = (12+obj.scrollHeight)+"px";
}

//jqGrid 버튼 팝업띄우기

function dbutton(cellvalue,options,rowobject){
    return '<input type="button" class="table_btn" onclick="popupNum_01()" value="확인" />';
}
function mbutton(cellvalue,options,rowobject){
    return '<input type="button" class="table_btn" onclick="popupNum_02()" value="수정" />';
}

//loader 띄우기

function loader(){
    $(".loader").show();
}
function loader_close(){
    $(".loader").hide();
}

//상단버튼 팝업띄우기

function popupNum_01(){
    jQuery.fn.center = function () {
        this.css("position","absolute");
        this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px");
        this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
        return this;
    }
    $("#myPopup").show();
    $(".popup_content").show();
    $(".popup_content").center();
}

function popupNum_02() {
    jQuery.fn.center = function () {
        this.css("position", "absolute");
        this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px");
        this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
        return this;
    }
    $("#myPopup").show();
    $(".popup_content2").show();
    $(".popup_content2").center();

}
    function popupNum_03() {
        jQuery.fn.center = function () {
            this.css("position", "absolute");
            this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px");
            this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
            return this;
        }
        $("#myPopup").show();
        $(".popup_content3").show();
        $(".popup_content3").center();
    }

    function popupNum_04() {
        jQuery.fn.center = function () {
            this.css("position", "absolute");
            this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px");
            this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
            return this;
        }
        $("#myPopup").show();
        $(".popup_content4").show();
        $(".popup_content4").center();
    }


    function popupNum_up() {
        jQuery.fn.center = function () {
            this.css("position", "absolute");
            this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px");
            this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
            return this;
        }
        $("#myPopup_front").show();
        $(".popup_front").show();
        $(".popup_front").center();
    }

function popupNum_up2() {
    jQuery.fn.center = function () {
        this.css("position", "absolute");
        this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px");
        this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
        return this;
    }
    $("#myPopup_front").show();
    $(".popup_front2").show();
    $(".popup_front2").center();
}


    var project = {
        init: function () {
            project.leftSlide();
            project.deps();
            project.jqWidth();
            project.jqWidth2();
            project.popup();
            project.popup2();

        },

        deps: function () {
            $(".deps2").click(function () {
                var self = $(this).next(".deps3")
                if (self.is(":hidden")) {
                    self.slideDown("slow");
                } else {
                    self.slideUp("slow");
                }
            });
        },

        leftSlide: function () {
            $("#menu_toggle").click(function () {
                $("#dash_board").toggleClass("active", 1000, 'easeOutExpo');
                if ($("#dash_board").hasClass('active')) {
                    jQuery("#jqGrid").jqGrid('setGridWidth', $("#content").width() + 250);
                } else {
                    jQuery("#jqGrid").jqGrid('setGridWidth', $("#content").width() - 250);
                }
            });

        },

        // jqGrid 가로크기
               jqWidth: function () {
            $(window).resize(function () {
                jQuery("#jqGrid").jqGrid('setGridWidth', $("#content").width());
                //$("#" + gridoption.id).setGridWidth($(this).width() * .100);
            });
        },

        // jqGrid 가로크기
        jqWidth2: function () {
            $(window).resize(function () {
                jQuery("#jqGrid2").jqGrid('setGridWidth', $("#content_p").width());
                //$("#" + gridoption.id).setGridWidth($(this).width() * .100);
            });
        },


        //팝업창닫기
        popup: function () {
            $("#myPopup").click(function () {
                $("#myPopup").css("display", "none");
                $(".popup_content").css("display", "none");
                $(".popup_content2").css("display", "none");
                $(".popup_content3").css("display", "none");
                $(".popup_content4").css("display", "none");
            });
            $(".top_close").click(function () {
                $("#myPopup").css("display", "none");
                $(".popup_content").css("display", "none");
                $(".popup_content2").css("display", "none");
                $(".popup_content3").css("display", "none");
                $(".popup_content4").css("display", "none");
            });
            $(".btn_cancel").click(function () {
                $("#myPopup").css("display", "none");
                $(".popup_content").css("display", "none");
                $(".popup_content2").css("display", "none");
                $(".popup_content3").css("display", "none");
                $(".popup_content4").css("display", "none");
            });
            $(".btn_close").click(function () {
                $("#myPopup").css("display", "none");
                $(".popup_content").css("display", "none");
                $(".popup_content2").css("display", "none");
                $(".popup_content3").css("display", "none");
                $(".popup_content4").css("display", "none");
            });
            //배치작업이력 - 이중 팝업창 중 하나만 닫기
            $(".btn_close4").click(function () {
                $("#myPopup").css("display", "none");
                $(".popup_content4").css("display", "none");
                
            });
            //배치작업이력 - 이중 팝업창 중 하나만 닫기
            $(".top_close4").click(function () {
                $("#myPopup").css("display", "none");
                $(".popup_content4").css("display", "none");
                
            });
        },


        //이중팝업창닫기
        popup2: function () {
            $("#myPopup_front").click(function () {
                $("#myPopup_front").css("display", "none");
                $(".popup_front").css("display", "none");
                $(".popup_front2").css("display", "none");
            });
            $(".top_close2").click(function () {
                $("#myPopup_front").css("display", "none");
                $(".popup_front").css("display", "none");
                $(".popup_front2").css("display", "none");
            });
            $(".btn_cancel2").click(function () {
                $("#myPopup_front").css("display", "none");
                $(".popup_front").css("display", "none");
                $(".popup_front2").css("display", "none");
            });
            $(".btn_close2").click(function () {
                $("#myPopup_front").css("display", "none");
                $(".popup_front").css("display", "none");
                $(".popup_front2").css("display", "none");
            });
        },

    }


var lib = {
    init : function () {
        lib.datepicker();
    },

    // 사이트 슬라이드
    datepicker : function () {
        $("#datepicker").datepicker();

    }

}








