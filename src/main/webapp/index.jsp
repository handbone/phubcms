<%@  page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<script type="text/javascript" src="/resources/common/jQuery/jquery.min.js"></script>
<script type="text/javascript" src="/resources/common/js/ph-fnc.js"></script>
<script type="text/javascript" src="/resources/common/js/ph-util.js"></script>
<script type="text/javascript" src="/resources/common/js/ph-const.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	PHFnc.doAction('/login/login.do');
});
</script>
</head>
<body>

</body>
</html>