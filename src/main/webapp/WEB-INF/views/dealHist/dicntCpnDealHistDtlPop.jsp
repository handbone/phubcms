<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%--
 **********************************************************************************************
 * @desc		: 단말할인권 거래내역 상세팝업
 * @FileName 	: /pHubCMS/src/main/webapp/WEB-INF/views/dealHist/dicntCpnDealHistDtlPop.jsp
 * @author 		: bmg
 * @since 		: 2019.03.05
 * @version 	: 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.03.05   bmg        최초생성
 **********************************************************************************************
dicntCpnDealHistView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>
<div class="popup_content">
	<div class="popup_title">단말할인권 상세거래내역<a href="#!"><div class="top_close"></div></a></div>
	<table id="popTable1" class="info_table" border="0" cellpadding="0" cellspacing="0">
		<thead>
			<tr>
				<th>포인트허브<br>거래번호</th>
				<th>고객ID</th>
				<th>고객명</th>
				<th>사용<br>포인트</th>
				<th>전환금액</th>
				<th>상품결제금액</th>
				<th>거래구분</th>
				<th>PG사명</th>
				<th>취소여부</th>
				<th>거래일시</th>
			</tr>
		</thead>
		<tbody />
	</table>
	<table id="popTable2" class="detail_table" border="0" cellpadding="0" cellspacing="0">
		<thead>
			<tr>
				<th>NO</th>
				<th>포인트제공처</th>
				<th>포인트별거래번호</th>
				<th>사용포인트</th>
				<th>포인트전환율</th>
				<th>포인트전환금액</th>
			</tr>
		</thead>
		<tbody />
	</table>
	<table id="popTable3" class="info_table" border="0" cellpadding="0" cellspacing="0">
		<thead>
			<tr>
				<th>할인권번호</th>
				<th>KT고객ID</th>
				<th>개통자<br>고객ID</th>
				<th>만료일자</th>
				<th>쿠폰상태</th>
				<th>발행일시</th>
				<th>사용일시</th>
				<th>발행/사용<br>취소일시</th>
				<th>발행취소</th>
				<th>사용취소</th>
			</tr>
		</thead>
		<tbody />
	</table>
	<a href="#!"><div class="btn_close">닫기</div></a>
	<!-- 상세거래내역 끝-->
</div>