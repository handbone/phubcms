<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
 **********************************************************************************************
 * @desc : 고객 정보 관리 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/dealHist/custInfoViewJs.jsp
 * @author ojh
 * @since 2018.08.21
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.21    ojh        최초생성
 * </pre>
 **********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
$(document).ready(function () {
	
//jQgrid 초기화
	var colNames = ['No', 'ID', '이름','핸드폰번호','통신사','클립멤버<br>여부','클립가입일','클립탈퇴일','가입채널','약관동의<br>내역'];
	var colModel = [{ label: 'No' ,name: 'no' ,width: cWidth * 0.05 ,key:true }
		,{ label: 'ID'			,name: 'cust_id'  		,width: cWidth * 0.10 	}
		,{ label: '이름'		,name: 'cust_nm'  		,width: cWidth * 0.08	}
		,{ label: '핸드폰번호' 	,name: 'cust_ctn' 		,width: cWidth * 0.12	}
		,{ label: '통신사' 		,name: 'telecom_ind' 	,width: cWidth * 0.06	}
		,{ label: '클립멤버여부',name: 'clip_mmbr_yn' 	,width: cWidth * 0.06	}
		,{ label: '클립가입일' 	,name: 'clip_sbsc_dt'  	,width: cWidth * 0.12	}
		,{ label: '클립탈퇴일' 	,name: 'clip_rtr_dd'  	,width: cWidth * 0.12	}
		,{ label: '가입채널' 	,name: 'cprt_cmpn_nm'  	,width: cWidth * 0.12	}
		,{ label: '약관동의내역',name: ''  				,width: cWidth * 0.06	,formatter:dbutton}
	];
	PHJQg.load('jqGrid','jqGridPager',colNames,colModel);
	//엑셀다운로드
	$("#btn_excel").on("click", function(){
		var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");	
		if(conf){		
			var postData = {
				CUST_ID : $("#in_id").val()
				, CUST_NM 		: $("#in_nm").val()
				, CUST_CTN 		: $("#in_pn").val()
				, CLIP_MMBR_YN 	: $("#sel_mYN").val()
				, PG_DEAL_NO 	: $("#in_pgDealNo").val()
				, CPRT_CMPN_NM 	: $("#in_cprt_cmpn_nm").val()
			};
			var colNames = ['No', 'ID', '이름','핸드폰번호','통신사','클립멤버여부','클립가입일','클립탈퇴일','가입채널'];
			var colModel = [{ label: 'No' ,name: 'no'	}
				,{ label: 'ID'			,name: 'cust_id'		}
				,{ label: '이름'		,name: 'cust_nm'		}
				,{ label: '핸드폰번호' 	,name: 'cust_ctn'		}
				,{ label: '통신사' 		,name: 'telecom_ind'	}
				,{ label: '클립멤버여부',name: 'clip_mmbr_yn'	}
				,{ label: '클립가입일' 	,name: 'clip_sbsc_dt'	}
				,{ label: '클립탈퇴일' 	,name: 'clip_rtr_dd'	}
				,{ label: '가입채널' 	,name: 'cprt_cmpn_nm'	}
			];
			PHJQg.excel('jqGridExcelDiv','${ViewRoot}/dealHist/jgCustInfoExcel.do',postData,colNames,colModel,'고객정보관리');
		}
	}) 
});
//조회시 reload
function search(){	
	var postData = {
		CUST_ID : $("#in_id").val()
		, CUST_NM : $("#in_nm").val()
		, CUST_CTN : $("#in_pn").val()
		, CLIP_MMBR_YN : $("#sel_mYN").val()
		, PG_DEAL_NO : $("#in_pgDealNo").val()
		, CPRT_CMPN_NM : $("#in_cprt_cmpn_nm").val()
	};
	PHJQg.reload('jqGrid','jqGridPager','${ViewRoot}/dealHist/jgCustInfo.do',postData);
	
}
//formatter 버튼 만들어 주기 클릭시 no전달
function dbutton(cellvalue,options,rowobject){
    return '<input type="button" class="table_btn" onclick="popClick('+rowobject.no+')" value="확인" />';
}
//클릭시 상세 정보 창에 data 셋팅
function popClick(data){
	//요청 내역 셋팅
	var row = $("#jqGrid").jqGrid('getRowData',data);	
	var hText = "<td>"+row.cust_id+"</td>";
	hText += "<td>"+row.cust_nm+"</td>";
	hText += "<td>"+row.cust_ctn+"</td>";
	hText += "<td>"+row.cprt_cmpn_nm+"</td>";
	$("#Dtl").html(hText);		
	
	//약관 내역 aJax호출하여 가져오기
	var params = {
		CUST_ID : row.cust_id
	};
	PHFnc.ajax("${ViewRoot}/dealHist/ajaxAgrHist.do",params,"POST","json",function(data, textStatus, jqXHR) {
		//Row 개수에 따라 td태그 생성하여 Html 변환
		var hText = "";		
		for(var i=0; i<data.rows.length; i++){
			hText += "<tr>";
			hText += "<td>"+data.rows[i].no+"</td>";
			hText += "<td>"+data.rows[i].cls_titl+"</td>";
			hText += "<td>"+data.rows[i].agre_yn+"</td>";
			hText += "<td>"+data.rows[i].mand_yn+"</td>";
			hText += "<td>"+data.rows[i].mdfy_dt+"</td>";
			hText += "</tr>";
		}
		$("#tBody").html(hText);
	},null,true,true,false);
	//팝업창 오픈
	PHFnc.layerPopOpen(1);
}

</script>