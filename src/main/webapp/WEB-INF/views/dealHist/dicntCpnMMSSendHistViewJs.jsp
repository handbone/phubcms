<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="com.olleh.pHubCMS.common.utils.SessionUtils"%> <%--
 **********************************************************************************************
 * @desc		: 단말할인권 조르기 문자발송이력 Js 
 * @FileName 	: /pHubCMS/src/main/webapp/WEB-INF/views/dealHist/dicntCpnMMSSendHistViewJs.jsp
 * @author 		: bmg
 * @since 		: 2019.03.05
 * @version 	: 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.03.05   bmg        최초생성
 **********************************************************************************************
--%><script type="text/javascript">
var cWidth = $("#content").width();
$(document).ready(function () {
	var colNames = ['No'
	                ,'메시지ID'
	                ,'메시지발송번호'
	                ,'포인트허브거래번호'
	                ,'요청고객명'
	                ,'요청고객<br>핸드폰번호'
	                ,'수신고객명'
	                ,'수신고객<br>핸드폰번호'
	                ,'전송일시'
	                ,'전송결과'];
	var colModel = [{ label: 'No', name: 'no', width: cWidth * 0.03, key:true }
		,{ label: '메시지ID'				,name: 'mms_msg_id'		,width: cWidth * 0.08 }
		,{ label: '메시지발송번호'			,name: 'mms_msg_no'		,width: cWidth * 0.16 }
		,{ label: '포인트허브거래번호'		,name: 'phub_tr_no'		,width: cWidth * 0.12 }
		,{ label: '요청고객명'				,name: 'send_cust_nm'	,width: cWidth * 0.08 }
		,{ label: '요청고객핸드폰번호'		,name: 'send_cust_ctn'	,width: cWidth * 0.10 }
		,{ label: '수신고객명'				,name: 'recv_cust_nm'	,width: cWidth * 0.08 }
		,{ label: '수신고객핸드폰번호'		,name: 'recv_cust_ctn'	,width: cWidth * 0.10 }
		,{ label: '전송일시'				,name: 'send_dt'		,width: cWidth * 0.12 }
		,{ label: '전송결과'				,name: 'send_rslt_msg'	,width: cWidth * 0.10 }
		
	];
	PHJQg.load('jqGrid','jqGridPager',colNames,colModel);

	var dt = new Date();	
    setDate(dt,dt);
    
    $('#in_cuCtn').keypress(function(e) {
    	var keycode = e.keyCode ? e.keyCode : e.which;
    	if ((keycode >= 48 && keycode <= 57) || (keycode >= 96 && keycode <= 105)) {
    		return true;
    	} else {
    		switch(keycode) {
    			case 8:		// BACKSPACE
    			case 9:		// TAB
    			case 45:	// -
    				return true;
    				break;
    			default:
    				e.preventDefault();
    		}
    	}
    }).keyup(function(e) {
    	var value = $(this).val().replace(/[^0-9-]/g,"");
    	$(this).val(value);
    });
    
  	// 엑셀다운로드
    $("#btn_excel").on("click", function() {
    	if (PHJQg.size('jqGrid') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
    	var postData = {
    		MMS_MSG_ID: $('#sel_mmsMsgId').val()
    		, PHUB_TR_NO: $('#in_trNo').val()
    		, SEND_CUST_NM: $('#in_sendCustNm').val()
    		, SEND_CUST_CTN: $('#in_sendCustCtn').val()
    		, RECV_CUST_NM: $('#in_recvCustNm').val()
    		, RECV_CUST_CTN: $('#in_recvCustCtn').val()
   		};
    	var colNamesExcel = $.map(colNames, function(o,i) {
   			return o.replace(/(<br>)/g,'');
    	});
    	PHJQg.excel('jqGridExcelDiv','${ViewRoot}/dealHist/jgDicntCpnMMSSendHistExcel.do',postData,colNamesExcel,colModel,'단말할인권조르기문자발송이력');
    });
});
// 조회 버튼 클릭 시
function search() {
	var postData = {
		MMS_MSG_ID: $('#sel_mmsMsgId').val()
		, PHUB_TR_NO: $('#in_trNo').val()
   		, SEND_CUST_NM: $('#in_sendCustNm').val()
   		, SEND_CUST_CTN: $('#in_sendCustCtn').val()
   		, RECV_CUST_NM: $('#in_recvCustNm').val()
   		, RECV_CUST_CTN: $('#in_recvCustCtn').val()
	};
	PHJQg.reload('jqGrid','jqGridPager','${ViewRoot}/dealHist/jgDicntCpnMMSSendHist.do',postData);
}

</script>