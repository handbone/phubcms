<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
 **********************************************************************************************
 * @desc : 약관동의내역 조회Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/dealHist/agrHistViewJs.jsp
 * @author ojh
 * @since 2018.09.28
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.28    ojh        최초생성
 * </pre>
 **********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
$(document).ready(function () {
	var colNames = ['No', 'ID', '이름','핸드폰번호','클립멤버<br>여부','약관명','약관동의일','가입채널'];
	var colModel = [{ label: 'No', name: 'no', width: cWidth * 0.05	,key:true }
		,{ label: 'ID'			,name: 'cust_id'  		,width: cWidth * 0.10 }
		,{ label: '이름'		,name: 'cust_nm'  		,width: cWidth * 0.06 }
		,{ label: '핸드폰번호' 	,name: 'cust_ctn' 		,width: cWidth * 0.10 }
		,{ label: '클립멤버여부',name: 'clip_mmbr_yn'	,width: cWidth * 0.06 }
		,{ label: '약관명' 		,name: 'cls_titl'  		,width: cWidth * 0.40	,align:'left' }
		,{ label: '약관동의일' 	,name: 'rgst_dt'	  	,width: cWidth * 0.12 }
		,{ label: '가입채널' 	,name: 'cprt_cmpn_nm'	,width: cWidth * 0.11 }
	];
	PHJQg.load('jqGrid','jqGridPager',colNames,colModel);
	
    setDate(null,null);
  	
    //엑셀다운로드
    $("#btn_excel").on("click", function() {
    	if (PHJQg.size('jqGrid') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
		if (!validateDate()) {
			return;
		}
    	var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");	
    	if(conf){		
    		var postData = {
    			CUST_ID 		: $("#in_id").val()
    			,CUST_NM 		: $("#in_nm").val()
    			,CUST_CTN 		: $("#in_ctn").val()
    			,CLIP_MMBR_YN 	: $("#sel_cYn").val()
    			,CLS_TITL		: $("#in_tt").val()
    			,CPRT_CMPN_NM 	: $("#in_cprt_cmpn_nm").val()
    			,START_DATE		: $('#start_date').val()
    			,END_DATE		: $('#end_date').val()
    		};
    		var colNames = ['No', 'ID', '이름','핸드폰번호','클립멤버여부','약관명','약관동의일','가입채널'];
    		var colModel = [{ label: 'No', name: 'no', width: cWidth * 0.07, key:true }
           		,{ label: 'ID'			,name: 'cust_id' }
           		,{ label: '이름'		,name: 'cust_nm'	,align:'left' }
           		,{ label: '핸드폰번호' 	,name: 'cust_ctn'	,align:'left' }
           		,{ label: '클립멤버여부',name: 'clip_mmbr_yn' }
           		,{ label: '약관명' 		,name: 'cls_titl'	,align:'left' }
           		,{ label: '약관동의일' 	,name: 'rgst_dt' }
           		,{ label: '가입채널' 	,name: 'cprt_cmpn_nm' }
           	];
    		PHJQg.excel('jqGridExcelDiv','${ViewRoot}/dealHist/jgAgrHistExcel.do',postData,colNames,colModel,'약관동의내역');
    	}
    });
});
//조회시 reload
function search() {
	if (!validateDate()) {
		return;
	}
	var postData = {
		CUST_ID 		: $("#in_id").val()
		,CUST_NM 		: $("#in_nm").val()
		,CUST_CTN 		: $("#in_ctn").val()
		,CLIP_MMBR_YN 	: $("#sel_cYn").val()
		,CLS_TITL		: $("#in_tt").val()
		,CPRT_CMPN_NM 	: $("#in_cprt_cmpn_nm").val()
		,START_DATE		: $('#start_date').val()
		,END_DATE		: $('#end_date').val()
	};
	PHJQg.reload('jqGrid','jqGridPager','${ViewRoot}/dealHist/jgAgrHist.do',postData);
}
//기간 조건 체크
function validateDate() {
	var sDate = $("#start_date").val();
	var eDate = $("#end_date").val();
	if (!PHUtil.isEmpty(sDate) && !PHValid.date(PHUtil.replaceAll(sDate,'-',''))) {
		alert('시작일자가 유효한 날짜 형식이 아닙니다');
		$('#start_date').val('');
		$('#start_date').focus();
		return false;
	}
	if (!PHUtil.isEmpty(eDate) && !PHValid.date(PHUtil.replaceAll(eDate,'-',''))) {
		alert('종료일자가 유효한 날짜 형식이 아닙니다');
		$('#end_date').val('');
		$('#end_date').focus();
		return false;
	}
	var startDate = new Date(sDate);
	var endDate = new Date(eDate);
	if (startDate.getTime() > endDate.getTime()) {		
		alert('뒤의 날짜는 앞의 날짜 이전으로 검색할 수 없습니다.')
		return false;
	}
	return true;
}
</script>