<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="com.olleh.pHubCMS.common.utils.SessionUtils"%> <%--
 **********************************************************************************************
 * @desc		: 단말할인권 거래내역 Js 
 * @FileName 	: /pHubCMS/src/main/webapp/WEB-INF/views/dealHist/dicntCpnDealHistViewJs.jsp
 * @author 		: bmg
 * @since 		: 2019.03.05
 * @version 	: 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.03.05   bmg        최초생성
 **********************************************************************************************
--%><script type="text/javascript">
var cWidth = $("#content").width();
$(document).ready(function () {
	var colNames = ['No'
	                ,'포인트허브거래번호'
	                ,'고객ID'
	                ,'고객명'
	                ,'사용포인트'
	                ,'상품명'
	                ,'사용처명'
	                ,'PG거래번호'
	                ,'취소<br>여부'
	                ,'원포인트허브거래번호'
	                ,'거래일시'
	                ,'할인권번호'
	                ,'할인권금액'
	                ,'KT고객<br>ID'
	                ,'개통자<br>여부'
	                ,'KOS거래번호'
	                ,'만료일자'
	                ,'쿠폰상태'
	                ,'발행일시'
	                ,'사용일시'
	                ,'발행/사용취소일시'
	                ,'거래구분'
	                ,'상세거래내역'
	                ,'포인트허브쿠폰번호'
	                ,'전환금액'
	                ,'상품결제금액'
	                ,'PG사명'
	                ,'거래구분명'
	                ,'개통자고객ID'];
	var colModel = [{ label: 'No', name: 'no', width: cWidth * 0.03, key:true }
		,{ label: '포인트허브거래번호'	,name: 'phub_tr_no'		,width: cWidth * 0.12 }
		,{ label: '고객ID'				,name: 'cust_id'		,width: cWidth * 0.10 }
		,{ label: '고객명'				,name: 'cust_nm'		,width: cWidth * 0.08 }
		,{ label: '사용포인트'			,name: 'ttl_pnt'		,width: cWidth * 0.08	,align:'right',formatter:caStr }
		,{ label: '상품명'				,name: 'goods_nm'		,width: cWidth * 0.15 }
		,{ label: '사용처명'			,name: 'cprt_cmpn_nm'	,width: cWidth * 0.10 }
		,{ label: 'PG거래번호'			,name: 'pg_deal_no'		,width: cWidth * 0.25 }
		,{ label: '취소<br>여부'		,name: 'cncl_yn'		,width: cWidth * 0.05 }
		,{ label: '원포인트허브거래번호',name: 'ori_phub_tr_no'	,width: cWidth * 0.13 }
		,{ label: '거래일시'			,name: 'deal_dt'		,width: cWidth * 0.12 }
		,{ label: '할인권번호'			,name: 'pin_no'			,width: cWidth * 0.10 }
		,{ label: '할인권금액'			,name: 'cpn_amt'		,width: cWidth * 0.10	,align:'right',formatter:'integer',formatoptions:{thousandsSeparator:','} }
		,{ label: 'KT고객<br>ID'		,name: 'kt_cust_id'		,width: cWidth * 0.10 }
		,{ label: '개통자<br>여부'		,name: 'own_yn'			,width: cWidth * 0.05 }
		,{ label: 'KOS거래번호'			,name: 'kos_tr_no'		,width: cWidth * 0.10 }
		,{ label: '만료일자'			,name: 'efct_end_dd'	,width: cWidth * 0.10 }
		,{ label: '쿠폰상태'			,name: 'cpn_stat_nm'	,width: cWidth * 0.08 }
		,{ label: '발행일시'			,name: 'cpn_isue_dt'	,width: cWidth * 0.12 }
		,{ label: '사용일시'			,name: 'cpn_use_dt'		,width: cWidth * 0.12 }
		,{ label: '발행/사용취소일시'	,name: 'cncl_dt'		,width: cWidth * 0.12 }
		,{ label: '거래구분'			,name: 'deal_ind'		,hidden: true }
		,{ label: '상세거래내역'		,name: ''				,width: cWidth * 0.08,	formatter:dbutton }
		,{ label: '포인트허브쿠폰번호'	,name: 'phub_cpn_no'	,hidden: true }
		,{ label: '전환금액'			,name: 'ttl_pnt_amt'	,hidden: true	,align:'right',formatter:caStr }
		,{ label: '상품결제금액'		,name: 'ttl_pay_amt'	,hidden: true	,align:'right',formatter:caStr }
		,{ label: 'PG사명'				,name: 'pg_cmpn_nm'		,hidden: true }
		,{ label: '거래구분명'			,name: 'deal_ind_nm'	,hidden: true }
		,{ label: '개통자고객ID'		,name: 'own_kt_cust_id'	,hidden: true }
	];
	PHJQg.load('jqGrid','jqGridPager',colNames,colModel);

	var dt = new Date();	
    setDate(dt,dt);
    
    // 핸드폰번호 입력 제한 정의
    $('#in_cuCtn').keypress(function(e) {
    	var keycode = e.keyCode ? e.keyCode : e.which;
    	if ((keycode >= 48 && keycode <= 57) || (keycode >= 96 && keycode <= 105)) {
    		return true;
    	} else {
    		switch(keycode) {
    			case 8:		// BACKSPACE
    			case 9:		// TAB
    			case 45:	// -
    				return true;
    				break;
    			default:
    				e.preventDefault();
    		}
    	}
    }).keyup(function(e) {
    	var value = $(this).val().replace(/[^0-9-]/g,"");
    	$(this).val(value);
    });
    
  	// 엑셀다운로드
    $("#btn_excel").on("click", function() {
    	if (PHJQg.size('jqGrid') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
    	if (!validateDate()) {
    		return;
    	}
    	var custCtn = PHUtil.replaceAll($("#in_cuCtn").val(),"-","");
    	var postData = { START_DATE: $("#start_date").val(), END_DATE: $("#end_date").val()
   			, DEAL_IND: $('#sel_ind').val()
   			, PG_CMPN_ID: $('#sel_pgId').val()
   			, CNCL_YN: $('#sel_cYn').val()
			, PG_CPRT_ID : $('#sel_cprtId').val()
   			, PIN_NO: $('#in_pinNo').val()
   			, CUST_CTN: PHUtil.replaceAll(custCtn,"/","")
   			, CUST_ID: $('#in_cuId').val()
   			, PHUB_TR_NO: $('#in_trNo').val()
   			, CPN_STAT_CD: $('#sel_cpnStatCd').val()
   		};
    	var colNamesExcel = $.map(colNames, function(o,i) {
    		return i < 22 ? o.replace(/(<br>)/g,'') : null;
    	});
    	var colModelExcel = $.map(colModel, function(o,i) {
    		return i < 22 ? o : null;
    	});
    	PHJQg.excel('jqGridExcelDiv','${ViewRoot}/dealHist/jgDicntCpnDealHistExcel.do',postData,colNamesExcel,colModelExcel,'단말할인권거래내역');
    });
});
// 상세 버튼 정의
function dbutton(cellvalue,options,rowobject) {
	return '<input type="button" class="table_btn" onclick="popClick('+rowobject.no+')" value="확인" />';
}
// 금액 표시
function caStr(cellvalue,options,rowobject){
	cellvalue = cellvalue != null ? cellvalue+'' : cellvalue;
	if(rowobject.deal_ind != 'PA'){
		if (Number(cellvalue)) {
			return "-"+PHUtil.setComma(cellvalue);
		} else {
			return "";
		}
	} else{
		return PHUtil.nvl(PHUtil.setComma(cellvalue),'');
	}
}
//조회기간 제약 // 제약 Max한달 
function validateDate() {
	var sDate = $("#start_date").val();
	var eDate = $("#end_date").val();
	if (!PHUtil.isEmpty(sDate) && !PHValid.date(PHUtil.replaceAll(sDate,'-',''))) {
		alert('시작일자가 유효한 날짜 형식이 아닙니다');
		$('#start_date').val('');
		$('#start_date').focus();
		return false;
	}
	if (!PHUtil.isEmpty(eDate) && !PHValid.date(PHUtil.replaceAll(eDate,'-',''))) {
		alert('종료일자가 유효한 날짜 형식이 아닙니다');
		$('#end_date').val('');
		$('#end_date').focus();
		return false;
	}
	var startDate = new Date(sDate);
	var endDate = new Date(eDate);
	if (startDate.getTime() > endDate.getTime()) {		
		alert('뒤의 날짜는 앞의 날짜 이전으로 검색할 수 없습니다.')
		return false;
	} else {
		var valDate = new Date(sDate);
		valDate.setMonth(valDate.getMonth()+1);
		if (endDate.getTime() > valDate.getTime()) {
			alert('검색 기간은 한달을 넘을 수 없습니다.');
			return false;
		}
	}
	return true;
}
// 조회 버튼 클릭 시
function search() {
	if (!validateDate()) {
		return;
	}
	var custCtn = PHUtil.replaceAll($("#in_cuCtn").val(),"-","");
	var postData = { START_DATE: $("#start_date").val(), END_DATE: $("#end_date").val()	
		, DEAL_IND: $('#sel_ind').val()
 		, PG_CMPN_ID: $('#sel_pgId').val()
 		, CNCL_YN: $('#sel_cYn').val()
		, PG_CPRT_ID : $('#sel_cprtId').val() 
 		, PIN_NO: $('#in_pinNo').val()
 		, CUST_CTN: PHUtil.replaceAll(custCtn,"/","")
 		, CUST_ID: $('#in_cuId').val()
 		, PHUB_TR_NO: $('#in_trNo').val()
 		, CPN_STAT_CD: $('#sel_cpnStatCd').val()
	};
	PHJQg.reload('jqGrid','jqGridPager','${ViewRoot}/dealHist/jgDicntCpnDealHist.do',postData);
}
// 클릭시 상세 정보 창에 data 셋팅
function popClick(data) {
	var row = $("#jqGrid").jqGrid('getRowData',data);
	$('#popTable1 tbody').empty();
	$(	'<tr>'+
			'<td>'+ row.phub_tr_no +'</td>'+
			'<td>'+ row.cust_id +'</td>'+
			'<td>'+ row.cust_nm +'</td>'+
			'<td>'+ row.ttl_pnt +'</td>'+
			'<td>'+ row.ttl_pnt_amt +'</td>'+
			'<td>'+ row.ttl_pay_amt +'</td>'+
			'<td>'+ row.deal_ind_nm +'</td>'+
			'<td>'+ row.pg_cmpn_nm +'</td>'+
			'<td>'+ row.cncl_yn +'</td>'+
			'<td>'+ popDateValue(row.deal_dt) +'</td>'+
		'</tr>'
	).appendTo('#popTable1 tbody');
	$('#popTable3 tbody').empty();
	$(	'<tr>'+
			'<td>'+ row.pin_no +'</td>'+
			'<td>'+ row.kt_cust_id +'</td>'+
			'<td>'+ row.own_kt_cust_id +'</td>'+
			'<td>'+ row.efct_end_dd +'</td>'+
			'<td>'+ row.cpn_stat_nm +'</td>'+
			'<td>'+ popDateValue(row.cpn_isue_dt) +'</td>'+
			'<td>'+ popDateValue(row.cpn_use_dt) +'</td>'+
			'<td>'+ popDateValue(row.cncl_dt) +'</td>'+
			'<td>'+
				(
					(row.deal_ind=='PA' && row.cncl_yn=='N')
						? '<button class="table_btn" onclick="popDicntCpnCancel(this,\'A01\',\'N\');">내부<br>취소</button>&nbsp;<button class="table_btn" onclick="popDicntCpnCancel(this,\'A01\',\'Y\');">연동<br>취소</button>'
						: 	''
				) +
			'</td>'+
			'<td>'+
				(
					(row.deal_ind=='PA' && row.cncl_yn=='N')
						? '<button class="table_btn" onclick="popDicntCpnCancel(this,\'A02\',\'N\');">내부<br>취소</button>'
						: ''
				) +			
			'</td>'+
		'</tr>'
	).data('item',row).appendTo('#popTable3 tbody');
	var params = {
		PHUB_TR_NO : row.phub_tr_no
	};
	PHFnc.ajax("${ViewRoot}/dealHist/ajaxDicntCpnDealHistDtl.do",params,"POST","json",function(data, textStatus, jqXHR) {
		$('#popTable2 tbody').empty();
		if (data.rows == null) {
			return;
		}
		for (var i=0; i<data.rows.length; i++) {
			var item = data.rows[i];
			var prefix = (item.deal_ind=='CA' ? '-' : '');
			$(	'<tr>'+
					'<td>'+ item.no +'</td>'+
					'<td>'+ item.pnt_nm +'</td>'+
					'<td>'+ item.pnt_tr_no +'</td>'+
					'<td style="text-align:right">'+ prefix+PHUtil.setComma(item.ans_pnt) +'</td>'+
					'<td>'+ item.pnt_exch_rate +'</td>'+
					'<td style="text-align:right">'+ prefix+PHUtil.setComma(item.ans_pnt_amt) +'</td>'+
				'</tr>'
			).appendTo('#popTable2 tbody');
		}
	},null,true,true,false);
	
	PHFnc.layerPopOpen(1);
}
// 팝업 날짜 개행 표시
function popDateValue(str) {
	return str.replace(' ','<br>');
}
// 취소 처리
function popDicntCpnCancel(obj,type,yn) {
	var confirm_message = '';
	switch (type) {
		case 'A01':
			confirm_message = '해당 할인권의 발행 취소하시겠습니까?';
			break;
		case 'A02':
			confirm_message = '해당 할인권의 사용 취소하시겠습니까?';
			break;
		default:
	}
	if (confirm_message != '' && confirm(confirm_message)) {
		var item = $(obj).parents('tr:eq(0)').data('item');
		var params = {
			phub_tr_no: item.phub_tr_no
			, pg_tr_no: item.pg_deal_no
			, tr_div: 'CA'
			, if_yn: yn
			, cncl_ind: 'A'
			, cncl_yn: 'A'
			, tr_id: item.phub_tr_no
			, trade_code: type
			, pin_no: item.pin_no
		};
		PHFnc.ajax("${ViewRoot}/dealHist/ajaxDicntCpnCancel.do",params,"POST","json",
			function(data, textStatus, jqXHR) {
				alert("처리에 성공하였습니다.");
				$(".top_close").click();
				search();
			},
			function(data, textStatus, jqXHR) {
				if (!PHUtil.isEmpty(data.eMsg)) {
					PHFnc.alert(data.eMsg);	
				} else {
					PHFnc.ajaxError(null,jqXHR);
				}
			},
		true,true,false);
	}
}

</script>