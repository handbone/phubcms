
<%@  page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
 **********************************************************************************************
 * @desc : 조르기 내역 조회 
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/dealHist/fmlySupotView.jsp
 * @author kimht
 * @since 2019.05.22
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2019.05.22    kimht        최초생성
 * 2019.05.28    kimht        include 수정
 **********************************************************************************************
--%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<%-- global --%>
	<jsp:include page="/WEB-INF/views/include/globalVar.jsp" flush="false" />
	<%-- head --%>
	<jsp:include page="/WEB-INF/views/include/incHead.jsp" flush="false" />
</head>
<body>
<!-- loader 시작-->
<div class="loader" onclick="loader_close()">
    <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
</div>
<!-- loader 끝-->
<div id="wrap">	
<header>
	<%-- header --%>
	<jsp:include page="/WEB-INF/views/include/incHeader.jsp" flush="false" />
	<style>
	.search_item li
	{
		padding-right : 30px;
	
	}
	.search_item input
	{
		padding-left : 15px;
	
	}
	.search_item .search-first-child li
	{
		padding-left : 25px;
	}
	</style>
</header>

    <!-- 레프트 메뉴 해당페이지 표시 -->
    <style>.${sessionScope.USER_MENU.menu_id}{ color: #fff !important; background: #3f4759 !important; border-left: 3px solid #fff; cursor: hand;} </style>
    <!-- 레프트 메뉴 해당페이지 표시 끝 -->

<!-- dash_board 시작-->
<section id="dash_board">
    <!--side_menu 시작-->
	<jsp:include page="/WEB-INF/views/include/incMenu.jsp" flush="false" />
    <!--side_menu 끝-->

    <!--contents 시작-->
    <div class="contents mCustomScrollbar">
        <div class="page_title"><div class="page_name">${sessionScope.USER_MENU.menu_nm}</div><div class="page_location"><i class="fas fa-home"></i>${sessionScope.USER_MENU.mnu_path}</div></div>
        <div class="subject">
        	<div class="page_search">
                <table class="search_tb" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="search_item">
                            <ul class="search_first_child">
			                    <li style="padding-right : 10px; margin-right:2px;">등록일자</li>
			                    <li><input type="text" name="name" id="start_date" autocomplete="off" readonly> ~ <input type="text" name="name" id="end_date" autocomplete="off" readonly></li>
			                    <li style="padding-left : 35px;">포인트허브 거래번호</li>
			                    <li> <input type="text" name="name" id="in_phubTrNo" maxlength="20" autocomplete="off"></li>
			                    <li>이름</li>
			                    <li><input type="text" name="name" id="in_nm" maxlength="20" autocomplete="off"></li>
							</ul>
							<ul>
							    <li>
							    할인권번호 <input type="text" name="name" id="in_cpnNo" maxlength="20" autocomplete="off">
							    </li>
								<li>요청상태코드
								 	<select id="in_reqStatCd">
			                            <option value="all">전체</option>
				                        <c:forEach items="${CPN_REQ_STAT}" var="code">
			                           		<option value="${code.dtlCd}">${code.dtlCdNm}</option>  <%-- ${code.dtlCdNm} ${code.dtlCd} --%>
			                           	</c:forEach>
			                        </select>
								 </li>
			                    <li>진행상태</li>
			                    <li style="padding-right:20px;">
								 	<select id="in_prgrStat">
			                            <option value="all">전체</option>
				                        <c:forEach items="${CPN_REQ_PRGRS}" var="code">
			                           		<option value="${code.dtlCd}">${code.dtlCdNm}</option>
			                           	</c:forEach>
			                        </select>
								 </li>
			                	 <li>쿠폰상태
			                         <select id="in_cpnStatCd">
			                            <option value="all">전체</option>
				                        <c:forEach items="${CPN_STAT_IND}" var="code">
			                           		<option value="${code.dtlCd}">${code.dtlCdNm}</option>
			                           	</c:forEach>
			                       </select>
			                    </li>			
			                </ul>
                        </td>
                        <td class="btn_search">
                            <button type="button" onClick="search()">조회</button>
                        </td>
                    </tr>
                </table>
            </div>            
            <div class="page_contents">
                <div class="btn_excel" id="excel_form"><button id="btn_excel">엑셀다운</button></div>
                <div id="content">
                    <table id="jqGrid"></table>                
                    <div id="jqGridPager"></div>
                    <div id="jqGridExcelDiv" style="display:none">
                		<table id="jqGridExcel" ></table>
                	</div>
                </div>
            </div>
        </div>
        <!-- 스크롤 때문에 여백 있어야함 -->
        <br>
        <br>
        <br>
        <br>
        <br>

        </div>
    <!--contents 끝-->
</section>
<!-- dash_board 끝--> 

    <!-- footer 시작-->
    <jsp:include page="/WEB-INF/views/include/incFooter.jsp" flush="false" />
    <section class="footer">	 
    </section>
    <!-- footer 끝-->
    
    <%-- js --%>
	<jsp:include page="./fmlySupotViewJs.jsp" flush="false" />
	
</body>
</html>