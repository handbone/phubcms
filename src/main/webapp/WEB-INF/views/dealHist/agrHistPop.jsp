<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="popup_content">
<%--
 **********************************************************************************************
 * @desc : 약관 동의 상세 팝업
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/dealHist/agrHistPop.jsp
 * @author ojh
 * @since 2018.08.14
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.14    ojh        최초생성
 *
 **********************************************************************************************
custInfoView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>
        <div class="popup_title">약관동의내역<a href="#!"><div class="top_close"></div></a></div>
        <table class="info_table" border="0" cellpadding="0" cellspacing="0">
            <tbody>
            <tr>
                <th>ID</th>
                <th>이름</th>
                <th>핸드폰번호</th>
                <th>가입채널</th>
            </tr>
            <tr id="Dtl">
            </tr>
            </tbody>
        </table>

        <table class="detail_table" border="0" cellpadding="0" cellspacing="0">
            <thead>
            <tr>
                <th>NO</th>
                <th>약관타이틀</th>
                <th>동의여부</th>
                <th>필수여부</th>
                <th>동의일</th>
            </tr>
            </thead>
            <tbody id="tBody">
            </tbody>
        </table>
        <a href="#!"><div class="btn_close">닫기</div></a>
        <!-- 상세거래내역 끝-->
    </div>
    <div id="myPopup" class="popup">
    </div>