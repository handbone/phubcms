<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
 **********************************************************************************************
 * @desc : 조르기 내역 조회Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/dealHist/fmlySupotViewJs.jsp
 * @author kimht
 * @since 2018.09.28
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2019.05.22    kimht        최초생성
 * </pre>
 **********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
$(document).ready(function () {
	var colNames = ['포인트허브<br>거래번호', '고객명', '핸드폰번호','KT고객ID','개통자<br>여부','종료<br>여부','요청상태','진행상태','할인권번호','쿠폰상태','쿠폰금액','등록일시'];
	var colModel = [{ label: '포인트허브 거래번호', name: 'phub_tr_no', width: cWidth * 0.10	,key:true }
		,{ label: '고객명'				,name: 'cust_nm'  		,width: cWidth * 0.04 }
		,{ label: '핸드폰번호'			,name: 'cust_ctn'  		,width: cWidth * 0.07 }
		,{ label: 'KT고객ID' 			,name: 'kt_cust_id' 	,width: cWidth * 0.07 }
		,{ label: '개통자여부'			,name: 'own_yn'			,width: cWidth * 0.04 }
		,{ label: '종료여부' 			,name: 'end_yn'  		,width: cWidth * 0.04 }
		,{ label: '요청상태' 			,name: 'req_stat_cd'	,width: cWidth * 0.04 }
		,{ label: '진행상태' 			,name: 'prgr_stat'		,width: cWidth * 0.05 }
		,{ label: '할인권번호' 			,name: 'pin_no'			,width: cWidth * 0.07 }
		,{ label: '쿠폰상태' 			,name: 'cpn_stat_cd'	,width: cWidth * 0.05 }
		,{ label: '쿠폰금액' 			,name: 'cpn_amt'		,width: cWidth * 0.05 ,align:'right',formatter:'integer',formatoptions:{thousandsSeparator:','}}
		,{ label: '등록일시' 			,name: 'rgst_dt'		,width: cWidth * 0.10}
	];
	PHJQg.load('jqGrid','jqGridPager',colNames,colModel);
	
    setDate(null,null);
  	
    //엑셀다운로드
    $("#btn_excel").on("click", function() {
    	if (PHJQg.size('jqGrid') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
		if (!validateDate()) {
			return;
		}
    	var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");	
    	if(conf){		
    		var postData = {
    				PHUB_TR_NO 		: $("#in_phubTrNo").val()
    				,CUST_NM 		: $("#in_nm").val()
 					,PHUB_CPN_NO 	: $("#in_cpnNo").val()
    				,REQ_STAT_CD 	: $("#in_reqStatCd").val()
    				,PRGR_STAT		: $("#in_prgrStat").val()
    				,CPN_STAT_CD	: $("#in_cpnStatCd").val()
    				,START_DATE		: $('#start_date').val()
    				,END_DATE		: $('#end_date').val()
    			};
    		var colNames = ['포인트허브 거래번호', '핸드폰번호', '고객명','KT고객ID','개통자 여부','종료 여부','요청상태','진행상태','할인권번호','쿠폰상태','쿠폰금액','등록일시'];
    		var colModel = [{ label: '포인트허브 거래번호', name: 'phub_tr_no', width: cWidth * 0.10	,key:true }
    			,{ label: '핸드폰번호'			,name: 'cust_ctn'  		,width: cWidth * 0.07 }
    			,{ label: '고객명'				,name: 'cust_nm'  		,width: cWidth * 0.04 }
    			,{ label: 'KT고객ID' 			,name: 'kt_cust_id' 	,width: cWidth * 0.07 }
    			,{ label: '개통자여부'			,name: 'own_yn'			,width: cWidth * 0.04 }
    			,{ label: '종료여부' 			,name: 'end_yn'  		,width: cWidth * 0.04 }
    			,{ label: '요청상태' 			,name: 'req_stat_cd'	,width: cWidth * 0.04 }
    			,{ label: '진행상태' 			,name: 'prgr_stat'		,width: cWidth * 0.08 }
    			,{ label: '할인권번호' 			,name: 'pin_no'			,width: cWidth * 0.07 }
    			,{ label: '쿠폰상태' 		,name: 'cpn_stat_cd'	,width: cWidth * 0.05 }
    			,{ label: '쿠폰금액' 			,name: 'cpn_amt'		,width: cWidth * 0.05 ,align:'right',formatter:'integer',formatoptions:{thousandsSeparator:','}}
    			,{ label: '등록일시' 			,name: 'rgst_dt'		,width: cWidth * 0.10}
    		];
    		PHJQg.excel('jqGridExcelDiv','${ViewRoot}/dealHist/jgfmlySupotExcel.do',postData,colNames,colModel,'조르기현황내역');
    	}
    });
});
//조회시 reload
function search() {
 if (!validateDate()) {
		return;
	}
	var postData = {
		PHUB_TR_NO 		: $("#in_phubTrNo").val()
		,CUST_NM 		: $("#in_nm").val()
		,PIN_NO 		: $("#in_cpnNo").val()
		,REQ_STAT_CD 	: $("#in_reqStatCd").val()
		,PRGR_STAT		: $("#in_prgrStat").val()
		,CPN_STAT_CD	: $("#in_cpnStatCd").val()
		,START_DATE		: $('#start_date').val()
		,END_DATE		: $('#end_date').val()
	};
	PHJQg.reload('jqGrid','jqGridPager','${ViewRoot}/dealHist/jgSupotList.do',postData);
}
//기간 조건 체크
function validateDate() {
	var sDate = $("#start_date").val();
	var eDate = $("#end_date").val();
	if (!PHUtil.isEmpty(sDate) && !PHValid.date(PHUtil.replaceAll(sDate,'-',''))) {
		alert('시작일자가 유효한 날짜 형식이 아닙니다');
		$('#start_date').val('');
		$('#start_date').focus();
		return false;
	}
	if (!PHUtil.isEmpty(eDate) && !PHValid.date(PHUtil.replaceAll(eDate,'-',''))) {
		alert('종료일자가 유효한 날짜 형식이 아닙니다');
		$('#end_date').val('');
		$('#end_date').focus();
		return false;
	}
	var startDate = new Date(sDate);
	var endDate = new Date(eDate);
	if (startDate.getTime() > endDate.getTime()) {		
		alert('뒤의 날짜는 앞의 날짜 이전으로 검색할 수 없습니다.')
		return false;
	}
	return true;
}
</script>