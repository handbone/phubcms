<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="com.olleh.pHubCMS.common.utils.SessionUtils"%>
<%--
 **********************************************************************************************
 * @desc : 거래 내역 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/connHist/dealReqHistViewJs.jsp
 * @author ojh
 * @since 2018.08.13
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.13    ojh        최초생성
 * </pre>
 **********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
$(document).ready(function () {  	
	var colNames = ['No', '포인트허브<br>거래번호', '고객ID', '이름','사용<br>포인트','전환<br>금액','상품<br>결제금액','상품명','거래구분',
	                'PG사명','사용처명','PG<br>거래번호','PG제공<br>사용처ID','취소<br>여부','취소<br>구분','원포인트허브<br>거래번호','서비스명','거래일시','상세<br>거래내역','거래구분','서비스ID'];
	var colModel = [{ label: 'No', name: 'no', width: cWidth * 0.03, key:true }
		,{ label: '포인트허브거래번호'	,name: 'phub_tr_no'		,width: cWidth * 0.12 }
		,{ label: '고객ID'				,name: 'cust_id'		,width: cWidth * 0.10 }
		,{ label: '이름'				,name: 'cust_nm'		,width: cWidth * 0.08 }
		,{ label: '사용포인트'			,name: 'ttl_pnt'		,width: cWidth * 0.06	,formatter: caStr, align:'right' }
		,{ label: '전환금액'			,name: 'ttl_pnt_amt'	,width: cWidth * 0.06	,formatter: caStr, align:'right' }
		,{ label: '상품결제금액'		,name: 'ttl_pay_amt'	,width: cWidth * 0.06	,formatter: caStr, align:'right' }
		,{ label: '상품명'				,name: 'goods_nm' 		,width: cWidth * 0.08	,align:'left' }
		,{ label: '거래구분'			,name: 'deal_ind_nm'	,width: cWidth * 0.08 }                  
		,{ label: 'PG사명'				,name: 'pg_cmpn_nm' 	,width: cWidth * 0.08	,align:'left' }
		,{ label: '사용처명'			,name: 'cprt_cmpn_nm' 	,width: cWidth * 0.10	,align:'left' }
		,{ label: 'PG거래번호'			,name: 'pg_deal_no' 	,width: cWidth * 0.25 }
		,{ label: 'PG제공사용처ID' 		,name: 'pg_send_po_id'  ,width: cWidth * 0.08	,align:'left' }
		,{ label: '취소여부' 			,name: 'cncl_yn'  		,width: cWidth * 0.04 }                 
		,{ label: '취소구분' 			,name: 'cncl_ind'  		,width: cWidth * 0.04 }                 
		,{ label: '원포인트허브거래번호',name: 'ori_phub_tr_no' ,width: cWidth * 0.13 }
		,{ label: '서비스명'			,name: 'service_nm' 	,width: cWidth * 0.10 }
		,{ label: '거래일시' 			,name: 'deal_dt'  		,width: cWidth * 0.12 }
		,{ label: '상세거래내역'	 	,name: ''  				,width: cWidth * 0.06	,formatter:dbutton }
		,{ label: '거래구분'	 		,name: 'deal_ind'  		,hidden:true	}
		,{ label: '서비스ID'	 		,name: 'service_id'  	,hidden:true	}
	];
	PHJQg.load('jqGrid','jqGridPager',colNames,colModel);

	var dt = new Date();	
    setDate(dt,dt);

    // cell 클릭 이벤트 bind
    cellClickEvent();
    
    $('#in_cuTn').keypress(function(e) {
    	var keycode = e.keyCode ? e.keyCode : e.which;
    	if ((keycode >= 48 && keycode <= 57) || (keycode >= 96 && keycode <= 105)) {
    		return true;
    	} else {
    		switch(keycode) {
    			case 8:		// BACKSPACE
    			case 9:		// TAB
    			case 45:	// -
    				return true;
    				break;
    			default:
    				e.preventDefault();
    		}
    	}
    }).keyup(function(e) {
    	var value = $(this).val().replace(/[^0-9-]/g,"");
    	$(this).val(value);
    });
    
  	//엑셀다운로드
    $("#btn_excel").on("click", function() {
    	if (PHJQg.size('jqGrid') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
		if (!validateDate()) {
			return;
		}
    	var ctn = PHUtil.replaceAll($("#in_cuTn").val(),"-","");
    	ctn = PHUtil.replaceAll(ctn,"/","");
    	var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");	
    	if(conf){		
    		var postData = {
    			PHUB_TR_NO 	: $("#in_trNo").val()
    			,DEAL_IND 	: $("#sel_ind").val()
    			,PG_CMPN_ID : $("#sel_pgId").val()
				,PG_CPRT_ID : $('#sel_cprtId').val()   			
    			,PG_DEAL_NO :$('#in_pgDealNo').val()
    			,CNCL_YN 	: $("#sel_cYn").val()
    			,CUST_ID 	: $("#in_cuId").val()
    			,CUST_NM	: $("#in_cuNm").val()
    			,CUST_CTN 	: ctn
    			,PH_SVC_CD 	: $('#sel_svcCd').val()
    			,ORI_PHUB_TR_NO	: $("#in_onePointHub").val()
    			,START_DATE : $("#start_date").val()
    			,END_DATE 	: $("#end_date").val()	
    		};
    		var colNames = ['No', '포인트허브 거래번호', '고객ID', '이름','사용 포인트','전환 금액','상품 결제금액','상품명','거래구분','PG사명','사용처명','PG 거래번호','PG제공 사용처ID','취소 여부','취소 구분','원포인트허브 거래번호','서비스명','거래일시'];
    		var colModel = [{ label: 'No', name: 'no', width: cWidth * 0.03 }
           		,{ label: '포인트허브거래번호'	,name: 'phub_tr_no' }
           		,{ label: '고객ID'				,name: 'cust_id' }
           		,{ label: '이름'				,name: 'cust_nm' }
           		,{ label: '총사용포인트'		,name: 'ttl_pnt'		,formatter: caStr}
           		,{ label: '총포인트전환금액'	,name: 'ttl_pnt_amt'	,formatter: caStr}
           		,{ label: '총결제금액'			,name: 'ttl_pay_amt'	,formatter: caStr}
           		,{ label: '상품명'				,name: 'goods_nm' }
           		,{ label: '거래구분'			,name: 'deal_ind' }                  
           		,{ label: 'PG사명'				,name: 'pg_cmpn_nm' }
           		,{ label: '사용처명'			,name: 'cprt_cmpn_nm'}
           		,{ label: 'PG거래번호'			,name: 'pg_deal_no' }
           		,{ label: 'PG제공사용처ID' 		,name: 'pg_send_po_id' }
           		,{ label: '취소여부' 			,name: 'cncl_yn' }       
        		,{ label: '취소구분' 			,name: 'cncl_ind'}                 
           		,{ label: '원포인트허브거래번호',name: 'ori_phub_tr_no' }
           		,{ label: '서비스명'			,name: 'service_nm' }
           		,{ label: '거래일시' 			,name: 'deal_dt' }
    		];
    		PHJQg.excel('jqGridExcelDiv','${ViewRoot}/dealHist/jgDealHistExcel.do',postData,colNames,colModel,'거래내역');
    	}
    }) 
});

<%-- cell 클릭 이벤트 bind --%>
function cellClickEvent() {
	$("tbody").on("click", ">tr>td[aria-describedby='jqGrid_no']", function(e) {
		// 상세정보 팝업 Open
		popClick($(this).html());
	});
}

function search() {
	if (!validateDate()) {
		return;
	}	
	var ctn = PHUtil.replaceAll($("#in_cuTn").val(),"-","");
	ctn = PHUtil.replaceAll(ctn,"/","");
	var postData = {
		PHUB_TR_NO 	: $("#in_trNo").val()
		,DEAL_IND 	: $("#sel_ind").val()
		,PG_CMPN_ID : $("#sel_pgId").val()
		,PG_CPRT_ID : $('#sel_cprtId').val()
		,PG_DEAL_NO : $('#in_pgDealNo').val()
		,CNCL_YN 	: $("#sel_cYn").val()
		,CUST_ID 	: $("#in_cuId").val()
		,CUST_NM	: $("#in_cuNm").val()
		,CUST_CTN 	: ctn
		,PH_SVC_CD 	: $('#sel_svcCd').val()
		,ORI_PHUB_TR_NO	: $("#in_onePointHub").val()
		,START_DATE : $("#start_date").val()
		,END_DATE 	: $("#end_date").val()	
	};
	PHJQg.reload('jqGrid','jqGridPager','${ViewRoot}/dealHist/jgDealHist.do',postData);
}
//조회기간 제약 // 제약 Max한달 
function validateDate(){
	var sDate = $("#start_date").val();
	var eDate = $("#end_date").val();
	if (!PHUtil.isEmpty(sDate) && !PHValid.date(PHUtil.replaceAll(sDate,'-',''))) {
		alert('시작일자가 유효한 날짜 형식이 아닙니다');
		$('#start_date').val('');
		$('#start_date').focus();
		return false;
	}
	if (!PHUtil.isEmpty(eDate) && !PHValid.date(PHUtil.replaceAll(eDate,'-',''))) {
		alert('종료일자가 유효한 날짜 형식이 아닙니다');
		$('#end_date').val('');
		$('#end_date').focus();
		return false;
	}
	var startDate = new Date(sDate);
	var endDate = new Date(eDate);
	if (startDate.getTime() > endDate.getTime()) {		
		alert('뒤의 날짜는 앞의 날짜 이전으로 검색할 수 없습니다.')
		return false;
	} else {
		var valDate = new Date(sDate);
		valDate.setMonth(valDate.getMonth()+1);
		if (endDate.getTime() > valDate.getTime()) {
			alert('검색 기간은 한달을 넘을 수 없습니다.');
			return false;
		}
	}
	return true;
}
function caStr(cellvalue,options,rowobject){
	cellvalue = cellvalue != null ? cellvalue+'' : cellvalue;
	if(rowobject.deal_ind != 'PA'){
		if (Number(cellvalue)) {
			return "-"+PHUtil.setComma(cellvalue);
		}
		else {
			return "";
		}
	} else{
		return PHUtil.nvl(PHUtil.setComma(cellvalue),'');
	}
}
//formatter 버튼 만들어 주기 클릭시 No전달
function dbutton(cellvalue,options,rowobject){
    return '<input type="button" class="table_btn" onclick="popClick('+rowobject.no+')" value="확인" />';
}
//클릭시 상세 정보 창에 data 셋팅
function popClick(data){	
	var row = $("#jqGrid").jqGrid('getRowData',data);
	$('.info_table tbody').empty();
	$(	'<tr>'+
			"<td>"+row.phub_tr_no+"</td>"+
			"<td>"+row.cust_id+"</td>"+
			"<td>"+row.cust_nm+"</td>"+
			"<td style='text-align:right'>"+row.ttl_pnt+"</td>"+
			"<td style='text-align:right'>"+row.ttl_pnt_amt+"</td>"+
			"<td style='text-align:right'>"+row.ttl_pay_amt+"</td>"+
			"<td>"+row.deal_ind_nm+"</td>"+
			"<td>"+row.pg_cmpn_nm+"</td>"+
			"<td>"+row.cncl_yn+"</td>"+
			"<td>"+row.cncl_ind+"</td>"+
			"<td>"+row.deal_dt+"</td>"+
			"<td>"+
				((row.deal_ind=="PA" && row.cncl_yn == "N" && row.service_id == 'SVC_BASE') ?
					"<button class='table_btn' onclick=\"cancelDeal(\'"+row.phub_tr_no+"\',\'"+row.pg_deal_no+"\','N')\">승인취소<br>(내부취소)</button> "
						+"<button class='table_btn' onclick=\"cancelDeal(\'"+row.phub_tr_no+"\',\'"+row.pg_deal_no+"\','Y')\">승인취소<br>(연동취소)</button>"
					: ""
				)+
			"</td>"+
		'</tr>'
	).appendTo('.info_table tbody');
	
	//제공처별 요청 내역 aJax호출하여 가져오기
	var params = {
		PHUB_TR_NO : row.phub_tr_no
	};
	PHFnc.ajax("${ViewRoot}/dealHist/ajaxDealHistDtl.do",params,"POST","json",successAjax,null,true,true,false);
	
	PHFnc.layerPopOpen(1);
}
function successAjax(data, textStatus, jqXHR){
	$('#tBody').empty();
	if (data.rows == null) {
		return;
	}
	//Row 개수에 따라 td태그 생성하여 Html 변환
	var hText = "";		
	var cText = "";
	for(var i=0; i<data.rows.length; i++){
		if(data.rows[i].deal_ind =="CA"){
			cText ="-";
		} else{
			cText = "";
		}
		$(	"<tr>"+
				"<td>"+data.rows[i].no+"</td>"+
				"<td style='text-align:left'>"+data.rows[i].pnt_nm+"</td>"+
				"<td>"+data.rows[i].pnt_tr_no+"</td>"+
				"<td style='text-align:right'>"+cText+PHUtil.setComma(data.rows[i].ans_pnt)+"</td>"+
				"<td>"+data.rows[i].pnt_exch_rate+"</td>"+
				"<td style='text-align:right'>"+cText+PHUtil.setComma(data.rows[i].ans_pnt_amt)+"</td>"+
			"</tr>"
		).appendTo('#tBody');
	}
}

//승인취소 버튼 클릭 시 포인트허브 URL 요청
function cancelDeal(phub, pg, yn){	
	var userId = '<%=SessionUtils.getUserId(request)%>';
	var phUrl = '${PH_DOMAIN}/cms/cancel.do'
	var params = {
		phub_tr_no : phub
		, pg_tr_no : pg
		, tr_div   : 'CA'
		, user_id  : userId
		, if_yn	   : yn
		, cncl_yn  : 'A'
		, cncl_ind : 'A'
	};		
	var conf;	
	if(yn == 'Y'){
		conf = confirm("해당 데이터의 승인을 취소하시겠습니까? \n클립포인트(외부시스템)와 연동되어 승인취소 됩니다.");
	} else if(yn == 'N'){
		conf = confirm("해당 데이터의 승인을 취소하시겠습니까? \n(포인트허브만 승인취소 됩니다.)");
	}
	if(conf){
		$(".loader").show();
		PHFnc.ajax("${ViewRoot}/dealHist/ajaxDealHistCancel.do",params,"POST","json",
			function(data, textStatus, jqXHR) {
				alert("처리에 성공하였습니다.");
				$(".top_close").click();
				$(".loader").hide();
				search();
			},
			function(data, textStatus, jqXHR) {
				if (!PHUtil.isEmpty(data.eMsg)) {
					PHFnc.alert(data.eMsg);	
				} else {
					PHFnc.ajaxError(null,jqXHR);
				}
				$(".loader").hide();
			},
		true,true,false);
	}
}

</script>