<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%>
<%--
 **********************************************************************************************
 * @desc : 포인트 사용처 등록 팝업
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/cmpn/pntUseCmpnCreatePop.jsp
 * @author ojh
 * @since 2018.08.14
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.14    ojh        최초생성
 *
 **********************************************************************************************
 pntUseCmpnView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>
<div class="popup_content">
        <div class="popup_title">포인트사용처 등록<a href="#!"><div class="top_close"></div></a></div>
        <table class="half_table" border="0" cellpadding="0" cellspacing="0">
            <tbody>
            <tr>
                <th>사용처ID</th>
                <td>(시스템에서 자동으로 생성합니다)</td>
                <th>사용처명  <span class="red">*</span></th>
                <td><input type="text" name="name" id="in_CmNm" maxlength="50"></td>
            </tr>
            <tr>
                <th>PG사명 <span class="red">*</span></th>
                <td>
                    <select id="sel_PgId">
                        <option value="all">선택</option>
                        <c:forEach items="${PG_CMPN}" var="code">
                           	<option value="${code.pg_cmpn_id}">${code.pg_cmpn_nm}(${code.pg_cmpn_id})</option>
                        </c:forEach>
                    </select>
                </td>
                <th>PG제공사용처ID  <span class="red">*</span></th>
                <td><input type="text" name="name" id="in_PoId" maxlength="20"></td>
            </tr>
            <tr>
                <th>사업자번호</th>
                <td><input type="text" name="name" size="5" id="in_BizNo_1" onKeyDown="return onlyNum(event)" onKeyUp="return keyReplace(this)"> 
                - <input type="text" name="name" size="5" id="in_BizNo_2" onKeyDown="return onlyNum(event)" onKeyUp="return keyReplace(this)"> 
                - <input type="text" name="name" size="5" id="in_BizNo_3" onKeyDown="return onlyNum(event)" onKeyUp="return keyReplace(this)"></td>
                <th>충전/결제구분</th>
                <td>
                    <input type="radio" name="c_radio" value="RE" checked> 충전
                    <input type="radio" name="c_radio" value="PA" > 결제
                </td>
            </tr>
            <tr>
                <th>PG 수수료율  <span class="red">*</span></th>
                <td><input type="text" name="name" id="in_pgCm"> %</td>
                <th>수수료율적용일자 <span class="red">*</span></th>
                <td><input type="text" name="name" id="in_cmsnDate" size="10" readonly><p>*수수료 적용 시작일자만 입력해 주세요.</p></td>
            </tr>
             <tr>
                <th>KT 수수료율  <span class="red">*</span></th>
                <td><input type="text" name="name" id="in_ktCm"> %</td>
                <th>지원 포인트제공처 <span class="red">*</span></th>
                <td><button onClick="PHFnc.layerPopOpen(5);return false;">상세보기</button></td>
            </tr>            
            </tbody>
        </table>
        <div class="btnalign">
            <ul>
            	<li><a href="#!"><div class="btn_cancel">취소</div></a></li>
                <li><a href="#!"><div class="btn_save" onClick="createPntUseCmpn()">저장</div></a></li>                
            </ul>
        </div>
    </div>