<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
 **********************************************************************************************
 * @desc : 포인트제공처 관리 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/cmpn/pntPrvdrViewJs.jsp
 * @author ojh
 * @since 2018.10.01
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.01    ojh        최초생성
 * 
 **********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
$(document).ready(function () {  
	var colNames = ['No', '제공처ID', '제공처명','포인트코드','수수료율(VAT포함)','전환율','수정자','수정일시','관리'];
	var colModel = [{ label: 'No', name: 'no', width: cWidth * 0.05, key:true }
		,{ label: '제공처ID'			,name: 'prvdr_id' 		,width: cWidth * 0.15 }
		,{ label: '제공처명'			,name: 'prvdr_nm' 		,width: cWidth * 0.15, align:'left' }
		,{ label: '포인트코드'			,name: 'pnt_cd'			,width: cWidth * 0.10, align:'left' }
		,{ label: '수수료율(VAT포함)'	,name: 'pnt_cmsn_rate'	,width: cWidth * 0.10, formatter:cmsn }
		,{ label: '전환율'				,name: 'pnt_exch_rate'	,width: cWidth * 0.10, formatter:exch }
		,{ label: '수정자'				,name: 'mdfy_user_nm' 	,width: cWidth * 0.15 }
		,{ label: '수정일시'			,name: 'mdfy_dt' 		,width: cWidth * 0.15 }
		,{ label: '관리'				,name: ''  				,width: cWidth * 0.05, formatter:mbutton }
	];	
	PHJQg.load('jqGrid','jqGridPager',colNames,colModel);
	
	// 엑셀다운로드
	$("#btn_excel").on("click", function(){
		var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");	
		if(conf){		
			var postData = {
				PRVDR_ID : $("#in_id").val(),
				PRVDR_NM : $("#in_nm").val(),
				PNT_CD 		 : $("#in_cd").val()
			};
			var colNames = ['No', '제공처ID', '제공처명','포인트코드','수수료율(VAT포함)','전환율','수정자','수정일시'];
			var colModel = [{ label: 'No', name: 'no' }
	       		,{ label: '제공처ID'			,name: 'prvdr_id' }
	       		,{ label: '제공처명'			,name: 'prvdr_nm' }
	       		,{ label: '포인트코드'			,name: 'pnt_cd' }
	       		,{ label: '수수료율(VAT포함)'	,name: 'pnt_cmsn_rate' ,formatter:cmsn }
	       		,{ label: '전환율'				,name: 'pnt_exch_rate' ,formatter:exch }
	       		,{ label: '수정자'				,name: 'mdfy_user_nm' }
	       		,{ label: '수정일시'			,name: 'mdfy_dt' }
			];
			PHJQg.excel('jqGridExcelDiv','${ViewRoot}/cmpn/jgPntPrvdrExcel.do',postData,colNames,colModel,'포인트제공처');
		}
	});
	
	//달력 셋팅
	$("#c_exchDate").datepicker();
	$("#c_cmsnDate").datepicker();
	$("#m_exchDate").datepicker();
	$("#m_cmsnDate").datepicker();
	
	//달력 크기 조정
	$('img.ui-datepicker-trigger').css({
		'width':'35px'
		, 'height':'35px'
		, 'padding':'5px'
		, 'margin-bottom':'-12px'
		, 'display': 'inline-block'
		, '-webkit-background-size': '100%'
		, 'background-size': '100%'
		, 'vertical-align':'baseline'
	});
	
	//X버튼이나 취소 버튼 클릭시 팝업창 초기화
	$(".top_close").click(function () {
		resetPop();		
    });
    $(".btn_cancel").click(function () {
    	resetPop();
    });
    $('#c_pnt_prfx,#m_pnt_prfx').keyup(function(e) {
    	var value = $(this).val().replace(/[^A-Za-z]/g,"");
    	$(this).val(value);
    });
    $('#c_min_avl_pnt,#m_min_avl_pnt').keypress(function(e) {
    	var keycode = e.keyCode ? e.keyCode : e.which;
    	if ((keycode >= 48 && keycode <= 57) || (keycode >= 96 && keycode <= 105)) {
    		return true;
    	} else {
    		switch(keycode) {
    			case 8:		// BACKSPACE
    			case 9:		// TAB
    			case 46:
    				return true;
    				break;
    			default:
    				e.preventDefault();
    		}
    	}
    }).keyup(function(e) {
    	var value = $(this).val().replace(/[^0-9.]/g,"");
    	$(this).val(value);
    });
});	
//수수료율 표기
function cmsn(cellvalue,options,rowobject){
	return cellvalue+" %";
}
//전환율 표기
function exch(cellvalue,options,rowobject){
	return cellvalue+" : 1"
}

function resetPop(){
	//등록창 팝업
	$('input:text[id^=c_]').val('');
	$("#c_ck_win").prop("checked",false);
	$("#c_ck_and").prop("checked",false);
	$("#c_ck_ios").prop("checked",false);
	
	//수정창 팝업
	$('input:text[id^=m_]').val('');
	$("#m_ck_win").prop("checked",false);
	$("#m_ck_and").prop("checked",false);
	$("#m_ck_ios").prop("checked",false);
}

//검색버튼 클릭
//파라미터 전달 후 jQgrid reload 
function search(){
	var postData = {
		PRVDR_ID : $("#in_id").val(),
		PRVDR_NM : $("#in_nm").val(),
		PNT_CD 		 : $("#in_cd").val()
	};
	PHJQg.reload('jqGrid','jqGridPager','${ViewRoot}/cmpn/jgPntPrvdr.do',postData);
}

//formatter 버튼 만들어 주기 클릭시 No번호 param 전달
function mbutton(cellvalue,options,rowobject){
    return '<input type="button" class="table_btn" onclick="popClick('+rowobject.no+')" value="수정" />';
}

//수정창의 수수료율 이력조회 클릭
function cmsnHistPopClick(){	
	//누른 제공처 ID를 key로 상세 조회
	var params = {PRVDR_ID : $("#m_id").html()};
	PHFnc.ajax("${ViewRoot}/cmpn/ajaxPntPrvdrCmsnHist.do",params,"POST","json",function(data, textStatus, jqXHR) {
		$("#c_id").html($("#m_id").html());	
		$("#c_nm").html($("#m_in_nm").val());	
		
		//Row 개수에 따라 td태그 생성하여 Html 변환
		var hText = "";		
		for(var i=0; i<data.rows.length; i++){
			hText += "<tr>";
			hText += "<td>"+data.rows[i].no+"</td>";
			hText += "<td style='text-align:right'>"+data.rows[i].pnt_cmsn_rate+"%</td>";
			hText += "<td>"+data.rows[i].rate_aply_strt_dd+"~"+data.rows[i].rate_aply_end_dd+"</td>";
			hText += "<td>"+PHUtil.nvl(data.rows[i].mdfy_user_nm)+"</td>";
			hText += "<td>"+PHUtil.nvl(data.rows[i].mdfy_dt)+"</td>";
			hText += "</tr>";
		}
		$("#tBody").html(hText);
		PHFnc.layerPopOpen(5);
	},null,true,true,false);
}

//수정창의 전환율 이력조회 클릭
function exchHistPopClick(){	
	//누른 제공처 ID를 key로 상세 조회
	var params = {PRVDR_ID : $("#m_id").html()};
	PHFnc.ajax("${ViewRoot}/cmpn/ajaxPntPrvdrExchHist.do",params,"POST","json",function(data, textStatus, jqXHR) {
		$("#e_id").html($("#m_id").html());	
		$("#e_nm").html($("#m_in_nm").val());	
		
		//Row 개수에 따라 td태그 생성하여 Html 변환
		var hText = "";		
		for(var i=0; i<data.rows.length; i++){
			hText += "<tr>";
			hText += "<td>"+data.rows[i].no+"</td>";
			hText += "<td style='text-align:right'>"+data.rows[i].pnt_exch_rate+" : 1</td>";
			hText += "<td>"+data.rows[i].rate_aply_strt_dd+"~"+data.rows[i].rate_aply_end_dd+"</td>";
			hText += "<td>"+PHUtil.nvl(data.rows[i].mdfy_user_nm)+"</td>";
			hText += "<td>"+PHUtil.nvl(data.rows[i].mdfy_dt)+"</td>";
			hText += "</tr>";
		}
		$("#tBody_ex").html(hText);
		PHFnc.layerPopOpen(6);
	},null,true,true,false);
	
	
}

//<<<<<<<<<< 수정버튼 클릭 후 데이터 가져오기
//팝업창 오픈 및 상세 정보 가져오기 
function popClick(data){	
	var row = $("#jqGrid").jqGrid('getRowData',data);

	//누른 제공처 ID를 key로 상세 조회
	var params = {PRVDR_ID : row.prvdr_id};
	PHFnc.ajax("${ViewRoot}/cmpn/ajaxGetPntPrvdr.do",params,"POST","json",function(data, textStatus, jqXHR) {
		$("#m_id").html(data.rows.prvdr_id);	
		$("#m_in_nm").val(data.rows.prvdr_nm);	
		
		$("#m_in_pntCd").val(data.rows.pnt_cd);	
		$("#m_in_pntNm").val(data.rows.pnt_nm);	
		
		$("#m_in_dp").val(data.rows.dp_unit);	
		$("#m_in_deal").val(data.rows.deal_unit);		
		
	/* 	$("#m_ck_win").val(data.rows.window_yn);
		$("#m_ck_and").val(data.rows.user_nm);
		$("#m_ck_ios").val(data.rows.user_nm); */
		
		if(data.rows.window_yn == 'Y') $("#m_ck_win").prop("checked",true);	
		if(data.rows.andr_yn == 'Y') $("#m_ck_and").prop("checked",true);
		if(data.rows.ios_yn == 'Y') $("#m_ck_ios").prop("checked",true);	
		
		$("#m_in_cert").val(data.rows.athn_key);
		$("#m_in_cm").val(data.rows.pnt_cmsn_rate);
		$("#m_in_ex").val(data.rows.pnt_exch_rate);
		
		$("#m_cmsnDate").val(data.rows.rate_aply_strt_dd_cmsn);
		$("#m_exchDate").val(data.rows.rate_aply_strt_dd_exch);
		cmsnDate = data.rows.rate_aply_strt_dd_cmsn;
		exchDate = data.rows.rate_aply_strt_dd_exch;
		$("#m_rId").html(data.rows.rgst_user_nm);	
		$("#m_rDt").html(data.rows.rgst_dt);	
		$("#m_mId").html(data.rows.mdfy_user_nm);	
		$("#m_mDt").html(data.rows.mdfy_dt);	
		$('#m_imag_link').val(data.rows.imag_link);
		$('#m_pnt_rule').val(data.rows.pnt_rule);
		$('#m_pnt_prfx').val(data.rows.pnt_prfx);
		$('#m_min_avl_pnt').val(data.rows.min_avl_pnt);
		
		PHFnc.layerPopOpen(2);
	},null,true,true,false);
}

var cmsnDate;
var exchDate;
//포인트 제공처 등록
function createPntPrvdr(){
	if($("#c_in_nm").val() == ""){
		alert("제공처명을 입력해 주세요.");
		return;
	}
	if($("#c_in_pntCd").val() == ""){
		alert("포인트코드를 입력해 주세요.");
		return;
	}
	if($("#c_in_pntNm").val() == ""){
		alert("포인트명을 입력해 주세요.");
		return;
	}	
	if($("#c_in_dp").val() == ""){
		alert("표시단위를 입력해 주세요.");
		return;
	}	
	if($("#c_in_deal").val() == ""){
		alert("거래단위를 입력해 주세요.");
		return;
	}
	if($("#c_in_cm").val() == ""){
		alert("수수료율을 입력해 주세요.");
		return;
	}
	if($("#c_in_ex").val() == ""){
		alert("전환율을 입력해 주세요.");
		return;
	}
	if($("#c_cmsnDate").val() == ""){
		alert("수수료율 적용일자를 선택해 주세요.");
		return;
	}
	if($("#c_exchDate").val() == ""){
		alert("전환율 적용일자를 선택해 주세요.");
		return;
	}	
	if($("#c_in_cm").val() < 0 || $("#c_in_cm").val() > 100){
		alert("수수료율은 백분율, 소수점 셋째자리까지 허용됩니다.");
		return;
	}	
	
	var win
	var andr
	var ios
	
  	if( $("#c_ck_win").is(':checked') ){
  		win = 'Y';
  	} else{
  		win = 'N';
  	}
  	if( $("#c_ck_and").is(':checked') ){
  		andr = 'Y';
  	} else{
  		andr = 'N';
  	}
  	if( $("#c_ck_ios").is(':checked') ){
  		ios = 'Y';
  	} else{
  		ios = 'N';
  	}
	
	if(confirm("저장하시겠습니까?")){
		var params = { 
			PRVDR_ID 				: $("#c_id").html()
			, PRVDR_NM 				: $("#c_in_nm").val()	
			, PNT_CD 				: $("#c_in_pntCd").val()	
			, PNT_NM 				: $("#c_in_pntNm").val()
			, DP_UNIT 				: $("#c_in_dp").val()
			, DEAL_UNIT 			: $("#c_in_deal").val()
			, WINDOW_YN 			: win
			, ANDR_YN 				: andr
			, IOS_YN 				: ios
			, ATHN_KEY 				: $("#c_in_cert").val()
			, PNT_CMSN_RATE 		: $("#c_in_cm").val()
			, PNT_EXCH_RATE 		: $("#c_in_ex").val()
			, RATE_APLY_STRT_DD_CMSN: PHUtil.replaceAll($("#c_cmsnDate").val(),"-","")
			, RATE_APLY_STRT_DD_EXCH: PHUtil.replaceAll($("#c_exchDate").val(),"-","")
			, IMAG_LINK				: $('#c_imag_link').val()
			, PNT_RULE				: $('#c_pnt_rule').val()
			, PNT_PRFX				: $('#c_pnt_prfx').val().toUpperCase()
			, MIN_AVL_PNT			: $('#c_min_avl_pnt').val()
		};	
		PHFnc.ajax("${ViewRoot}/cmpn/ajaxCreatePntPrvdr.do",params,"POST","json",
			function(data, textStatus, jqXHR) {
				alert("등록하였습니다.");
				$(".top_close").click();
				search();
			},
			function(data, textStatus, jqXHR) {
				if (!PHUtil.isEmpty(data.eMsg)) {
					PHFnc.alert(data.eMsg);	
				} else {
					PHFnc.ajaxError(null,jqXHR);
				}
				PHFnc.submitLoadingDestory();
			},
		true,true,false);
	}
}
//포인트 제공처 수정
function modifyPntPrvdr(){
	
	if($("#m_in_nm").val() == ""){
		alert("제공처명을 입력해 주세요.");
		return;
	}
	if($("#m_in_pntCd").val() == ""){
		alert("포인트코드를 입력해 주세요.");
		return;
	}
	if($("#m_in_pntNm").val() == ""){
		alert("포인트명을 입력해 주세요.");
		return;
	}	
	if($("#m_in_dp").val() == ""){
		alert("표시단위를 입력해 주세요.");
		return;
	}	
	if($("#m_in_deal").val() == ""){
		alert("거래단위를 입력해 주세요.");
		return;
	}
	if($("#m_in_cm").val() == ""){
		alert("수수료율을 입력해 주세요.");
		return;
	}
	if($("#m_in_ex").val() == ""){
		alert("전환율을 입력해 주세요.");
		return;
	}
	if($("#m_cmsnDate").val() == ""){
		alert("수수료율 적용일자를 선택해 주세요.");
		return;
	}
	if($("#m_exchDate").val() == ""){
		alert("전환율 적용일자를 선택해 주세요.");
		return;
	}	
	if($("#m_in_cm").val() < 0 || $("#m_in_cm").val() > 100){
		alert("수수료율은 백분율, 소수점 셋째자리까지 허용됩니다.");
		return;
	}
	
	var cmsnDate_o = new Date(cmsnDate);
	var cmsnDate_n = new Date($("#m_cmsnDate").val());
	var exchDate_o = new Date(exchDate);
	var exchDate_n = new Date($("#m_exchDate").val());
	var cmsnDate_mdfy
	var exchDate_mdfy
	
	if(cmsnDate_o.getTime() > cmsnDate_n.getTime()){
		alert("수수료율 적용일자는 현재 적용일자 이전으로 선택할 수 없습니다.");
		return;
	} else if(cmsnDate_o.getTime() == cmsnDate_n.getTime()){
		cmsnDate_mdfy = 0;
	} else{
		cmsnDate_mdfy = 1;
	}
	
	if(exchDate_o.getTime() > exchDate_n.getTime()){
		alert("전환율 적용일자는 현재 적용일자 이전으로 선택할 수 없습니다.");
		return;
	} else if(exchDate_o.getTime() == exchDate_n.getTime()){
		exchDate_mdfy = 0;
	} else{
		exchDate_mdfy = 1;
	}
	
	var win
	var andr
	var ios
	
  	if( $("#m_ck_win").is(':checked') ){
  		win = 'Y';
  	} else{
  		win = 'N';
  	}
  	if( $("#m_ck_and").is(':checked') ){
  		andr = 'Y';
  	} else{
  		andr = 'N';
  	}
  	if( $("#m_ck_ios").is(':checked') ){
  		ios = 'Y';
  	} else{
  		ios = 'N';
  	}
	
	if(confirm("저장하시겠습니까?")){
		var params = { 
			PRVDR_ID 				: $("#m_id").html()
			, PRVDR_NM 				: $("#m_in_nm").val()	
			, PNT_CD 				: $("#m_in_pntCd").val()	
			, PNT_NM 				: $("#m_in_pntNm").val()
			, DP_UNIT 				: $("#m_in_dp").val()
			, DEAL_UNIT 			: $("#m_in_deal").val()
			, WINDOW_YN 			: win
			, ANDR_YN 				: andr
			, IOS_YN 				: ios
			, ATHN_KEY 				: $("#m_in_cert").val()
			, PNT_CMSN_RATE 		: $("#m_in_cm").val()
			, PNT_EXCH_RATE 		: $("#m_in_ex").val()
			, RATE_APLY_STRT_DD_CMSN: PHUtil.replaceAll($("#m_cmsnDate").val(),"-","")
			, RATE_APLY_STRT_DD_EXCH: PHUtil.replaceAll($("#m_exchDate").val(),"-","")
			, MDFY_CMSN_DATE		: cmsnDate_mdfy
			, MDFY_EXCH_DATE		: exchDate_mdfy
			, IMAG_LINK				: $('#m_imag_link').val()
			, PNT_RULE				: $('#m_pnt_rule').val()
			, PNT_PRFX				: $('#m_pnt_prfx').val().toUpperCase()
			, MIN_AVL_PNT			: $('#m_min_avl_pnt').val()
		};	
		PHFnc.ajax("${ViewRoot}/cmpn/ajaxModifyPntPrvdr.do",params,"POST","json",
			function(data, textStatus, jqXHR) {
				alert("수정하였습니다.");
				$(".top_close").click();
				search();
			},
			function(data, textStatus, jqXHR) {
				if (!PHUtil.isEmpty(data.eMsg)) {
					PHFnc.alert(data.eMsg);	
				} else {
					PHFnc.ajaxError(null,jqXHR);
				}
				PHFnc.submitLoadingDestory();
			},
		true,true,false);
	}
}
 
</script>