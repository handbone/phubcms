<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--
 **********************************************************************************************
 * @desc : PG사 등록 팝업
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/cmpn/pntPgCmpnCreatePop.jsp
 * @author ojh
 * @since 2018.10.05
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.05    ojh        최초생성
 *
 **********************************************************************************************
 pntPgCmpnView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>
    <!-- PG사 등록 시작-->
    <div class="popup_content">
        <div class="popup_title">PG사 등록<a href="#!"><div class="top_close"></div></a></div>
        <table class="half_table" border="0" cellpadding="0" cellspacing="0">
            <tbody>
            <tr>
                <th>PG사ID</th>
                <td>(시스템에서 자동으로 생성합니다.)</td>
                <th>PG사명  <span class="red">*</span></th>
                <td><input type="text" name="name" id="c_in_nm"></td>
            </tr>
            <tr>
                <th>인증키</th>
                <td><input type="text" name="name" id="c_in_cert"></td>
                <th>사용가능일자  <span class="red">*</span></th>
                <td> <input type="text" name="name" id="c_startDate" size="10" readonly> ~ <input type="text" name="name" id="c_endDate" size="10" readonly></td>
            </tr>
            <tr>
                <th>담당자명 <span class="red">*</span></th>
                <td><input type="text" name="name" id="c_in_chrNm"></td>
                <th>담당자연락처  <span class="red">*</span></th>
                <td><input type="text" name="name" size="3" id="c_in_tel1" onKeyDown="return onlyNum(event)" onKeyUp="return keyReplace(this)">
                 - <input type="text" name="name" size="4" id="c_in_tel2" onKeyDown="return onlyNum(event)" onKeyUp="return keyReplace(this)">
                 - <input type="text" name="name" size="4" id="c_in_tel3" onKeyDown="return onlyNum(event)" onKeyUp="return keyReplace(this)"></td>
            </tr>
            </tbody>
        </table>
        <div class="btnalign">
            <ul>
                <li><a href="#!"><div class="btn_cancel">취소</div></a></li>
                <li><a href="#!"><div class="btn_save" onClick="createPntPgCmpn()">저장</div></a></li>
            </ul>
        </div>
    </div>