 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%>
<%--
 **********************************************************************************************
 * @desc : PG사 수정 팝업
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/cmpn/pntPgCmpnModifyPop.jsp
 * @author ojh
 * @since 2018.10.05
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.05    ojh        최초생성
 *
 **********************************************************************************************
 pntPgCmpnView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>
    <!-- PG사 수정 시작-->
    <div class="popup_content2">
        <div class="popup_title">PG사 수정<a href="#!"><div class="top_close"></div></a></div>
        <table class="half_table" border="0" cellpadding="0" cellspacing="0">
            <tbody>
            <tr>
                <th>PG사ID <span class="red">*</span></th>
                <td id="m_id"></td>
                <th>PG사명  <span class="red">*</span></th>
                <td><input type="text" name="name" id="m_in_nm"></td>
            </tr>
            <tr>
                <th>인증키</th>
                <td><input type="text" name="name" id="m_in_cert"></td>
                <th>사용가능일자  <span class="red">*</span></th>
                <td> <input type="text" name="name" id="m_startDate" size="10" readonly> ~ <input type="text" name="name"  id="m_endDate" size="10" readonly></td>
            </tr>
            <tr>
                <th>담당자명 <span class="red">*</span></th>
                <td><input type="text" name="name" id="m_in_chrNm"></td>
                <th>담당자연락처  <span class="red">*</span></th>
                <td><input type="text" name="name" size="3" id="m_in_tel1" onKeyDown="return onlyNum(event)" onKeyUp="return keyReplace(this)">
                 - <input type="text" name="name" size="4" id="m_in_tel2" onKeyDown="return onlyNum(event)" onKeyUp="return keyReplace(this)">
                 - <input type="text" name="name" size="4" id="m_in_tel3" onKeyDown="return onlyNum(event)" onKeyUp="return keyReplace(this)"></td>
            </tr>
            <tr>
                <th>등록자</th>
                <td id="m_rId"></td>
                <th>등록일시</th>
                <td id="m_rDt"></td>
            </tr>
            <tr>
                <th>수정자</th>
                <td id="m_mId"></td>
                <th>수정일시</th>
                <td id="m_mDt"></td>
            </tr>
            </tbody>
        </table>
        <div class="btnalign">
            <ul>
                <li><a href="#!"><div class="btn_cancel">취소</div></a></li>
                <li><a href="#!"><div class="btn_save" onClick="modifyPntPgCmpn()">저장</div></a></li>
            </ul>
        </div>
    </div>