 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--
 **********************************************************************************************
 * @desc : 포인트 제공처 등록 팝업
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/cmpn/pntPrvdrCreatePop.jsp
 * @author ojh
 * @since 2018.10.01
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.01    ojh        최초생성
 *
 **********************************************************************************************
 pntPrvdrView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>
<!-- 포인트제공처 등록 시작-->
<div class="popup_content">
	<div class="popup_title">포인트제공처 등록<a href="#!"><div class="top_close"></div></a></div>
	<table class="half_table" border="0" cellpadding="0" cellspacing="0">
		<tbody>
			<tr>
			    <th>제공처ID</th>
			    <td>(시스템에서 자동으로 생성합니다)</td>
			    <th>제공처명  <span class="red">*</span></th>
			    <td><input type="text" name="name" id="c_in_nm"></td>
			</tr>
			<tr>
			    <th>포인트코드 <span class="red">*</span></th>
			    <td><input type="text" name="name" id="c_in_pntCd"></td>
			    <th>포인트명  <span class="red">*</span></th>
			    <td><input type="text" name="name" id="c_in_pntNm"></td>
			</tr>
			<tr>
			    <th>표시단위 <span class="red">*</span></th>
			    <td><input type="text" name="name" id="c_in_dp"></td>
			    <th>거래단위  <span class="red">*</span></th>
			    <td><input type="number" name="name" id="c_in_deal"></td>
			</tr>
			<tr>
			    <th>OS지원</th>
			    <td>
			        <input type="checkbox" id="c_ck_win"> 윈도우
			        <input type="checkbox" id="c_ck_and"> 안드로이드
			        <input type="checkbox" id="c_ck_ios"> IOS
			    </td>
			    <th>인증키</th>
			    <td><input type="text" name="name" id="c_in_cert"></td>
			</tr>
			<tr>
			    <th>수수료율 <span class="red">*</span></th>
			    <td><input type="text" name="name" id="c_in_cm"> % <p>*부가세를 포함하여 숫자만 입력해 주세요.</p></td>
			    <th>수수료율적용일자 <span class="red">*</span></th>
			    <td><input type="text" name="name" id="c_cmsnDate" size="10" readonly><p>*수수료 적용 시작일자만 입력해 주세요.</p></td>
			</tr>
			<tr>
			    <th>전환율 <span class="red">*</span></th>
			    <td>
			        <table class="point_table" border="0" cellpadding="0" cellspacing="0">
			            <tbody>
			            <tr>
			                <td>제공처</td>
			                <td></td>
			                <td>클립포인트</td>
			            </tr>
			            <tr>
			                <td><input type="text" name="name" id="c_in_ex" size="8"></td>
			                <td>:</td>
			                <td>1</td>
			            </tr>
			            </tbody>
			        </table>
			    </td>
			    <th>전환율적용일자 <span class="red">*</span></th>
			    <td><input type="text" name="name" id="c_exchDate" size="10" readonly><p>*전환율 적용 시작일자만 입력해 주세요.</p></td>
			</tr>
			<tr>
				<th>CI 경로</th>
				<td>
					<input type="text" name="name" id="c_imag_link" style="width:400px;" maxlength="100">
				</td>
				<th>포인트 규칙</th>
				<td>
					<input type="text" name="name" id="c_pnt_rule" style="width:400px;" maxlength="500" />
				</td>
			</tr>
			<tr>
				<th>포인트 접두사</th>
				<td>
					<input type="text" name="name" id="c_pnt_prfx" style="width:50px;text-transform:uppercase;" maxlength="2" />
				</td>
				<th>최소거래포인트</th>
				<td>
					<input type="text" name="name" id="c_min_avl_pnt" style="width:100px;" maxlength="5" />
				</td>
			</tr>
		</tbody>
	</table>
	<div class="btnalign">
		<ul>
			<li><a href="#!"><div class="btn_cancel">취소</div></a></li>
			<li><a href="#!"><div class="btn_save" onClick="createPntPrvdr()"> 저장</div></a></li>
		</ul>
	</div>
</div>