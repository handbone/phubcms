<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
 **********************************************************************************************
 * @desc : 포인트사용처 관리 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/cmpn/pntUseCmpnViewJs.jsp
 * @author ojh
 * @since 2018.08.14
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.14    ojh        최초생성
 * 
 *********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
var cmsnDate;
$(document).ready(function () {  
	var colNames = ['No', '사용처ID', '사용처명','사용처사업자번호','PG사명','PG제공사용처ID','수정자','수정일시','관리'];
	var colModel = [{ label: 'No', name: 'no', width: cWidth * 0.07, key:true }
		,{ label: '사용처ID'		,name: 'cprt_cmpn_id' 	,width: cWidth * 0.10 }
		,{ label: '사용처명'		,name: 'cprt_cmpn_nm' 	,width: cWidth * 0.15	,align:'left' }
		,{ label: '사용처사업자번호',name: 'biz_no'			,width: cWidth * 0.11}
		,{ label: 'PG사명'			,name: 'pg_cmpn_nm'		,width: cWidth * 0.15	,align:'left' }
		,{ label: 'PG제공사용처ID'	,name: 'pg_send_po_id'	,width: cWidth * 0.12	,align:'left' }
		,{ label: '수정자'			,name: 'mdfy_user_nm' 	,width: cWidth * 0.08 }
		,{ label: '수정일시'		,name: 'mdfy_dt' 		,width: cWidth * 0.12 }
		,{ label: '관리'			,name: ''  				,width: cWidth * 0.06	,formatter:mbutton }
	];	
	PHJQg.load('jqGrid','jqGridPager',colNames,colModel);
	// 엑셀다운로드
	$("#btn_excel").on("click", function(){
		var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");	
		if(conf){		
			var postData = {
				CPRT_CMPN_ID : $("#in_id").val(),
				CPRT_CMPN_NM : $("#in_nm").val(),
				BIZ_NO : $("#in_biz").val(),
				PG_CMPN_ID : $("#sel_pgId").val()
			};
			var colNames = ['No', '사용처ID', '사용처명','사용처사업자번호','PG사명','PG제공사용처ID','수정자','수정일시'];
			var colModel = [{ label: 'No', name: 'no' }
	       		,{ label: '사용처ID'		,name: 'cprt_cmpn_id' }
	       		,{ label: '사용처명'		,name: 'cprt_cmpn_nm' }
	       		,{ label: '사용처사업자번호',name: 'biz_no' }
	       		,{ label: 'PG사명'			,name: 'pg_cmpn_nm' }
	       		,{ label: 'PG제공사용처ID'	,name: 'pg_send_po_id' }
	       		,{ label: '수정자'			,name: 'mdfy_user_nm' }
	       		,{ label: '수정일시'		,name: 'mdfy_dt' }
			];
			PHJQg.excel('jqGridExcelDiv','${ViewRoot}/cmpn/jgPntUseCmpnExcel.do',postData,colNames,colModel,'포인트사용처');
		}
	});
	//달력 셋팅
	$("#in_cmsnDate").datepicker();
	$("#m_in_cmsnDate").datepicker();
	//달력 크기 조정
	$('img.ui-datepicker-trigger').css({
		'width':'35px'
		, 'height':'35px'
		, 'padding':'5px'
		, 'margin-bottom':'-12px'
		, 'display': 'inline-block'
		, '-webkit-background-size': '100%'
		, 'background-size': '100%'
		, 'vertical-align':'baseline'
	});
	//X버튼이나 취소 버튼 클릭시 팝업창 초기화
	$(".top_close,.btn_cancel").click(function () {
		resetPop();		
    });
});
//팝업창 초기화
function resetPop(){
	// 등록창 팝업
	$(':text[id^=in_]').val('');
	$('#sel_PgId').val("all");
	$(':radio[name=c_radio]').prop('checked', true);
	// 수정창 팝업
	$('td[id^=mdfy_]').html('');
	$(':text[id^=mdfy_]').val('');
	$(':radio[name=m_radio]').prop('checked', true);
	$(':checkbox[name=pbox]').prop('checked', true);
	$('#detailTable tbody').empty();
	popId = null;
}
//검색버튼 클릭
//파라미터 전달 후 jQgrid reload 
function search(){
	var postData = {
		CPRT_CMPN_ID : $("#in_id").val(),
		CPRT_CMPN_NM : $("#in_nm").val(),
		BIZ_NO : $("#in_biz").val(),
		PG_CMPN_ID : $("#sel_pgId").val()
	};
	PHJQg.reload('jqGrid','jqGridPager','${ViewRoot}/cmpn/jgPntUseCmpn.do',postData);
}
//formatter 버튼 만들어 주기 클릭시 No번호 param 전달
function mbutton(cellvalue,options,rowobject){
    return '<input type="button" class="table_btn" onclick="popClick('+rowobject.no+')" value="수정" />';
}
//[등록] 버튼 클릭 시
function createPopClick() {
	cprtPrvdrDetailPop('CREATE');
	PHFnc.layerPopOpen(1);
}
var popId;
//팝업창 오픈 및 상세 정보 가져오기 
function popClick(data){	
 	var row = $("#jqGrid").jqGrid('getRowData',data);	
	var params = {
		CPRT_CMPN_ID : row.cprt_cmpn_id
	};	
	PHFnc.ajax("${ViewRoot}/cmpn/ajaxGetPntUseCmpn.do",params,"POST","json",function(data, textStatus, jqXHR) {
		var item = data.rows;
		$('#mdfy_in_CmId').html(item.cprt_cmpn_id);
		$("#mdfy_in_CmNm").val(item.cprt_cmpn_nm);	
		$("#mdfy_sel_PgNm").val(item.pg_cmpn_id);	
		$("#mdfy_in_PgId").val(item.pg_send_po_id);
		$("#mdfy_in_biz1").val(item.biz_no.substring(0 ,3));
		$("#mdfy_in_biz2").val(item.biz_no.substring(3 ,5));
		$("#mdfy_in_biz3").val(item.biz_no.substring(5 ,10));
		$(':radio[value='+ item.rchr_pay_ind +']').prop('checked', true);
		$("#m_in_pgCm").val(item.pg_cmsn_rate);
		$("#m_in_ktCm").val(item.kt_cmsn_rate);
		$("#m_in_cmsnDate").val(item.rate_aply_strt_dd);
		$("#mdfy_rgst_dt").html(item.rgst_dt);
		$("#mdfy_rgst_id").html(item.rgst_user_nm);	
		$("#mdfy_mdfy_dt").html(item.mdfy_dt);
		$("#mdfy_mdfy_id").html(item.mdfy_user_nm);
		popId = item.cprt_cmpn_id;
		cmsnDate = item.rate_aply_strt_dd;
		
		cprtPrvdrDetailPop('MODIFY');
	},null,true,true,false);	
	
	PHFnc.layerPopOpen(2);
}
//제공처목록 가져오기
function cprtPrvdrDetailPop(flag) {
	var params = {
		PTYPE : flag	
	};
	if (flag == 'MODIFY') {
		params['CPRT_CMPN_ID'] = popId;
	}
	PHFnc.ajax("${ViewRoot}/cmpn/ajaxGetPgCprtPrvdr.do",params,"POST","json",function(data, textStatus, jqXHR) {
		$('#detailTable tbody').empty();
		for (var i=0; i<data.rows.length; i++) {
			var item = data.rows[i];
			var checked = (item.flag == 'Y' ? 'checked' : '');
			var sort_ord = (item.sort_ord == null) ? '' : item.sort_ord;
			$(	'<tr>' + 
					'<td><input type="checkbox" name="pbox" class="checkSelect" '+ checked +'></td>' +
					'<td style="text-align:left;white-space:nowrap;text-overflow:ellipsis;overflow:hidden;" title="'+ item.prvdr_nm +'">' + item.prvdr_nm + '</td>' +
					'<td><input type="text" name="ci_in" style="width:100%" value="'+ item.imag_link +'" autocomplete="off" /></td>' +
					'<td><input type="text" name="sort_ord" style="width:50px;" value="' + sort_ord + '" maxlength="2" /></td>' +
				'</tr>'
			).data('item', item).appendTo('#detailTable tbody');
			$(':text[name=sort_ord]').keypress(function(e) {
		    	var keycode = e.keyCode ? e.keyCode : e.which;
		    	if ((keycode >= 48 && keycode <= 57) || (keycode >= 96 && keycode <= 105)) {
		    		return true;
		    	} else {
		    		switch(keycode) {
		    			case 8:		// BACKSPACE
		    			case 9:		// TAB
		    				return true;
		    				break;
		    			default:
		    				e.preventDefault();
		    		}
		    	}
		    }).keyup(function(e) {
		    	var value = $(this).val().replace(/[^0-9]/g,"");
		    	$(this).val(value);
		    });
		}
	},null,true,true,false);
}
//포인트 사용처 관리 - 등록하기
function createPntUseCmpn(){	
	var biz = $("#in_BizNo_1").val()+$("#in_BizNo_2").val()+$("#in_BizNo_3").val();	
	var arr = [];
	jQuery.each($('.checkSelect:checked'), function(i, v) {
		var tr = $(this).parents('tr:eq(0)');
		var item = tr.data('item');
		item['imag_link'] = tr.find(':text[name=ci_in]:eq(0)').val();
		item['sort_ord'] = tr.find(':text[name=sort_ord]:eq(0)').val();
		arr.push(item);
	});
	if($.trim($("#in_CmNm").val()) == ""){
		alert("사용처명을 입력해 주세요.");
		return;
	}
	if($("#sel_PgId").val() == "all"){
		alert("PG사명을 선택해 주세요.");
		return;
	}
	if($.trim($("#in_PoId").val()) == ""){
		alert("PG제공사용처ID를 입력해 주세요.");
		return;
	}	
// 	if(!PHValid.bizNo(biz)){
// 		alert("올바르지 않은 사업자번호 형식입니다.");
// 		return;
// 	}		
	if($.trim($("#in_pgCm").val()) == ""){
		alert("PG 수수료율을 입력해 주세요.");
		return;
	}	
	if($.trim($("#in_ktCm").val()) == ""){
		alert("KT 수수료율을 입력해 주세요.");
		return;
	}	
	if($("#in_cmsnDate").val() == ""){
		alert("수수료율 적용일자를 선택해 주세요.");
		return;
	}	
	if(arr.length == 0){
		alert("지원 제공처를 선택해 주세요.");
		return;
	}	
	var conf = confirm("저장하시겠습니까?");	
	if(conf){
		var params = {
			CPRT_CMPN_NM 	: $("#in_CmNm").val()
			, PG_CMPN_ID    : $("#sel_PgId").val()
			, BIZ_NO 		: biz
			, RCHR_PAY_IND 	: $("input[name=c_radio]:checked").val()
			, PG_SEND_PO_ID : $("#in_PoId").val()
			, RATE_APLY_STRT_DD  : PHUtil.replaceAll($("#in_cmsnDate").val(),"-","")
			, KT_CMSN_RATE  : $("#in_pgCm").val()
			, PG_CMSN_RATE  : $("#in_ktCm").val()
			, PRVDR_LIST	: JSON.stringify(arr)
		};	
		PHFnc.ajax("${ViewRoot}/cmpn/ajaxCreatePntUseCmpn.do",params,"POST","json",function(data, textStatus, jqXHR) {
			if(data.eStat == 0){
				alert("등록하였습니다.");
				$(".top_close").click();
				search();
			} else if(data.eStat == 1){
				alert("이미 존재하는 사업자 번호입니다.");
			}
		},false,true,true,false);
	}
}
//포인트 사용처 관리 - 수정하기
function modifyPntUseCmpn(){	
	var biz = $("#mdfy_in_biz1").val()+$("#mdfy_in_biz2").val()+$("#mdfy_in_biz3").val();
	var arr = [];
	jQuery.each($('.checkSelect:checked'), function(i, v) {
		var tr = $(this).parents('tr:eq(0)');
		var item = tr.data('item');
		item['imag_link'] = tr.find(':text[name=ci_in]:eq(0)').val();
		item['sort_ord'] = tr.find(':text[name=sort_ord]:eq(0)').val();
		arr.push(item);
	});
	if($("#mdfy_in_CmNm").val() == ""){
		alert("사용처명을 입력해 주세요.");
		return;
	}
	if($("#mdfy_sel_PgNm").val() == "all"){
		alert("PG사명을 선택해 주세요.");
		return;
	}
	if($("#mdfy_in_PgId").val() == ""){
		alert("PG제공사용처ID를 입력해 주세요.");
		return;
	}	
// 	if(!PHValid.bizNo(biz)){
// 		alert("올바르지 않은 사업자번호 형식입니다.");
// 		return;
// 	}	
	if($("#m_in_pgCm").val() == ""){
		alert("PG 수수료율을 입력해 주세요.");
		return;
	}	
	if($("#m_in_ktCm").val() == ""){
		alert("KT 수수료율을 입력해 주세요.");
		return;
	}	
	if($("#m_in_cmsnDate").val() == ""){
		alert("수수료율 적용일자를 선택해 주세요.");
		return;
	}	
	if(arr.length == 0){
		alert("지원 제공처를 선택해 주세요.");
		return;
	}
	var cmsnDate_o = new Date(cmsnDate);
	var cmsnDate_n = new Date($("#m_in_cmsnDate").val());
	var cmsnDate_mdfy
	if(cmsnDate_o.getTime() > cmsnDate_n.getTime()){
		alert("수수료율 적용일자는 현재 적용일자 이전으로 선택할 수 없습니다.");
		return;
	} else if(cmsnDate_o.getTime() == cmsnDate_n.getTime()){
		//수수료율 변경 없을시 정보만 변경
		cmsnDate_mdfy = 0;
	} else if(cmsnDate == ""){
		//정보 변경시 기존 수수료율이 없을때
		cmsnDate_mdfy = 2;
	} else{
		//기존 수수료율 변경시
		cmsnDate_mdfy = 1;
	}
	var conf = confirm("저장하시겠습니까?");	
	if(conf){
		var params = {
			CPRT_CMPN_ID 	: $("#mdfy_in_CmId").html()
			, CPRT_CMPN_NM 	: $("#mdfy_in_CmNm").val()
			, PG_CMPN_ID    : $("#mdfy_sel_PgNm").val()
			, BIZ_NO 		: biz
			, RCHR_PAY_IND 	: $("input[name=m_radio]:checked").val()
			, PG_SEND_PO_ID : $("#mdfy_in_PgId").val()
			, RATE_APLY_STRT_DD  : PHUtil.replaceAll($("#m_in_cmsnDate").val(),"-","")
			, PG_CMSN_RATE  : $("#m_in_pgCm").val()
			, KT_CMSN_RATE  : $("#m_in_ktCm").val()
			, MDFY_CMSN_DATE : cmsnDate_mdfy
			, PRVDR_LIST	: JSON.stringify(arr)
		};	
		PHFnc.ajax("${ViewRoot}/cmpn/ajaxModifyPntUseCmpn.do",params,"POST","json",function(data, textStatus, jqXHR) {
			alert("수정하였습니다.");
			$(".top_close").click();
			search();
		},null,true,true,false);	
	}
}
 
</script>