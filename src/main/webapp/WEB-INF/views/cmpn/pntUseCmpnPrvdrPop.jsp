<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%>
<%--
 **********************************************************************************************
 * @desc : 포인트 사용처 제공처 팝업
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/cmpn/pntUseCmpnPrvdrPop.jsp
 * @author ojh
 * @since 2018.10.23
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.23    ojh        최초생성
 *
 **********************************************************************************************
 pntUseCmpnView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>
<div class="popup_front" style="width:800px;">
	<div class="popup_title">지원 포인트제공처<a href="#!"><div class="top_close2"></div></a></div>
	<div style="height:400px;overflow-y:auto;">
		<table id="detailTable" border="0" cellpadding="0" cellspacing="0" style="table-layout:fixed;">
			<thead>
	        	<tr>
	        		<th style="width:50px;">체크</th>
	        		<th style="width:150px;">포인트제공처</th>
	        		<th>제공처 CI경로</th>
	        		<th style="width:80px;">정렬순서</th>  
	        	</tr>      		
			</thead>
			<tbody />           
		</table>
	</div>
	<a href="#!"><div class="btn_close2">닫기</div></a>
</div>