 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--
 **********************************************************************************************
 * @desc : 포인트 제공처 수정 팝업
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/cmpn/prvdrModifyPop.jsp
 * @author ojh
 * @since 2018.10.01
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.01    ojh        최초생성
 *
 **********************************************************************************************
 pntPrvdrView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>
<!-- 포인트제공처 수정 시작-->
<div class="popup_content2">
	<div class="popup_title">포인트제공처 수정<a href="#!"><div class="top_close"></div></a></div>
	<table class="half_table" border="0" cellpadding="0" cellspacing="0">
		<tbody>
			<tr>
			    <th>제공처ID</th>
			    <td id="m_id"></td>
			    <th>제공처명  <span class="red">*</span></th>
			    <td><input type="text" name="name" id="m_in_nm"></td>
			</tr>
			<tr>
			    <th>포인트코드 <span class="red">*</span></th>
			    <td><input type="text" name="name" id="m_in_pntCd"></td>
			    <th>포인트명  <span class="red">*</span></th>
			    <td><input type="text" name="name" id="m_in_pntNm"></td>
			</tr>
			<tr>
			    <th>표시단위 <span class="red">*</span></th>
			    <td><input type="text" name="name" id="m_in_dp"></td>
			    <th>거래단위  <span class="red">*</span></th>
			    <td><input type="number" name="name" id="m_in_deal"></td>
			</tr>
			<tr>
			    <th>OS지원</th>
			    <td>
			        <input type="checkbox" id="m_ck_win"> 윈도우
			        <input type="checkbox" id="m_ck_and"> 안드로이드
			        <input type="checkbox" id="m_ck_ios"> IOS
			    </td>
			    <th>인증키</th>
			    <td><input type="text" name="name" id="m_in_cert"></td>
			</tr>
			<tr>
			    <th>수수료율 <span class="red">*</span> <br> <button onclick="cmsnHistPopClick()">이력조회</button> </th>
			    <td><input type="number" name="name" id="m_in_cm"> % <p>*부가세를 포함하여 숫자만 입력해 주세요.</p></td>
			    <th>수수료율적용일자 <span class="red">*</span></th>
			    <td><input type="text" name="name" id="m_cmsnDate" size="10" readonly><p>*수수료 적용 시작일자만 입력해 주세요.</p></td>
			</tr>
			<tr>
			    <th>전환율 <span class="red">*</span> <br>  <button onclick="exchHistPopClick()">이력조회</button></th>
			    <td>
			        <table class="point_table" border="0" cellpadding="0" cellspacing="0">
			            <tbody>
			            <tr>
			                <td>제공처</td>
			                <td></td>
			                <td>클립포인트</td>
			            </tr>
			            <tr>
			                <td><input type="number" name="name" id="m_in_ex" size="8"></td>
			                <td>:</td>
			                <td>1</td>
			            </tr>
			            </tbody>
			        </table>
			    </td>
			    <th>전환율적용일자 <span class="red">*</span></th>
			    <td><input type="text" name="name" id="m_exchDate" size="10" readonly><p>*전환율 적용 시작일자만 입력해 주세요.</p></td>
			</tr>
			<tr>
				<th>CI 경로</th>
				<td>
					<input type="text" name="name" id="m_imag_link" style="width:400px;" maxlength="100">
				</td>
				<th>포인트 규칙</th>
				<td>
					<input type="text" name="name" id="m_pnt_rule" style="width:400px;" maxlength="500" />
				</td>
			</tr>
			<tr>
				<th>포인트 접두사</th>
				<td>
					<input type="text" name="name" id="m_pnt_prfx" style="width:50px;text-transform:uppercase;" maxlength="2" />
				</td>
				<th>최소거래포인트</th>
				<td>
					<input type="text" name="name" id="m_min_avl_pnt" style="width:100px;" maxlength="5" />
				</td>
			</tr>
			<tr>
			    <th>등록자</th>
			    <td id="m_rId"></td>
			    <th>등록일시</th>
			    <td id="m_rDt"></td>
			</tr>
			<tr>
			    <th>수정자</th>
			    <td id="m_mId"></td>
			    <th>수정일시</th>
			    <td id="m_mDt"></td>
			</tr>
		</tbody>
	</table>
	<div class="btnalign">
		<ul>
			<li><a href="#!"><div class="btn_cancel">취소</div></a></li>
			<li><a href="#!"><div class="btn_save" onClick="modifyPntPrvdr()">저장</div></a></li>
		</ul>
	</div>
</div>