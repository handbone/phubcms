<%@  page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
 **********************************************************************************************
 * @desc : 포인트 사용처 관리
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/cmpn/pntUseCmpnView.jsp
 * @author ojh
 * @since 2018.08.14
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.14    ojh        최초생성
 * 2019.05.27    kimht        include 수정
 **********************************************************************************************
--%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<%-- global --%>
	<jsp:include page="/WEB-INF/views/include/globalVar.jsp" flush="false" />
	<%-- head --%>
	<jsp:include page="/WEB-INF/views/include/incHead.jsp" flush="false" />
</head>
<body>
<!-- loader 시작-->
<div class="loader" onclick="loader_close()">
    <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
</div>
<!-- loader 끝-->
<div id="wrap">	
<header>
	<%-- header --%>
	<jsp:include page="/WEB-INF/views/include/incHeader.jsp" flush="false" />
</header>
    <!-- 레프트 메뉴 해당페이지 표시 -->
    <style>..${sessionScope.USER_MENU.menu_id}{ color: #fff !important; background: #3f4759 !important; border-left: 3px solid #fff; cursor: hand;} </style>
    <!-- 레프트 메뉴 해당페이지 표시 끝 -->

    <!-- 포인트사용처 등록 시작-->
    <jsp:include page="./pntUseCmpnCreatePop.jsp" flush="false" />

    <!-- 포인트사용처 수정 시작-->
	<jsp:include page="./pntUseCmpnModifyPop.jsp" flush="false" />
	
	<!-- 포인트사용처 제공처 시작-->
	<jsp:include page="./pntUseCmpnPrvdrPop.jsp" flush="false" />

<!-- dash_board 시작-->
<section id="dash_board">
    <!--side_menu 시작-->
	<jsp:include page="/WEB-INF/views/include/incMenu.jsp" flush="false" />
    <!--side_menu 끝--> 

    <!--contents 시작-->
    <div class="contents mCustomScrollbar">
        <div class="page_title"><div class="page_name">${sessionScope.USER_MENU.menu_nm}</div><div class="page_location"><i class="fas fa-home"></i>${sessionScope.USER_MENU.mnu_path}</div></div>
        <div class="subject">
            <div class="page_search">
                <table class="search_tb" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="search_item">
                            <ul>
                                <li>사용처ID <input type="text" name="name" id="in_id"></li>
                                <li>사용처명 <input type="text" name="name" id="in_nm"></li>
                                <li>사용처사업자번호 <input type="text" name="name" id="in_biz"></li>
                                <li>PG사명
                                    <select id="sel_pgId">
			                        <option value="all">전체</option>
				                        <c:forEach items="${PG_CMPN}" var="code">
				                           	<option value="${code.pg_cmpn_id}">${code.pg_cmpn_nm}(${code.pg_cmpn_id})</option>
				                        </c:forEach>
			                        </select>
                                </li>
                            </ul>
                        </td>
                        <td class="btn_search">
                        	<button type="button" onClick="search()">조회</button>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="page_contents">
                <div class="btn_excel"><button id="btn_excel">엑셀다운</button> <button onclick="createPopClick();return false;">등록</button></div>
                <div id="content">
                    <table id="jqGrid"></table>
                    <div id="jqGridPager"></div>
                </div>
                <div id="jqGridExcelDiv" style="display:none">
                	<table id="jqGridExcel" ></table>
                </div>
                <!-- jqGrid  끝 -->
            </div>

        </div>

        <!-- 스크롤 때문에 여백 있어야함 -->
        <br>
        <br>
        <br>
        <br>
        <br>

        </div>
    <!--contents 끝-->
</section>
<!-- dash_board 끝--> 

    <!-- footer 시작-->
    <jsp:include page="/WEB-INF/views/include/incFooter.jsp" flush="false" />
    <section class="footer">	 
    </section>
    <!-- footer 끝-->
    
    <%-- js --%>
	<jsp:include page="./pntUseCmpnViewJs.jsp" flush="false" />
	
</body>
</html>