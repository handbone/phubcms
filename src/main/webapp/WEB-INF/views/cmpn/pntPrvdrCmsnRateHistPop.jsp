 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--
 **********************************************************************************************
 * @desc : 수수료 이력 조회
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/cmpn/pntPrvdrCmsnRateHistPop.jsp
 * @author ojh
 * @since 2018.10.01
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.01    ojh        최초생성
 *
 **********************************************************************************************
 pntPrvdrView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>
<!-- 수수료 이력조회 시작-->
    <div class="popup_front">
        <div class="popup_title">수수료 이력조회<a href="#!"><div class="top_close2"></div></a></div>
        <table class="info_table" border="0" cellpadding="0" cellspacing="0">
            <tbody>
            <tr>
                <th>제공처ID</th>
                <th>제공처명</th>
            </tr>
            <tr>
                <td id="c_id"></td>
                <td id="c_nm"></td>
            </tr>
            </tbody>
        </table>
        <table class="detail_table" border="0" cellpadding="0" cellspacing="0">
            <thead>
            <tr>
                <th>NO</th>
                <th>수수료율</th>
                <th>수수료율적용기간</th>
                <th>수정자</th>
                <th>수정일자</th>
            </tr>
            </thead>
            <tbody id="tBody">
            </tbody>
        </table>
        <a href="#!"><div class="btn_close2">닫기</div></a>
    </div>