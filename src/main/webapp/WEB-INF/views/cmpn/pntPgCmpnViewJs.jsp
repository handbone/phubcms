<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
 **********************************************************************************************
 * @desc : PG사 관리 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/cmpn/pntPgCmpnViewJs.jsp
 * @author ojh
 * @since 2018.10.04
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.04    ojh        최초생성
 * 
 **********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
$(document).ready(function () {  
	var colNames = ['No','PG사ID','PG사명','담당자명(담당자연락처)','수정자','수정일시','관리','담당자명','담당자연락처'];
	var colModel = [{ label: 'No', name: 'no', width: cWidth * 0.03 ,key:true }
		,{ label: 'PG사ID'					,name: 'pg_cmpn_id' 	,width: cWidth * 0.10 }
		,{ label: 'PG사명'					,name: 'pg_cmpn_nm' 	,width: cWidth * 0.20, align:'left' }
		,{ label: '담당자명(담당자연락처)'	,name: ''				,width: cWidth * 0.12, formatter:chrgr }
		,{ label: '수정자'					,name: 'mdfy_user_nm' 	,width: cWidth * 0.10 }
		,{ label: '수정일시'				,name: 'mdfy_dt' 		,width: cWidth * 0.12 }
		,{ label: '관리'					,name: ''  				,width: cWidth * 0.06, formatter:mbutton }
		,{ label: '담당자명'				,name: 'chrgr_nm'  		,hidden:true }
		,{ label: '담당자연락처'			,name: 'chrgr_tel'		,hidden:true }
	];	
	PHJQg.load('jqGrid','jqGridPager',colNames,colModel);
	
	// 엑셀다운로드
	$("#btn_excel").on("click", function(){
		var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");	
		if(conf){		
			var postData = {
				PG_CMPN_ID : $("#in_id").val(),
				PG_CMPN_NM : $("#in_nm").val()
			};
			var colNames = ['No', 'PG사ID', 'PG사명','담당자명','담당자연락처','수정자','수정일시',];
			var colModel = [{ label: 'No', name: 'no' }
	      		,{ label: 'PG사ID'			,name: 'pg_cmpn_id' }
	      		,{ label: 'PG사명'			,name: 'pg_cmpn_nm' }
	      		,{ label: '담당자명'		,name: 'chrgr_nm' }
	      		,{ label: '담당자연락처'	,name: 'chrgr_tel' }
	      		,{ label: '수정자'			,name: 'mdfy_user_nm' }
	      		,{ label: '수정일시'		,name: 'mdfy_dt' }
			];	
			PHJQg.excel('jqGridExcelDiv','${ViewRoot}/cmpn/jgPntPgCmpnExcel.do',postData,colNames,colModel,'PG사');
		}
	});
	
	//달력 셋팅
	$("#c_startDate").datepicker();
	$("#c_endDate").datepicker();
	$("#m_startDate").datepicker();
	$("#m_endDate").datepicker();
	
	//달력 크기 조정
	$('img.ui-datepicker-trigger').css({
		'width':'35px'
		, 'height':'35px'
		, 'padding':'5px'
		, 'margin-bottom':'-12px'
		, 'display': 'inline-block'
		, '-webkit-background-size': '100%'
		, 'background-size': '100%'
		, 'vertical-align':'baseline'
	});
	
	//X버튼이나 취소 버튼 클릭시 팝업창 초기화
	$(".top_close").click(function () {
		resetPop();		
    });
    $(".btn_cancel").click(function () {
    	resetPop();
    });
});	
function chrgr(cellvalue,options,rowobject){
	return rowobject.chrgr_nm+"<br>("+rowobject.chrgr_tel+")";
}

function resetPop(){
	//등록창 팝업
	$("#c_in_tel1").val("");
	$("#c_in_tel2").val("");
	$("#c_in_tel3").val("");
	$("#c_in_nm").val("");	
	$("#c_in_cert").val("");
	$("#c_startDate").val("");
	$("#c_endDate").val("");
	$("#c_in_chrNm").val("");	
	
	//수정창 팝업
	$("#m_in_tel1").val("");
	$("#m_in_tel2").val("");
	$("#m_in_tel3").val("");
	$("#m_id").html("");	
	$("#m_in_nm").val("");	
	$("#m_in_cert").val("");
	$("#m_startDate").val("");
	$("#m_endDate").val("");
	$("#m_in_chrNm").val("");	
	$("#m_rId").html("");
	$("#m_rDt").html("");
	$("#m_mId").html("");
	$("#m_mDt").html("");	
}

//검색버튼 클릭
//파라미터 전달 후 jQgrid reload 
function search(){
	var postData = {
		PG_CMPN_ID : $("#in_id").val(),
		PG_CMPN_NM : $("#in_nm").val()
	};
	PHJQg.reload('jqGrid','jqGridPager','${ViewRoot}/cmpn/jgPntPgCmpn.do',postData);
}

//formatter 버튼 만들어 주기 클릭시 No번호 param 전달
function mbutton(cellvalue,options,rowobject){
    return '<input type="button" class="table_btn" onclick="popClick('+rowobject.no+')" value="수정" />';
}

//<<<<<<<<<< 수정버튼 클릭 후 데이터 가져오기
//팝업창 오픈 및 상세 정보 가져오기 
function popClick(data){	
	var row = $("#jqGrid").jqGrid('getRowData',data);

	//누른 제공처 ID를 key로 상세 조회
	var params = {PG_CMPN_ID : row.pg_cmpn_id};
	PHFnc.ajax("${ViewRoot}/cmpn/ajaxGetPntPgCmpn.do",params,"POST","json",successAjaxGet,null,true,true,false);
	
	PHFnc.layerPopOpen(2);
}

function successAjaxGet(data, textStatus, jqXHR){
	var cnt = data.rows.chrgr_tel.split("-");	
	if(cnt != null){
		$("#m_in_tel1").val(cnt[0]);
		$("#m_in_tel2").val(cnt[1]);
		$("#m_in_tel3").val(cnt[2]);
	}
	$("#m_id").html(data.rows.pg_cmpn_id);	
	$("#m_in_nm").val(data.rows.pg_cmpn_nm);	
	$("#m_in_cert").val(data.rows.athn_key);
	$("#m_startDate").val(data.rows.pnt_aply_strt_dd);
	$("#m_endDate").val(data.rows.pnt_aply_end_dd);
	$("#m_in_chrNm").val(data.rows.chrgr_nm);
	$("#m_rId").html(data.rows.rgst_user_nm);
	$("#m_rDt").html(data.rows.rgst_dt);
	$("#m_mId").html(data.rows.mdfy_user_nm);
	$("#m_mDt").html(data.rows.mdfy_dt);	
}
function modifyPntPgCmpn(){
	var tel = $("#m_in_tel1").val()+"-"+$("#m_in_tel2").val()+"-"+$("#m_in_tel3").val();
	
	if($("#m_in_nm").val() == ""){
		alert("PG사명을 입력해 주세요.");
		return;
	}
	if($("#m_startDate").val() == ""){
		alert("사용가능일자를 선택해주세요.");
		return;
	}
	if($("#m_endDate").val() == ""){
		alert("사용가능일자를 선택해주세요.");
		return;
	}	
	if($("#m_in_chrNm").val() == ""){
		alert("담당자명을 입력해 주세요.");
		return;
	}	
	if(!PHValid.telNo(tel)){
		alert("연락처 핸드폰 번호 형식이 올바르지 않습니다.")
		return;
	}
	
	var conf = confirm("저장하시겠습니까?");	
	if(conf){
		var params = { 
			PG_CMPN_ID				: $("#m_id").html()
			, PG_CMPN_NM 			: $("#m_in_nm").val()	
			, ATHN_KEY 				: $("#m_in_cert").val()	
			, PNT_APLY_STRT_DD		: PHUtil.replaceAll($("#m_startDate").val(),"-","")
			, PNT_APLY_END_DD 		: PHUtil.replaceAll($("#m_endDate").val(),"-","")
			, CHRGR_NM 				: $("#m_in_chrNm").val()
			, CHRGR_TEL 			: tel
		};	
		PHFnc.ajax("${ViewRoot}/cmpn/ajaxModifyPntPgCmpn.do",params,"POST","json",successModifyAjax,null,true,true,false);
	}
}
//성공 func
function successModifyAjax(data, textStatus, jqXHR){
	alert("수정하였습니다.");
	$(".top_close").click();
	search();
}
//PG사 등록
function createPntPgCmpn(){
	var tel = $("#c_in_tel1").val()+"-"+$("#c_in_tel2").val()+"-"+$("#c_in_tel3").val();
	
	if($("#c_in_nm").val() == ""){
		alert("PG사명을 입력해 주세요.");
		return;
	}
	if($("#c_startDate").val() == ""){
		alert("사용가능일자를 선택해주세요.");
		return;
	}
	if($("#c_endDate").val() == ""){
		alert("사용가능일자를 선택해주세요.");
		return;
	}	
	if($("#c_in_chrNm").val() == ""){
		alert("담당자명을 입력해 주세요.");
		return;
	}	
	if(!PHValid.telNo(tel)){
		alert("연락처 핸드폰 번호 형식이 올바르지 않습니다.")
		return;
	}
	
	var conf = confirm("저장하시겠습니까?");	
	if(conf){
		var params = { 
			PG_CMPN_NM 				: $("#c_in_nm").val()	
			, ATHN_KEY 				: $("#c_in_cert").val()	
			, PNT_APLY_STRT_DD		: PHUtil.replaceAll($("#c_startDate").val(),"-","")
			, PNT_APLY_END_DD 		: PHUtil.replaceAll($("#c_endDate").val(),"-","")
			, CHRGR_NM 				: $("#c_in_chrNm").val()
			, CHRGR_TEL 			: tel
		};	
		PHFnc.ajax("${ViewRoot}/cmpn/ajaxCreatePntPgCmpn.do",params,"POST","json",successCreateAjax,null,true,true,false);
	}	
}

//성공 func
function successCreateAjax(data, textStatus, jqXHR){
	alert("등록하였습니다.");
	$(".top_close").click();
	search();
}
 
</script>