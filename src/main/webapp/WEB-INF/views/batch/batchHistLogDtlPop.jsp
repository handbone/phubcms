<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--
 **********************************************************************************************
 * @desc : 클립포인트 대사파일 내역 팝업
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/batch/batchHistLogDtlPop.jsp
 * @author kimht
 * @since 2019.05.20
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2019.5.17			kimht			최초생성
 *
 **********************************************************************************************
clipPntCmprHistView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>    
<!-- 대사파일 내역 시작 -->
<style type="text/css">
.popup_content4 .btn_excel button { 
	color: #fff; 
	padding: 5px 14px; 
	margin-top: 10px; 
	font-size: 15px; 
	font-weight: 400; 
	letter-spacing: -1px; 
	background: green; 
	border: none; 
	text-align: center;  
}
.popup_content4 #content_p4 { padding: 0 !important; margin-top: 30px; }
.popup_content4 #content_p4 #gbox_jqGrid4 { 
	padding-top: 0px !important; 
	width: 100% !important;
	padding: 0 !important;
	margin: 0 !important; 
}
.popup_content4 #content_p4 .ui-jqgrid-htable { margin-top: 0px !important; }
.popup_content4 #content_p4 .jqgfirstrow td { 
	height: 0px !important;
	padding: 0 !important;
	border-bottom: 0px; 
}
.popup_content4 #content_p4 .ui-jqgrid-bdiv { margin-top: -20px !important; }
.popup_content4 #content_p4 table th {
	padding: 5px;
	color: #3c3c3c;
	font-weight: 600;
	background: #f2f2f2;
	text-align: center;
	vertical-align: middle;
	line-height: 20px;
	letter-spacing: -1px;
	font-size: 14px;
	border-right: 1px solid #e4e4e4;
	border-bottom: 1px solid #e4e4e4; 
}
.popup_content4 #content_p4 table td {
	padding: 5px;
	color: #999;
	text-align: center;
	vertical-align: middle;
	line-height: 20px;
	font-size: 14px;
	border-right: 1px solid #e4e4e4;
	border-bottom: 1px solid #e4e4e4; 
}
.popup_content4 #content_p4 table td#jqGridPager4_center { border: none !important; width: 200px !important; }
.popup_content4 #content_p4 table td#jqGridPager4_center table { border: none !important; }
.popup_content4 #content_p4 table td#jqGridPager4_center table tr td { border: none !important; }
.popup_content4 #content_p4 table td#jqGridPager4_left { border: none !important; border-right: none; }
.popup_content4 #content_p4 table td#jqGridPager4_left tr td { border: none !important; }
.popup_content4 #content_p4 table td#jqGridPager4_right { border: none !important; }
.popup_content4 #content_p4 table td#jqGridPager4_right tr td { border: none !important; }
.popup_content4 #content_p4 table .ui-common-table { width: 100% !important; }
.popup_content4 #content_p4 table .ui-jqgrid .ui-jqgrid-hbox { padding-right: 0px !important; }
.popup_content4 #content_p4 table td.ui-state-default ui-th-column ui-th-ltr { width: 0px !important; }
.popup_content4 #content_p4 table td#prev_jqGridPager4 {
	position: relative;
	top: 5px;
	width: 20px !important;
	height: 20px !important;
	background: url(/resources/img/prev.png) no-repeat !important; 
}
.popup_content4 #content_p4 table td#next_jqGridPager4 {
	position: relative;
	top: 5px;
	width: 20px !important;
	height: 20px !important;
	background: url(/resources/img/next.png) no-repeat !important; 
}
.popup_content4 #content_p4 table td#first_jqGridPager4 {
	position: relative;
	top: 5px;
	width: 20px !important;
	height: 20px !important;
	background: url(/resources/img/pprev.png) no-repeat !important; 
}
.popup_content4 #content_p4 table td#last_jqGridPager4 {
	position: relative;
	top: 5px;
	width: 20px !important;
	height: 20px !important;
	background: url(/resources/img/nnext.png) no-repeat !important; 
}
.popup_content4 #content_p4 table td.ui-pg-button span { display: none; }
.popup_content4 #content_p4 table button { color: #3c3c3c; }
.popup_content4 #content_p4 table .btn_sc {
	color: #3c3c3c;
	padding: 2px 10px;
	font-size: 14px;
	font-weight: 400;
	letter-spacing: -1px;
	background: #f2f2f2;
	border: 1px solid #e4e4e4;
	text-align: center; }
.popup_content4 #content_p4 button { color: #3c3c3c; }
.popup_content4 #content_p4 .btn_sc {
	color: #3c3c3c;
	padding: 2px 10px;
	font-size: 14px;
	font-weight: 400;
	letter-spacing: -1px;
	background: #f2f2f2;
	border: 1px solid #e4e4e4;
	text-align: center; 
}
</style>
<div class="popup_content4">
	<div class="popup_title">상세로그 내역<a href="#!"><div class="top_close4"></div></a></div>
		<table class="log_info_table" border="0" cellpadding="0" cellspacing="0">
		    <thead>
		        <tr>
		        	<th>로그번호</th>
		            <th>Task</th>
		            <th>대상건수</th>
		            <th>처리건수</th>
		            <th>대상파일경로</th>
		            <th>처리파일경로</th>
		            <th>작업시작일시</th>
		            <th>작업종료일시</th>
		            <th>결과코드</th>
		        </tr>
		    </thead>
		    <tbody></tbody>
		</table>
		<div class="btn_excel" style="float:right;"><button id="btn_excel4">엑셀다운</button></div>
		<div style="clear:both;"></div>
		<div id="content_p4" style="margin-top:10px;">
			<table id="jqGrid4"></table>
			<div id="jqGridPager4"></div>
			<div id="jqGridExcelDiv4" style="display:none">
				<table id="jqGridExcel4" ></table>
			</div>
		</div>
		<a href="#!"><div class="btn_close4">닫기</div></a>
	</div>	
<!-- 대사파일 내역 내역 끝-->
</div>