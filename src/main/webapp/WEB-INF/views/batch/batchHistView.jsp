<%@  page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
 **********************************************************************************************
 * @desc : 배치작업이력조회
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/batch/batchHistView.jsp
 * @author bmg
 * @since 2018.09.28
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.28    bmg        최초생성
 * 2019.05.27    kimht        include 수정
 **********************************************************************************************
--%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<%-- global --%>
	<jsp:include page="/WEB-INF/views/include/globalVar.jsp"  flush="false" /> 
	<%-- head --%>
	<jsp:include page="/WEB-INF/views/include/incHead.jsp" flush="false" />
</head>
<body>
<!-- loader 시작-->
<div class="loader" onclick="loader_close()">
    <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
</div>
<!-- loader 끝-->
<div id="wrap">	
<header>
	<%-- header --%>
	<jsp:include page="/WEB-INF/views/include/incHeader.jsp" flush="false" />
</header>
    <!-- 레프트 메뉴 해당페이지 표시 -->
    <style>.${sessionScope.USER_MENU.menu_id}{ color: #fff  !important; background: #3f4759 !important; border-left: 3px solid #fff; cursor: hand;}</style>
    <!-- 레프트 메뉴 해당페이지 표시 끝 -->
    
    <!-- popup 시작-->
    <jsp:include page="./batchHistDtlPop.jsp" flush="false" /> 
    <jsp:include page="./batchHistLogDtlPop.jsp" flush="false" />   
    <jsp:include page="./batchHistExecPop.jsp" flush="false" />   
    <!-- popup 끝-->
	
<!-- dash_board 시작-->
<section id="dash_board">
    <!--side_menu 시작-->
	<jsp:include page="/WEB-INF/views/include/incMenu.jsp" flush="false" />
    <!--side_menu 끝-->
	
	<!--contents 시작-->
    <div class="contents mCustomScrollbar">
        <div class="page_title"><div class="page_name">${sessionScope.USER_MENU.menu_nm}</div><div class="page_location"><i class="fas fa-home"></i>${sessionScope.USER_MENU.mnu_path}</div></div>
        <div class="subject">
            <div class="page_search">
                <table class="search_tb" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="search_item">
                            <ul>
                                <li>대사일자 <input type="text" id="start_date" readonly> ~ <input type="text" id="end_date" readonly></li>
                                <li>작업명 
	                                <select id="sel_wrkn_nm">
										<option value="all">전체</option>
										<c:forEach items="${BTCH_JOB_CD}" var="code">
											<option value="${code.dtlCd}">${code.dtlCdNm}</option>
										</c:forEach>
									</select>
                                <li>전문파일명 <input type="text" name="doc_file_nm" maxlength="20"></li>
                                <li>오류코드
                                    <select id="rslt_code_type" style="width:50px;">
                                        <option value="NOT">!=</option>
                                        <option value="EQ">=</option>
                                    </select>
                                    <input type="text" name="rslt_code">
                                </li>
                            </ul>
                        </td>
                        <td class="btn_search">
                            <button type="button" onclick="search();">조회</button>
                        </td>
                    </tr>
                </table>

            </div>
             <div class="page_contents">
                <div class="btn_excel"><button id="btn_exec" style="margin-right:10px;">수동실행</button><button id="btn_excel">엑셀다운</button></div>
                <div id="content">
                    <table id="jqGrid"></table>
                    <div id="jqGridPager"></div>
                </div>
				<div id="jqGridExcelDiv" style="display:none">
                	<table id="jqGridExcel" ></table>
                </div>
                <!-- jqGrid  끝 -->
            </div>
        </div>

        <!-- 스크롤 때문에 여백 있어야함 -->
        <br>
        <br>
        <br>
        <br>
        <br>

    </div>
    <!--contents 끝-->
    
</section>
<!-- dash_board 끝--> 

    <!-- footer 시작-->
    <jsp:include page="/WEB-INF/views/include/incFooter.jsp" flush="false" />
    <section class="footer">	 
    </section>
    <!-- footer 끝-->
    
    <%-- js --%>
	<jsp:include page="./batchHistViewJs.jsp" flush="false" />
	
</body>
</html>