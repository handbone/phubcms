<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--
 **********************************************************************************************
 * @desc : 작업배치이력 상세 팝업
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/batch/batchHistDtlPop.jsp
 * @author bmg
 * @since 2018.09.28
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.28    bmg        최초생성
 * 2019.05.21    kimht     수정
 **********************************************************************************************
batchHistView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>
<div class="popup_content">
	<div class="popup_title">상세거래요청내역<a href="#!"><div class="top_close"></div></a></div>
	<table class="info_table" border="0" cellpadding="0" cellspacing="0">
	    <thead>
	        <tr>
	        	<th>일대사번호</th>
	            <th>대사일자</th>
	            <th>작업명</th>
	            <th>전문파일명</th>
	            <th>총거래건수</th>
	            <th>결과코드</th>
	            <th>결과메시지</th>
	            <th>작업시작일시</th>
	            <th>작업종료일시</th>
	        </tr>
	    </thead>
	    <tbody />
	</table>
	<table class="detail_table" id="detail_table_desc" border="0" cellpadding="0" cellspacing="0">
	    <thead>
	        <tr>
	        	<th>로그번호</th>
	            <th>Task</th>
	            <th>대상건수</th>
	            <th>처리건수</th>
	            <th>대상파일경로</th>
	            <th>처리파일경로</th>
	            <th>작업시작일시</th>
	            <th>작업종료일시</th>
	            <th>결과코드</th>
	            <th>결과메시지</th>
	            <th>상세로그내역<th>
	        </tr>
	    </thead>
	    <tbody></tbody>
	</table>
	<a href="#!"><div class="btn_close">닫기</div></a>
</div>
<div id="myPopup" class="popup">
</div>