<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%>
<%--
 **********************************************************************************************
 * @desc : 배치 수동 실행 팝업
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/batch/batchHistExecPop.jsp
 * @author bmg
 * @since 2018.10.24
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.24    bmg        최초생성
 *
 **********************************************************************************************
batchHistView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>
<div class="popup_content2">
	<div class="popup_title">배치수동 실행<a href="#!"><div class="top_close"></div></a></div>
	<table class="half_table" border="0" cellpadding="0" cellspacing="0">
		<tbody>
			<tr>
				<th>배치작업</th>
				<td>
					<select id="btch_job_cd">
						<option value="">선택</option>
						<c:forEach items="${BTCH_JOB_CD}" var="code">
							<option value="${code.dtlCd}">${code.dtlCdNm}</option>
						</c:forEach>
					</select>
				</td>
				<th>실행일자</th>
				<td>
					<input type="text" id="exec_date" readonly>
				</td>
			</tr>
		</tbody>
	</table>
	<div style="margin-top:5px;display:inline-block;color:gray;font-size:9pt">※ 실행 시 실행일자에 해당하는 기존 데이터는 모두 삭제 후 수행됩니다.</div>
	<div class="btnalign">
	    <ul>
	        <li><a href="#!"><div class="btn_cancel">취소</div></a></li>
	        <li><a href="#!"><div class="btn_save" onclick="batchProcExec();">실행</div></a></li>
	    </ul>
	</div>
</div>
