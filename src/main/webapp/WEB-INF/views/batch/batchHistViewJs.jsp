<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="com.olleh.pHubCMS.common.components.Constant"%>
<%--
 **********************************************************************************************
 * @desc : 배치작업이력조회 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/batch/batchHistViewJs.jsp
 * @author bmg
 * @since 2018.09.28
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.28    bmg        최초생성
 * 2019.05.20	 kimht		수정(상세보기추가)
 **********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
$(document).ready(function () { 
	var colNames = ['No','일대사번호', '대사일자', '작업명','전문파일명','총거래<br>건수','일치<br>건수','불일치<br>건수','작업시작일시','작업종료일시', '결과<br>코드', '결과메시지', '등록자ID', '등록일시', '상세작업<br>내역'];
	var colModel = [{ label: 'No'	,name: 'no'				,width: cWidth * 0.05	, key:true }
        ,{ label: '일대사번호'		,name: 'dd_cmpr_no'		,width: cWidth * 0.10 	}
        ,{ label: '대사일자'		,name: 'cmpr_dd' 		,width: cWidth * 0.10 	,formatter:'string',formatoptions:{srcformat:'Ymdd',newformat:'Y-mm-dd'}}
        ,{ label: '작업명'			,name: 'wrkn_nm' 		,width: cWidth * 0.10 	}
        ,{ label: '전문파일명'		,name: 'doc_file_nm'	,width: cWidth * 0.14	,align:'left' }
        ,{ label: '총거래건수'		,name: 'ttl_deal_cnt'	,width: cWidth * 0.05	,align:'right',formatter:'integer',formatoptions:{thousandsSeparator:','} }
        ,{ label: '일치건수'		,name: 'mtch_cnt'		,width: cWidth * 0.05	,align:'right',formatter:'integer',formatoptions:{thousandsSeparator:','} }
        ,{ label: '불일치건수'		,name: 'no_acr_cnt' 	,width: cWidth * 0.05	,align:'right',formatter:'integer',formatoptions:{thousandsSeparator:','} }
        ,{ label: '작업시작일시'	,name: 'strt_dt' 		,width: cWidth * 0.12 	}
        ,{ label: '작업종료일시'	,name: 'end_dt' 		,width: cWidth * 0.12 	}
        ,{ label: '결과코드'		,name: 'rslt_code' 		,width: cWidth * 0.05 	}
        ,{ label: '결과메시지'		,name: 'rslt_msg' 		,width: cWidth * 0.12	,align: 'left' }
        ,{ label: '등록자ID'		,name: 'rgst_user_id' 	,width: cWidth * 0.08 	}
        ,{ label: '등록일시'		,name: 'rgst_dt' 		,width: cWidth * 0.12 	}
        ,{ label: '상세작업내역'	,name: 'c' 				,width: cWidth * 0.06	,formatter:mbutton }
	];
	PHJQg.load('jqGrid','jqGridPager',colNames,colModel);
	
	$('#btn_excel').on('click', function() {
		if (PHJQg.size('jqGrid') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
		if (!validateDate()) {
			return;
		}
		var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");
		if (conf) {
			var postData = {
				START_DATE			: $('#start_date').val()
				, END_DATE			: $('#end_date').val()
				, WRKN_NM			: $('#sel_wrkn_nm').val()
				, DOC_FILE_NM		: $(':text[name=doc_file_nm]').val()
				, RSLT_CODE_TYPE	: $('#rslt_code_type').val()
				, RSLT_CODE			: $(':text[name=rslt_code]').val()
			};
			var colNames = ['No','일대사번호', '대사일자', '작업명','전문파일명','총거래<br>건수','일치<br>건수','불일치<br>건수','작업시작일시','작업종료일시', '결과<br>코드', '결과메시지', '등록자ID', '등록일시', '상세작업<br>내역'];
			var colModel = [{ label: 'No'	,name: 'no'				,width: cWidth * 0.05	, key:true }
		        ,{ label: '일대사번호'		,name: 'dd_cmpr_no'		,width: cWidth * 0.10 	}
		        ,{ label: '대사일자'		,name: 'cmpr_dd' 		,width: cWidth * 0.10 	,formatter:'string',formatoptions:{srcformat:'Ymdd',newformat:'Y-mm-dd'}}
		        ,{ label: '작업명'			,name: 'wrkn_nm' 		,width: cWidth * 0.10 	}
		        ,{ label: '전문파일명'		,name: 'doc_file_nm'	,width: cWidth * 0.14	,align:'left' }
		        ,{ label: '총거래건수'		,name: 'ttl_deal_cnt'	,width: cWidth * 0.05	,align:'right',formatter:'integer',formatoptions:{thousandsSeparator:','} }
		        ,{ label: '일치건수'		,name: 'mtch_cnt'		,width: cWidth * 0.05	,align:'right',formatter:'integer',formatoptions:{thousandsSeparator:','} }
		        ,{ label: '불일치건수'		,name: 'no_acr_cnt' 	,width: cWidth * 0.05	,align:'right',formatter:'integer',formatoptions:{thousandsSeparator:','} }
		        ,{ label: '작업시작일시'	,name: 'strt_dt' 		,width: cWidth * 0.12 	}
		        ,{ label: '작업종료일시'	,name: 'end_dt' 		,width: cWidth * 0.12 	}
		        ,{ label: '결과코드'		,name: 'rslt_code' 		,width: cWidth * 0.05 	}
		        ,{ label: '결과메시지'		,name: 'rslt_msg' 		,width: cWidth * 0.12	,align: 'left' }
		        ,{ label: '등록자ID'		,name: 'rgst_user_id' 	,width: cWidth * 0.08 	}
		        ,{ label: '등록일시'		,name: 'rgst_dt' 		,width: cWidth * 0.12 	}
		        ,{ label: '상세작업내역'	,name: 'c' 				,width: cWidth * 0.06	,formatter:mbutton }
			];
			PHJQg.excel('jqGridExcelDiv','${ViewRoot}/btch/jgBatchHistExcel.do',postData,colNames,colModel,'배치작업이력');
		}
	});

	$('#btn_exec').on('click', function() {
		PHFnc.layerPopOpen(2);
	});
	
	//검색일자 달력 셋팅
	$("#exec_date").datepicker();
	var dt = new Date();
	dt.setDate(dt.getDate()-1);
	setDate(dt,dt);
	
	$(':text').keydown(function(e) {
		if (e.keyCode == 32) {
			return false;
		}
	});
});	
//검색버튼 클릭
function search() {
	if (!validateDate()) {
		return;
	}
	var postData = {
		START_DATE			: $('#start_date').val()
		, END_DATE			: $('#end_date').val()
		, WRKN_NM			: $('#sel_wrkn_nm').val()
		, DOC_FILE_NM		: $(':text[name=doc_file_nm]').val()
		, RSLT_CODE_TYPE	: $('#rslt_code_type').val()
		, RSLT_CODE			: $(':text[name=rslt_code]').val()
	};
	PHJQg.reload('jqGrid','jqGridPager','${ViewRoot}/btch/jgBatchHist.do',postData);
}
//formatter 버튼 만들어 주기 클릭시 No번호 param 전달
function mbutton(cellvalue,options,rowobject){
	return '<input type="button" class="table_btn" onclick="detailPopClick('+rowobject.no+')" value="확인" />';
}
// 기간 조건 체크
function validateDate() {
	var sDate = $("#start_date").val();
	var eDate = $("#end_date").val();
	if (!PHUtil.isEmpty(sDate) && !PHValid.date(PHUtil.replaceAll(sDate,'-',''))) {
		alert('시작일자가 유효한 날짜 형식이 아닙니다');
		$('#start_date').val('');
		$('#start_date').focus();
		return false;
	}
	if (!PHUtil.isEmpty(eDate) && !PHValid.date(PHUtil.replaceAll(eDate,'-',''))) {
		alert('종료일자가 유효한 날짜 형식이 아닙니다');
		$('#end_date').val('');
		$('#end_date').focus();
		return false;
	}
	var startDate = new Date(sDate);
	var endDate = new Date(eDate);
	if (startDate.getTime() > endDate.getTime()) {		
		alert('뒤의 날짜는 앞의 날짜 이전으로 검색할 수 없습니다.')
		return false;
	} else {
		var valDate = new Date(sDate);
		valDate.setMonth(valDate.getMonth()+1);
		if (endDate.getTime() > valDate.getTime()) {
			alert('검색 기간은 한달을 넘을 수 없습니다.');
			return false;
		}
	}
	return true;
}

var popInfo;
// 상세 작업내역 팝업 호출
function detailPopClick(data) {
	var row = $("#jqGrid").jqGrid('getRowData',data);
	popInfo = row;
	
	
	//누른 창 번호를 key로 상세 조회
	var params = { DD_CMPR_NO : row.dd_cmpr_no };
	PHFnc.ajax("${ViewRoot}/btch/ajaxGetBatchHistDtl.do",params,"POST","json",successAjaxGetDetail,null,true,true,false);
}

function initBatchHistDtlLogPop() {
	var colNames = ['포인트허브<br>거래번호','포인트허브<br>쿠폰번호','핀번호','고객<br>아이디','거래금액','결과코드','결과메시지','등록일시'];
	var colModel = [{ label: '포인트허브 거래번호'			,name: 'phub_tr_no'		,width: 80 }
		,{ label: '포인트허브 쿠폰번호'			,name: 'phub_cpn_no',width: 100 }
		,{ label: '핀번호'						,name: 'pin_no'		,width: 70 }
		,{ label: '고객 아이디'					,name: 'cust_id'	,width: 70	,align:'right'}
		,{ label: '거래금액'					,name: 'tr_amt'		,width: 40	,align:'right', formatter:'integer', formatoptions:{thousandsSeparator:','} }
		,{ label: '결과코드'					,name: 'rslt_code'	,width: 50 }
		,{ label: '결과메시지'					,name: 'rslt_msg'	,width: 180	,align:'center' }
		,{ label: '등록일시'					,name: 'rgst_dt'	,width: 90 ,formatter:'string',formatoptions:{srcformat:'Ymdd',newformat:'Y-mm-dd'}}
	];
	PHJQg.load('jqGrid4','jqGridPager4',colNames,colModel);
}

//상세로그내역 조회
function dtlGetDetail(data) {
	initBatchHistDtlLogPop();
	
	//이전 로그내역정보를 상위에 표시하기 위한 배열
	var tdArr = new Array();
	var row = $("#"+data).children();
	
	row.each(function(i){	
		tdArr.push(row.eq(i).html());
	});
	
	$('.log_info_table tbody').empty();
	$(
		'<tr>' +
			'<td>' + tdArr[0] +'</td>' +
			'<td style="text-align:left;">' + tdArr[1] + '</td>' +
			'<td>' + tdArr[2] + '</td>' +
			'<td style="text-align:right;">' + PHUtil.setComma(tdArr[3]) + '</td>' +
			'<td>' + tdArr[4] + '</td>' +
			'<td style="text-align:left;">' + tdArr[5] + '</td>' +
			'<td>' + tdArr[6].replace('<br>', '&nbsp;') + '</td>' +
			'<td>' + tdArr[7].replace('<br>', '&nbsp;') + '</td>' +
			'<td>' + tdArr[8] + '</td>' +
		'</tr>' 
	).appendTo($('.log_info_table tbody'));
	
	var params = {
		LOG_NO: data
	};
	
	PHJQg.reloadCallback('jqGrid4','jqGridPager4','${ViewRoot}/btch/ajaxGetBatchHistDtlLog.do', params, function(o) {
		$(o).jqGrid('setGridWidth', $("#content_p4").width());
		$('.popup_content4').css({
	 		top: Math.max(0, (($(window).height() - $('.popup_content4').outerHeight()) / 2) + $(window).scrollTop()) + "px"
	 	});
	});
	PHFnc.layerPopOpen(4);
	
	//엑셀 다운로드 - 배치상세로그내역
	$('#btn_excel4').off().on('click', function() {
		if (PHJQg.size('jqGrid4') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
		else {
			var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");
			if (conf) {
				var postData = {
					LOG_NO: data
				};
				var colNames = $('#jqGrid4').jqGrid('getGridParam','colNames');
				var colModel = $('#jqGrid4').jqGrid('getGridParam','colModel');
				var colNamesExcel = $.map(colNames, function(o,i) {
	    			return o.replace(/(<br>)/g,'');
		    	});
				PHJQg.excelPopup('jqGridExcelDiv4','jqGridExcel4','${ViewRoot}/btch/jgBatchHistDtlExcel.do',postData,colNamesExcel,colModel,'배치상세로그내역');
			}	
		}
	});
}



function successAjaxGetDetail(data, textStatus, jqXHR) {
	
	$('.info_table tbody').empty();
	$(
		'<tr>' +
			'<td>' + popInfo.dd_cmpr_no + '</td>' +
			'<td>' + popInfo.cmpr_dd + '</td>' +
			'<td style="text-align:left;">' + popInfo.wrkn_nm + '</td>' +
			'<td>' + popInfo.doc_file_nm + '</td>' +
			'<td style="text-align:right;">' + PHUtil.setComma(popInfo.ttl_deal_cnt) + '</td>' +
			'<td>' + popInfo.rslt_code + '</td>' +
			'<td style="text-align:left;">' + popInfo.rslt_msg + '</td>' +
			'<td>' + popInfo.strt_dt + '</td>' +
			'<td>' + popInfo.end_dt + '</td>' +
		'</tr>' 
	).appendTo($('.info_table tbody'));
	popInfo = {};
	$('.detail_table tbody').empty();
	for (var i=0; i<data.rows.length; i++) {
		var item = data.rows[i];
		$(
			'<tr role="row" id="'+item.log_no+'">' +
				'<td>'+ item.log_no +'</td>' +
				'<td style="text-align:left;">'+ item.task_id +'</td>' +
				'<td style="text-align:right;">'+ PHUtil.setComma(item.tgt_cnt) +'</td>' +
				'<td style="text-align:right;">'+ PHUtil.setComma(item.cmpr_cnt) +'</td>' +
				'<td style="text-align:left;">'+ item.tgt_file_path +'</td>' +
				'<td style="text-align:left;">'+ item.cmpr_file_path +'</td>' +
				'<td>'+ item.strt_dt.replace(' ', '<br>') +'</td>' +
				'<td>'+ item.end_dt.replace(' ', '<br>') +'</td>' +
				'<td>'+ item.rslt_code +'</td>' +
				'<td>'+ item.rslt_msg +'</td>' +
				'<td>'+ '<input type="button" class="dtl_table_btn" onclick=dtlGetDetail("'+item.log_no+'") value="확인" />' +'</td>' +
			'</tr>'
		).appendTo($('.detail_table tbody'));
	}
	PHFnc.layerPopOpen(1);
}
// 수동 실행 
function batchProcExec() {
	if ($('#btch_job_cd').val() == '') {
		alert('배치작업을 선택해 주세요');
		return;
	}
	if ($('#exec_date').val() == '') {
		alert('실행일자를 입력해 주세요');
		return;
	}
	var conf = confirm("수동 실행 하시겠습니까?");
	if (conf) {
		var params = {
			btchId: $('#btch_job_cd').val(),
			cmprDd: $('#exec_date').val().replace(/-/gi, '')
		};
		PHFnc.ajax("${ViewRoot}/btch/batchManProc.do",params,"POST","json",function(data, textStatus, jqXHR) {
			alert("수동 실행하였습니다.");
			$(".top_close").click();
		},null,true,true,false);
	}
}
</script>