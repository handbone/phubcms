<%@  page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%--
 **********************************************************************************************
 * @desc : 에러화면 (custom)
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/common/errorCustom.jsp
 * @author lys
 * @since 2018.10.25
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.25   lys        최초생성
 
 * </pre>
 **********************************************************************************************
--%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<%-- Head --%>
	<jsp:include page="/WEB-INF/views/include/globalVar.jsp" flush="false" />
	
	<%-- Head --%>
	<jsp:include page="/WEB-INF/views/include/incHead.jsp" flush="false" />
</head>

<body>
<div id="wrap">
<!-- head 끝-->


    <!-- content 시작-->
    <div class="p_error">
        <div class="p_error_img"><img src="${ResRoot}/img/error.png"></div></li>
        <div class="p_error_script">
        	<c:out value='${eMsg}' default='시스템 장애 입니다.' escapeXml="false" />
        </div>
        <button id="ok">확인</button>
    </div>
    <!-- content 끝-->
</div>
</body>
<%-- Footer --%>
<jsp:include page="/WEB-INF/views/include/incFooter.jsp" flush="false" />

<script type="text/javascript">
	$(document).ready(function(){
		$('#ok').click(function(){
			var url = PHUtil.nvl("<c:out value='${sessionScope.adminLoginVO.welcomUri}' default='' />", "<c:url value='/index.jsp' />");
			PHFnc.doAction(url);
		});
	});
</script>
</html>