<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%--
 **********************************************************************************************
 * @desc		: 기프티쇼 단말할인권 문자발송 정보 관리 상세팝업
 * @FileName 	: /pHubCMS/src/main/webapp/WEB-INF/views/fmlyPnt/dicntCpnMMSInfoModifyPop.jsp
 * @author 		: bmg
 * @since 		: 2019.03.06
 * @version 	: 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.03.06   bmg        최초생성
 **********************************************************************************************
dicntCpnMMSInfoView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>
<div class="popup_content2">
	<div class="popup_title">기프티쇼 단말할인권 문자발송 정보 수정<a href="#!"><div class="top_close"></div></a></div>
	<table class="half_table" border="0" cellpadding="0" cellspacing="0">
		<tbody>
			<tr>
				<th style="width:200px;">메시지ID</th>
				<td style="width:300px;"><input type="text" name="text" id="m_mmsMsgId" style="background-color:lightgray;" readonly /></td>
				<th style="width:200px;">메시지구분 <span class="red">*</span></th>
				<td><input type="text" name="text" id="m_msgInd" maxlength="10" /></td>
			</tr>
			<tr>
				<th>메시지유형 <span class="red">*</span></th>
				<td>
					<select id="m_msgType" style="width:168px;">
						<option value="MMS">MMS</option>
						<option value="SMS">SMS</option>
					</select>
				</td>
				<th>콜백번호 <span class="red">*</span></th>
				<td><input type="text" name="text" id="m_clbcCtn" /></td>
			</tr>
			<tr>
				<th>메시지제목 <span class="red">*</span></th>
				<td colspan="3">
					<input type="text" name="text" id="m_msgTitl" style="width:500px;" maxlength="100" />
				</td>
			</tr>
			<tr>
				<th>메시지본문</th>
				<td colspan="3">
					<textarea rows="5" cols="100" id="m_msgBody"></textarea>
				</td>
			</tr>
			<tr>
				<th>사용여부</th>
				<td colspan="3">
					<select id="m_useYn" style="width:168px;">
						<option value="Y">Y</option>
						<option value="N">N</option>
					</select>
				</td>
			</tr>
			<tr>
				<th>등록자</th>
				<td id="m_rgstUserNm"></td>
				<th>등록일시</th>
				<td id="m_rgstDt"></td>
			</tr>
			<tr>
				<th>수정자</th>
				<td id="m_mdfyUserNm"></td>
				<th>수정일시</th>
				<td id="m_mdfyDt"></td>
			</tr>
		</tbody>
	</table>
	<div class="btnalign">
		<ul>
			<li><a href="#!"><div class="btn_cancel">취소</div></a></li>
			<li><a href="#!"><div class="btn_save" onClick="modifyMMSInfo()">저장</div></a></li>
		</ul>
	</div>
	<!-- 상세거래내역 끝-->
</div>