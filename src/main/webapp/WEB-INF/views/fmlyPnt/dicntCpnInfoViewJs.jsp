<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="com.olleh.pHubCMS.common.utils.SessionUtils"%> <%--
 **********************************************************************************************
 * @desc		: 기프티쇼 단말할인권 정보 관리 Js 
 * @FileName 	: /pHubCMS/src/main/webapp/WEB-INF/views/fmlyPnt/dicntCpnInfoViewJs.jsp
 * @author 		: bmg
 * @since 		: 2019.03.06
 * @version 	: 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.03.06   bmg        최초생성
 **********************************************************************************************
--%><script type="text/javascript">
var cWidth = $("#content").width();
$(document).ready(function () {
	var colNames = ['No'
	                ,'상품ID'
	                ,'상품명'
	                ,'상품유형명'
	                ,'상품금액(원)'
	                ,'등록자'
	                ,'등록일시'
	                ,'관리'
	                ,'상품이미지'
	                ,'상품설명'
	                ,'브랜드아이콘'
	                ,'브랜드썸네일이미지'
	                ,'브랜드코드'
	                ,'브랜드명'];
	var colModel = [{ label: 'No', name: 'no', width: cWidth * 0.03, key:true }
		,{ label: '상품ID'				,name: 'goods_cd'			,width: cWidth * 0.10 }
		,{ label: '상품명'				,name: 'goods_nm'			,width: cWidth * 0.15 }
		,{ label: '상품유형명'			,name: 'goods_type_nm'		,width: cWidth * 0.10 }
		,{ label: '상품금액(원)'		,name: 'cnsm_price_amt'		,width: cWidth * 0.10	,align:'right',formatter:'integer',formatoptions:{thousandsSeparator:','} }
		,{ label: '등록자'				,name: 'rgst_user_nm'		,width: cWidth * 0.08 }
		,{ label: '등록일시'			,name: 'rgst_dt'			,width: cWidth * 0.12 }
		,{ label: '관리'				,name: ''					,width: cWidth * 0.06	,formatter:dbutton }
		,{ label: '상품이미지'			,name: 'mms_goods_img'		,hidden: true }
		,{ label: '상품설명'			,name: 'goods_expl'			,hidden: true }
		,{ label: '브랜드아이콘'		,name: 'brand_icon_img'		,hidden: true }
		,{ label: '브랜드썸네일이미지'	,name: 'mms_brand_thum_img'	,hidden: true }
		,{ label: '브랜드코드'			,name: 'brand_cd'			,hidden: true }
		,{ label: '브랜드명'			,name: 'brand_nm'			,hidden: true }
	];
	PHJQg.load('jqGrid','jqGridPager',colNames,colModel);
	
	// 핸드폰번호 입력 제한 정의
    $('#in_priceAmt').keypress(function(e) {
    	var keycode = e.keyCode ? e.keyCode : e.which;
    	if ((keycode >= 48 && keycode <= 57) || (keycode >= 96 && keycode <= 105)) {
    		return true;
    	} else {
    		switch(keycode) {
    			case 8:		// BACKSPACE
    			case 9:		// TAB
    				return true;
    				break;
    			default:
    				e.preventDefault();
    		}
    	}
    }).keyup(function(e) {
    	var value = $(this).val().replace(/[^0-9]/g,"");
    	$(this).val(value);
    });
	
	// 엑셀다운로드
    $("#btn_excel").on("click", function() {
    	if (PHJQg.size('jqGrid') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
    	var postData = {
   			GOODS_CD: $('#in_goodsCd').val()
   			, GOODS_NM: $('#in_goodsNm').val()
   			, GOODS_TYPE_NM: $('#in_goodsTypeNm').val()
   			, PRICE_AMT: $('#in_priceAmt').val()
   		};
    	var colNamesExcel = $.map(colNames, function(o,i) {
    		return i < 7 ? o : null;
    	});
    	var colModelExcel = $.map(colModel, function(o,i) {
    		return i < 7 ? o : null;
    	});
    	PHJQg.excel('jqGridExcelDiv','${ViewRoot}/fmlyPnt/jgDicntCpnInfoExcel.do',postData,colNamesExcel,colModelExcel,'기프티쇼단말할인권정보관리');
    });
});
//조회 버튼 클릭 시
function search() {
	var postData = { 
		GOODS_CD: $('#in_goodsCd').val()
		, GOODS_NM: $('#in_goodsNm').val()
		, GOODS_TYPE_NM: $('#in_goodsTypeNm').val()
		, PRICE_AMT: $('#in_priceAmt').val()
	};
	PHJQg.reload('jqGrid','jqGridPager','${ViewRoot}/fmlyPnt/jgDicntCpnInfo.do',postData);
}
//상세 버튼 정의
function dbutton(cellvalue,options,rowobject) {
	return '<input type="button" class="table_btn" onclick="popClick('+rowobject.no+')" value="확인" />';
}
//클릭시 상세 정보 창에 data 셋팅
function popClick(data) {
	var row = $("#jqGrid").jqGrid('getRowData',data);
	$('#m_goodsCd').val(row.goods_cd);
	$('#m_goodsNm').val(row.goods_nm);
	$('#m_goodsTypeNm').val(row.goods_type_nm);
	$('#m_priceAmt').val(PHUtil.setComma(row.cnsm_price_amt));
	$('#m_mmsGoodsImg').val(row.mms_goods_img);
	$('#m_goodsExpl').val(row.goods_expl);
	$('#m_brandIconImg').val(row.brand_icon_img);
	$('#m_mmsBrandThumImg').val(row.mms_brand_thum_img);
	$('#m_brandCd').val(row.brand_cd);
	$('#m_brandNm').val(row.brand_nm);
	$('#m_rgstUserNm').val(row.rgst_user_nm);
	$('#m_rgstDt').val(row.rgst_dt);
	PHFnc.layerPopOpen(1);
}

</script>