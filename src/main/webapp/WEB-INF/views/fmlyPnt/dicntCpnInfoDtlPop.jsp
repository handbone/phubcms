<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%--
 **********************************************************************************************
 * @desc		: 기프티쇼 단말할인권 정보 관리 상세팝업
 * @FileName 	: /pHubCMS/src/main/webapp/WEB-INF/views/fmlyPnt/dicntCpnInfoDtlPop.jsp
 * @author 		: bmg
 * @since 		: 2019.03.06
 * @version 	: 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.03.06   bmg        최초생성
 **********************************************************************************************
dicntCpnInfoView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>
<div class="popup_content">
	<div class="popup_title">기프티쇼 단말할인권 정보 상세거래내역<a href="#!"><div class="top_close"></div></a></div>
	<table class="half_table" border="0" cellpadding="0" cellspacing="0">
		<tbody>
			<tr>
				<th style="width:200px;">상품ID</th>
				<td style="width:300px;"><input type="text" name="text" id="m_goodsCd" readonly /></td>
				<th style="width:200px;">상품명</th>
				<td><input type="text" name="text" id="m_goodsNm" style="width:300px;" readonly /></td>
			</tr>
			<tr>
				<th>상품유형명</th>
				<td><input type="text" name="text" id="m_goodsTypeNm" readonly /></td>
				<th>상품금액(원)</th>
				<td><input type="text" name="text" id="m_priceAmt" style="text-align:right;" readonly /></td>
			</tr>
			<tr>
				<th>상품이미지</th>
				<td><input type="text" name="text" id="m_mmsGoodsImg" style="width:300px;" readonly /></td>
				<th>상품설명</th>
				<td><input type="text" name="text" id="m_goodsExpl" style="width:300px;" readonly /></td>
			</tr>
			<tr>
				<th>브랜드아이콘</th>
				<td><input type="text" name="text" id="m_brandIconImg" style="width:300px;" readonly /></td>
				<th>브랜드썸네일이미지</th>
				<td><input type="text" name="text" id="m_mmsBrandThumImg" style="width:300px;" readonly /></td>
			</tr>
			<tr>
				<th>브랜드코드</th>
				<td><input type="text" name="text" id="m_brandCd" readonly /></td>
				<th>브랜드명</th>
				<td><input type="text" name="text" id="m_brandNm" readonly /></td>
			</tr>
			<tr>
				<th>등록자</th>
				<td><input type="text" name="text" id="m_rgstUserNm" readonly /></td>
				<th>등록일시</th>
				<td><input type="text" name="text" id="m_rgstDt" readonly /></td>
			</tr>
		</tbody>
	</table>
	<a href="#!"><div class="btn_close">닫기</div></a>
	<!-- 상세거래내역 끝-->
</div>