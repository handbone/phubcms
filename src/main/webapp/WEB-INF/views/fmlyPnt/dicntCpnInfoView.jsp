<%@  page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
 **********************************************************************************************
 * @desc		: 기프티쇼 단말할인권 정보 관리 
 * @FileName 	: /pHubCMS/src/main/webapp/WEB-INF/views/fmlyPnt/dicntCpnInfoView.jsp
 * @author 		: bmg
 * @since 		: 2019.03.06
 * @version 	: 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.03.06   bmg        최초생성
 *
 **********************************************************************************************
--%><!DOCTYPE html>
<html lang="ko">
<head>
	<%-- global --%>
	<jsp:include page="/WEB-INF/views/include/globalVar.jsp" flush="false" />
	<%-- head --%>
	<jsp:include page="/WEB-INF/views/include/incHead.jsp" flush="false" />
</head>
<body>
<!-- loader 시작-->
<div class="loader" onclick="loader_close()">
    <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
</div>
<!-- loader 끝-->
<div id="wrap">	
<header>
	<%-- header --%>
	<jsp:include page="/WEB-INF/views/include/incHeader.jsp" flush="false" />
</header>

    <!-- 레프트 메뉴 해당페이지 표시 -->
    <style>
    	.${sessionScope.USER_MENU.menu_id}{ color: #fff !important; background: #3f4759 !important; border-left: 3px solid #fff; cursor: hand;}
    	.half_table td input { background-color:#F8F8F8; } 
    </style>
    <!-- 레프트 메뉴 해당페이지 표시 끝 -->
    
    <!-- popup 시작-->
	<jsp:include page="./dicntCpnInfoDtlPop.jsp" flush="false" />
    <!-- popup 끝-->

<!-- dash_board 시작-->
<section id="dash_board">
    <!--side_menu 시작-->
	<jsp:include page="/WEB-INF/views/include/incMenu.jsp" flush="false" />
    <!--side_menu 끝-->

    <!--contents 시작-->
    <div class="contents mCustomScrollbar">
        <div class="page_title"><div class="page_name">${sessionScope.USER_MENU.menu_nm}</div><div class="page_location"><i class="fas fa-home"></i>${sessionScope.USER_MENU.mnu_path}</div></div>
        <div class="subject">
        	<div class="page_search">
                <table class="search_tb" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="search_item">
                            <ul>
			                    <li>상품ID <input type="text" name="name" id="in_goodsCd" maxlength="20" autocomplete="off"></li>
			                    <li>상품명 <input type="text" name="name" id="in_goodsNm" maxlength="20" autocomplete="off"></li>
			                    <li>상품유형명 <input type="text" name="name" id="in_goodsTypeNm" maxlength="20" autocomplete="off"></li>
			                    <li>상품금액(원) <input type="text" name="name" id="in_priceAmt" maxlength="10" autocomplete="off"></li>
			                </ul>
                        </td>
                        <td class="btn_search">
                            <button type="button" onClick="search();">조회</button>
                        </td>
                    </tr>
                </table>
            </div>            
            <div class="page_contents">
                <div class="btn_excel" id="excel_form"><button id="btn_excel">엑셀다운</button></div>
                <div id="content">
                    <table id="jqGrid"></table>                
                    <div id="jqGridPager"></div>
                    <div id="jqGridExcelDiv" style="display:none">
                		<table id="jqGridExcel" ></table>
                	</div>
                </div>
            </div>
        </div>
        <!-- 스크롤 때문에 여백 있어야함 -->
        <br>
        <br>
        <br>
        <br>
        <br>

        </div>
    <!--contents 끝-->
</section>
<!-- dash_board 끝--> 

    <!-- footer 시작-->
    <jsp:include page="/WEB-INF/views/include/incFooter.jsp" flush="false" />
    <section class="footer">	 
    </section>
    <!-- footer 끝-->
    
    <%-- js --%>
	<jsp:include page="./dicntCpnInfoViewJs.jsp" flush="false" />
	
</body>
</html>