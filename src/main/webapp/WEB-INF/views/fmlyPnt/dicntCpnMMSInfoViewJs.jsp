<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="com.olleh.pHubCMS.common.utils.SessionUtils"%> <%--
 **********************************************************************************************
 * @desc		: 기프티쇼 단말할인권 문자발송 정보 관리 Js 
 * @FileName 	: /pHubCMS/src/main/webapp/WEB-INF/views/fmlyPnt/dicntCpnMMSInfoViewJs.jsp
 * @author 		: bmg
 * @since 		: 2019.03.06
 * @version 	: 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.03.06   bmg        최초생성
 **********************************************************************************************
--%><script type="text/javascript">
var cWidth = $("#content").width();
$(document).ready(function () {
	var colNames = ['No'
	                ,'메시지ID'
	                ,'메시지유형'
	                ,'콜백번호'
	                ,'메시지제목'
	                ,'사용여부'
	                ,'수정자'
	                ,'수정일시'
	                ,'관리'
	                ,'메시지구분'
	                ,'메시지본문'
	                ,'등록자'
	                ,'등록일시'];
	var colModel = [{ label: 'No', name: 'no', width: cWidth * 0.03, key:true }
		,{ label: '메시지ID'	,name: 'mms_msg_id' 	,width: cWidth * 0.10 }
		,{ label: '메시지유형'	,name: 'msg_type' 		,width: cWidth * 0.10 }
		,{ label: '콜백번호'	,name: 'clbc_ctn' 		,width: cWidth * 0.10 }
		,{ label: '메시지제목'	,name: 'msg_titl' 		,width: cWidth * 0.20 }
		,{ label: '사용여부'	,name: 'use_yn' 		,width: cWidth * 0.06 }
		,{ label: '수정자'		,name: 'mdfy_user_nm' 	,width: cWidth * 0.08 }
		,{ label: '수정일시'	,name: 'mdfy_dt' 		,width: cWidth * 0.12 }
		,{ label: '관리'		,name: ''				,width: cWidth * 0.08	,formatter:dbutton }
		,{ label: '메시지구분'	,name: 'msg_ind' 		,hidden: true }
		,{ label: '메시지본문'	,name: 'msg_body' 		,hidden: true }
		,{ label: '등록자'		,name: 'rgst_user_nm' 	,hidden: true }
		,{ label: '등록일시'	,name: 'rgst_dt' 		,hidden: true }
	];
	PHJQg.load('jqGrid','jqGridPager',colNames,colModel);
	
	// 엑셀다운로드
    $("#btn_excel").on("click", function() {
    	if (PHJQg.size('jqGrid') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
    	var postData = {
   			MSG_IND: 'all'
  			, MSG_TYPE: $('#in_msgType').val()
  			, USE_YN: $('#in_useYn').val()
   		};
    	var colNamesExcel = $.map(colNames, function(o,i) {
    		return i < 8 ? o : null;
    	});
    	var colModelExcel = $.map(colModel, function(o,i) {
    		return i < 8 ? o : null;
    	});
    	PHJQg.excel('jqGridExcelDiv','${ViewRoot}/fmlyPnt/jgDicntCpnMMSInfoExcel.do',postData,colNamesExcel,colModelExcel,'기프티쇼단말할인권문자발송정보관리');
    });
});
//조회 버튼 클릭 시
function search() {
	var postData = { 
		MSG_IND: 'all'
		, MSG_TYPE: $('#in_msgType').val()
		, USE_YN: $('#in_useYn').val()
	};
	PHJQg.reload('jqGrid','jqGridPager','${ViewRoot}/fmlyPnt/jgDicntCpnMMSInfo.do',postData);
}
//상세 버튼 정의
function dbutton(cellvalue,options,rowobject) {
	return '<input type="button" class="table_btn" onclick="popClick('+rowobject.no+')" value="수정" />';
}
//클릭시 상세 정보 창에 data 셋팅
function popClick(data) {
	var row = $("#jqGrid").jqGrid('getRowData',data);
	$('#m_mmsMsgId').val(row.mms_msg_id);
	$('#m_msgInd').val(row.msg_ind);
	$('#m_msgType').val(row.msg_type);
	$('#m_clbcCtn').val(row.clbc_ctn);
	$('#m_msgTitl').val(row.msg_titl);
	$('#m_msgBody').val(row.msg_body.replace(/(<br>)/g,'\n'));
	$('#m_useYn').val(row.use_yn);
	$('#m_rgstUserNm').html(row.rgst_user_nm);
	$('#m_rgstDt').html(row.rgst_dt);
	$('#m_mdfyUserNm').html(row.mdfy_user_nm);
	$('#m_mdfyDt').html(row.mdfy_dt);
	PHFnc.layerPopOpen(2);
}
// 등록
function createMMSInfo() {
	if ($('#c_msgType').val() == '') {
		alert('메시지 유형을 선택해 주세요.');
		return;
	}
	if ($.trim($('#c_clbcCtn').val()) == '') {
		alert('콜백번호를 입력해 주세요.');
		return;
	} else {
		if (!/^[-0-9]+$/.test($('#c_clbcCtn').val())) {
			alert('콜백번호 입력값이 잘못되었습니다.');
			return;
		}
	}
	if (getByteLength($('#c_msgTitl').val()) > 100) {
		alert('메시지 제목은 100 byte를 넘을 수 없습니다.');
		return;
	}
	if (getByteLength($('#c_msgBody').val()) > 1000) {
		alert('메시지 본문은 1000 byte를 넘을 수 없습니다.');
		return;
	}
	if ($('#c_useYn').val() == '') {
		alert('사용여부를 선택해 주세요.');
		return;
	}
	if (confirm("저장하시겠습니까?")) {
		var postData = {
			MSG_IND: $('#c_msgInd').val()
			, MSG_TITL: $('#c_msgTitl').val()
			, MSG_BODY: $('#c_msgBody').val()
			, MSG_TYPE: $('#c_msgType').val()
			, CLBC_CTN: $('#c_clbcCtn').val()
			, USE_YN: $('#c_useYn').val()
		};
		PHFnc.ajax("${ViewRoot}/fmlyPnt/ajaxCreateDicntCpnMMSInfo.do",postData,"POST","json",function(data, textStatus, jqXHR) {
			alert("저장되었습니다.");
			$(".top_close").click();
			search();
		},null,true,true,false);
	}
}
// 수정
function modifyMMSInfo() {
	if ($.trim($('#m_clbcCtn').val()) == '') {
		alert('콜백번호를 입력해 주세요.');
		return;
	} else {
		if (!/^[-0-9]+$/.test($('#m_clbcCtn').val())) {
			alert('콜백번호 입력값이 잘못되었습니다.');
			return;
		}
	}
	if (getByteLength($('#m_msgTitl').val()) > 100) {
		alert('메시지 제목은 100 byte를 넘을 수 없습니다.');
		return;
	}
	if (getByteLength($('#m_msgBody').val()) > 1000) {
		alert('메시지 본문은 1000 byte를 넘을 수 없습니다.');
		return;
	}
	if (confirm("저장하시겠습니까?")) {
		var postData = {
			MMS_MSG_ID: $('#m_mmsMsgId').val()
			, MSG_IND: $('#m_msgInd').val()
			, MSG_TITL: $('#m_msgTitl').val()
			, MSG_BODY: $('#m_msgBody').val()
			, MSG_TYPE: $('#m_msgType').val()
			, CLBC_CTN: $('#m_clbcCtn').val()
			, USE_YN: $('#m_useYn').val()
		};
		PHFnc.ajax("${ViewRoot}/fmlyPnt/ajaxModifyDicntCpnMMSInfo.do",postData,"POST","json",function(data, textStatus, jqXHR) {
			alert("저장되었습니다.");
			$(".top_close").click();
			search();
		},null,true,true,false);
	}
}

function getByteLength(s,b,i,c){
    for(b=i=0;c=s.charCodeAt(i++);b+=c>>11?3:c>>7?2:1);
    return b;
}

</script>