<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@  page language="java" contentType="text/html; charset=UTF-8"%><%--
 **********************************************************************************************
 * @desc : 공통 LAYOUT -> menu
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/include/incMenu.jsp
 * @author lys
 * @since 2018.08.13
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.13   ojh        공통 메뉴 
 * </pre>
 **********************************************************************************************
--%>
<%--
	***************************************************************************
	** 메뉴 스크립트 기술
	***************************************************************************
--%>

    <div class="side_menu mCustomScrollbar" data-mcs-theme="minimal">
        <nav>
            <ul>
            <c:forEach var="item" items="${sessionScope.USER_MENU_LIST}" varStatus="vs">
            	<c:if test="${(item.menu_lvl == 1 || item.menu_lvl == 2) && beforeLv == 3}">
            		</div>
            	</c:if>
            	<c:if test="${item.menu_lvl == 1}">
            		<li class="deps1" >${item.menu_nm}</li>
            	</c:if>            	
            	<c:if test="${item.menu_lvl == 2 && item.menu_uri == ''}">
            		<li class="deps2"><div class="menu_name">${item.menu_nm}</div><div class="deps_arrow"><img src="${ResRoot}/img/arrow.png"></div></li>
            	</c:if>
            	<c:if test="${item.menu_lvl == 2 && item.menu_uri != ''}">
            		<a href="${ViewRoot}${item.menu_uri}" onclick="PHFnc.lnbDoAction(this);return false;"><li class="deps2 ${item.menu_id}"><div class="menu_name">${item.menu_nm}</div></li></a>
            	</c:if>            	
            	<c:if test="${item.menu_lvl == 3 && beforeLv == 2}">
            		<div class="deps3">
            	</c:if>
            	<c:if test="${item.menu_lvl == 3}">
            		<a href="${ViewRoot}${item.menu_uri}" onclick="PHFnc.lnbDoAction(this);return false;" class="${item.menu_id}">${item.menu_nm}</a>
            	</c:if>            	
            	<c:set var="beforeLv" value="${item.menu_lvl}"/>  
            </c:forEach>
            
                <!-- 스크롤바 하단 영역으로 인한 공백 (공백없을시 메뉴 짤림)-->
                <li style="pointer-events: none;"></li>
                <li style="pointer-events: none;"></li>
                <li style="pointer-events: none;"></li>
                <li style="pointer-events: none;"></li>
                <li style="pointer-events: none;"></li>
            </ul>
        </nav>
    </div>