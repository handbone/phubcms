<%@  page language="java" contentType="text/html; charset=UTF-8"%>
<%@ page import="com.olleh.pHubCMS.common.utils.SessionUtils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%>
<%--
 **********************************************************************************************
 * @desc : 공통 LAYOUT -> header
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/include/incHeader.jsp
 * @author ojh
 * @since 2018.08.10
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.10   ojh        공통 LAYOUT -> header 
 * </pre>
 **********************************************************************************************
--%>
<c:set var="welcomUri" value="${sessionScope.adminLoginVO.welcomUri}" />
<div class="container_box">
	<div class="left">
		<div class="logo">
			<a href="<c:out value='${welcomUri}' default= '' />"
				onclick="PHFnc.lnbDoAction(this);return false;">POINT HUB</a>
		</div>
		<div class="s_menu">
			<a href="javascript:void(0);" id="menu_toggle"><img
				src="${ResRoot}/img/menu.png"></a>
		</div>
	</div>
	<div class="right">
		<!-- <div class="info"><a href="!#"><i class="far fa-bell"></i></a></div>
        <div class="photo"><img src="../img/avatar-1.jpg" alt="user_img"></div> -->
		<div class="user_loginfo">
			<div class="dropdown">
				<a href="javascript:void(0);"><%=SessionUtils.getUserNm(request)%></a>
				<div class="dropdown_contents">
					<div class="arrow_top"></div>
					<div class="logout">
						<a onclick="PHFnc.lnbDoAction(this);return false;"
							href="${ViewRoot}/login/logOut.do" class="ddli">로그아웃</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>