<%@  page language="java" contentType="text/html; charset=UTF-8"%><%--
 **********************************************************************************************
 * @desc : 공통 LAYOUT -> head
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/include/incHead.jsp
 * @author lys
 * @since 2018.07.07
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.07.07   lys        LAYOUT -> head 
 * </pre>
 **********************************************************************************************
--%>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <link rel="shortcut icon" type="image/x-icon" href="${ResRoot}/img/clippoint_favicon.png">
    <title>POINT HUB</title>
    
    <link rel="stylesheet" href="${ResRoot}/css/lib/bootstrap.min.css">
    <link rel="stylesheet" href="${ResRoot}/css/lib/ui.jqgrid-bootstrap.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="${ResRoot}/css/lib/mCustomScrollbar.css" />
    <link rel="stylesheet" href="${ResRoot}/css/style.css?${TimeStamp}">
    <script type="text/javascript" src="${ResRoot}/common/jQuery/jquery.min.js"></script>
    <script type="text/javascript" src="${ResRoot}/common/jQuery/jquery.form.js"></script>
    <script type="text/javascript" src="${ResRoot}/common/pubJs/script.js?v=js_Ver1"></script>
    <script type="text/javascript" src="${ResRoot}/common/pubJs/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="${ResRoot}/common/pubJs/mCustomScrollbar.concat.min.js"></script>
    
    
    <script type="text/javascript" src="${ResRoot}/common/jQuery/jquery.ui.datepicker.js"></script>
    <link rel="stylesheet" href="${ResRoot}/common/jQueryUi/jquery-ui.css">
    <script type="text/javascript" src="${ResRoot}/common/jQueryUi/jquery-ui.min.js"></script>
    
    <!--[if lt IE 9]>
    <script src="${ResRoot}/common/pubJs/html5shiv.js"></script>
    <script src="${ResRoot}/common/pubJs/html5shiv-printshiv.js"></script><![endif]-->
    <script type="text/ecmascript" src="${ResRoot}/common/jQgrid/js/i18n/grid.locale-en.js"></script>
	<script type="text/ecmascript" src="${ResRoot}/common/jQgrid/js/jquery.jqGrid.min.js"></script>
	
	<script type="text/javascript" src="${ResRoot}/common/pubJs/jszip.min.js"></script>
	
 