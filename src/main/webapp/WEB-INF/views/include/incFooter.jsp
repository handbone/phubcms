<%@  page language="java" contentType="text/html; charset=UTF-8" buffer="16kb"
%><%@ page import="com.olleh.pHubCMS.common.utils.SessionUtils"
%><%--
 **********************************************************************************************
 * @desc : 공통 LAYOUT -> Footer
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/include/incFooter.jsp
 * @author lys
 * @since 2018.07.07
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.07.07   lys        LAYOUT -> Footer 
 * </pre>
 **********************************************************************************************
--%>

<%--
	**************************************************************************
	Footer HTML 소스 구현
	**************************************************************************
 --%>

<%-- 공통 js --%>
<script src="${ResRoot}/common/js/ph-const.js?${TimeStamp}"></script>
<script src="${ResRoot}/common/js/ph-fnc.js?${TimeStamp}"></script>
<script src="${ResRoot}/common/js/ph-util.js?${TimeStamp}"></script>
<script src="${ResRoot}/common/js/ph-valid.js?${TimeStamp}"></script>
<script src="${ResRoot}/common/js/ph-jQgrid.js?${TimeStamp}"></script>
<script type="text/javascript">

$(function() {	
	$.datepicker.setDefaults({
		dateFormat: 'yy-mm-dd'
		,showOtherMonths: true 		//빈 공간에 현재월의 앞뒤월의 날짜를 표시
		,showMonthAfterYear:true 	//년도 먼저 나오고, 뒤에 월 표시  
		,changeMonth:true 			//콤보박스에서 년 선택 가능
		,changeYear:true  			//콤보박스에서 월 선택 가능     
		,showOn: "both" 			//button:버튼을 표시하고,버튼을 눌러야만 달력 표시 ^ both:버튼을 표시하고,버튼을 누르거나 input을 클릭하면 달력 표시 
		,buttonImage: "${ResRoot}/img/cal.png"  //버튼 이미지 경로
		,buttonImageOnly: true 		//기본 버튼의 회색 부분을 없애고, 이미지만 보이게 함			
	 	,buttonText: "선택" 			//버튼에 마우스 갖다 댔을 때 표시되는 텍스트   	          
        ,yearSuffix: "년" 			//달력의 년도 부분 뒤에 붙는 텍스트			
        ,yearRange: 'c-100:c+100'
		,monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'] //달력의 월 부분 텍스트
        ,monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'] //달력의 월 부분 Tooltip 텍스트
        ,dayNamesMin: ['일','월','화','수','목','금','토'] //달력의 요일 부분 텍스트
        ,dayNames: ['일요일','월요일','화요일','수요일','목요일','금요일','토요일'] //달력의 요일 부분 Tooltip 텍스트
        //,minDate: "-10y" 			//최소 선택일자(-1D:하루전, -1M:한달전, -1Y:일년전)
        //,maxDate: "+10Y" 			//최대 선택일자(+1D:하루후, -1M:한달후, -1Y:일년후)
	});		
});
//메뉴 이동시 현재 메뉴 표시가 보이도록 스크롤 내림
$(window)
	.on("load", function () {		
	
	<%if(SessionUtils.getUserMenu(request) != null){%>
		var ord = <%=SessionUtils.getUserMenu(request).get("no")%>;
		$(".side_menu > nav").mCustomScrollbar();
		if(ord > 17){
			$(".side_menu > nav").mCustomScrollbar("scrollTo",ord * 25);
		}
	<%}%>
	$('input:text').attr('autocomplete', 'off');
	})
	.bind('resize', function() {
		$('#jqGrid').setGridWidth($("#content").width(), false);
	});
	
	
</script>
<%-- 
<script type="text/javascript">
$(function(){
	// javascript 구현
});
</script>
<section id="pop-loading" class="popup loading" style="position:fixed;">
	<div class="bg"></div>
	<img src="${ResRoot}/mb/images/common/loading.gif" alt="로딩중"/>
</section>

<footer>
	<small>Copyright (c) 2018 KT, Inc.</small>
</footer>
--%>