<%@  page language="java" contentType="text/html; charset=UTF-8"
%><%@taglib prefix="c"      uri="http://java.sun.com/jsp/jstl/core"
%><%@taglib prefix="fn"	    uri="http://java.sun.com/jsp/jstl/functions"
%><%@taglib prefix="fmt"    uri="http://java.sun.com/jsp/jstl/fmt"
%><%@taglib prefix="spring" uri="http://www.springframework.org/tags"%><%--
 **********************************************************************************************
 * @desc : 공통 전역변수 설정
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/include/globalVar.jsp
 * @author lys
 * @since 2018.07.07
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.07.07   lys        공통 전역변수 설정 
 * </pre>
 **********************************************************************************************
--%><%
String  gbTimeStamp   = String.valueOf(System.currentTimeMillis());
pageContext.setAttribute("CRCN", "\r\n");
pageContext.setAttribute("CN", "\n");
%><spring:eval var="GbServerType" expression="3"/> <%-- 1:운영 2:테스트 3:로컬 --%>
<c:set var="TimeStamp" value="${(GbServerType ne '1') ? gbTimeStamp : '19062401'}" scope="request" />
<c:set var="ResRoot"   value="${pageContext.request.contextPath}/resources" scope="request" />
<c:set var="ViewRoot"  value="${pageContext.request.contextPath}" scope="request" />
<script type="text/javascript">
var g_eMsg     = "${fn:replace(fn:replace(eMsg, CN, ''), CRCN, '')}";
//
function cfSetAddZero(target) {     
    var num = parseInt(target);
    var str = num > 9 ? num : "0" + num;
    return str.toString();
}
//달력 셋팅 //기본값 : 접속한날
function setDate(start_dt,end_dt){	
	$("#start_date").datepicker();
	$("#end_date").datepicker();
	//달력 크기 조정
	$('img.ui-datepicker-trigger').css({
		'width':'35px'
		, 'height':'35px'
		, 'padding':'5px'
		, 'margin-bottom':'-12px'
		, 'display': 'inline-block'
		, '-webkit-background-size': '100%'
		, 'background-size': '100%'
		, 'vertical-align':'baseline'
	});
	if(start_dt !=null){
		$("#start_date").val(start_dt.getFullYear()+"-"+cfSetAddZero(start_dt.getMonth()+1)+"-"+cfSetAddZero(start_dt.getDate()));
	}
	if(end_dt !=null){
		$("#end_date").val(end_dt.getFullYear()+"-"+cfSetAddZero(end_dt.getMonth()+1)+"-"+cfSetAddZero(end_dt.getDate()));
	}
	if ($('#start_date,#end_date').attr('maxlength') == null) {
		$('#start_date,#end_date').attr('maxlength', 10);
	}
	$('#start_date,#end_date').removeAttr('readonly')
		.keypress(function(e) {
			var keycode = e.keyCode ? e.keyCode : e.which;
			if ((keycode >= 48 && keycode <= 57) || (keycode >= 96 && keycode <= 105)) {
				switch ($(this).val().length) {
					case 4:
					case 7:
						if (($(this).val().match(/-/g) || []).length < 2) {
							$(this).val($(this).val()+'-');	
						}
						break;
					default:
				}
				return true;
			} else {
	    		switch(keycode) {
		    		case 8:		// BACKSPACE
	    			case 9:		// TAB
	    			case 45:	// -
	    				return true;
	    				break;
	    			default:
	    				e.preventDefault();
	    		}
			}
		})
		.focusout(function(e) {
			if(undefined == $(e.relatedTarget).attr("id")) {
				return false;
			}
			var value = PHUtil.replaceAll($(this).val(), "-", "");
			if (!PHUtil.isEmpty(value) && !PHValid.date(value)) {
				alert('유효한 날짜형식이 아닙니다');
				$(this).val('');
				$(this).select();
				e.stopPropagation();
				return false;
			}
		});
}
//숫자만 입력 하도록 제한
function onlyNum(event, type) {
	event = event || window.event;
    var keyID = (event.which) ? event.which : event.keyCode;
    if ( (keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39 
    		|| keyID == 9 || keyID == 16 || keyID == 17 || keyID == 35 || keyID == 36){
        return;
	} else {
    	if(type == "ip"){
    		//ip는 . 가능 
    		if(keyID == 190){
    			return;
    		} else{
    			return false;
    		}	
    	} else{
    		return false;
    	}
    }
}
//한글 지우기
function keyReplace(obj){
	var inputVal = obj.value;
	obj.value = inputVal.replace(/[^a-z0-9.]/gi,'');
}
</script>