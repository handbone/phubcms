<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
 **********************************************************************************************
 * @desc : 포인트거래 일별 통계 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/statistic/pntDealStatDViewJs.jsp
 * @author ojh
 * @since 2018.10.10
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.10    ojh        최초생성
 * 
 **********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
$(document).ready(function () { 
	var colNames = ['No','거래일자','PG사명','정산포인트','사용포인트<br>건수','사용포인트','취소포인트<br>건수','취소포인트','사용실패포인트<br>건수','사용실패포인트','취소실패포인트<br>건수','취소실패포인트','서비스명','PG사명','카드사명'];
	var colModel = [{ label: 'No', name: 'no', width: cWidth * 0.05, key:true }
		,{ label: '거래일자'			,name: 'deal_dd'		,width: cWidth * 0.07 	,cellattr:jsFormatterCell	}
		,{ label: 'PG사명'				,name: 'srch_nm' 		,width: cWidth * 0.11 	,formatter:srch_nm 		,align:'left'			}
		,{ label: '정산포인트'			,name: 'cm_pnt' 		,width: cWidth * 0.09 	,formatter:cm_pnt		,align:'right' 			}
		,{ label: '사용포인트 건수'		,name: 'pay_cnt'		,width: cWidth * 0.08 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
		,{ label: '사용포인트'			,name: 'pay_pnt'		,width: cWidth * 0.09 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
		,{ label: '취소포인트 건수'		,name: 'cncl_cnt' 		,width: cWidth * 0.08 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
		,{ label: '취소포인트'			,name: 'cncl_pnt' 		,width: cWidth * 0.09 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
		,{ label: '사용실패포인트 건수'	,name: 'pay_fail_cnt' 	,width: cWidth * 0.10 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
		,{ label: '사용실패포인트'		,name: 'pay_fail_pnt' 	,width: cWidth * 0.09 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
		,{ label: '취소실패포인트 건수'	,name: 'cncl_fail_cnt' 	,width: cWidth * 0.10 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
		,{ label: '취소실패포인트'		,name: 'cncl_fail_pnt' 	,width: cWidth * 0.09 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
		,{ label: '서비스명'			,name: 'service_nm' 	,width: cWidth * 0.10 }
		,{ label: 'PG사명'				,name: 'pg_cmpn_nm' 	,hidden:true}
		,{ label: '카드사명'			,name: 'prvdr_nm'	 	,hidden:true}
	];
	PHJQg.loadTotalRow('jqGrid','jqGridPager',colNames,colModel);

	//검색일자 달력 셋팅
	var dt = new Date();
	dt.setDate(dt.getDate()-1);
	setDate(dt,dt);
	
	//엑셀다운로드
	$("#btn_excel").on("click", function(){
		if (PHJQg.size('jqGrid') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
		if (!validateDate()) {
			return;
		}
		var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");	
		if(conf){		
			var postData = {
					SRCH_IND 	: $("#sel_ind").val()
					,PH_SVC_CD  : $('#sel_svcCd').val()
		        	,START_DATE : PHUtil.replaceAll($("#start_date").val(),"-","") 
		        	,END_DATE 	: PHUtil.replaceAll($("#end_date").val(),"-","") 
				};
			var colNames = ['No','거래일자','PG사명','정산포인트','사용포인트건수','사용포인트','취소포인트건수','취소포인트','사용실패포인트건수','사용실패포인트','취소실패포인트건수','취소실패포인트','서비스명','PG사명','카드사명'];
			var colModel = [{ label: 'No', name: 'no', key:true }
					,{ label: '거래일자'			,name: 'deal_dd'		,cellattr:jsFormatterCell	}
					,{ label: 'PG사명'				,name: 'srch_nm' 		,formatter:srch_nm 		,align:'left'			}
					,{ label: '정산포인트'			,name: 'cm_pnt' 		,formatter:cm_pnt		,align:'right' 			}
					,{ label: '사용포인트 건수'		,name: 'pay_cnt'		,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
					,{ label: '사용포인트'			,name: 'pay_pnt'		,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
					,{ label: '취소포인트 건수'		,name: 'cncl_cnt' 		,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
					,{ label: '취소포인트'			,name: 'cncl_pnt' 		,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
					,{ label: '사용실패포인트 건수'	,name: 'pay_fail_cnt' 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
					,{ label: '사용실패포인트'		,name: 'pay_fail_pnt' 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
					,{ label: '취소실패포인트 건수'	,name: 'cncl_fail_cnt' 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
					,{ label: '취소실패포인트'		,name: 'cncl_fail_pnt' 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
					,{ label: '서비스명'			,name: 'service_nm' }
					,{ label: 'PG사명'				,name: 'pg_cmpn_nm' 	,hidden:true}
					,{ label: '카드사명'			,name: 'prvdr_nm'	 	,hidden:true}
			];
			PHJQg.excel('jqGridExcelDiv','${ViewRoot}/statistic/jgPntDealStatDExcel.do',postData,colNames,colModel,'포인트거래 일별 통계');
		}
	});
});
function cm_pnt(cellvalue,options,rowobject) {
	var cmPnt = rowobject.pay_pnt - rowobject.cncl_pnt;
	if (cmPnt < 0) {
		cmPnt = cmPnt+'';
		return '-'+ PHUtil.setComma(cmPnt.substring(1));
	} else {
		return PHUtil.setComma(cmPnt);
	}
}

var srch;
function srch_nm(cellvalue,options,rowobject){
	if(srch == "PG"){
		return rowobject.pg_cmpn_nm
	} else if(srch == "CD"){
		return rowobject.prvdr_nm
	} else{
		return ""
	}	
}

//jqGrid 셀병합 포맷
var chkcell={cellId:undefined, chkval:undefined};
function jsFormatterCell(rowid, val, rowObject, cm, rdata){

  var result = "";     
  if(chkcell.chkval != val){ //check 값이랑 비교값이 다른 경우
      var cellId = this.id + '_row_'+rowid+'-'+cm.name;
      result = ' rowspan="1" id ="'+cellId+'" + name="cellRowspan"';
      chkcell = {cellId:cellId, chkval:val};
  }else{
      result = 'style="display:none"  rowspanid="'+chkcell.cellId+'"'; //같을 경우 display none 처리
  }
  return result;
}
//검색버튼 클릭
function search() {
	if($("#sel_ind").val() == "all"){
		alert("조회구분을 선택해 주세요.");
		return;
	} else if($("#sel_ind").val() == "PG"){
		srch = "PG";
		$("#jqGrid").jqGrid('setLabel', "srch_nm","PG사명"); 
	} else if($("#sel_ind").val() == "CD"){
		srch = "CD";
		$("#jqGrid").jqGrid('setLabel', "srch_nm","카드사명"); 
	}
	if (!validateDate()) {
		return;
	}
	var postData = {
		SRCH_IND 	: $("#sel_ind").val()
		,PH_SVC_CD  : $('#sel_svcCd').val()
       	,START_DATE : PHUtil.replaceAll($("#start_date").val(),"-","") 
       	,END_DATE 	: PHUtil.replaceAll($("#end_date").val(),"-","") 
	};
	chkcell={cellId:undefined, chkval:undefined};
	
	PHJQg.reloadCallback('jqGrid','jqGridPager','${ViewRoot}/statistic/jgPntDealStatD.do',postData,function(o) {
		$(o).jqGrid('footerData','set', {
			no:'Total',
			cm_pnt:PHJQg.colSum(o,'cm_pnt'),
			pay_cnt:PHJQg.colSum(o,'pay_cnt'),
			pay_pnt:PHJQg.colSum(o,'pay_pnt'),
			cncl_cnt:PHJQg.colSum(o,'cncl_cnt'),
			cncl_pnt:PHJQg.colSum(o,'cncl_pnt'),
			pay_fail_cnt:PHJQg.colSum(o,'pay_fail_cnt'),
			pay_fail_pnt:PHJQg.colSum(o,'pay_fail_pnt'),
			cncl_fail_cnt:PHJQg.colSum(o,'cncl_fail_cnt'),
			cncl_fail_pnt:PHJQg.colSum(o,'cncl_fail_pnt')
		});
	});
}
//기간 조건 체크
function validateDate(){
	var sDate = $("#start_date").val();
	var eDate = $("#end_date").val();
	if (!PHUtil.isEmpty(sDate) && !PHValid.date(PHUtil.replaceAll(sDate,'-',''))) {
		alert('시작일자가 유효한 날짜 형식이 아닙니다');
		$('#start_date').val('');
		$('#start_date').focus();
		return false;
	}
	if (!PHUtil.isEmpty(eDate) && !PHValid.date(PHUtil.replaceAll(eDate,'-',''))) {
		alert('종료일자가 유효한 날짜 형식이 아닙니다');
		$('#end_date').val('');
		$('#end_date').focus();
		return false;
	}
	var startDate = new Date(sDate);
	var endDate = new Date(eDate);
	if (startDate.getTime() > endDate.getTime()) {		
		alert('뒤의 날짜는 앞의 날짜 이전으로 검색할 수 없습니다.')
		return false;
	} else {
		var valDate = new Date(sDate);
		valDate.setMonth(valDate.getMonth()+1);
		if (endDate.getTime() > valDate.getTime()) {
			alert('검색 기간은 한달을 넘을 수 없습니다.');
			return false;
		}
	}
	return true;
}

</script>