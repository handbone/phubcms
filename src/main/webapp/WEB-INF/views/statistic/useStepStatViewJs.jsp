<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
 **********************************************************************************************
 * @desc : 결제단계별 이용건수 통계 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/statistic/useStepStatViewJs.jsp
 * @author ojh
 * @since 2018.11.14
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.11.14    ojh        최초생성
 * 
 **********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
var GAP = 5;
var firstYear = new Date();
var today = new Date();
$(document).ready(function () { 
	var colNames = ['거래일자','사용처','서비스명','이용건수','(1)이용약관<br>동의/본인인증','(2)가용포인트<br>조회','(3)포인트<br>결제 요청','(4)포인트<br>결제 완료','(4-1)포인트<br>결제 성공','(4-2)포인트<br>결제 실패','(2)/(1)','(3)/(2)','(4)/(3)','(4)/(1)','(4-1)/(4)','(4-2)/(4)','거래일자','거래월'];
	var colModel = [{ label: '거래일자'			,name: 'mmdd'			,width: cWidth * 0.07	,key:true 		,formatter:mdChk }
		,{ label: '사용처'				 		,name: 'cprt_cmpn_nm'	,width: cWidth * 0.10	,align:'left'	}
		,{ label: '서비스명'				 	,name: 'service_nm'		,width: cWidth * 0.08	}
		,{ label: '이용건수'			 		,name: 'cust_cnt'		,width: cWidth * 0.075 	,align:'right'	,formatter: 'integer',formatoptions:{thousandsSeparator:","} }
		,{ label: '(1)이용약관 동의/본인인증'	,name: 'step_cnt_01'	,width: cWidth * 0.08 	,align:'right'	,formatter: 'integer',formatoptions:{thousandsSeparator:","} }
		,{ label: '(2)가용포인트 조회'			,name: 'step_cnt_05'	,width: cWidth * 0.075 	,align:'right'	,formatter: 'integer',formatoptions:{thousandsSeparator:","} }
		,{ label: '(3)포인트결제 요청'			,name: 'step_cnt_06'	,width: cWidth * 0.075 	,align:'right'	,formatter: 'integer',formatoptions:{thousandsSeparator:","} }
		,{ label: '(4)포인트결제 완료'			,name: 'step_cnt_07'	,width: cWidth * 0.075 	,align:'right'	,formatter: 'integer',formatoptions:{thousandsSeparator:","} }
		,{ label: '(4-1)포인트결제 성공'		,name: 'sucs_cnt' 		,width: cWidth * 0.075 	,align:'right'	,formatter: 'integer',formatoptions:{thousandsSeparator:","} }
		,{ label: '(4-2)포인트결제 실패'		,name: 'fail_cnt' 		,width: cWidth * 0.075 	,align:'right'	,formatter: 'integer',formatoptions:{thousandsSeparator:","} }
		,{ label: '(2)/(1)'						,name: 'a' 				,width: cWidth * 0.05 	,align:'right'	,formatter: cmsnA	}
		,{ label: '(3)/(2)'						,name: 'b' 				,width: cWidth * 0.05 	,align:'right'	,formatter: cmsnB 	}
		,{ label: '(4)/(3)'						,name: 'c' 				,width: cWidth * 0.05 	,align:'right'	,formatter: cmsnC	}
		,{ label: '(4)/(1)'						,name: 'd' 				,width: cWidth * 0.05 	,align:'right'	,formatter: cmsnD	}
		,{ label: '(4-1)/(4)'					,name: 'e'	 			,width: cWidth * 0.05 	,align:'right'	,formatter: cmsnE	}
		,{ label: '(4-2)/(4)'					,name: 'f'	 			,width: cWidth * 0.05 	,align:'right'	,formatter: cmsnF	}
		,{ label: '거래일자'					,name: 'deal_dd'		,hidden:true 			}
		,{ label: '거래월'						,name: 'deal_mm'		,hidden:true 			}
	];
	PHJQg.loadTotalRow('jqGrid','jqGridPager',colNames,colModel);

	//검색일자 달력 셋팅
	var dt = new Date();
	dt.setDate(dt.getDate()-1);
	setDate(dt,dt);
	$("#d_date").show();
	
	//검색월 Select 셋팅
	firstYear.setYear(today.getFullYear() - GAP);
	for (var i=firstYear.getFullYear(); i<=today.getFullYear(); i++) {
		$('#start_year').append('<option value="'+ i +'">' +i+'년</option>');
		$('#end_year').append('<option value="'+ i +'">' +i+'년</option>');
	}
	$('#start_year option:last').prop('selected', true);
	$('#end_year option:last').prop('selected', true);
	$('#start_month').val(PHFnc.leadingZero(today.getMonth() + 1, 2));
	$('#end_month').val(PHFnc.leadingZero(today.getMonth() + 1, 2));
	
	//엑셀다운로드
	$("#btn_excel").on("click", function() {
		if (PHJQg.size('jqGrid') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
		if (!validateDate()) {
			return;
		}
		var conf         = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");
		var custCntLabel = ($('#sel_cnt_ind').val() == 'USE' ? "이용건수" : "이용자수");
		if(conf){
			if(validateDate()){	
				var postData;
				if($("#sel_ind").val() == "M"){
					mmdd = "M";
					postData = {
						SRCH_IND 	: $("#sel_ind").val()
						,CPRT_CMPN_ID : $("#sel_cmId").val()
			        	,START_DATE : $('#start_year').val()+""+$('#start_month').val()
		        		,END_DATE 	: $('#end_year').val()+""+$('#end_month').val()
					};
				} else if($("#sel_ind").val() == "D"){
					mmdd = "D";
					postData = {
						SRCH_IND 	: $("#sel_ind").val()
						,CPRT_CMPN_ID : $("#sel_cmId").val()
			        	,START_DATE : PHUtil.replaceAll($("#start_date").val(),"-","") 
			        	,END_DATE 	: PHUtil.replaceAll($("#end_date").val(),"-","") 
					};
				}
				postData['PH_SVC_CD'] = $('#sel_svcCd').val();
				postData["CNT_IND"] = $('#sel_cnt_ind').val();
				var colNames = ['거래일자','사용처','서비스명',($('#sel_cnt_ind').val() == 'USE' ? '이용건수' : '이용자수'),'(1)이용약관 동의/본인인증','(2)가용포인트 조회','(3)포인트결제 요청','(4)포인트결제 완료','(4-1)포인트결제 성공','(4-2)포인트결제 실패'
				                ,'(2)/(1)','(3)/(2)','(4)/(3)','(4)/(1)','(4-1)/(4)','(4-2)/(4)','거래일자','거래월'];
				var colModel = [{ label: '거래일자', name: '', formatter: mdChk }
					,{ label: '사용처'						,name: 'cprt_cmpn_nm' }
					,{ label: '서비스명'					,name: 'service_nm' }
					,{ label: custCntLabel					,name: 'cust_cnt'   ,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}	}
		       		,{ label: '(1)이용약관 동의/본인인증'	,name: 'step_cnt_01',formatter: 'integer'	,formatoptions:{thousandsSeparator:","}	}
		       		,{ label: '(2)가용포인트 조회'			,name: 'step_cnt_05',formatter: 'integer'	,formatoptions:{thousandsSeparator:","}	}
		       		,{ label: '(3)포인트결제 요청'			,name: 'step_cnt_06',formatter: 'integer'	,formatoptions:{thousandsSeparator:","}	}
		       		,{ label: '(4)포인트결제 완료'			,name: 'step_cnt_07',formatter: 'integer'	,formatoptions:{thousandsSeparator:","}	}
		       		,{ label: '(4-1)포인트결제 성공'		,name: 'sucs_cnt' 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}	}
		       		,{ label: '(4-2)포인트결제 실패'		,name: 'fail_cnt' 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}	}
		       		,{ label: '(2)/(1)'						,name: 'a' 			,formatter: cmsnA }
		       		,{ label: '(3)/(2)'						,name: 'b' 			,formatter: cmsnB }
			       	,{ label: '(4)/(3)'						,name: 'c' 			,formatter: cmsnC }
			       	,{ label: '(4)/(1)'						,name: 'd' 			,formatter: cmsnD }
			       	,{ label: '(4-1)/(4)'					,name: 'e'	 		,formatter: cmsnE }
			       	,{ label: '(4-2)/(4)'					,name: 'f'	 		,formatter: cmsnF }
		       		,{ label: '거래일자'					,name: 'deal_dd'	,hidden:true }
		    		,{ label: '거래월'						,name: 'deal_mm'	,hidden:true }
		       	];
				PHJQg.excel('jqGridExcelDiv','${ViewRoot}/statistic/jgUseStepStatExcel.do',postData,colNames,colModel,'결제단계별 이용건수');
			}
		}
	}) 
});
function cmsnA(cellvalue,options,rowobject){
	var rate = Math.round((rowobject.step_cnt_05 / rowobject.step_cnt_01 * 100));
	var isCheck = false;
	if (isInfinity(rate)) {
		isCheck = true;
	}
	if (!isCheck && isNaN(rate)) {
		isCheck = true;
	}
	return isCheck ? "-" : PHUtil.setComma(rate) +'%';
}
function cmsnB(cellvalue,options,rowobject){
	var rate = Math.round((rowobject.step_cnt_06 / rowobject.step_cnt_05 * 100));
	var isCheck = false;
	if (isInfinity(rate)) {
		isCheck = true;
	}
	if (!isCheck && isNaN(rate)) {
		isCheck = true;
	}
	return isCheck ? "-" : PHUtil.setComma(rate) +'%';
}
function cmsnC(cellvalue,options,rowobject){
	var rate = Math.round((rowobject.step_cnt_07 / rowobject.step_cnt_06 * 100));
	var isCheck = false;
	if (isInfinity(rate)) {
		isCheck = true;
	}
	if (!isCheck && isNaN(rate)) {
		isCheck = true;
	}
	return isCheck ? "-" : PHUtil.setComma(rate) +'%';
}
function cmsnD(cellvalue,options,rowobject){
	var rate = Math.round((rowobject.step_cnt_07 / rowobject.step_cnt_01 * 100));
	var isCheck = false;
	if (isInfinity(rate)) {
		isCheck = true;
	}
	if (!isCheck && isNaN(rate)) {
		isCheck = true;
	}
	return isCheck ? "-" : PHUtil.setComma(rate) +'%';
}
function cmsnE(cellvalue,options,rowobject){
	var rate = Math.round((rowobject.sucs_cnt / rowobject.step_cnt_07 * 100));
	var isCheck = false;
	if (isInfinity(rate)) {
		isCheck = true;
	}
	if (!isCheck && isNaN(rate)) {
		isCheck = true;
	}
	return isCheck ? "-" : PHUtil.setComma(rate) +'%';
}
function cmsnF(cellvalue,options,rowobject){
	var rate = Math.round((rowobject.fail_cnt / rowobject.step_cnt_07 * 100));
	var isCheck = false;
	if (isInfinity(rate)) {
		isCheck = true;
	}
	if (!isCheck && isNaN(rate)) {
		isCheck = true;
	}
	return isCheck ? "-" : PHUtil.setComma(rate) +'%';
}

var mmdd;
function mdChk(cellvalue,options,rowobject){
	if(mmdd == "D"){
		return rowobject.deal_dd;
	} else if(mmdd == "M"){
		return rowobject.deal_mm;
	} else{
		return "";
	}	
}

//검색버튼 클릭
function search() {
	if (!validateDate()) {
		return;
	}
	var postData;
	if($("#sel_ind").val() == "M"){
		mmdd = "M";
		$("#jqGrid").jqGrid('setLabel', "mmdd","거래월"); 
		postData = {
			SRCH_IND 	: $("#sel_ind").val()
			,CPRT_CMPN_ID : $("#sel_cmId").val()
        	,START_DATE : $('#start_year').val()+""+$('#start_month').val()
       		,END_DATE 	: $('#end_year').val()+""+$('#end_month').val()
		};
	} else if($("#sel_ind").val() == "D"){
		mmdd = "D";
		$("#jqGrid").jqGrid('setLabel', "mmdd","거래일자"); 
		postData = {
			SRCH_IND 	: $("#sel_ind").val()
			,CPRT_CMPN_ID : $("#sel_cmId").val()
        	,START_DATE : PHUtil.replaceAll($("#start_date").val(),"-","") 
        	,END_DATE 	: PHUtil.replaceAll($("#end_date").val(),"-","")
		};
	}
	postData['PH_SVC_CD'] = $('#sel_svcCd').val();
	postData["CNT_IND"] = $('#sel_cnt_ind').val();
	PHJQg.reloadCallback('jqGrid','jqGridPager','${ViewRoot}/statistic/jgUseStepStat.do',postData,function(o) {
		$(o).jqGrid("setLabel", 3, ($('#sel_cnt_ind').val() == 'USE' ? "이용건수" : "이용자수"));
		$(o).jqGrid('footerData','set', {
			mmdd:'Total',
			cust_cnt:PHJQg.colSum(o,'cust_cnt'),
			step_cnt_01:PHJQg.colSum(o,'step_cnt_01'),
			step_cnt_01:PHJQg.colSum(o,'step_cnt_01'),
			step_cnt_05:PHJQg.colSum(o,'step_cnt_05'),
			step_cnt_06:PHJQg.colSum(o,'step_cnt_06'),
			step_cnt_07:PHJQg.colSum(o,'step_cnt_07'),
			sucs_cnt:PHJQg.colSum(o,'sucs_cnt'),
			fail_cnt:PHJQg.colSum(o,'fail_cnt'),
			a:cmsnA,
			b:cmsnB,
			c:cmsnC,
			d:cmsnD,
			e:cmsnE,
			f:cmsnF
		});
	});
}
//검색일자 확인
function validateDate(){
	if($("#sel_ind").val() == "M"){
		var startDate = new Date($('#start_year').val()+ '-' + $('#start_month').val() + '-01');
		var endDate = new Date($('#end_year').val()+ '-' + $('#end_month').val() + '-01');
		if (startDate.getTime() > endDate.getTime()){		
			alert('뒤의 날짜는 앞의 날짜 이전으로 검색할 수 없습니다.');
			return false;
		} else {
			var valDate = new Date($('#start_year').val()+ '-' + $('#start_month').val() + '-01');
			valDate.setMonth(valDate.getMonth() + 11);
			if (endDate.getTime() > valDate.getTime()) {
				alert('검색 기간은 1년을 넘을 수 없습니다.');
				return false;
			}	
		}
		return true;		
	} else if($("#sel_ind").val() == "D"){
		var sDate = $("#start_date").val();
		var eDate = $("#end_date").val();
		if (!PHUtil.isEmpty(sDate) && !PHValid.date(PHUtil.replaceAll(sDate,'-',''))) {
			alert('시작일자가 유효한 날짜 형식이 아닙니다');
			$('#start_date').val('');
			$('#start_date').focus();
			return false;
		}
		if (!PHUtil.isEmpty(eDate) && !PHValid.date(PHUtil.replaceAll(eDate,'-',''))) {
			alert('종료일자가 유효한 날짜 형식이 아닙니다');
			$('#end_date').val('');
			$('#end_date').focus();
			return false;
		}
		var startDate = new Date(sDate);
		var endDate = new Date(eDate);
		if (startDate.getTime() > endDate.getTime()) {		
			alert('뒤의 날짜는 앞의 날짜 이전으로 검색할 수 없습니다.')
			return false;
		} else {
			var valDate = new Date(sDate);
			valDate.setMonth(valDate.getMonth()+1);
			if (endDate.getTime() > valDate.getTime()) {
				alert('검색 기간은 한달을 넘을 수 없습니다.');
				return false;
			}
		}
		return true;
	}	
}
//일/월 구분 선택시 선택 달력 변경
function sel_ind(type){
	if(type=="M"){
		$("#d_date").hide();
		$("#m_date").show();
	} else {
		$("#d_date").show();
		$("#m_date").hide();
	}
}
// Infinity Check
function isInfinity(val) {
	if (val == Infinity)
		return true;
	return false;
}

</script>