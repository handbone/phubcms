<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
 **********************************************************************************************
 * @desc : 연동시스템 오류 월별 통계 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/statistic/sysErrStatMViewJs.jsp
 * @author ojh
 * @since 2018.10.16
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.16    ojh        최초생성
 * 
 **********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
var cWidth2 = $("#content_p").width();
var GAP = 5;
var firstYear = new Date();
var today = new Date();
$(document).ready(function () { 
	var colNames = ['No','연동월','연동시스템명','오류건수','상세 오류내역','시스템코드'];
	var colModel = [{ label: 'No' ,name: 'no', width: cWidth * 0.05, key:true }
		,{ label: '연동월'			,name: 'if_mm'			,width: cWidth * 0.2 	,cellattr:jsFormatterCell	}
		,{ label: '연동시스템명'	,name: 'if_sys_nm' 		,width: cWidth * 0.3 	,align:'left' 			}
		,{ label: '오류건수'		,name: 'err_cnt'		,width: cWidth * 0.299 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","} ,align:'right'			}
		,{ label: '상세 오류내역'	,name: '' 				,width: cWidth * 0.15 	,formatter:dbutton		}
		,{ label: '시스템코드'		,name: 'if_sys_cd' 		,hidden:true		}
		
	];
	PHJQg.loadTotalRow('jqGrid','jqGridPager',colNames,colModel);
	
	//상세 오류내역 팝업
 	var colNames = ['No','오류코드','오류메시지','오류건수'];
	var colModel = [{ label: 'No', name: 'no', width: 100, key:true }
		,{ label: '오류코드'	,name: 'err_cd'		,width: 300 }
		,{ label: '오류메시지'	,name: 'err_msg' 	,width: 475 ,align:'left' 	}
		,{ label: '오류건수'	,name: 'err_cnt'	,width: 300 ,align:'right'	}
	];
	PHJQg.load('jqGrid2','jqGridPager2',colNames,colModel); 	
	
	// 대사일자 셋팅
	firstYear.setYear(today.getFullYear() - GAP);
	for (var i=firstYear.getFullYear(); i<=today.getFullYear(); i++) {
		$('#start_year').append('<option value="'+ i +'">' +i+'년</option>');
		$('#end_year').append('<option value="'+ i +'">' +i+'년</option>');
	}
	$('#start_year option:last').prop('selected', true);
	$('#end_year option:last').prop('selected', true);
	$('#start_month').val(PHFnc.leadingZero(today.getMonth() + 1, 2));
	$('#end_month').val(PHFnc.leadingZero(today.getMonth() + 1, 2));
	
	//엑셀다운로드
	$("#btn_excel").on("click", function(){
		if (PHJQg.size('jqGrid') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
		if (!validateDate()) {
			return;
		}
		var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");	
		if(conf){		
			var postData = {
				IF_SYS_CD 	: $("#sel_sysId").val()
				,ERR_CD 	: $("#in_errCd").val()
	        	,START_DATE : $('#start_year').val()+""+$('#start_month').val()
	        	,END_DATE 	: $('#end_year').val()+""+$('#end_month').val()
			};
			var colNames = ['No','연동월','연동시스템명','오류건수'];
			var colModel = [{ label: 'No', name: 'no', key:true }
				,{ label: '연동월'			,name: 'if_mm'		,cellattr:jsFormatterCell	}
				,{ label: '연동시스템명'	,name: 'if_sys_nm' 	,align:'left' 			}
				,{ label: '오류건수'		,name: 'err_cnt'	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","} ,align:'right' }
			];
			PHJQg.excel('jqGridExcelDiv','${ViewRoot}/statistic/jgSysErrStatMExcel.do',postData,colNames,colModel,'연동시스템 오류 월별 통계');
		}
	})
});

//jqGrid 셀병합 포맷
var chkcell={cellId:undefined, chkval:undefined};
function jsFormatterCell(rowid, val, rowObject, cm, rdata){

  var result = "";     
  if(chkcell.chkval != val){ //check 값이랑 비교값이 다른 경우
      var cellId = this.id + '_row_'+rowid+'-'+cm.name;
      result = ' rowspan="1" id ="'+cellId+'" + name="cellRowspan"';
      chkcell = {cellId:cellId, chkval:val};
  }else{
      result = 'style="display:none"  rowspanid="'+chkcell.cellId+'"'; //같을 경우 display none 처리
  }
  return result;
}
//formatter 버튼 만들어 주기 클릭시 No전달
function dbutton(cellvalue,options,rowobject){
    return '<input type="button" class="table_btn" onclick="popClick('+rowobject.no+')" value="확인" />';
}

//검색버튼 클릭
function search() {
	if (!validateDate()) {
		return;
	}
	var postData = {
		IF_SYS_CD 	: $("#sel_sysId").val()
		,ERR_CD 	: $("#in_errCd").val()
       	,START_DATE : $('#start_year').val()+""+$('#start_month').val()
       	,END_DATE 	: $('#end_year').val()+""+$('#end_month').val()
	};
	chkcell={cellId:undefined, chkval:undefined};
	PHJQg.reloadCallback('jqGrid','jqGridPager','${ViewRoot}/statistic/jgSysErrStatM.do',postData,function(o) {
		$(o).jqGrid('footerData','set', {
			no:'Total',
			err_cnt:PHJQg.colSum(o,'err_cnt')
		});
	});
}

function validateDate() {
	var startDate = new Date($('#start_year').val()+ '-' + $('#start_month').val() + '-01');
	var endDate = new Date($('#end_year').val()+ '-' + $('#end_month').val() + '-01');
	if (startDate.getTime() > endDate.getTime()){		
		alert('뒤의 날짜는 앞의 날짜 이전으로 검색할 수 없습니다.');
		return false;
	} else {
		var valDate = new Date($('#start_year').val()+ '-' + $('#start_month').val() + '-01');
		valDate.setMonth(valDate.getMonth() + 11);
		if (endDate.getTime() > valDate.getTime()) {
			alert('검색 기간은 1년을 넘을 수 없습니다.');
			return false;
		}
	}
	return true;
}

//클릭시 상세 정보 창에 data 셋팅
function popClick(data){
	
	var row = $("#jqGrid").jqGrid('getRowData',data);		
	$("#d_mm").html(row.if_mm);
	$("#d_sys").html(row.if_sys_nm);	
	$("#d_cnt").html(row.err_cnt);	
	
	//jqGrid aJax호출하여 가져오기
	var postData = {
		IF_MM 		: row.if_mm
		, IF_SYS_CD : row.if_sys_cd
	};
	PHJQg.reload('jqGrid2','jqGridPager2','${ViewRoot}/statistic/jgSysErrStatMDtl.do',postData);
	PHFnc.layerPopOpen(1);
	jQuery("#jqGrid2").jqGrid('setGridWidth', $("#content_p").width());
}
 
</script>