<%@  page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
 **********************************************************************************************
 * @desc : 포인트제공처별 결제실패 원인별별 이용건수 통계
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/statistic/payErrPntStatView.jsp
 * @author ojh
 * @since 2018.11.20
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.11.20    ojh        최초생성
 * 2019.05.28    kimht        include 수정
 **********************************************************************************************
--%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<%-- global --%>
	<jsp:include page="/WEB-INF/views/include/globalVar.jsp" flush="false" />
	<%-- head --%>
	<jsp:include page="/WEB-INF/views/include/incHead.jsp" flush="false" />
</head>
<body>
<!-- loader 시작-->
<div class="loader" onclick="loader_close()">
    <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
</div>
<!-- loader 끝-->
<div id="wrap">	
<header>
	<%-- header --%>
	<jsp:include page="/WEB-INF/views/include/incHeader.jsp" flush="false" />
</header>
    <!-- 레프트 메뉴 해당페이지 표시 -->
    <style>${sessionScope.USER_MENU.menu_id}{ color: #fff !important; background: #3f4759 !important; border-left: 3px solid #fff; cursor: hand;} </style>
    <!-- 레프트 메뉴 해당페이지 표시 끝 -->    
	
<!-- dash_board 시작-->
<section id="dash_board">
    <!--side_menu 시작-->
	<jsp:include page="/WEB-INF/views/include/incMenu.jsp" flush="false" />
    <!--side_menu 끝-->
	
	<!--contents 시작-->
    <div class="contents mCustomScrollbar">
        <div class="page_title"><div class="page_name">${sessionScope.USER_MENU.menu_nm}</div><div class="page_location"><i class="fas fa-home"></i>${sessionScope.USER_MENU.mnu_path}</div></div>
        <div class="subject">
            <div class="page_search">
                <table class="search_tb" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="search_item">
                            <ul>
                            	<div id="d_date" style="display:none">
                                	<li>거래일자 <input type="text" name="name" id="start_date" readonly> ~ <input type="text" name="name"  id="end_date" readonly></li>
                                </div>
                                <div id="m_date" style="display:none">
                                	<li>거래월
                                	<select id="start_year"></select> 
                                	<select id="start_month">
                               		<c:forEach var="i" begin="1" end="12">
                               			<c:choose>
                               				<c:when test="${i < 10}"><option value="0${i}"><c:out value="${i}월" /></option></c:when>
                               				<c:otherwise><option value="${i}"><c:out value="${i}월" /></option></c:otherwise>
                               			</c:choose>
                               		</c:forEach>
                               		</select>
                                	&nbsp;~&nbsp;
                                	<select id="end_year"></select> 
                                	<select id="end_month">
                               		<c:forEach var="i" begin="1" end="12">
                               			<c:choose>
                               				<c:when test="${i < 10}"><option value="0${i}"><c:out value="${i}월" /></option></c:when>
                               				<c:otherwise><option value="${i}"><c:out value="${i}월" /></option></c:otherwise>
                               			</c:choose>
                               		</c:forEach>
                                	</select>
                                </li>
                                </div>
                                <li>일/월구분
                                    <select id="sel_ind" onchange="sel_ind(this.value)">
                                        <option value="D">일</option>
                                        <option value="M">월</option>
                                    </select>
                                </li>
								<li>이용구분
                                	<select id="sel_cnt_ind">
                                		<option value="USE">이용건수</option>
                                		<option value="USR">이용자수</option>
                                	</select>
                                </li>
                                <li>제공처
			                    	<select id="sel_card">
			                            <option value="all">전체</option>
			                            <c:forEach items="${PV_CMPN}" var="code">
			                           		<option value="${code.pnt_cd}">${code.prvdr_nm}</option>
			                           	</c:forEach>
			                        </select> 
								</li>
                            </ul>
                        </td>
                        <td class="btn_search">
                            <button type="button" onClick="search()">조회</button>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="page_contents">
                <div class="btn_excel"><button id="btn_excel">엑셀다운</button></div>
                <div id="content">
                    <table id="jqGrid"></table>                
                    <div id="jqGridPager"></div>
                    <div id="jqGridExcelDiv" style="display:none">
                		<table id="jqGridExcel" ></table>
                	</div>
                	<div style="color:#000000">
                	ⓐ :포인트 차감 결제 전체 실패건수/실패자수<br>
                	①~⑦:포인트차감 결제 실패 빈도 높은 상위 7개 건수/자수
                	</div>

                </div>
                <!-- jqGrid  끝 -->
            </div>
        </div>

        <!-- 스크롤 때문에 여백 있어야함 -->
        <br>
        <br>
        <br>
        <br>
        <br>
    </div>
    <!--contents 끝-->
    
</section>
<!-- dash_board 끝--> 

    <!-- footer 시작-->
    <jsp:include page="/WEB-INF/views/include/incFooter.jsp" flush="false" />
    <section class="footer">	 
    </section>
    <!-- footer 끝-->
    
    <%-- js --%>
	<jsp:include page="./payErrPntStatViewJs.jsp" flush="false" />
	
</body>
</html>