<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
 **********************************************************************************************
 * @desc : 연동시스템 오류 일별 통계 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/statistic/sysErrStatDViewJs.jsp
 * @author ojh
 * @since 2018.10.15
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.15    ojh        최초생성
 * 
 **********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
var cWidth2 = $("#content_p").width();
$(document).ready(function () { 
	var colNames = ['No','연동일자','연동시스템명','오류건수','상세 오류내역','시스템코드'];
	var colModel = [{ label: 'No', name: 'no', width: cWidth * 0.05, key:true }
		,{ label: '연동일자'		,name: 'if_dd'			,width: cWidth * 0.2 	,cellattr:jsFormatterCell	}
		,{ label: '연동시스템명'	,name: 'if_sys_nm' 		,width: cWidth * 0.3 	,align:'left' 			}
		,{ label: '오류건수'		,name: 'err_cnt'		,width: cWidth * 0.299 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","} ,align:'right'			}
		,{ label: '상세 오류내역'	,name: '' 				,width: cWidth * 0.15 	,formatter:dbutton		}
		,{ label: '시스템코드'		,name: 'if_sys_cd' 		,hidden:true		}
		
	];
	PHJQg.loadTotalRow('jqGrid','jqGridPager',colNames,colModel);
	
	//상세 오류내역 팝업
 	var colNames = ['No','오류코드','오류메시지','오류건수'];
	var colModel = [{ label: 'No', name: 'no' ,width: 100, key:true }
		,{ label: '오류코드'		,name: 'err_cd'		,width: 300 }
		,{ label: '오류메시지'		,name: 'err_msg' 	,width: 475		,align:'left' 	}
		,{ label: '오류건수'		,name: 'err_cnt'	,width: 300 	,align:'right'	}
	];
	PHJQg.load('jqGrid2','jqGridPager2',colNames,colModel); 	
	
	//검색일자 달력 셋팅
	var dt = new Date();
	dt.setDate(dt.getDate()-1);
	setDate(dt,dt);
	
	//엑셀다운로드
	$("#btn_excel").on("click", function() {
		if (PHJQg.size('jqGrid') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
		if (!validateDate()) {
			return;
		}
		var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");	
		if(conf){		
			var postData = {
				IF_SYS_CD 	: $("#sel_sysId").val()
				,ERR_CD 	: $("#in_errCd").val()
	        	,START_DATE : PHUtil.replaceAll($("#start_date").val(),"-","") 
	        	,END_DATE 	: PHUtil.replaceAll($("#end_date").val(),"-","") 
			};
			var colNames = ['No','연동일자','연동시스템명','오류건수'];
			var colModel = [{ label: 'No', name: 'no', key:true }
				,{ label: '연동일자'		,name: 'if_dd'		,cellattr:jsFormatterCell }
				,{ label: '연동시스템명'	,name: 'if_sys_nm' 	,align:'left' }
				,{ label: '오류건수'		,name: 'err_cnt'	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","} ,align:'right' }
			];
			PHJQg.excel('jqGridExcelDiv','${ViewRoot}/statistic/jgSysErrStatDExcel.do',postData,colNames,colModel,'연동시스템 오류 일별 통계');
		}
	}) 
});

//jqGrid 셀병합 포맷
var chkcell={cellId:undefined, chkval:undefined};
function jsFormatterCell(rowid, val, rowObject, cm, rdata){

  var result = "";     
  if(chkcell.chkval != val){ //check 값이랑 비교값이 다른 경우
      var cellId = this.id + '_row_'+rowid+'-'+cm.name;
      result = ' rowspan="1" id ="'+cellId+'" + name="cellRowspan"';
      chkcell = {cellId:cellId, chkval:val};
  }else{
      result = 'style="display:none"  rowspanid="'+chkcell.cellId+'"'; //같을 경우 display none 처리
  }
  return result;
}
//formatter 버튼 만들어 주기 클릭시 No전달
function dbutton(cellvalue,options,rowobject){
    return '<input type="button" class="table_btn" onclick="popClick('+rowobject.no+')" value="확인" />';
}

//검색버튼 클릭
function search() {
	if (!validateDate()) {
		return;
	}
	var postData = {
		IF_SYS_CD 	: $("#sel_sysId").val()
		,ERR_CD 	: $("#in_errCd").val()
       	,START_DATE : PHUtil.replaceAll($("#start_date").val(),"-","") 
       	,END_DATE 	: PHUtil.replaceAll($("#end_date").val(),"-","") 
	};
	chkcell={cellId:undefined, chkval:undefined};
	PHJQg.reloadCallback('jqGrid','jqGridPager','${ViewRoot}/statistic/jgSysErrStatD.do',postData,function(o) {
		$(o).jqGrid('footerData','set', {
			no:'Total',
			err_cnt:PHJQg.colSum(o,'err_cnt')
		});
	});
}
//기간 조건 체크
function validateDate(){
	var sDate = $("#start_date").val();
	var eDate = $("#end_date").val();
	if (!PHUtil.isEmpty(sDate) && !PHValid.date(PHUtil.replaceAll(sDate,'-',''))) {
		alert('시작일자가 유효한 날짜 형식이 아닙니다');
		$('#start_date').val('');
		$('#start_date').focus();
		return false;
	}
	if (!PHUtil.isEmpty(eDate) && !PHValid.date(PHUtil.replaceAll(eDate,'-',''))) {
		alert('종료일자가 유효한 날짜 형식이 아닙니다');
		$('#end_date').val('');
		$('#end_date').focus();
		return false;
	}
	var startDate = new Date(sDate);
	var endDate = new Date(eDate);
	if (startDate.getTime() > endDate.getTime()) {		
		alert('뒤의 날짜는 앞의 날짜 이전으로 검색할 수 없습니다.')
		return false;
	} else {
		var valDate = new Date(sDate);
		valDate.setMonth(valDate.getMonth()+1);
		if (endDate.getTime() > valDate.getTime()) {
			alert('검색 기간은 한달을 넘을 수 없습니다.');
			return false;
		}
	}
	return true;
}

//클릭시 상세 정보 창에 data 셋팅
function popClick(data){
	
	var row = $("#jqGrid").jqGrid('getRowData',data);		
	$("#d_dd").html(row.if_dd);
	$("#d_sys").html(row.if_sys_nm);	
	$("#d_cnt").html(row.err_cnt);	
	
	//jqGrid aJax호출하여 가져오기
	var postData = {
		IF_DD 		: row.if_dd
		, IF_SYS_CD : row.if_sys_cd
	};
	PHJQg.reload('jqGrid2','jqGridPager2','${ViewRoot}/statistic/jgSysErrStatDDtl.do',postData);
	PHFnc.layerPopOpen(1);
	jQuery("#jqGrid2").jqGrid('setGridWidth', $("#content_p").width());
}

</script>