<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
 **********************************************************************************************
 * @desc : 가맹점별 결제실패 원인별 이용건수 통계 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/statistic/payErrCprtStatViewJs.jsp
 * @author ojh
 * @since 2018.11.16
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.11.16    ojh        최초생성
 * 
 **********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
var GAP = 5;
var firstYear = new Date();
var today = new Date();
$(document).ready(function () { 
	var colNames = ['거래일자','사용처','이용건수/<br>이용자수','(a)포인트결제<br>실패(소계)','(1)','(2)','(3)','(4)','(5)','(6)','(7)','(8)포인트결제<br>실패(기타)'
	                ,'(1)/(a)','(2)/(a)','(3)/(a)','(4)/(a)','(5)/(a)','(6)/(a)','(7)/(a)','(8)/(a)'
	                ,'거래일자','거래월'];
	var colModel = [{ label: '거래일자', name: 'mmdd', width: cWidth * 0.07, formatter:mdChk, key:true}
		,{ label: '사용처'				,name: 'cprt_cmpn_nm'	,width: cWidth * 0.108	,align:'left'}
		,{ label: '이용건수/이용자수'	,name: 'cust_cnt'		,width: cWidth * 0.075 	,formatter: 'integer'	,align:'right' ,formatoptions:{thousandsSeparator:","}}
		,{ label: '(a)'					,name: 'err_cnt'		,width: cWidth * 0.075 	,formatter: 'integer'	,align:'right' ,formatoptions:{thousandsSeparator:","}}
		,{ label: '(1)'					,name: 'cnt_1'			,width: cWidth * 0.10  	,formatter: 'integer'	,align:'right' ,formatoptions:{thousandsSeparator:","}}
		,{ label: '(2)'					,name: 'cnt_2'			,width: cWidth * 0.10 	,formatter: 'integer'	,align:'right' ,formatoptions:{thousandsSeparator:","}}
		,{ label: '(3)'					,name: 'cnt_3'			,width: cWidth * 0.10 	,formatter: 'integer'	,align:'right' ,formatoptions:{thousandsSeparator:","}}
		,{ label: '(4)'					,name: 'cnt_4' 			,width: cWidth * 0.10 	,formatter: 'integer'	,align:'right' ,formatoptions:{thousandsSeparator:","}}
		,{ label: '(5)'					,name: 'cnt_5' 			,width: cWidth * 0.10 	,formatter: 'integer'	,align:'right' ,formatoptions:{thousandsSeparator:","}}
		,{ label: '(6)'					,name: 'cnt_6' 			,width: cWidth * 0.10 	,formatter: 'integer'	,align:'right' ,formatoptions:{thousandsSeparator:","}}
		,{ label: '(7)'					,name: 'cnt_7' 			,width: cWidth * 0.10	,formatter: 'integer'	,align:'right' ,formatoptions:{thousandsSeparator:","}}
		,{ label: '(8)'					,name: 'cnt_a' 			,width: cWidth * 0.075 	,formatter: 'integer'	,align:'right' ,formatoptions:{thousandsSeparator:","}}
		,{ label: '(1)/(a)'				,name: 'a' 				,width: cWidth * 0.05 	,formatter: cmsnA		,align:'right'}
		,{ label: '(2)/(a)'				,name: 'b' 				,width: cWidth * 0.05 	,formatter: cmsnB		,align:'right'}
		,{ label: '(3)/(a)'				,name: 'c' 				,width: cWidth * 0.05 	,formatter: cmsnC		,align:'right'}
		,{ label: '(4)/(a)'				,name: 'd' 				,width: cWidth * 0.05 	,formatter: cmsnD		,align:'right'}
		,{ label: '(5)/(a)'				,name: 'e' 				,width: cWidth * 0.05 	,formatter: cmsnE		,align:'right'}
		,{ label: '(6)/(a)'				,name: 'f' 				,width: cWidth * 0.05 	,formatter: cmsnF		,align:'right'}
		,{ label: '(7)/(a)'				,name: 'g' 				,width: cWidth * 0.05 	,formatter: cmsnG		,align:'right'}
		,{ label: '(8)/(a)'				,name: 'h' 				,width: cWidth * 0.05 	,formatter: cmsnH		,align:'right'}
		,{ label: '거래일자'			,name: 'deal_dd'		,hidden:true}
		,{ label: '거래월'				,name: 'deal_mm'		,hidden:true}
	];
	PHJQg.loadTotalRow('jqGrid','jqGridPager',colNames,colModel);
	
	//검색일자 달력 셋팅
	var dt = new Date();
	dt.setDate(dt.getDate()-1);
	setDate(dt,dt);
	$("#d_date").show();
	
	//검색월 Select 셋팅
	firstYear.setYear(today.getFullYear() - GAP);
	for (var i=firstYear.getFullYear(); i<=today.getFullYear(); i++) {
		$('#start_year').append('<option value="'+ i +'">' +i+'년</option>');
		$('#end_year').append('<option value="'+ i +'">' +i+'년</option>');
	}
	$('#start_year option:last').prop('selected', true);
	$('#end_year option:last').prop('selected', true);
	$('#start_month').val(leadingZero((today.getMonth() + 1), 2));
	$('#end_month').val(leadingZero((today.getMonth() + 1), 2));
	
	//엑셀다운로드
	$("#btn_excel").on("click", function() {
		if (PHJQg.size('jqGrid') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
		if (!validateDate()) {
			return;
		}
		var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");	
		if(conf){
			var postData;
			if($("#sel_ind").val() == "M"){
				mmdd = "M";
				$("#jqGridExcel").jqGrid('setLabel', "mmdd","거래월"); 
				postData = {
					SRCH_IND 	: $("#sel_ind").val()
					,CPRT_CMPN_ID : $("#sel_cmId").val()
		        	,START_DATE : $('#start_year').val()+""+$('#start_month').val()+"01"
	        		,END_DATE 	: $('#end_year').val()+""+$('#end_month').val()+"31"
				};
			} else if($("#sel_ind").val() == "D"){
				mmdd = "D";
				$("#jqGridExcel").jqGrid('setLabel', "mmdd","거래일자"); 
				postData = {
					SRCH_IND 	: $("#sel_ind").val()
					,CPRT_CMPN_ID : $("#sel_cmId").val()
		        	,START_DATE : PHUtil.replaceAll($("#start_date").val(),"-","") 
		        	,END_DATE 	: PHUtil.replaceAll($("#end_date").val(),"-","") 
				};
			}
			postData["CNT_IND"] = $('#sel_cnt_ind').val();
			if ($("#sel_ind").val() == "M") {
				colNames[0] = '거래월';
				colModel[0].label = "거래월";
			} else if ($("#sel_ind").val() == "D") {
				colNames[0] = '거래일자';
				colModel[0].label = "거래일자";
			}
			var colNamesExcel = $.map(jQuery('#jqGrid').jqGrid('getGridParam','colNames'), function(o,i) {
	    		return o.replace(/(<br>)/g,'')+' ';
	    	});
	    	var colModelExcel = $.map(jQuery('#jqGrid').jqGrid('getGridParam','colModel'), function(o,i) {
	    		return o;
	    	});
			PHJQg.excel('jqGridExcelDiv','${ViewRoot}/statistic/jgPayErrCprtStatExcel.do',postData,colNamesExcel,colModelExcel,'가맹점별 결제실패 원인별 이용건수');
		}
	});
});

function leadingZero(num, size) {
	var s = num + "";
	while (s.length < size) {
		s = '0' + s;
	}
	return s;
};

function cmsnA(cellvalue,options,rowobject){
	var rate = Math.round((rowobject.cnt_1 / rowobject.err_cnt * 100));	
	return (isNaN(rate)?"-":rate) + " %";
}
function cmsnB(cellvalue,options,rowobject){
	var rate = Math.round((rowobject.cnt_2 / rowobject.err_cnt * 100));
	return (isNaN(rate)?"-":rate) + " %";
}
function cmsnC(cellvalue,options,rowobject){
	var rate = Math.round((rowobject.cnt_3 / rowobject.err_cnt * 100));
	return (isNaN(rate)?"-":rate) + " %";
}
function cmsnD(cellvalue,options,rowobject){
	var rate = Math.round((rowobject.cnt_4 / rowobject.err_cnt * 100));
	return (isNaN(rate)?"-":rate) + " %";
}
function cmsnE(cellvalue,options,rowobject){
	var rate = Math.round((rowobject.cnt_5 / rowobject.err_cnt * 100));
	return (isNaN(rate)?"-":rate) + " %";
}
function cmsnF(cellvalue,options,rowobject){
	var rate = Math.round((rowobject.cnt_6 / rowobject.err_cnt * 100));
	return (isNaN(rate)?"-":rate) + " %";
}
function cmsnG(cellvalue,options,rowobject){
	var rate = Math.round((rowobject.cnt_7 / rowobject.err_cnt * 100));
	return (isNaN(rate)?"-":rate) + " %";
}
function cmsnH(cellvalue,options,rowobject){
	var rate = Math.round((rowobject.cnt_a / rowobject.err_cnt * 100));
	return (isNaN(rate)?"-":rate) + " %";
}

var mmdd;
function mdChk(cellvalue,options,rowobject){
	if(mmdd == "D"){
		return rowobject.deal_dd;
	} else if(mmdd == "M"){
		return rowobject.deal_mm;
	} else{
		return "";
	}	
}

//검색버튼 클릭
function search() {
	if (!validateDate()) {
		return;
	}
	for(var i=1; i<8; i++){
		$("#jqGrid").jqGrid('setLabel',"cnt_"+i,"("+i+")");
	}
	var postData;
	if($("#sel_ind").val() == "M"){
		mmdd = "M";
		$("#jqGrid").jqGrid('setLabel', "mmdd","거래월"); 
		postData = {
			SRCH_IND 	: $("#sel_ind").val()
			,CPRT_CMPN_ID : $("#sel_cmId").val()
        	,START_DATE : $('#start_year').val()+""+$('#start_month').val()+"01"
       		,END_DATE 	: $('#end_year').val()+""+$('#end_month').val()+"31"
		};
	} else if($("#sel_ind").val() == "D"){
		mmdd = "D";
		$("#jqGrid").jqGrid('setLabel', "mmdd","거래일자"); 
		postData = {
			SRCH_IND 	: $("#sel_ind").val()
			,CPRT_CMPN_ID : $("#sel_cmId").val()
        	,START_DATE : PHUtil.replaceAll($("#start_date").val(),"-","") 
        	,END_DATE 	: PHUtil.replaceAll($("#end_date").val(),"-","") 
		};
	}
	postData["CNT_IND"] = $('#sel_cnt_ind').val();
	PHJQg.reloadCallback2('jqGrid','jqGridPager','${ViewRoot}/statistic/jgPayErrCprtStat.do',postData,function(o) {
			$(o).jqGrid("setLabel", 2, ($('#sel_cnt_ind').val() == 'USE' ? "이용건수" : "이용자수"));
			$(o).jqGrid('footerData','set', {
				mmdd:'Total',
				cust_cnt:PHJQg.colSum(o,'cust_cnt'),
				err_cnt:PHJQg.colSum(o,'err_cnt'),
				cnt_1:PHJQg.colSum(o,'cnt_1'),
				cnt_2:PHJQg.colSum(o,'cnt_2'),
				cnt_3:PHJQg.colSum(o,'cnt_3'),
				cnt_4:PHJQg.colSum(o,'cnt_4'),
				cnt_5:PHJQg.colSum(o,'cnt_5'),
				cnt_6:PHJQg.colSum(o,'cnt_6'),
				cnt_7:PHJQg.colSum(o,'cnt_7'),
				cnt_a:PHJQg.colSum(o,'cnt_a'),
				a:PHJQg.colSum(o,'a'),
				b:PHJQg.colSum(o,'b'),
				c:PHJQg.colSum(o,'c'),
				d:PHJQg.colSum(o,'d'),
				e:PHJQg.colSum(o,'e'),
				f:PHJQg.colSum(o,'f'),
				g:PHJQg.colSum(o,'g'),
				h:PHJQg.colSum(o,'h')
			});
		},function(data) {
			PHFnc.log(data);				
			if(data.rank != null){
				var sub;
				for(var i=1; i<data.rank.length + 1; i++){						
					if(data.rank[i-1].err_nm != null){
						sub = data.rank[i-1].err_nm
					} else{
						sub = "";
					}
					$("#jqGrid").jqGrid('setLabel',"cnt_"+i,"("+i+")포인트결제 실패<br>("+sub+")");
				}
			}
		}
	);
}

//검색일자 확인
function validateDate(){
	if($("#sel_ind").val() == "M"){
		var startDate = new Date($('#start_year').val()+ '-' + $('#start_month').val() + '-01');
		var endDate = new Date($('#end_year').val()+ '-' + $('#end_month').val() + '-01');
		if (startDate.getTime() > endDate.getTime()){		
			alert('뒤의 날짜는 앞의 날짜 이전으로 검색할 수 없습니다.');
			return false;
		} else {
			var valDate = new Date($('#start_year').val()+ '-' + $('#start_month').val() + '-01');
			valDate.setMonth(valDate.getMonth() + 11);
			if (endDate.getTime() > valDate.getTime()) {
				alert('검색 기간은 1년을 넘을 수 없습니다.');
				return false;
			}
		}
		return true;		
	} else if($("#sel_ind").val() == "D"){
		var sDate = $("#start_date").val();
		var eDate = $("#end_date").val();
		if (!PHUtil.isEmpty(sDate) && !PHValid.date(PHUtil.replaceAll(sDate,'-',''))) {
			alert('시작일자가 유효한 날짜 형식이 아닙니다');
			$('#start_date').val('');
			$('#start_date').focus();
			return false;
		}
		if (!PHUtil.isEmpty(eDate) && !PHValid.date(PHUtil.replaceAll(eDate,'-',''))) {
			alert('종료일자가 유효한 날짜 형식이 아닙니다');
			$('#end_date').val('');
			$('#end_date').focus();
			return false;
		}
		var startDate = new Date(sDate);
		var endDate = new Date(eDate);
		if (startDate.getTime() > endDate.getTime()) {		
			alert('뒤의 날짜는 앞의 날짜 이전으로 검색할 수 없습니다.')
			return false;
		} else {
			var valDate = new Date(sDate);
			valDate.setMonth(valDate.getMonth()+1);
			if (endDate.getTime() > valDate.getTime()) {
				alert('검색 기간은 한달을 넘을 수 없습니다.');
				return false;
			}
		}
		return true;
	}	
}
//일/월 구분 선택시 선택 달력 변경
function sel_ind(type){
	if(type=="M"){
		$("#d_date").hide();
		$("#m_date").show();
	} else {
		$("#d_date").show();
		$("#m_date").hide();
	}
}

</script>