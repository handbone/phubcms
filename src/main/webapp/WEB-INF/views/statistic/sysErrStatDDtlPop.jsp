<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--
 **********************************************************************************************
 * @desc : 연동시스템 오류 일별 통계 상세 팝업
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/statistic/sysErrStatDDtlPop.jsp
 * @author ojh
 * @since 2018.10.15
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.15    ojh        최초생성
 *
 **********************************************************************************************
sysErrStatDView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>    
<!--상세 오류 내역 시작 -->
    <div class="popup_content">
        <div class="popup_title">상세 오류 내역<a href="#!"><div class="top_close"></div></a></div>
        <table class="info_table" border="0" cellpadding="0" cellspacing="0">
            <tbody>
            <tr>
                <th>연동일자</th>
                <th>연동시스템명</th>
                <th>오류건수</th>
            </tr>
            <tr>
                <td id="d_dd"></td>
                <td id="d_sys"></td>
                <td id="d_cnt"></td>
            </tr>
            </tbody>
        </table>

        <div id="content_p">
            <table id="jqGrid2"></table>
            <div id="jqGridPager2"></div>
        </div>

        <a href="#!"><div class="btn_close">닫기</div></a>
        <!-- 상세거래요청내역 끝-->
    </div>
    </div>
    <div id="myPopup" class="popup"></div>
    <!-- popup 끝-->