<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
 **********************************************************************************************
 * @desc : 포인트거래 사용처별 일별 통계 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/statistic/pntDealCmpnStatDViewJs.jsp
 * @author ojh
 * @since 2018.10.11
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.11    ojh        최초생성
 * 
 **********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
$(document).ready(function () { 
	var colNames = ['No','거래일자','PG사명','사용처명','정산포인트','사용포인트<br>건수','사용포인트','취소포인트<br>건수','취소포인트','사용실패포인트<br>건수','사용실패포인트','취소실패포인트<br>건수','취소실패포인트','서비스명'];
	var colModel = [{ label: 'No', name: 'no', width: cWidth * 0.05, key:true }
		,{ label: '거래일자'			,name: 'deal_dd'		,width: cWidth * 0.072 	,cellattr:jsFormatterCell }
		,{ label: 'PG사명'				,name: 'pg_cmpn_nm' 	,width: cWidth * 0.1 	,cellattr:jsFormatterCell, align:'left' }
		,{ label: '사용처명 '			,name: 'cprt_cmpn_nm' 	,width: cWidth * 0.1 	,align:'left' }
		,{ label: '정산포인트'			,name: 'cm_pnt' 		,width: cWidth * 0.08 	,formatter:cm_pnt		,align:'right' }
		,{ label: '사용포인트 건수'		,name: 'pay_cnt'		,width: cWidth * 0.07 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
		,{ label: '사용포인트'			,name: 'pay_pnt'		,width: cWidth * 0.08 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
		,{ label: '취소포인트 건수'		,name: 'cncl_cnt' 		,width: cWidth * 0.07 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
		,{ label: '취소포인트'			,name: 'cncl_pnt' 		,width: cWidth * 0.08 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
		,{ label: '사용실패포인트 건수'	,name: 'pay_fail_cnt' 	,width: cWidth * 0.10 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
		,{ label: '사용실패포인트'		,name: 'pay_fail_pnt' 	,width: cWidth * 0.09 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
		,{ label: '취소실패포인트 건수'	,name: 'cncl_fail_cnt' 	,width: cWidth * 0.10 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
		,{ label: '취소실패포인트'		,name: 'cncl_fail_pnt' 	,width: cWidth * 0.09 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
		,{ label: '서비스명'			,name: 'service_nm' 	,width: cWidth * 0.10 }
	];
	PHJQg.loadTotalRow('jqGrid','jqGridPager',colNames,colModel);

	//검색일자 달력 셋팅
	var dt = new Date();
	dt.setDate(dt.getDate()-1);
	setDate(dt,dt);
	
	//엑셀다운로드
	$("#btn_excel").on("click", function() {
		if (PHJQg.size('jqGrid') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
		if (!validateDate()) {
			return;
		}
		var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");	
		if(conf){		
			var postData = { 
				SRCH_IND 	: $("#sel_ind").val()
				,PG_CMPN_ID 	: $("#sel_pgId").val()
				,CPRT_CMPN_ID	: $("#sel_cmId").val()
				,PH_SVC_CD      : $('#sel_svcCd').val()
	        	,START_DATE 	: PHUtil.replaceAll($("#start_date").val(),"-","") 
	        	,END_DATE 		: PHUtil.replaceAll($("#end_date").val(),"-","") 
			};
			var colNames = ['No','거래일자','PG사명','사용처명','정산포인트','사용포인트건수','사용포인트','취소포인트건수','취소포인트','사용실패포인트건수','사용실패포인트','취소실패포인트건수','취소실패포인트','서비스명'];
			var colModel = [{ label: 'No', name: 'no', key:true }
						,{ label: '거래일자'			,name: 'deal_dd'		,cellattr:jsFormatterCell }
						,{ label: 'PG사명'				,name: 'pg_cmpn_nm' 	,cellattr:jsFormatterCell, align:'left' }
						,{ label: '사용처명 '			,name: 'cprt_cmpn_nm' 	,align:'left' }
						,{ label: '정산포인트'			,name: 'cm_pnt' 		,formatter:cm_pnt		,align:'right' }
						,{ label: '사용포인트 건수'		,name: 'pay_cnt'		,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
						,{ label: '사용포인트'			,name: 'pay_pnt'		,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
						,{ label: '취소포인트 건수'		,name: 'cncl_cnt' 		,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
						,{ label: '취소포인트'			,name: 'cncl_pnt' 		,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
						,{ label: '사용실패포인트 건수'	,name: 'pay_fail_cnt' 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
						,{ label: '사용실패포인트'		,name: 'pay_fail_pnt' 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
						,{ label: '취소실패포인트 건수'	,name: 'cncl_fail_cnt' 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
						,{ label: '취소실패포인트'		,name: 'cncl_fail_pnt' 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
						,{ label: '서비스명'			,name: 'service_nm' }
			];
			chkcellObj = {deal_dd:{cellId:undefined, chkval:undefined}, pg_cmpn_nm:{cellId:undefined, chkval:undefined}};
			PHJQg.excel('jqGridExcelDiv','${ViewRoot}/statistic/jgPntDealCmpnStatDExcel.do',postData,colNames,colModel,'포인트거래 사용처별 일별 통계');
		}
	}) 
});
function cm_pnt(cellvalue,options,rowobject) {
	var cmPnt = rowobject.pay_pnt - rowobject.cncl_pnt;
	if (cmPnt < 0) {
		cmPnt = cmPnt+'';
		return '-'+ PHUtil.setComma(cmPnt.substring(1));
	} else {
		return PHUtil.setComma(cmPnt);
	}
}

//jqGrid 셀병합 포맷
var chkcellObj = {deal_dd:{cellId:undefined, chkval:undefined}, pg_cmpn_nm:{cellId:undefined, chkval:undefined}};

<%--
grid 셀병합 체크 펑션
--%>
function jsFormatterCell(rowid, val, rowObject, cm, rdata){
  	var result = "";
  	var tmpVal = "";
  	
  	if( cm.name == "deal_dd" ) {
  		tmpVal = PHUtil.nvl(val);
  	} else if( cm.name == "pg_cmpn_nm" ) {
  		tmpVal = PHUtil.nvl(rdata.deal_dd) + PHUtil.nvl(val);
  	}
  	
  	if(chkcellObj[cm.name].chkval != tmpVal){ //check 값이랑 비교값이 다른 경우
      	var cellId = this.id + '_row_'+rowid+'-'+cm.name;
      	result = ' rowspan="1" id ="'+cellId+'" + name="cellRowspan"';
      	chkcellObj[cm.name] = {cellId:cellId, chkval:tmpVal};
  	}else{
      	result = 'style="display:none"  rowspanid="'+chkcellObj[cm.name].cellId+'"'; //같을 경우 display none 처리
  	}
  	return result;
}

//검색버튼 클릭
function search() {
	if (!validateDate()) {
		return;
	}
	var postData = {
		SRCH_IND 	: $("#sel_ind").val()
		,PG_CMPN_ID 	: $("#sel_pgId").val()
		,CPRT_CMPN_ID 	: $("#sel_cmId").val()
		,PH_SVC_CD      : $('#sel_svcCd').val()
       	,START_DATE 	: PHUtil.replaceAll($("#start_date").val(),"-","") 
       	,END_DATE 		: PHUtil.replaceAll($("#end_date").val(),"-","") 
	};
	PHJQg.reloadCallback('jqGrid','jqGridPager','${ViewRoot}/statistic/jgPntDealCmpnStatD.do',postData,function(o) {
		chkcellObj = {deal_dd:{cellId:undefined, chkval:undefined}, pg_cmpn_nm:{cellId:undefined, chkval:undefined}};
		$(o).jqGrid('footerData','set', {
			no:'Total',
			cm_pnt:PHJQg.colSum(o,'cm_pnt'),
			pay_cnt:PHJQg.colSum(o,'pay_cnt'),
			pay_pnt:PHJQg.colSum(o,'pay_pnt'),
			cncl_cnt:PHJQg.colSum(o,'cncl_cnt'),
			cncl_pnt:PHJQg.colSum(o,'cncl_pnt'),
			pay_fail_cnt:PHJQg.colSum(o,'pay_fail_cnt'),
			pay_fail_pnt:PHJQg.colSum(o,'pay_fail_pnt'),
			cncl_fail_cnt:PHJQg.colSum(o,'cncl_fail_cnt'),
			cncl_fail_pnt:PHJQg.colSum(o,'cncl_fail_pnt')
		});
	});
}
//기간 조건 체크
function validateDate() {
	var sDate = $("#start_date").val();
	var eDate = $("#end_date").val();
	if (!PHUtil.isEmpty(sDate) && !PHValid.date(PHUtil.replaceAll(sDate,'-',''))) {
		alert('시작일자가 유효한 날짜 형식이 아닙니다');
		$('#start_date').val('');
		$('#start_date').focus();
		return false;
	}
	if (!PHUtil.isEmpty(eDate) && !PHValid.date(PHUtil.replaceAll(eDate,'-',''))) {
		alert('종료일자가 유효한 날짜 형식이 아닙니다');
		$('#end_date').val('');
		$('#end_date').focus();
		return false;
	}
	var startDate = new Date(sDate);
	var endDate = new Date(eDate);
	if (startDate.getTime() > endDate.getTime()) {		
		alert('뒤의 날짜는 앞의 날짜 이전으로 검색할 수 없습니다.')
		return false;
	} else {
		var valDate = new Date(sDate);
		valDate.setMonth(valDate.getMonth()+1);
		if (endDate.getTime() > valDate.getTime()) {
			alert('검색 기간은 한달을 넘을 수 없습니다.');
			return false;
		}
	}
	return true;
}

</script>