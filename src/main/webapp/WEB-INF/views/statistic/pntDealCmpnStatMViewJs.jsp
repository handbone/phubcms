<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
 **********************************************************************************************
 * @desc : 포인트거래 사용처별 월별 통계 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/statistic/pntDealCmpnStatMViewJs.jsp
 * @author ojh
 * @since 2018.10.15
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.15    ojh        최초생성
 * 
 **********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
var GAP = 5;
var firstYear = new Date();
var today = new Date();
$(document).ready(function () { 
	var colNames = ['No','거래월','PG사명','사용처명','정산포인트','사용포인트<br>건수','사용포인트','취소포인트<br>건수','취소포인트','사용실패포인트<br>건수','사용실패포인트','취소실패포인트<br>건수','취소실패포인트','서비스명'];
	var colModel = [{ label: 'No', name: 'no', width: cWidth * 0.05, key:true }
		,{ label: '거래월'				,name: 'deal_mm'		,width: cWidth * 0.072 	,cellattr:jsFormatterCell }
		,{ label: 'PG사명'				,name: 'pg_cmpn_nm' 	,width: cWidth * 0.10 	,align:'left' }
		,{ label: '사용처명'			,name: 'cprt_cmpn_nm' 	,width: cWidth * 0.10 	,align:'left' }
		,{ label: '정산포인트'			,name: 'cm_pnt' 		,width: cWidth * 0.08 	,formatter:cm_pnt		,align:'right' }
		,{ label: '사용포인트 건수'		,name: 'pay_cnt'		,width: cWidth * 0.07 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
		,{ label: '사용포인트'			,name: 'pay_pnt'		,width: cWidth * 0.08 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
		,{ label: '취소포인트 건수'		,name: 'cncl_cnt' 		,width: cWidth * 0.07 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
		,{ label: '취소포인트'			,name: 'cncl_pnt' 		,width: cWidth * 0.08 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
		,{ label: '사용실패포인트 건수'	,name: 'pay_fail_cnt' 	,width: cWidth * 0.10 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
		,{ label: '사용실패포인트'		,name: 'pay_fail_pnt' 	,width: cWidth * 0.09 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
		,{ label: '취소실패포인트 건수'	,name: 'cncl_fail_cnt' 	,width: cWidth * 0.10 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
		,{ label: '취소실패포인트'		,name: 'cncl_fail_pnt' 	,width: cWidth * 0.09 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
		,{ label: '서비스명'			,name: 'service_nm' 	,width: cWidth * 0.10 }
	];
	PHJQg.loadTotalRow('jqGrid','jqGridPager',colNames,colModel);

	// 대사일자 셋팅
	firstYear.setYear(today.getFullYear() - GAP);
	for (var i=firstYear.getFullYear(); i<=today.getFullYear(); i++) {
		$('#start_year').append('<option value="'+ i +'">' +i+'년</option>');
		$('#end_year').append('<option value="'+ i +'">' +i+'년</option>');
	}
	$('#start_year option:last').prop('selected', true);
	$('#end_year option:last').prop('selected', true);
	$('#start_month').val(PHFnc.leadingZero(today.getMonth() + 1, 2));
	$('#end_month').val(PHFnc.leadingZero(today.getMonth() + 1, 2));
	
	//엑셀다운로드
	$("#btn_excel").on("click", function(){
		if (PHJQg.size('jqGrid') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
		if (!validateDate()) {
			return;
		}
		var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");	
		if(conf){		
			var postData = {
				SRCH_IND 	: $("#sel_ind").val()
				,PG_CMPN_ID 		: $("#sel_pgId").val()
				,CPRT_CMPN_ID 	: $("#sel_cmId").val()
				,PH_SVC_CD  : $('#sel_svcCd').val()
		        ,START_DATE : $('#start_year').val()+""+$('#start_month').val()
		        ,END_DATE 	: $('#end_year').val()+""+$('#end_month').val()
			};
			var colNames = ['No','거래월','PG사명','사용처명','정산포인트','사용포인트건수','사용포인트','취소포인트<br>건수','취소포인트','사용실패포인트건수','사용실패포인트','취소실패포인트건수','취소실패포인트','서비스명'];
			var colModel = [{ label: 'No', name: 'no', width: cWidth * 0.05, key:true }
					,{ label: '거래월'				,name: 'deal_mm'		,cellattr:jsFormatterCell }
					,{ label: 'PG사명'				,name: 'pg_cmpn_nm' 	,align:'left' }
					,{ label: '사용처명'			,name: 'cprt_cmpn_nm' 	,align:'left' }
					,{ label: '정산포인트'			,name: 'cm_pnt' 		,formatter:cm_pnt		,align:'right' }
					,{ label: '사용포인트 건수'		,name: 'pay_cnt'		,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
					,{ label: '사용포인트'			,name: 'pay_pnt'		,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
					,{ label: '취소포인트 건수'		,name: 'cncl_cnt' 		,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
					,{ label: '취소포인트'			,name: 'cncl_pnt' 		,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
					,{ label: '사용실패포인트 건수'	,name: 'pay_fail_cnt' 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
					,{ label: '사용실패포인트'		,name: 'pay_fail_pnt' 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
					,{ label: '취소실패포인트 건수'	,name: 'cncl_fail_cnt' 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
					,{ label: '취소실패포인트'		,name: 'cncl_fail_pnt' 	,formatter: 'integer'	,formatoptions:{thousandsSeparator:","}		,align:'right'}
					,{ label: '서비스명'			,name: 'service_nm' }
			];
			PHJQg.excel('jqGridExcelDiv','${ViewRoot}/statistic/jgPntDealCmpnStatMExcel.do',postData,colNames,colModel,'포인트거래 사용처별 월별 통계');
		}
	}) 
});
function cm_pnt(cellvalue,options,rowobject) {
	var cmPnt = rowobject.pay_pnt - rowobject.cncl_pnt;
	if (cmPnt < 0) {
		cmPnt = cmPnt+'';
		return '-'+ PHUtil.setComma(cmPnt.substring(1));
	} else {
		return PHUtil.setComma(cmPnt);
	}
}
//jqGrid 셀병합 포맷
var chkcell={cellId:undefined, chkval:undefined};
function jsFormatterCell(rowid, val, rowObject, cm, rdata){

  var result = "";     
  if(chkcell.chkval != val){ //check 값이랑 비교값이 다른 경우
      var cellId = this.id + '_row_'+rowid+'-'+cm.name;
      result = ' rowspan="1" id ="'+cellId+'" + name="cellRowspan"';
      chkcell = {cellId:cellId, chkval:val};
  }else{
      result = 'style="display:none"  rowspanid="'+chkcell.cellId+'"'; //같을 경우 display none 처리
  }
  return result;
}
//검색버튼 클릭
function search(){
	if (!validateDate()) {
		return;
	}
	var postData = {
		SRCH_IND 	: $("#sel_ind").val()
		,PG_CMPN_ID 		: $("#sel_pgId").val()
		,CPRT_CMPN_ID 	: $("#sel_cmId").val()
		,PH_SVC_CD  : $('#sel_svcCd').val()
        ,START_DATE : $('#start_year').val()+""+$('#start_month').val()
        ,END_DATE 	: $('#end_year').val()+""+$('#end_month').val()
	};
	chkcell={cellId:undefined, chkval:undefined};
	PHJQg.reloadCallback('jqGrid','jqGridPager','${ViewRoot}/statistic/jgPntDealCmpnStatM.do',postData,function(o) {
		$(o).jqGrid('footerData','set', {
			no:'Total',
			cm_pnt:PHJQg.colSum(o,'cm_pnt'),
			pay_cnt:PHJQg.colSum(o,'pay_cnt'),
			pay_pnt:PHJQg.colSum(o,'pay_pnt'),
			cncl_cnt:PHJQg.colSum(o,'cncl_cnt'),
			cncl_pnt:PHJQg.colSum(o,'cncl_pnt'),
			pay_fail_cnt:PHJQg.colSum(o,'pay_fail_cnt'),
			pay_fail_pnt:PHJQg.colSum(o,'pay_fail_pnt'),
			cncl_fail_cnt:PHJQg.colSum(o,'cncl_fail_cnt'),
			cncl_fail_pnt:PHJQg.colSum(o,'cncl_fail_pnt')
		});
	});
}
function validateDate() {
	var startDate = new Date($('#start_year').val()+ '-' + $('#start_month').val() + '-01');
	var endDate = new Date($('#end_year').val()+ '-' + $('#end_month').val() + '-01');
	if (startDate.getTime() > endDate.getTime()){		
		alert('뒤의 날짜는 앞의 날짜 이전으로 검색할 수 없습니다.');
		return false;
	} else {
		var valDate = new Date($('#start_year').val()+ '-' + $('#start_month').val() + '-01');
		valDate.setMonth(valDate.getMonth() + 11);
		if (endDate.getTime() > valDate.getTime()) {
			alert('검색 기간은 1년을 넘을 수 없습니다.');
			return false;
		}
	}
	return true;
}

</script>