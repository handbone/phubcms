<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--
 **********************************************************************************************
 * @desc : 클립포인트 대사 일별 통계 상세 팝업
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/statistic/clipPntStatDDtlPop.jsp
 * @author bmg
 * @since 2018.10.05
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.05    bmg        최초생성
 *
 **********************************************************************************************
clipPntStatDView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>
<div class="popup_content">
	<div class="popup_title">상세거래요청내역<a href="#!"><div class="top_close"></div></a></div>
	<table class="info_table" border="0" cellpadding="0" cellspacing="0">
	    <thead>
	        <tr>
	        	<th>거래일자</th>
	            <th>총대사건수</th>
	            <th>총사용포인트</th>
	            <th>대사불일치건수</th>
	        </tr>
	    </thead>
	    <tbody />
	</table>
	<table class="detail_table" border="0" cellpadding="0" cellspacing="0">
	    <thead>
	        <tr>
	        	<th>No</th>
	            <th>포인트별거래번호</th>
	            <th>카드사명</th>
	            <th>사용포인트</th>
	            <th>오류메시지</th>
	        </tr>
	    </thead>
	    <tbody />
	</table>
	<a href="#!"><div class="btn_close">닫기</div></a>
</div>
<div id="myPopup" class="popup">
</div>