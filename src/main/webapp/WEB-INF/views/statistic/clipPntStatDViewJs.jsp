<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="com.olleh.pHubCMS.common.components.Constant"%>
<%--
 **********************************************************************************************
 * @desc : 클립포인트 대사 일별 통계 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/statistic/clipPntStatDViewJs.jsp
 * @author bmg
 * @since 2018.10.05
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.05    bmg        최초생성
 * 
 **********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
$(document).ready(function () { 
	var colNames = ['No', '대사일자', '총대사건수', '총사용포인트', '대사불일치건수', '상세불일치내역'];
	var colModel = [{ label: 'No', name: 'no', width: cWidth * 0.10, key:true }
		,{ label: '대사일자', 		name: 'cmpr_dd', 		width: cWidth * 0.20 }
		,{ label: '총대사건수', 	name: 'ttl_deal_cnt' , 	width: cWidth * 0.20, formatter: 'integer', formatoptions:{thousandsSeparator:","}, align:'right' }
		,{ label: '총사용포인트', 	name: 'ttl_ans_pnt' , 	width: cWidth * 0.20, formatter: 'integer', formatoptions:{thousandsSeparator:","}, align:'right' }
		,{ label: '대사불일치건수', name: 'ttl_no_acr_cnt', width: cWidth * 0.20, formatter: 'integer', formatoptions:{thousandsSeparator:","}, align:'right' }
		,{ label: '상세불일치내역', name: 'c' , 			width: cWidth * 0.10, 	formatter:mbutton }
	];
	PHJQg.loadTotalRow('jqGrid','jqGridPager',colNames,colModel);
	$('#btn_excel').on('click', function() {
		if (PHJQg.size('jqGrid') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
		if (!validateDate()) {
			return;
		}
		var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");
		if (conf) {
			var postData = {
				START_DATE: $('#start_date').val(),
				END_DATE: $('#end_date').val()
			};
			var colNames = ['No', '대사일자', '총대사건수', '총사용포인트', '대사불일치건수'];
			var colModel = [{ label: 'No', name: 'no' }
				,{ label: '대사일자', 		name: 'cmpr_dd' }
				,{ label: '총대사건수', 	name: 'ttl_deal_cnt',	formatter: 'integer', formatoptions:{thousandsSeparator:","} }
				,{ label: '총사용포인트', 	name: 'ttl_ans_pnt' ,	formatter: 'integer', formatoptions:{thousandsSeparator:","} }
				,{ label: '대사불일치건수', name: 'ttl_no_acr_cnt', formatter: 'integer', formatoptions:{thousandsSeparator:","} }
			];
			PHJQg.excel('jqGridExcelDiv','${ViewRoot}/statistic/jgClipPntStatDayExcel.do',postData,colNames,colModel,'클립포인트대사일별통계');
		}
	});
	
	//검색일자 달력 셋팅
	var dt = new Date();
	dt.setDate(dt.getDate()-1);
	setDate(dt,dt);
});

//검색버튼 클릭
function search() {
	if (!validateDate()) {
		return;
	}
	var postData = {
		START_DATE: $('#start_date').val(),
		END_DATE: $('#end_date').val()
	};
	PHJQg.reloadCallback('jqGrid','jqGridPager','${ViewRoot}/statistic/jgClipPntStatDay.do',postData,function(o) {
		$(o).jqGrid('footerData','set', {
			no:'Total',
			ttl_deal_cnt:PHJQg.colSum(o,'ttl_deal_cnt'),
			ttl_ans_pnt:PHJQg.colSum(o,'ttl_ans_pnt'),
			ttl_no_acr_cnt:PHJQg.colSum(o,'ttl_no_acr_cnt')
		});
	});
}

//formatter 버튼 만들어 주기 클릭시 No번호 param 전달
function mbutton(cellvalue,options,rowobject){
	return '<input type="button" class="table_btn" onclick="detailPopClick('+rowobject.no+')" value="확인" />';
}
//기간 조건 체크
function validateDate() {
	var sDate = $("#start_date").val();
	var eDate = $("#end_date").val();
	if (!PHUtil.isEmpty(sDate) && !PHValid.date(PHUtil.replaceAll(sDate,'-',''))) {
		alert('시작일자가 유효한 날짜 형식이 아닙니다');
		$('#start_date').val('');
		$('#start_date').focus();
		return false;
	}
	if (!PHUtil.isEmpty(eDate) && !PHValid.date(PHUtil.replaceAll(eDate,'-',''))) {
		alert('종료일자가 유효한 날짜 형식이 아닙니다');
		$('#end_date').val('');
		$('#end_date').focus();
		return false;
	}
	var startDate = new Date(sDate);
	var endDate = new Date(eDate);
	if (startDate.getTime() > endDate.getTime()) {		
		alert('뒤의 날짜는 앞의 날짜 이전으로 검색할 수 없습니다.')
		return false;
	} else {
		var valDate = new Date(sDate);
		valDate.setMonth(valDate.getMonth()+1);
		if (endDate.getTime() > valDate.getTime()) {
			alert('검색 기간은 한달을 넘을 수 없습니다.');
			return false;
		}
	}
	return true;
}

var popInfo;
// 상세 불일치 내역 팝업 호출
function detailPopClick(data) {
	var row = $("#jqGrid").jqGrid('getRowData',data);
	popInfo = row;
	//누른 창 번호를 key로 상세 조회
	var params = { CMPR_DD : row.cmpr_dd.replace(/-/gi, '') };
	PHFnc.ajax("${ViewRoot}/statistic/ajaxClipPntStatDDtl.do",params,"POST","json",successAjaxGetDetail,null,true,true,false);
}

function successAjaxGetDetail(data, textStatus, jqXHR) {
	$('.info_table tbody').empty();
	$(
		'<tr>' +
			'<td>' + popInfo.cmpr_dd + '</td>' +
			'<td style="text-align:right;">' + PHUtil.setComma(popInfo.ttl_deal_cnt) + '</td>' +
			'<td style="text-align:right;">' + PHUtil.setComma(popInfo.ttl_ans_pnt) + '</td>' +
			'<td style="text-align:right;">' + PHUtil.setComma(popInfo.ttl_no_acr_cnt) + '</td>' +
		'</tr>' 
	).appendTo($('.info_table tbody'));
	popInfo = {};
	$('.detail_table tbody').empty();
	for (var i=0; i<data.rows.length; i++) {
		var item = data.rows[i];
		$(
			'<tr>' +
				'<td>'+ item.no +'</td>' +
				'<td>'+ item.pnt_tr_no +'</td>' +
				'<td style="text-align:left;">'+ item.prvdr_nm +'</td>' +
				'<td style="text-align:right;">'+ PHUtil.setComma(item.ans_pnt) +'</td>' +
				'<td style="text-align:left;">'+ item.err_msg +'</td>' +
			'</tr>'
		).appendTo($('.detail_table tbody'));
	}
	PHFnc.layerPopOpen(1);
}
</script>