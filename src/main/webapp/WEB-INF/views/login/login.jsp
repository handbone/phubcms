<%@  page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="botDetect" uri="https://captcha.com/java/jsp"%><%--
 **********************************************************************************************
 * @desc : 로그인
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/login/loginJs.jsp
 * @author lys
 * @since 2018.08.25
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.25   lys        최초생성
 * </pre>
 **********************************************************************************************
--%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<%-- global --%>
	<jsp:include page="/WEB-INF/views/include/globalVar.jsp" flush="false" />
	<%-- head --%>
	<jsp:include page="/WEB-INF/views/include/incHead.jsp" flush="false" />
	<script type="text/javascript" src="${ResRoot}/js/rsa.js"></script>
	<script type="text/javascript" src="${ResRoot}/js/jsbn.js"></script>
	<script type="text/javascript" src="${ResRoot}/js/prng4.js"></script>
	<script type="text/javascript" src="${ResRoot}/js/rng.js"></script>
</head>
<body class="bg_color">
	<!-- popup 시작-->
	<jsp:include page="./otpPop.jsp" flush="false" />
    <!-- popup 끝-->
    
	<!-- loader 시작-->
	<div class="loader" onclick="loader_close()">
	    <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
	</div>
	<!-- loader 끝-->
	<div id="wrap">
	    <!-- login 시작-->
	    <div class="login">
	        <ul>
	        	<form name="loginForm" id="loginForm" method="post">
	        		<input type="hidden" id="RSAModulus" value="${sessionScope.publickeymodulus}" />
	        		<input type="hidden" id="RSAExponent" value="${sessionScope.publickeyexponent}" />
	        		<input type="hidden" id="encPassword" name="encPassword" value="" />
	            	<div class="kt_logo"><img src="${ResRoot}/img/kt.png"></div>
	            	<li>포인트 허브 관리자 로그인</li>
	            	<li><input type="text" id="userId" name="userId" onfocus="this.value=''" value="아이디" maxlength="20" tabindex="1"></li>
	            	<li><input type="password" id="password" onfocus="this.value=''" alt="비밀번호" autocomplete="off" maxlength="20" tabindex="2"></li>	            
		            <li>
		            	<div style="width:324px; margin:0 auto;">
		            		<botDetect:captcha id="loginCaptcha" 
			                userInputID="captchaCode"
			                codeLength="5"
			                imageWidth="300"
			                imageStyle="GRAFFITI2" />
		            	</div>
		            </li>
	            	<li><input id="captchaCode" type="text" name="captchaCode" maxlength="5" tabindex="3" style="margin:20 auto;text-transform:uppercase;" /></li>
	            </form>
	            <li><button id="loginBtn">로그인</button></li>
	        </ul>
	    </div>
	    <!-- login 끝-->
	</div>

<!-- footer 시작-->
<jsp:include page="/WEB-INF/views/include/incFooter.jsp" flush="false" />
<!-- footer 끝-->
   
<%-- js --%>
<jsp:include page="./loginJs.jsp" flush="false" />
</body>
</html>