<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%><%--
 **********************************************************************************************
 * @desc		: OTP 입력 팝업
 * @FileName 	: /pHubCMS/src/main/webapp/WEB-INF/views/login/otpPop.jsp
 * @author 		: bmg
 * @since 		: 2019.04.16
 * @version 	: 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.04.016   bmg        최초생성
 **********************************************************************************************
login.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>
<div class="popup_content">
	<div class="popup_title">SMS OTP 인증<a href="#!"><div class="top_close"></div></a></div>
	<div style="margin:10px 0 10px 20px;">
		<div class="popup_info"><strong>※ 인증번호를 입력 후 확인 버튼을 누르십시오</strong></div>
		<div style="clear:both;"></div>
	</div>
	<table class="info_table" border="0" cellpadding="0" cellspacing="0" style="margin:0 0 0 20px;width:92%">
		<tbody>
			<tr>
				<th style="width:100px;">인증번호</th>
				<td><input type="text" id="inOtpNo" style="text-align:center;" onKeyDown="return onlyNum(event)" onKeyUp="return keyReplace(this)" maxlength="6" /></td>
				<th style="width:150px;color:blue;">남은시간 [ <span id="popTimer"></span> ]</th>
			</tr>
		</tbody>
	</table>
	<div class="btnalign">
		<ul>
		    <li><a href="#!"><div class="btn_save" onClick="reOtpSend();return false;">재전송</div></a></li>
		    <li><a href="#!"><div class="btn_save" onClick="otpInputCheck();return false;">확인</div></a></li>
		</ul>
	</div>
</div>