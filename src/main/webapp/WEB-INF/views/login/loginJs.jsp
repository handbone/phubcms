<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="com.captcha.botdetect.web.servlet.SimpleCaptcha" %><%--
 **********************************************************************************************
 * @desc : 로그인 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/login/loginJs.jsp
 * @author lys
 * @since 2018.08.25
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일              수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.25   lys        최초생성
 * </pre>
 **********************************************************************************************
--%>
<script type="text/javascript">
var RSA;
var INTERVAL;
var LIMIT = 5;
$(document).ready(function () {
	// login button event
	$("#loginBtn").on("click", function() {
		loginProc();
	});
	$('#captchaCode').keydown(function(e) {
		if ((e.keyCode ? e.keyCode : e.which) == 13) {
			loginProc();
		}
	});
	$('.popup_content').css('width', '600px');
	$('.top_close,.btn_cancel').click(function() {
		clearInterval(INTERVAL);
	});
	$('#inOtpNo').keydown(function(e) {
		if ((e.keyCode ? e.keyCode : e.which) == 13) {
			otpInputCheck();
		}
	});
	
	RSA = new RSAKey();
	RSA.setPublic($('#RSAModulus').val(), $('#RSAExponent').val());
});
// 유효성 체크
function validate() {
	var userId  = $('#userId').val();
	var pwdCtnt = $('#password').val();
	var captcha = $('#captchaCode').val().toUpperCase();
	
	if (userId === "" || userId == null || userId == "아이디") {
		PHFnc.alert("아이디를 입력 해 주세요.");
		$('#userId').focus();
		return false;
	} else if (pwdCtnt === "" || pwdCtnt == null || pwdCtnt == "비밀번호") {
		//PHFnc.alert("비밀번호를 입력 해 주세요.");
		alert("비밀번호를 입력 해 주세요.");
		$('#password').focus();
		return false;
	} else if (captcha === "" || captcha == null) {
		PHFnc.alert("보안문자를 입력해 주세요.");
		$('#captchaCode').focus();
		return false;
	} else {
		return true;
	}
}
// 로그인 수행
function loginProc() {
	// 유효성 체크
	if (!validate()) {
		return;
	}
	$(".loader").show();
	// RSA 암호화
	$("#encPassword").val(RSA.encrypt($('#password').val()));
	// Login Process
	$("#loginForm").ajaxForm({
		type     : 'POST',
		url      : '${ViewRoot}/login/loginProc.do',
		dataType : 'json',
		success  : function(data) {
			successResult(data);
		},
		error    : function(data, textStatus, jqXHR) {
   			$(".loader").hide();
   			PHFnc.ajaxError(null, jqXHR);
   			PHFnc.doAction("<c:url value='/login/login.do' />");
		}
	}).submit();
	
}
// OTP 팝업 타이머 생성
function otpTimer(callback) {
	var timer = LIMIT * 60;
	var minutes, seconds;
	clearInterval(INTERVAL);
	INTERVAL = setInterval(function () {
		minutes = parseInt(timer / 60 % 60, 10);
		seconds = parseInt(timer % 60, 10);
		minutes = minutes < 10 ? "0"+ minutes : minutes;
		seconds = seconds < 10 ? "0"+ seconds : seconds;
		$('#popTimer').html(minutes +":"+ seconds);
		if (--timer < 0) {
			timer = 0;
			clearInterval(INTERVAL);
			alert('OTP 입력시간이 초과되었습니다');
			PHFnc.doAction("/login/login.do");
		}
		callback();
	}, 1000);
}
// OTP 입력 값 체크
function otpInputCheck() {
	if ($('#inOtpNo').val() == '') {
		alert('OTP 번호를 입력하세요');
		$('#inOtpNo').focus();
		return;
	}
	if ($('#inOtpNo').val().length != 6) {
		alert('OTP 번호는 6자리입니다.');
		$('#inOtpNo').val('').focus();
		return;
	}
	$(".loader").show();
	PHFnc.layerPopClose(1);
	var params = {
		'userId': $('#userId').val()
		, 'encOtpNo': RSA.encrypt($('#inOtpNo').val())
	};
	$.ajax({
		type     : 'POST',
		data     : params,
		url      : "${ViewRoot}/login/loginProc.do",
		dataType : 'json',
		success  : function(data) {
			successResult(data);
		},
		error    : function(request, status, error) {
			$(".loader").hide();
   			PHFnc.ajaxError(null, jqXHR);
   			PHFnc.doAction("<c:url value='/login/login.do' />");
		}
	});
}
// 로그인 처리
function successResult(data) {
	switch (data.eStat) {
	case '0':
		// 로그인 성공
		PHFnc.doAction(data.afterLoginUrl);
		break;
	case '2':
		if (confirm(data.eMsg)) {
			loginProc();
		} else {
			// Confirm 취소 시 CAPTCHA 오류로 인한 Refresh
			PHFnc.doAction("<c:url value='/login/login.do' />");
		}
		break;
	case '3':
		otpTimer(function() {
			$(".loader").hide();
			PHFnc.layerPopOpen(1);
		});
		break;
	case '4':
		// 비밀번호 만료
		$(".loader").hide();
		PHFnc.alert(data.eMsg);
		//비밀번호 변경 페이지 팝업
		PHFnc.windowOpenGet('http://nsso.kt.com/ssokt/pwdTab.html',520,590,null);
		break;
	default:
		$(".loader").hide();
		if (data != null && data.eMsg != '') {
			PHFnc.alert(data.eMsg);	
		} else {
			PHFnc.ajaxError(null, null);
		}
		PHFnc.doAction("/login/login.do");
	}
}
// OTP 재전송
function reOtpSend() {
	PHFnc.ajax("${ViewRoot}/login/previousStepsLogin.do",null,"POST","json",
		function(data, textStatus, jqXHR) {
			otpTimer(function() {
				loginProc();
			});
		},
	null,false,true,false);
}

</script>