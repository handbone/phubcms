<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--
 **********************************************************************************************
 * @desc : 거래요청정보 상세 팝업
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/connHist/dealReqHistDtlPop.jsp
 * @author ojh
 * @since 2018.08.14
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.14    ojh        최초생성
 *
 **********************************************************************************************
dealReqHistView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>
<div class="popup_content">
        <div class="popup_title">상세거래요청내역<a href="#!"><div class="top_close"></div></a></div>
        <table class="info_table" border="0" cellpadding="0" cellspacing="0">
            <thead>
            <tr>
            	<th>포인트허브<br> 거래번호</th>
                <th>고객ID</th>
                <th>사용<br>포인트</th>
                <th>전환금액</th>
                <th>상품결제금액</th>
                <th>거래구분</th>
                <th>PG사명</th>
                <th>거래상태</th>
                <th>거래요청일시</th>
            </tr>
            </thead>
            <tbody>
            <tr id="bdTr">
            </tr>
            </tbody>
        </table>

        <table class="detail_table" border="0" cellpadding="0" cellspacing="0">
            <thead>
            <tr>
            	<th>NO</th>
                <th>포인트제공처</th>
                <th>포인트별거래번호</th>
                <th>사용포인트</th>
                <th>포인트전환율</th>
                <th>포인트전환금액</th>
                <th>거래구분</th>
                <th>성공여부</th>
                <th>오류코드</th>
                <th>오류메시지</th>
            </tr>
            </thead>
            <tbody id="tBody">
            </tbody>
        </table>
        <a href="#!"><div class="btn_close">닫기</div></a>
    </div>
    <div id="myPopup" class="popup">
    </div>