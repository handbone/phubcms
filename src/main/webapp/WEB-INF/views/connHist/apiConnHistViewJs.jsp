<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
 **********************************************************************************************
 * @desc : Api연동이력 조회 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/connHist/apiConnHistViewJs.jsp
 * @author ojh
 * @since 2018.08.21
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.21    ojh        최초생성
 * </pre>
 **********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
$(document).ready(function () {  
	var colNames = ['No', '로그No', '액션URL','발신처','수신처','포인트허브거래번호','응답코드','응답메세지','등록일시','송수신<br>데이터 확인','송신데이터','수신데이터'];
	var colModel = [{ label: 'No' ,name: 'no' ,width: cWidth * 0.05, key:true }
		,{ label: '로그No'				,name: 'log_no'		,width: cWidth * 0.11	}
		,{ label: '액션URL'				,name: 'actn_uri'	,width: cWidth * 0.22	,align:'left'}
		,{ label: '발신처'				,name: 'send_plc'	,width: cWidth * 0.07	,align:'left'}
		,{ label: '수신처'				,name: 'rcv_plc'	,width: cWidth * 0.09	,align:'left'}
		,{ label: '포인트허브거래번호'	,name: 'phub_tr_no'	,width: cWidth * 0.12	}
		,{ label: '응답코드'			,name: 'rply_cd'	,width: cWidth * 0.06	}
		,{ label: '응답메세지'			,name: 'rply_msg'	,width: cWidth * 0.08	}
		,{ label: '등록일시'			,name: 'rgst_dt'	,width: cWidth * 0.12	}
		,{ label: '송수신데이터 확인'	,name: ''			,width: cWidth * 0.07	,formatter:dbutton}
		,{ label: '송신데이터'			,name: 'req_data'	,hidden:true			}
		,{ label: '수신데이터'			,name: 'res_data'	,hidden:true			}
	];
	PHJQg.load('jqGrid','jqGridPager',colNames,colModel);
	
	var dt = new Date();
	setDate(dt, dt);
	
	//엑셀다운로드
	$("#btn_excel").on("click", function() {
		
		if (PHJQg.size('jqGrid') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
		if (!validateDate()) {
			return;
		}
		var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");	
		if(conf){		
			var postData = {
				ACTN_URI 		: $("#in_acUri").val()
				, ACTN_URI_EQ 	: $("#sel_actn_uri").val()
				, SEND_PLC 		: $("#sel_send").val()
				, RCV_PLC 		: $("#sel_rcv").val()
				, PHUB_TR_NO 	: $("#in_trNo").val()
				, RPLY_CD 		: $("#in_rpCd").val()
				, RPLY_MSG		: $('#sel_rpMsg').val()
				, RPLY_CD_EQ 	: $("#sel_rpCd_e").val()		
	        	, START_DATE 	: $("#start_date").val()
	        	, END_DATE 		: $("#end_date").val()	
			};
			var colNames = ['No', '로그No', '액션URL','발신처','수신처','포인트허브거래번호','응답코드','응답메세지','등록일시'];
			var colModel = [{ label: 'No' ,name: 'no' ,width: cWidth * 0.05}
				,{ label: '로그No'				,name: 'log_no' 	}
				,{ label: '액션URL'				,name: 'actn_uri'	}
				,{ label: '발신처'				,name: 'send_plc'	}
				,{ label: '수신처'				,name: 'rcv_plc'	}
				,{ label: '포인트허브거래번호'	,name: 'phub_tr_no'	}
				,{ label: '응답코드'			,name: 'rply_cd'	}
				,{ label: '응답메세지'			,name: 'rply_msg'	}
				,{ label: '등록일시'			,name: 'rgst_dt'	}
		    ];
			PHJQg.excel('jqGridExcelDiv','${ViewRoot}/connHist/jgApiConnHistExcel.do',postData,colNames,colModel,'API연동이력');
		}
	}) 
});	
//검색버튼 클릭
//파라미터 전달 후 jQgrid reload 
function search() {
	if (!validateDate()) {
		return;
	}
	var postData = {
		ACTN_URI 		: $("#in_acUri").val()
		, ACTN_URI_EQ 	: $("#sel_actn_uri").val()
		, SEND_PLC		: $("#sel_send").val()
		, RCV_PLC 		: $("#sel_rcv").val() 
		, PHUB_TR_NO 	: $("#in_trNo").val()
		, RPLY_CD 		: $("#in_rpCd").val()
		, RPLY_CD_EQ 	: $("#sel_rpCd_e").val()	
		, RPLY_MSG		: $('#sel_rpMsg').val()
       	, START_DATE 	: $("#start_date").val()
       	, END_DATE 		: $("#end_date").val()     
	};
	PHJQg.reload('jqGrid','jqGridPager','${ViewRoot}/connHist/jgApiConnHist.do',postData);
}

//조회기간 제약 // 제약 Max한달 
function validateDate(){
	var sDate = $("#start_date").val();
	var eDate = $("#end_date").val();
	if (!PHUtil.isEmpty(sDate) && !PHValid.date(PHUtil.replaceAll(sDate,'-',''))) {
		alert('시작일자가 유효한 날짜 형식이 아닙니다');
		$('#start_date').val('');
		$('#start_date').focus();
		return false;
	}
	if (!PHUtil.isEmpty(eDate) && !PHValid.date(PHUtil.replaceAll(eDate,'-',''))) {
		alert('종료일자가 유효한 날짜 형식이 아닙니다');
		$('#end_date').val('');
		$('#end_date').focus();
		return false;
	}
	var startDate = new Date(sDate);
	var endDate = new Date(eDate);
	if (startDate.getTime() > endDate.getTime()) {		
		alert('뒤의 날짜는 앞의 날짜 이전으로 검색할 수 없습니다.')
		return false;
	} else {
		var valDate = new Date(sDate);
		valDate.setMonth(valDate.getMonth()+1);
		if (endDate.getTime() > valDate.getTime()) {
			alert('검색 기간은 한달을 넘을 수 없습니다.');
			return false;
		}
	}
	return true;
}
//formatter 버튼 만들어 주기 클릭시 No전달
function dbutton(cellvalue,options,rowobject){
    return '<input type="button" class="table_btn" onclick="popClick('+rowobject.no+')" value="확인" />';
}
function popClick(no){
	var row = $("#jqGrid").jqGrid('getRowData',no);
	
	var hText = "<td>"+row.send_plc+"</td>";
	hText += "<td>"+row.rcv_plc+"</td>";
	hText += "<td>"+row.rply_cd+"</td>";
	hText += "<td>"+row.rply_msg+"</td>";
	hText += "<td>"+row.rgst_dt+"</td>";	
	$("#bdTr").html(hText);	

	var req_data = row.req_data.replace(/\",\"/g,"\",\n\"");
	var res_data = row.res_data.replace(/\",\"/g,"\",\n\"");
	$("#ta_send").val(req_data);	
	$("#ta_rcv").val(res_data);
	
	PHFnc.layerPopOpen(1);
}

</script>