<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
 **********************************************************************************************
 * @desc : 거래요청 내역 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/connHist/dealReqHistViewJs.jsp
 * @author ojh
 * @since 2018.08.13
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.13    ojh        최초생성
 * 2019.05.22    kimht      검색컬럼추가
 * </pre>
 **********************************************************************************************
  class="w100"
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
$(document).ready(function () {	
	var colNames = ['No', '포인트허브<br>거래번호', '고객ID','사용<br>포인트','전환<br>금액','상품<br>결제금액','상품명','거래구분','PG사명','사용처명'
	                ,'PG거래번호','PG제공<br>사용처ID','거래상태','오류코드','오류메세지','원포인트허브<br>거래번호','서비스명','거래요청일시','상세거래<br>요청내역','거래구분'];
	var colModel = [ { label: 'No', name: 'no', width: cWidth * 0.05, key:true }
		,{ label: '포인트허브거래번호'	,name: 'phub_tr_no' 	,width: cWidth * 0.12 	}
		,{ label: '고객ID'				,name: 'cust_id' 		,width: cWidth * 0.10 	}
		,{ label: '사용포인트'			,name: 'ttl_pnt'		,width: cWidth * 0.06	,formatter: caStr,align:'right' }
		,{ label: '전환금액'			,name: 'ttl_pnt_amt'	,width: cWidth * 0.06	,formatter: caStr,align:'right' }
		,{ label: '상품결제금액'		,name: 'ttl_pay_amt' 	,width: cWidth * 0.06	,formatter: caStr,align:'right' }
		,{ label: '상품명'				,name: 'goods_nm' 		,width: cWidth * 0.10	,align:'left' }
		,{ label: '거래구분'			,name: 'deal_ind_str'  	,width: cWidth * 0.08 	}
		,{ label: 'PG사명'				,name: 'pg_cmpn_id'  	,width: cWidth * 0.08	,align:'left' }
		,{ label: '사용처명'			,name: 'cprt_cmpn_id' 	,width: cWidth * 0.10	,align:'left' }
		,{ label: 'PG거래번호'			,name: 'pg_deal_no' 	,width: cWidth * 0.25 	}
		,{ label: 'PG제공사용처ID'		,name: 'pg_send_po_id' 	,width: cWidth * 0.08	,align:'left' }
		,{ label: '거래상태'			,name: 'deal_stat' 		,width: cWidth * 0.08 	}
		,{ label: '오류코드'			,name: 'err_cd' 		,width: cWidth * 0.06 	}
		,{ label: '오류메세지'			,name: 'err_msg' 		,width: cWidth * 0.10 	}
		,{ label: '원포인트허브거래번호',name: 'ori_phub_tr_no' ,width: cWidth * 0.13 	}
		,{ label: '서비스명'			,name: 'service_nm' 	,width: cWidth * 0.10 	}
		,{ label: '거래요청일시'		,name: 'deal_dt' 		,width: cWidth * 0.12 	}
		,{ label: '상세거래요청내역'	,name: '' 				,width: cWidth * 0.06	,formatter:dbutton }
		,{ label: '거래구분'			,name: 'deal_ind' 		,hidden:true			}
	];
	PHJQg.load('jqGrid','jqGridPager',colNames,colModel);
	
    var dt = new Date();
	setDate(dt, dt);
	
	$('#in_cuTn').keypress(function(e) {
    	var keycode = e.keyCode ? e.keyCode : e.which;
    	if ((keycode >= 48 && keycode <= 57) || (keycode >= 96 && keycode <= 105)) {
    		return true;
    	} else {
    		switch(keycode) {
    			case 8:		// BACKSPACE
    			case 9:		// TAB
    			case 45:	// -
    				return true;
    				break;
    			default:
    				e.preventDefault();
    		}
    	}
    }).keyup(function(e) {
    	var value = $(this).val().replace(/[^0-9-]/g,"");
    	$(this).val(value);
    });
	
	//엑셀다운로드
	$("#btn_excel").on("click", function() {
		if (PHJQg.size('jqGrid') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
		if (!validateDate()) {
			return;
		}
		var ctn = PHUtil.replaceAll($("#in_cuTn").val(),"-","");
		ctn = PHUtil.replaceAll(ctn,"/","");
		var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");	
		if(conf){		
			var postData = {
				DEAL_IND 	: $("#sel_ind").val()
				,PG_CMPN_ID : $("#sel_pgId").val()
				,PG_DEAL_NO : $('#in_pgDealNo').val()
				,PG_CPRT_ID : $('#sel_cprtId').val()				
				,DEAL_STAT 	: $("#sel_stat").val()
				,PHUB_TR_NO : $("#in_trNo").val()
				,CUST_ID 	: $("#in_cuId").val()
				,CUST_CTN 	: ctn
				,ERR_CD 	: $("#in_errCd").val()
				,ERR_CD_EQ 	: $("#sel_errCd_e").val()
				,PH_SVC_CD 	: $('#sel_svcCd').val()
				,ORI_PHUB_TR_NO	: $("#in_onePointHub").val()
				,START_DATE : $("#start_date").val()
				,END_DATE : $("#end_date").val()	
			};
			var colNames = ['No', '포인트허브거래번호', '고객ID','사용포인트','전환금액','상품결제금액','상품명','거래구분','PG사명','사용처명'
			                ,'PG거래번호','PG제공사용처ID','거래상태','오류코드','오류메세지','원포인트허브거래번호','서비스ID','거래요청일시','거래구분'];
			var colModel = [{ label: 'No', name: 'no', width: cWidth * 0.05 }
	       		,{ label: '포인트허브거래번호'	,name: 'phub_tr_no' 	}
	       		,{ label: '고객ID'				,name: 'cust_id' 		}
	       		,{ label: '사용포인트'			,name: 'ttl_pnt'		,formatter: caStr }
	       		,{ label: '전환금액'			,name: 'ttl_pnt_amt'	,formatter: caStr }
	       		,{ label: '상품결제금액'		,name: 'ttl_pay_amt' 	,formatter: caStr }
	       		,{ label: '상품명'				,name: 'goods_nm' 		}
	       		,{ label: '거래구분'			,name: 'deal_ind_str' 	}
	       		,{ label: 'PG사명'				,name: 'pg_cmpn_id' 	}
	       		,{ label: '사용처명'			,name: 'cprt_cmpn_id' 	}
	       		,{ label: 'PG거래번호'			,name: 'pg_deal_no' 	}
	       		,{ label: 'PG제공사용처ID'		,name: 'pg_send_po_id' 	}
	       		,{ label: '거래상태'			,name: 'deal_stat' 		}
	       		,{ label: '오류코드'			,name: 'err_cd' 		}
	       		,{ label: '오류메세지'			,name: 'err_msg' 		}
	       		,{ label: '원포인트허브거래번호',name: 'ori_phub_tr_no' }
	       		,{ label: '서비스명'			,name: 'service_nm' 	}
	       		,{ label: '거래요청일시'		,name: 'deal_dt' 		}
	       		,{ label: '거래구분'			,name: 'deal_ind'		,hidden:true	}
			];
			PHJQg.excel('jqGridExcelDiv','${ViewRoot}/connHist/jgDealReqHistExcel.do',postData,colNames,colModel,'거래요청내역');
		}
	}) 
});	
//검색버튼 클릭
//파라미터 전달 후 jQgrid reload 
function search() {
	if (!validateDate()) {
		return;
	}
	var ctn = PHUtil.replaceAll($("#in_cuTn").val(),"-","");
	ctn = PHUtil.replaceAll(ctn,"/","");
	var postData = {
		DEAL_IND 	: $("#sel_ind").val()
		,PG_CMPN_ID : $("#sel_pgId").val()
		,PG_DEAL_NO : $('#in_pgDealNo').val()
		,PG_CPRT_ID : $('#sel_cprtId').val()
		,DEAL_STAT 	: $("#sel_stat").val()
		,PHUB_TR_NO : $("#in_trNo").val()
		,CUST_ID 	: $("#in_cuId").val()
		,CUST_CTN 	: ctn
		,ERR_CD 	: $("#in_errCd").val()
		,ORI_PHUB_TR_NO	: $("#in_onePointHub").val()
		,ERR_CD_EQ 	: $("#sel_errCd_e").val()
		,PH_SVC_CD 	: $('#sel_svcCd').val()
		,START_DATE : $("#start_date").val()
		,END_DATE : $("#end_date").val()	
	};
	PHJQg.reload('jqGrid','jqGridPager','${ViewRoot}/connHist/jgDealReqHist.do',postData);		
}

//조회기간 제약 // 제약 Max한달 
function validateDate() {
	var sDate = $("#start_date").val();
	var eDate = $("#end_date").val();
	if (!PHUtil.isEmpty(sDate) && !PHValid.date(PHUtil.replaceAll(sDate,'-',''))) {
		alert('시작일자가 유효한 날짜 형식이 아닙니다');
		$('#start_date').val('');
		$('#start_date').focus();
		return false;
	}
	if (!PHUtil.isEmpty(eDate) && !PHValid.date(PHUtil.replaceAll(eDate,'-',''))) {
		alert('종료일자가 유효한 날짜 형식이 아닙니다');
		$('#end_date').val('');
		$('#end_date').focus();
		return false;
	}
	var startDate = new Date(sDate);
	var endDate = new Date(eDate);
	if (startDate.getTime() > endDate.getTime()) {		
		alert('뒤의 날짜는 앞의 날짜 이전으로 검색할 수 없습니다.')
		return false;
	} else {
		var valDate = new Date(sDate);
		valDate.setMonth(valDate.getMonth()+1);
		if (endDate.getTime() > valDate.getTime()) {
			alert('검색 기간은 한달을 넘을 수 없습니다.');
			return false;
		}
	}
	return true;
}

function caStr(cellvalue,options,rowobject){
	cellvalue = cellvalue != null ? cellvalue+'' : cellvalue;
	if(rowobject.deal_ind != 'PA'){
		if (Number(cellvalue)) {
			return "-"+PHUtil.setComma(cellvalue);
		}
		else {
			return "";
		}
	} else{
		return PHUtil.nvl(PHUtil.setComma(cellvalue),'');
	}
}

//formatter 버튼 만들어 주기 클릭시 No전달
function dbutton(cellvalue,options,rowobject){
    return '<input type="button" class="table_btn" onclick="popClick('+rowobject.no+')" value="확인" />';
}
//클릭시 상세 정보 창에 data 셋팅
function popClick(data){
	//요청 내역 셋팅
	var row = $("#jqGrid").jqGrid('getRowData',data);	
	var hText = "<td>"+row.phub_tr_no+"</td>";
	hText += "<td>"+row.cust_id+"</td>";
	hText += "<td style='text-align:right'>"+row.ttl_pnt+"</td>";
	hText += "<td style='text-align:right'>"+row.ttl_pnt_amt+"</td>";
	hText += "<td style='text-align:right'>"+row.ttl_pay_amt+"</td>";
	hText += "<td>"+row.deal_ind_str+"</td>";
	hText += "<td>"+row.pg_cmpn_id+"</td>";
	hText += "<td>"+row.deal_stat+"</td>";
	hText += "<td>"+row.deal_dt+"</td>";	
	$("#bdTr").html(hText);	
	
	//제공처별 요청 내역 aJax호출하여 가져오기
	var params = {
		PHUB_TR_NO : row.phub_tr_no
	};
	PHFnc.ajax("${ViewRoot}/connHist/ajaxDealReqDtl.do",params,"POST","json",successAjax,false,true,true,false);
	//팝업창 오픈
	PHFnc.layerPopOpen(1);
}
function successAjax(data, textStatus, jqXHR){
	$('#tBody').empty();
	if (data.rows == null) {
		return;
	}
	//Row 개수에 따라 td태그 생성하여 Html 변환
	var hText = "";		
	var cText = "";
	for(var i=0; i<data.rows.length; i++){
		if(data.rows[i].deal_ind =="CA"){
			cText ="-";
		} else{
			cText = "";
		}
		hText += "<tr>";
		hText += "<td>"+data.rows[i].no+"</td>";
		hText += "<td style='text-align:left'>"+data.rows[i].pnt_nm+"</td>";
		hText += "<td>"+data.rows[i].pnt_tr_no+"</td>";
		hText += "<td style='text-align:right'>"+cText+PHUtil.setComma(data.rows[i].ans_pnt)+"</td>";
		hText += "<td>"+data.rows[i].pnt_exch_rate+"</td>";
		hText += "<td style='text-align:right'>"+cText+PHUtil.setComma(data.rows[i].ans_pnt_amt)+"</td>";
		hText += "<td>"+data.rows[i].deal_ind_str+"</td>";		
		hText += "<td>"+data.rows[i].sucs_yn+"</td>";
		hText += "<td>"+data.rows[i].err_cd+"</td>";
		hText += "<td>"+data.rows[i].err_msg+"</td>";
		hText += "</tr>";
	}
	$("#tBody").html(hText);
}
</script>