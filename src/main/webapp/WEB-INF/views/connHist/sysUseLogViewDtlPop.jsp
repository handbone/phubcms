<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--
 **********************************************************************************************
 * @desc : 시스템 사용로그 상세 팝업(파라미터 확인)
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/connHist/sysUseLogViewDtlPop.jsp
 * @author ojh
 * @since 2018.09.20
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.20    ojh        최초생성
 *
 **********************************************************************************************
sysUseLogView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>

    <!--송수신데이터확인 시작 -->
    <div class="popup_content">

        <div class="popup_title">파라미터 확인<a href="#!"><div class="top_close"></div></a></div>
        <table class="info_table" border="0" cellpadding="0" cellspacing="0">
            <tbody>
            <tr>
                <th>메뉴명</th>
                <th>접속IP</th>
                <th>등록자ID</th>
                <th>등록일시</th>
            </tr>
            <tr id="bdTr">
            </tr>
            </tbody>
        </table>

        <table class="detail_table" border="0" cellpadding="0" cellspacing="0">
            <tbody>
            <tr>
                <th>구분</th>
                <th>데이터</th>
            </tr>
            <tr>
                <td style="width:100px">&nbsp;파라미터 </td>
                <td><textarea name="textarea" rows="9" id="ta_param">
                    </textarea>
                </td>
            </tr>
            </tbody>
        </table>
        <a href="#!"><div class="btn_close">닫기</div></a>
        <!-- 상세거래요청내역 끝-->
    </div>
    </div>
    <div id="myPopup" class="popup"></div>
    <!-- popup 끝-->