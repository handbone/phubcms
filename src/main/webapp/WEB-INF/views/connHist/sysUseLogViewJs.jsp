<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
 **********************************************************************************************
 * @desc : 시스템 사용로그 조회 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/connHist/sysUseLogViewJs.jsp
 * @author ojh
 * @since 2018.08.22
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.22    ojh        최초생성
 * </pre>
 **********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
$(document).ready(function () {  		
	var colNames = ['No', '로그No', 'URI','액션ID','메뉴명','파라미터','접속IP','등록자ID','등록일시','파라미터<br>확인'];
	var colModel = [{ label: 'No' ,name: 'no' ,width: cWidth * 0.05 ,key:true }
		,{ label: '로그No'			,name: 'log_no'			,width: cWidth * 0.12	}
		,{ label: 'URI'				,name: 'uri'			,width: cWidth * 0.15	,align:'left'}
		,{ label: '액션ID'			,name: 'actn_id'		,width: cWidth * 0.08	}
		,{ label: '메뉴명'			,name: 'menu_nm'		,width: cWidth * 0.15	,align:'left'}
		,{ label: '파라미터'		,name: 'prmt'			,width: cWidth * 0.10	,align:'left'}
		,{ label: '접속IP'			,name: 'jnng_ip' 		,width: cWidth * 0.10	,align:'left'}
		,{ label: '등록자ID'		,name: 'rgst_user_id'	,width: cWidth * 0.06	}
		,{ label: '등록일시'		,name: 'rgst_dt'		,width: cWidth * 0.12	}
		,{ label: '파라미터확인'	,name: ''				,width: cWidth * 0.07	,formatter:dbutton}
	];	
	PHJQg.load('jqGrid','jqGridPager',colNames,colModel);
    
    var dt = new Date();
	setDate(dt, dt);
	
	//엑셀다운로드
	$("#btn_excel").on("click", function() {
		if (PHJQg.size('jqGrid') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
		if (!validateDate()) {
			return;
		}
		var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");	
		if(conf){		
			var postData = {
				URI : $("#in_uri").val(),
				MENU_NM : $("#in_menu").val(),
				JNNG_IP : $("#in_ip").val(),
				RGST_USER_ID : $("#in_id").val(),
				START_DATE : $("#start_date").val(),
	        	END_DATE : $("#end_date").val()	
			};
			var colNames = ['No', '로그No', 'URI','액션ID','메뉴명','파라미터','접속IP','등록자ID','등록일시'];
			var colModel = [{ label: 'No' ,name: 'no' }
				,{ label: '로그No'		,name: 'log_no'			}
				,{ label: 'URI'			,name: 'uri'			}
				,{ label: '액션ID'		,name: 'actn_id'		}
				,{ label: '메뉴명'		,name: 'menu_nm'		}
				,{ label: '파라미터'	,name: 'prmt'			}
				,{ label: '접속IP'		,name: 'jnng_ip'		}
				,{ label: '등록자ID'	,name: 'rgst_user_id'	}
				,{ label: '등록일시'	,name: 'rgst_dt'		}
			];	
			PHJQg.excel('jqGridExcelDiv','${ViewRoot}/connHist/jgSysUseLogExcel.do',postData,colNames,colModel,'시스템 사용로그');
		}
	}) 
});	
//formatter 버튼 만들어 주기 클릭시 No전달
function dbutton(cellvalue,options,rowobject){
    return '<input type="button" class="table_btn" onclick="popClick('+rowobject.no+')" value="확인" />';
}
function popClick(no){
	var row = $("#jqGrid").jqGrid('getRowData',no);
	
	var hText = "<td>"+row.menu_nm+"</td>";
	hText += "<td>"+row.jnng_ip+"</td>";
	hText += "<td>"+row.rgst_user_id+"</td>";
	hText += "<td>"+row.rgst_dt+"</td>";
	$("#bdTr").html(hText);	
	
	var param = row.prmt.replace(/\"],\"/g,"\",]\n\"");
	$("#ta_param").val(param);	
	

	PHFnc.layerPopOpen(1);
}

//검색버튼 클릭
//파라미터 전달 후 jQgrid reload 
function search() {
	if (!validateDate()) {
		return;
	}
	var postData = {
		URI : $("#in_uri").val(),
		MENU_NM : $("#in_menu").val(),
		JNNG_IP : $("#in_ip").val(),
		RGST_USER_ID : $("#in_id").val(),
		START_DATE : $("#start_date").val(),
       	END_DATE : $("#end_date").val()	
	};
	PHJQg.reload('jqGrid','jqGridPager','${ViewRoot}/connHist/jgSysUseLog.do',postData);
}

//조회기간 제약 // 제약 Max한달 
function validateDate(){
	var sDate = $("#start_date").val();
	var eDate = $("#end_date").val();
	if (!PHUtil.isEmpty(sDate) && !PHValid.date(PHUtil.replaceAll(sDate,'-',''))) {
		alert('시작일자가 유효한 날짜 형식이 아닙니다');
		$('#start_date').val('');
		$('#start_date').focus();
		return false;
	}
	if (!PHUtil.isEmpty(eDate) && !PHValid.date(PHUtil.replaceAll(eDate,'-',''))) {
		alert('종료일자가 유효한 날짜 형식이 아닙니다');
		$('#end_date').val('');
		$('#end_date').focus();
		return false;
	}
	var startDate = new Date(sDate);
	var endDate = new Date(eDate);
	if (startDate.getTime() > endDate.getTime()) {		
		alert('뒤의 날짜는 앞의 날짜 이전으로 검색할 수 없습니다.')
		return false;
	} else {
		var valDate = new Date(sDate);
		valDate.setMonth(valDate.getMonth()+1);
		if (endDate.getTime() > valDate.getTime()) {
			alert('검색 기간은 한달을 넘을 수 없습니다.');
			return false;
		}
	}
	return true;
}

</script>