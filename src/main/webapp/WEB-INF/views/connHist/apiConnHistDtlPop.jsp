<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--
 **********************************************************************************************
 * @desc : 연동이력조회 상세 팝업(송수신데이터 확인)
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/connHist/apiConnHistDtlPop.jsp
 * @author ojh
 * @since 2018.08.29
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.29    ojh        최초생성
 *
 **********************************************************************************************
apiConnHistView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>

    <!--송수신데이터확인 시작 -->
    <div class="popup_content">

        <div class="popup_title">송수신데이터 확인<a href="#!"><div class="top_close"></div></a></div>
        <table class="info_table" border="0" cellpadding="0" cellspacing="0">
            <tbody>
            <tr>
                <th>발신처</th>
                <th>수신처</th>
                <th>응답코드</th>
                <th>HTTP응답코드</th>
                <th>등록일시</th>
            </tr>
            <tr id="bdTr">
            </tr>
            </tbody>
        </table>

        <table class="detail_table" border="0" cellpadding="0" cellspacing="0">
            <tbody>
            <tr>
                <th>구분</th>
                <th>데이터</th>
            </tr>
            <tr>
                <td>&nbsp;송신 </td>
                <td><textarea name="textarea" rows="9" id="ta_send">
                    </textarea>
                </td>
            </tr>
            <tr>
                <td>&nbsp;수신 </td>
                <td>
                    <textarea name="textarea" rows="9" id="ta_rcv">
                    </textarea>
                </td>
            </tr>

            </tbody>
        </table>
        <a href="#!"><div class="btn_close">닫기</div></a>
        <!-- 상세거래요청내역 끝-->
    </div>
    </div>
    <div id="myPopup" class="popup"></div>
    <!-- popup 끝-->