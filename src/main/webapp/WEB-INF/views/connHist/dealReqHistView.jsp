<%@  page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
 **********************************************************************************************
 * @desc : 거래요청 내역
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/connHist/dealReqHistView.jsp
 * @author ojh
 * @since 2018.08.10
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.10    ojh        최초생성
 * 2019.05.27    kimht        include 수정
 **********************************************************************************************
--%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<%-- global --%>
	<jsp:include page="/WEB-INF/views/include/globalVar.jsp" flush="false" />
	<%-- head --%>
	<jsp:include page="/WEB-INF/views/include/incHead.jsp" flush="false" />
</head>
<body>
<!-- loader 시작-->
<div class="loader" onclick="loader_close()">
    <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
</div>
<!-- loader 끝-->
<div id="wrap">	
<header>
	<%-- header --%>
	<jsp:include page="/WEB-INF/views/include/incHeader.jsp" flush="false" />
</header>

    <!-- 레프트 메뉴 해당페이지 표시 -->
    <style>.${sessionScope.USER_MENU.menu_id}{ color: #fff !important; background: #3f4759 !important; border-left: 3px solid #fff; cursor: hand;} </style>
    <!-- 레프트 메뉴 해당페이지 표시 끝 -->
    
    <!-- popup 시작-->
    <jsp:include page="./dealReqHistDtlPop.jsp" flush="false" />    
    <!-- popup 끝-->

<!-- dash_board 시작-->
<section id="dash_board">
    <!--side_menu 시작-->
	<jsp:include page="/WEB-INF/views/include/incMenu.jsp" flush="false" />
    <!--side_menu 끝-->

    <!--contents 시작-->
    <div class="contents mCustomScrollbar">
        <div class="page_title"><div class="page_name">${sessionScope.USER_MENU.menu_nm}</div><div class="page_location"><i class="fas fa-home"></i>${sessionScope.USER_MENU.mnu_path}</div></div>
        <div class="subject">
            <div class="page_search">
            	<table class="search_tb" border=01" cellpadding="0" cellspacing="0">
			    	<tr>
			        	<td class="search_item">
			            	<ul>
			                    <li>거래요청일자 <input type="text" name="name" id="start_date" readonly> ~ <input type="text" name="name"  id="end_date" readonly></li>
								<li style="width:30.92px;" />
			                    <li>거래구분
			                        <select id="sel_ind">
			                            <option value="all">전체</option>                            
			                           	<c:forEach items="${DEAL_IND}" var="code">
			                           		<option value="${code.dtlCd}">${code.dtlCdNm}</option>
			                           	</c:forEach>                           	
			                        </select>
			                    </li>
			                    <li style="width:4px;" /> 
			                    
			                    <li>거래상태
			                        <select id="sel_stat">
			                            <option value="all">전체</option>
			                            <c:forEach items="${DEAL_REQ_STAT}" var="code">
			                           		<option value="${code.dtlCd}">${code.dtlCdNm}</option>
			                           	</c:forEach>
			                        </select>
			                    </li>
			                     <li style="width:40px;" /> 			                    
			                    <li>오류코드
                                	<select id="sel_errCd_e">
                                		<option value="N">!=</option>
                                        <option value="E">=</option>
                                    </select> 
                                	<input type="text" name="name" id="in_errCd" maxlength="20">
                                </li>
							</ul>
							<ul>
			                    <li>포인트허브거래번호 <input type="text" name="name" id="in_trNo" style="width:100px" maxlength="30" autocomplete="off"></li>			                    
			                    <li>PG사ID
			                        <select id="sel_pgId">
			                        <option value="all">전체</option>
			                        <c:forEach items="${PG_CMPN}" var="code">
			                           	<option value="${code.pg_cmpn_id}">${code.pg_cmpn_nm}(${code.pg_cmpn_id})</option>
			                        </c:forEach>
			                        </select>
			                    </li>
			                    <li>PG거래번호 <input type="text" name="name" id="in_pgDealNo" style="width:100px" maxlength="40" autocomplete="off"></li>
			                    <li>가맹점별
			                        <select id="sel_cprtId">
			                            <option value="all">전체</option>
				                        <c:forEach items="${USE_CMPN}" var="code">
				                           	<option value="${code.cprt_cmpn_id}">${code.cprt_cmpn_nm}</option>
				                        </c:forEach>
			                       </select>
			                    </li>			                    
			                    <li>고객 ID <input type="text" name="name" id="in_cuId" style="width:100px" maxlength="20" autocomplete="off"></li>
			                </ul>
			                <ul>
			                	<li>핸드폰번호 <input type="text" name="name" id="in_cuTn" style="width:100px" maxlength="20" autocomplete="off"></li>
			                    <li style="width:33px;" /> 	
			                	<li>서비스명
			                		<select id="sel_svcCd">
			                			<option value="all">전체</option>
			                			<c:forEach items="${PH_SVC_CD}" var="code">
			                				<option value="${code.dtlCd}">${code.dtlCdNm}</option>
			                			</c:forEach>
			                		</select>
			                	</li>
			                    <li style="width:26px;" /> 	
			                	<li>원포인트허브 거래번호 <input type="text" name="name" id="in_onePointHub" style="width:100px" maxlength="20" autocomplete="off"></li>
			                </ul>
                        </td>
                        <td class="btn_search">
                            <button type="button" onClick="search()">조회</button>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="page_contents">
                <div class="btn_excel"><button id="btn_excel">엑셀다운</button></div>
                <div id="content">
                	<!--  jQGrid 시작-->
                    <table id="jqGrid"></table>
                    <div id="jqGridPager"></div>
                    
	                <div id="jqGridExcelDiv" style="display:none">
	                	<table id="jqGridExcel" ></table>
	                </div>
                </div>
            </div>
        </div>
        <!-- 스크롤 때문에 여백 있어야함 -->
        <br>
        <br>
        <br>
        <br>
        <br>
        </div>
    <!--contents 끝-->
</section>
<!-- dash_board 끝--> 


    <!-- footer 시작-->
    <jsp:include page="/WEB-INF/views/include/incFooter.jsp" flush="false" />
    <section class="footer">	 
    </section>
    <!-- footer 끝-->
    
    <%-- js --%>
	<jsp:include page="./dealReqHistViewJs.jsp" flush="false" />
	
</body>
</html>