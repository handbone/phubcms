<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
 **********************************************************************************************
 * @desc : PG사 수수료 정산조회 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/cmpr/pgCmprViewJs.jsp
 * @author ojh
 * @since 2018.10.17
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.17    ojh        최초생성
 * </pre>
 **********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
$(document).ready(function () {  
		
	var colNames = ['No', 'PG사명','포인트사용처<br>(가맹점)','포인트허브거래번호','사용포인트','사용<br>/취소','포인트사용처<br>수수료율<br>(VAT포함)','포인트사용처<br>수수료금액<br>(VAT포함)'
	               ,'PG수수료율<br>(VAT포함)','PG수수료금액<br>(VAT포함)','거래일시','서비스명'];
	var colModel = [{ label: 'No' ,name: 'no' ,width: cWidth * 0.04 ,key:true }
		,{ label: 'PG사명'					,name: 'pg_cmpn_nm'		,width: cWidth * 0.10	,align:'left'	}
		,{ label: '포인트사용처'			,name: 'cprt_cmpn_nm'	,width: cWidth * 0.10	,align:'left'	}
		,{ label: '포인트허브거래번호'		,name: 'phub_tr_no'		,width: cWidth * 0.12	}
		,{ label: '사용포인트'				,name: 'ttl_pnt_amt' 	,width: cWidth * 0.09	,formatter:setComma,align:'right'	}
		,{ label: '사용/취소'				,name: 'deal_ind'		,width: cWidth * 0.07	}
		,{ label: '포인트사용처수수료율'	,name: 'kt_cmsn_rate'	,width: cWidth * 0.08	,formatter:cmsn,align:'right'	}
		,{ label: '포인트사용처수수료금액'	,name: ''  				,width: cWidth * 0.08	,formatter:ktcmsn,align:'right'	}
		,{ label: 'PG수수료율'				,name: 'pg_cmsn_rate'	,width: cWidth * 0.08	,formatter:cmsn,align:'right'	}
		,{ label: 'PG수수료금액'			,name: ''  				,width: cWidth * 0.08	,formatter:pgcmsn,align:'right'	}
		,{ label: '거래일시'				,name: 'rgst_dt'		,width: cWidth * 0.12	}
		,{ label: '서비스명'				,name: 'service_nm'		,width: cWidth * 0.10	}
	];
	PHJQg.load('jqGrid','jqGridPager',colNames,colModel);
	
	//datePicker 셋팅
	//하루 이전으로 달력 셋팅
	var dt = new Date();
	dt.setDate(dt.getDate()-1);
	setDate(dt,dt);
	
	//엑셀다운로드
	$("#btn_excel").on("click", function() {
		if (PHJQg.size('jqGrid') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
		if (!validateDate()) {
			return;
		}
		var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");	
		if(conf){		
			var postData = {
				PG_CMPN_ID 	: $("#sel_pgId").val()
				,DEAL_IND 	: $("#sel_ind").val()
				,PH_SVC_CD 	: $('#sel_svcCd').val()
	        	,START_DATE : $("#start_date").val()
	        	,END_DATE 	: $("#end_date").val()	
			};
			var colNames = ['No', 'PG사명','포인트사용처(가맹점)','포인트허브거래번호','사용포인트','사용/취소','포인트사용처수수료율(VAT포함)','포인트사용처수수료금액VAT포함)'
				               ,'PG수수료율(VAT포함)','PG수수료금액(VAT포함)','거래일시','서비스명'];
			var colModel = [{ label: 'No' ,name: 'no' ,width: cWidth * 0.05 ,key:true }
	      		,{ label: 'PG사명'					,name: 'pg_cmpn_nm'		}
	      		,{ label: '포인트사용처'			,name: 'cprt_cmpn_nm'	}
	      		,{ label: '포인트허브거래번호'		,name: 'phub_tr_no'		}
	      		,{ label: '사용포인트'				,name: 'ttl_pnt_amt'	,formatter:setComma		}
	      		,{ label: '사용/취소'				,name: 'deal_ind'		}
	      		,{ label: '포인트사용처수수료율'	,name: 'kt_cmsn_rate'	,formatter:cmsn			}
	      		,{ label: '포인트사용처수수료금액'	,name: ''				,formatter:ktcmsn		}
	      		,{ label: 'PG수수료율'				,name: 'pg_cmsn_rate'	,formatter:cmsn			}
	      		,{ label: 'PG수수료금액'			,name: ''  				,formatter:pgcmsn		}
	      		,{ label: '거래일시'				,name: 'rgst_dt'		}
	      		,{ label: '서비스명'				,name: 'service_nm'		}
			];
			PHJQg.excel('jqGridExcelDiv','${ViewRoot}/cmpr/jgPgCmprExcel.do',postData,colNames,colModel,'PG사 수수료정산');
		}
	});
});	

function ktcmsn(cellvalue,options,rowobject){	
	var amount = 0;
	if (rowobject.ttl_pnt != null) {
		amount = Math.round(rowobject.ttl_pnt * rowobject.kt_cmsn_rate / 100);
	}
	return PHUtil.setComma(amount);	
}
function pgcmsn(cellvalue,options,rowobject){
	var amount = 0;
	if (rowobject.ttl_pnt != null) {
		amount = Math.round( rowobject.ttl_pnt * rowobject.pg_cmsn_rate / 100);
	}
	return PHUtil.setComma(amount);	
}
function cmsn(cellvalue,options,rowobject){
	return cellvalue + " %";
}
//금액으로 표시, 콤마 넣기
function setComma(cellvalue,options,rowobject){	
	return PHUtil.setComma(cellvalue);
}

//검색버튼 클릭
//파라미터 전달 후 jQgrid reload 
function search() {
	if (!validateDate()) {
		return;
	}
	var postData = {
		PG_CMPN_ID 	: $("#sel_pgId").val()
		,DEAL_IND 	: $("#sel_ind").val()
		,PH_SVC_CD 	: $('#sel_svcCd').val()
       	,START_DATE : $("#start_date").val()
       	,END_DATE 	: $("#end_date").val()	
	};
	PHJQg.reload('jqGrid','jqGridPager','${ViewRoot}/cmpr/jgPgCmpr.do',postData);
}

//조회기간 제약 // 제약 Max한달 
function validateDate() {
	var sDate = $("#start_date").val();
	var eDate = $("#end_date").val();
	if (!PHUtil.isEmpty(sDate) && !PHValid.date(PHUtil.replaceAll(sDate,'-',''))) {
		alert('시작일자가 유효한 날짜 형식이 아닙니다');
		$('#start_date').val('');
		$('#start_date').focus();
		return false;
	}
	if (!PHUtil.isEmpty(eDate) && !PHValid.date(PHUtil.replaceAll(eDate,'-',''))) {
		alert('종료일자가 유효한 날짜 형식이 아닙니다');
		$('#end_date').val('');
		$('#end_date').focus();
		return false;
	}
	var startDate = new Date(sDate);
	var endDate = new Date(eDate);
	if (startDate.getTime() > endDate.getTime()) {		
		alert('뒤의 날짜는 앞의 날짜 이전으로 검색할 수 없습니다.')
		return false;
	} else {
		var valDate = new Date(sDate);
		valDate.setMonth(valDate.getMonth()+1);
		if (endDate.getTime() > valDate.getTime()) {
			alert('검색 기간은 한달을 넘을 수 없습니다.');
			return false;
		}
	}
	return true;
}
</script>