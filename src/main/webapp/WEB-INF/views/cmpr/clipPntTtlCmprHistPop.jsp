<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--
 **********************************************************************************************
 * @desc : 클립포인트 전체 대사 내역 팝업
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/cmpr/clipPntTtlCmprHistPop.jsp
 * @author bmg
 * @since 2018.10.18
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.18    bmg        최초생성
 *
 **********************************************************************************************
clipPntCmprHistView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>    
<!-- 전체 대사 내역 시작 -->
<style type="text/css">
.popup_content .btn_excel button { 
	color: #fff; 
	padding: 5px 14px; 
	margin-top: 10px; 
	font-size: 15px; 
	font-weight: 400; 
	letter-spacing: -1px; 
	background: green; 
	border: none; 
	text-align: center;  
}
.popup_content #content_p table td#jqGridPager1_center { border: none !important; width: 200px !important; }
.popup_content #content_p table td#jqGridPager1_center table { border: none !important; }
.popup_content #content_p table td#jqGridPager1_center table tr td { border: none !important; }
.popup_content #content_p table td#jqGridPager1_left { border: none !important; border-right: none; }
.popup_content #content_p table td#jqGridPager1_left tr td { border: none !important; }
.popup_content #content_p table td#jqGridPager1_right { border: none !important; }
.popup_content #content_p table td#jqGridPager1_right tr td { border: none !important; }
.popup_content #content_p table td#prev_jqGridPager1 {
	position: relative;
	top: 5px;
	width: 20px !important;
	height: 20px !important;
	background: url(/resources/img/prev.png) no-repeat !important; 
}
.popup_content #content_p table td#next_jqGridPager1 {
	position: relative;
	top: 5px;
	width: 20px !important;
	height: 20px !important;
	background: url(/resources/img/next.png) no-repeat !important; 
}
.popup_content #content_p table td#first_jqGridPager1 {
	position: relative;
	top: 5px;
	width: 20px !important;
	height: 20px !important;
	background: url(/resources/img/pprev.png) no-repeat !important; 
}
.popup_content #content_p table td#last_jqGridPager1 {
	position: relative;
	top: 5px;
	width: 20px !important;
	height: 20px !important;
	background: url(/resources/img/nnext.png) no-repeat !important; 
}
</style>
<div class="popup_content">
	<div class="popup_title">전체 대사 내역<a href="#!"><div class="top_close"></div></a></div>
	<div>
		<div class="popup_info">
			일대사번호: <span id="ttl_dd_cmpr_no" class="popup_value"></span>&nbsp;대사일자: <span id="ttl_cmpr_dd" class="popup_value"></span>
		</div>
		<div class="btn_excel" style="float:right;"><button id="btn_excel1">엑셀다운</button></div>
		<div style="clear:both;"></div>
	</div>	
	<div id="content_p">
		<table id="jqGrid1"></table>
		<div id="jqGridPager1"></div>
		<div id="jqGridExcelDiv1" style="display:none">
			<table id="jqGridExcel1" ></table>
		</div>
	</div>
	<a href="#!"><div class="btn_close">닫기</div></a>
<!-- 전체 대사 내역 끝-->
</div>
<div id="myPopup" class="popup">
</div>