<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="com.olleh.pHubCMS.common.components.Constant"%>
<%--
 **********************************************************************************************
 * @desc : 클립포인트 일대사 내역 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/cmpr/clipPntCmprHistViewJs.jsp
 * @author bmg
 * @since 2018.10.18
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.18    bmg        최초생성
 * 
 **********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
$(document).ready(function () { 
	var colNames = ['No','일대사번호','대사일자','전문파일명','총거래건수','일치건수','불일치건수','작업자ID','작업일시','파일내역','재처리'];
	var colModel = [{ label: 'No' ,name: 'no' ,width: cWidth * 0.05 ,key:true 	}
		,{ label: '일대사번호'	,name: 'dd_cmpr_no'		,width: cWidth * 0.10 	}
		,{ label: '대사일자'	,name: 'cmpr_dd' 		,width: cWidth * 0.10 	}
		,{ label: '전문파일명'	,name: 'doc_file_nm'	,width: cWidth * 0.13	, align:'left' 	}
		,{ label: '총거래건수'	,name: 'ttl_deal_cnt'	,width: cWidth * 0.08	, align:'right', formatter:'integer', formatoptions:{thousandsSeparator:','}, cellattr:tclick }
		,{ label: '일치건수'	,name: 'mtch_cnt'		,width: cWidth * 0.08	, align:'right', formatter:'integer', formatoptions:{thousandsSeparator:','}, cellattr:sclick }
		,{ label: '불일치건수'	,name: 'no_acr_cnt' 	,width: cWidth * 0.08	, align:'right', formatter:'integer', formatoptions:{thousandsSeparator:','}, cellattr:fclick }
		,{ label: '작업자ID'	,name: 'mdfy_user_id' 	,width: cWidth * 0.10 	}
		,{ label: '작업일시'	,name: 'mdfy_dt' 		,width: cWidth * 0.12 	}
		,{ label: '파일내역'	,name: 'f' 				,width: cWidth * 0.09	, formatter:hbutton }
		,{ label: '재처리'		,name: 'c' 				,width: cWidth * 0.09	, formatter:cbutton }
	];
	PHJQg.load('jqGrid','jqGridPager',colNames,colModel);
	
	initTtlCmprHistPop();
	initSucsHistPop();
	initFailHistPop();
	initCmprFileHistPop();
	
	var sdt = new Date();
	var edt = new Date();
	sdt.setDate(sdt.getDate()-(1+7));
	edt.setDate(edt.getDate()-1);
	setDate(sdt,edt);
	
	$('#btn_excel').off().on('click', function() {
		if (PHJQg.size('jqGrid') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
		if (!validateDate()) {
			return;
		}
		var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");
		if (conf) {
			var postData = {
				START_DATE: $('#start_date').val(),
				END_DATE: $('#end_date').val(),
				NO_ACR_CNT_TYPE: $('#no_acr_cnt_type').val(),
				NO_ACR_CNT: $(':text[name=no_acr_cnt]').val()
			};
			var colNames = ['No','일대사번호','대사일자','전문파일명','총거래건수','일치건수','불일치건수','작업자ID','작업일시'];
			var colModel = [{ label: 'No' ,name: 'no' 	}
				,{ label: '일대사번호'	,name: 'dd_cmpr_no'		}
				,{ label: '대사일자'	,name: 'cmpr_dd' , 		}
				,{ label: '전문파일명'	,name: 'doc_file_nm', 	}
				,{ label: '총거래건수'	,name: 'ttl_deal_cnt', 	}
				,{ label: '일치건수'	,name: 'mtch_cnt', 		}
				,{ label: '불일치건수'	,name: 'no_acr_cnt' , 	}
				,{ label: '작업자ID'	,name: 'mdfy_user_id' 	}
				,{ label: '작업일시'	,name: 'mdfy_dt' 		}
			];
			PHJQg.excel('jqGridExcelDiv','${ViewRoot}/cmpr/jgClipPntCmprHistExcel.do',postData,colNames,colModel,'클립포인트일대사내역');
		}	
	});
});	
//검색버튼 클릭
function search() {
	if (!validateDate()) {
		return;
	}
	var postData = {
		START_DATE: $('#start_date').val(),
		END_DATE: $('#end_date').val(),
		NO_ACR_CNT_TYPE: $('#no_acr_cnt_type').val(),
		NO_ACR_CNT: $(':text[name=no_acr_cnt]').val()
	};
	PHJQg.reload('jqGrid','jqGridPager','${ViewRoot}/cmpr/jgClipPntCmprHist.do',postData);
}
// 기간 조건 체크
function validateDate(){
	var sDate = $("#start_date").val();
	var eDate = $("#end_date").val();
	if (!PHUtil.isEmpty(sDate) && !PHValid.date(PHUtil.replaceAll(sDate,'-',''))) {
		alert('시작일자가 유효한 날짜 형식이 아닙니다');
		$('#start_date').val('');
		$('#start_date').focus();
		return false;
	}
	if (!PHUtil.isEmpty(eDate) && !PHValid.date(PHUtil.replaceAll(eDate,'-',''))) {
		alert('종료일자가 유효한 날짜 형식이 아닙니다');
		$('#end_date').val('');
		$('#end_date').focus();
		return false;
	}
	var startDate = new Date(sDate);
	var endDate = new Date(eDate);
	if (startDate.getTime() > endDate.getTime()) {		
		alert('뒤의 날짜는 앞의 날짜 이전으로 검색할 수 없습니다.')
		return false;
	} else {
		var valDate = new Date(sDate);
		valDate.setFullYear(valDate.getFullYear()+1)
		if (endDate.getTime() > valDate.getTime()) {
			alert('검색 기간은 1년을 넘을 수 없습니다.');
			return false;
		}
	}
	return true;
}

//formatter 버튼 만들어 주기 (파일내역)
function hbutton(cellvalue,options,rowobject) {
	return '<input type="button" class="table_btn" onclick="cmprFileHistPopClick('+rowobject.no+')" value="확인" />';
}
//formatter 버튼 만들어 주기 (재처리)
function cbutton(cellvalue,options,rowobject) {
	return '<input type="button" class="table_btn" onclick="reProcCmprClipPointClick('+rowobject.no+')" value="재처리" />';
}
//formatter 버튼 만들어 주기 (총거래건수)
function tclick(rowId,tv,rowobject,cm,rdata) {
	if (rowobject.ttl_deal_cnt != null && Number(rowobject.ttl_deal_cnt) > 0) {
		return 'style="cursor:pointer;" onclick="ttlCmprHistPopClick('+rowobject.no+');"';
	} else {
		return '';
	}
}
//formatter 버튼 만들어 주기 (일치건수)
function sclick(cellvalue,options,rowobject) {
	if (rowobject.mtch_cnt != null && Number(rowobject.mtch_cnt) > 0) {
		return 'style="cursor:pointer;" onclick="sucsHistPopClick('+rowobject.no+');"';
	} else { 
		return '';
	}
}
//formatter 버튼 만들어 주기 (불일치건수)
function fclick(cellvalue,options,rowobject) {
	if (rowobject.no_acr_cnt != null && Number(rowobject.no_acr_cnt) > 0) {
		return 'style="cursor:pointer;color:red;" onclick="failHistPopClick('+rowobject.no+');"';
	} else {
		return '';
	}
}
// 전체 대사 내역 팝업 JQGrid 세팅
function initTtlCmprHistPop() {
	var colNames = ['Seq','포인트제공처','포인트허브거래번호','포인트거래번호','포인트','이전포인트거래번호','불일치코드','불일치사유'];
	var colModel = [{ label: 'Seq' ,name: 'seq' ,width: 50 ,key:true 	}
		,{ label: '포인트제공처'		,name: 'prvdr_nm'		,width: 150 	}
		,{ label: '포인트허브거래번호'	,name: 'phub_tr_no'		,width: 155 	}
		,{ label: '포인트거래번호'		,name: 'pnt_tr_no'		,width: 170 	}
		,{ label: '포인트'				,name: 'ans_pnt'		,width: 100		,align:'right',formatter:'integer',formatoptions:{thousandsSeparator:','} }
		,{ label: '이전포인트거래번호'	,name: 'ori_pnt_tr_no'	,width: 200 	}
		,{ label: '불일치코드'			,name: 'err_cd'			,width: 100 	}
		,{ label: '불일치사유'			,name: 'err_msg'		,width: 250		,align:'left' 	}
	];
	var rowNum = 10;
	PHJQg.load('jqGrid1','jqGridPager1',colNames,colModel,rowNum);
}
//전체 대사 내역 팝업 호출 & 엑셀다운로드 세팅
function ttlCmprHistPopClick(data) {
	var row = $("#jqGrid").jqGrid('getRowData',data);
	var postData = {
		DD_CMPR_NO: row.dd_cmpr_no
	};	

	PHJQg.reloadCallback('jqGrid1','jqGridPager1','${ViewRoot}/cmpr/jgClipPntTtlCmprHist.do',postData, function(o) {
		$(o).jqGrid('setGridWidth', $("#content_p").width());
		$('.popup_content').css({
	 		top: Math.max(0, (($(window).height() - $('.popup_content').outerHeight()) / 2) + $(window).scrollTop()) + "px"
	 	});
	});
	$('#ttl_dd_cmpr_no').html(row.dd_cmpr_no);
	$('#ttl_cmpr_dd').html(row.cmpr_dd);
	PHFnc.layerPopOpen(1);
	
	$('#btn_excel1').off().on('click', function() {
		if (PHJQg.size('jqGrid1') == 0) { 
			alert('검색 결과가 없습니다.');
			return;
		}
		else {
			var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");
			if (conf) {
				var postData = {
					DD_CMPR_NO: row.dd_cmpr_no
				};
				var colNames = $('#jqGrid1').jqGrid('getGridParam','colNames');
				var colModel = $('#jqGrid1').jqGrid('getGridParam','colModel');
				PHJQg.excelPopup('jqGridExcelDiv1','jqGridExcel1','${ViewRoot}/cmpr/jgClipPntTtlCmprHistExcel.do',postData,colNames,colModel,'클립포인트일대사전체내역');
			}	
		}
	});
}
//일치 내역 팝업 JQGrid 세팅
function initSucsHistPop() {
	var colNames = ['Seq','포인트제공처','포인트허브거래번호','포인트거래번호','포인트','취소포인트거래번호'];
	var colModel = [{ label: 'Seq' ,name: 'seq' ,width: 170 ,key:true	}
		,{ label: '포인트제공처'		,name: 'prvdr_nm'		,width: 200 	}
		,{ label: '포인트허브거래번호'	,name: 'phub_tr_no'		,width: 200 	}
		,{ label: '포인트거래번호'		,name: 'pnt_tr_no'		,width: 200 	}
		,{ label: '포인트'				,name: 'ans_pnt'		,width: 200		,align:'right',formatter:'integer',formatoptions:{thousandsSeparator:','} }
		,{ label: '취소포인트거래번호'	,name: 'ori_pnt_tr_no'	,width: 200 	}
	];
	var rowNum = 10;
	PHJQg.load('jqGrid2','jqGridPager2',colNames,colModel,rowNum);
}
//일치 내역 팝업 호출 & 엑셀다운로드 세팅
function sucsHistPopClick(data) {
	var row = $("#jqGrid").jqGrid('getRowData',data);
	var postData = {
		DD_CMPR_NO: row.dd_cmpr_no
	};
	
	PHJQg.reloadCallback('jqGrid2','jqGridPager2','${ViewRoot}/cmpr/jgClipPntSucsHist.do',postData, function(o) {
		$(o).jqGrid('setGridWidth', $("#content_p2").width());
		$('.popup_content2').css({
	 		top: Math.max(0, (($(window).height() - $('.popup_content2').outerHeight()) / 2) + $(window).scrollTop()) + "px"
	 	});
	});
	$('#sucs_dd_cmpr_no').html(row.dd_cmpr_no);
	$('#sucs_cmpr_dd').html(row.cmpr_dd);
	PHFnc.layerPopOpen(2);
	
	$('#btn_excel2').off().on('click', function() {
		if (PHJQg.size('jqGrid2') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
		else {
			var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");
			if (conf) {
				var postData = {
					DD_CMPR_NO: row.dd_cmpr_no
				};
				var colNames = $('#jqGrid2').jqGrid('getGridParam','colNames');
				var colModel = $('#jqGrid2').jqGrid('getGridParam','colModel');
				PHJQg.excelPopup('jqGridExcelDiv2','jqGridExcel2','${ViewRoot}/cmpr/jgClipPntSucsHistExcel.do',postData,colNames,colModel,'클립포인트일대사일치내역');
			}	
		}
	});
}
//불일치 내역 팝업 JQGrid 세팅
function initFailHistPop() {
	var colNames = ['Seq','포인트제공처','포인트허브거래번호','포인트거래번호','포인트','취소포인트거래번호','불일치코드','불일치사유'];
	var colModel = [{ label: 'Seq' ,name: 'seq' ,width: 50 ,key:true }
		,{ label: '포인트제공처'		,name: 'prvdr_nm'		,width: 150 }
		,{ label: '포인트허브거래번호'	,name: 'phub_tr_no'		,width: 155 }
		,{ label: '포인트거래번호'		,name: 'pnt_tr_no'		,width: 200 }
		,{ label: '포인트'				,name: 'ans_pnt'		,width: 100	,align:'right', formatter:'integer', formatoptions:{thousandsSeparator:','} }
		,{ label: '취소포인트거래번호'	,name: 'ori_pnt_tr_no'	,width: 200 }
		,{ label: '불일치코드'			,name: 'err_cd'			,width: 200 }
		,{ label: '불일치사유'			,name: 'err_msg'		,width: 200	,align:'left' }
	];
	var rowNum = 10;	
	PHJQg.load('jqGrid3','jqGridPager3',colNames,colModel,rowNum);
}
//불일치 내역 팝업 호출 & 엑셀다운로드 세팅
function failHistPopClick(data) {
	var row = $("#jqGrid").jqGrid('getRowData',data);
	var postData = {
		DD_CMPR_NO: row.dd_cmpr_no
	};
	
	PHJQg.reloadCallback('jqGrid3','jqGridPager3','${ViewRoot}/cmpr/jgClipPntFailHist.do',postData, function(o) {
		$(o).jqGrid('setGridWidth', $("#content_p3").width());
		$('.popup_content3').css({
	 		top: Math.max(0, (($(window).height() - $('.popup_content3').outerHeight()) / 2) + $(window).scrollTop()) + "px"
	 	});
	});
	$('#fail_dd_cmpr_no').html(row.dd_cmpr_no);
	$('#fail_cmpr_dd').html(row.cmpr_dd);
	PHFnc.layerPopOpen(3);
	
	$('#btn_excel3').off().on('click', function() {
		if (PHJQg.size('jqGrid3') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
		else {
			var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");
			if (conf) {
				var postData = {
					DD_CMPR_NO: row.dd_cmpr_no
				};
				var colNames = $('#jqGrid3').jqGrid('getGridParam','colNames');
				var colModel = $('#jqGrid3').jqGrid('getGridParam','colModel');
				PHJQg.excelPopup('jqGridExcelDiv3','jqGridExcel3','${ViewRoot}/cmpr/jgClipPntFailHistExcel.do',postData,colNames,colModel,'클립포인트일대사불일치내역');
			}	
		}
	});
}
//대사파일 내역 팝업 JQGrid 세팅
function initCmprFileHistPop() {
	var colNames = ['거래일련<br>번호','업무코드','카드사코드','포인트거래번호','승인<br>포인트','거래처처리<br>포인트','취소포인트거래번호','처리결과<br>코드','등록일시'];
	var colModel = [{ label: '거래일련번호' ,name: 'pt_hb_seqnum' ,width: 80 ,key:true }
		,{ label: '업무코드'			,name: 'pt_hb_bne_cd'		,width: 80 }
		,{ label: '카드사코드'			,name: 'pt_hb_partner'		,width: 150 }
		,{ label: '포인트거래번호'		,name: 'pt_hb_uqe_n'		,width: 170 }
		,{ label: '승인포인트'			,name: 'pt_hb_apv_pnt'		,width: 80	,align:'right', formatter:'integer', formatoptions:{thousandsSeparator:','} }
		,{ label: '거래처처리포인트'	,name: 'pt_hb_ri_pnt'		,width: 80	,align:'right', formatter:'integer', formatoptions:{thousandsSeparator:','} }
		,{ label: '취소포인트거래번호'	,name: 'pt_hb_ori_uqe_n'	,width: 170 }
		,{ label: '처리결과코드'		,name: 'pt_hb_ucd'			,width: 80	,align:'center',formatter:jqGridTrim }
		,{ label: '등록일시'			,name: 'rgst_dt'			,width: 150 }
	];
	var rowNum = 10;
	PHJQg.load('jqGrid4','jqGridPager4',colNames,colModel,rowNum);
}
function jqGridTrim(cellvalue,options,rowobject) {
	return $.trim(cellvalue);
}
//대사파일 내역 팝업 호출 & 엑셀다운로드 세팅
function cmprFileHistPopClick(data) {
	var row = $("#jqGrid").jqGrid('getRowData',data);
	var postData = {
		DD_CMPR_NO: row.dd_cmpr_no
	};
	
	PHJQg.reloadCallback('jqGrid4','jqGridPager4','${ViewRoot}/cmpr/jgClipPntCmprFileHist.do',postData, function(o) {
		$(o).jqGrid('setGridWidth', $("#content_p4").width());
		$('.popup_content4').css({
	 		top: Math.max(0, (($(window).height() - $('.popup_content4').outerHeight()) / 2) + $(window).scrollTop()) + "px"
	 	});
	});
	$('#file_dd_cmpr_no').html(row.dd_cmpr_no);
	$('#file_cmpr_dd').html(row.cmpr_dd);
	PHFnc.layerPopOpen(4);
	
	$('#btn_excel4').off().on('click', function() {
		if (PHJQg.size('jqGrid4') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
		else {
			var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");
			if (conf) {
				var postData = {
					DD_CMPR_NO: row.dd_cmpr_no
				};
				var colNames = $('#jqGrid4').jqGrid('getGridParam','colNames');
				var colModel = $('#jqGrid4').jqGrid('getGridParam','colModel');
				var colNamesExcel = $.map(colNames, function(o,i) {
	    			return o.replace(/(<br>)/g,'');
		    	});
				PHJQg.excelPopup('jqGridExcelDiv4','jqGridExcel4','${ViewRoot}/cmpr/jgClipPntCmprFileHistExcel.do',postData,colNamesExcel,colModel,'클립포인트일대사파일내역');
			}	
		}
	});
}
//재처리 버튼 클릭
function reProcCmprClipPointClick(data) {
	var row = $("#jqGrid").jqGrid('getRowData',data);
	var conf = confirm(row.cmpr_dd+"일자 대사를 재처리 하시겠습니까?");
	if (conf) {
		var params = { 
			cmprDd: row.cmpr_dd.replace(/-/gi, '')
		};
		PHFnc.ajax("${ViewRoot}/comm/btch/reProcCmprClipPoint.do",params,"POST","json",function() {
			alert("재처리를 요청하였습니다.");
			$(".top_close").click();
		},null,true,true,false);
	}
}
</script>