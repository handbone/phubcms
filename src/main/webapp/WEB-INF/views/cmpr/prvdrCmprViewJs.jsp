<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
 **********************************************************************************************
 * @desc : 포인트 제공처 수수료 정산조회 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/cmpr/prvdrCmprViewJs.jsp
 * @author ojh
 * @since 2018.08.27
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.27    ojh        최초생성
 * </pre>
 **********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
$(document).ready(function () {  
	var colNames = ['No', '카드사명','포인트별거래번호','사용포인트','사용/취소','수수료율(VAT포함)','수수료금액(VAT포함)','거래일시'];
	var colModel = [{ label: 'No' ,name: 'no' ,width: cWidth * 0.04, key:true }
		,{ label: '카드사명'			,name: 'prvdr_nm'		,width: cWidth * 0.16		,align:'left'}
		,{ label: '포인트별거래번호'	,name: 'pnt_tr_no'		,width: cWidth * 0.12 }
		,{ label: '사용포인트'			,name: 'ans_pnt' 		,width: cWidth * 0.10		,formatter:setComma,align:'right' }
		,{ label: '사용/취소'			,name: 'deal_ind'		,width: cWidth * 0.08 }
		,{ label: '수수료율(VAT포함)'	,name: 'pnt_cmsn_rate'  ,width: cWidth * 0.10		,formatter:cmsn }
		,{ label: '수수료금액(VAT포함)'	,name: 'cmsn_amt'  		,width: cWidth * 0.10		,formatter:calAmt,align:'right' }
		,{ label: '거래일시'			,name: 'rgst_dt'		,width: cWidth * 0.12 }
	];
	PHJQg.load('jqGrid','jqGridPager',colNames,colModel);
	
	//datePicker 셋팅
	var dt = new Date();
	setDate(dt, dt);
	
	//엑셀다운로드
	$("#btn_excel").on("click", function() {
		if (PHJQg.size('jqGrid') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
		if (!validateDate()) {
			return;
		}
		var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");	
		if(conf){		
			var postData = {
				PNT_CD : $("#sel_card").val(),
				DEAL_IND : $("#sel_ind").val(),
	        	START_DATE : $("#start_date").val(),
	        	END_DATE : $("#end_date").val()	
			};
			var colNames = ['No', '카드사명','포인트별거래번호','사용포인트','사용/취소','수수료율(VAT포함)','수수료금액(VAT포함)','거래일시'];
			var colModel = [{ label: 'No' ,name: 'no' }
				,{ label: '카드사명'			,name: 'prvdr_nm' }
				,{ label: '포인트별거래번호'	,name: 'pnt_tr_no' }
				,{ label: '사용포인트'			,name: 'ans_pnt'		,formatter:setComma }
				,{ label: '사용/취소'			,name: 'deal_ind' }
				,{ label: '수수료율(VAT포함)'	,name: 'pnt_cmsn_rate'	,formatter:cmsn }
				,{ label: '수수료금액(VAT포함)'	,name: 'cmsn_amt'  		,formatter:calAmt }
				,{ label: '거래일시'			,name: 'rgst_dt' }
			];
			PHJQg.excel('jqGridExcelDiv','${ViewRoot}/cmpr/jgPrvdrCmprExcel.do',postData,colNames,colModel,'포인트제공처수수료정산');
		}
	});
});	
function cmsn(cellvalue,options,rowobject){
	return cellvalue + " %";
}
//수수료 금액 계산
function calAmt(cellvalue,options,rowobject){
	var amount = Math.round( (rowobject.ans_pnt) * (rowobject.pnt_cmsn_rate) / 100);
	return PHUtil.setComma(amount);	
}
//금액으로 표시, 콤마 넣기
function setComma(cellvalue,options,rowobject){
	
	return PHUtil.setComma(cellvalue);
}
//검색버튼 클릭
//파라미터 전달 후 jQgrid reload 
function search() {
	if (!validateDate()) {
		return;
	}
	var postData = {
		PNT_CD : $("#sel_card").val(),
		DEAL_IND : $("#sel_ind").val(),
       	START_DATE : $("#start_date").val(),
       	END_DATE : $("#end_date").val()	
	};
	PHJQg.reload('jqGrid','jqGridPager','${ViewRoot}/cmpr/jgPrvdrCmpr.do',postData);
}

//조회기간 제약 // 제약 Max한달 
function validateDate() {
	var sDate = $("#start_date").val();
	var eDate = $("#end_date").val();
	if (!PHUtil.isEmpty(sDate) && !PHValid.date(PHUtil.replaceAll(sDate,'-',''))) {
		alert('시작일자가 유효한 날짜 형식이 아닙니다');
		$('#start_date').val('');
		$('#start_date').focus();
		return false;
	}
	if (!PHUtil.isEmpty(eDate) && !PHValid.date(PHUtil.replaceAll(eDate,'-',''))) {
		alert('종료일자가 유효한 날짜 형식이 아닙니다');
		$('#end_date').val('');
		$('#end_date').focus();
		return false;
	}
	var startDate = new Date(sDate);
	var endDate = new Date(eDate);
	if (startDate.getTime() > endDate.getTime()) {		
		alert('뒤의 날짜는 앞의 날짜 이전으로 검색할 수 없습니다.')
		return false;
	} else {
		var valDate = new Date(sDate);
		valDate.setMonth(valDate.getMonth()+1);
		if (endDate.getTime() > valDate.getTime()) {
			alert('검색 기간은 한달을 넘을 수 없습니다.');
			return false;
		}
	}
	return true;
}
//formatter 버튼 만들어 주기 클릭시 No전달
function dbutton(cellvalue,options,rowobject){	
	//포인트허브 있으며 클립포인트에 없을때 분기
    return '<input type="button" class="table_btn" onclick="" value="승인취소" />';
}
</script>