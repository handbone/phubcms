<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
 **********************************************************************************************
 * @desc : 클립일대사 관리회 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/cmpr/clipCmprViewJs.jsp
 * @author ojh
 * @since 2018.08.24
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.24    ojh        최초생성
 * </pre>
 **********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
$(document).ready(function () { 
	var colNames = ['No', '포인트허브<br>거래번호', '상품명','총<br>결제금액','카드사명','포인트별거래번호','포인트취소<br>원거래번호','PG거래번호','사용포인트','사용<br>/취소','거래일시','불일치<br>코드','불일치<br>내용','서비스명'];
	var colModel = [{ label: 'No'		,name: 'no'				,width: cWidth * 0.04	,key:true}
		,{ label: '포인트허브거래번호'	,name: 'phub_tr_no' 	,width: cWidth * 0.12 }
		,{ label: '상품명'				,name: 'goods_nm' 		,width: cWidth * 0.10	,align:'left' }
		,{ label: '총결제금액'			,name: 'ttl_pay_amt'	,width: cWidth * 0.07	,align:'right'	,formatter: caStr }
		,{ label: '카드사명'			,name: 'prvdr_nm'		,width: cWidth * 0.12	,align:'left' }
		,{ label: '포인트별거래번호'	,name: 'pnt_tr_no'		,width: cWidth * 0.15 }
		,{ label: '포인트취소원거래번호',name: 'ori_pnt_tr_no' 	,width: cWidth * 0.14 }
		,{ label: 'PG거래번호'			,name: 'pg_deal_no' 	,width: cWidth * 0.25 }
		,{ label: '사용포인트'			,name: 'ans_pnt_amt' 	,width: cWidth * 0.07	,align:'right'	,formatter: caStr }
		,{ label: '사용/취소'			,name: 'cncl_yn'  		,width: cWidth * 0.05	,formatter:cnclStr }
		,{ label: '거래일시'			,name: 'deal_dt'  		,width: cWidth * 0.12 }
		,{ label: '불일치코드'			,name: 'err_cd'  		,width: cWidth * 0.06 }
		,{ label: '불일치내용'			,name: 'err_msg'  		,width: cWidth * 0.06 }
		,{ label: '서비스명'			,name: 'service_nm'  	,width: cWidth * 0.10 }
	];		
	PHJQg.load('jqGrid','jqGridPager',colNames,colModel);
	
	//datePicker 셋팅
	//하루 이전으로 달력 셋팅
	var dt = new Date();
	dt.setDate(dt.getDate()-1);
	setDate(dt,dt);
	
	//엑셀다운로드
	$("#btn_excel").on("click", function(){
		if (PHJQg.size('jqGrid') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
		if (!validateDate()) {
			return;
		}
		var ctn = PHUtil.replaceAll($("#in_cuTn").val(),"-","");
		ctn = PHUtil.replaceAll(ctn,"/","");
		var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");	
		if(conf){		
			var postData = {
				PHUB_TR_NO : $("#in_trNo").val()
				,PNT_TR_NO : $("#in_ptNo").val()
				,PG_DEAL_NO: $("#in_pgNo").val()
				,ERR_YN    : $("#sel_err").val()
				,CUST_CTN  : ctn
				,PH_SVC_CD 	: $('#sel_svcCd').val()
				,START_DATE : $("#start_date").val()
				,END_DATE 	: $("#end_date").val()	
			};
			var colNames = ['No', '포인트허브거래번호', '상품명','총결제금액','카드사명','포인트별거래번호','포인트취소원거래번호','PG거래번호','사용포인트','사용/취소','거래일시','불일치코드','불일치내용','서비스명'];
			var colModel = [{ label: 'No'		,name: 'no' }
				,{ label: '포인트허브거래번호'	,name: 'phub_tr_no' }
				,{ label: '상품명'				,name: 'goods_nm' }
				,{ label: '총결제금액'			,name: 'ttl_pay_amt'	,formatter: caStr }
				,{ label: '카드사명'			,name: 'prvdr_nm' }
				,{ label: '포인트별거래번호'	,name: 'pnt_tr_no' }
				,{ label: '포인트취소원거래번호',name: 'ori_pnt_tr_no' }
				,{ label: 'PG거래번호'			,name: 'pg_deal_no' }
				,{ label: '사용포인트'			,name: 'ans_pnt_amt'	,formatter: caStr }
				,{ label: '사용/취소'			,name: 'cncl_yn'		,formatter:cnclStr }
				,{ label: '거래일시'			,name: 'deal_dt' }
				,{ label: '불일치코드'			,name: 'err_cd' }
				,{ label: '불일치내용'			,name: 'err_msg' }
				,{ label: '서비스명'			,name: 'service_nm' }
			];
			PHJQg.excel('jqGridExcelDiv','${ViewRoot}/cmpr/jgClipCmprExcel.do',postData,colNames,colModel,'클립일대사관리');
		}
	});
});	
function caStr(cellvalue,options,rowobject){
	cellvalue = cellvalue != null ? cellvalue+'' : cellvalue;
	if(rowobject.deal_ind != 'PA'){
		if (Number(cellvalue)) {
			return "-"+PHUtil.setComma(cellvalue);
		}
		else {
			return "";
		}
	} else{
		return PHUtil.nvl(PHUtil.setComma(cellvalue),'');
	}
}
function cnclStr(cellvalue,options,rowobject){
	if(rowobject.cncl_yn == 'Y'){
		return '취소'
	} else if(rowobject.cncl_yn == 'N'){
		return '사용'
	} else if(rowobject.cncl_yn == 'A'){
		return '취소'
	}
	return ''
}

//검색버튼 클릭
//파라미터 전달 후 jQgrid reload 
function search() {
	if (!validateDate()) {
		return;
	}
	var ctn = PHUtil.replaceAll($("#in_cuTn").val(),"-","");
	ctn = PHUtil.replaceAll(ctn,"/","");
	var postData = {
		PHUB_TR_NO : $("#in_trNo").val()
		,PNT_TR_NO : $("#in_ptNo").val()
		,PG_DEAL_NO: $("#in_pgNo").val()
		,ERR_YN    : $("#sel_err").val()
		,CUST_CTN  : ctn
		,PH_SVC_CD 	: $('#sel_svcCd').val()
		,START_DATE : $("#start_date").val()
		,END_DATE 	: $("#end_date").val()	
	};
	PHJQg.reload('jqGrid','jqGridPager','${ViewRoot}/cmpr/jgClipCmpr.do',postData);
}

//조회기간 제약 // 제약 Max한달 
function validateDate() {
	var sDate = $("#start_date").val();
	var eDate = $("#end_date").val();
	if (!PHUtil.isEmpty(sDate) && !PHValid.date(PHUtil.replaceAll(sDate,'-',''))) {
		alert('시작일자가 유효한 날짜 형식이 아닙니다');
		$('#start_date').val('');
		$('#start_date').focus();
		return false;
	}
	if (!PHUtil.isEmpty(eDate) && !PHValid.date(PHUtil.replaceAll(eDate,'-',''))) {
		alert('종료일자가 유효한 날짜 형식이 아닙니다');
		$('#end_date').val('');
		$('#end_date').focus();
		return false;
	}
	var startDate = new Date(sDate);
	var endDate = new Date(eDate);
	if (startDate.getTime() > endDate.getTime()) {		
		alert('뒤의 날짜는 앞의 날짜 이전으로 검색할 수 없습니다.')
		return false;
	} else {
		var valDate = new Date(sDate);
		valDate.setMonth(valDate.getMonth()+1);
		if (endDate.getTime() > valDate.getTime()) {
			alert('검색 기간은 한달을 넘을 수 없습니다.');
			return false;
		}
	}
	return true;
}
function successAjax(){
	alert("처리되었습니다.")
	search();
}
</script>