<%@  page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
 **********************************************************************************************
 * @desc : 클립포인트 일대사 내역
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/cmpr/clipPntCmprHistView.jsp
 * @author bmg
 * @since 2018.10.18
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.18    bmg        최초생성
 * 2019.05.27    kimht        include 수정
 **********************************************************************************************
--%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<%-- global --%>
	<jsp:include page="/WEB-INF/views/include/globalVar.jsp" flush="false" />
	<%-- head --%>
	<jsp:include page="/WEB-INF/views/include/incHead.jsp" flush="false" />
</head>
<body>
<!-- loader 시작-->
<div class="loader" onclick="loader_close()">
    <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
</div>
<!-- loader 끝-->
<div id="wrap">	
<header>
	<%-- header --%>
	<jsp:include page="/WEB-INF/views/include/incHeader.jsp" flush="false" />
</header>
    <!-- 레프트 메뉴 해당페이지 표시 -->
    <style type="text/css">
    	.${sessionScope.USER_MENU.menu_id}{ color: #fff !important; background: #3f4759 !important; border-left: 3px solid #fff; cursor: hand;} 
		.popup_info {
			float:left;
			padding: 5px 14px;
			font-size: 15px;
			font-weight: 400;
			margin-top: 10px;
		}
		.popup_value {
			margin-right: 20px;
			font-weight: bold;
			text-decoration: underline;
		}
    </style>
    <!-- 레프트 메뉴 해당페이지 표시 끝 -->
    
    <!-- popup 시작-->
    <jsp:include page="./clipPntTtlCmprHistPop.jsp" flush="false" />
    
    <jsp:include page="./clipPntSucsHistPop.jsp" flush="false" />
    
    <jsp:include page="./clipPntFailHistPop.jsp" flush="false" />
    
    <jsp:include page="./clipPntCmprFileHistPop.jsp" flush="false" />    
    <!-- popup 끝-->
	
<!-- dash_board 시작-->
<section id="dash_board">
    <!--side_menu 시작-->
	<jsp:include page="/WEB-INF/views/include/incMenu.jsp" flush="false" />
    <!--side_menu 끝-->
	
	<!--contents 시작-->
    <div class="contents mCustomScrollbar">
        <div class="page_title"><div class="page_name">${sessionScope.USER_MENU.menu_nm}</div><div class="page_location"><i class="fas fa-home"></i>${sessionScope.USER_MENU.mnu_path}</div></div>
        <div class="subject">
            <div class="page_search">
                <table class="search_tb" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="search_item">
                            <ul>
                                <li>대사일자 <input type="text" id="start_date" readonly> ~ <input type="text" id="end_date" readonly></li>
                                <li>불일치건수
                                    <select id="no_acr_cnt_type" style="width:50px;">
                                        <option value="GE">>=</option>
                                        <option value="LE"><=</option>
                                    </select>
                                    <input type="text" name="no_acr_cnt" mexlength="5">
                                </li>
                            </ul>
                        </td>
                        <td class="btn_search">
                            <button type="button" onclick="search();">조회</button>
                        </td>
                    </tr>
                </table>

            </div>
            <div class="page_contents">
                <div class="btn_excel"><button id="btn_excel">엑셀다운</button></div>
                <div id="content">
                    <table id="jqGrid"></table>
                    <div id="jqGridPager"></div>
                </div>
                <div id="jqGridExcelDiv" style="display:none">
                	<table id="jqGridExcel" ></table>
                </div>
                <!-- jqGrid  끝 -->
            </div>
        </div>

        <!-- 스크롤 때문에 여백 있어야함 -->
        <br>
        <br>
        <br>
        <br>
        <br>

    </div>
    <!--contents 끝-->
    
</section>
<!-- dash_board 끝--> 

    <!-- footer 시작-->
    <jsp:include page="/WEB-INF/views/include/incFooter.jsp" flush="false" />
    <section class="footer">	 
    </section>
    <!-- footer 끝-->
    
    <%-- js --%>
	<jsp:include page="./clipPntCmprHistViewJs.jsp" flush="false" />
	
</body>
</html>