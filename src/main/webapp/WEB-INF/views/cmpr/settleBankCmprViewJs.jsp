<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="com.olleh.pHubCMS.common.components.Constant"%>
<%--
 **********************************************************************************************
 * @desc : 세틀뱅크 일대사 내역 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/cmpr/settleBankCmprViewJs.jsp
 * @author bmg
 * @since 2018.11.13
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.11.13    bmg        최초생성
 * 
 **********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
$(document).ready(function () { 
	var colNames = ['No','일대사번호','대사일자','전문파일명','총거래건수<br>(포인트허브)','총거래금액<br>(포인트허브)','생성건수<br>(세틀뱅크)','생성금액<br>(세틀뱅크)','불일치건수','불일치금액','작업자ID','작업일시','파일내역','재처리'];
	var colModel = [{ label: 'No' ,name: 'no' ,width: cWidth * 0.05 ,key:true }
		,{ label: '일대사번호'				,name: 'dd_cmpr_no'		,width: cWidth * 0.10 	}
		,{ label: '대사일자'				,name: 'cmpr_dd' 		,width: cWidth * 0.10 	}
		,{ label: '전문파일명'				,name: 'file_nm'		,width: cWidth * 0.13	,align:'left' }
		,{ label: '총거래건수(포인트허브)'	,name: 'pay_cnt'		,width: cWidth * 0.08	,align:'right',formatter:'integer',formatoptions:{thousandsSeparator:','}	}
		,{ label: '총거래금액(포인트허브)'	,name: 'pay_pnt_amt'	,width: cWidth * 0.08	,align:'right',formatter:'integer',formatoptions:{thousandsSeparator:','}	}
		,{ label: '생성건수(세틀뱅크)'		,name: 'cmpr_ttl_cnt' 	,width: cWidth * 0.08	,align:'right',formatter:'integer',formatoptions:{thousandsSeparator:','}	}
		,{ label: '생성금액(세틀뱅크)'		,name: 'cmpr_ttl_amt' 	,width: cWidth * 0.08	,align:'right',formatter:'integer',formatoptions:{thousandsSeparator:','}	}
		,{ label: '불일치건수'				,name: 'ttl_cncl_cnt' 	,width: cWidth * 0.08	,align:'right',formatter:'integer',formatoptions:{thousandsSeparator:','}	}
		,{ label: '불일치금액'				,name: 'ttl_cncl_amt' 	,width: cWidth * 0.08	,align:'right',formatter:'integer',formatoptions:{thousandsSeparator:','}	}
		,{ label: '작업자ID'				,name: 'mdfy_user_id' 	,width: cWidth * 0.08 	}
		,{ label: '작업일시'				,name: 'mdfy_dt' 		,width: cWidth * 0.12 	}
		,{ label: '파일내역'				,name: 'f' 				,width: cWidth * 0.09	,formatter:fbutton }
		,{ label: '재처리'					,name: 'd' 				,width: cWidth * 0.08	,formatter:cbutton }
	];
	PHJQg.load('jqGrid','jqGridPager',colNames,colModel);
	
	$('#btn_excel').off().on('click', function() {
		if (PHJQg.size('jqGrid') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
		if (!validateDate()) {
			return;
		}
		var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");
		if (conf) {
			var postData = {
				START_DATE: $('#start_date').val(),
				END_DATE: $('#end_date').val(),
				TTL_CNCL_CNT_TYPE: $('#ttl_cncl_cnt_type').val(),
				TTL_CNCL_CNT: $(':text[name=ttl_cncl_cnt]').val()
			};
			var colNames = ['No','일대사번호','대사일자','전문파일명','총거래건수(포인트허브)','총거래금액(포인트허브)','생성건수(세틀뱅크)','생성금액(세틀뱅크)','불일치건수','불일치금액','작업자ID','작업일시'];
			var colModel = [{ label: 'No' ,name: 'no' }
				,{ label: '일대사번호'				,name: 'dd_cmpr_no'		}
				,{ label: '대사일자'				,name: 'cmpr_dd'		}
				,{ label: '전문파일명'				,name: 'file_nm'		}
				,{ label: '총거래건수(포인트허브)'	,name: 'pay_cnt'		, formatter:'integer'	}
				,{ label: '총거래금액(포인트허브)'	,name: 'pay_pnt_amt'	, formatter:'integer'	}
				,{ label: '생성건수(세틀뱅크)'		,name: 'cmpr_ttl_cnt'	, formatter:'integer'	}
				,{ label: '생성금액(세틀뱅크)'		,name: 'cmpr_ttl_amt'	, formatter:'integer'	}
				,{ label: '불일치건수'				,name: 'ttl_cncl_cnt'	, formatter:'integer'	}
				,{ label: '불일치금액'				,name: 'ttl_cncl_amt'	, formatter:'integer'	}
				,{ label: '작업자ID'				,name: 'mdfy_user_id'	}
				,{ label: '작업일시'				,name: 'mdfy_dt'		}
			];
			PHJQg.excel('jqGridExcelDiv','${ViewRoot}/cmpr/jgSettleBankCmprExcel.do',postData,colNames,colModel,'세틀뱅크일대사내역');
		}
	});
	
	initCmprFilePop();
	
	var sdt = new Date();
	var edt = new Date();
	sdt.setDate(sdt.getDate()-(1+7));
	edt.setDate(edt.getDate()-1);
	setDate(sdt,edt);
});	
//formatter 버튼 만들어 주기 (파일내역)
function fbutton(cellvalue,options,rowobject) {
	return '<input type="button" class="table_btn" onclick="cmprFilePopClick('+rowobject.no+')" value="파일내역" />';
}
//formatter 버튼 만들어 주기 (재처리)
function cbutton(cellvalue,options,rowobject) {
	return '<input type="button" class="table_btn" onclick="reBatchManProcClick('+rowobject.no+')" value="재처리" />';
}
//검색버튼 클릭
function search() {
	if (!validateDate()) {
		return;
	}
	var postData = {
		START_DATE: $('#start_date').val(),
		END_DATE: $('#end_date').val(),
		TTL_CNCL_CNT_TYPE: $('#ttl_cncl_cnt_type').val(),
		TTL_CNCL_CNT: $(':text[name=ttl_cncl_cnt]').val()
	};
	PHJQg.reload('jqGrid','jqGridPager','${ViewRoot}/cmpr/jgSettleBankCmpr.do',postData);
}
//기간 조건 체크
function validateDate() {
	var sDate = $("#start_date").val();
	var eDate = $("#end_date").val();
	if (!PHUtil.isEmpty(sDate) && !PHValid.date(PHUtil.replaceAll(sDate,'-',''))) {
		alert('시작일자가 유효한 날짜 형식이 아닙니다');
		$('#start_date').val('');
		$('#start_date').focus();
		return false;
	}
	if (!PHUtil.isEmpty(eDate) && !PHValid.date(PHUtil.replaceAll(eDate,'-',''))) {
		alert('종료일자가 유효한 날짜 형식이 아닙니다');
		$('#end_date').val('');
		$('#end_date').focus();
		return false;
	}
	var startDate = new Date(sDate);
	var endDate = new Date(eDate);
	if (startDate.getTime() > endDate.getTime()) {		
		alert('뒤의 날짜는 앞의 날짜 이전으로 검색할 수 없습니다.')
		return false;
	} else {
		var valDate = new Date(sDate);
		valDate.setFullYear(valDate.getFullYear()+1);
		if (endDate.getTime() > valDate.getTime()) {
			alert('검색 기간은 1년을 넘을 수 없습니다.');
			return false;
		}
	}
	return true;
}

function reBatchManProcClick(data) {
	var row = $("#jqGrid").jqGrid('getRowData',data);
	var conf = confirm(row.cmpr_dd+"일자 대사를 재처리 하시겠습니까?");
	if (conf) {
		var params = {
			btchId: 'PHubToPGJob',
			cmprDd: row.cmpr_dd.replace(/-/gi, '')
		};
		PHFnc.ajax("${ViewRoot}/btch/batchManProc.do",params,"POST","json",function() {
			alert("재처리를 요청하였습니다.");
			$(".top_close").click();
		},null,true,true,false);
	}
}
//대사 파일 내역 팝업 JQGrid 세팅
function initCmprFilePop() {
	var colNames = ['Seq','포인트허브거래번호','PG거래번호','상품금액','잔여결재금액','전환포인트','거래구분','등록일시'];
	var colModel = [{ label: 'Seq' ,name: 'seq' ,width: 50 ,key:true }
		,{ label: '포인트허브거래번호'	,name: 'phub_tr_no'		,width: 170 	}
		,{ label: 'PG거래번호'			,name: 'pg_deal_no'		,width: 330 	}
		,{ label: '상품금액'			,name: 'pay_amt'		,width: 100		, align:'right',formatter:'integer',formatoptions:{thousandsSeparator:','} }
		,{ label: '잔여결재금액'		,name: 'rmnd_amt'		,width: 100		, align:'right',formatter:'integer',formatoptions:{thousandsSeparator:','} }
		,{ label: '전환포인트'			,name: 'ans_pnt'		,width: 100		, align:'right',formatter:'integer',formatoptions:{thousandsSeparator:','} }
		,{ label: '거래구분'			,name: 'deal_ind_nm'	,width: 100 	}
		,{ label: '등록일시'			,name: 'rgst_dt'		,width: 160 	}
	];
	PHJQg.load('jqGrid1','jqGridPager1',colNames,colModel);
}
//대사 파일 내역 팝업 호출 & 엑셀다운로드 세팅
function cmprFilePopClick(data) {
	var row = $("#jqGrid").jqGrid('getRowData',data);
	var postData = {
		DD_CMPR_NO: row.dd_cmpr_no,
		rows: 10
	};
	PHJQg.reloadCallback('jqGrid1','jqGridPager1','${ViewRoot}/cmpr/jgSettleBankCmprFile.do',postData, function(o) {
		$(o).jqGrid('setGridWidth', $("#content_p").width());
		$('.popup_content').css({
	 		top: Math.max(0, (($(window).height() - $('.popup_content').outerHeight()) / 2) + $(window).scrollTop()) + "px"
	 	});
	});
	$('#settle_dd_cmpr_no').html(row.dd_cmpr_no);
	$('#settle_cmpr_dd').html(row.cmpr_dd);
	PHFnc.layerPopOpen(1);
	
	$('#btn_excel1').off().on('click', function() {
		if (PHJQg.size('jqGrid1') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
		else {
			var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");
			if (conf) {
				var postData = {
					DD_CMPR_NO: row.dd_cmpr_no
				};
				var colNames = $('#jqGrid1').jqGrid('getGridParam','colNames');
				var colModel = $('#jqGrid1').jqGrid('getGridParam','colModel');
				PHJQg.excelPopup('jqGridExcelDiv1','jqGridExcel1','${ViewRoot}/cmpr/jgSettleBankCmprFileExcel.do',postData,colNames,colModel,'세틀뱅크대사파일내역');
			}	
		}
	});
}

</script>