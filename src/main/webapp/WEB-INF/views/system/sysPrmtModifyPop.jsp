<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%>
<%--
 **********************************************************************************************
 * @desc : 시스템파라미터 수정 팝업
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/system/sysPrmtModifyPop.jsp
 * @author bmg
 * @since 2018.09.10
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.10    bmg        최초생성
 *
 **********************************************************************************************
 sysPrmtInfoView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>

<div class="popup_content2">
	<div class="popup_title">시스템파라미터 수정<a href="#!"><div class="top_close"></div></a></div>
	<table class="half_table" border="0" cellpadding="0" cellspacing="0">
		<tbody>
		<tr>
			<th style="width:200px;">파라미터코드</th>
			<td style="width:300px;" id="m_prmt_cd"></td>
			<th style="width:200px;">파라미터그룹코드  <span class="red">*</span></th>
			<td>
			    <select id="m_prmt_grp_cd" style="min-width:165px;">
			    	<option value="">선택</option>
			        <c:forEach items="${PRMT_GRP}" var="code">
			        	<option value="${code.dtlCd}">${code.dtlCdNm}</option>
			        </c:forEach>
			    </select>
			</td>
		</tr>
		<tr>
			<th>용도구분 <!--<span class="red">*</span>--></th>
			<td>
			    <select id="m_use_ind" style="min-width:165px;">
			    	<option value="">선택</option>
			    	<c:forEach items="${PRMT_USE_IND}" var="code">
			        	<option value="${code.dtlCd}">${code.dtlCdNm}</option>
			        </c:forEach>
			    </select>
			</td>
			<th>사용여부 </th>
			<td>
			    <input type="radio" name="m_use_yn" value="Y"> 사용
			    <input type="radio" name="m_use_yn" value="N"> 미사용
			</td>
		</tr>
		<tr>
			<th>파라미터값  <span class="red">*</span></th>
			<td colspan="3"><input type="text" name="m_prmt_val" class="w100" maxlength="100"></td>
		</tr>
		<tr>
			<th>파라미터설명</th>
			<td colspan="3"> <textarea id="m_prmt_desc" rows="5" maxlength="100"></textarea></td>
		</tr>
		<tr>
			<th>등록자</th>
			<td id="m_rgst_user_id_nm"></td>
			<th>등록일시</th>
			<td id="m_rgst_dt"></td>
		</tr>
		<tr>
			<th>수정자</th>
			<td id="m_mdfy_user_id_nm"></td>
			<th>수정일시</th>
			<td id="m_mdfy_dt"></td>
		</tr>
		</tbody>
	</table>
	
	<div class="btnalign">
	    <ul>
			<li><a href="#!"><div class="btn_cancel">취소</div></a></li>
			<li><a href="#!"><div class="btn_save" onclick="modifySysPrmt();">저장</div></a></li>
	    </ul>
	</div>
</div>
<div id="myPopup" class="popup">
</div>