<%@  page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
 **********************************************************************************************
 * @desc : 메뉴관리
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/system/menuInfoView.jsp
 * @author bmg
 * @since 2018.09.20
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.20    bmg        최초생성
 * 2019.05.29    kimht        include 수정
 **********************************************************************************************
--%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<%-- global --%>
	<jsp:include page="/WEB-INF/views/include/globalVar.jsp" flush="false" />
	<%-- head --%>
	<jsp:include page="/WEB-INF/views/include/incHead.jsp" flush="false" />
</head>
<body>
<!-- loader 시작-->
<div class="loader" onclick="loader_close()">
    <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
</div>
<!-- loader 끝-->
<div id="wrap">	
<header>
	<%-- header --%>
	<jsp:include page="/WEB-INF/views/include/incHeader.jsp" flush="false" />
</header>
    <!-- 레프트 메뉴 해당페이지 표시 -->
    <style>.${sessionScope.USER_MENU.menu_id}{ color: #fff !important; background: #3f4759 !important; border-left: 3px solid #fff; cursor: hand; }</style>
    <!-- 레프트 메뉴 해당페이지 표시 끝 -->
    
    <!-- 메뉴정보 등록 시작-->
    <jsp:include page="./menuInfoCreatePop.jsp" flush="false" />
    
    <!-- 메뉴정보 수정 시작-->
    <jsp:include page="./menuInfoModifyPop.jsp" flush="false" />
    
    <!-- Request 액션 정보 수정 등록-->
    <jsp:include page="./actInfoCreatePop.jsp" flush="false" />
    
    <!-- Request 액션 정보 수정 시작-->
    <jsp:include page="./actInfoModifyPop.jsp" flush="false" />

<!-- dash_board 시작-->
<section id="dash_board">
    <!--side_menu 시작-->
	<jsp:include page="/WEB-INF/views/include/incMenu.jsp" flush="false" />
    <!--side_menu 끝-->
	
	<!--contents 시작-->
        <div class="contents mCustomScrollbar">
	        <div class="page_title"><div class="page_name">${sessionScope.USER_MENU.menu_nm}</div><div class="page_location"><i class="fas fa-home"></i>${sessionScope.USER_MENU.mnu_path}</div></div>
            <div class="subject">
                <div class="page_search">
                    <table class="search_tb" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="search_item">
                                <ul>
                                    <li>메뉴명 <input type="text" name="menu_nm"></li>
                                    <li>Request URI <input type="text" name="actn_uri"></li>
                                    <li>메뉴구분
                                        <select id="menu_ind">
                                            <option value="all">선택</option>
                                            <c:forEach items="${MENU_IND}" var="code">
			                           			<option value="${code.dtlCd}">${code.dtlCdNm}</option>
			                           		</c:forEach>
                                        </select>
                                    </li>
                                    <li>메뉴사용여부
                                        <select id="use_yn">
                                            <option value="all">전체</option>
                                            <option value="Y">사용</option>
                                            <option value="N">미사용</option>
                                        </select>
                                    </li>
                                </ul>
                            </td>
                            <td class="btn_search">
                                <button type="button" onclick="search();">조회</button>
                            </td>
                        </tr>
                    </table>

                </div>
                <div class="page_contents">
                    <div class="menuinfview_box">
                    <ul>
                        <li class="contents_left">
                            <div class="tt_line">메뉴목록<button onclick="menuInfoCreatePopClick();">등록</button></div>
                            <div class="menu_list mCustomScrollbar">
                                <ul id="menuRows"></ul>
                            </div>
                        </li>
                        <li class="contents_right">
                            <div class="tt_line">Request 액션정보목록<button onclick="actnInfoCreatePopClick()">등록</button></div>
                            <div class="con_table mCustomScrollbar">
                                <table class="cord_table" border="0" cellpadding="0" cellspacing="0" style="table-layout:fixed;">
                                	<thead>
                                		<tr>
	                                        <th style="width:50px;">No</th>
	                                        <th>URI</th>
	                                        <th>Action명</th>
	                                        <th style="width:65px;">URI<br>Method</th>
	                                        <th style="width:65px;">로그기록</th>
	                                        <th style="width:60px;">수정자</th>
	                                        <th style="width:100px;">수정일시</th>
	                                        <th style="width:70px;">관리</th>
	                                    </tr>
                                	</thead>
                                    <tbody />
                                </table>
                            </div>
                        </li>
                    </ul>
                    </div>
                </div>

            </div>

            <!-- 스크롤 때문에 여백 있어야함 -->
            <br>
            <br>
            <br>
            <br>
            <br>

        </div>
        <!--contents 끝-->
	
</section>
<!-- dash_board 끝--> 

    <!-- footer 시작-->
    <jsp:include page="/WEB-INF/views/include/incFooter.jsp" flush="false" />
    <section class="footer">	 
    </section>
    <!-- footer 끝-->
    
    <%-- js --%>
	<jsp:include page="./menuInfoViewJs.jsp" flush="false" />
	
</body>
</html>