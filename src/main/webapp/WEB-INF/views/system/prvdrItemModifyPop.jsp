<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%--
 **********************************************************************************************
 * @desc : 제공사항정보 상세 팝업
 * @FileName 	: /pHubCMS/src/main/webapp/WEB-INF/views/system/prvdrItemModifyPop.jsp
 * @author 		: bmg
 * @since 		: 2019.05.07
 * @version 	: 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.05.07   bmg        최초생성
 *
 **********************************************************************************************
 agrInfoView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>
<div class="popup_content4">
	<div class="popup_title">제공사항정보 수정<a href="#!"><div class="top_close"></div></a></div>
	<table class="half_table" border="0" cellpadding="0" cellspacing="0">
		<tbody>
            <tr>
                <th>약관ID <span class="red">*</span></th>
                <td>
                	<input type="text" id="m_in_cls_id4" style="width:100%;background-color:lightgray;" maxlength="10" readonly />
                </td>
                <th>약관버전 <span class="red">*</span></th>
                <td>
                	<input type="text" id="m_in_cls_ver4" style="width:100%;background-color:lightgray;" maxlength="10" readonly />
                </td>
            </tr>
            <tr>
            	<th>제공처ID <span class="red">*</span></th>
            	<td>
            		<input type="text" id="m_in_prvdr_id" style="width:100%;" maxlength="20" />
            		<input type="hidden" id="m_in_prvdr_id_original" style="width:100%;" maxlength="20" />
            	</td>
            	<th>제공처이름 <span class="red">*</span></th>
            	<td>
            		<input type="text" id="m_in_prvdr_nm" style="width:100%;" maxlength="50" />
            	</td>
            </tr>
            <tr>
            	<th>제공받는자</th>
            	<td>
            		<input type="text" id="m_in_rcvr_id" style="width:100%;" maxlength="10" onkeyup="return keyReplace(this);" />
            	</td>
            	<th>제공받는자이름</th>
            	<td>
            		<input type="text" id="m_in_rcvr_nm" style="width:100%;" maxlength="20" />
            	</td>
            </tr>
            <tr>
            	<th>목적</th>
            	<td colspan="3">
            		<input type="text" id="m_in_prps" style="width:100%;" maxlength="50" />
            	</td>
            </tr>
            <tr>
            	<th>항목</th>
            	<td colspan="3">
            		<input type="text" id="m_in_item" style="width:100%;" maxlength="200" />
            	</td>
            </tr>
            <tr>
            	<th>보유기간</th>
            	<td colspan="3">
            		<input type="text" id="m_in_hold_prd" style="width:100%;" maxlength="200" />
            	</td>
            </tr>
            <tr>
            	<th>정렬순서 <span class="red">*</span></th>
            	<td>
            		<input type="text" id="m_in_sort_ord4" style="width:50%;" maxlength="4" onkeydown="return onlyNum(event);" onkeyup="return keyReplace(this);" />
            	</td>
            	<th>동의여부</th>
            	<td>
            		<input type="radio" name="m_ra_agre_yn" value="Y"> 사용
			    	<input type="radio" name="m_ra_agre_yn" value="N"> 미사용
            	</td>
            </tr>
            <tr>
            	<th>비고</th>
            	<td colspan="3">
            		<input type="text" id="m_in_rmrk4" rows="3" style="width:100%;" maxlength="100"></textarea>
            	</td>
            </tr>
            <tr>
            	<th>등록자</th>
            	<td name="m_rgst_user_nm"></td>
            	<th>등록일시</th>
            	<td name="m_rgst_dt"></td>
            </tr>
            <tr>
            	<th>수정자</th>
            	<td name="m_mdfy_user_nm"></td>
            	<th>수정일시</th>
            	<td name="m_mdfy_dt"></td>
            </tr>
		</tbody>
	</table>
	<div class="btnalign">
		<ul>
			<li><a href="#!"><div class="btn_cancel">취소</div></a></li>
			<li><a href="#!"><div class="btn_save" onClick="modifyPrvdrItem();">저장</div></a></li>
		</ul>
	</div>
</div>
<!-- popup 끝-->