<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="com.olleh.pHubCMS.common.components.Constant"%>
<%--
 **********************************************************************************************
 * @desc : 사용자 관리 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/system/userInfoViewJs.jsp
 * @author ojh
 * @since 2018.08.22
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.22    ojh        최초생성
 * 
 **********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
$(document).ready(function () { 
	var colNames = ['No', '사용자ID', '사용자명','사용자그룹','연락처','사용자상태','로그인<br>오류횟수','수정자','수정일시','관리','ID'];
	var colModel = [{ label: 'No', name: 'no', width: cWidth * 0.05, key:true }
		,{ label: '사용자ID'		,name: 'm_user_id' 		,width: cWidth * 0.10 }
		,{ label: '사용자명'		,name: 'user_nm' 		,width: cWidth * 0.10 }
		,{ label: '사용자그룹'		,name: 'user_grp_id'	,width: cWidth * 0.10 }
		,{ label: '연락처'			,name: 'cnt_plc'		,width: cWidth * 0.10 }
		,{ label: '사용자상태'		,name: 'stat_cd'		,width: cWidth * 0.10 }
		,{ label: '로그인오류횟수'	,name: 'login_err_cnt' 	,width: cWidth * 0.08 }
		,{ label: '수정자'			,name: 'mdfy_user_nm' 	,width: cWidth * 0.10 }
		,{ label: '수정일시'		,name: 'mdfy_dt' 		,width: cWidth * 0.12 }		
		,{ label: '관리'			,name: '' 				,width: cWidth * 0.05,formatter:mbutton }
		,{ label: 'ID'				,name: 'user_id'		,hidden:true }
	];
	PHJQg.load('jqGrid','jqGridPager',colNames,colModel);
	// 엑셀다운로드
	$("#btn_excel").on("click", function() {
		if (PHJQg.size('jqGrid') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
		if (!validateDate($('#start_date'), $('#end_date'))) {
			return;
		}
		var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");	
		if(conf){		
			var postData = {
				USER_ID : $("#in_id").val(),
				USER_NM : $("#in_nm").val(),
				USER_GRP_ID : $("#sel_grp").val(),
				STAT_CD : $("#sel_stat").val(),
		       	START_DATE : $("#start_date").val(),
		       	END_DATE : $("#end_date").val()	
			};
			var colNames = ['No', '사용자ID', '사용자명','사용자그룹','연락처','사용자상태','로그인오류횟수','수정자','수정일시'];
			var colModel = [{ label: 'No', name: 'no' }
	       		,{ label: '사용자ID'		,name: 'm_user_id' }
	       		,{ label: '사용자명'		,name: 'user_nm' }
	       		,{ label: '사용자그룹'		,name: 'user_grp_id' }
	       		,{ label: '연락처'			,name: 'cnt_plc' }
	       		,{ label: '사용자상태'		,name: 'stat_cd' }
	       		,{ label: '로그인오류횟수'	,name: 'login_err_cnt' }
	       		,{ label: '수정자'			,name: 'mdfy_user_nm' }
	       		,{ label: '수정일시'		,name: 'mdfy_dt' }	
			];
			PHJQg.excel('jqGridExcelDiv','${ViewRoot}/system/jgUserInfoExcel.do',postData,colNames,colModel,'사용자관리');
		}
	});
	
    //사용가능 일 달력 셋팅
	$("#efct_start_date").datepicker();
	$("#efct_end_date").datepicker();	
	$("#m_efct_start_date").datepicker();
	$("#m_efct_end_date").datepicker();
	var dt = new Date();	
	$("#efct_start_date").val(dt.getFullYear()+"-"+cfSetAddZero(dt.getMonth()+1)+"-"+cfSetAddZero(dt.getDate()));
	$("#efct_end_date").val((dt.getFullYear()+1)+"-"+cfSetAddZero(dt.getMonth()+1)+"-"+cfSetAddZero(dt.getDate()-1));
	
	$("#efct_start_date,#efct_end_date,#m_efct_start_date,#m_efct_end_date").removeAttr('readonly')
		.keypress(function(e) {
			var keycode = e.keyCode ? e.keyCode : e.which;
			if ((keycode >= 48 && keycode <= 57) || (keycode >= 96 && keycode <= 105)) {
				switch ($(this).val().length) {
					case 4:
					case 7:
						if (($(this).val().match(/-/g) || []).length < 2) {
							$(this).val($(this).val()+'-');	
						}
						break;
					default:
				}
				return true;
			} else {
	    		switch(keycode) {
		    		case 8:		// BACKSPACE
	    			case 9:		// TAB
	    			case 45:	// -
	    				return true;
	    				break;
	    			default:
	    				e.preventDefault();
	    		}
			}
		})
		.focusout(function(e) {
			if(undefined == $(e.relatedTarget).attr("id")) {
				return false;
			}
			var value = PHUtil.replaceAll($(this).val(), "-", "");
			if (!PHUtil.isEmpty(value) && !PHValid.date(value)) {
				alert('유효한 날짜형식이 아닙니다');
				$(this).val('');
				$(this).select();
				e.stopPropagation();
				return false;
			}
		});

	//검색일자 달력 셋팅
	setDate(null,null);
	
	//X버튼이나 취소 버튼 클릭시 팝업창 초기화
	$(".top_close").click(function () {
		resetPop();		
    });
    $(".btn_cancel").click(function () {
    	resetPop();
    });
    $('.ME02020100').parents('.deps3:eq(0)').css('display', 'block');
});	
function clickStart(id){
	$("#"+id).val("");		
}
function clickEnd(id){
	$("#"+id).val("");		
}

function resetPop(){
	//사용자정보 등록창
	$("#c_in_id").val("");	
	$("#c_in_nm").val("");	
	$("#c_in_pw").val("");
	$("#c_sel_grp").val("all");
	$("#c_sel_stat").val("all");
	$("#c_sel_dept").val("all");	
	$("#in_cnt1").val("");
	$("#in_cnt2").val("");
	$("#in_cnt3").val("");	
	$("#c_in_ip").val("");	
	
	$('#c_in_maxInqrCnt').val('');
	var dt = new Date();	
	$("#efct_start_date").val(dt.getFullYear()+"-"+cfSetAddZero(dt.getMonth()+1)+"-"+cfSetAddZero(dt.getDate()));
	$("#efct_end_date").val((dt.getFullYear()+1)+"-"+cfSetAddZero(dt.getMonth()+1)+"-"+cfSetAddZero(dt.getDate()-1));
	
	//사용자정보 수정창
  	$("#m_in_id").html("");	
	$("#m_in_nm").val("");	
	
	$("#m_sel_grp").val("all");	
	$("#m_sel_stat").val("all");
	
	$("#m_in_cnt1").val("");
	$("#m_in_cnt2").val("");
	$("#m_in_cnt3").val("");	
	
	$("#m_sel_dept").val("all");
	$("#m_in_ip").val("");
	
	var dt = new Date();	
	$("#m_efct_start_date").val(dt.getFullYear()+"-"+cfSetAddZero(dt.getMonth()+1)+"-"+cfSetAddZero(dt.getDate()));
	$("#m_efct_end_date").val((dt.getFullYear()+1)+"-"+cfSetAddZero(dt.getMonth()+1)+"-"+cfSetAddZero(dt.getDate()-1));
	
	$("#m_last_dt").html("");
	$("#m_err_cnt").html("");
	
	$("#m_rgst_user").html("");
	$("#m_rgst_dt").html("");
	
	$("#m_mdfy_user").html("");
	$("#m_mdfy_dt").html("");
}

//기간 조건 체크
function validateDate(start, end) {
	var sDate = start.val();
	var eDate = end.val();
	if (!PHUtil.isEmpty(sDate) && !PHValid.date(PHUtil.replaceAll(sDate,'-',''))) {
		alert('시작일자가 유효한 날짜 형식이 아닙니다');
		start.val('');
		start.focus();
		return false;
	}
	if (!PHUtil.isEmpty(eDate) && !PHValid.date(PHUtil.replaceAll(eDate,'-',''))) {
		alert('종료일자가 유효한 날짜 형식이 아닙니다');
		end.val('');
		end.focus();
		return false;
	}
	var startDate = new Date(sDate);
	var endDate = new Date(eDate);
	if (startDate.getTime() > endDate.getTime()) {		
		alert('뒤의 날짜는 앞의 날짜 이전으로 검색할 수 없습니다.')
		return false;
	}
	return true;
}

//검색버튼 클릭
function search() {
	if (!validateDate($('#start_date'), $('#end_date'))) {
		return;
	}
	var postData = {
		USER_ID : $("#in_id").val(),
		USER_NM : $("#in_nm").val(),
		USER_GRP_ID : $("#sel_grp").val(),
		STAT_CD : $("#sel_stat").val(),
       	START_DATE : $("#start_date").val(),
       	END_DATE : $("#end_date").val()	
	};	
	PHJQg.reload('jqGrid','jqGridPager','${ViewRoot}/system/jgUserInfo.do',postData);	
}

//formatter 버튼 만들어 주기 클릭시 No번호 param 전달
function mbutton(cellvalue,options,rowobject){
    return '<input type="button" class="table_btn" onclick="modifyPopClick('+rowobject.no+')" value="수정" />';
}
//등록 팝업창 초기화
function createPopClick(){
 	PHFnc.layerPopOpen(1);
}

//팝업창 오픈 및 상세 정보 가져오기 
//popId 현재 팝업 정보 띄운 사용자 ID(마스킹 때문에 변수로 지정)
var popId;
function modifyPopClick(data){	
	var row = $("#jqGrid").jqGrid('getRowData',data);
	//누른 창 번호를 key로 상세 조회
	var params = {USER_ID : row.user_id};
	popId = row.user_id;
	PHFnc.ajax("${ViewRoot}/system/ajaxGetUserInfo.do",params,"POST","json",successAjaxGet,null,true,true,false);
	PHFnc.layerPopOpen(2);
}
// 성공 func
function successAjaxGet(data, textStatus, jqXHR){	
	//팝업창에 데이터 세팅
	
	var cnt = data.row.cnt_plc.split("-");	
	if(cnt != null){		
		$("#m_in_cnt1").val(cnt[0]);
		$("#m_in_cnt2").val(cnt[1]);
		$("#m_in_cnt3").val(cnt[2]);
	}
	
  	$("#m_in_id").html(data.row.user_id);	
	$("#m_in_nm").val(data.row.user_nm);	
	$("#m_sel_grp").val(data.row.user_grp_id);	
	$("#m_sel_stat").val(data.row.stat_cd);
	
	$("#m_sel_dept").val(data.row.dept_cd);
	$("#m_in_ip").val(data.row.ip_addr);
	
	$("#m_rgst_user").html(data.row.rgst_user_nm);
	$("#m_rgst_dt").html(data.row.rgst_dt);
	
	$("#m_mdfy_user").html(data.row.mdfy_user_nm);
	$("#m_mdfy_dt").html(data.row.mdfy_dt);
	
	$("#m_last_dt").html(data.row.login_dt);
	$("#m_err_cnt").html(data.row.login_err_cnt);
	
	$("#m_efct_start_date").val(data.row.efct_strt_dd);
	$("#m_efct_end_date").val(data.row.efct_end_dd);
	$('#m_maxInqrCnt').val(data.row.max_inqr_cnt);
	
	//로그인 실패횟수 초과시 잠금 해제 버튼 표시
	if(Number($("#m_err_cnt").html()) >= '${USR_PW_ERR_CNT}'){
		$("#btn_un").show();
	} else{
		$("#btn_un").hide();
	}
	
}

//사용자 정보- 등록하기
function createUserInfo(){	
	var cnt = $("#in_cnt1").val()+"-"+$("#in_cnt2").val()+"-"+$("#in_cnt3").val();	
	
	if($("#c_in_id").val() == ""){
		alert("사용자 ID를 입력해 주세요.");
		return;
	}
	if($("#c_in_nm").val() == ""){
		alert("사용자명을 입력해 주세요.");
		return;
	}
	if($("#c_sel_grp").val() == "all"){
		alert("사용자 그룹을 선택해 주세요.");
		return;
	}	
	if($("#c_sel_stat").val() == "all"){
		alert("사용자 상태를 선택해 주세요.");
		return;
	}	
	if(!PHValid.hpNo(cnt)){
		alert("연락처 핸드폰 번호 형식이 올바르지 않습니다.")
		return;
	}
	if($("#c_sel_dept").val() == "all"){
		alert("부서코드를 선택해 주세요.");
		return;
	}
	if(!PHValid.ip($("#c_in_ip").val())){
		alert("올바르지 않은 IP주소 형식입니다.");
		return;
	}
	if($("#c_maxInqrCnt").val() == '') {
		alert('최대 조회 수를 입력하세요.');
		return;
	}
	if (!validateDate($('#efct_start_date'), $('#efct_end_date'))) {
		return;
	}
	var conf = confirm("저장하시겠습니까?");	
	if(conf){
		var params = {
			USER_ID 		: $("#c_in_id").val()
			, USER_NM   	: $("#c_in_nm").val()
			, USER_GRP_ID 	: $("#c_sel_grp").val()
			, STAT_CD 		: $("#c_sel_stat").val()
			, CNT_PLC 		: cnt
			, DEPT_CD 		: $("#c_sel_dept").val()
			, IP_ADDR 		: $("#c_in_ip").val()
			, EFCT_STRT_DD 	: PHUtil.replaceAll($("#efct_start_date").val(),"-","")
			, EFCT_END_DD 	: PHUtil.replaceAll($("#efct_end_date").val(),"-","")
			, MAX_INQR_CNT	: $("#c_maxInqrCnt").val()
		};	
		PHFnc.ajax("${ViewRoot}/system/ajaxCreateUserInfo.do",params,"POST","json",successCreateAjax,null,true,true,false);
	}
}
//등록 성공 func
function successCreateAjax(data, textStatus, jqXHR){
	alert("등록에 성공했습니다.");
	$(".top_close").click();
	search();
}

function modifyPntUseCmpn(){
	var cnt = $("#m_in_cnt1").val()+"-"+$("#m_in_cnt2").val()+"-"+$("#m_in_cnt3").val();
	if($("#m_in_nm").val() == ""){
		alert("사용자명을 입력해 주세요.");
		return;
	}
	if($("#m_sel_grp").val() == "all"){
		alert("사용자 그룹을 선택해 주세요.");
		return;
	}	
	if($("#m_sel_stat").val() == "all"){
		alert("사용자 상태를 선택해 주세요.");
		return;
	}	
	if(!PHValid.hpNo(cnt)){
		alert("연락처 핸드폰 번호 형식이 올바르지 않습니다.")
		return;
	}
	if($("#m_sel_dept").val() == "all"){
		alert("부서코드를 선택해 주세요.");
		return;
	}
	if(!PHValid.ip($("#m_in_ip").val())){
		alert("올바르지 않은 IP주소 형식입니다.");
		return;
	}	
	if($("#m_maxInqrCnt").val() == '') {
		alert('최대 조회 수를 입력하세요.');
		return;
	}
	if (!validateDate($('#m_efct_start_date'), $('#m_efct_end_date'))) {
		return;
	}
	var conf = confirm("저장하시겠습니까?");	
	if(conf){
		var params = {
			  USER_ID   	: popId
			, USER_NM   	: $("#m_in_nm").val()
			, USER_GRP_ID 	: $("#m_sel_grp").val()
			, STAT_CD 		: $("#m_sel_stat").val()
			, CNT_PLC 		: cnt
			, DEPT_CD 		: $("#m_sel_dept").val()
			, IP_ADDR 		: $("#m_in_ip").val()			
			, EFCT_STRT_DD 	: PHUtil.replaceAll($("#m_efct_start_date").val(),"-","")
			, EFCT_END_DD 	: PHUtil.replaceAll($("#m_efct_end_date").val(),"-","")
			, MAX_INQR_CNT	: $("#m_maxInqrCnt").val()
		};
		PHFnc.ajax("${ViewRoot}/system/ajaxModifyUserInfo.do",params,"POST","json",successModifyAjax,null,true,true,false);	
	}
}
//등록 성공 func
function successModifyAjax(data, textStatus, jqXHR){
	alert("수정에 성공했습니다.");
	$(".top_close").click();
	search();
}
function modifyErrCnt(){
	var conf = confirm("해당 사용자의 계정잠금 상태를 해제 하시겠습니까?");	
	if(conf){
		var params = {
			USER_ID : $("#m_in_id").html()
		};
		PHFnc.ajax("${ViewRoot}/system/ajaxModifyErrCnt.do",params,"POST","json",successModifyCntAjax,null,true,true,false);	
	}
}
function successModifyCntAjax(data, textStatus, jqXHR){
	alert("해제되었습니다.");
	$(".top_close").click();
	search();
}

function testLdapId(ind){	
	if(ind == 1){
		if($("#c_in_id").val() == ""){
			alert("사용자 ID를 입력해 주세요.");
			return;
		}
		if($("#c_in_pw").val() == ""){
			alert("비밀번호를 입력해 주세요.");
			return;
		}
		var params = {
			 USER_ID : $("#c_in_id").val()
			,USER_PW : $("#c_in_pw").val()
		};
		PHFnc.ajax("${ViewRoot}/system/ajaxTestLdapId.do",params,"POST","json",successTestIdAjax,null,true,true,false);	
	} 
// 	else if(ind == 2){
//		if($("#c_in_id").val() == ""){
//			alert("사용자 ID를 입력해 주세요.");
//			return;
//		}
//		if($("#c_in_pw").val() == ""){
//			alert("비밀번호를 입력해 주세요.");
//			return;
//		}
//		var params = {
//			 USER_ID : $("#c_in_id").val()
//			,USER_PW : $("#c_in_pw").val()
//		};
//		PHFnc.ajax("${ViewRoot}/system/ajaxTestLdapId.do",params,"POST","json",successTestIdCreateAjax,null,true,true,false);	
//	}
}
function successTestIdAjax(data, textStatus, jqXHR){	
	if(data.eStat == 0){
		alert("등록 가능한 아이디 입니다.");
		
		$("#c_in_nm").val(data.userName);		
		var cnt = data.mobile.split("-");	
		if(cnt != null){		
			$("#in_cnt1").val(cnt[0]);
			$("#in_cnt2").val(cnt[1]);
			$("#in_cnt3").val(cnt[2]);
		}
		
	} else if(data.eStat == 1){
		alert("이미 존재하는 아이디 입니다.");
	} else if(data.eStat == 2){
		alert("계정 연동에 실패했습니다.");
	} else{
		alert("오류가 발생했습니다.");
	}
}
function successTestIdCreateAjax(data, textStatus, jqXHR){
	if(data.eStat == 0){
		createUserInfo();	
	} else if(data.eStat == 1){
		alert("이미 존재하는 아이디 입니다.");
	} else if(data.eStat == 2){
		alert("계정 연동에 실패했습니다.");
	} else{
		alert("오류가 발생했습니다.");
	}
}
 
</script>