<%@  page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
 **********************************************************************************************
 * @desc : 권한 관리
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/system/authManageView.jsp
 * @author ojh
 * @since 2018.08.22
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.22    ojh        최초생성
 * 2019.05.28    kimht        include 수정
 **********************************************************************************************
--%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<%-- global --%>
	<jsp:include page="/WEB-INF/views/include/globalVar.jsp" flush="false" />
	<%-- head --%>
	<jsp:include page="/WEB-INF/views/include/incHead.jsp" flush="false" />
</head>
<body>
<!-- loader 시작-->
<div class="loader" onclick="loader_close()">
    <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
</div>
<!-- loader 끝-->
<div id="wrap">	
<header>
	<%-- header --%>
	<jsp:include page="/WEB-INF/views/include/incHeader.jsp" flush="false" />
</header>

    <!-- 레프트 메뉴 해당페이지 표시 -->
    <style>.${sessionScope.USER_MENU.menu_id}{ background: #316fe2; } </style>
    <!-- 레프트 메뉴 해당페이지 표시 끝 -->

<!-- dash_board 시작-->
<section id="dash_board">
    <!--side_menu 시작-->
	<jsp:include page="/WEB-INF/views/include/incMenu.jsp" flush="false" />
    <!--side_menu 끝-->

    <!--contents 시작-->
    <div class="contents mCustomScrollbar">
        <div class="page_title"><div class="page_name">${sessionScope.USER_MENU.menu_nm}</div><div class="page_location"><i class="fas fa-home"></i>${sessionScope.USER_MENU.mnu_path}</div></div>
        <div class="subject">
            <div class="page_contents">
                <div class="btn_excel"><button onClick="setAuthInfo()">적용</button></div>
                <div class="authmanage_box">
                <ul>
                    <li class="contents_list">
                            <div class="tt_line">사용자그룹 목록</div>
                            <div class="group_list mCustomScrollbar">
                                <ul id="groups">
                                    <li class="groupitem">사용자 그룹</li>
                                    <c:forEach items="${USER_GRP}" var="code">
                                    	<li id="item_${code.dtlCd}" class="groupitem"><a onClick="clickGrp('${code.dtlCd}')">${code.dtlCdNm}</a></li>
			                        </c:forEach>
                                </ul>
                            </div>
                    </li>
                    <li class="contents_list">
                        <div class="tt_line">메뉴목록</div>
                        <div class="menu_list mCustomScrollbar">
                        <form name="menuForm" id="menuForm" method="post">
                        <input type="hidden" name="USER_GRP_ID" id="grp_id" value=""/>
                            <ul id="menuRow">
                                <%--<li><div class="menu_checkbox"> <input type="checkbox" name="checkbox" ></div>메뉴명</li>
                                  <li class="menuitem deps1"><div class="menu_checkbox"> <input type="checkbox" name="checkbox" id="checkbox"></div>시스템관리</li>
                                <li class="menuitem deps2"><div class="menu_checkbox"> <input type="checkbox" name="checkbox" id="checkbox"></div><a href="#!">└ 사용자 관리</a><div class="deps_arrow"><img src="${ResRoot}/img/arrow_dark.png"></div></li>
                                <div class="deps3">
                                  <a href="#!"><div class="menu_checkbox"><input type="checkbox" name="checkbox" id="checkbox"></div>└ 사용자정보 관리</a>
                                  <a href="#!"><div class="menu_checkbox"><input type="checkbox" name="checkbox" id="checkbox"></div>└ 권한 관리</a>
                                </div>
                                <li class="menuitem deps1"><div class="menu_checkbox"> <input type="checkbox" id="checkbox"></div>업체 관리</li>
                                <li class="menuitem deps2"><div class="menu_checkbox"> <input type="checkbox" name="checkbox" id="checkbox"></div>└ 포인트사용처 관리</li>
                                <li class="menuitem deps1"><div class="menu_checkbox"> <input type="checkbox" name="checkbox" id="checkbox"></div>대사/정산 관리</li>
                                <li class="menuitem deps2"><div class="menu_checkbox"> <input type="checkbox" name="checkbox" id="checkbox"></div>└ 클립일대사 관리</li>
                                <li class="menuitem deps1"><div class="menu_checkbox"> <input type="checkbox" name="checkbox" id="checkbox"></div>연동이력 조회</li>
                                <li class="menuitem deps2"><div class="menu_checkbox"> <input type="checkbox" name="checkbox" id="checkbox"></div>└ 거래요청내역 조회</li>
                                <li class="menuitem deps2"><div class="menu_checkbox"> <input type="checkbox" name="checkbox" id="checkbox"></div>└ 시스템사용로그 조회</li>
                                <li class="menuitem deps2"><div class="menu_checkbox"> <input type="checkbox" name="checkbox" id="checkbox"></div>└ API연동이력 조회</li>
                                
                                <li class="menuitem deps1"><div class="menu_checkbox"> <input type="checkbox" name="checkbox" id="checkbox"></div>거래내역 조회</li>
                                <li class="menuitem deps2"><div class="menu_checkbox"> <input type="checkbox" name="checkbox" id="checkbox"></div>└ 거래내역 조회</li>
                                <li class="menuitem deps2"><div class="menu_checkbox"> <input type="checkbox" name="checkbox" id="checkbox"></div>└ 고객정보 조회</li> --%>
                         	</ul> 
                        </form>                        
                        </div>
                        </div>
                    </li>
                </ul>
				<br><br><br><br>
            </div>
            </div>

        </div>

        <!-- 스크롤 때문에 여백 있어야함 -->
        <br>
        <br>
        <br>
        <br>
        <br>

        </div>
    <!--contents 끝-->
</section>
<!-- dash_board 끝--> 

    <!-- footer 시작-->
    <jsp:include page="/WEB-INF/views/include/incFooter.jsp" flush="false" />
    <section class="footer">	 
    </section>
    <!-- footer 끝-->
    
    <%-- js --%>
	<jsp:include page="./authManageViewJs.jsp" flush="false" />
	
</body>
</html>