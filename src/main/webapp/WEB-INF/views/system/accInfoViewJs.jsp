<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
 **********************************************************************************************
 * @desc : 접근제어 관리 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/system/accInfoViewJs.jsp
 * @author ojh
 * @since 2018.08.23
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.23    ojh        최초생성
 * 
 **********************************************************************************************
--%>
<script type="text/javascript">
$(document).ready(function () {  
	//X버튼이나 취소 버튼 클릭시 팝업창 초기화
	$(".top_close").click(function () {
		resetPop();		
    });
    $(".btn_cancel").click(function () {
    	resetPop();
    });
});	
function resetPop(){
	//접근제어정보 등록 초기화
	$("#c_sel_ind").val("all");
	$("#c_sel_id").val("all");
	$("#c_sel_stat").val("all");
	//접근제어정보 수정 초기화
	$("#m_sel_ind").val("all");
	$("#m_sel_id").val("all");
	$("#m_in_id").html("");	
	$("#m_sel_stat").val("all");	
	$("#m_rgId").html("");
	$("#m_rgDt").html("");
	$("#m_mfId").html("");
	$("#m_mfDt").html("");		
	//접근 IP정보 등록 초기화
	$("#c_ip_dtl").val("");	
	$("#c_stat_dtl").val("all");
	$("#m_id_dtl").html("");
	$("#m_nm_dtl").html("");	
	//접근 IP정보 수정 초기화
	$("#m_id_dtl").html("");	
	$("#m_nm_dtl").html("");
	$("#m_ip_dtl").val("");	
	$("#m_stat_dtl").val("all");
	$("#m_rgId_dtl").html("");
	$("#m_rgDt_dtl").html("");
	$("#m_mfId_dtl").html("");
	$("#m_mfDt_dtl").html("");
}
//<<<<<<<<<<<<<<<<<<<<
//접근제어정보 검색 > 검색버튼 클릭 > html row 생성
function search(){
	var params = {
		CTRL_ID 		: $("#in_id").val()
		, RLTN_CORP_IND   	: $("#sel_ind").val()
		, CTRL_STAT 	: $("#sel_stat").val()
		, IP_ADDR 		: $("#in_ip").val()
	};
	PHFnc.ajax("${ViewRoot}/system/ajaxAccInfo.do",params,"POST","json",function(data, textStatus, jqXHR) {
		//Row 개수에 따라 td태그 생성하여 Html 변환
		$('#tBody').empty();
		for (var i=0; i<data.rows.length; i++) {
			var item = data.rows[i];
			$(	'<tr onclick="selectRow(this);">'+
					'<td>'+ item.no +'</td>'+
					'<td>'+ item.ctrl_id +'</td>'+
					'<td>'+ PHUtil.nvl(item.rltn_corp_ind_nm) +'</td>'+
					'<td style="text-align:left;">'+ PHUtil.nvl(item.rltn_corp_id_nm) +'</td>'+
					'<td>'+ PHUtil.nvl(item.ctrl_stat_nm) +'</td>'+
					'<td>'+ PHUtil.nvl(item.mdfy_user_nm) +'</td>'+
					'<td>'+ popDateValue(PHUtil.nvl(item.mdfy_dt)) +'</td>'+
					'<td><button onclick="modifyPopClick(this);">수정</button></td>'+
				'</tr>'
			).data('item',item).appendTo('#tBody');
		}
		//접근 IP 정보 초기화
		$("#tbodyDtl").html("");
		//접근 IP 등록 팝업 초기화
		$("#c_id_dtl").html("");
	},null,true,true,false);
}
//제휴사 명 가져오기
//제휴사 구분 선택 > 해당 구분의 제휴사 목록 가져오기 > Select 생성
function select_ind(type){
	var ind;
	if(type == "1"){
		ind = $("#c_sel_ind").val();
		$("#m_sel_ind").val(ind);
	} else if(type == 2){
		ind = $("#m_sel_ind").val();
		$("#c_sel_ind").val(ind);
	}
	var params = {
		RLTN_CORP_IND : ind
	};
	PHFnc.ajax("${ViewRoot}/system/ajaxGetCorpInd.do",params,"POST","json",function(data, textStatus, jqXHR) {
		var ind = $("#c_sel_ind").val();	
		$("#c_sel_id").empty();
		$("#m_sel_id").empty();
		for(var i=0; i<data.rows.length; i++) {
			var item = data.rows[i];
			if (i == 0) {
				$('<option value="all">선택</option>').appendTo('#c_sel_id,#m_sel_id');
			}
			var option;
			switch (ind) {
				case 'PG':
					option = '<option value="'+ item.pg_cmpn_id +'">'+ item.pg_cmpn_nm +'('+ item.pg_cmpn_id +')</option>';
					break;
				case 'CL':
					option = '<option value="'+ item.cprt_cmpn_id +'">'+ item.cprt_cmpn_nm +'('+ item.cprt_cmpn_id +')</option>';
					break;
				case 'PV':
					option = '<option value="'+ item.prvdr_id +'">'+ item.prvdr_nm +'('+ item.prvdr_id +')</option>';
					break;
				default:
					option = '';
			}
			$(option).appendTo('#c_sel_id,#m_sel_id');
		}	
		if(ind == "CS"){
			$('<option value="CS0001">클립서버</option>').appendTo('#c_sel_id,#m_sel_id');
		} 
		//수정과 등록 select 동시에 변경 처리
		//접근제어정보 수정에서 선택된 ID로 설정
		if($('#m_sel_id option[value='+selectedId+']').length != 0){
			$("#m_sel_id").val(selectedId);	
			$("#m_sel_id_old").val(selectedId);
		} else{
			$("#m_sel_id").val("all");	
		}
	},null,true,true,false);
}
//접근 제어 정보 등록
//등록 버튼 클릭 > 저장
//등록 팝업창 초기화
function createPopClick(){
	PHFnc.layerPopOpen(1);	
}
function createAccInfo(){
	if($("#c_sel_ind").val() == "all"){
		alert("제휴사 구분을 선택하세요.");
		return;
	}
	if($("#c_sel_id").val() == "all" || $("#c_sel_id").val() == null){
		alert("제휴사 구분을 선택 후, 제휴사 ID를 선택하세요.");
		return;
	}
	if($("#c_sel_stat").val() == "all"){
		alert("접근제어상태를 선택하세요.");
		return;
	}
	var conf = confirm("저장하시겠습니까?");	
	if(conf){
		var params = {
			RLTN_CORP_IND	: $("#c_sel_ind").val()
			, RLTN_CORP_ID  : $("#c_sel_id").val()
			, CTRL_STAT 	: $("#c_sel_stat").val()
			, ATHN_KEY		: $("#c_athn_key").val()
		};	
		PHFnc.ajax("${ViewRoot}/system/ajaxCreateAccInfo.do",params,"POST","json",
			function(data, textStatus, jqXHR) {
				alert("등록되었습니다.");
				$(".top_close").click();
				search();
			},
			function(data, textStatus, jqXHR) {
				if (!PHUtil.isEmpty(data.eMsg)) {
					PHFnc.alert(data.eMsg);	
				} else {
					PHFnc.ajaxError(null,jqXHR);
				}
				PHFnc.submitLoadingDestory();
			},
		true,true,false);
	}
}
//접근제어 정보 수정
//접근제어 정보 수정버튼 클릭 - 팝업창 오픈 및 상세 정보 가져오기 > 수정
//전역변수 selectedId
//팝업창이 열린후 선택된 구분에 따른 제휴사 ID 비동기ajax 로딩 완료후 가져오기 위해 변수에 아이디 저장
var selectedId;
function modifyPopClick(o) {
	var item = $(o).parents('tr:eq(0)').data('item');
	selectedId = item.rltn_corp_id;
	$("#m_sel_ind").val(item.rltn_corp_ind);
	select_ind(2);
	$("#m_in_id").html(item.ctrl_id);	
	$("#m_sel_stat").val(PHUtil.nvl(item.ctrl_stat));
	$('#m_athn_key').val(PHUtil.nvl(item.athn_key));
	$("#m_rgId").html(PHUtil.nvl(item.rgst_user_nm));
	$("#m_rgDt").html(PHUtil.nvl(item.rgst_dt));
	$("#m_mfId").html(PHUtil.nvl(item.mdfy_user_nm));
	$("#m_mfDt").html(PHUtil.nvl(item.mdfy_dt));
	PHFnc.layerPopOpen(2)
}
//접근 제어 정보 수정
function modifyAccInfo(){
	if($("#m_sel_ind").val() == "all"){
		alert("제휴사 구분을 선택하세요.");
		return;
	}
	if($("#m_sel_id").val() == "all" || $("#m_sel_id").val() == null){
		alert("제휴사 구분을 선택 후, 제휴사 ID를 선택하세요.");
		return;
	}
	if($("#m_sel_stat").val() == "all"){
		alert("접근제어상태를 선택하세요.");
		return;
	}
	var conf = confirm("저장하시겠습니까?");	
	if(conf){
		var params = {
			RLTN_CORP_IND	: $("#m_sel_ind").val()
			, RLTN_CORP_ID  : $("#m_sel_id").val()
			, RLTN_CORP_ID_OLD   : $("#m_sel_id_old").val()
			, CTRL_STAT 	: $("#m_sel_stat").val()
			, CTRL_ID 		: $("#m_in_id").html()
			, ATHN_KEY		: $("#m_athn_key").val()
		};	
		PHFnc.ajax("${ViewRoot}/system/ajaxModifyAccInfo.do",params,"POST","json",
			function(data, textStatus, jqXHR) {
				alert("수정되었습니다.");
				$(".top_close").click();
				search();
			},
			function(data, textStatus, jqXHR) {
				if (!PHUtil.isEmpty(data.eMsg)) {
					PHFnc.alert(data.eMsg);	
				} else {
					PHFnc.ajaxError(null,jqXHR);
				}
				PHFnc.submitLoadingDestory();
			},
		true,true,false);
	}
}
//접근제어 상세정보 검색
//접근제어 정보 선택 > 해당 ID의 상세 정보 가져오기 > html row생성
function selectRow(o){
	var row = $(o).data('item');
	//선택시 tBody 이하 tr 해제
	//선택한 row 표시
	$('#tBody tr').removeAttr('class');
	$(o).attr('class','bgon');
	//상세정보 등록창에 데이터 전달
	$("#c_id_dtl").html(row.ctrl_id);
	$("#c_nm_dtl").html(PHUtil.nvl(row.rltn_corp_id_nm));
	var params = {
		CTRL_ID	: row.ctrl_id
	};	
	PHFnc.ajax("${ViewRoot}/system/ajaxAccInfoDtl.do",params,"POST","json",function(data, textStatus, jqXHR) {
		//접근 IP 정보 리스트 생성
		$('#tbodyDtl').empty();
		for(var i=0; i<data.rows.length; i++) {
			var item = data.rows[i];
			$(	'<tr>'+
					'<td>'+ item.no +'</td>'+
					'<td>'+ item.ip_addr +'</td>'+
					'<td>'+ PHUtil.nvl(item.ip_stat_nm) +'</td>'+
					'<td>'+ PHUtil.nvl(item.mdfy_user_nm) +'</td>'+
					'<td>'+ popDateValue(PHUtil.nvl(item.mdfy_dt)) +'</td>'+
					'<td><button onclick="modifyPopDtlClick(this);">수정</button></td>'+
				'</tr>'
			).data('item',item).appendTo('#tbodyDtl');
		}
	},null,true,true,false);
}
//접근 IP정보 등록
//접근제어정보 선택 후 등록버튼 클릭 > 등록
function createPopDtlClick(){
	if($("#c_id_dtl").html() == ""){
		alert("접근제어정보 선택 후 클릭해주세요.")
		return;
	}
	$("#c_sel_ind").val("all");
	$("#c_sel_id").val("all");
	$("#c_sel_stat").val("all");	
	PHFnc.layerPopOpen(3);
}
function createAccInfoDtl(){
	if($("#c_ip_dtl").val() == ""){
		alert("접근가능IP를 입력해 주세요.");
		return;
	}
	if(checkIpValidate($("#c_ip_dtl").val())){
		return;
	}
	if($("#c_stat_dtl").val() == "all"){
		alert("IP상태를 선택해 주세요.");
		return;
	}	
	var conf = confirm("저장하시겠습니까?");	
	if(conf){
		var params = {
			CTRL_ID	: $("#c_id_dtl").html()
			, IP_ADDR  : $("#c_ip_dtl").val()
			, IP_STAT 	: $("#c_stat_dtl").val()
		};	
		PHFnc.ajax("${ViewRoot}/system/ajaxCreateAccInfoDtl.do",params,"POST","json",
			function(data, textStatus, jqXHR) {
				alert("등록되었습니다.");
				$(".top_close").click();
				var dtlId = $.trim($("#c_id_dtl").html());
				jQuery.each($('#tBody').find('tr'), function(i,o) {
					if ($(o).data('item').ctrl_id == dtlId) {
						selectRow(o);
						return false;
					}
				});
			},
			function(data, textStatus, jqXHR) {
				if (!PHUtil.isEmpty(data.eMsg)) {
					PHFnc.alert(data.eMsg);	
				} else {
					PHFnc.ajaxError(null,jqXHR);
				}
				PHFnc.submitLoadingDestory();
			},
		true,true,false);
	}	
}
//접근제어정보 선택 후 접근 IP정보 수정버튼 클릭
//seq 저장 (수정시 사용)
var ctrl_seq;
function modifyPopDtlClick(o){
	var item = $(o).parents('tr:eq(0)').data('item');
	ctrl_seq = item.ctrl_seq;
	$("#m_id_dtl").html(item.ctrl_id);
	$("#m_nm_dtl").html(PHUtil.nvl(item.rltn_corp_id_nm));	
	$("#m_ip_dtl").val(PHUtil.nvl(item.ip_addr));
	$("#m_ip_dtl_old").val(PHUtil.nvl(item.ip_addr));
	$("#m_stat_dtl").val(PHUtil.nvl(item.ip_stat));
	$("#m_rgId_dtl").html(PHUtil.nvl(item.rgst_user_nm));
	$("#m_rgDt_dtl").html(PHUtil.nvl(item.rgst_dt));
	$("#m_mfId_dtl").html(PHUtil.nvl(item.mdfy_user_nm));
	$("#m_mfDt_dtl").html(PHUtil.nvl(item.mdfy_dt));
	PHFnc.layerPopOpen(4);
}
//접근 IP 정보 수정
function modifyAccInfoDtl(){
	if($("#m_ip_dtl").val() == ""){
		alert("접근가능IP를 입력해 주세요.");
		return;
	}
	if(checkIpValidate($("#m_ip_dtl").val())){
		return;
	}
	if($("#m_stat_dtl").val() == "all"){
		alert("IP상태를 선택해 주세요.");
		return;
	}	
	var conf = confirm("저장하시겠습니까?");	
	if(conf){
		var params = {
			IP_ADDR		: $("#m_ip_dtl").val()
			, IP_ADDR_OLD: $("#m_ip_dtl_old").val()
			, IP_STAT  	: $("#m_stat_dtl").val()
			, CTRL_ID 	: $("#m_id_dtl").html()
			, CTRL_SEQ 	: ctrl_seq
		};	
		PHFnc.ajax("${ViewRoot}/system/ajaxModifyAccInfoDtl.do",params,"POST","json",
			function(data, textStatus, jqXHR) {
				alert("수정되었습니다.")
				$(".top_close").click();
				var dtlId = $.trim($("#m_id_dtl").html());
				jQuery.each($('#tBody').find('tr'), function(i,o) {
					if ($(o).data('item').ctrl_id == dtlId) {
						selectRow(o);
						return false;
					}
				});
			},
			function(data, textStatus, jqXHR) {
				if (!PHUtil.isEmpty(data.eMsg)) {
					PHFnc.alert(data.eMsg);	
				} else {
					PHFnc.ajaxError(null,jqXHR);
				}
				PHFnc.submitLoadingDestory();
			},
		true,true,false);
	}
}
//IP 형식이 올바르지 않은지 판별, 정규식에 맞지 않으면 TRUE, 올바른 형식 FALSE
function checkIpValidate(ip){
	 var filter = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/;

	 if (filter.test(ip) == true){
	  return false;
	 } else{
	  alert("IP 주소 형식이 틀립니다.");
	  return true;
	 }
}
//날짜 개행 표시
function popDateValue(str) {
	return str.replace(' ','<br>');
}
 
</script>