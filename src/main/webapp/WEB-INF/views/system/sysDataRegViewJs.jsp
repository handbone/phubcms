<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
 **********************************************************************************************
 * @desc : 시스템데이터반영 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/system/sysDataRegViewJs.jsp
 * @author ojh
 * @since 2018.10.18
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.18    ojh        최초생성
 * 
 **********************************************************************************************
--%>
<script type="text/javascript">
$(document).ready(function () { 	

});	

function mdfy_sysData(type){
	var conf = confirm("반영하시겠습니까?");
	if (conf) {
		var params = {
			DATA_TYPE : type
		};
		PHFnc.ajax("${ViewRoot}/system/ajaxModifySysDataReg.do",params,"POST","json",successModifyAjax,null,true,true,false);
	}	
}
function successModifyAjax(data, textStatus, jqXHR){
	alert("처리되었습니다.");	
}
 
</script>