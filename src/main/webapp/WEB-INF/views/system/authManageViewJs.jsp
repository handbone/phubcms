<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
 **********************************************************************************************
 * @desc : 권한관리 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/system/authManageViewJs.jsp
 * @author ojh
 * @since 2018.08.24
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.24    ojh        최초생성
 * 
 **********************************************************************************************
--%>
<script type="text/javascript">
$(document).ready(function () {  	
	clickGrp("ADM");
});	
//사용자 그룹 선택시 
function clickGrp(id){

	$("#grp_id").val(id);
	
	//다른 그룹 해제, 선택한 그룹 on 으로 class 변경
	$("#groups li").attr('class','groupitem');
	$("#item_"+id).attr('class','groupitem_on'); 
	
	var params = {
		USER_GRP_ID : id
	}
	PHFnc.ajax("${ViewRoot}/system/ajaxGetMenuInfo.do",params,"POST","json",successAjax,null,true,true,false);	
}
//메뉴 목록 가져오고 등록된 권한 체크 하기
function successAjax(data, textStatus, jqXHR){
	
	var hText = "";		
	hText += "<li><div class=\"menu_checkbox\"> <input type=\"checkbox\" id=\"checkbox_all\" onChange=\"check_all()\"></div>메뉴명</li>";
	
 	for(var i=0; i<data.rows.length; i++){
		if(data.rows[i].menu_lvl != 3){
			hText += "<li class=\"menuitem deps"+data.rows[i].menu_lvl+"\"><a href=\"#!\"><div class=\"menu_checkbox\">";
			hText += "<input type=\"checkbox\" name=\""+data.rows[i].menu_id+"\" class=\""+data.rows[i].upr_menu_id+"\""; 
			
			if(data.rows[i].menu_lvl == 1){
				hText += " onChange=\"checkBox(this.name)\" ";
			} else if(data.rows[i].menu_lvl == 2){
				hText += " onChange=\"checkBox2(this.name)\" ";
			}
			
			if(data.rows[i].read_yn =="Y"){
				hText += " checked></div>";
			} else{
				hText += "></div>";
			}
			if(data.rows[i].menu_lvl == "2" && data.rows[i].child =="Y"){
				hText += "└ "+data.rows[i].menu_nm+"</a></li><div class=\"deps3\">";	
			} else{
				if(data.rows[i].menu_lvl != "1"){
					hText += "└ ";
				}
				hText += data.rows[i].menu_nm+"</a></li>";
			}
		} else{
			hText += "<a href=\"#!\"><div class=\"menu_checkbox\"><input type=\"checkbox\" onChange=\"checkBox3(this.name)\" name=\""+data.rows[i].menu_id+"\" class=\""+data.rows[i].upr_menu_id+"\"";
			
			
			if(data.rows[i].read_yn =="Y"){
				hText += " checked></div>";
			} else{
				hText += "></div>";
			}
			hText += "└ "+data.rows[i].menu_nm+"</a>";
			if(data.rows[i].menu_lvl == "3" && data.rows[i].last =="Y"){
				hText += "</div>";
			}
		}
	}
	$("#menuRow").html(hText);
	$(".deps3").show();
}

//체크박스 전체 선택, 전체 해제
function check_all(){
	if($("#checkbox_all").is(':checked')){
		$("input[type=checkbox]").prop("checked",true);		
	} else{
		$("input[type=checkbox]").prop("checked",false);
	}
}

function checkBox(name){
	
 	if( $("input[name="+name+"]").is(':checked') ){		
		$("."+name).prop("checked", "checked").change();
	} else{
		$("."+name).removeProp("checked").change();
	}	
}
function checkBox2(name){
	var cls = $("input[name="+name+"]").attr('class').split(" ");
  	if( $("input[name="+name+"]").is(':checked') ){		
 		$("input[name="+cls[0]+"]").prop("checked", "checked");
	}
  	checkBox(name);
}
function checkBox3(name){
	var cls = $("input[name="+name+"]").attr('class').split(" ");
  	if( $("input[name="+name+"]").is(':checked') ){		
 		$("input[name="+cls[0]+"]").prop("checked", "checked");
 		
 		var cls2 = $("input[name="+cls[0]+"]").attr('class').split(" "); 		
 		if( $("input[name="+cls[0]+"]").is(':checked')){
 			$("input[name="+cls2[0]+"]").prop("checked", "checked");
 		}
 		
	}
}
//체크된 메뉴 권한 추가하기
function setAuthInfo(){	
	var conf = confirm("적용하시겠습니까?");	
	if(conf){
		$("#menuForm").ajaxForm({
			url  : '<c:url value="/system/ajaxSetMenuInfo.do" />',
			type : 'POST',
			dataType : 'json',
			success : function(data, textStatus, jqXHR){
				if (data.eCode === "0") {
					alert("적용되었습니다.")
				} 
			},
	   		error : function(data, textStatus, jqXHR){
				alert(data.eMsg);
			}
		});
		$("#menuForm").submit();
	}
}
 
</script>