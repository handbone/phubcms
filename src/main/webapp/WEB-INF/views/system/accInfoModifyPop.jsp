<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%>
<%--
 **********************************************************************************************
 * @desc : 접근제어 정보 수정 팝업
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/system/accInfoModifyPop.jsp
 * @author ojh
 * @since 2018.08.23
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.23    ojh        최초생성
 *
 **********************************************************************************************
 accInfoView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>
<div class="popup_content2">
	<div class="popup_title">접근제어정보 수정<a href="#!"><div class="top_close"></div></a></div>
	<table class="half_table" border="0" cellpadding="0" cellspacing="0">
		<tbody>
            <tr>
                <th style="width:200px;">제휴사구분 <span class="red">*</span></th>
                <td style="width:300px;">
                    <select id="m_sel_ind" onchange="select_ind(2)" style="width:180px;">
                        <option value="all">선택</option>
                        <c:forEach items="${CMPN_IND}" var="code">
                    		<option value="${code.dtlCd}">${code.dtlCdNm}</option>
                   		</c:forEach>
                    </select>
                </td>
                <th style="width:200px;">제휴사명 <span class="red">*</span></th>
                <td>
                    <select id="m_sel_id" style="width:180px;">
                        <option value="all">선택</option>
                    </select>
                    <input type="hidden" id="m_sel_id_old"/>
                </td>
            </tr>
            <tr>
                <th>접근제어ID <span class="red">*</span></th>
                <td id="m_in_id"></td>
                <th>접근제어상태 <span class="red">*</span></th>
                <td>
                    <select id="m_sel_stat" style="width:180px;">
                        <option value="all">선택</option>
                        <c:forEach items="${CTRL_IND}" var="code">
                    		<option value="${code.dtlCd}">${code.dtlCdNm}</option>
                    	</c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
            	<th>인증키</th>
            	<td colspan="3">
            		<input type="text" name="name" id="m_athn_key" style="width:490px;" maxlength="50">
            	</td>
            </tr>
            <tr>
                <th>등록자</th>
                <td id="m_rgId"></td>
                <th>등록일시</th>
                <td id="m_rgDt"></td>
            </tr>
            <tr>
                <th>수정자</th>
                <td id="m_mfId"></td>
                <th>수정일시</th>
                <td id="m_mfDt"></td>
            </tr>
		</tbody>
	</table>
	<div class="btnalign">
		<ul>
			<li><a href="#!"><div class="btn_cancel">취소</div></a></li>
			<li><a href="#!"><div class="btn_save" onClick="modifyAccInfo()">저장</div></a></li>
		</ul>
	</div>
</div>