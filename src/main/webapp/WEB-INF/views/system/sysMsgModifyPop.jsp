<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%>
<%--
 **********************************************************************************************
 * @desc : 시스템메시지 수정 팝업
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/system/sysMsgModifyPop.jsp
 * @author bmg
 * @since 2018.09.14
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.14    bmg        최초생성
 *
 **********************************************************************************************
 sysMsgView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>
<!-- 시스템메시지 수정 시작-->
<div class="popup_content2">
	<div class="popup_title">시스템메시지 수정<a href="#!"><div class="top_close"></div></a></div>
	<table class="half_table" border="0" cellpadding="0" cellspacing="0">
		<tbody>
			<tr>
			    <th>메시지 ID</th>
			    <td colspan="3"><input type="text" name="m_msg_id" style="background-color:lightgray;" readonly></td>
			</tr>
			<tr>
				<th>메시지명  <span class="red">*</span></th>
			    <td colspan="3"><input type="text" name="m_msg_nm" style="width:540px;" maxlength="100"></td>
			</tr>
			<tr>
			    <th style="width:200px;">메시지그룹 <span class="red">*</span></th>
			    <td style="width:350px;"><input type="text" name="m_msg_grp_nm" style="background-color:lightgray;" readonly></td>
			    <th style="width:200px;">메시지구분 <span class="red">*</span></th>
			    <td><input type="text" name="m_msg_ind_nm" style="background-color:lightgray;" readonly></td>
			</tr>
			<tr>
			    <th>메시지코드  <span class="red">*</span></th>
			    <td><input type="text" name="m_msg_cd" style="background-color:lightgray;" readonly></td>
			    <th>사용여부 </th>
			    <td>
			        <input type="radio" name="m_use_yn" value="Y"> 사용
			        <input type="radio" name="m_use_yn" value="N"> 미사용
			    </td>
			</tr>
			<tr>
			    <th>비고</th>
			    <td colspan="3"> <textarea id="m_rmrk" rows="5" maxlength="100"></textarea></td>
			</tr>
			<tr>
			    <th>등록자</th>
			    <td id="m_rgst_id_nm"></td>
			    <th>등록일시</th>
			    <td id="m_rgst_dt"></td>
			</tr>
			<tr>
			    <th>수정자</th>
			    <td id="m_mdfy_id_nm"></td>
			    <th>수정일시</th>
			    <td id="m_mdfy_dt"></td>
			</tr>
		</tbody>
	</table>
	<div class="btnalign">
	    <ul>
	        <li><a href="#!"><div class="btn_cancel">취소</div></a></li>
	        <li><a href="#!"><div class="btn_save" onclick="modifySysMsg();">저장</div></a></li>
	    </ul>
	</div>
</div>