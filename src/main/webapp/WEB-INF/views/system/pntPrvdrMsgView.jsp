<%@  page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
 **********************************************************************************************
 * @desc : 연동시스템 응답코드 정의
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/system/pntPrvdrMsgView.jsp
 * @author jungukjae
 * @since 2019.06.17
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일      수 정 자        		수정내용
 * ----------   ----------   -----------------------------
 * 2019.06.17    jungukjae        		최초생성
 **********************************************************************************************
--%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<%-- global --%>
	<jsp:include page="/WEB-INF/views/include/globalVar.jsp" flush="false" />
	<%-- head --%>
	<jsp:include page="/WEB-INF/views/include/incHead.jsp" flush="false" />
	<style type="text/css">
      .btnalign ul li #btn_delete {
        width: 150px;
        padding: 10px;
        color: #fff;
        font-weight: 600;
        font-size: 16px;
        text-align: center;
        background: #3c3c3c; }
	</style> 
</head>
<body>
<!-- loader 시작-->
<div class="loader" onclick="loader_close()">
    <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
</div>
<!-- loader 끝-->
<div id="wrap">	
<header>
	<%-- header --%>
	<jsp:include page="/WEB-INF/views/include/incHeader.jsp" flush="false" />
</header>
    <!-- 레프트 메뉴 해당페이지 표시 -->
    <style>.${sessionScope.USER_MENU.menu_id}{ color: #fff !important; background: #3f4759 !important; border-left: 3px solid #fff; cursor: hand;} </style>
    <!-- 레프트 메뉴 해당페이지 표시 끝 -->
	
	<!-- 연동시스템 응답코드 등록팝업 -->
	<jsp:include page="./pntPrvdrMsgCreatePop.jsp" flush="false" /> 

	
	<!-- 연동시스템 응답코드 수정팝업 -->
	<jsp:include page="./pntPrvdrMsgModifyPop.jsp" flush="false" />
	
	<!-- dash_board 시작-->
	<section id="dash_board">
    <!--side_menu 시작-->
	<jsp:include page="/WEB-INF/views/include/incMenu.jsp" flush="false" />
    <!--side_menu 끝-->
	
	<!--contents 시작-->
    <div class="contents mCustomScrollbar">
        <div class="page_title"><div class="page_name">${sessionScope.USER_MENU.menu_nm}</div><div class="page_location"><i class="fas fa-home"></i>${sessionScope.USER_MENU.mnu_path}</div></div>
        <div class="subject">
            <div class="page_search">
                <table class="search_tb" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="search_item">
                            <ul>
                                <li>연동시스템ID <input type="text" name="prvdr_id" id="prvdr_id"></li>
                                <li>포인트코드 <input type="text" name="pnt_cd" id="pnt_cd"></li>
                                <li>응답코드 <input type="text" name="msg_id" id="msg_id"></li>
                                <li>응답메세지 <input type="text" name="msg" id="msg"></li>
                                <li>참조값1 <input type="text" name="rfrn_val1" id="rfrn_val1"></li>
                            </ul>
                        </td>
                        <td class="btn_search">
                            <button type="button" onclick="search();">조회</button>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="page_contents">
                <div class="btn_excel"><button id="btn_excel">엑셀다운</button> <button onclick="createPntPrvdrMsgPopClick();">등록</button></div>
                <div id="content">																
                    <table id="jqGrid"></table>
                    <div id="jqGridPager"></div>
                </div>
				<div id="jqGridExcelDiv" style="display:none">
                	<table id="jqGridExcel" ></table>
                </div>
                <!-- jqGrid  끝 -->
            </div>
        </div>

        <!-- 스크롤 때문에 여백 있어야함 -->
        <br>
        <br>
        <br>
        <br>
        <br>

    </div>
    <!--contents 끝-->
    
</section>
<!-- dash_board 끝--> 

    <!-- footer 시작-->
    <jsp:include page="/WEB-INF/views/include/incFooter.jsp" flush="false" />
    <section class="footer">	 
    </section>
    <!-- footer 끝-->
    <%-- js --%> 
	<jsp:include page="./pntPrvdrMsgViewJs.jsp" flush="false" />
	
</body>
</html>