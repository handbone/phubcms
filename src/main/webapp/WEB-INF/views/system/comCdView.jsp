<%@  page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
 **********************************************************************************************
 * @desc : 공통코드 관리
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/system/comCdView.jsp
 * @author bmg
 * @since 2018.09.11
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.07    bmg        최초생성
 * 2019.05.28    kimht        include 수정
 **********************************************************************************************
--%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<%-- global --%>
	<jsp:include page="/WEB-INF/views/include/globalVar.jsp" flush="false" />
	<%-- head --%>
	<jsp:include page="/WEB-INF/views/include/incHead.jsp" flush="false" />
</head>
<body>
<!-- loader 시작-->
<div class="loader" onclick="loader_close()">
    <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
</div>
<!-- loader 끝-->
<div id="wrap">	
<header>
	<%-- header --%>
	<jsp:include page="/WEB-INF/views/include/incHeader.jsp" flush="false" />
</header>
    <!-- 레프트 메뉴 해당페이지 표시 -->
    <style>.${sessionScope.USER_MENU.menu_id}{ color: #fff !important; background: #3f4759 !important; border-left: 3px solid #fff; cursor: hand;} </style>
    <!-- 레프트 메뉴 해당페이지 표시 끝 -->

	<!-- 공통코드 등록 시작-->
    <jsp:include page="./comCdCreatePop.jsp" flush="false" />
    
	<!-- 공통코드 수정 시작-->
    <jsp:include page="./comCdModifyPop.jsp" flush="false" />
    
    <!-- 상세코드 등록 시작-->
    <jsp:include page="./dtlCdCreatePop.jsp" flush="false" />
    
	<!-- 상세코드 수정 시작-->
    <jsp:include page="./dtlCdModifyPop.jsp" flush="false" />

<!-- dash_board 시작-->
<section id="dash_board">
    <!--side_menu 시작-->
	<jsp:include page="/WEB-INF/views/include/incMenu.jsp" flush="false" />
    <!--side_menu 끝-->

    <!--contents 시작-->
    <div class="contents mCustomScrollbar">
        <div class="page_title"><div class="page_name">${sessionScope.USER_MENU.menu_nm}</div><div class="page_location"><i class="fas fa-home"></i>${sessionScope.USER_MENU.mnu_path}</div></div>
        <div class="subject">
            <div class="page_search"> 
                <table class="search_tb" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="search_item">
                            <ul>
                                <li>그룹코드
                                    <select id="grp_cd">
                                        <option value="all">전체</option>
                                        <c:forEach items="${COM_GRP}" var="code">
	                           				<option value="${code.dtlCd}">${code.dtlCdNm}</option>
	                           			</c:forEach>
                                    </select>
                                </li>
                                <li>구분코드 <input type="text" name="ind_cd" maxlength="20" style="text-transform:uppercase;"></li>
                                <li>구분코드명 <input type="text" name="ind_cd_nm" maxlength="30"></li>
                                <li>구분코드사용여부
                                    <select id="com_use_yn">
                                        <option value="all">전체</option>
                                        <option value="Y">사용</option>
                                        <option value="N">미사용</option>
                                    </select>
                                </li>
							</ul>
							<ul>
								<li style="width:152.92px;" />
                                <li>상세코드 <input type="text" name="dtl_cd" maxlength="20"></li>
                                <li>상세코드명 <input type="text" name="dtl_cd_nm" maxlength="20"></li>
                                <li>상세코드사용여부
                                    <select id="dtl_use_yn">
                                        <option value="all">전체</option>
                                        <option value="Y">사용</option>
                                        <option value="N">미사용</option>
                                    </select>
                                </li>
                            </ul>
                        </td>
                        <td class="btn_search">
                            <button type="button" onclick="search();">조회</button>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="page_contents">
                <ul>
                    <li class="contents_left">
                        <div class="tt_line">구분코드<button onclick="comCdCreatePopClick();">등록</button></div>
                        <div class="con_table mCustomScrollbar">
                        <table id="comCdTable" class="cord_table" border="0" cellpadding="0" cellspacing="0" style="table-layout:fixed">
                        	<thead>
                        		<tr>
	                                <th style="width:50px;">NO</th>
	                                <th style="width:50px;">그룹<br>코드</th>
	                                <th style="width:120px;">구분코드</th>
	                                <th>구분코드명</th>
	                                <th style="width:65px;">사용<br>여부</th>
	                                <th style="width:60px;">수정자</th>
	                                <th>수정일시</th>
	                                <th style="width:70px;">관리</th>
	                            </tr>
                        	</thead>
                            <tbody />
                        </table>
                        </div>
                    </li>
                    <li class="contents_right">
                        <div class="tt_line">상세코드<button onclick="createDtlCdPopClick();">등록</button></div>
                        <div class="con_table mCustomScrollbar">
                        <table id="dtlCdTable" class="cord_table" border="0" cellpadding="0" cellspacing="0" style="table-layout:fixed">
                        	<thead>
                        		<tr>
	                                <th style="width:50px;">NO</th>
	                                <th>상세코드</th>
	                                <th>상세코드명</th>
	                                <th style="width:65px;">사용<br>여부</th>
	                                <th style="width:60px;">수정자</th>
	                                <th>수정일시</th>
	                                <th style="width:70px;">관리</th>
	                            </tr>
                        	</thead>
                            <tbody />
                        </table>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <!-- 스크롤 때문에 여백 있어야함 -->
        <br>
        <br>
        <br>
        <br>
        <br>

        </div>
        <!--contents 끝-->
</section>
<!-- dash_board 끝--> 

    <!-- footer 시작-->
    <jsp:include page="/WEB-INF/views/include/incFooter.jsp" flush="false" />
    <section class="footer">	 
    </section>
    <!-- footer 끝-->
    
    <%-- js --%>
	<jsp:include page="./comCdViewJs.jsp" flush="false" />
	
</body>
</html>