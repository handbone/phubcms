<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%>
<%--
 **********************************************************************************************
 * @desc : 사용자 정보 수정 팝업
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/system/userInfoModifyPop.jsp
 * @author ojh
 * @since 2018.08.22
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.22    ojh        최초생성
 *
 **********************************************************************************************
 userInfoView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>
          <div class="popup_content2">
        <div class="popup_title">사용자정보 수정<a href="#!"><div class="top_close"></div></a></div>
        <table class="half_table" border="0" cellpadding="0" cellspacing="0">
            <tbody>
            <tr>
                <th>사용자ID <span class="red">*</span><br> <button id="btn_un" onClick="modifyErrCnt()">계정잠금 해제</button></th>
                <td id="m_in_id" ></td>
                <th>사용자명  <span class="red">*</span></th>
                <td><input type="text" name="name" id="m_in_nm" maxlength="40"> </td>
            </tr>
            <tr>
                <th>사용자그룹 <span class="red">*</span></th>
                <td>
                    <select id="m_sel_grp">
                        <option value="all">선택</option>
                        <c:forEach items="${USER_GRP}" var="code">
			            	<option value="${code.dtlCd}">${code.dtlCdNm}</option>
			            </c:forEach>
                    </select>
                </td>
                <th>사용자상태 <span class="red">*</span></th>
                <td>
                    <select id="m_sel_stat">
                        <option value="all">선택</option>
                        <c:forEach items="${STAT_CD}" var="code">
			            	<option value="${code.dtlCd}">${code.dtlCdNm}</option>
			            </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <th>연락처  <span class="red">*</span></th>
                <td><input type="text" name="name" size="5" id="m_in_cnt1" onKeyDown="return onlyNum(event)" onKeyUp="return keyReplace(this)">
                 - <input type="text" name="name" size="5" id="m_in_cnt2" onKeyDown="return onlyNum(event)" onKeyUp="return keyReplace(this)"> 
                 - <input type="text" name="name" size="5" id="m_in_cnt3" onKeyDown="return onlyNum(event)" onKeyUp="return keyReplace(this)"></td>
                <th>부서코드  <span class="red">*</span></th>
                <td>
                    <select id="m_sel_dept">
                        <option value="all">선택</option>
                        <c:forEach items="${USER_DEPT}" var="code">
			            	<option value="${code.dtlCd}">${code.dtlCdNm}</option>
			            </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <th>IP주소  <span class="red">*</span></th>
                <td><input type="text" name="name" id="m_in_ip" onKeyDown="return onlyNum(event,'ip')" onKeyUp="return keyReplace(this)"></td>
                <th>사용가능일자 </th>
                <td><input type="text" name="name" size="10" id="m_efct_start_date" readonly> ~ <input type="text" name="name" size="10" id="m_efct_end_date" readonly></td>
            </tr>
            <tr>
            	<th>최대조회수 <span class="red">*</span></th>
            	<td colspan="3">
            		<input type="text" name="name" size="5" id="m_maxInqrCnt" onKeyDown="return onlyNum(event)" onKeyUp="return keyReplace(this)" autocomplete="off">
            	</td>
            </tr>
            <tr>
                <th>최근접속일시</th>
                <td id="m_last_dt"></td>
                <th>로그인오류횟수</th>
                <td id="m_err_cnt"></td>
            </tr>
            <tr>
                <th>등록자</th>
                <td id="m_rgst_user"></td>
                <th>등록일시</th>
                <td id="m_rgst_dt"></td>
            </tr>
            <tr>
                <th>수정자</th>
                <td id="m_mdfy_user"></td>
                <th>수정일시</th>
                <td id="m_mdfy_dt"></td>
            </tr>
            </tbody>
        </table>

        <div class="btnalign">
            <ul>
                <li><a href="#!"><div class="btn_cancel">취소</div></a></li>
                <li><a href="#!"><div class="btn_save" onClick="modifyPntUseCmpn()">저장</div></a></li>
            </ul>
        </div>
    </div>
    <div id="myPopup" class="popup">
    </div>