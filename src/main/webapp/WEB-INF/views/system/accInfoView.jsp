<%@  page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
 **********************************************************************************************
 * @desc : 접근제어 관리
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/system/accInfoView.jsp
 * @author ojh
 * @since 2018.08.23
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용mCustomScrollbar
 * ----------   --------   -----------------------------
 * 2018.08.23    ojh        최초생성
 * 2019.05.28    kimht        include 수정
 **********************************************************************************************
--%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<%-- global --%>
	<jsp:include page="/WEB-INF/views/include/globalVar.jsp" flush="false" />
	<%-- head --%>
	<jsp:include page="/WEB-INF/views/include/incHead.jsp" flush="false" />
</head>
<body>
<!-- loader 시작-->
<div class="loader" onclick="loader_close()">
    <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
</div>
<!-- loader 끝-->
<div id="wrap">	
<header>
	<%-- header --%>
	<jsp:include page="/WEB-INF/views/include/incHeader.jsp" flush="false" />
</header>

    <!-- 레프트 메뉴 해당페이지 표시 -->
    <style>.${sessionScope.USER_MENU.menu_id}{ color: #fff !important; background: #3f4759 !important; border-left: 3px solid #fff; cursor: hand;} </style>
    <!-- 레프트 메뉴 해당페이지 표시 끝 -->

    <!-- 접근제어 정보 등록 시작-->
    <jsp:include page="./accInfoCreatePop.jsp" flush="false" />

    <!-- 접근제어 정보 수정 시작-->
	<jsp:include page="./accInfoModifyPop.jsp" flush="false" />
	
	<!-- 접근제어 상세 정보 등록 시작-->
	<jsp:include page="./accInfoDtlCreatePop.jsp" flush="false" />
	
	<!-- 접근제어 상세 정보 수정 시작-->
	<jsp:include page="./accInfoDtlModifyPop.jsp" flush="false" />

<!-- dash_board 시작-->
<section id="dash_board">
    <!--side_menu 시작-->
	<jsp:include page="/WEB-INF/views/include/incMenu.jsp" flush="false" />
    <!--side_menu 끝-->

   <!--contents 시작-->
        <div class="contents mCustomScrollbar">
	        <div class="page_title"><div class="page_name">${sessionScope.USER_MENU.menu_nm}</div><div class="page_location"><i class="fas fa-home"></i>${sessionScope.USER_MENU.mnu_path}</div></div>
            <div class="subject">
                <div class="page_search">
                <form name="searchForm" id="searchForm" method="POST">
                	     <table class="search_tb" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="search_item">
                                <ul>
                                    <li class="sname">접근제어ID <input type="text" name="name" id="in_id"></li>
                                    <li class="sname">제휴사구분
                                        <select id="sel_ind">
                                            <option value="all">전체</option>
                                            <c:forEach items="${CMPN_IND}" var="code">
			                           			<option value="${code.dtlCd}">${code.dtlCdNm}</option>
			                           		</c:forEach>
                                        </select>
                                    </li>
                                    <li class="sname">접근제어상태
                                        <select id="sel_stat">
                                            <option value="all">전체</option>
                                            <c:forEach items="${CTRL_IND}" var="code">
			                           			<option value="${code.dtlCd}">${code.dtlCdNm}</option>
			                           		</c:forEach>
                                        </select>
                                    </li>
                                    <li class="sname">접속가능IP <input type="text" name="name" id="in_ip"></li>
                                </ul>
                            </td>
                            <td class="btn_search">
                               <button type="button" onClick="search()">조회</button>
                            </td>
                        </tr>
                    </table>
                </form>
              </div>
                <div class="page_contents">
                    <ul>
                        <li class="contents_left" style="width:55%;">
                            <div class="tt_line">접근제어정보<button onclick="createPopClick()">등록</button></div>
                            <div class="con_table mCustomScrollbar">
                                <table class="cord_table" border="0" cellpadding="0" cellspacing="0" style="table-layout:fixed;">
                                    <thead>
                                    <tr>
                                        <th style="width:50px;">NO</th>
                                        <th style="width:75px;">접근제어<br>ID</th>
                                        <th style="width:100px;">제휴사<br>구분</th>
                                        <th>제휴사명</th>
                                        <th style="width:70px;">접근제어<br>상태</th>
                                        <th style="width:65px;">수정자</th>
                                        <th style="width:100px;">수정일시</th>
                                        <th style="width:70px;">관리</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tBody">
                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <li class="contents_right" style="width:44%;">
                            <div class="tt_line">접근 IP정보 <button onclick="createPopDtlClick()">등록</button></div>
                            <div class="con_table mCustomScrollbar">
                                <table class="cord_table" border="0" cellpadding="0" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th style="width:50px;">NO</th>
                                        <th>접속가능<br>IP</th>
                                        <th style="width:70px;">IP<br>상태</th>
                                        <th style="width:65px;">수정자</th>
                                        <th style="width:150px;">수정일시</th>
                                        <th style="width:70px;">관리</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbodyDtl">
                                    </tbody>
                                </table>
                            </div>
                        </li>
                    </ul>
                </div>

            </div>

            <!-- 스크롤 때문에 여백 있어야함 -->
            <br>
            <br>
            <br>
            <br>
            <br>

        </div>
        <!--contents 끝-->
</section>
<!-- dash_board 끝--> 

    <!-- footer 시작-->
    <jsp:include page="/WEB-INF/views/include/incFooter.jsp" flush="false" />
    <section class="footer">	 
    </section>
    <!-- footer 끝-->
    
    <%-- js --%>
	<jsp:include page="./accInfoViewJs.jsp" flush="false" />
	
</body>
</html>