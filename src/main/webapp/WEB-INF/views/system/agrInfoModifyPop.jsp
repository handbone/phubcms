<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%--
 **********************************************************************************************
 * @desc : 약관정보 상세 팝업
 * @FileName 	: /pHubCMS/src/main/webapp/WEB-INF/views/system/agrInfoModifyPop.jsp
 * @author 		: bmg
 * @since 		: 2019.05.07
 * @version 	: 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.05.07   bmg        최초생성
 *
 **********************************************************************************************
 agrInfoView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>
<div class="popup_content2">
	<div class="popup_title">약관정보 수정<a href="#!"><div class="top_close"></div></a></div>
	<table class="half_table" border="0" cellpadding="0" cellspacing="0">
		<tbody>
            <tr>
                <th>약관ID <span class="red">*</span></th>
                <td>
                	<input type="text" id="m_in_cls_id2" style="width:100%;background-color:lightgray;" maxlength="10" readonly />
                </td>
                <th>약관버전 <span class="red">*</span></th>
                <td>
                	<input type="text" id="m_in_cls_ver2" style="width:100%;background-color:lightgray;"maxlength="10" readonly />
                </td>
            </tr>
            <tr>
                <th>서비스ID <span class="red">*</span></th>
                <td>
                	<select id="m_sel_service_id">
                		<option value="">선택</option>
                		<option value="SVC_BASE">포인트허브</option>
                		<option value="SVC_FP">패밀리포인트</option> 
                	</select>
                </td>
                <th>사용여부</th>
                <td>
                    <input type="radio" name="m_ra_use_yn" value="Y"> 사용
			    	<input type="radio" name="m_ra_use_yn" value="N"> 미사용
                </td>
            </tr>
            <tr>
            	<th>약관타이틀 <span class="red">*</span></th>
            	<td colspan="3">
            		<input type="text" id="m_in_cls_titl" style="width:100%;" maxlength="200" />
            	</td>
            </tr>
            <tr>
            	<th>서브타이틀</th>
            	<td colspan="3">
            		<input type="text" id="m_in_cls_sub_titl" style="width:100%;" maxlength="200" />
            	</td>
            </tr>
            <tr>
            	<th>약관상세팝업주소</th>
            	<td colspan="3">
            		<input type="text" id="m_in_cls_url" style="width:100%;" maxlength="200" />
            	</td>
            </tr>
            <tr>
            	<th>정렬순서 <span class="red">*</span></th>
            	<td>
            		<input type="text" id="m_in_sort_ord2" style="width:50%;text-align:right;" maxlength="4" onkeydown="return onlyNum(event);" onkeyup="return keyReplace(this);" />
            	</td>
            	<th>필수여부</th>
            	<td>
            		<input type="radio" name="m_ra_mand_yn" value="Y"> 사용
			    	<input type="radio" name="m_ra_mand_yn" value="N"> 미사용
            	</td>
            </tr>
            <tr>
            	<th>비고</th>
            	<td colspan="3">
            		<input type="text" id="m_in_rmrk2" rows="2" style="width:100%;" maxlength="100"></textarea>
            	</td>
            </tr>
            <tr>
            	<th>적용시작일자</th>
            	<td>
            		<input type="text" id="m_in_aply_strt_dd" maxlength="10" style="width:50%;" readonly />
            	</td>
            	<th>적용종료일자</th>
            	<td>
            		<input type="text" id="m_in_aply_end_dd" maxlength="10" style="width:50%;" readonly />
            	</td>
            </tr>
            <tr>
            	<th>등록자</th>
            	<td name="m_rgst_user_nm"></td>
            	<th>등록일시</th>
            	<td name="m_rgst_dt"></td>
            </tr>
            <tr>
            	<th>수정자</th>
            	<td name="m_mdfy_user_nm"></td>
            	<th>수정일시</th>
            	<td name="m_mdfy_dt"></td>
            </tr>
		</tbody>
	</table>
	<div class="btnalign">
		<ul>
			<li><a href="#!"><div class="btn_cancel">취소</div></a></li>
			<li><a href="#!"><div class="btn_save" onClick="modifyAgrInfo();">저장</div></a></li>
		</ul>
	</div>
</div>
<!-- popup 끝-->