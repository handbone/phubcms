<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%>
<%--
 **********************************************************************************************
 * @desc : 공통코드 등록 팝업
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/system/comCdCreatePop.jsp
 * @author bmg
 * @since 2018.09.12
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.12    bmg        최초생성
 *
 **********************************************************************************************
 comCdView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>

<!-- 구분코드 등록 시작 -->
<div class="popup_content">
	<div class="popup_title">구분코드 등록<a href="#!"><div class="top_close"></div></a></div>
	<table class="half_table" border="0" cellpadding="0" cellspacing="0">
		<tbody>
			<tr>
			    <th>구분코드 <span class="red">*</span></th>
			    <td><input type="text" name="c_ind_cd" maxlength="20" style="text-transform:uppercase;"></td>
			    <th>구분코드명 <span class="red">*</span></th>
			    <td><input type="text" name="c_ind_cd_nm" maxlength="30"></td>
			</tr>
			<tr>
			    <th>그룹코드</th>
			    <td>
			        <select id="c_grp_cd">
			            <option value="">선택</option>
			            <c:forEach items="${COM_GRP}" var="code">
                        	<option value="${code.dtlCd}">${code.dtlCdNm}</option>
                        </c:forEach>
			        </select>
			    </td>
			    <th>사용여부</th>
			    <td>
			        <input type="radio" name="c_com_use_yn" value="Y" checked> 사용
			        <input type="radio" name="c_com_use_yn" value="N"> 미사용
			    </td>
			</tr>
			<tr>
			    <th>코드설명</th>
			    <td colspan="3"> <textarea id="c_cd_desc" rows="5" maxlength="100"></textarea></td>
			</tr>
		</tbody>
	</table>
	<div class="btnalign">
		<ul>
		    <li><a href="#!"><div class="btn_cancel">취소</div></a></li>
		    <li><a href="#!"><div class="btn_save" onclick="createComCd();">저장</div></a></li>
		</ul>
	</div>
</div>