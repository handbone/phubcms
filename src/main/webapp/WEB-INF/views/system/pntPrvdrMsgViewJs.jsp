<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
 **********************************************************************************************
 * @desc : 연동시스템 응답코드 정의 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/cmpn/pntPrvdrMsgViewJs.jsp
 * @author jungukjae
 * @since 2019.06.17
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일      수 정 자        		수정내용
 * ----------   ----------   -----------------------------
 * 2019.06.17    jungukjae        		최초생성
 * 
 **********************************************************************************************
--%>


<script type="text/javascript">
var cWidth = $("#content").width();
$(document).ready(function () {  
	var colNames = ['No','연동시스템ID','포인트코드','응답코드','응답순번','응답메세지','참조값1','수정자','수정일시','관리'];
	var colModel = [
	     { label: 'No'					,name: 'no'				,width: cWidth * 0.05, key:true }
		,{ label: '연동시스템ID'		,name: 'prvdr_id' 		,width: cWidth * 0.10 }
		,{ label: '포인트코드'			,name: 'pnt_cd' 		,width: cWidth * 0.10 }
		,{ label: '응답코드'			,name: 'msg_id'			,width: cWidth * 0.10 }
		,{ label: '응답순번'			,name: 'msg_seq'		,width: cWidth * 0.10 }
		,{ label: '응답메세지'			,name: 'msg'			,width: cWidth * 0.30 }
		,{ label: '참조값1'				,name: 'rfrn_val1'		,width: cWidth * 0.15 }
		,{ label: '수정자'				,name: 'mdfy_user_id' 	,width: cWidth * 0.10 }
		,{ label: '수정일시'			,name: 'mdfy_dt' 		,width: cWidth * 0.10 }
		,{ label: '관리'				,name: ''  				,width: cWidth * 0.05, formatter:mbutton }
	];	
	PHJQg.load('jqGrid','jqGridPager',colNames,colModel);
	
	// 엑셀다운로드
	$("#btn_excel").on("click", function(){
		var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");	
		if(conf){		
			var postData = {
					PRVDR_ID : $("#prvdr_id").val(),
					PNT_CD : $("#pnt_cd").val(), 
					MSG_ID : $('#msg_id').val(),
					MSG : $('#msg').val(),
					RFRN_VAL1 : $('#rfrn_val1').val()
			};
			var colNames = ['No','연동시스템ID','포인트코드','응답코드','응답순번','응답메세지','참조값1','수정자','수정일시'];
			var colModel = [ 
		       	     { label: 'No'					,name: 'no'				,width: cWidth * 0.05 }
		     		,{ label: '연동시스템ID'		,name: 'prvdr_id' 		,width: cWidth * 0.10 }
		     		,{ label: '포인트코드'			,name: 'pnt_cd' 		,width: cWidth * 0.10 }
		     		,{ label: '응답코드'			,name: 'msg_id'			,width: cWidth * 0.10 }
		     		,{ label: '응답순번'			,name: 'msg_seq'		,width: cWidth * 0.10 }
		     		,{ label: '응답메세지'			,name: 'msg'			,width: cWidth * 0.30 }
		     		,{ label: '참조값1'				,name: 'rfrn_val1'		,width: cWidth * 0.15 }
		     		,{ label: '수정자'				,name: 'mdfy_user_id' 	,width: cWidth * 0.10 }
		     		,{ label: '수정일시'			,name: 'mdfy_dt' 		,width: cWidth * 0.10 }
			];
			PHJQg.excel('jqGridExcelDiv','${ViewRoot}/system/jqPntPrvdrMsgExcel.do',postData,colNames,colModel,'연동시스템 응답코드');
		}
	});

	$('.top_close').click(function(){
		resetPop();		
	});

	$('.btn_close').click(function(){
		resetPop();		
	});
});
	
//입력을 초기화시키는 함수
function resetPop() {
 	$("#c_prvdr_id").val('');  
	$("#c_pnt_cd").val('');	
	$("#c_msg_id").val('');	
	$("#c_msg").val('');	
	$("#c_rfrn_val1").val('');	
	$("#c_rfrn_val2").val('');	
	$("#c_rfrn_val3").val(''); 	
}	
	
//검색버튼 클릭
//파라미터 전달 후 jQgrid reload 
function search(){
	var postData = {
		PRVDR_ID : $("#prvdr_id").val(),
		PNT_CD : $("#pnt_cd").val(), 
		MSG_ID : $('#msg_id').val(),
		MSG : $('#msg').val(),
		RFRN_VAL1 : $('#rfrn_val1').val()
	};
	PHJQg.reload('jqGrid','jqGridPager','${ViewRoot}/system/ajaxViewPntPrvdrMsg.do',postData);
}
	
//formatter 버튼 만들어 주기 클릭시 No번호 param 전달
function mbutton(cellvalue,options,rowobject){
    return '<input type="button" class="table_btn" onclick="popClick('+rowobject.no+')" value="수정" />';
}

//등록버튼 클릭시 오픈팝업(layerPopOpen(1))
function createPntPrvdrMsgPopClick() {
	PHFnc.layerPopOpen(1);
}

//저장 
function createPntPrvdrMsg() {	
	var c_prvdr_id = $('#c_prvdr_id').val();
	var c_pnt_cd = $('#c_pnt_cd').val();
	var c_msg_id = $('#c_msg_id').val();
	if(c_prvdr_id == "") {
		alert('연동시스템ID를 입력해 주세요.');
		return;
	}
	if(c_pnt_cd == "") {
		alert('포인트코드를 입력해 주세요.');
		return;
	}
	if(c_msg_id == "") {
		alert('응답코드를 입력해 주세요.');
		return;
	}	
	var params = {
		 	  PRVDR_ID 	: c_prvdr_id  
			, PNT_CD 	: c_pnt_cd	
			, MSG_ID 	: c_msg_id		
	};
	PHFnc.ajax("${ViewRoot}/system/checkPntPrvdrMsgKey.do",params,"POST","json",function(data, textStatus, jqXHR) {
		var keyCode = data.keyCount;
		if(keyCode == 0) {
			if(confirm("저장하시겠습니까?")){
				var params = { 
		  		 	  PRVDR_ID 				: $("#c_prvdr_id").val()  
					, PNT_CD 				: $("#c_pnt_cd").val()	
					, MSG_ID 				: $("#c_msg_id").val()	
					, MSG					: $("#c_msg").val()	
					, RFRN_VAL1				: $("#c_rfrn_val1").val()	
					, RFRN_VAL2				: $("#c_rfrn_val2").val()	
					, RFRN_VAL3				: $("#c_rfrn_val3").val()	
				};	
				PHFnc.ajax("${ViewRoot}/system/ajaxCreatePntPrvdrMsgInfo.do",params,"POST","json",
					function(data, textStatus, jqXHR) {
						alert("등록하였습니다.");
						$(".top_close").click();
						search();
					},
					function(data, textStatus, jqXHR) {
						if (!PHUtil.isEmpty(data.eMsg)) {
							PHFnc.alert(data.eMsg);	
						} else {
							PHFnc.ajaxError(null,jqXHR);
						}
						PHFnc.submitLoadingDestory();
					},
				true,true,false);
			}			
		}else {
			PHFnc.alert('키값이 중복됩니다. 재입력 바랍니다.');						
		}
	},null,true,true,false); 	
}

//수정버튼 클릭시 오픈팝업(layerPopOpen(2))  
function popClick(data){	
	var row = $("#jqGrid").jqGrid('getRowData',data);
	
	//누른 제공처 ID를 key로 상세 조회
	var params = {
			PRVDR_ID : row.prvdr_id,
			MSG_SEQ : row.msg_seq 
	};
	PHFnc.ajax("${ViewRoot}/system/ajaxGetPntPrvdrMsgInfo.do",params,"POST","json",function(data, textStatus, jqXHR) {
		$("#m_prvdr_id").val(data.rows.prvdr_id);		// 연동시스템ID
		$("#m_msg_seq").val(data.rows.msg_seq);			// 응답순번
		$("#m_pnt_cd").val(data.rows.pnt_cd);			// 포인트코드 
		$("#m_msg_id").val(data.rows.msg_id);			// 응답코드
		$("#m_msg").val(data.rows.msg);					// 응답메세지
		$("#m_rfrn_val1").val(data.rows.rfrn_val1);		// 참조값1
		$("#m_rfrn_val2").val(data.rows.rfrn_val2);		// 참조값2
		$("#m_rfrn_val3").val(data.rows.rfrn_val3);		// 참조값3		
		$("#m_rId").html(data.rows.rgst_user_id);		// 등록자ID
		$("#m_rDt").html(data.rows.rgst_dt);			// 등록일시
		$("#m_mId").html(data.rows.mdfy_user_id);		// 수정자ID
		$("#m_mDt").html(data.rows.mdfy_dt);			// 수정일시
		PHFnc.layerPopOpen(2);
	},null,true,true,false); 
}

//수정
function modifyPntPrvdrMsg() {
	if($('#m_pnt_cd').val() == "") {
		alert('포인트코드를 입력해 주세요.');
		return;
	}
	if($('#m_msg_id').val() == "") {
		alert('응답코드를 입력해 주세요.');
		return;
	}
	if(confirm("저장하시겠습니까?")){
		var params = { 
  		 	  PRVDR_ID 				: $("#m_prvdr_id").val()  
  		 	, MSG_SEQ 				: $("#m_msg_seq").val()  
  			, PNT_CD 				: $("#m_pnt_cd").val()	
  			, MSG_ID 				: $("#m_msg_id").val()	
  			, MSG					: $("#m_msg").val()	
  			, RFRN_VAL1				: $("#m_rfrn_val1").val()	
  			, RFRN_VAL2				: $("#m_rfrn_val2").val()	
  			, RFRN_VAL3				: $("#m_rfrn_val3").val()	
		};	
		PHFnc.ajax("${ViewRoot}/system/ajaxModifyPntPrvdrMsgInfo.do",params,"POST","json",
			function(data, textStatus, jqXHR) {
				alert("수정하였습니다.");
				$(".top_close").click();
				search();
			},
			function(data, textStatus, jqXHR) {
				if (!PHUtil.isEmpty(data.eMsg)) {
					PHFnc.alert(data.eMsg);	
				} else {
					PHFnc.ajaxError(null,jqXHR);
				}
				PHFnc.submitLoadingDestory();
			},
		true,true,false);		
	}
}

//삭제 
function deletePntPrvdrMsg() {
	if(confirm("삭제 하시겠습니까?")){
		var params = { 
	  		 	  PRVDR_ID 				: $("#m_prvdr_id").val()  
    		 	, MSG_SEQ 				: $("#m_msg_seq").val()  
		};	
		PHFnc.ajax("${ViewRoot}/system/ajaxDeletePntPrvdrMsgInfo.do",params,"POST","json",
			function(data, textStatus, jqXHR) {
				alert("삭제하였습니다.");
				$(".top_close").click();
				search();
			},
			function(data, textStatus, jqXHR) {
				if (!PHUtil.isEmpty(data.eMsg)) {
					PHFnc.alert(data.eMsg);	
				} else {
					PHFnc.ajaxError(null,jqXHR);
				}
				PHFnc.submitLoadingDestory();
			},
		true,true,false);		
	}
}


</script>



























