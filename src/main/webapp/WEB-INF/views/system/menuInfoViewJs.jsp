<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="com.olleh.pHubCMS.common.components.Constant"%>
<%--
 **********************************************************************************************
 * @desc : 메뉴관리 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/system/menuInfoViewJs.jsp
 * @author bmg
 * @since 2018.09.20
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.20    bmg        최초생성
 * 
 **********************************************************************************************
--%>
<script type="text/javascript">
$(document).ready(function () { 
	$('#menuRows').html('<li>메뉴명</li>');
	
	$(':text[name$=menu_id],:text[name$=menu_uri],:text[name$=menu_ord]').keydown(function(e) {
		if (e.keyCode == 32) {
			return false;
		}
	});
	
	$(':text').keyup(function(e) {
		var keyID = e.keyCode;
		if (((keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39 
	    		|| keyID == 9 || keyID == 16 || keyID == 17 || keyID == 35 || keyID == 36)) {
			return true;
		} else {
			switch ($(e.target).attr('name')) {
				case 'c_menu_id':
					$(this).val($(this).val().replace(/[^a-zA-Z0-9]/gi, ""));
					break;
				case 'c_menu_ord':
				case 'm_menu_ord':
					$(this).val($(this).val().replace(/[^0-9]/gi, ""));
					break;
				case 'c_menu_uri':
				case 'm_menu_uri':
					$(this).val($(this).val().replace(/[^a-zA-Z0-9:/.?&=]/gi, ""));
					break;
			}
		}
	});
	
	$('select[id$=_menu_ind],select[id$=_menu_lvl]').change(function(e) {
		if ($(e.target).is('[id^=c_]')) {
			changeUprMenuId(true);
		}
		if ($(e.target).is('[id^=m_]')) {
			changeUprMenuId(false);
		}
	});
});
//검색 버튼 클릭 시
function search() {
	var params = {
		MENU_NM: $(':text[name=menu_nm]').val()
		, ACTN_URI: $(':text[name=actn_uri]').val()
		, MENU_IND: $('#menu_ind').val()
		, USE_YN: $('#use_yn').val()
	};
	PHFnc.ajax("${ViewRoot}/system/ajaxViewMenuInfo.do",params,"POST","json",
		function(data, textStatus, jqXHR) {
			var rows = data.rows;
			var func = "menuInfoModifyPopClick(this);";
			$('#menuRows').empty();
			$('.cord_table tbody').empty();
			$('#menuRows').html('<li>메뉴명</li>');
			for (var i=0; i<rows.length; i++) {
				var row = rows[i];
				switch (row.menu_lvl) {
					case 1:
						$('<li class=\"menuitem deps1\">'+ row.menu_nm +' <button onclick=\"'+ func +'event.stopPropagation();\">수정</button></li>').data('item',row).appendTo($('#menuRows'));
						break;
					case 2:
						if (row.is_child == 'Y') {
							$('<li class=\"menuitem deps2\">&nbsp;└ &nbsp;' + row.menu_nm + ' <button onclick=\"'+ func +'event.stopPropagation();\">수정</button></li><div class="deps3"></div>').data('item',row).appendTo($('#menuRows'));
						} else {
							$('<li class=\"menuitem deps2\">&nbsp;└ &nbsp;' + row.menu_nm + ' <button onclick=\"'+ func +'event.stopPropagation();\">수정</button></li>').data('item',row).appendTo($('#menuRows'));
						}
						break;
					case 3:
						$('<a href=\"#!\">&nbsp;└ &nbsp;'+ row.menu_nm +' <button onclick=\"'+ func +'event.stopPropagation();\">수정</button></a>').data('item', row).appendTo($('#menuRows div.deps3:last'));
						break;
					default:
				}
			}
			$('#menuRows').find('div.deps3').css('display', 'block');
			$('#menuRows').find('li.deps1,li.deps2,div.deps3 a').click(function(e) {
				$('#menuRows').find('li.deps1,li.deps2,div.deps3 a').removeClass('bgon');
				$(e.target).addClass('bgon');
				viewActnInfo($(e.target).data('item'));
			});
		},null,true,true,false);
}
//상위메뉴ID 불러오기
function changeUprMenuId(isCreate) {
	$('#c_upr_menu_id,#m_upr_menu_id').empty().append('<option value="">선택</option>');
	var params = {};
	if (isCreate) {
		params = {
			MENU_IND: $('#c_menu_ind').val()
			, MENU_LVL: $('#c_menu_lvl').val()
		}
	} else {
		params = {
			MENU_IND: $('#m_menu_ind').val()
			, MENU_LVL: $('#m_menu_lvl').val()
		}
	}
	if (params.MENU_IND == '' || params.MENU_LVL == '') {
		return false;
	}
	PHFnc.ajax("${ViewRoot}/system/ajaxUprMenuInfo.do",params,"POST","json",function(data, textStatus, jqXHR) {
		for (var i=0; i<data.rows.length; i++) {
			var item = data.rows[i];
			$('#c_upr_menu_id,#m_upr_menu_id').append('<option value="'+ item.menu_id +'">' + item.menu_nm + '</option>');
		}
	},null,true,true,false);
}
//메뉴 등록 팝업 호출
function menuInfoCreatePopClick() {
	$(':text[name^=c_],select[id^=c_],textarea[id^=c_]').val('');
	$(':radio[name=c_dp_yn]:eq(0)').prop('checked', true);
	$(':radio[name=c_use_yn]:eq(0)').prop('checked', true);
	$('#c_upr_menu_id').empty().append('<option value="">선택</option>');
	PHFnc.layerPopOpen(1);
}
// 메뉴 등록
function createMenuInfo() {
	if ($.trim($(':text[name=c_menu_id]').val()) == '') {
		alert('메뉴ID를 입력해 주세요.');
		return;
	}
	if ($.trim($(':text[name=c_menu_nm]').val()) == '') {
		alert('메뉴명을 입력해 주세요.');
		return;
	}
	if ($('#c_menu_ind').val() == '') {
		alert('메뉴구분을 선택해 주세요.');
		return;
	}
	if ($('#c_menu_lvl').val() == '') {
		alert('메뉴레벨을 선택해 주세요.');
		return;
	}
	if ($('#c_upr_menu_id').val() == '') {
		alert('상위메뉴ID를 선택해 주세요.');
		return;
	}
	if ($.trim($(':text[name=c_menu_ord]').val()) == '') {
		alert('메뉴순위를 입력해 주세요.');
		return;
	}
	var conf = confirm("저장하시겠습니까?");
	if (conf) {
		var params = {
			MENU_ID: $(':text[name=c_menu_id]').val()
			, MENU_NM: $(':text[name=c_menu_nm]').val()
			, MENU_IND: $('#c_menu_ind').val()
			, MENU_LVL: $('#c_menu_lvl').val()
			, UPR_MENU_ID: $('#c_upr_menu_id').val()
			, MENU_URI: $(':text[name=c_menu_uri]').val()
			, MENU_ORD: $(':text[name=c_menu_ord]').val()
			, DP_YN: $(':radio[name=c_dp_yn]:checked').val()
			, USE_YN: $(':radio[name=c_use_yn]:checked').val()
			, MENU_DESC: $('#c_menu_desc').val()
		};
		PHFnc.ajax("${ViewRoot}/system/ajaxCreateMenuInfo.do",params,"POST","json",
			function(data, textStatus, jqXHR) {
				alert("등록에 성공했습니다.");
				$(".top_close").click();
				search();
			},
			function(data, textStatus, jqXHR) {
				if (!PHUtil.isEmpty(data.eMsg)) {
					PHFnc.alert(data.eMsg);	
				} else {
					PHFnc.ajaxError(null,jqXHR);
				}
				PHFnc.submitLoadingDestory();
			},
		true,true,false);
	}
}
//메뉴 수정 팝업 호출
function menuInfoModifyPopClick(obj) {
	var item = $(obj).parents('li:first,a:first').data('item');
	var params = {MENU_ID: item.menu_id};
	PHFnc.ajax("${ViewRoot}/system/ajaxGetMenuInfoView.do",params,"POST","json",function(data, textStatus, jqXHR) {
		var item = data.row;
		$(':text[name^=m_],select[id^=m_],textarea[id^=m_]').val('');
		$(':radio[name=m_dp_yn]:eq(0)').prop('checked', true);
		$(':radio[name=m_use_yn]:eq(0)').prop('checked', true);
		$('#m_upr_menu_id').empty().append('<option value="">선택</option>');
		//팝업창에 데이터 세팅
		for (var i=0; i<data.upr.length; i++) {
			$('#m_upr_menu_id').append('<option value="'+ data.upr[i].menu_id +'">'+ data.upr[i].menu_nm + '</option>');
		}
		$(':text[name=m_menu_id]').val(item.menu_id);
		$(':text[name=m_menu_nm]').val(item.menu_nm);
		$('#m_menu_ind').val(item.menu_ind);
		$('#m_menu_lvl').val(item.menu_lvl);
		$('#m_upr_menu_id').val(item.upr_menu_id);
		$(':text[name=m_menu_uri]').val(item.menu_uri);
		$(':text[name=m_menu_ord]').val(item.menu_ord);
		$(':radio[name=m_dp_yn][value='+ item.dp_yn +']').prop('checked', true);
		$(':radio[name=m_use_yn][value='+ item.use_yn +']').prop('checked', true);
		$('#m_menu_desc').val(item.menu_desc);
		$('#m_rgst_id_nm').html(item.rgst_user_nm);
		$('#m_rgst_dt').html(item.rgst_dt);
		$('#m_mdfy_id_nm').html(item.mdfy_user_nm);
		$('#m_mdfy_dt').html(item.mdfy_dt);
		PHFnc.layerPopOpen(2);
	},null,true,true,false);
}
//메뉴 수정
function modifyMenuInfo() {
	if ($.trim($(':text[name=m_menu_id]').val()) == '') {
		alert('메뉴ID를 입력해 주세요.');
		return;
	}
	if ($.trim($(':text[name=m_menu_nm]').val()) == '') {
		alert('메뉴명을 입력해 주세요.');
		return;
	}
	if ($('#m_menu_ind').val() == '') {
		alert('메뉴구분을 선택해 주세요.');
		return;
	}
	if ($('#m_menu_lvl').val() == '') {
		alert('메뉴레벨을 선택해 주세요.');
		return;
	}
	if ($('#m_upr_menu_id').val() == '') {
		alert('상위메뉴ID를 선택해 주세요.');
		return;
	}
	if ($.trim($(':text[name=m_menu_ord]').val()) == '') {
		alert('메뉴순위를 입력해 주세요.');
		return;
	}
	var conf = confirm("저장하시겠습니까?");
	if (conf) {
		var params = {
			MENU_ID: $(':text[name=m_menu_id]').val()
			, MENU_NM: $(':text[name=m_menu_nm]').val()
			, MENU_IND: $('#m_menu_ind').val()
			, MENU_LVL: $('#m_menu_lvl').val()
			, UPR_MENU_ID: $('#m_upr_menu_id').val()
			, MENU_URI: $(':text[name=m_menu_uri]').val()
			, MENU_ORD: $(':text[name=m_menu_ord]').val()
			, DP_YN: $(':radio[name=m_dp_yn]:checked').val()
			, USE_YN: $(':radio[name=m_use_yn]:checked').val()
			, MENU_DESC: $('#m_menu_desc').val()
		};
		PHFnc.ajax("${ViewRoot}/system/ajaxModifyMenuInfo.do",params,"POST","json",
			function(data, textStatus, jqXHR) {
				alert("수정에 성공했습니다.");
				$(".top_close").click();
				search();
			},
			function(data, textStatus, jqXHR) {
				if (!PHUtil.isEmpty(data.eMsg)) {
					PHFnc.alert(data.eMsg);	
				} else {
					PHFnc.ajaxError(null,jqXHR);
				}
				PHFnc.submitLoadingDestory();
			},
		true,true,false);
	}
}
//액션 정보 목록 조회
function viewActnInfo(item) {
	var params = {
		MENU_ID: item.menu_id
		, ACTN_URI: $(':text[name=actn_uri]').val()
	};
	PHFnc.ajax("${ViewRoot}/system/ajaxViewActnInfo.do",params,"POST","json",function(data, textStatus, jqXHR) {
		var rows = data.rows;
		$('.cord_table tbody').empty();
		for (var i=0; i<rows.length; i++) {
			var row = rows[i];
			$(
				'<tr>' +
					'<td>' + row.no + '</td>' +						
					'<td style="text-align:left;white-space:nowrap;text-overflow:ellipsis;overflow:hidden;" title="'+ row.uri +'">' + row.uri + '</td>' +						
					'<td style="text-align:left;white-space:nowrap;text-overflow:ellipsis;overflow:hidden;" title="'+ PHUtil.nvl(row.actn_nm) +'">' + PHUtil.nvl(row.actn_nm) + '</td>' +						
					'<td style="white-space:nowrap;text-overflow:ellipsis;overflow:hidden;" title="'+ PHUtil.nvl(row.uri_method_nm) +'">' + PHUtil.nvl(row.uri_method_nm) + '</td>' +						
					'<td>' + PHUtil.nvl(row.log_write_yn) + '</td>' +						
					'<td>' + PHUtil.nvl(row.mdfy_user_nm) + '</td>' +						
					'<td>' + popDateValue(PHUtil.nvl(row.mdfy_dt)) + '</td>' +						
					'<td><button onclick="actnInfoModifyPopClick(\''+ row.menu_id +'\',\''+ row.uri +'\')">수정</button></td>' +						
				'</tr>'
			).appendTo($('.cord_table tbody'));
		}
	},null,true,true,false);
}
//액션 정보 등록 팝업 호출
function actnInfoCreatePopClick() {
	if ($('#menuRows').find('.bgon').length == 0) {
		alert('메뉴를 먼저 선택해 주세요');
		return;
	}
	var item = $('#menuRows').find('.bgon').data('item');
	$(':text[name^=c_],select[id^=c_]').val('');
	$('#c_actn_menu_id').html(item.menu_id);
	$('#c_actn_menu_nm').html(item.menu_nm);
	PHFnc.layerPopOpen(3);
}
//액션 정보 등록
function createActnInfo() {
	if ($.trim($(':text[name=c_actn_uri]').val()) == '') {
		alert('URI를 입력해 주세요');
		return;
	}
	
	if ($.trim($(':text[name=c_actn_nm]').val()) == '') {
		alert('ACTION명을 입력해 주세요');
		return;
	}
	
	if ($('#c_actn_uri_method').val() == '') {
		alert('URI METHOD를 선택해 주세요');
		return;
	}
	
	if ($('#c_log_write_yn').val() == '') {
		alert('로그기록 여부를 선택해 주세요');
		return;
	}
	
	if (confirm("저장하시겠습니까?")) {
		var item = $('#menuRows').find('.bgon').data('item'); 
		var params = {
			MENU_ID: item.menu_id
			, MENU_NM: item.menu_nm
			, URI: $(':text[name=c_actn_uri]').val()
			, ACTN_NM: $(':text[name=c_actn_nm]').val()
			, URI_MEHTOD: $('#c_actn_uri_method').val()
			, LOG_WRITE_YN: $('#c_log_write_yn').val()
		};
		PHFnc.ajax("${ViewRoot}/system/ajaxCreateActnInfo.do",params,"POST","json",
			function(data, textStatus, jqXHR) {
				alert("등록에 성공했습니다.");
				$(".top_close").click();
				viewActnInfo($('#menuRows').find('li.bgon,a.bgon').data('item'));
			},
			function(data, textStatus, jqXHR) {
				if (!PHUtil.isEmpty(data.eMsg)) {
					PHFnc.alert(data.eMsg);	
				} else {
					PHFnc.ajaxError(null,jqXHR);
				}
				PHFnc.submitLoadingDestory();
			},
		true,true,false);
	}
}
//액션 정보 수정 팝업 호출
function actnInfoModifyPopClick(id,uri) {
	var params = {
		MENU_ID: id,
		URI: uri
	};
	PHFnc.ajax("${ViewRoot}/system/ajaxGetActnInfo.do",params,"POST","json",function(data, textStatus, jqXHR) {
		var item = data.row;
		$('td[id^=m_actn_]').html('');
		$(':text[name^=m_],select[id^=m_]').val('');
		//팝업창에 데이터 세팅
		$(':hidden[name=m_actn_uri_key]').val(item.uri);
		$('#m_actn_menu_id').html(item.menu_id);
		$('#m_actn_menu_nm').html(item.menu_nm);
		$(':text[name=m_actn_uri]').val(item.uri);
		$(':text[name=m_actn_nm]').val(item.actn_nm);
		$('#m_actn_uri_method').val(item.uri_method);
		$('#m_log_write_yn').val(item.log_write_yn);
		$('#m_actn_rgst_id_nm').html(item.rgst_user_nm);
		$('#m_actn_mdfy_id_nm').html(item.mdfy_user_nm);
		$('#m_actn_rgst_dt').html(item.rgst_dt);
		$('#m_actn_mdfy_dt').html(item.mdfy_dt);
		PHFnc.layerPopOpen(4);
	},null,true,true,false);
}
//액션 정보 수정
function modifyActnInfo() {
	if ($.trim($(':text[name=m_actn_uri]').val()) == '') {
		alert('URI를 입력해 주세요');
		return;
	}
	
	if ($.trim($(':text[name=m_actn_nm]').val()) == '') {
		alert('ACTION명을 입력해 주세요');
		return;
	}
	
	if ($('#m_actn_uri_method').val() == '') {
		alert('URI METHOD를 선택해 주세요');
		return;
	}
	
	if ($('#m_log_write_yn').val() == '') {
		alert('로그기록 여부를 선택해 주세요');
		return;
	}
	
	if (confirm("저장하시겠습니까?")) {
		var item = $('#menuRows').find('.bgon').data('item'); 
		var params = {
			MENU_ID: item.menu_id
			, MENU_NM: item.menu_nm
			, URI: $(':text[name=m_actn_uri]').val()
			, ACTN_NM: $(':text[name=m_actn_nm]').val()
			, URI_METHOD: $('#m_actn_uri_method').val()
			, LOG_WRITE_YN: $('#m_log_write_yn').val()
			, URI_KEY: $(':hidden[name=m_actn_uri_key]').val() 
		};
		PHFnc.ajax("${ViewRoot}/system/ajaxModifyActnInfo.do",params,"POST","json",
			function(data, textStatus, jqXHR) {
				alert("수정에 성공했습니다.");
				$(".top_close").click();
				viewActnInfo($('#menuRows').find('li.bgon,a.bgon').data('item'));
			},
			function(data, textStatus, jqXHR) {
				if (!PHUtil.isEmpty(data.eMsg)) {
					PHFnc.alert(data.eMsg);	
				} else {
					PHFnc.ajaxError(null,jqXHR);
				}
				PHFnc.submitLoadingDestory();
			},
		true,true,false);
	}
}
//날짜 개행 표시
function popDateValue(str) {
	return str.replace(' ','<br>');
}
</script>