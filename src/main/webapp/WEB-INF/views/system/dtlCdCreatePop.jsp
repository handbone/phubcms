<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%>
<%--
 **********************************************************************************************
 * @desc : 상세코드 등록 팝업
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/system/dtlCdCreatePop.jsp
 * @author bmg
 * @since 2018.09.12
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.12    bmg        최초생성
 *
 **********************************************************************************************
 comCdView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>

<!-- 상세코드 등록 시작 -->
<div class="popup_content3">
	<div class="popup_title">상세코드 등록<a href="#!"><div class="top_close"></div></a></div>
    <table class="half_table" border="0" cellpadding="0" cellspacing="0">
        <tbody>
	        <tr>
	            <th>구분코드</th>
	            <td><input type="text" name="c_dtl_ind_cd" style="background-color:lightgray;" readonly></td>
	            <th>구분코드명</th>
	            <td><input type="text" name="c_dtl_ind_cd_nm" style="background-color:lightgray;" readonly></td></td>
	        </tr>
	        <tr>
	            <th>상세코드 <span class="red">*</span></th>
				<td><input type="text" name="c_dtl_cd" maxlength="20"></td>
	            <th>상세코드명 <span class="red">*</span></th>
	            <td><input type="text" name="c_dtl_cd_nm" maxlength="20"></td>
	        </tr>
	        <tr>
	            <th>노출여부</th>
	            <td>
	                <input type="radio" name="c_dtl_dp_yn" value="Y" checked> 노출
	                <input type="radio" name="c_dtl_dp_yn" value="N"> 숨김
	            </td>
	            <th>사용여부</th>
	            <td>
	                <input type="radio" name="c_dtl_use_yn" value="Y" checked> 사용
	                <input type="radio" name="c_dtl_use_yn" value="N"> 미사용
	            </td>
	        </tr>
	        <tr>
	            <th>코드값</th>
	            <td><input type="text" name="c_dtl_cd_val" maxlength="10"></td>
	            <th>정렬순서 <span class="red">*</span></th>
	            <td><input type="text" name="c_dtl_sort_ord" maxlength="4" autocomplete="off"></td>
	        </tr>
	        <tr>
	            <th>코드설명</th>
	            <td colspan="3"> <textarea id="c_dtl_cd_desc" rows="5" maxlength="100"></textarea></td>
	        </tr>
	        <tr>
	            <th>비고</th>
	            <td colspan="3"> <textarea id="c_dtl_rmrk" rows="5" maxlength="100"></textarea></td>
	        </tr>
        </tbody>
    </table>
    <div class="btnalign">
        <ul>
            <li><a href="#!"><div class="btn_cancel">취소</div></a></li>
            <li><a href="#!"><div class="btn_save" onclick="createDtlCd();">저장</div></a></li>
        </ul>
    </div>
</div>