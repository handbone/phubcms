<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%>
<%--
 **********************************************************************************************
 * @desc : 메뉴 정보 등록 팝업
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/system/menuInfoCreatePop.jsp
 * @author bmg
 * @since 2018.09.27
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.27    bmg        최초생성
 *
 **********************************************************************************************
 menuInfoView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>
<!-- 메뉴정보 등록 시작 -->
<div class="popup_content">
    <div class="popup_title">메뉴정보 등록<a href="#!"><div class="top_close"></div></a></div>
    <table class="half_table" border="0" cellpadding="0" cellspacing="0">
        <tbody>
	        <tr>
	            <th style="width:200px;">메뉴ID <span class="red">*</span></th>
	            <td style="width:300px;"><input type="text" name="c_menu_id" maxlength="20"></td>
	            <th style="width:200px;">메뉴명 <span class="red">*</span></th>
	            <td><input type="text" name="c_menu_nm" maxlength="40" style="width:310px;"></td>
	        </tr>
	        <tr>
	            <th>메뉴구분 <span class="red">*</span></th>
	            <td>
	                <select id="c_menu_ind" style="width:165px;">
	                    <option value="">선택</option>
	                    <c:forEach items="${MENU_IND}" var="code">
							<option value="${code.dtlCd}">${code.dtlCdNm}</option>
						</c:forEach>
	                </select>
	            </td>
	            <th>메뉴레벨 <span class="red">*</span></th>
	            <td>
	                <select id="c_menu_lvl" style="width:165px;">
	                    <option value="">선택</option>
	                    <c:forEach items="${MENU_LVL}" var="code">
	                    	<c:if test="${code.useYn == 'Y'}">
								<option value="${code.dtlCd}">${code.dtlCdNm}</option>
							</c:if>
						</c:forEach>
	                </select>
	            </td>
	        </tr>
	        <tr>
	            <th>상위메뉴ID <span class="red">*</span></th>
	            <td colspan="3">
	                <select id="c_upr_menu_id" style="width:165px;"></select>
	                &nbsp;&nbsp;&nbsp; * 메뉴구분과 메뉴레벨 선택 후, 상위메뉴 ID를 선택해 주세요</td>
	        </tr>
	        <tr>
	        	<th>메뉴순위 <span class="red">*</span></th>
	            <td><input type="text" name="c_menu_ord" maxlength="5" style="text-align:right;"></td>
	            <th>메뉴URI</th>
	            <td>
	            	<div style="display:inline-block;width:0px;">
	            		<input type="text" name="c_menu_uri" style="width:310px;" maxlength="200">
	            	</div>
	            </td>
	        </tr>
	        <tr>
	            <th>노출여부</th>
	            <td>
	                <input type="radio" name="c_dp_yn" value="Y" checked> 노출
	                <input type="radio" name="c_dp_yn" value="N"> 숨김
	            </td>
	            <th>사용여부</th>
	            <td>
	                <input type="radio" name="c_use_yn" value="Y" checked> 사용
	                <input type="radio" name="c_use_yn" value="N"> 미사용
	            </td>
	        </tr>
	        <tr>
	            <th>메뉴설명</th>
	            <td colspan="3"> <textarea id="c_menu_desc" rows="5" maxlength="200"></textarea></td>
	        </tr>
        </tbody>
    </table>
    <div class="btnalign">
        <ul>
            <li><a href="#!"><div class="btn_cancel">취소</div></a></li>
            <li><a href="#!"><div class="btn_save" onclick="createMenuInfo();">저장</div></a></li>
        </ul>
    </div>
</div>