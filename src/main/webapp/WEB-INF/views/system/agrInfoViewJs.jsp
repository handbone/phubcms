<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
 **********************************************************************************************
 * @desc : 약관정보 관리 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/system/agrInfoViewJs.jsp
 * @author 	bmg
 * @since 	2019.05.07
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.05.07   bmg        최초생성
 * </pre>
 **********************************************************************************************
--%>
<script type="text/javascript">
$(document).ready(function () {
	
	$('#c_in_aply_strt_dd').datepicker();
	$('#c_in_aply_end_dd').datepicker();
	$('#m_in_aply_strt_dd').datepicker();
	$('#m_in_aply_end_dd').datepicker();
	
	$('.ui-datepicker-trigger').css({
		'padding-left': '10px', 'width': '40px'
	});
	$('#in_clsId,#in_prvdrId,#c_in_cls_id1,#c_in_cls_ver1,#c_in_prvdr_id,#m_in_prvdr_id').keyup(function(e) {
		if (!(e.keyCode >= 37 && e.keyCode <=40)) {
			var input = $(this).val();
			$(this).val(input.replace(/[^a-z_0-9]/gi, ""));
		}
	});
	$("#c_in_aply_strt_dd,#c_in_aply_end_dd,#m_in_aply_strt_dd,#m_in_aply_end_dd").removeAttr('readonly')
		.keypress(function(e) {
			var keycode = e.keyCode ? e.keyCode : e.which;
			if ((keycode >= 48 && keycode <= 57) || (keycode >= 96 && keycode <= 105)) {
				switch ($(this).val().length) {
					case 4:
					case 7:
						if (($(this).val().match(/-/g) || []).length < 2) {
							$(this).val($(this).val()+'-');	
						}
						break;
					default:
				}
				return true;
			} else {
	    		switch(keycode) {
		    		case 8:		// BACKSPACE
	    			case 9:		// TAB
	    			case 45:	// -
	    				return true;
	    				break;
	    			default:
	    				e.preventDefault();
	    		}
			}
		})
		.focusout(function(e) {
			var value = PHUtil.replaceAll($(this).val(), "-", "");
			if (!PHUtil.isEmpty(value) && !PHValid.date(value)) {
				alert('유효한 날짜형식이 아닙니다');
				$(this).val('');
				$(this).select();
				e.stopPropagation();
				return false;
			}
		}
	);
});
//검색 버튼 클릭 시
function search() {
	var params = {
		SERVICE_ID  : $('#sel_serviceId').val()
		, CLS_ID	: $('#in_clsId').val()
		, PRVDR_ID	: $('#in_prvdrId').val()
		, USE_YN	: $('#sel_useYn').val()
	};
	PHFnc.ajax("${ViewRoot}/system/ajaxViewAgrInfo.do",params,"POST","json",function(data, textStatus, jqXHR) {
		$('#clsInfoTable tbody').empty();
		$('#prvdItemTable tbody').empty();
		for (var i=0; i<data.rows.length; i++) {
			var item = data.rows[i];
			$(	'<tr>' +
					'<td>'+ item.no +'</td>' +
					'<td>'+ item.cls_id +'</td>' +
					'<td>'+ item.cls_ver +'</td>' +
					'<td style="text-align:left;white-space:nowrap;text-overflow:ellipsis;overflow:hidden;" title="'+ PHUtil.nvl(item.cls_titl) +'">'+ PHUtil.nvl(item.cls_titl) +'</td>' +
					'<td>'+ PHUtil.nvl(item.use_yn) +'</td>' +
					'<td>'+ PHUtil.nvl(item.mand_yn) +'</td>' +
					'<td><button onclick="getAgrInfoPopClick(\''+ item.cls_id +'\',\''+ item.cls_ver +'\');event.stopPropagation();">수정</button></td>' +
				'</tr>'
			).data('item', item).appendTo('#clsInfoTable tbody');
		}
		$('#clsInfoTable tbody').find('tr').css('cursor', 'pointer')
		.click(function(e) {
			$('#clsInfoTable tbody').find('tr').removeClass('bgon');
			$(this).addClass('bgon');
			ajaxViewPrvdrItem($(this).data('item').cls_id, $(this).data('item').cls_ver);
		});
		$('#clsInfoTable tbody tr:eq(0)').trigger('click');
	},null,true,true,false);
}
//제공사항정보 조회
function ajaxViewPrvdrItem(cls_id, cls_ver) {
	var params = {
		CLS_ID		: cls_id
		, CLS_VER	: cls_ver
		, PRVDR_ID  : $('#in_prvdrId').val()
	};
	PHFnc.ajax("${ViewRoot}/system/ajaxViewPrvdrItem.do",params,"POST","json",function(data, textStatus, jqXHR) {
		$('#prvdItemTable tbody').empty();
		for (var i=0; i<data.rows.length; i++) {
			var item = data.rows[i];
			$(	'<tr>' +
					'<td>'+ item.no +'</td>' +
					'<td>'+ item.prvdr_id +'</td>' +
					'<td style="text-align:left;white-space:nowrap;text-overflow:ellipsis;overflow:hidden;" title="'+ item.prvdr_nm +'">'+ PHUtil.nvl(item.prvdr_nm) +'</td>' +
					'<td>'+ PHUtil.nvl(item.rcvr_id) +'</td>' +
					'<td>'+ item.agre_yn +'</td>' +
					'<td>'+ popDateValue(PHUtil.nvl(item.mdfy_dt)) +'</td>' +
					'<td><button onclick="getPrvdrItemPopClick(\''+ item.cls_id+'\', \''+ item.cls_ver + '\', \''+ item.prvdr_id +'\');event.stopPropagation();">수정</button></td>' +
				'</tr>'
			).data('item', item).appendTo('#prvdItemTable tbody');
		}
	},null,true,true,false);
}
//팝업 세팅 초기화
function initPopClick() {
	$('[id^=c_in_]').val('');
	$('[id^=c_sel_]').val('');
	
	$('[name=c_ra_use_yn]').prop('checked', false);
	$('[name=c_ra_mand_yn]').prop('checked', false);
	$('[name=c_ra_agre_yn]').prop('checked', false);
	
	$('[name=m_rgst_user_nm]').html('');
	$('[name=m_rgst_dt]').html('');
	$('[name=m_mdfy_user_nm]').html('');
	$('[name=m_mdfy_dt]').html('');
} 
//약관정보 등록 팝업 호출
function createAgrInfoPopClick() {
	initPopClick();
	PHFnc.layerPopOpen(1);
}
//약관정보 상세 팝업 호출
function getAgrInfoPopClick(cls_id, cls_ver) {
	initPopClick();
	var params = {
		CLS_ID	: cls_id
		, CLS_VER : cls_ver
	};
	PHFnc.ajax("${ViewRoot}/system/ajaxGetAgrInfo.do",params,"POST","json",function(data, textStatus, jqXHR) {
		var item = data.row;
		//팝업창에 데이터 세팅
		$('#m_in_cls_id2').val(item.cls_id);
		$('#m_in_cls_ver2').val(item.cls_ver);
		$('#m_sel_service_id').val(item.service_id);
		$(':radio[name=m_ra_use_yn][value='+ item.use_yn +']').prop('checked', true);
		$('#m_in_cls_titl').val(item.cls_titl);
		$('#m_in_cls_sub_titl').val(item.cls_sub_titl);
		$('#m_in_cls_url').val(item.cls_url);
		$('#m_in_sort_ord2').val(item.sort_ord);
		$(':radio[name=m_ra_mand_yn][value='+ item.mand_yn +']').prop('checked', true);
		$('#m_in_rmrk2').val(item.rmrk);
		$('#m_in_aply_strt_dd').val(PHUtil.isEmpty(item.aply_strt_dd) ? '' : PHUtil.fmtDate(item.aply_strt_dd,'-'));
		$('#m_in_aply_end_dd').val(PHUtil.isEmpty(item.aply_end_dd) ? '' : PHUtil.fmtDate(item.aply_end_dd,'-'));
		$('[name=m_rgst_user_nm]').html(item.rgst_user_nm);
		$('[name=m_rgst_dt]').html(item.rgst_dt);
		$('[name=m_mdfy_user_nm]').html(item.mdfy_user_nm);
		$('[name=m_mdfy_dt]').html(item.mdfy_dt);
		PHFnc.layerPopOpen(2);
	},null,true,true,false);
}
//제공사항정보 등록 팝업 호출
function createPrvdrItemPopClick() {
	if ($('#clsInfoTable tbody').find('tr.bgon').length == 0) {
		alert('약관정보를 먼저 선택하세요');
		return;
	}
	initPopClick();
	var item = $('#clsInfoTable tbody').find('tr.bgon:eq(0)').data('item');
	$('#c_in_cls_id3').val(item.cls_id);
	$('#c_in_cls_ver3').val(item.cls_ver);
	PHFnc.layerPopOpen(3);
}
//제공사항정보 상세 팝업 호출
function getPrvdrItemPopClick(cls_id, cls_ver, prvdr_id) {
	initPopClick();
	var params = {
		CLS_ID	  	: cls_id
		, CLS_VER 	: cls_ver
		, PRVDR_ID 	: prvdr_id 
	};
	PHFnc.ajax("${ViewRoot}/system/ajaxGetPrvdrItem.do",params,"POST","json",function(data, textStatus, jqXHR) {
		var item = data.row;
		//팝업창에 데이터 세팅
		$('#m_in_cls_id4').val(item.cls_id);
		$('#m_in_cls_ver4').val(item.cls_ver);
		$('#m_in_prvdr_id').val(item.prvdr_id);
		
		//이전 제공처ID 백업
		$('#m_in_prvdr_id_original').val(item.prvdr_id);
		
		$('#m_in_prvdr_nm').val(item.prvdr_nm);
		$('#m_in_rcvr_id').val(item.rcvr_id);
		$('#m_in_rcvr_nm').val(item.rcvr_nm);
		$('#m_in_prps').val(item.prps);
		$('#m_in_item').val(item.item);
		$('#m_in_hold_prd').val(item.hold_prd);
		$('#m_in_sort_ord4').val(item.sort_ord);
		$('[name=m_ra_agre_yn][value='+ item.agre_yn +']').prop('checked', true);
		$('#m_in_rmrk4').val(item.rmrk);
		$('[name=m_rgst_user_nm]').html(item.rgst_user_nm);
		$('[name=m_rgst_dt]').html(item.rgst_dt);
		$('[name=m_mdfy_user_nm]').html(item.mdfy_user_nm);
		$('[name=m_mdfy_dt]').html(item.mdfy_dt);
		PHFnc.layerPopOpen(4);
	},null,true,true,false);
}
//약관정보 등록
function createAgrInfo() {
	var currentDay = PHUtil.getToday();
	var cuYear = currentDay.substring(0,4);
	var cuMonth = currentDay.substring(4,6);
	var cuDay = currentDay.substring(6,8);
	var cuStrDay = cuYear+"-"+cuMonth+"-"+cuDay;
	var startDay = jQuery('#c_in_aply_strt_dd').val();
	var endDay = jQuery('#c_in_aply_end_dd').val(); 
	if(!(startDay=="" || startDay==null)) {
		if(startDay > cuStrDay) {
			alert("시작일자는 현재날짜를 초과할 수 없습니다.");	
			return;
		}else {
			if(!(endDay=="" || endDay==null)) {
				if(startDay > endDay) {
					alert("시작일자는 종료일자를 초과할 수 없습니다.");
					return;				
				}
				if(endDay > cuStrDay) {
					alert("종료일자는 현재날짜를 초과할 수 없습니다.");		
					return;
				} 
			}
		}
	}	
	if (jQuery.trim($('#c_in_cls_id1').val()) == '') {
		alert('약관ID를 입력하세요');
		return;
	}
	if (jQuery.trim($('#c_in_cls_ver1').val()) == '') {
		alert('약관버전을 입력하세요');
		return;
	}
	if ($('#c_sel_service_id').val() == '') {
		alert('서비스ID를 선택하세요');
		return;
	}
	if (jQuery.trim($('#c_in_cls_titl').val()) == '') {
		alert('약관타이틀을 입력하세요');
		return;
	}
	if (jQuery.trim($('#c_in_sort_ord1').val()) == '') {
		alert('정렬순서를 입력하세요');
		return;
	}
	if (confirm("저장하시겠습니까?")) {
		var params = {
			CLS_ID			: $('#c_in_cls_id1').val()
			, CLS_VER		: $('#c_in_cls_ver1').val()
			, SERVICE_ID	: $('#c_sel_service_id').val()
			, APLY_STRT_DD	: $('#c_in_aply_strt_dd').val()
			, APLY_END_DD	: $('#c_in_aply_end_dd').val()
			, CLS_TITL		: $('#c_in_cls_titl').val()
			, CLS_SUB_TITL	: $('#c_in_cls_sub_titl').val()
			, USE_YN		: $('[name=c_ra_use_yn]:checked').val()
			, SORT_ORD		: $('#c_in_sort_ord1').val()
			, MAND_YN		: $('[name=c_ra_mand_yn]:checked').val()
			, CLS_URL		: $('#c_in_cls_url').val()
			, RMRK			: $('#c_in_rmrk1').val()
		};
		PHFnc.ajax("${ViewRoot}/system/ajaxCreateAgrInfo.do",params,"POST","json",
			function (data, textStatus, jqXHR) {
				alert("등록에 성공했습니다.");
				$(".top_close").click();
				search();
			},
			function (data, textStatus, jqXHR) {
				PHFnc.alert(data.eMsg);
				PHFnc.submitLoadingDestory();
			},
		true,true,false);
	} 
}
//약관정보 수정
function modifyAgrInfo() {
	var currentDay = PHUtil.getToday();
	var cuYear = currentDay.substring(0,4);
	var cuMonth = currentDay.substring(4,6);
	var cuDay = currentDay.substring(6,8);
	var cuStrDay = cuYear+"-"+cuMonth+"-"+cuDay;
	var startDay = jQuery('#m_in_aply_strt_dd').val();
	var endDay = jQuery('#m_in_aply_end_dd').val(); 
	if(!(startDay=="" || startDay==null)) {
		if(startDay > cuStrDay) {
			alert("시작일자는 현재날짜를 초과할 수 없습니다.");	
			return;
		}else {
			if(!(endDay=="" || endDay==null)) {
				if(startDay > endDay) {
					alert("시작일자는 종료일자를 초과할 수 없습니다.");
					return;				
				}
				if(endDay > cuStrDay) {
					alert("종료일자는 현재날짜를 초과할 수 없습니다.");		
					return;
				} 
			}
		}
	}	
	if ($('#m_sel_service_id').val() == '') {
		alert('서비스ID를 선택하세요');
		return;
	}
	if (jQuery.trim($('#m_in_cls_titl').val()) == '') {
		alert('약관타이틀을 입력하세요');
		return;
	}
	if (jQuery.trim($('#m_in_sort_ord2').val()) == '') {
		alert('정렬순서를 입력하세요');
		return;
	}
	if (confirm("저장하시겠습니까?")) {
		var params = {
			CLS_ID			: $('#m_in_cls_id2').val()
			, CLS_VER		: $('#m_in_cls_ver2').val()
			, SERVICE_ID	: $('#m_sel_service_id').val()
			, APLY_STRT_DD	: $('#m_in_aply_strt_dd').val()
			, APLY_END_DD	: $('#m_in_aply_end_dd').val()
			, CLS_TITL		: $('#m_in_cls_titl').val()
			, CLS_SUB_TITL	: $('#m_in_cls_sub_titl').val()
			, USE_YN		: $('[name=m_ra_use_yn]:checked').val()
			, SORT_ORD		: $('#m_in_sort_ord2').val()
			, MAND_YN		: $('[name=m_ra_mand_yn]:checked').val()
			, CLS_URL		: $('#m_in_cls_url').val()
			, RMRK			: $('#m_in_rmrk2').val()
		};
		PHFnc.ajax("${ViewRoot}/system/ajaxModifyAgrInfo.do",params,"POST","json",
			function (data, textStatus, jqXHR) {
				alert("수정에 성공했습니다.");
				$(".top_close").click();
				search();
			},
			function (data, textStatus, jqXHR) {
				PHFnc.alert(data.eMsg);
				PHFnc.submitLoadingDestory();
			},
		true,true,false);
	}
}
//제공사항정보 등록
function createPrvdrItem() {
	if (jQuery.trim($('#c_in_prvdr_id').val()) == '') {
		alert('제공처ID를 입력하세요');
		return;
	}
	if (jQuery.trim($('#c_in_prvdr_nm').val()) == '') {
		alert('제공처이름을 입력하세요');
		return;
	}
	if (jQuery.trim($('#c_in_sort_ord3').val()) == '') {
		alert('정렬순서를 입력하세요');
		return;
	}
	if (confirm("저장하시겠습니까?")) {
		var params = {
			CLS_ID		: $('#c_in_cls_id3').val()
			, CLS_VER	: $('#c_in_cls_ver3').val()
			, PRVDR_ID	: $('#c_in_prvdr_id').val()
			, PRVDR_NM	: $('#c_in_prvdr_nm').val()
			, RCVR_ID	: $('#c_in_rcvr_id').val()
			, RCVR_NM	: $('#c_in_rcvr_nm').val()
			, PRPS		: $('#c_in_prps').val()
			, ITEM		: $('#c_in_item').val()
			, HOLD_PRD	: $('#c_in_hold_prd').val()
			, AGRE_YN	: $('[name=c_ra_agre_yn]:checked').val()
			, SORT_ORD	: $('#c_in_sort_ord3').val()
			, RMRK		: $('#c_in_rmrk3').val()
		};
		PHFnc.ajax("${ViewRoot}/system/ajaxCreatePrvdrItem.do",params,"POST","json",
			function (data, textStatus, jqXHR) {
				alert("등록에 성공했습니다.");
				$(".top_close").click();
				search();
			},
			function (data, textStatus, jqXHR) {
				PHFnc.alert(data.eMsg);
				PHFnc.submitLoadingDestory();
			},
		true,true,false);
	}
}
//제공사항정보 수정
function modifyPrvdrItem() {
	if (jQuery.trim($('#m_in_prvdr_id').val()) == '') {
		alert('제공처ID를 입력하세요');
		return;
	}
	if (jQuery.trim($('#m_in_prvdr_nm').val()) == '') {
		alert('제공처이름을 입력하세요');
		return;
	}
	if (jQuery.trim($('#m_in_sort_ord4').val()) == '') {
		alert('정렬순서를 입력하세요');
		return;
	}
	if (confirm("저장하시겠습니까?")) {
		var params = {
			CLS_ID		: $('#m_in_cls_id4').val()
			, CLS_VER	: $('#m_in_cls_ver4').val()
			, PRVDR_ID	: $('#m_in_prvdr_id').val()
			, PRVDR_ID_ORIGIN : $('#m_in_prvdr_id_original').val()
			, PRVDR_NM	: $('#m_in_prvdr_nm').val()
			, RCVR_ID	: $('#m_in_rcvr_id').val()
			, RCVR_NM	: $('#m_in_rcvr_nm').val()
			, PRPS		: $('#m_in_prps').val()
			, ITEM		: $('#m_in_item').val()
			, HOLD_PRD	: $('#m_in_hold_prd').val()
			, AGRE_YN	: $('[name=m_ra_agre_yn]:checked').val()
			, SORT_ORD	: $('#m_in_sort_ord4').val()
			, RMRK		: $('#m_in_rmrk4').val()
		};
		PHFnc.ajax("${ViewRoot}/system/ajaxModifyPrvdrItem.do",params,"POST","json",
			function (data, textStatus, jqXHR) {
				alert("수정에 성공했습니다.");
				$(".top_close").click();
				search();
			},
			function (data, textStatus, jqXHR) {
				PHFnc.alert(data.eMsg);
				PHFnc.submitLoadingDestory();
			},
		true,true,false);
	} 
}
//날짜 개행 표시
function popDateValue(str) {
	return str.replace(' ','<br>');
}
</script>