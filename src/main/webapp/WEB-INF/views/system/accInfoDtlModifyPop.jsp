<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%>
<%--
 **********************************************************************************************
 * @desc : 접근제어 정보 상세 수정 팝업
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/system/accInfoDtlModifyPop.jsp
 * @author ojh
 * @since 2018.08.23
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.08.23    ojh        최초생성
 *
 **********************************************************************************************
 accInfoView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>
<div class="popup_content4">
	<div class="popup_title">접근제어상세정보 수정<a href="#!"><div class="top_close"></div></a></div>
	<table class="half_table" border="0" cellpadding="0" cellspacing="0">
		<tbody>
            <tr>
                <th>접근제어ID</th>
                <td id="m_id_dtl"></td>
                <th>제휴사명</th>
                <td id="m_nm_dtl"></td>
            </tr>
            <tr>
                <th>접근가능IP <span class="red">*</span></th>
                <td><input type="text" name="name" id="m_ip_dtl" onKeyDown="return onlyNum(event,'ip')" onKeyUp="return keyReplace(this)" style="width:200px;">
                <input type="hidden" name="name" id="m_ip_dtl_old">
                </td>
                <th>IP상태 <span class="red">*</span></th>
                <td>
                    <select id="m_stat_dtl" style="width:150px;">
                        <option value="all">선택</option>
                        <c:forEach items="${CTRL_IND}" var="code">
                    		<option value="${code.dtlCd}">${code.dtlCdNm}</option>
                    	</c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <th>등록자</th>
                <td id="m_rgId_dtl"></td>
                <th>등록일시</th>
                <td id="m_rgDt_dtl"></td>
            </tr>
            <tr>
                <th>수정자</th>
                <td id="m_mfId_dtl"></td>
                <th>수정일시</th>
                <td id="m_mfDt_dtl"></td>
            </tr>
		</tbody>
	</table>
	<div class="btnalign">
		<ul>
			<li><a href="#!"><div class="btn_cancel">취소</div></a></li>
			<li><a href="#!"><div class="btn_save" onClick="modifyAccInfoDtl()">저장</div></a></li>
		</ul>
	</div>
</div>
<div id="myPopup" class="popup">
</div>
<!-- popup 끝-->