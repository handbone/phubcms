 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%--
 **********************************************************************************************
 * @desc : 연동시스템 응답코드 수정 팝업
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/cmpn/pntPrvdrMsgModifyPop.jsp
 * @author jungukjae
 * @since 2019.06.18
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일      수 정 자        		수정내용
 * ----------   ----------   -----------------------------
 * 2019.06.18    jungukjae        		최초생성
 *
 **********************************************************************************************
 pntPrvdrMsgView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>

<!-- 포인트제공처 수정 시작-->
<div class="popup_content2">
	<div class="popup_title">연동시스템 응답코드 수정<a href="#!"><div class="top_close"></div></a></div>
	<table class="half_table" border="0" cellpadding="0" cellspacing="0">
		<tbody>
			<tr>
			    <th>연동시스템ID<span class="red">*</span></th>
			    <td><input type="text" name="name" id="m_prvdr_id" disabled="disabled" maxlength="20"></td>
			    <th>포인트코드<span class="red">*</span></th>
			    <td><input type="text" name="name" id="m_pnt_cd" maxlength="30"></td>
			</tr>
			<tr>
			    <th>메세지순번<span class="red">*</span></th>
			    <td><input type="text" name="name" id="m_msg_seq" disabled="disabled"></td>
			    <th>응답코드<span class="red">*</span></th>
			    <td><input type="text" name="name" id="m_msg_id" maxlength="10"></td>
			</tr>
			<tr>
			    <th>응답메세지</th>
			    <td colspan="3"><input type="text" name="name" id="m_msg" style="width: 100%;" maxlength="200"></td>
			</tr>
			<tr>
			    <th>참조값1</th>
			    <td colspan="3"><input type="text" name="name" id="m_rfrn_val1" style="width: 100%;" maxlength="20"></td>
			</tr>
			<tr>
			    <th>참조값2</th>
			    <td colspan="3"><input type="text" name="name" id="m_rfrn_val2" style="width: 100%;" maxlength="20"></td>
			</tr>
			<tr>
			    <th>참조값3</th>
			    <td colspan="3"><input type="text" name="name" id="m_rfrn_val3" style="width: 100%;" maxlength="20"></td>
			</tr>
			<tr>
			    <th>등록자</th>
			    <td id="m_rId"></td>
			    <th>등록일시</th>
			    <td id="m_rDt"></td>
			</tr>
			<tr>
			    <th>수정자</th>
			    <td id="m_mId"></td>
			    <th>수정일시</th>
			    <td id="m_mDt"></td>
			</tr>
		</tbody>
	</table>
	<div class="btnalign">
		<ul>
			<li><a href="#!"><div class="btn_cancel">취소</div></a></li>
			<li><a href="#!"><div class="btn_save" onClick="modifyPntPrvdrMsg()">저장</div></a></li>
			<li><a href="#!"><div id="btn_delete" onClick="deletePntPrvdrMsg()">삭제</div></a></li>
		</ul>
	</div>
</div>