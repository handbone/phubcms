<%@  page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
 **********************************************************************************************
 * @desc : 약관정보 관리
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/system/agrInfoView.jsp
 * @author bmg
 * @since 2019.05.07
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.05.07   bmg        최초생성
 * 2019.05.28    kimht        include 수정
 **********************************************************************************************
--%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<%-- global --%>
	<jsp:include page="/WEB-INF/views/include/globalVar.jsp" flush="false" />
	<%-- head --%>
	<jsp:include page="/WEB-INF/views/include/incHead.jsp" flush="false" />
</head>
<body>
<!-- loader 시작-->
<div class="loader" onclick="loader_close()">
    <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
</div>
<!-- loader 끝-->
<div id="wrap">	
<header>
	<%-- header --%>
	<jsp:include page="/WEB-INF/views/include/incHeader.jsp" flush="false" />
</header>
    <!-- 레프트 메뉴 해당페이지 표시 -->
    <style>.${sessionScope.USER_MENU.menu_id}{ color: #fff !important; background: #3f4759 !important; border-left: 3px solid #fff; cursor: hand;} </style>
    <!-- 레프트 메뉴 해당페이지 표시 끝 -->
    
    <!-- 약관정보 등록 시작-->
    <jsp:include page="./agrInfoCreatePop.jsp" flush="false" />
    
    <!-- 약관정보 수정 시작-->
    <jsp:include page="./agrInfoModifyPop.jsp" flush="false" />
    
    <!-- 제공사항정보 등록 시작-->
    <jsp:include page="./prvdrItemCreatePop.jsp" flush="false" />
    
    <!-- 제공사항정보 수정 시작-->
    <jsp:include page="./prvdrItemModifyPop.jsp" flush="false" />

<!-- dash_board 시작-->
<section id="dash_board">
    <!--side_menu 시작-->
	<jsp:include page="/WEB-INF/views/include/incMenu.jsp" flush="false" />
    <!--side_menu 끝-->

    <!--contents 시작-->
    <div class="contents mCustomScrollbar"> 
        <div class="page_title"><div class="page_name">${sessionScope.USER_MENU.menu_nm}</div><div class="page_location"><i class="fas fa-home"></i>${sessionScope.USER_MENU.mnu_path}</div></div>
        <div class="subject">
            <div class="page_search">
                <table class="search_tb" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="search_item">
                            <ul>
                                <li>서비스ID
                                	<select id="sel_serviceId">
			                            <option value="all">전체</option>
			                            <option value="SVC_BASE">포인트허브</option>
			                            <option value="SVC_FP">패밀리포인트</option>
			                        </select>
                                </li>
                                <li>약관ID
                                	<input type="text" name="name" id="in_clsId" maxlength="20" autocomplete="off">
                                </li>
                                <li>제공처ID
                                	<input type="text" name="name" id="in_prvdrId" maxlength="20" autocomplete="off">
                                </li>
                                <li>사용여부
                                	<select id="sel_useYn">
                                		<option value="all">전체</option>
                                		<option value="Y">사용</option>
                                		<option value="N">미사용</option>
                                	</select>
                                </li>
                            </ul>
                        </td>
                        <td class="btn_search">
                            <button type="button" onclick="search();">조회</button>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="page_contents">
                <ul>
                    <li class="contents_left">
                        <div class="tt_line">약관정보<button onclick="createAgrInfoPopClick();">등록</button></div>
                        <div class="con_table mCustomScrollbar">
                        <table id="clsInfoTable" class="cord_table" border="0" cellpadding="0" cellspacing="0" style="table-layout:fixed">
                        	<thead>
                        		<tr>
	                                <th style="width:40px;">NO</th>
	                                <th style="width:70px;">약관ID</th>
	                                <th style="width:80px;">약관버전</th>
	                                <th>약관타이틀</th>
	                                <th style="width:50px;">사용<br>여부</th>
	                                <th style="width:50px;">필수<br>여부</th>
	                                <th style="width:70px;">관리</th>
	                            </tr>
                        	</thead>
                            <tbody />
                        </table>
                        </div>
                    </li>
                    <li class="contents_right">
                        <div class="tt_line">제공사항정보<button onclick="createPrvdrItemPopClick();">등록</button></div>
                        <div class="con_table mCustomScrollbar">
                        <table id="prvdItemTable" class="cord_table" border="0" cellpadding="0" cellspacing="0" style="table-layout:fixed">
                        	<thead>
                        		<tr>
	                                <th style="width:40px;">NO</th>
	                                <th style="width:70px;">제공처<br>ID</th>
	                                <th>제공처명</th>
	                                <th style="width:70px;">제공<br>받는자</th>
	                                <th style="width:50px;">동의<br>여부</th>
	                                <th style="width:100px;">수정일시</th>
	                                <th style="width:70px;">관리</th>
	                            </tr>
                        	</thead>
                            <tbody />
                        </table>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <!-- 스크롤 때문에 여백 있어야함 -->
        <br>
        <br>
        <br>
        <br>
        <br>

        </div>
        <!--contents 끝-->
</section>
<!-- dash_board 끝--> 

    <!-- footer 시작-->
    <jsp:include page="/WEB-INF/views/include/incFooter.jsp" flush="false" />
    <section class="footer">	 
    </section>
    <!-- footer 끝-->
    
    <%-- js --%>
	<jsp:include page="./agrInfoViewJs.jsp" flush="false" />
	
</body>
</html>