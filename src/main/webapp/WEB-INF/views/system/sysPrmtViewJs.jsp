<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="com.olleh.pHubCMS.common.components.Constant"%>
<%--
 **********************************************************************************************
 * @desc : 시스템파라미터 관리 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/system/sysPrmtViewJs.jsp
 * @author bmg
 * @since 2018.09.07
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.07    bmg        최초생성
 * 
 **********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
$(document).ready(function () { 
	var colNames = ['NO', '파라미터코드', '파라미터값','파라미터그룹코드','용도구분','사용여부','수정자','수정일시','관리'];
	var colModel = [{ label: 'NO', name: 'no', width: cWidth * 0.03, key:true}
        ,{ label: '파라미터코드'	,name: 'prmt_cd' 		,width: cWidth * 0.14 }
        ,{ label: '파라미터값'		,name: 'prmt_val' 		,width: cWidth * 0.28, align: 'left' }
        ,{ label: '파라미터그룹코드',name: 'prmt_grp_nm'	,width: cWidth * 0.11 }
        ,{ label: '용도구분'		,name: 'use_ind_nm'		,width: cWidth * 0.08 }
        ,{ label: '사용여부'		,name: 'use_yn'			,width: cWidth * 0.08 }
        ,{ label: '수정자'			,name: 'mdfy_user_nm' 	,width: cWidth * 0.08 }
        ,{ label: '수정일시'		,name: 'mdfy_dt' 		,width: cWidth * 0.12 }
        ,{ label: '관리'			,name: 'c' 				,width: cWidth * 0.05,formatter:mbutton }
	];
	PHJQg.load('jqGrid','jqGridPager',colNames,colModel);
	
	$('#btn_excel').on('click', function() {
		if (PHJQg.size('jqGrid') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
		else {
			
			var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");
			if (conf) {
				var postData = {
					PRMT_CD: $(':text[name=prmt_cd]').val().toUpperCase(),
					PRMT_VAL: $(':text[name=prmt_val]').val(),
					PRMT_GRP_CD: $('#prmt_grp_cd').val(),
					USE_YN: $('#use_yn').val()
				};
				var colNames = ['NO', '파라미터코드', '파라미터값','파라미터그룹코드','용도구분','사용여부','수정자','수정일시'];
				var colModel = [{ label: 'NO', name: 'no' }
	                ,{ label: '파라미터코드'	,name: 'prmt_cd' }
	                ,{ label: '파라미터값'		,name: 'prmt_val' }
	                ,{ label: '파라미터그룹코드',name: 'prmt_grp_nm' }
	                ,{ label: '용도구분'		,name: 'use_ind_nm' }
	                ,{ label: '사용여부'		,name: 'use_yn' }
	                ,{ label: '수정자'			,name: 'mdfy_user_nm' }
	                ,{ label: '수정일시'		,name: 'mdfy_dt' }
				];
				PHJQg.excel('jqGridExcelDiv','${ViewRoot}/system/jgSysPrmtExcel.do',postData,colNames,colModel,'시스템파라미터관리');
			}	
		}
	});
	
	$(':text[name^=m_],:text[name^=c_]').keydown(function(e) {
		if (e.keyCode == 32) {
			return false;
		}
	});
	
	$(':text[name=m_prmt_cd],:text[name=c_prmt_cd]').keyup(function(e) {
		var keyID = e.keyCode;
		if (((keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39 
	    		|| keyID == 9 || keyID == 16 || keyID == 17 || keyID == 35 || keyID == 36)) {
			return true;
		} else {
			$(this).val($(this).val().replace(/[^a-z_0-9]/gi, ''));
			$(this).val($(this).val().toUpperCase());
		}
	});
});	
//검색버튼 클릭
function search() {
	var postData = {
		PRMT_CD: $(':text[name=prmt_cd]').val().toUpperCase(),
		PRMT_VAL: $(':text[name=prmt_val]').val(),
		PRMT_GRP_CD: $('#prmt_grp_cd').val(),
		USE_YN: $('#use_yn').val()
	};
	PHJQg.reload('jqGrid','jqGridPager','${ViewRoot}/system/jgSysPrmt.do',postData);
}
//formatter 버튼 만들어 주기 클릭시 No번호 param 전달
function mbutton(cellvalue,options,rowobject){
	return '<input type="button" class="table_btn" onclick="modifyPopClick('+rowobject.no+')" value="수정" />';
}
//등록 팝업 호출
function createPopClick() {
	$(':text[name=c_prmt_cd],:text[name=c_prmt_val]').val('');
	$('#c_prmt_grp_cd,#c_use_ind,#c_prmt_desc').val('');
	$(':radio[name=c_use_yn]:eq(0)').prop('checked', true);
	PHFnc.layerPopOpen(1);
}
var popId;
//수정 팝업 호출
function modifyPopClick(data){	
	var row = $("#jqGrid").jqGrid('getRowData',data);
	//누른 창 번호를 key로 상세 조회
	var params = {PRMT_CD : row.prmt_cd};
	popId = row.prmt_cd;
	PHFnc.ajax("${ViewRoot}/system/ajaxGetSysPrmt.do",params,"POST","json",function(data, textStatus, jqXHR) {
		//팝업창에 데이터 세팅
		$("#m_prmt_cd").html(data.row.prmt_cd);
		$("#m_prmt_grp_cd").val(data.row.prmt_grp_cd);
		$("#m_use_ind").val(data.row.use_ind);
		$(":text[name=m_prmt_val]").val(data.row.prmt_val);
		$(":radio[name=m_use_yn][value="+ data.row.use_yn +"]").prop('checked', true);
		$("#m_prmt_desc").val(data.row.prmt_desc);
		$("#m_rgst_user_id_nm").html(data.row.rgst_user_nm);
		$("#m_rgst_dt").html(data.row.rgst_dt);
		$("#m_mdfy_user_id_nm").html(data.row.mdfy_user_nm);
		$("#m_mdfy_dt").html(data.row.mdfy_dt);
		PHFnc.layerPopOpen(2);
	},null,true,true,false);
}
//시스템 파라미터 수정
function modifySysPrmt() {
	if ($('#m_prmt_grp_cd').val() == "") {
		alert('파라미터그룹코드를 선택해 주세요.');
		return;
	}
	
// 	if ($('#m_use_ind').val() == "") {
// 		alert('용도구분을 선택해주세요.');
// 		return;
// 	}

	if ($(':radio[name=m_use_yn]:checked').length == 0) {
		alert('사용여부를 체크해주세요.');
		return;
	}
	
	if ($.trim($(':text[name=m_prmt_val]').val()) == "") {
		alert('파라미터값을 입력해 주세요.');
		return;
	}
	
	if (confirm("저장하시겠습니까?")) {
		var params = {
			PRMT_CD: popId,
			PRMT_GRP_CD:  $('#m_prmt_grp_cd').val(),
			USE_IND: $('#m_use_ind').val(),
			USE_YN: $(':radio[name=m_use_yn]:checked').val(),
			PRMT_VAL: $(':text[name=m_prmt_val]').val(),
			PRMT_DESC: $.trim($('#m_prmt_desc').val())
		};
		PHFnc.ajax("${ViewRoot}/system/ajaxModifySysPrmt.do",params,"POST","json",
			function(data, textStatus, jqXHR) {
				alert("수정에 성공했습니다.");
				$(".top_close").click();
				search();
			},
			function(data, textStatus, jqXHR) {
				if (!PHUtil.isEmpty(data.eMsg)) {
					PHFnc.alert(data.eMsg);	
				} else {
					PHFnc.ajaxError(null,jqXHR);
				}
				PHFnc.submitLoadingDestory();
			},
		true,true,false);
	}
}
//시스템 파라미터 등록
function createSysPrmt() {
	if ($.trim($(':text[name=c_prmt_cd]').val()) == "") {
		alert('파라미터코드를 입력해 주세요.');
		return;
	}
	
	if ($('#c_prmt_grp_cd').val() == "") {
		alert('파라미터그룹코드를 선택해 주세요.');
		return;
	}
	
// 	if ($('#c_use_ind').val() == "") {
// 		alert('용도구분을 선택해주세요.');
// 		return;
// 	}

	if ($(':radio[name=c_use_yn]:checked').length == 0) {
		alert('사용여부를 체크해주세요.');
		return;
	}
	
	if ($.trim($(':text[name=c_prmt_val]').val()) == "") {
		alert('파라미터값을 입력해 주세요.');
		return;
	}

	if (confirm("저장하시겠습니까?")) {
		var params = {
			PRMT_CD: $(':text[name=c_prmt_cd]').val().toUpperCase(),
			PRMT_GRP_CD:  $('#c_prmt_grp_cd').val(),
			USE_IND: $('#c_use_ind').val(),
			USE_YN: $(':radio[name=c_use_yn]:checked').val(),
			PRMT_VAL: $(':text[name=c_prmt_val]').val(),
			PRMT_DESC: $.trim($('#c_prmt_desc').val())
		};
		PHFnc.ajax("${ViewRoot}/system/ajaxCreateSysPrmt.do",params,"POST","json",
			function(data, textStatus, jqXHR) {
				alert("등록에 성공했습니다.");
				$(".top_close").click();
				search();
			},
			function(data, textStatus, jqXHR) {
				if (!PHUtil.isEmpty(data.eMsg)) {
					PHFnc.alert(data.eMsg);	
				} else {
					PHFnc.ajaxError(null,jqXHR);
				}
				PHFnc.submitLoadingDestory();
			},
		true,true,false);
	}
}
 
</script>