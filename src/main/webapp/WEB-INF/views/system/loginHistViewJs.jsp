<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="com.olleh.pHubCMS.common.components.Constant"%>
<%--
 **********************************************************************************************
 * @desc : 로그인 이력 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/system/loginHistViewJs.jsp
 * @author bmg
 * @since 2018.09.18
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.18    bmg        최초생성
 * 
 **********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
$(document).ready(function () { 
	var colNames = ['No', '사용자ID', '사용자명','사용자그룹','로그인일시','로그인IP','로그인상태','로그인실패사유'];
	var colModel = [{ label: 'No', name: 'no', width: cWidth * 0.05 }
        ,{ label: '사용자ID'		, name: 'user_id' 		, width: cWidth * 0.08 	}
        ,{ label: '사용자명'		, name: 'user_nm' 		, width: cWidth * 0.08 	}
        ,{ label: '사용자그룹'		, name: 'user_grp_nm'	, width: cWidth * 0.08 	}
        ,{ label: '로그인일시'		, name: 'login_dt'		, width: cWidth * 0.12 	}
        ,{ label: '로그인IP'		, name: 'login_ip'		, width: cWidth * 0.10 	}
        ,{ label: '로그인상태'		, name: 'login_stat_nm'	, width: cWidth * 0.08 	}
        ,{ label: '로그인실패사유'	, name: 'login_err_rsn'	, width: cWidth * 0.16	,align:'left' }
	];
	PHJQg.load('jqGrid','jqGridPager',colNames,colModel);
	
	$('#btn_excel').on('click', function() {
		if (PHJQg.size('jqGrid') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
		if (!validateDate()) {
			return;
		}
		var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");
		if (conf) {
			var postData = {
				START_DATE: $('#start_date').val(),
				END_DATE: $('#end_date').val(),
				USER_ID: $(':text[name=user_id]').val(),
				USER_NM: $(':text[name=user_nm]').val(),
				USER_GRP: $('#user_grp').val(),
				LOGIN_IP: $(':text[name=login_ip]').val()
			};
			var colNames = ['No', '사용자ID', '사용자명','사용자그룹','로그인일시','로그인IP','로그인상태','로그인실패사유'];
			var colModel = [{ label: 'No', name: 'no' }
				,{ label: '사용자ID'		, name: 'user_id' 		}
				,{ label: '사용자명'		, name: 'user_nm' 		}
				,{ label: '사용자그룹'		, name: 'user_grp_nm' 	}
				,{ label: '로그인일시'		, name: 'login_dt' 		}
				,{ label: '로그인IP'		, name: 'login_ip' 		}
				,{ label: '로그인상태'		, name: 'login_stat_nm' }
				,{ label: '로그인실패사유'	, name: 'login_err_rsn'	}
			];
			PHJQg.excel('jqGridExcelDiv','${ViewRoot}/system/jgLoginHistExcel.do',postData,colNames,colModel,'로그인이력');
		}
	});
	
	//검색일자 달력 셋팅
	var dt = new Date();
	setDate(dt, dt);
	
	$(':text[name^=user_],:text[name=login_ip]').keydown(function(e) {
		if (e.keyCode == 32) {
			return false;
		}
	});
	
	$(':text[name=login_ip]').keyup(function(e) {
		var keyID = e.keyCode;
		if (((keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39 
	    		|| keyID == 9 || keyID == 16 || keyID == 17 || keyID == 35 || keyID == 36)) {
			return true;
		} else {
			$(this).val($(this).val().replace(/[^.0-9]/gi, ""));
		}
	});
	
	$('.ME02020100').parents('.deps3:eq(0)').css('display', 'block');
});	
//검색버튼 클릭
function search() {
	if (!validateDate()) {
		return;
	}
	var postData = {
		START_DATE: $('#start_date').val(),
		END_DATE: $('#end_date').val(),
		USER_ID: $(':text[name=user_id]').val(),
		USER_NM: $(':text[name=user_nm]').val(),
		USER_GRP: $('#user_grp').val(),
		LOGIN_IP: $(':text[name=login_ip]').val()
	};
	PHJQg.reload('jqGrid','jqGridPager','${ViewRoot}/system/jgLoginHist.do',postData);
}

//조회기간 제약 // 제약 Max한달 
function validateDate() {
	var sDate = $("#start_date").val();
	var eDate = $("#end_date").val();
	if (!PHUtil.isEmpty(sDate) && !PHValid.date(PHUtil.replaceAll(sDate,'-',''))) {
		alert('시작일자가 유효한 날짜 형식이 아닙니다');
		$('#start_date').val('');
		$('#start_date').focus();
		return false;
	}
	if (!PHUtil.isEmpty(eDate) && !PHValid.date(PHUtil.replaceAll(eDate,'-',''))) {
		alert('종료일자가 유효한 날짜 형식이 아닙니다');
		$('#end_date').val('');
		$('#end_date').focus();
		return false;
	}
	var startDate = new Date(sDate);
	var endDate = new Date(eDate);
	if (startDate.getTime() > endDate.getTime()) {		
		alert('뒤의 날짜는 앞의 날짜 이전으로 검색할 수 없습니다.')
		return false;
	} else {
		var valDate = new Date(sDate);
		valDate.setMonth(valDate.getMonth()+1);
		if (endDate.getTime() > valDate.getTime()) {
			alert('검색 기간은 한달을 넘을 수 없습니다.');
			return false;
		}
	}
	return true;
}

</script>