<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="com.olleh.pHubCMS.common.components.Constant"%>
<%--
 **********************************************************************************************
 * @desc : 공통코드 관리 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/system/comCdViewJs.jsp
 * @author bmg
 * @since 2018.09.11
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.11    bmg        최초생성
 * 
 **********************************************************************************************
--%>
<script type="text/javascript">
//Initialize
$(document).ready(function() {
	$(':text[name*=m_],:text[name*=c_]').keydown(function(e) {
		if (e.keyCode == 32) {
			return false;
		}
	});
	
	<%-- $(':text[name$=ind_cd],:text[name$=dtl_cd]').keyup(function(e) { --%>
	$(':text[name$=ind_cd]').keyup(function(e) {
		if (!(e.keyCode >= 37 && e.keyCode <=40)) {
			var input = $(this).val();
			$(this).val(input.toUpperCase());
			$(this).val(input.replace(/[^a-z_0-9]/gi, ""));
		}
	});
	
	$(':text[name$=sort_ord]').keyup(function(e) {
		var keyID = e.keyCode;
		if (((keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39 
	    		|| keyID == 9 || keyID == 16 || keyID == 17 || keyID == 35 || keyID == 36)) {
			return true;
		} else {
			$(this).val($(this).val().replace(/[^0-9]/gi, ""));
		}
	});
});
//검색 버튼 클릭 시
function search() {
	var params = {
		GRP_CD		: $('#grp_cd').val()
		, IND_CD	: $(':text[name=ind_cd]').val().toUpperCase()
		, IND_CD_NM	: $(':text[name=ind_cd_nm]').val()
		, COM_USE_YN: $('#com_use_yn').val()
		, DTL_CD	: $(':text[name=dtl_cd]').val()
		, DTL_CD_NM	: $(':text[name=dtl_cd_nm]').val()
		, DTL_USE_YN: $('#dtl_use_yn').val()
	};
	PHFnc.ajax("${ViewRoot}/system/ajaxViewComCd.do",params,"POST","json",function(data, textStatus, jqXHR) {
		$('#comCdTable tbody').empty();
		$('#dtlCdTable tbody').empty();
		for (var i=0; i<data.rows.length; i++) {
			var item = data.rows[i];
			$(	'<tr>' +
					'<td>'+ item.no +'</td>' +
					'<td>'+ PHUtil.nvl(item.grp_cd) +'</td>' +
					'<td style="text-align:left;white-space:nowrap;text-overflow:ellipsis;overflow:hidden;" title="'+ item.ind_cd +'">'+ item.ind_cd +'</td>' +
					'<td style="text-align:left;white-space:nowrap;text-overflow:ellipsis;overflow:hidden;" title="'+ PHUtil.nvl(item.ind_cd_nm) +'">'+ PHUtil.nvl(item.ind_cd_nm) +'</td>' +
					'<td>'+ PHUtil.nvl(item.use_yn) +'</td>' +
					'<td>'+ PHUtil.nvl(item.mdfy_user_nm) +'</td>' +
					'<td>'+ popDateValue(PHUtil.nvl(item.mdfy_dt)) +'</td>' +
					'<td><button onclick="getComCdPopClick(\''+ item.ind_cd +'\');event.stopPropagation();">수정</button></td>' +
				'</tr>'
			).data('item', item).appendTo('#comCdTable tbody');
		}
		$('#comCdTable tbody').find('tr').css('cursor', 'pointer')
		.click(function(e) {
			$('#comCdTable tbody').find('tr').removeClass('bgon');
			$(this).addClass('bgon');
			ajaxViewDtlCd($(this).data('item').ind_cd);
		});
		$('#comCdTable tbody tr:eq(0)').trigger('click');
	},null,true,true,false);
}
//공통코드 등록 팝업 호출
function comCdCreatePopClick() {
	$(':text[name=c_ind_cd],:text[name=c_ind_cd_nm]').val('');
	$('#c_grp_cd,#c_cd_desc').val('');
	$(':radio[name=c_com_use_yn]:eq(0)').prop('checked', true);
	PHFnc.layerPopOpen(1);
}
//공통 코드 등록
function createComCd() {
	if ($.trim($(':text[name=c_ind_cd]').val()) == "") {
		alert('구분코드를 입력해 주세요.');
		return;
	}
	if ($.trim($(':text[name=c_ind_cd_nm]').val()) == "") {
		alert('구분코드명을 입력해 주세요.');
		return;
	}
	if (confirm("저장하시겠습니까?")) {
		var params = {
			IND_CD		: $(':text[name=c_ind_cd]').val().toUpperCase()
			, IND_CD_NM	: $(':text[name=c_ind_cd_nm]').val()
			, GRP_CD	: $('#c_grp_cd').val()
			, USE_YN	: $(':radio[name=c_com_use_yn]:checked').val()
			, CD_DESC	: $.trim($('#c_cd_desc').val())
		};
		PHFnc.ajax("${ViewRoot}/system/ajaxCreateComCd.do",params,"POST","json",
			function (data, textStatus, jqXHR) {
				alert("등록에 성공했습니다.");
				$(".top_close").click();
				search();
			},
			function (data, textStatus, jqXHR) {
				PHFnc.alert(data.eMsg);
				PHFnc.submitLoadingDestory();
			},
		true,true,false);
	}
}
//공통 코드 수정 팝업 호출
function getComCdPopClick(code) {
	//누른 창 번호를 key로 상세 조회
	var params = {
		IND_CD: code
	};
	PHFnc.ajax("${ViewRoot}/system/ajaxGetComCd.do",params,"POST","json",function(data, textStatus, jqXHR) {
		var item = data.row;
		//팝업창에 데이터 세팅
		$(':text[name=m_ind_cd]').val(item.ind_cd);
		$(':text[name=m_ind_cd_nm]').val(item.ind_cd_nm);
		$('#m_grp_cd').val(item.grp_cd);
		$(':radio[name=m_com_use_yn][value='+ item.use_yn +']').prop('checked', true);
		$('#m_cd_desc').val(item.cd_desc);
		$('#m_rgst_user_id_nm').html(item.rgst_user_nm);
		$('#m_rgst_dt').html(item.rgst_dt);
		$('#m_mdfy_user_id_nm').html(item.mdfy_user_nm);
		$('#m_mdfy_dt').html(item.mdfy_dt);
		PHFnc.layerPopOpen(2);
	},null,true,true,false);
}
//공통 코드 수정
function modifyComCd() {
	if ($.trim($(':text[name=m_ind_cd_nm]').val()) == "") {
		alert('구분코드명을 입력해 주세요.');
		return;
	}
	if (confirm("저장하시겠습니까?")) {
		var params = {
			IND_CD		: $(':text[name=m_ind_cd]').val()
			, IND_CD_NM	: $(':text[name=m_ind_cd_nm]').val()
			, GRP_CD	: $('#m_grp_cd').val()
			, USE_YN	: $(':radio[name=m_com_use_yn]:checked').val()
			, CD_DESC	: $.trim($('#m_cd_desc').val())
		};
		PHFnc.ajax("${ViewRoot}/system/ajaxModifyComCd.do",params,"POST","json",
			function(data, textStatus, jqXHR) {
				alert("수정에 성공했습니다.");
				$(".top_close").click();
				search();
			},
			function (data, textStatus, jqXHR) {
				if (!PHUtil.isEmpty(data.eMsg)) {
					PHFnc.alert(data.eMsg);	
				} else {
					PHFnc.ajaxError(null,jqXHR);
				}
				PHFnc.submitLoadingDestory();
			},
		true,true,false);
	}
}
//상세코드 목록 조회
function ajaxViewDtlCd(code) {
	var params = {
		IND_CD		: code
		, DTL_CD	: $(':text[name=dtl_cd]').val()
		, DTL_CD_NM	: $(':text[name=dtl_cd_nm]').val()
		, DTL_USE_YN: $('#dtl_use_yn').val()
	};
	PHFnc.ajax("${ViewRoot}/system/ajaxViewDtlCd.do",params,"POST","json",function(data, textStatus, jqXHR) {
		$('#dtlCdTable tbody').empty();
		for (var i=0; i<data.rows.length; i++) {
			var item = data.rows[i];
			$(	'<tr>' +
					'<td>'+ PHUtil.nvl(item.no) +'</td>' +
					'<td>'+ PHUtil.nvl(item.dtl_cd) +'</td>' +
					'<td style="text-align:left;white-space:nowrap;text-overflow:ellipsis;overflow:hidden;" title="'+ item.dtl_cd_nm +'">'+ PHUtil.nvl(item.dtl_cd_nm) +'</td>' +
					'<td>'+ PHUtil.nvl(item.use_yn) +'</td>' +
					'<td>'+ PHUtil.nvl(item.mdfy_user_nm) +'</td>' +
					'<td>'+ popDateValue(PHUtil.nvl(item.mdfy_dt)) +'</td>' +
					'<td><button onclick="getDtlCdPopClick(\''+ item.ind_cd +'\', \''+ item.dtl_cd +'\');event.stopPropagation();">수정</button></td>' +
				'</tr>'
			).data('item', item).appendTo('#dtlCdTable tbody');
		}
	},null,true,true,false);
}
//상세코드 등록 팝업 호출
function createDtlCdPopClick() {
	var comCd = $('#comCdTable tbody').find('tr.bgon').data('item');
	if (comCd == null) {
		alert('구분코드 조회 후 선택하세요.');
		return;
	}
	$(':text[name=c_dtl_ind_cd]').val(comCd.ind_cd);
	$(':text[name=c_dtl_ind_cd_nm]').val(comCd.ind_cd_nm);
	$(':text[name=c_dtl_cd],:text[name=c_dtl_cd_nm],:text[name=c_dtl_cd_val],:text[name=c_dtl_sort_ord]').val('');
	$(':radio[name=c_dtl_dp_yn]:eq(0),:radio[name=c_dtl_use_yn]:eq(0)').prop('checked', true);
	$('#c_dtl_cd_desc,#c_dtl_rmrk').val('');
	PHFnc.layerPopOpen(3);
}
//상세코드 등록
function createDtlCd() {
	if ($.trim($(':text[name=c_dtl_cd]').val()) == "") {
		alert('상세코드를 입력해 주세요.');
		return;
	}
	if ($.trim($(':text[name=c_dtl_cd_nm]').val()) == "") {
		alert('상세코드명을 입력해 주세요.');
		return;
	}
	if ($.trim($(':text[name=c_dtl_sort_ord]').val()) == "") {
		alert('정렬순서를 입력해 주세요.');
		return;
	}
	var conf = confirm("저장하시겠습니까?");
	if (conf) {
		var params = {
			IND_CD		: $(':text[name=c_dtl_ind_cd]').val()
			, DTL_CD	: $(':text[name=c_dtl_cd]').val()
			, DTL_CD_NM	: $(':text[name=c_dtl_cd_nm]').val()
			, DP_YN		: $(':radio[name=c_dtl_dp_yn]:checked').val()
			, USE_YN	: $(':radio[name=c_dtl_use_yn]:checked').val()
			, CD_VAL	: $.trim($(':text[name=c_dtl_cd_val]').val())
			, SORT_ORD	: $(':text[name=c_dtl_sort_ord]').val()
			, CD_DESC	: $.trim($('#c_dtl_cd_desc').val())
			, RMRK		: $.trim($('#c_dtl_rmrk').val())
		};
		PHFnc.ajax("${ViewRoot}/system/ajaxCreateDtlCd.do",params,"POST","json",
			function(data, textStatus, jqXHR) {
				alert("등록에 성공했습니다.");
				$(".top_close").click();
				ajaxViewDtlCd($('#comCdTable tbody').find('tr.bgon').data('item').ind_cd);
			},
			function(data, textStatus, jqXHR) {
				PHFnc.alert(data.eMsg);
				PHFnc.submitLoadingDestory();
			},
		true,true,false);
	}
}
//상세코드 수정 팝업 호출
function getDtlCdPopClick(indCd, dtlCd) {
	//누른 창 번호를 key로 상세 조회
	var params = {IND_CD: indCd, DTL_CD : dtlCd};
	PHFnc.ajax("${ViewRoot}/system/ajaxGetDtlCd.do",params,"POST","json",function(data, textStatus, jqXHR) {
		var item = data.row;
		//팝업창에 데이터 세팅
		$('#m_dtl_ind_cd').html(item.ind_cd);
		$('#m_dtl_ind_cd_nm').html(item.ind_cd_nm);
		$(':text[name=m_dtl_cd]').val(item.dtl_cd);
		$(':text[name=m_dtl_cd_nm]').val(item.dtl_cd_nm);
		$(':radio[name=m_dtl_dp_yn][value='+ item.dp_yn +']').prop('checked', true);
		$(':radio[name=m_dtl_use_yn][value='+ item.use_yn +']').prop('checked', true);
		$(':text[name=m_dtl_cd_val]').val(item.cd_val);
		$(':text[name=m_dtl_sort_ord]').val(item.sort_ord);
		$('#m_dtl_cd_desc').val(item.cd_desc);
		$('#m_dtl_rmrk').val(item.rmrk);
		$('#m_dtl_rgst_id_nm').html(item.rgst_user_nm);
		$('#m_dtl_rgst_dt').html(item.rgst_dt);
		$('#m_dtl_mdfy_id_nm').html(item.mdfy_user_nm);
		$('#m_dtl_mdfy_dt').html(item.mdfy_dt);
		PHFnc.layerPopOpen(4);
	},null,true,true,false);
}
//상세코드 수정
function modifyDtlCd() {
	if ($.trim($(':text[name=m_dtl_cd_nm]').val()) == "") {
		alert('상세코드명을 입력해 주세요.');
		return;
	}
	if ($.trim($(':text[name=m_dtl_sort_ord]').val()) == "") {
		alert('정렬순서를 입력해 주세요.');
		return;
	}
	if (confirm("저장하시겠습니까?")) {
		var params = {
			IND_CD		: $('#m_dtl_ind_cd').html()
			, DTL_CD	: $(':text[name=m_dtl_cd]').val()
			, DTL_CD_NM	: $(':text[name=m_dtl_cd_nm]').val()
			, DP_YN		: $(':radio[name=m_dtl_dp_yn]:checked').val()
			, USE_YN	: $(':radio[name=m_dtl_use_yn]:checked').val()
			, CD_VAL	: $.trim($(':text[name=m_dtl_cd_val]').val())
			, SORT_ORD	: $(':text[name=m_dtl_sort_ord]').val()
			, CD_DESC	: $.trim($('#m_dtl_cd_desc').val())
			, RMRK		: $.trim($('#m_dtl_rmrk').val())
		};
		PHFnc.ajax("${ViewRoot}/system/ajaxModifyDtlCd.do",params,"POST","json",
			function(data, textStatus, jqXHR) {
				alert("수정에 성공했습니다.");
				$(".top_close").click();
				ajaxViewDtlCd($('#comCdTable tbody').find('tr.bgon').data('item').ind_cd);
			},
			function (data, textStatus, jqXHR) {
				if (!PHUtil.isEmpty(data.eMsg)) {
					PHFnc.alert(data.eMsg);	
				} else {
					PHFnc.ajaxError(null,jqXHR);
				}
				PHFnc.submitLoadingDestory();
			},
		true,true,false);
	}
}
// 날짜 개행 표시
function popDateValue(str) {
	return str.replace(' ','<br>');
}

</script>