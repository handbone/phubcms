<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="com.olleh.pHubCMS.common.components.Constant"%>
<%--
 **********************************************************************************************
 * @desc : 시스템메시지 관리 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/system/sysMsgViewJs.jsp
 * @author bmg
 * @since 2018.09.13
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.13    bmg        최초생성
 * 
 **********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
$(document).ready(function () { 
	var colNames = ['No','메시지ID', '메시지코드', '메시지명','메시지그룹','메시지구분','사용여부','수정자','수정일시','관리'];
	var colModel = [{ label: 'No', name: 'no', width: cWidth * 0.05, key:true }
        ,{ label: '메시지ID', 	name: 'msg_id', 		width: cWidth * 0.10 }
        ,{ label: '메시지코드', name: 'msg_cd' , 		width: cWidth * 0.08 }
        ,{ label: '메시지명', 	name: 'msg_nm' , 		width: cWidth * 0.30, align: 'left' }
        ,{ label: '메시지그룹', name: 'msg_grp_nm', 	width: cWidth * 0.10 }
        ,{ label: '메시지구분', name: 'msg_ind_nm', 	width: cWidth * 0.08 }
        ,{ label: '사용여부', 	name: 'use_yn', 		width: cWidth * 0.06 }
        ,{ label: '수정자', 	name: 'mdfy_user_nm' , 	width: cWidth * 0.053 }
        ,{ label: '수정일시', 	name: 'mdfy_dt' , 		width: cWidth * 0.12 }
        ,{ label: '관리', 		name: 'c' , 			width: cWidth * 0.05, formatter:mbutton }
	];
	PHJQg.load('jqGrid','jqGridPager',colNames,colModel);
	
	$('#btn_excel').on('click', function() {
		if (PHJQg.size('jqGrid') == 0) {
			alert('검색 결과가 없습니다.');
			return;
		}
		if (!validateDate()) {
			return;
		}
		var conf = confirm("해당 조건을 기준으로 엑셀 파일을 다운로드를 하시겠습니까?");
		if (conf) {
			var postData = {
				START_DATE: $('#start_date').val(),
				END_DATE: $('#end_date').val(),
				MSG_ID: $(':text[name=msg_id]').val(),
				MSG_CD: $(':text[name=msg_cd]').val(),
				MSG_NM: $(':text[name=msg_nm]').val(),
				MSG_GRP: $('#msg_grp').val(),
				MSG_IND: $('#msg_ind').val(),
				USE_YN: $('#use_yn').val()
			};
			var colNames = ['NO','메시지ID', '메시지코드', '메시지명','메시지그룹','메시지구분','사용여부','수정자','수정일시'];
			var colModel = [{ label: 'NO', name: 'no' }
		        ,{ label: '메시지ID', 	name: 'msg_id' }
		        ,{ label: '메시지코드', name: 'msg_cd' }
		        ,{ label: '메시지명', 	name: 'msg_nm' }
		        ,{ label: '메시지그룹', name: 'msg_grp_nm' }
		        ,{ label: '메시지구분', name: 'msg_ind_nm' }
		        ,{ label: '사용여부', 	name: 'use_yn' }
		        ,{ label: '수정자', 	name: 'mdfy_user_nm' }
		        ,{ label: '수정일시', 	name: 'mdfy_dt' }
			];
			PHJQg.excel('jqGridExcelDiv','${ViewRoot}/system/jgSysMsgExcel.do',postData,colNames,colModel,'시스템메시지관리');
		}
	});
	
	//검색일자 달력 셋팅
	setDate(null,null);
	
	$('#start_date,#end_date').click(function(e) {
		$(e.target).val('');
	});
	
	$(':text[name$=msg_cd]').keydown(function(e) {
		if (e.keyCode == 32) {
			return false;
		}
	});
	
	$(':text[name$=msg_cd]').keyup(function(e) {
		var keyID = e.keyCode;
		if (((keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 46 || keyID == 37 || keyID == 39 
	    		|| keyID == 9 || keyID == 16 || keyID == 17 || keyID == 35 || keyID == 36)) {
			return true;
		} else {
			$(this).val($(this).val().replace(/[^a-zA-Z0-9]/gi, ""));
		}
	});
});	
//검색버튼 클릭
function search() {
	if (!validateDate()) {
		return;
	}
	var postData = {
		START_DATE: $('#start_date').val(),
		END_DATE: $('#end_date').val(),
		MSG_ID: $(':text[name=msg_id]').val(),
		MSG_CD: $(':text[name=msg_cd]').val(),
		MSG_NM: $(':text[name=msg_nm]').val(),
		MSG_GRP: $('#msg_grp').val(),
		MSG_IND: $('#msg_ind').val(),
		USE_YN: $('#use_yn').val()
	};
	PHJQg.reload('jqGrid','jqGridPager','${ViewRoot}/system/jgSysMsg.do',postData);
}
//formatter 버튼 만들어 주기 클릭시 No번호 param 전달
function mbutton(cellvalue,options,rowobject){
	return '<input type="button" class="table_btn" onclick="modifyPopClick('+rowobject.no+')" value="수정" />';
}
var popId;
//수정 팝업 호출
function modifyPopClick(data){	
	var row = $("#jqGrid").jqGrid('getRowData',data);
	//누른 창 번호를 key로 상세 조회
	var params = { MSG_ID : row.msg_id };
	popId = row.msg_id;
	PHFnc.ajax("${ViewRoot}/system/ajaxGetSysMsg.do",params,"POST","json",function(data, textStatus, jqXHR) {
		var item = data.row;
		//팝업창에 데이터 세팅
		$(':text[name=m_msg_id]').val(item.msg_id);
		$(':text[name=m_msg_nm]').val(item.msg_nm);
		$(':text[name=m_msg_grp_nm]').val(item.msg_grp_nm);
		$(':text[name=m_msg_ind_nm]').val(item.msg_ind_nm);
		$(':text[name=m_msg_cd]').val(item.msg_cd);
		$(':radio[name=m_use_yn][value='+ item.use_yn +']').prop('checked', true);
		$('#m_rmrk').val(item.rmrk);
		$('#m_rgst_id_nm').html(item.rgst_user_nm);
		$('#m_rgst_dt').html(item.rgst_dt);
		$('#m_mdfy_id_nm').html(item.mdfy_user_nm);
		$('#m_mdfy_dt').html(item.mdfy_dt);
		PHFnc.layerPopOpen(2);
	},null,true,true,false);
}
//시스템 메시지 수정
function modifySysMsg() {
	if ($.trim($(':text[name=m_msg_nm]').val()) == "") {
		alert('메시지명을 입력해 주세요.');
		return;
	}
	if (confirm("저장하시겠습니까?")) {
		var params = {
			MSG_ID: popId,
			MSG_NM: $(':text[name=m_msg_nm]').val(),
			MSG_CD: $(':text[name=m_msg_cd]').val(),
			USE_YN: $(':radio[name=m_use_yn]:checked').val(),
			RMRK: $.trim($('#m_rmrk').val())
		};
		PHFnc.ajax("${ViewRoot}/system/ajaxModifySysMsg.do",params,"POST","json",
			function(data, textStatus, jqXHR) {
				alert("수정에 성공했습니다.");
				$(".top_close").click();
				search();
			},
			function(data, textStatus, jqXHR) {
				if (!PHUtil.isEmpty(data.eMsg)) {
					PHFnc.alert(data.eMsg);	
				} else {
					PHFnc.ajaxError(null,jqXHR);
				}
				PHFnc.submitLoadingDestory();
			},
		true,true,false);
	}
}
//시스템 메시지 등록 팝업 호출
function createSysMsgPopClick() {
	$(':text[name=c_msg_nm],:text[name=c_msg_cd]').val('');
	$('#c_msg_grp,#c_msg_ind,#c_rmrk').val('');
	$(':radio[name=c_use_yn]:eq(0)').prop('checked', true);
	PHFnc.layerPopOpen(1);
}
//시스템 메시지 등록
function createSysMsg() {
	if ($.trim($(':text[name=c_msg_nm]').val()) == "") {
		alert('메시지명을 입력해 주세요.');
		return;
	}
	if ($('#c_msg_grp').val() == "") {
		alert('메시지그룹을 선택해 주세요.');
		return;
	}
	if ($('#c_msg_ind').val() == "") {
		alert('메시지구분을 선택해 주세요.');
		return;
	}
	if ($.trim($(':text[name=c_msg_cd]').val()) == "") {
		alert('메시지코드를 입력해 주세요.');
		return;
	} else {
		if ($(':text[name=c_msg_cd]').val().length < 2) {
			alert('메시지코드를 2자리 이상 입력해 주세요.');
			return;
		}
	}
	if (confirm("저장하시겠습니까?")) {
		var params = {
			MSG_NM: $(':text[name=c_msg_nm]').val(),
			MSG_GRP: $('#c_msg_grp').val(),
			MSG_IND: $('#c_msg_ind').val(),
			MSG_CD: $(':text[name=c_msg_cd]').val(),
			USE_YN: $(':radio[name=c_use_yn]:checked').val(),
			RMRK: $.trim($('#c_rmrk').val())
		};
		PHFnc.ajax("${ViewRoot}/system/ajaxCreateSysMsg.do",params,"POST","json",
			function(data, textStatus, jqXHR) {
				alert("등록에 성공했습니다.");
				$(".top_close").click();
				search();
			},
			function(data, textStatus, jqXHR) {
				if (!PHUtil.isEmpty(data.eMsg)) {
					PHFnc.alert(data.eMsg);	
				} else {
					PHFnc.ajaxError(null,jqXHR);
				}
				PHFnc.submitLoadingDestory();
			},
		true,true,false);
	}
}
//기간 조건 체크
function validateDate() {
	var sDate = $("#start_date").val();
	var eDate = $("#end_date").val();
	if (!PHUtil.isEmpty(sDate) && !PHValid.date(PHUtil.replaceAll(sDate,'-',''))) {
		alert('시작일자가 유효한 날짜 형식이 아닙니다');
		$('#start_date').val('');
		$('#start_date').focus();
		return false;
	}
	if (!PHUtil.isEmpty(eDate) && !PHValid.date(PHUtil.replaceAll(eDate,'-',''))) {
		alert('종료일자가 유효한 날짜 형식이 아닙니다');
		$('#end_date').val('');
		$('#end_date').focus();
		return false;
	}
	var startDate = new Date(sDate);
	var endDate = new Date(eDate);
	if (startDate.getTime() > endDate.getTime()) {		
		alert('뒤의 날짜는 앞의 날짜 이전으로 검색할 수 없습니다.')
		return false;
	}
	return true;
}
</script>