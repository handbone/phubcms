<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%>
<%--
 **********************************************************************************************
 * @desc : 메뉴 정보 등록 팝업
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/system/actInfoCreatePop.jsp
 * @author bmg
 * @since 2018.10.01
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.10.01    bmg        최초생성
 *
 **********************************************************************************************
 menuInfoView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>
<!-- Request 액션정보 등록 시작 -->
<div class="popup_content3">
    <div class="popup_title">Request 액션정보 등록<a href="#!"><div class="top_close"></div></a></div>
	<table class="half_table" border="0" cellpadding="0" cellspacing="0">
	    <tbody>
		    <tr>
		        <th style="width:200px;">메뉴ID</th>
		        <td style="width:350px;" id="c_actn_menu_id"></td>
		        <th style="width:200px;">메뉴명</th>
		        <td id="c_actn_menu_nm"></td>
		    </tr>
		    <tr>
		        <th>URI <span class="red">*</span></th>
		        <td>
		        	<div style="display:inline-block;width:0px;">
		        		<input type="text" name="c_actn_uri" style="width:310px;">
		        	</div>
		        </td>
		        <th>AUTION명 <span class="red">*</span></th>
		        <td><input type="text" name="c_actn_nm" style="width:310px;"></td>
		    </tr>
		    <tr>
		        <th>URI METHOD <span class="red">*</span></th>
		        <td>
		            <select id="c_actn_uri_method" style="width:150px;">
		                <option value="">선택</option>
		                <c:forEach items="${URI_METHOD}" var="code">
							<option value="${code.dtlCd}">${code.dtlCdNm}</option>
						</c:forEach>
		            </select>
		        </td>
		        <th>로그기록 <span class="red">*</span></th>
		        <td>
		            <select id="c_log_write_yn" style="width:150px;">
		                <option value="">선택</option>
		                <option value="Y">사용</option>
		                <option value="N">미사용</option>
		            </select>
		        </td>
		    </tr>
	    </tbody>
	</table>
	<div class="btnalign">
	    <ul>
	        <li><a href="#!"><div class="btn_cancel">취소</div></a></li>
	        <li><a href="#!"><div class="btn_save" onclick="createActnInfo();">저장</div></a></li>
	    </ul>
	</div>
</div>