<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%>
<%--
 **********************************************************************************************
 * @desc : 시스템메시지 등록 팝업
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/system/sysMsgCreatePop.jsp
 * @author bmg
 * @since 2018.09.14
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일             수 정 자          수정내용
 * ----------   --------   -----------------------------
 * 2018.09.14    bmg        최초생성
 *
 **********************************************************************************************
 sysMsgView.jsp 파일에 import 되며 버튼 클릭시 보여진다.
--%>
<!-- 시스템메시지 등록 시작-->
<div class="popup_content">
    <div class="popup_title">시스템메시지 등록<a href="#!"><div class="top_close"></div></a></div>
    <table class="half_table" border="0" cellpadding="0" cellspacing="0">
        <tbody>
	        <tr>
	            <th>메시지 ID</th>
	            <td colspan="3"><input type="text" value="시스템에서 자동으로 생성합니다." style="width:200px;background-color:lightgray;" readonly></td>
	        </tr>
	        <tr>
	        	<th>메시지명  <span class="red">*</span></th>
	            <td colspan="3"><input type="text" name="c_msg_nm" style="width:540px;" maxlength="100"></td>
	        </tr>
	        <tr>
	            <th style="width:200px;">메시지그룹 <span class="red">*</span></th>
	            <td style="width:350px;">
	                <select id="c_msg_grp" style="width:165px;">
	                    <option value="">선택</option>
	                    <c:forEach items="${MSG_GRP}" var="code">
                        	<option value="${code.dtlCd}">${code.dtlCdNm}</option>
                        </c:forEach>
	                </select>
	            </td>
	            <th style="width:200px;">메시지구분 <span class="red">*</span></th>
	            <td>
	                <select id="c_msg_ind" style="width:165px;">
	                    <option value="">선택</option>
	                    <c:forEach items="${MSG_TYPE}" var="code">
	                    	<option value="${code.dtlCd}">${code.dtlCdNm}</option>
	                    </c:forEach>
	                </select>
	            </td>
	        </tr>
	        <tr>
	            <th>메시지코드  <span class="red">*</span></th>
	            <td><input type="text" name="c_msg_cd"></td>
	            <th>사용여부 </th>
	            <td>
	                <input type="radio" name="c_use_yn" value="Y" checked> 사용
	                <input type="radio" name="c_use_yn" value="N"> 미사용
	            </td>
	        </tr>
	        <tr>
	            <th>비고</th>
	            <td colspan="3"> <textarea id="c_rmrk" rows="5" maxlength="100"></textarea></td>
	        </tr>
        </tbody>
    </table>

    <div class="btnalign">
        <ul>
            <li><a href="#!"><div class="btn_cancel">취소</div></a></li>
            <li><a href="#!"><div class="btn_save" onclick="createSysMsg();">저장</div></a></li>
        </ul>
    </div>
</div>