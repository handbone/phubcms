<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="com.olleh.pHubCMS.common.utils.SessionUtils"%><%--
 **********************************************************************************************
 * @desc : 거래 내역(세틀뱅크) Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/voc/settleDealHistViewJs.jsp
 * @author bmg
 * @since 2019.01.09
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.01.09   ojh        최초생성
 * </pre>
 **********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();

$(document).ready(function () {
	var colNames = ['No', '포인트허브<br>거래번호', '고객ID', '이름','사용<br>포인트','전환<br>금액','상품<br>결제금액','상품명','거래구분',
	                'PG사명','사용처명','PG<br>거래번호','PG제공<br>사용처ID','취소<br>여부','원포인트허브<br>거래번호','거래일시','서비스명','상세<br>거래내역','거래구분코드'];
	var colModel = [ { label: 'No', name: 'no', width: cWidth * 0.03, key:true }
		,{ label: '포인트허브거래번호' ,name: 'phub_tr_no'		,width: cWidth * 0.12 }
		,{ label: '고객ID'				,name: 'cust_id'		,width: cWidth * 0.10 }
		,{ label: '이름'				,name: 'cust_nm'		,width: cWidth * 0.08 }
		,{ label: '사용포인트'			,name: 'ttl_pnt'		,width: cWidth * 0.06	,formatter: caStr, align:'right' }
		,{ label: '전환금액'			,name: 'ttl_pnt_amt'	,width: cWidth * 0.06	,formatter: caStr, align:'right' }
		,{ label: '상품결제금액'		,name: 'ttl_pay_amt'	,width: cWidth * 0.06	,formatter: caStr, align:'right' }
		,{ label: '상품명'				,name: 'goods_nm' 		,width: cWidth * 0.10	,align:'left' }
		,{ label: '거래구분'			,name: 'deal_ind_nm'	,width: cWidth * 0.08 }                  
		,{ label: 'PG사명'				,name: 'pg_cmpn_nm' 	,width: cWidth * 0.08	,align:'left' }
		,{ label: '사용처명'			,name: 'cprt_cmpn_nm' 	,width: cWidth * 0.10	,align:'left' }
		,{ label: 'PG거래번호'			,name: 'pg_deal_no' 	,width: cWidth * 0.25 }
		,{ label: 'PG제공사용처ID' 		,name: 'pg_send_po_id'  ,width: cWidth * 0.08	,align:'left' }
		,{ label: '취소여부' 			,name: 'cncl_yn'  		,width: cWidth * 0.04 }                 
		,{ label: '원포인트허브거래번호',name: 'ori_phub_tr_no' ,width: cWidth * 0.13 }
		,{ label: '거래일시' 			,name: 'deal_dt'  		,width: cWidth * 0.12 }
		,{ label: '서비스명' 			,name: 'service_nm'  	,width: cWidth * 0.10 }
		,{ label: '상세거래내역'	 	,name: ''  				,width: cWidth * 0.06	,formatter:dbutton }
		,{ label: '거래구분코드'	 	,name: 'deal_ind'  		,hidden:true	}
	];
	PHJQg.load('jqGrid','jqGridPager',colNames,colModel);
	
	var dt = new Date();	
    setDate(dt,dt);
    
    $('#in_cuTn').keypress(function(e) {
    	var keycode = e.keyCode ? e.keyCode : e.which;
    	if ((keycode >= 48 && keycode <= 57) || (keycode >= 96 && keycode <= 105)) {
    		return true;
    	} else {
    		switch(keycode) {
    			case 8:		// BACKSPACE
    			case 9:		// TAB
    			case 45:	// -
    				return true;
    				break;
    			default:
    				e.preventDefault();
    		}
    	}
    }).keyup(function(e) {
    	var value = $(this).val().replace(/[^0-9-]/g,"");
    	$(this).val(value);
    });
});

function caStr(cellvalue,options,rowobject){
	cellvalue = cellvalue != null ? cellvalue+'' : cellvalue;
	if(rowobject.deal_ind != 'PA'){
		if (Number(cellvalue)) {
			return "-"+PHUtil.setComma(cellvalue);
		}
		else {
			return "";
		}
	} else{
		return PHUtil.nvl(PHUtil.setComma(cellvalue),'');
	}
}

function dbutton(cellvalue,options,rowobject){
    return '<input type="button" class="table_btn" onclick="popClick('+rowobject.no+')" value="확인" />';
}

function search(){
	if (!validate()) {
		return;
	}
	var ctn = PHUtil.replaceAll($("#in_cuTn").val(),"-","");
	var postData = {
		DEAL_IND : $("#sel_ind").val()
		, CNCL_YN : $("#sel_cYn").val()
		, CUST_ID : $("#in_cuId").val()
		, CUST_NM : $("#in_cuNm").val()
		, CUST_CTN : PHUtil.replaceAll(ctn,"/","")
		, PHUB_TR_NO : $("#in_trNo").val()
		, PG_DEAL_NO : $('#in_pgDealNo').val()
		, PH_SVC_CD 	: $('#sel_svcCd').val()
		, START_DATE : $("#start_date").val()
		, END_DATE 	: $("#end_date").val()
	};
	PHJQg.reloadMessage('jqGrid', null,'${ViewRoot}/voc/jgSettleDealHist.do',postData);
}

function validate() {
	if ($.trim($("#in_cuId").val()) == '' && $.trim($("#in_cuNm").val()) == '' && $.trim($("#in_cuTn").val()) == '' && 
			$.trim($("#in_trNo").val()) == '' && $.trim($('#in_pgDealNo').val()) == '') {
		alert('검색 조건을 입력하세요.');
		return false;
	}
	return validateDate();
}

function validateDate() {
	var sDate = $("#start_date").val();
	var eDate = $("#end_date").val();
	if (!PHUtil.isEmpty(sDate) && !PHValid.date(PHUtil.replaceAll(sDate,'-',''))) {
		alert('시작일자가 유효한 날짜 형식이 아닙니다');
		$('#start_date').val('');
		$('#start_date').focus();
		return false;
	}
	if (!PHUtil.isEmpty(eDate) && !PHValid.date(PHUtil.replaceAll(eDate,'-',''))) {
		alert('종료일자가 유효한 날짜 형식이 아닙니다');
		$('#end_date').val('');
		$('#end_date').focus();
		return false;
	}
	var startDate = new Date(sDate);
	var endDate = new Date(eDate);
	if (startDate.getTime() > endDate.getTime()) {		
		alert('뒤의 날짜는 앞의 날짜 이전으로 검색할 수 없습니다.')
		return false;
	} else {
		var valDate = new Date(sDate);
		valDate.setMonth(valDate.getMonth()+1);
		if (endDate.getTime() > valDate.getTime()) {
			alert('검색 기간은 한달을 넘을 수 없습니다.');
			return false;
		}
	}
	return true;
}

function popClick(no) {
	var row = $("#jqGrid").jqGrid('getRowData', no);
	var bText = [];
	$('.info_table tbody').html(
		"<tr>" +
			"<td>"+row.phub_tr_no+"</td>"+
			"<td>"+row.cust_id+"</td>"+
			"<td>"+row.cust_nm+"</td>"+
			"<td style='text-align:right'>"+row.ttl_pnt+"</td>"+
			"<td style='text-align:right'>"+row.ttl_pnt_amt+"</td>"+
			"<td style='text-align:right'>"+row.ttl_pay_amt+"</td>"+
			"<td>"+row.deal_ind_nm+"</td>"+
			"<td>"+row.pg_cmpn_nm+"</td>"+
			"<td>"+row.cncl_yn+"</td>"+
			"<td>"+row.deal_dt+"</td>"+
		"</tr>"
	);
	PHFnc.ajax("${ViewRoot}/voc/ajaxDealReqDtl.do", { 'PHUB_TR_NO': row.phub_tr_no },"POST","json",function(data,textStatus,jqXHR) {
		$('.detail_table tbody').empty();
		if (data.rows == null) {
			return;
		}
		var arr_html = [];
		for (var i=0; i<data.rows.length; i++) {
			var gb = (data.rows[i].deal_ind == "CA")? "-" : "";
			arr_html.push("<tr>");
				arr_html.push("<td>"+data.rows[i].no+"</td>");
				arr_html.push("<td style='text-align:left'>"+data.rows[i].pnt_nm+"</td>");
				arr_html.push("<td>"+data.rows[i].pnt_tr_no+"</td>");
				arr_html.push("<td style='text-align:right'>"+ gb + PHUtil.setComma(data.rows[i].ans_pnt) +"</td>");
				arr_html.push("<td>"+data.rows[i].pnt_exch_rate+"</td>");
				arr_html.push("<td style='text-align:right'>"+ gb + PHUtil.setComma(data.rows[i].ans_pnt_amt)+"</td>");
			arr_html.push("</tr>");
		}
		$('.detail_table tbody').html(arr_html.join(''));
	},null,true,true);
	
	PHFnc.layerPopOpen(1);
}
</script>