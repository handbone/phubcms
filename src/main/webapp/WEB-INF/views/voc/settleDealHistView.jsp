<%@  page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
 **********************************************************************************************
 * @desc : 거래 내역(세틀뱅크)
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/voc/settleDealHistView.jsp
 * @author bmg
 * @since 2019.01.09
 * @version 1.0
 * @see 
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.01.09   bmg        최초생성
 * 2019.05.29    kimht        include 수정
 **********************************************************************************************
--%>
<!DOCTYPE html>
<html lang="ko">
<head>
	<%-- global --%>
	<jsp:include page="/WEB-INF/views/include/globalVar.jsp" flush="false" />
	<%-- head --%>
	<jsp:include page="/WEB-INF/views/include/incHead.jsp" flush="false" />
</head>
<body>
<!-- loader 시작-->
<div class="loader" onclick="loader_close()">
    <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
</div>
<!-- loader 끝-->
<div id="wrap">	
<header>
	<%-- header --%>
	<jsp:include page="/WEB-INF/views/include/incHeader.jsp" flush="false" />
</header>

    <!-- 레프트 메뉴 해당페이지 표시 -->
    <style>.${sessionScope.USER_MENU.menu_id}{ color: #fff !important; background: #3f4759 !important; border-left: 3px solid #fff; cursor: hand;} </style>
    <!-- 레프트 메뉴 해당페이지 표시 끝 -->

    <!-- popup 시작-->
    <jsp:include page="./settleDealHistDtlPop.jsp" flush="false" />
    <!-- popup 끝-->

<!-- dash_board 시작-->
<section id="dash_board">
    <!--side_menu 시작-->
	<jsp:include page="/WEB-INF/views/include/incMenu.jsp" flush="false" />
    <!--side_menu 끝-->

    <!--contents 시작-->
    <div class="contents mCustomScrollbar">
        <div class="page_title"><div class="page_name">${sessionScope.USER_MENU.menu_nm}</div><div class="page_location"><i class="fas fa-home"></i>${sessionScope.USER_MENU.mnu_path}</div></div>
        <div class="subject">
        	<div class="page_search">
                <table class="search_tb" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="search_item">
                            <ul>
			                    <li>거래일자 <input type="text" name="name" id="start_date" readonly> ~ <input type="text" name="name"  id="end_date" readonly></li>
			                    <li>거래구분
			                        <select id="sel_ind">
			                            <option value="all">전체</option>
			                           	<c:forEach items="${DEAL_IND}" var="code">
			                           		<option value="${code.dtlCd}">${code.dtlCdNm}</option>
			                           	</c:forEach>
			                        </select>
			                    </li>			                    
			                    <li>취소여부
			                        <select id="sel_cYn">
			                            <option value="all">전체</option>
			                            <option value="Y">Y</option>
			                            <option value="N">N</option>
			                            <option value="A">A</option>
			                        </select>
			                    </li>
			                    <li>고객ID <input type="text" name="name" id="in_cuId" autocomplete="off" maxlength="20"></li>
								<li>이름 <input type="text" name="name" id="in_cuNm" autocomplete="off" maxlength="20"></li>
							</ul>
							<ul>
								<li>포인트허브거래번호 <input type="text" name="name" id="in_trNo" autocomplete="off" maxlength="30" style="width:215px"></li>
								<li>PG거래번호 <input type="text" name="name" id="in_pgDealNo" style="width:217px" autocomplete="off" maxlength="40"></li>
								<li>핸드폰번호 <input type="text" name="name" id="in_cuTn" autocomplete="off" maxlength="20" style="width:100px"></li>
								<li>서비스명
			                		<select id="sel_svcCd">
			                			<option value="all">전체</option>
			                			<c:forEach items="${PH_SVC_CD}" var="code">
			                				<option value="${code.dtlCd}">${code.dtlCdNm}</option>
			                			</c:forEach>
			                		</select>
			                	</li>
							</ul>
                        </td>
                        <td class="btn_search">
                            <button type="button" onClick="search()">조회</button>
                        </td>
                    </tr>
                </table>
            </div>            
            <div class="page_contents">
                <div id="content">
                    <table id="jqGrid"></table>                
                </div>
            </div>
        </div>
        <!-- 스크롤 때문에 여백 있어야함 -->
        <br>
        <br>
        <br>
        <br>
        <br>

        </div>
    <!--contents 끝-->
</section>
<!-- dash_board 끝--> 

    <!-- footer 시작-->
    <jsp:include page="/WEB-INF/views/include/incFooter.jsp" flush="false" />
    <section class="footer">	 
    </section>
    <!-- footer 끝-->
    
    <%-- js --%>
	<jsp:include page="./settleDealHistViewJs.jsp" flush="false" />
	
</body>
</html>