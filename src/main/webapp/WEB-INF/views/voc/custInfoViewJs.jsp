<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
 **********************************************************************************************
 * @desc : 고객정보 조회 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/voc/custInfoViewJs.jsp
 * @author bmg
 * @since 2019.01.15
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.01.15    bmg       최초생성
 * </pre>
 **********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
$(document).ready(function () {
	// jQgrid 초기화
	var colNames = ['No', 'ID', '이름','핸드폰번호','통신사','클립멤버여부','클립가입일','클립탈퇴일','가입채널','약관동의내역'];
	var colModel = [{ label: 'No', name: 'no', width: cWidth * 0.11, key:true }
		,{ label: 'ID'				,name: 'cust_id'  		,width: cWidth * 0.11 }
		,{ label: '이름'			,name: 'cust_nm'  		,width: cWidth * 0.11	,align:'left' }
		,{ label: '핸드폰번호' 		,name: 'cust_ctn' 		,width: cWidth * 0.11	,align:'left' }
		,{ label: '통신사' 			,name: 'telecom_ind' 	,width: cWidth * 0.11 }
		,{ label: '클립멤버여부'	,name: 'clip_mmbr_yn' 	,width: cWidth * 0.11 }
		,{ label: '클립가입일' 		,name: 'clip_sbsc_dt'  	,width: cWidth * 0.11 }
		,{ label: '클립탈퇴일' 		,name: 'clip_rtr_dd'  	,width: cWidth * 0.11 }
		,{ label: '가입채널' 		,name: 'cprt_cmpn_nm'  	,width: cWidth * 0.11 }
		,{ label: '약관동의내역'	,name: ''  				,width: cWidth * 0.12,	formatter:dbutton}
	];
	PHJQg.load('jqGrid','jqGridPager',colNames,colModel);
	
	$('#in_pn').keypress(function(e) {
    	var keycode = e.keyCode ? e.keyCode : e.which;
    	if ((keycode >= 48 && keycode <= 57) || (keycode >= 96 && keycode <= 105)) {
    		return true;
    	} else {
    		switch(keycode) {
    			case 8:		// BACKSPACE
    			case 9:		// TAB
    			case 45:	// -
    				return true;
    				break;
    			default:
    				e.preventDefault();
    				return false;
    		}
    	}
    }).keyup(function(e) {
    	var value = $(this).val().replace(/[^0-9-]/g,"");
    	$(this).val(value);
    });
});
//조회시 reload
function search(){
	if (!validate()) {
		return;
	}
	var postData = {
		CUST_ID : $("#in_id").val()
		, CUST_NM : $("#in_nm").val()
		, CUST_CTN : $("#in_pn").val()
		, CLIP_MMBR_YN : $("#sel_mYN").val()
		, PG_DEAL_NO : $("#in_pgDealNo").val()
		, CPRT_CMPN_NM 	: $("#in_cprt_cmpn_nm").val()
	};
	PHJQg.reloadMessage('jqGrid','jqGridPager','${ViewRoot}/voc/jgCustInfo.do',postData);
}
//formatter 버튼 만들어 주기 클릭시 no전달
function dbutton(cellvalue,options,rowobject){
    return '<input type="button" class="table_btn" onclick="popClick('+rowobject.no+')" value="확인" />';
}
//클릭시 상세 정보 창에 data 셋팅
function popClick(data){
	//요청 내역 셋팅
	var row = $("#jqGrid").jqGrid('getRowData',data);
	
	$('.info_table tbody').html(
		"<tr>" + 
			"<td>"+row.cust_id+"</td>" +
			"<td>"+row.cust_nm+"</td>" + 
			"<td>"+row.cust_ctn+"</td>" +
			"<td>"+row.cprt_cmpn_nm+"</td>" + 
		"</tr>"
	);
	
	//약관 내역 aJax호출하여 가져오기
	var params = {
		CUST_ID : row.cust_id
	};
	PHFnc.ajax("${ViewRoot}/voc/ajaxAgrHist.do",params,"POST","json",function(data) {
		$('.detail_table tbody').empty();
		if (data.rows == null || data.rows.length == 0) {
			return;
		}
		var arrHtml = [];
		for (var i=0; i<data.rows.length; i++) {
			arrHtml.push("<tr>");
				arrHtml.push("<td>"+data.rows[i].no+"</td>");
				arrHtml.push("<td>"+data.rows[i].cls_titl+"</td>");
				arrHtml.push("<td>"+data.rows[i].agre_yn+"</td>");
				arrHtml.push("<td>"+data.rows[i].mand_yn+"</td>");
				arrHtml.push("<td>"+data.rows[i].mdfy_dt+"</td>");
			arrHtml.push("</tr>");
		}
		$('.detail_table tbody').html(arrHtml.join(''));
	},null,true,true,false);
	
	//팝업창 오픈
	PHFnc.layerPopOpen(1);
}

function validate() {
	if ($.trim($("#in_id").val()) == '' && $.trim($("#in_nm").val()) == '' 
			&& $.trim($("#in_pn").val()) == '' && $.trim($("#in_pgDealNo").val()) == '') {
		alert('검색 조건을 입력하세요.');
		return false;
	}
	return true;
}
</script>