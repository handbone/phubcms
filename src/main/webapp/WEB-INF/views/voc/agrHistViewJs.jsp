<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"
%><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"
%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %><%--
 **********************************************************************************************
 * @desc : 약관동의내역 조회 Js
 * @FileName : /pHubCMS/src/main/webapp/WEB-INF/views/voc/agrHistViewJs.jsp
 * @author	bmg
 * @since 	2019.01.15
 * @version 1.0
 * @see 
 * <pre>
 * << 개정이력(Modification Information) >>
 * 수 정 일     수 정 자   수정내용
 * ----------   --------   -----------------------------
 * 2019.01.15   ojh        최초생성
 * </pre>
 **********************************************************************************************
--%>
<script type="text/javascript">
var cWidth = $("#content").width();
$(document).ready(function () {
	// jQgrid 초기화
	var colNames = ['No', 'ID', '이름','핸드폰번호','클립멤버여부','약관명','약관동의일','가입채널'];
	var colModel = [{ label: 'No', name: 'no', width: cWidth * 0.07	,key:true }
		,{ label: 'ID'			,name: 'cust_id'  		,width: cWidth * 0.10 }
		,{ label: '이름'		,name: 'cust_nm'  		,width: cWidth * 0.07	,align:'left' }
		,{ label: '핸드폰번호' 	,name: 'cust_ctn' 		,width: cWidth * 0.10	,align:'left' }
		,{ label: '클립멤버여부',name: 'clip_mmbr_yn'	,width: cWidth * 0.10 }
		,{ label: '약관명' 		,name: 'cls_titl'  		,width: cWidth * 0.45	,align:'left' }
		,{ label: '약관동의일' 	,name: 'rgst_dt'	  	,width: cWidth * 0.11 }
		,{ label: '가입채널' 	,name: 'cprt_cmpn_nm'	,width: cWidth * 0.11 }
	];
	PHJQg.load('jqGrid','jqGridPager',colNames,colModel);
	
	var dt = new Date();
    setDate(dt,dt);
    
    $('#in_ctn').keypress(function(e) {
    	var keycode = e.keyCode ? e.keyCode : e.which;
    	if ((keycode >= 48 && keycode <= 57) || (keycode >= 96 && keycode <= 105)) {
    		return true;
    	} else {
    		switch(keycode) {
    			case 8:		// BACKSPACE
    			case 9:		// TAB
    			case 45:	// -
    				return true;
    				break;
    			default:
    				e.preventDefault();
    				return false;
    		}
    	}
    }).keyup(function(e) {
    	var value = $(this).val().replace(/[^0-9-]/g,"");
    	$(this).val(value);
    });
});
//조회시 reload
function search() {
	if (!validate()) {
		return;
	}
	var postData = {
		CUST_ID 		: $("#in_id").val()
		,CUST_NM 		: $("#in_nm").val()
		,CUST_CTN 		: $("#in_ctn").val()
		,CLIP_MMBR_YN 	: $("#sel_cYn").val()
		,CPRT_CMPN_NM 	: $("#in_cprt_cmpn_nm").val()
		,START_DATE		: $('#start_date').val()
		,END_DATE		: $('#end_date').val()
	};
	PHJQg.reloadMessage('jqGrid','jqGridPager','${ViewRoot}/voc/jgAgrHist.do',postData);
}

function validate() {
	if ($('start_date').val() == '' || $('#end_date').val() == '') {
		alert('조회 시 동의 일자는 필수 입력입니다.');
		return false;
	}
	if ($.trim($("#in_id").val()) == '' && $.trim($("#in_nm").val()) == '' && $.trim($("#in_ctn").val()) == '') {
		alert('ID, 이름, 핸드폰번호 중 한개 이상의 검색 조건을 입력하세요.');
		return false;
	}
	return validateDate();
}

function validateDate() {
	var sDate = $("#start_date").val();
	var eDate = $("#end_date").val();
	if (!PHUtil.isEmpty(sDate) && !PHValid.date(PHUtil.replaceAll(sDate,'-',''))) {
		alert('시작일자가 유효한 날짜 형식이 아닙니다');
		$('#start_date').val('');
		$('#start_date').focus();
		return false;
	}
	if (!PHUtil.isEmpty(eDate) && !PHValid.date(PHUtil.replaceAll(eDate,'-',''))) {
		alert('종료일자가 유효한 날짜 형식이 아닙니다');
		$('#end_date').val('');
		$('#end_date').focus();
		return false;
	}
	var startDate = new Date(sDate);
	var endDate = new Date(eDate);
	if (startDate.getTime() > endDate.getTime()) {		
		alert('뒤의 날짜는 앞의 날짜 이전으로 검색할 수 없습니다.')
		return false;
	} else {
		var valDate = new Date(sDate);
		valDate.setMonth(valDate.getMonth()+1);
		if (endDate.getTime() > valDate.getTime()) {
			alert('검색 기간은 한달을 넘을 수 없습니다.');
			return false;
		}
	}
	return true;
}
</script>